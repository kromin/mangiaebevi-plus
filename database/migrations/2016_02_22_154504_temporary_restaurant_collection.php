<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TemporaryRestaurantCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        //
        Schema::create('temporary_restaurant_collection', function (Blueprint $collection) {
            //
            $collection->index('name');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('temporary_restaurant_collection', function (Blueprint $collection) {
            //
        });
    }
}
