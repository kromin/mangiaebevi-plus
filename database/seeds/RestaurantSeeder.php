<?php

use Illuminate\Database\Seeder;
use Jenssegers\Mongodb\Model as Eloquent;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::collection('restaurant_collection')->insert(['name'=>'prova','loc':['1','2']]);        
    }
}
