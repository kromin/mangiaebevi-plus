<?php 
	require_once base_path('resources/views/email/mail_header.php');
?>
	<div class="testomail">
<p><b><?php echo $mail_data['array_values']['user'];?></b> ha risposto alla tua richiesta di raccomandazione: "<?php echo esc_html($mail_data['array_values']['text']);?>"</p>
<br />

</div>
	<?php 
	require_once base_path('resources/views/email/mail_footer.php');
?>