<?php 
	require_once base_path('resources/views/email/mail_header.php');
?>
	<div class="testomail">
<p>La data della tua prenotazione a <b><?php echo $mail_data['array_values']['restaurant_name'];?></b> è stata modificata dal ristoratore.<br />
Accedi al tuo profilo per accettare o rifiutare il cambio</p>
<br />

<h3>Dettagli</h3>
<ul>
	<li><b>Vecchio orario:</b> <?php echo $mail_data['array_values']['reservation_old_date'];?></li>
	<li><b>Nuovo orario:</b> <?php echo $mail_data['array_values']['reservation_date'];?></li>
</ul>
</div>
	<?php 
	require_once base_path('resources/views/email/mail_footer.php');
?>