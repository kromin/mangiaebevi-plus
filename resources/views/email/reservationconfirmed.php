<?php 
	require_once base_path('resources/views/email/mail_header.php');
?>
	<div class="testomail">
	
		<p>La tua prenotazione a <b><?php echo $mail_data['array_values']['restaurant_name'];?></b> è stata confermata</p>
		<br />

		<h3>Dettagli</h3>
		<ul>
			<li><b>Orario:</b> <?php echo $mail_data['array_values']['reservation_date'];?></li>
		</ul>
	</div>
	<?php 
	require_once base_path('resources/views/email/mail_footer.php');
?>