<?php 
	require_once base_path('resources/views/email/mail_header.php');
?>
	<div class="testomail">
<p>
	Richiesta da: <?php echo $mail_data['name'];?><br />
	<b>Email:</b> <?php echo $mail_data['email'];?><br />
<?php 
	if($mail_data['phone'] != 0):
	?>
		<b>Telefono:</b> <?php echo $mail_data['phone'];?>
	<?php 
	endif;
	
	if($mail_data['risto'] != 0):
	?>
		<b>Ristorante:</b> <?php echo $mail_data['risto'];?>
	<?php 
	endif;
	
	if($mail_data['address'] != 0):
	?>
		<b>Indirizzo:</b> <?php echo $mail_data['address'];?>
	<?php 
	endif;
	?>
</p>

<p><?php echo $mail_data['message'];?></p>
</div>
	<?php 
	require_once base_path('resources/views/email/mail_footer.php');
?>