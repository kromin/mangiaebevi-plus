<?php
	require_once base_path('resources/views/email/mail_header.php');
?>
	<div class="testomail">
<p>Hai ricevuto una nuova prenotazione per il ristorante <b><?php echo $mail_data['restaurant'];?></b></p>
<br />

<h3>Dettagli</h3>
<ul>
	<li><b>Prenotato da:</b> <?php echo $mail_data['detail'][0];?> - <?php echo $mail_data['detail'][3];?></li>
	<li><b>Persone:</b> <?php echo $mail_data['detail'][1];?></li>
	<li><b>Data:</b> <?php echo date('d-m-Y H:i', strtotime($mail_data['detail'][2]));?></li>
</ul>


</div>
	<?php
	require_once base_path('resources/views/email/mail_footer.php');
?>