<?php 
	require_once base_path('resources/views/email/mail_header.php');
?>
<div class="testomail">
<p>Nuovo pagamento</p>
<h3>Utente</h3>
<ul>
	<?php 
	foreach($mail_data['payment']['userObj'] as $r => $i){
		if(is_string($i)):
		?>
		<li><?php echo $r.': '.$i;?></li>
		<?php
		endif;
	}
	?>
</ul>
<?php if(isset($mail_data['restaurant'])) : ?>
<h3>Ristorante</h3>
<p><?php echo $mail_data['restaurant']['name'].' - '.$mail_data['restaurant']['address'];?></p>
<?php endif; ?>
<h3>Prenotazione</h3>
<ul>
	<?php 
	foreach($mail_data['payment']['reservation'] as $r => $i){
		if(is_string($i)):
		?>
		<li><?php echo $r.': '.$i;?></li>
		<?php
		endif;
	}
	?>
</ul>
<h3>Pagamento</h3>
<ul>
	<?php 
	foreach($mail_data['payment']['paypal'] as $r => $i){
		if(is_string($i)):
		?>
		<li><?php echo $r.': '.$i;?></li>
		<?php
		endif;
	}
	?>
</ul>
<br />
</div>
	<?php 
	require_once base_path('resources/views/email/mail_footer.php');
?>