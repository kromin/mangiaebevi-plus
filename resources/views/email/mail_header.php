<html>
<head>
<style>
body{
	max-width: 100%;
}
.testata{
	width: 100%;
	text-align: center;
	background: #00b4cc;
}
.testata img{
	height: 50px;
    padding: 22px;
}
.mail_content{
	text-align: center;
}
ul li{
	list-style: none;
}
ul{
	padding-left: 0;
}
h3{
	font-size: 16px;
}
.mail_footer{
	background: #F7F7F7;
    padding: 20px;
}
.testomail{
    margin: 50px auto;
    padding: 20px;
}
</style>
</head>
<body>

	<div class="testata">
		<img src="<?php echo env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/logo-full.png';?>" alt="Menoo" title="Menoo logo" />
	</div>