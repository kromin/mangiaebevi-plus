<?php
	require_once base_path('resources/views/email/mail_header.php');
?>
	<div class="testomail">
<p><?php echo $mail_data['name'];?> ti ha invitato a registrarti a MangiaeBevi+</p>
<br />
<p>Registrati cliccando sul seguente link:
<br /><a href="<?php echo $mail_data['link'];?>" target="_blank"><?php echo $mail_data['link'];?></a>
</p>

</div>
	<?php
	require_once base_path('resources/views/email/mail_footer.php');
?>