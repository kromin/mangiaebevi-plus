<?php 
	require_once base_path('resources/views/email/mail_header.php');
?>
	<div class="testomail">
<p>La prenotazione al ristorante <b><?php echo esc_html($mail_data['restaurant']);?></b> è stata annullata </p>
<br />

<h3>Dettagli</h3>
<ul>
	<li><b>Prenotato da:</b> <?php echo $mail_data['detail'][0];?> - <?php echo $mail_data['detail'][2];?></li>
	<li><b>Persone:</b> <?php echo $mail_data['detail'][3];?></li>
	<li><b>Data:</b> <?php echo $mail_data['detail'][1];?></li>
</ul>


</div>
	<?php 
	require_once base_path('resources/views/email/mail_footer.php');
?>