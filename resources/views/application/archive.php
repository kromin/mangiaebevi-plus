<?php
require_once '_head.php';
$blue_header = 1;
$search_header = 1;
require_once '_topbar.php';

?>
<div id="searchPage" ng-controller="SearchCtrl" ng-init="SetRestaurants()">
    
    <div class="col-md-9">
        <div id="search_form">
            <form class="form-inline">
                <div class="col-sm-6 col-xs-12 no-pad">
                    <input ng-model="search.query_string" type="text" class="form-control" placeholder="Cosa vuoi cercare?" ng-enter="setNewSearch()" />
                </div>
                <div class="col-sm-6 col-xs-12 small_marginated_btn">
                    <a class="btn btn-primary btn-sm ghost" style="font-size: 13px;" ng-click="setNewSearch()">Cerca</a>
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div id="view_actions">
            <!-- grid or list -->
            <i class="fa fa-th" ng-click="view_type = 'grid'" ng-class="{'active':view_type=='grid'}"></i>
            <i class="fa fa-list" ng-click="view_type = 'list'" ng-class="{'active':view_type=='list'}"></i>
            <a ng-click="openAdvSearch()">Non sei soddisfatto dei risultati, <b>clicca qui</b> per la ricerca avanzata</a>
        </div>      
        <div id="search_results" class="" ng-class="(view_type == 'list') ? 'list_view' : 'grid_view'" style="min-height: 80vh;">
            <?php 
                
                for($r = 0; $r < count($restaurants); $r++):
                    if(isset($restaurants[$r]['city'])):
                    ?>
                    <div class="hidden show_on_load"><!-- open page -->                        
                        <div class="col-lg-3 col-md-4 col-sm-6 restaurant-grid-container" ng-init="
                        pushMapObj(
                        '<?php echo remove_quotes_and_tags($restaurants[$r]['name']);?>',
                        '<?php echo (isset($restaurants[$r]['image'])) ? escape_non_alphanum($restaurants[$r]['image']) : '';?>',
                        '<?php echo remove_quotes_and_tags($restaurants[$r]['address']);?>',
                        '<?php echo floatval($restaurants[$r]['loc']['coordinates'][1]);?>',
                        '<?php echo floatval($restaurants[$r]['loc']['coordinates'][0]);?>',
                        '<?php echo strip_slug($restaurants[$r]['slug']);?>',
                        '<?php echo urlencode($restaurants[$r]['city']);?>',
                        <?php echo intval($result_number);?>)
                        ">
                            <a href="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/<?php echo urlencode($restaurants[$r]['city']);?>/<?php echo strip_slug($restaurants[$r]['slug']);?>/">
                                <div class="restaurant box_images qt">
                                    <?php if(isset($restaurants[$r]['image']) && $restaurants[$r]['image'] != ''): ?>
                                    <div class="img" back-img img="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_STATIC_DOMAIN');?>/restaurant/small/<?php echo escape_non_alphanum($restaurants[$r]['image']);?>.png"></div>
                                    <?php else: ?>
                                    <div class="img" back-img img="/img/search_default.jpg"></div>
                                    <?php endif; ?>
                                    <div class="overlay"></div>
                                    <div class="text">
                                        <h3><?php echo $restaurants[$r]['name'];?></h3>
                                        <?php if(isset($restaurants[$r]['tipoLocale'][0])): ?>
                                        <span class="type">{{tipiLocali['<?php echo $restaurants[$r]['tipoLocale'][0];?>'].name}}</span>
                                        <?php endif; ?>
                                        <span class="address" ng-if="view_type == 'list'"><?php echo $restaurants[$r]['address'];?>
                                        <distance 
                                        ng-bind="returnDistance(
                                        <?php echo floatval($restaurants[$r]['loc']['coordinates'][1]);?>,
                                        <?php echo floatval($restaurants[$r]['loc']['coordinates'][0]);?>)">
                                        </distance></span>
                                    </div>
                                </div>
                            </a>                
                        </div>                        
                    </div> <!-- close page -->
                <?php        
                endif;             
        endfor; 
        
        if(count($restaurants) == 0):
            ?>
            <p class="text-center" style="font-size: 22px;margin-top: 100px;">La ricerca non ha prodotto risultati</p>
            <?php
        endif;
        ?>
        </div>
        <div class="clearfix"></div>
        <div class="pagination hidden show_on_load">
            <ul class="list-unstyled">


                <?php if($current_page > 3):
                ?>
                <li><a href="<?php echo $page_link;?>1">1</a></li>
                <?php if($current_page > 4) echo '<li>...</li>';?>
                <?php
                endif;
                for($start = $current_page - 2; $start < $current_page; $start++):
                    if($start > 0):
                    ?>
                <li><a href="<?php echo $page_link.$start;?>"><?php echo $start;?></a></li>
                <?php
                    endif;
                endfor;
                ?>
                <li class="active"><?php echo $current_page;?></li>
                <?php 
                for($start = $current_page+1; $start < $current_page+3 && $start <= $page_num; $start++):
                    ?>
                <li><a href="<?php echo $page_link.$start;?>"><?php echo $start;?></a></li>
                <?php
                endfor;
                if($current_page <= $page_num - 3):
                ?>
                <?php if($current_page < $page_num - 3) echo '<li>...</li>';?>
                <li><a href="<?php echo $page_link.$page_num;?>"><?php echo $page_num;?></a></li>
                <?php
                endif;
                 ?>
            </ul>
        </div>
    </div>
    <div class="col-md-3 map_container_right hidden-sm hidden-xs">
        <div ng-controller="MapCtrl">
            <div id="google_map"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>

<?php
require_once '_footer.php';
?>