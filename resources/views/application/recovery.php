<?php
require_once '_head.php';
$blue_header = 1;
require_once '_topbar.php';
?>

<div class="white_page_login" ng-controller="RecoveryCtrl">
	<div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
		<div class="form-info">			
			<form name="recoveryForm">
				<h3 class="text-center">Recupera la password per accedere</h3>
				<p class="text-center">Una nuova password verrà inviata al tuo indirizzo email</p>
				<p ng-if="error_recovery" class="text-danger">
					<span ng-if="error_recovery == 1">Indirizzo email non trovato</span>
					<span ng-if="error_recovery == 2">Sembra che fino ad oggi tu sia sempre entrato con i social</span>
				</p>
				<p ng-if="recovery_success" class="text-success">
					Dovresti aver ricevuto una email
				</p>
				<div class="form-group" ng-if="!recovery_success">
					<label for="email" class="sr-only">Email</label>
					<input type="email" ng-model="recovery.email" id="email" class="form-control" placeholder="Email" required/>
				</div>
				<a class="link_btn" href="/registrati/">Non sei registrato?</a>
				<div class="form-group" ng-if="!recovery_success">
					<input type="submit" style="margin: 12px 0;" ng-disabled="recoveryForm.$invalid" ng-click="recovery()" class="btn btn-block btn-primary ghost" value="Recupera" />
				</div>
			</form>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php
require_once '_footer.php';
?>