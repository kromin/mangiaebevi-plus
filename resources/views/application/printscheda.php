<?php // prende menu
?>
<!DOCTYPE html>
<html ng-app="app">
    <head>
        <meta charset="utf-8">
        <meta name=viewport content="width=device-width, initial-scale=1">
        <style></style><!-- debug bar in error without this-->
        <link rel="shortcut icon" href="/img/favicon.ico">
        <meta name="robots" content="noindex">
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/app.css" rel='stylesheet' type='text/css'>
        <base href="/" />                
    </head>    
    <body class="bg-white">
	<div class="standalone_menu">
		<?php // $client ?>
	<div class="text-left" ng-if="current_client != 0">
		<h3>Scheda di <?php echo $client->name . ' ' . $client->surname;?></h3>
		<div class="row">
		<div class="col-sm-12">
			<p><b>Nome: </b><?php echo esc_html($client->name);?></p>
			<p><b>Cognome: </b><?php echo esc_html($client->surname);?></p>
			<p><b>Email: </b><?php echo (isset($client->email)) ? esc_html($client->email) : '';?></p>
			<p><b>Mobile: </b><?php echo (isset($client->phone)) ? esc_html($client->phone) : '';?></p>
			<p><b>Telefono: </b><?php echo (isset($client->phone_)) ? esc_html($client->phone_) : '';?></p>
			<p><b>Data di nascita: </b><?php echo (isset($client->dt)) ? esc_html($client->dt) : '';?></p>
			<p><b>Indirizzo: </b>
			<?php echo (isset($client->address)) ? esc_html($client->address) : '';?>
			<?php echo (isset($client->cap)) ? ' '.esc_html($client->cap) : '';?>
			<?php echo (isset($client->city)) ? ' '.esc_html($client->city) : '';?>
			<?php echo (isset($client->pr)) ? ' ('.esc_html($client->pr).')' : '';?>
			<?php echo (isset($client->state)) ? ' '.esc_html($client->state) : '';?>
			</p>
			<p><b>Lingua: </b><?php echo (isset($client->language)) ? esc_html($client->language) : '';?></p>
			<p><b>Allergie: </b><?php echo (isset($client->allergie)) ? esc_html(implode(", " , $client->allergie)) : '';?></p>
			<p><b>Preferenze: </b><?php echo (isset($client->preferenze)) ? esc_html(implode(", " , $client->preferenze)) : '';?></p>
			<p><b>Allergie: </b><?php echo (isset($client->note)) ? esc_html($client->note) : '';?></p>
			<br />
			<hr />
			<p><b>Prenotazioni: </b><?php echo $infos['reservations'];?></p>
			<p><b>Prenotazioni con preordine: </b><?php echo $infos['preorders'];?></p>
			<p><b>No show: </b><?php echo $infos['discarded_reservations'];?></p>
		</div>
						
		</div>
	</div>
    <script type="text/javascript">
    setTimeout(function(){
    	window.print();
    },3000);
    </script>
        
    </body>
</html>