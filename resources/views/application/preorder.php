<?php
    
require_once '_head.php';
    
require_once '_topbar.php';

?>

<div id="restaurantPage" ng-controller="RestaurantCtrl" ng-init="initPage('<?php echo $restaurant->_id;?>')">
    <?php
    
    require_once 'restaurant/main.php';
    ?>
    <div class="pageContent" ng-class="{'slideDown':show_virtual_tour}">
                        
        <div class="container">
            <div class="col-lg-12">
                <h1 class="page_title">
					<?php echo esc_html($restaurant->name);?>
					<small>
						<?php if(isset($restaurant_infos['tipiLocale'][0])) : ?><span><?php echo esc_html($restaurant_infos['tipiLocale'][0]);?> |</span><?php endif;?>
						<?php if(isset($restaurant_infos['tipiCucina'][0])) : ?><span> <?php echo esc_html($restaurant_infos['tipiCucina'][0]);?> |</span><?php endif;?>
						<?php if($restaurant->district && count($restaurant->district) > 0 && $restaurant->district[0] != '') :?>
						<span> <?php echo esc_html($restaurant->district[0]);?> |</span>
						<?php endif; ?>
						<span><?php echo $restaurant_infos['europrice'];?></span>
						<span class="hint--bottom" aria-label="Raccomandazioni" ng-click="scrollToRec()"> | <i class="mebi-speaking"></i> <span id="update_recommend"><?php echo $restaurant->recommendations;?></span></span>
					</small>
				</h1>
            </div>
        </div>
        <?php require_once 'restaurant/white_top.php'; ?>
        <!-- identifico il cliente -->
        <div class="grey_content" ng-controller="MenuPreOrderCtrl" ng-init="initMenuPreorder('<?php echo $restaurant->_id;?>','<?php echo $reservation->_id;?>')">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <?php require_once 'restaurant/menuorder.php'; ?>
                    </div>


                    <!-- sidebar -->
                    <div class="col-md-4">
                        <!-- prenotazione -->
                        <?php require_once 'restaurant/order.php'; ?>
                        <!-- raccomandazione -->
                    </div>
                </div>
                <div class="clearfix"></div>
                
            </div>
        </div>   

    </div>
    <?php require_once 'restaurant/gallery.php'; ?>
    <?php require_once 'restaurant/menu_gallery.php'; ?>
</div>
<div class="clearfix"></div>

<?php

require_once '_footer.php';
?>