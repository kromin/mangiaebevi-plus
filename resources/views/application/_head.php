<!DOCTYPE html>
<html ng-app="app" ng-controller="ApplicationCtrl">
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5DHPV4T');</script>
        <!-- End Google Tag Manager -->
        <meta charset="utf-8">
        <meta name=viewport content="width=device-width, initial-scale=1">
        <style></style><!-- debug bar in error without this-->
        <link rel="shortcut icon" href="/img/favicon.png">

        <?php


        if(isset($seo)):
        ?>
        <title><?php echo remove_quotes_and_tags($seo[0]);?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta  name="description" content="<?php echo remove_quotes_and_tags($seo[5]);?>" />

        <!-- Schema.org markup for Google+ -->
        <meta  itemprop="name" content="<?php echo remove_quotes_and_tags($seo[0]);?>">
        <meta  itemprop="description" content="<?php echo remove_quotes_and_tags($seo[5]);?>">
        <meta  itemprop="image" content="<?php echo esc_url($seo[2]);?>">

        <!-- Twitter Card data -->
        <meta  name="twitter:card" content="<?php echo esc_url($seo[2]);?>">
        <meta name="twitter:site" content="@publisher_handle">
        <meta  name="twitter:title" content="<?php echo remove_quotes_and_tags($seo[0]);?>">
        <meta  name="twitter:description" content="<?php echo remove_quotes_and_tags($seo[5]);?>">
        <meta name="twitter:creator" content="@author_handle">
        <!-- Twitter summary card with large image must be at least 280x150px -->
        <meta  name="twitter:image:src" content="<?php echo esc_url($seo[3]);?>">

        <!-- Open Graph data -->
        <meta  property="og:title" content="<?php echo remove_quotes_and_tags($seo[0]);?>" />
        <meta  property="og:type" content="<?php echo $seo[4];?>" />
        <meta property="og:url" content="<?php echo env('APP_PROTOCOL').'://'.env('APP_DOMAIN').$seo[1];?>" />
        <meta  property="og:image" content="<?php echo esc_url($seo[3]);?>" />
        <meta  property="og:description" content="<?php echo remove_quotes_and_tags($seo[5]);?>" />
        <meta property="og:site_name" content="MangiaeBevi+" />
        <?php
        endif;

        if(isset($no_index) && $no_index):
        ?>
            <meta name="robots" content="noindex">
        <?php
        endif;

        if(isset($canonical) && $canonical):
        ?>
            <link rel="canonical" href="<?php echo env('APP_PROTOCOL').'://'.env('APP_DOMAIN').$canonical;?>"/>
        <?php
        endif;
        ?>
        <meta name="robots" content="follow" />

        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/app.css" rel='stylesheet' type='text/css'>
        <base href="/" />

    </head>
    <body ng-class="{'bg-white' : bg_w}">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5DHPV4T"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
        <div id="fb-root"></div>
        <script type="text/javascript">

            <?php if(env('APP_ENV') == 'production'): ?>
                (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&appId=<?php echo env('FB_KEY');?>&version=v2.0";
                fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));

                window.twttr = (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0],
                t = window.twttr || {};
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "https://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);
                t._e = [];
                t.ready = function(f) {
                t._e.push(f);
                };
                return t;
                }(document, "script", "twitter-wjs"));

                window.___gcfg = {
                lang: 'it-IT'
                };
            <?php endif; ?>
            (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();

        </script>