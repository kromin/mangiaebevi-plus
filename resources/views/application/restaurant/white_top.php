<div class="pageContentBg">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        
                        <div ng-init="SetRestaurantOrari('<?php echo $restaurant->_id;?>')">
                            <div class="col-sm-6">
                                <?php if($restaurant->address != ''):?>
                                <p><b><i class="fa fa-map-marker"></i></b> 
                                <?php 
                                echo esc_html($restaurant->address);
                                if($restaurant->district && count($restaurant->district) > 0 && $restaurant->district[0] != '') echo ', '.esc_html($restaurant->district[0]); ?>
                                </p>
                                <?php endif; ?>
                                <p>
                                    <span class="text-success" ng-if="isOpenRestaurant()">Aperto</span>
                                    <span class="text-danger" ng-if="!isOpenRestaurant()">Chiuso</span>
                                </p>
                                <p ng-if="fasce_oggi.length > 0">Orari di oggi: 
                                    <span ng-repeat="fascia_odierna in fasce_oggi track by $index">{{fascia_odierna}} </span></p>
                                <a ng-click="viewHours()">vedi tutti gli orari</a>
                            </div>
                            <div class="col-sm-6">
                                <?php 
                                if(isset($restaurant->contact['phone']) && count($restaurant->contact['phone']) > 0) : 
                                    ?>
                                <p><b><i class="fa fa-phone"></i></b>
                                    <?php 
                                        $n = 0; 
                                        foreach($restaurant->contact['phone'] as $phone) : $n++;
                                        if(!is_array($phone)):
                                    ?>
                                        <span> 
                                        <?php echo esc_html($phone);?> 
                                        <?php if($n < count($restaurant->contact['phone'])) : ?>
                                            <i>-</i>
                                        <?php endif; ?> </span>
                                    <?php 
                                        endif;
                                        endforeach; 
                                        ?>
                                </p>
                                <?php endif; 
                        
                                ?>
                                <?php if(isset($restaurant->contact['web']) && $restaurant->contact['web'] != '') : ?>
                                <p>
                                    <b><i class="fa fa-globe"></i></b> <a href="<?php echo (starts_with($restaurant->contact['web'],'htt')) ? esc_html($restaurant->contact['web']) : '//'.esc_html($restaurant->contact['web']);?>" target="_blank" class="web_link">
									<?php echo str_replace("http://","",esc_html($restaurant->contact['web']));?></a>
                                </p>
                                <?php endif; ?>
                                <div class="restaurant-social">
                                    <?php if(isset($restaurant->social[0]['facebook']) && $restaurant->social[0]['facebook'] != '') : ?>
                                    <a target="_blank" 
                                    href="<?php echo esc_url($restaurant->social[0]['facebook']);?>" class="social-page-link">
                                        <i class="fa fa-facebook fb"></i>
                                    </a>
                                    <?php endif; 
                                    if(isset($restaurant->social[1]['twitter']) && $restaurant->social[1]['twitter'] != '') :
                                    ?>
                                    <a target="_blank" 
                                    href="<?php echo esc_url($restaurant->social[1]['twitter']);?>" class="social-page-link">
                                        <i class="fa fa-twitter tw"></i>
                                    </a>
                                    <?php endif; 
                                    if(isset($restaurant->social[2]['pinterest']) && $restaurant->social[2]['pinterest'] != '') :
                                    ?>
                                    <a target="_blank" 
                                    href="<?php echo esc_url($restaurant->social[2]['pinterest']);?>" class="social-page-link">
                                        <i class="fa fa-pinterest pi"></i>
                                    </a>
                                    <?php endif; 
                                    if(isset($restaurant->social[3]['instagram']) && $restaurant->social[3]['instagram'] != '') :
                                    ?>
                                    <a target="_blank" n 
                                    href="<?php echo esc_url($restaurant->social[3]['instagram']);?>" class="social-page-link">
                                        <i class="fa fa-instagram in"></i>
                                    </a>
                                    <?php endif; 
                                    if(isset($restaurant->social[4]['googlePlus']) && $restaurant->social[4]['googlePlus'] != '') :
                                    ?>
                                    <a target="_blank" 
                                    href="<?php echo esc_url($restaurant->social[4]['googlePlus']);?>" class="social-page-link">
                                        <i class="fa fa-google-plus gp"></i>
                                    </a>
                                    <?php endif; 
                                    if(isset($restaurant->social[5]['linkedin']) && $restaurant->social[5]['linkedin'] != '') :
                                    ?>
                                    <a target="_blank" 
                                    href="<?php echo esc_url($restaurant->social[5]['linkedin']);?>" class="social-page-link">
                                        <i class="fa fa-linkedin li"></i>
                                    </a>
                                    <?php endif; 
                                    if(isset($restaurant->social[6]['youtube']) && $restaurant->social[6]['youtube'] != '') :
                                    ?>
                                    <a target="_blank" 
                                    href="<?php echo esc_url($restaurant->social[6]['youtube']);?>" class="social-page-link">
                                        <i class="fa fa-youtube yt"></i>
                                    </a>
                                    <?php endif; 
                                    if(isset($restaurant->social[7]['tripadvisor']) && $restaurant->social[7]['tripadvisor'] != '') :
                                    ?>
                                    <a target="_blank" ng-if="current_restaurant.social[7].tripadvisor && current_restaurant.social[7].tripadvisor != ''" 
                                    href="<?php echo esc_url($restaurant->social[7]['tripadvisor']);?>" class="social-page-link">
                                        <i class="fa fa-tripadvisor tr"></i>
                                    </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="reccomendation" <?php /*set-class-when-at-top="active"*/?>>

                            <div class="form-content">

                                <div class="cont">
                                    <div class="top_community_box">
                                        <div class="col-xs-4 text-center" ng-click="favourite()">
                                            <i class="fa fa-heart pad_t" ng-class="(current_user.user.favourite && current_user.user.favourite.indexOf(current_restaurant._id) != -1) ? 'fa-heart text-danger' : 'fa-heart-o'"></i>
                                            Preferiti
                                        </div>
                                        <div class="col-xs-4 text-center center_block_community" ng-click="wishlist()">
                                            <i class="mebi-bookmark" 
                                        ng-class="(current_user.user.wishlist && current_user.user.wishlist.length > 0 && current_user.user.wishlist.indexOf(current_restaurant._id) != -1) ? 'text-danger' : ''"></i>
                                            Da provare
                                        </div>
                                        <div class="col-xs-4 text-center" ng-click="recommendRestaurant()">
                                            <i class="mebi-speaking"></i>
                                            Raccomanda                                        
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <?php /*
                                    <span class="hint--bottom" aria-label="Aggiungi/rimuovi da ristoranti da provare">    
                                        <i class="mebi-bookmark" 
                                        ng-class="(current_user.user.wishlist && current_user.user.wishlist.indexOf(current_restaurant._id) != -1) ? 'text-success' : 'text-danger'" 
                                        ng-click="wishlist()"></i>
                                    </span>
                                    <span class="hint--bottom" aria-label="Raccomanda">    
                                        <i class="mebi-speaking"
                                        ng-click="recommendRestaurant()"
                                        ></i>
                                    </span>
                                    <a class="btn btn-primary ghost dark btn-block" ng-click="favourite()">
                                        <i class="fa fa-heart" ng-class="(current_user.user.favourite && current_user.user.favourite.indexOf(current_restaurant._id) != -1) ? 'fa-heart text-danger' : 'fa-heart-o'"></i> Preferiti
                                    </a>*/?>
                                    <h3>Condividi con un amico</h3>
                                    <div class="raccomanda_buttons">
                                        <a class="" ng-click="mailReccomendOpenDialog()">Email</a>
                                        <a class="" 
                                        ng-click="reccommend('facebook_share','<?php echo $share_url;?>','<?php echo remove_quotes_and_tags( $restaurant->name);?>','<?php echo $seo[2];?>')">Facebook</a>
                                        <a class="list-container" ng-click="open_drop_menu = !open_drop_menu">Altro...
											<ul class="list-unstyled" ng-class="{'open': open_drop_menu}">
												<li ng-click="reccommend('twitter','<?php echo $share_url;?>','<?php echo remove_quotes_and_tags( $restaurant->name);?>','<?php echo $seo[2];?>')">Twitter</li>
												<li ng-click="reccommend('google_plus','<?php echo $share_url;?>','<?php echo remove_quotes_and_tags( $restaurant->name);?>','<?php echo $seo[2];?>')">Google plus</li>
												<li ng-click="reccommend('linkedin','<?php echo $share_url;?>','<?php echo remove_quotes_and_tags( $restaurant->name);?>','<?php echo $seo[2];?>')">Linkedin</li>
												<li ng-click="reccommend('facebook_send','<?php echo $share_url;?>','<?php echo remove_quotes_and_tags( $restaurant->name);?>','<?php echo $seo[2];?>')">Messenger</li>
											</ul>
										</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="clearfix"></div>
                
            </div>
        </div>