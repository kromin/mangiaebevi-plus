<?php if($restaurant->recommendations > 0) : ?>
<div>
    <div class="restaurant_recommendations" ng-init="loadRestaurantRecommendations('<?php echo $restaurant->_id;?>',<?php echo $restaurant->recommendations;?>)" id="recContainer">
        <h3>Raccomandazioni</h3>

        <ul class="list-unstyled dashboard_list">
            <li ng-repeat="recommendation in restaurant_rec track by $index">
                <div class="dashboard_following">
                    <div class="element_header">
                        <div class="pull-left">
                            <img class="profile_image" 
                            ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/users/square/{{recommendation.user_obj.avatar}}.png" alt="" title="" 
                            ng-if="recommendation.user_obj.avatar && recommendation.user_obj.avatar != ''" />
                            <img class="profile_image" 
                            ng-src="/img/default-avatar.png" alt="" title="" 
                            ng-if="!recommendation.user_obj.avatar || recommendation.user_obj.avatar == ''" />
                        </div>
                        <div class="col-sm-10 col-xs-9">
                            <span class="text-primary"><a href="/u/{{recommendation.user_obj._id}}/" class="text-primary">{{recommendation.user_obj.name}} {{recommendation.user_obj.surname}}</a></span><br />
                            <span class="date">{{recommendation.updated_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
                            <div class="plate_comment">
                                <p>{{recommendation.text}}</p>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>                                        
                </div>                                                  
                <div class="clearfix"></div>
            </li>
        </ul> 
        <p ng-if="restaurant_rec.length == 0">Non ci sono raccomandazioni generiche del ristorante</p>  
        <p class="text-center">
            <a class="" ng-click="loadRecommendation()" 
            ng-if="!stop_load_rec_pagination"
                role="button">Carica altre raccomandazioni</a>   
        </p>  
        
    </div>
    <hr />
</div>
<?php endif; ?>