<div id="menoo" class="hidden">
	<!-- blocco menu -->
	<div class="menu_container" id="menuContainer" ng-if="menu_restaurant.menu.menus && menu_restaurant.menu.menus.length > 0">
        <p ng-if="!canPreOrder">Il ristorante non permette le pre ordinazioni</p>
        <ul class="list-unstyled menu-titles">
            <li ng-repeat="menu in menu_restaurant.menu.menus track by $index" ng-click="selectMenu($index)" ng-if="menu.categorie.length > 0 && menu.orderready == 1" ng-class="{'active':currentMenu==$index}">
                {{menu.name}}
            </li>
        </ul>

        <div class="standalone_menu" ng-if="((menu_restaurant.menu.menus.length)-1) >= currentMenu && menu_restaurant.menu.menus[currentMenu].orderready == 1">
            <div class="menu style_one">
                <div class="menu_title">
                    <h3>{{menu_restaurant.menu.menus[currentMenu].name}}</h3>
                </div>
                <div class="pre_text">
                    <p class="wrap_text">{{menu_restaurant.menu.menus[currentMenu].pre_text}}</p>
                </div>
                <div class="menu_price" ng-if="menu_restaurant.menu.menus[currentMenu].menu_type == 1">
                    <p class="menu_price_text">Menu a prezzo fisso</p>
                    <p class="menu_price_price">€ {{menu_restaurant.menu.menus[currentMenu].menu_price}}</p>
                    <p><i class="fa fa-plus text-primary" ng-click="addMenuFisso(menu_restaurant.menu.menus[currentMenu])"></i></p>
                </div>
                <div class="menu_category" ng-repeat="(i_cat,cat) in menu_restaurant.menu.menus[currentMenu].categorie">
                    <div class="clearfix"></div>
                    <div class="menu_category_title">
                        <h4>{{cat.name}}</h4>
                    </div>
                    <div class="menu_plate"
                        ng-class-even="'even'"
                        ng-repeat="(i_piatto, piatto) in menu_restaurant.menu.menus[currentMenu].categorie[i_cat].piatti">
                        <div class="contain_plate">
                            <div class="plate_image" ng-if="piatto.image && piatto.image != ''">
                                <img ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/menu/small/{{piatto.image}}.png" alt="plate image" />
                                &nbsp;
                            </div>
                            <div class="plate_content" ng-class="{'with_price' : menu_restaurant.menu.menus[currentMenu].menu_type == 0, 'with_image' : piatto.image && piatto.image != ''}">
                                <h5>{{piatto.name}}</h5>
                                <p>{{piatto.description}}</p>
                                <p class="plate_ingredienti" ng-if="piatto.ingredienti.length > 0"><b>Ingredienti: </b> <span ng-repeat="ing in piatto.ingredienti">{{ing}}{{$last ? '' : ', '}}</span></p>
                                <p class="plate_allergeni" ng-if="piatto.allergeni.length > 0"><b>Allergeni: </b> <span ng-repeat="all in piatto.allergeni">{{all}}{{$last ? '' : ', '}}</span></p>                                
                    			<div class="plate_options" ng-repeat="(i_opt_cat, opt_cat) in piatto.options">
                                    <p><b>{{opt_cat.name}}</b></p>
                                    <ul class="option_style">
                                        <li ng-repeat="(i_opzione,opzioni) in opt_cat.options">
                                            <span class="option_name">{{opzioni.name}}</span>
                                            <span class="option_addition" ng-if="opzioni.no_variation == 0 && opzioni.variation == 0">
                                                + {{opzioni.price}} €
                                            </span>
                                            <span class="option_addition" ng-if="opzioni.no_variation == 0 && opzioni.variation == 1">
                                                - {{opzioni.price}} €
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="plate_price" ng-if="menu_restaurant.menu.menus[currentMenu].menu_type == 0">
                                <p><span class="euro">€</span> <span class="price">{{piatto.price}}</span></p>
                                <p><i class="fa fa-plus text-primary" ng-click="addPlateToOrder(i_cat, i_piatto)"></i></p>
                            </div>
                        </div>                  
                    </div>
                    <div class="clearfix"></div>
                    <hr class="row_separator" />
                </div>
                <div class="post_text">
                    <p class="wrap_text" ng-bind="menu_restaurant.menu.menus[currentMenu].post_text"></p>
                </div>
            </div>
        </div>
	    
	    <p class="text-primary" style="line-height: 50px;">Menu Powered By 
	        <img class="logo_img" ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/logo_blue.png';?>" 
	        alt="Menoo" title="" style="height: 20px; width: auto; display: inline-block;" />
	    </p>
	    <!--</div>-->
	</div>

</div>