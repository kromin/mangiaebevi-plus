<?php if(count($related) > 0) : ?>
<div class="correlati">
    <h3>Potresti essere interessato anche a:</h3>
    <div class="grid_view related">
        <?php foreach($related as $rel): ?>
        <div class="col-sm-4 restaurant-grid-container" ng-init="
                        pushMapObj(
                        '<?php echo remove_quotes_and_tags($rel['name']);?>',
                        '<?php echo (isset($rel['image'])) ? escape_non_alphanum($rel['image']) : '';?>',
                        '<?php echo remove_quotes_and_tags($rel['address']);?>',
                        '<?php echo floatval($rel['loc']['coordinates'][1]);?>',
                        '<?php echo floatval($rel['loc']['coordinates'][0]);?>',
                        '<?php echo $rel['slug'];?>',
                        '<?php echo urlencode($rel['city']);?>')
                        ">
            <a href="/<?php echo $rel['city'];?>/<?php echo $rel['slug'];?>/">
                <div class="restaurant" back-img img="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/restaurant/small/<?php echo escape_non_alphanum($rel['image']);?>.png">
                    <div class="overlay"></div>
                    <div class="text">
                        <h3><?php echo $rel['name'];?></h3>
                        <span class="type"><?php echo $rel['tipoLocale'];?></span>
                        <?php /*<span class="address"><?php echo $rel['address'];?></span>*/?>
                    </div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>