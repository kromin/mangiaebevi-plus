<?php if($restaurant->images && count($restaurant->images) > 0) : 
// mostrare foto singola
?>
<div class="image_gallery" 
    ng-class="{'active':gallery_opened}" 
    ng-init="initGalleryImages(<?php echo str_replace("\"", "'", json_encode($restaurant->images));?>)">
        <div class="overlay"></div>
        <div class="image_active_container">
            <img galleryimageonload="active" ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/restaurant/big/{{gallery[current_image_index]}}.png" alt="" title="" />
        </div>
        <div class="controls">
            <a ng-click="closeGallery()" class="close_gallery_icon"><i class="mebi-close"></i></a>
            <a ng-click="prevImage()" class="left"><i class="mebi-left-open-big"></i></a>
            <a ng-click="nextImage()" class="right"><i class="mebi-right-open-big"></i></a>
        </div>
    </div>
<?php endif; ?>