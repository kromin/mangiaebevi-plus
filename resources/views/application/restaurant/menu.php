<?php if( isset($restaurant->menu['menus']) && is_array($restaurant->menu['menus']) && !empty($restaurant->menu['menus'])) : ?>
    
    <div class="menu_container" id="menuContainer">
        <ul class="list-unstyled menu-titles">
            <?php $n = 0; foreach ($restaurant->menu['menus'] as $menu) : ?>
            <li ng-click="selectMenu(<?php echo $n;?>)" ng-if="<?php echo count($menu['categorie']);?> > 0" ng-class="{'active':currentMenu==<?php echo $n;?>}">
                <?php echo esc_html($menu['name']);?>
            </li>
            <?php $n++; endforeach; ?>
        </ul>

        

        <?php 
        $m_num = 0;
        foreach ($restaurant->menu['menus'] as $menu){
            $visual = 0; 
            foreach($menu['categorie'] as $cat) {
                foreach ($cat['piatti'] as $plate){ 
                    if($plate['image'] != '') {
                        $visual = 1;
                        break;
                    }
                }
            }
            
            ?>
            <div class="standalone_menu" ng-if="currentMenu==<?php echo $m_num;?>" <?php if(isset($menu['orderready']) && $menu['orderready'] == 1) echo "ng-init='canPreOrder(1,\"".$restaurant->slug."\")'";?>>
            <?php
            if($visual == 1):
                $style = 2;
                // mostra menu visuale
                ?>
            <div class="menu style_tree">            
            <?php
            else:
                $style = 0;
                // mostra menu testuale
                ?>
            <div class="menu style_four">
            <?php
            endif;
            ?>
                    <div class="menu_title">
                        <h3><?php echo esc_html( $menu['name'] );?></h3>
                    </div>
                    <?php if(isset($menu['pre_text'])): ?>
                    <div class="pre_text">
                        <p class="text-left"><?php echo return_simple_text_br( $menu['pre_text'] );?></p>
                    </div>
                    <?php endif; 
                    if(!isset($menu['menu_type'])) $menu['menu_type'] = 0;
                    ?>
                    <?php if ( $menu['menu_type'] == 1 ) : ?>
                    <div class="menu_price">
                        <p class="menu_price_text">Menu a prezzo fisso</p>
                        <p class="menu_price_price">€ <?php echo format_price($menu['menu_price']);?></p>
                    </div>
                    <?php endif;            
                    foreach ( $menu['categorie'] as $categoria ) :
                    ?>
                    <div class="menu_category <?php if(isset($categoria['line_break']) && $categoria['line_break'] == 1) echo 'page-break';?>">
                        <div class="clearfix"></div>
                        <div class="menu_category_title">
                            <h4><?php echo esc_html($categoria['name']); ?></h4>
                        </div>
                        <?php
                        $m = 0;
                        foreach ( $categoria['piatti'] as $piatto ) :
                            ?>
                        <div class="menu_plate third_half">
                            <div class="contain_plate">
                                <?php if(isset($piatto['image']) && $piatto['image'] != ''): ?>
                                <div class="plate_image">
                                    <?php if($style == 0 || $style == 1): 
                                    $w = ($style == 0) ? 70 : 110;
                                        // i pdf vogliono i background image per i cerchi
                                        ?>
                                        <img ng-click="openMenuGallery('<?php echo escape_non_alphanum($piatto['image']);?>')" src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/menu/small/<?php echo $piatto['image'];?>.png" class="plate-image" style="border-radius: 50%; width: <?php echo $w;?>px; height: <?php echo $w;?>px; margin: auto;" />
                                    <?php else: ?>
                                    <img ng-click="openMenuGallery('<?php echo escape_non_alphanum($piatto['image']);?>')" src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/menu/small/<?php echo escape_non_alphanum($piatto['image']);?>.png" alt="plate image" />
                                    <?php endif; ?>
                                </div>
                                <?php endif; ?>
                                <div class="plate_content<?php if($menu['menu_type'] == 0) echo ' with_price'; if(isset($piatto['image']) && $piatto['image'] != '') echo ' with_image';?>" <?php if($style == 0 && $menu['menu_type']==0 && isset($piatto['image']) && $piatto['image'] != '') echo 'style="width: 55%; overflow: hidden;"';?><?php if($style == 3 && $menu['menu_type']==0) echo 'style="width: 78%; overflow: hidden;"';?>>
                                    <h5><a href="/piatto/<?php echo $piatto['id'];?>/"><?php echo esc_html($piatto['name']);?></a></h5>
                                    <p><?php if(isset($piatto['description'])) echo return_simple_text_br($piatto['description']);?></p>
                                    <?php if(isset($piatto['ingredienti']) && count($piatto['ingredienti']) > 0): ?>
                                    <p class="plate_ingredienti"><b>Ingredienti: </b> <?php echo esc_html( implode(", ",$piatto['ingredienti']) );?></p>
                                    <?php endif; 
                                    if(isset($piatto['allergeni']) && count($piatto['allergeni']) > 0): ?>
                                    <p class="plate_allergeni"><b>Allergeni: </b> <?php echo esc_html(implode(", ",$piatto['allergeni']));?></p>
                                    <?php endif; ?>
                                    <?php 
                                    if(isset($piatto['options'])) :
                                        foreach ($piatto['options'] as $opzione_cat) :
                                        ?>
                                        <div class="plate_options">
                                            <p><b><?php echo esc_html($opzione_cat['name']);?></b></p>
                                            <?php if(isset($opzione_cat['options']) && count($opzione_cat['options']) > 0): ?>
                                            <ul class="option_style">
                                                <?php foreach($opzione_cat['options'] as $opzione): ?>
                                                <li>
                                                    <span class="option_name"><?php echo esc_html($opzione['name']); ?></span>
                                                    <?php if($opzione['no_variation'] == 0): ?>
                                                    <span class="option_addition">
                                                        <?php echo ($opzione['variation'] == 0) ? '+': '-';?> <?php echo format_price($opzione['price']); ?> €
                                                    </span>
                                                    <?php endif; ?>
                                                </li>
                                                <?php endforeach; ?>
                                            </ul>
                                            <?php endif; ?>
                                        </div>
                                        <?php endforeach;
                                    endif;
                                    ?>
                                </div>
                                <div class="clearfix"></div>
                                <?php if($menu['menu_type'] == 0 && $piatto['price'] > 0) : ?>
                                <div class="plate_price text-primary">
                                    <p><span class="euro">€</span> <span class="price"><?php echo format_price($piatto['price']);?></span></p>
                                </div>
                                <?php endif; ?>
                            </div>                                  
                        </div>
                        <?php 
                        $m++;
                        if(($m %3) == 0) echo '<div class="clearfix"></div>';?> 
                        <?php ; 
                        endforeach; ?>
                        <div class="clearfix"></div>
                        <hr class="row_separator" />                
                    </div>
                    <?php endforeach; ?>
                    <?php if(isset($menu['post_text'])): ?>
                    <div class="post_text">
                        <p class="text-left"><?php echo return_simple_text_br($menu['post_text']);?></p>
                    </div>
                    <?php endif; ?>
                </div>
            </div><!-- standalone -->
            <?php 
            /*if($visual == 1):
            ?>
                <ul class="list-unstyled menu_tipe_selection_ul" ng-if="currentMenu==<?php echo $m;?>">
                    <li ng-click="setMenuType(0)" ng-class="{'active':menu_type==0}"><i class="fa fa fa-list"></i> testuale</li>
                    <li ng-click="setMenuType(1)" ng-class="{'active':menu_type==1}"><i class="fa fa fa-picture-o"></i> visuale</li>
                </ul>
            <?php 
            endif;*/
            $m_num++;
        }
        
        /*
        $m = 0; 
        foreach ($restaurant->menu['menus'] as $menu) : ?>

            <?php if(isset($menu['menu_type']) && $menu['menu_type'] == 1) : ?>

            <div class="fixed_price_menu" ng-if="currentMenu==<?php echo $m;?>">
                <div class="fixed_price_container">
                    <div class="col-xs-4"><label>Menu a prezzo fisso</label></div>
                    <div class="col-xs-8 price_content"><?php echo $menu['menu_price'];?> €</div>
                </div>
                <div class="clearfix"></div>
            </div>
            <?php endif; ?>


            <!-- text menu -->
            <?php 
            $n = 0; 
            foreach($menu['categorie'] as $categoria) : 
            $cl = ($n+1 == count($menu['categorie'])) ? ' no-border-bottom' : '';
            ?>        
                <div class="menu category<?php echo $cl;?>" ng-if="menu_type==0 && currentMenu==<?php echo $m;?>">
                    <h4 class="col-sm-12 categoria-name"><?php echo $categoria['name']?></h4>
                    <?php 
                    $p = 0; 
                    foreach($categoria['piatti'] as $piatto) : ?>
                    
                        <div class="col-sm-6 single_plate">
                            <div class="left">
                                <h5><a href="/piatto/<?php echo $piatto['id'];?>/"><?php echo $piatto['name'];?></a></h5>
                                <?php if(isset($piatto['ingredienti']) && count($piatto['ingredienti']) > 0) : ?>
                                <p class="ingredients">
                                    <span><b>Ingredienti:</b> </span> 
                                    <?php foreach($piatto['ingredienti'] as $i) : ?>
                                    <i><?php echo $i;?> </i> 
                                    <?php endforeach; ?>
                                </p>
                                <?php 
                                endif;
                                if(isset($piatto['allergeni']) && count($piatto['allergeni']) > 0) :
                                ?>
                                <p class="ingredients" >
                                    <span><b>Allergeni:</b></span>
                                    <?php foreach($piatto['allergeni'] as $a) : ?>
                                     <i><?php echo $a;?> </i> 
                                    <?php endforeach; ?>
                                </p>
                                <?php endif; ?>
                                <p class="desc"><?php echo $piatto['description'];?></p>
                                
                            </div>
                            <?php if(!isset($menu['menu_type']) || $menu['menu_type'] != 1): ?>
                            <div class="right">
                                <p class="price pull-right">{{<?php echo $piatto['price'];?> | number : 2}} €</p>
                            </div>
                            <?php endif; ?>
                            <div class="clearfix"></div>
                        </div>
                    <?php 
                    if($p % 2 != 0 && $p > 0 || $p == count($categoria['piatti'])-1) echo '<div class="clearfix"></div>';
                    $p++;
                    endforeach; ?>
                </div>
                <div class="clearfix"></div>
            <?php 
            $n++;
            endforeach; 
            if(isset($menu['asterischi']) && count($menu['asterischi'] > 0)):
                ?>
            <div ng-if="menu_type==0 && currentMenu==<?php echo $m;?>">
                <?php
                foreach ($menu['asterischi'] as $asterisco) {
                    echo '<p>'.$asterisco.'</p>';
                }
            ?>
            </div>
            <?php
            endif;
        $m++;
        endforeach; ?>
        <!-- visual menu -->
         <?php 
        $m = 0; 
        foreach ($restaurant->menu['menus'] as $menu) : 
            $n = 0; 
            foreach($menu['categorie'] as $categoria) : 
            $cl = ($n+1 == count($menu['categorie'])) ? ' no-border-bottom' : '';
            ?>  
            <div class="visual_menu category<?php echo $cl;?>" ng-if="menu_type==1 && currentMenu==<?php echo $m;?>">
                <h4 class="col-sm-12 categoria-name"><?php echo $categoria['name']?></h4>
                <div class="row">
                    <?php 
                    $p = 0; 
                    foreach($categoria['piatti'] as $piatto) : 
                        ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 small_pad">
                        <!--categoria.piatti[$index]-->
                        <div class="single_plate">
                            <div class="plate_image">
                                <?php if(isset($piatto['image']) && $piatto['image'] != '') : ?>
                                <img ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/menu/small/<?php echo $piatto['image'];?>.png" 
                                alt="" title="" />
                                <?php else: ?>
                                <img ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_DOMAIN');?>/img/no_plate.png" 
                                alt="" title="" />
                                <?php endif; ?>
                            </div>
                            <div class="text">
                                <h5><a href="/piatto/<?php echo $piatto['id'];?>/"><?php echo $piatto['name'];?></a></h5>
                                <?php if(!isset($menu['menu_type']) || $menu['menu_type'] != 1): ?>
                                    <div class="price">
                                        {{<?php echo $piatto['price'];?> | number : 2}} €                                
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                        <?php 
                        
                    $p++;
                    endforeach;
                    ?>
                </div>
            </div>
            <?php 
                $n++;                
            endforeach;   
            if(isset($menu['asterischi']) && count($menu['asterischi'] > 0)):
                ?>
            <div ng-if="menu_type==1 && currentMenu==<?php echo $m;?>">
                <?php
                foreach ($menu['asterischi'] as $asterisco) {
                    echo '<p>'.$asterisco.'</p>';
                }
            ?>
            </div>
            <?php
            endif;
        $m++;
        endforeach; 
        // old menu
        */
        ?>

    </div>
    
<?php endif; ?>