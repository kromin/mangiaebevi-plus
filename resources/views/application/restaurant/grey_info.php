<?php if(   (isset($restaurant->menu['menus']) && count($restaurant->menu['menus']) > 0)
            || ($restaurant->virtualTour && $restaurant->virtualTour != '')
            || ($restaurant->video && $restaurant->video != '')
            || ($restaurant->images && is_array($restaurant->images) && count($restaurant->images) > 0))
: ?>
<div class="row linking_btn">
    <?php if(isset($restaurant->menu['menus']) && count($restaurant->menu['menus']) > 0) : ?><a class="btn btn-primary ghost dark" ng-click="scrollToMenu()">Menu</a><?php endif; ?>
    <?php if($restaurant->virtualTour && $restaurant->virtualTour != '') : ?><a class="btn btn-primary ghost dark" ng-click="showTour()">Tour</a><?php endif; ?>
    <?php if($restaurant->images && is_array($restaurant->images) && count($restaurant->images) > 0) : ?><a class="btn btn-primary ghost dark" ng-click="openGallery(0)">Foto</a><?php endif; ?>
    <?php if($restaurant->video && $restaurant->video != '') : ?><a class="btn btn-primary ghost dark" ng-click="goToVideo(0)">Video</a><?php endif; ?>
</div>
<?php
endif;
?>

<!-- se aperto mostra -->
<?php if($restaurant->description && $restaurant->description != ''): ?>
<div class="row restaurant_element">
    <div class="col-sm-2">
        <p class="big_text"><b>Descrizione:</b></p>
    </div>
    <div class="col-sm-10">
        <div class="col-sm-12">
            <div class="big_text description pre_wrap" ng-class="{'open':description_opened}">
                <?php echo return_simple_text_br($restaurant->description); ?>
            </div>
            <a ng-if="!description_opened" class="open_description" ng-click="open_description()">Leggi di più <i class="fa fa-chevron-down"></i></a>
            <a style="background: none; margin-top: 20px;" ng-if="description_opened" class="open_description" ng-click="open_description()">Leggi di meno <i class="fa fa-chevron-up"></i></a>
        </div>
    </div>
</div>
<?php endif; ?>


<!-- tipologia -->
<?php if(count($restaurant_infos['tipiLocale']) > 0) : ?>
<div class="row restaurant_element">
    <div class="col-sm-2">
        <p class="big_text left_descriptor"><b>Tipologia:</b></p>
    </div>
    <div class="col-sm-10">
        <!--<div class="col-md-3 col-sm-6 col-xs-12" ng-if="current_restaurant.tipoLocale.length > 0">-->
        <ul class="list-unstyled">
            <?php foreach($restaurant_infos['tipiLocale'] as $locale): ?>
            <li class="col-md-3"><?php echo esc_html($locale);?></li>
            <?php endforeach;?>
        </ul>

    </div>
</div>
<?php endif; ?>

<!-- cucina -->
<?php if(count($restaurant_infos['tipiCucina']) > 0 || count($restaurant_infos['regioniCucina']) > 0) : ?>
<div class="row restaurant_element" >
    <div class="col-sm-2">
        <p class="big_text left_descriptor"><b>Cucina:</b></p>
    </div>
    <div class="col-sm-10">
        <!--<div class="col-md-3 col-sm-6 col-xs-12" ng-if="current_restaurant.tipoCucina.length > 0">-->
        <ul class="list-unstyled">
            <!--<li class="list-title">Tipo</li>-->
            <?php foreach($restaurant_infos['tipiCucina'] as $cucina): ?>
            <li class="col-md-3"><?php echo esc_html($cucina);?></li>
            <?php endforeach;?>
            <?php foreach($restaurant_infos['regioniCucina'] as $regione): ?>
            <li class="col-md-3"><?php echo esc_html($regione);?></li>
            <?php endforeach;?>
        </ul>

    </div>
</div>
<?php endif; ?>
<!-- staff -->
<?php

if( (isset($restaurant->staff['chef']) && is_array($restaurant->staff['chef']) && !empty($restaurant->staff['chef']))
    || (isset($restaurant->staff['sommelier']) && is_array($restaurant->staff['sommelier']) && !empty($restaurant->staff['sommelier']) > 0)
    || (isset($restaurant->staff['barman']) && is_array($restaurant->staff['barman']) && !empty($restaurant->staff['barman']))
    || (isset($restaurant->staff['maitre']) && is_array($restaurant->staff['maitre']) && !empty($restaurant->staff['maitre']))):
    ?>
<div class="row restaurant_element">
    <div class="col-sm-2">
        <p class="big_text left_descriptor"><b>Staff:</b></p>
    </div>
    <div class="col-sm-10">
        <div class="row">
            <?php if(isset($restaurant->staff['chef']) && is_array($restaurant->staff['chef']) && !empty($restaurant->staff['chef'])) : ?>
            <div class="col-md-3 col-sm-6">
                <ul class="list-unstyled">
                    <li class="padded"><b>CHEF</b></li>
                    <?php foreach($restaurant->staff['chef'] as $chef): ?>
                    <li class="padded"><?php echo esc_html($chef);?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>
            <?php if(isset($restaurant->staff['sommelier']) && is_array($restaurant->staff['sommelier']) && !empty($restaurant->staff['sommelier'])) : ?>
            <div class="col-md-3 col-sm-6">
                <ul class="list-unstyled">
                    <li class="padded"><b>SOMMELIER</b></li>
                    <?php foreach($restaurant->staff['sommelier'] as $sommelier): ?>
                    <li class="padded"><?php echo esc_html($sommelier);?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>
            <?php if(isset($restaurant->staff['barman']) && is_array($restaurant->staff['barman']) && !empty($restaurant->staff['barman'])) : ?>
            <div class="col-md-3 col-sm-6">
                <ul class="list-unstyled">
                    <li class="padded"><b>BARMAN</b></li>
                    <?php foreach($restaurant->staff['barman'] as $barman): ?>
                    <li class="padded"><?php echo esc_html($barman);?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>
            <?php if(isset($restaurant->staff['maitre']) && is_array($restaurant->staff['maitre']) && !empty($restaurant->staff['maitre'])) : ?>
            <div class="col-md-3 col-sm-6">
                <ul class="list-unstyled">
                    <li class="padded"><b>MAITRE</b></li>
                    <?php foreach($restaurant->staff['maitre'] as $maitre): ?>
                    <li class="padded"><?php echo esc_html($maitre);?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>
        </div>

    </div>
</div>
<?php endif; ?>


<!-- servizi -->
<?php if(count($restaurant_infos['services']) > 0): ?>
<div class="row restaurant_element">
    <div class="col-sm-2">
        <p class="big_text left_descriptor"><b>Servizi:</b></p>
    </div>
    <div class="col-sm-10">
        <?php
        $n = 0;
        foreach($restaurant_infos['services'] as $service): ?>
        <div class="col-lg-3 col-md-3 col-sm-6 single_service centered_icon_container">
            <i class="<?php echo $restaurant_infos['servicesicons'][$n];?> <?php if($restaurant_infos['servicesicons'][$n] != 'fa fa-beer') echo 'big'; ?>"></i>
            <span><?php echo esc_html($service);?></span>
        </div>
        <?php
        if(($n+1) % 4 == 0) :
            ?>
        <div class="clearfix"></div>
        <?php
        endif;
        $n++;
        endforeach;
        ?>
    </div>
</div>
<?php endif; ?>

<?php if(count($restaurant_infos['carte']) > 0): ?>
<div class="row restaurant_element">
    <div class="col-sm-2">
        <p class="big_text left_descriptor"><b>Carte accettate:</b></p>
    </div>
    <div class="col-sm-10 cards">
        <ul class="list-unstyled">
            <?php
            $n = 0;
            foreach($restaurant_infos['carte'] as $carta): ?>
            <li class="col-md-3 col-sm-6 single_card centered_icon_container">
                <i class="<?php echo $restaurant_infos['carteicons'][$n];?>"></i>
                <span><?php echo esc_html($carta);?></span>
            </li>
            <?php
            $n++;
            endforeach;
            ?>
        </ul>
    </div>
</div>
<?php endif; ?>