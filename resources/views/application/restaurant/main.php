<div class="mainContainer" ng-class="{'active':show_virtual_tour}">
    <?php if($restaurant->virtualTour && $restaurant->virtualTour != ''): ?>
    <div class="virtualTourContainer">
        <iframe width="100%" height="600" src="<?php echo $restaurant->virtualTour;?>" allowfullscreen="" frameborder="0"></iframe>
        <div class="close-tour" ng-if="show_virtual_tour">
            <a class="btn btn-primary ghost dark close-tour-btn" ng-click="showTour()">Chiudi</a>
        </div>
    </div>
    <?php 
    elseif($restaurant->image && $restaurant->image != '') :
        ?>
    <div class="imageContainer" back-img img="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/restaurant/big/<?php echo escape_non_alphanum($restaurant->image);?>.png">
    </div>
    <?php 
    else:
        ?>
    <div class="imageContainer" style="background-image: url(/img/home/home_1.jpg)">
    </div>
    <?php 
    endif;
    ?>
    <div class="overlay"></div>
    <?php 
    if(!$restaurant->user || $restaurant->user == ''): ?>
    <div class="claim_block">
        <div class="container">
            <div class="col-sm-12">
                <h3>Sei il titolare di questa attività? <a ng-click="claim()"><span class="text-warning">Reclama</span> la tua pagina</a></h3>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>