<?php if(($restaurant->user && $restaurant->user != '') || (isset($restaurant->contact['email']) && !empty($restaurant->contact['email'])) ) : ?>
<div class="reservation-form show_on_load hidden" <?php /*set-class-when-at-top="active"*/?> ng-init="takeAvaibleFasce(1,'<?php echo $restaurant->_id;?>')">
    <div class="reservation-header">
        Prenota un tavolo
    </div>

    <div class="form-content" ng-if="!reservation_as_to_add_phone">

        <div class="cont">
            <form class="form-inline">

                <div class="form-group" style="width: 100%;">
                    <input type="text" id="datetimepicker-day" class="form-control semi-date" placeholder="Data" />
                    <select ng-model="reservation.hour">
                        <option ng-repeat="fascia_oraria in avaible_fasce track by $index" value="{{fascia_oraria}}">{{fascia_oraria}}</option>
                    </select>
                    <select ng-model="reservation.people" ng-change="takeAvaibleFasce()">
                        <option ng-repeat="person in people" value="{{person}}">{{person}}</option>
                    </select>
                </div>
                <div class="clearfix"></div>
            </form>

            <div class="send_button" ng-if="!reservation_confirmed">
                <a class="btn btn-primary ghost dark btn-block disabled" id="reservation_btn" ng-click="reserve()">Prenota</a>
            </div>
            <p style="margin-top: 11px;" ng-if="reservation_confirmed">{{reservation_confirmed_text}}</p>


        </div>
    </div>

    <div class="form-content" ng-if="reservation_as_to_add_phone">
        <div class="cont">
            <form>
                <div class="form-group">
                    <label>Numero di telefono</label>
                    <input class="form-control" type="text" ng-model="user_phone.phone" />
                </div>
                <div class="form-group">
                    <a class="btn btn-primary btn-block ghost" ng-click="addPhoneNumber()">
						Conferma
					</a>
                </div>
            </form>
        </div>
    </div>
</div>
<?php endif; ?>