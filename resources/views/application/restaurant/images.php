<?php if($restaurant->video && $restaurant->video != '') : ?>
<div id="restaurant_video">
    <hr />
    <h3>Video</h3>    
    <div class="embed-responsive only_this embed-responsive-16by9">
      <iframe class="embed-responsive-item" src="<?php echo  $restaurant->video;?>"></iframe>
    </div>
</div>
<?php endif; ?>
<!-- blocco gallery -->

<?php 
$ni = count($restaurant->images);
if($ni > 0):
?>
<div>
    <hr />
    <h3>Foto del ristorante</h3>
    <div class="restaurant_gallery">

    <!-- 1 immagine -->
    <?php if($ni == 1) : ?>
        <div>
            <div>
                <div class="col-sm-6">
                    <div class="box_images qt" back-img img="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/restaurant/small/<?php echo escape_non_alphanum($restaurant->images[0]);?>.png" 
                        ng-click="openGallery(0)">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    <?php endif; if($ni == 2) : ?>
        <div>
            <div>
                <?php for($n = 0; $n < 2; $n++): ?>
                <div class="col-sm-6">
                    <div class="box_images qt" back-img img="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/restaurant/small/<?php echo escape_non_alphanum($restaurant->images[$n]);?>.png" 
                        ng-click="openGallery(<?php echo $n;?>)">
                    </div>
                </div>
            <?php endfor; ?>
            </div>
        </div>
    <?php endif; if($ni == 3) : ?>
        <!-- 3 immagini -->
        <div>
            <?php for($n = 0; $n < 3; $n++): ?>
            <div class="col-sm-4">
                <div class="box_images sn" back-img img="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/restaurant/small/<?php echo escape_non_alphanum($restaurant->images[$n]);?>.png" 
                    ng-click="openGallery(<?php echo $n;?>)">
                </div>
            </div>
            <?php endfor; ?>
        </div>
    <?php endif; if($ni == 4) : ?>
        <!-- 4 immagini -->
        <div>
            <div>
                <?php for($n = 0; $n < 4; $n++): ?>
                <div class="col-sm-3">
                    <div class="box_images sn" back-img img="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/restaurant/small/<?php echo escape_non_alphanum($restaurant->images[$n]);?>.png" 
                    ng-click="openGallery(<?php echo $n;?>)">
                    </div>
                </div>
            <?php endfor; ?>
            </div>            
        </div>
    <?php endif; if($ni > 4) : ?>
        <!-- 5 immagini -->
        <div>
            <div class="right-border">
                <?php for($n = 0; $n < 2; $n++): ?>
                <div class="col-sm-6" >
                    <div class="box_images qt" back-img img="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/restaurant/small/<?php echo escape_non_alphanum($restaurant->images[$n]);?>.png" 
                    ng-click="openGallery(<?php echo $n;?>)">
                    </div>
                </div>
                <?php endfor; ?>
            </div>
            <div class="clearfix"></div>
            <div class="right-padding">
                <?php for($n = 2; $n < 5; $n++): ?>
                <div class="col-sm-4" >
                    <div class="box_images sn" back-img img="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/restaurant/small/<?php echo escape_non_alphanum($restaurant->images[$n]);?>.png" 
                    ng-click="openGallery(<?php echo $n;?>)">
                        <?php if($ni > 5 && $n == 4) : ?>
                        <div class="overlay">
                            <p>+ <?php echo $ni-5;?></p>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    <?php endif; ?>
        <div class="clearfix"></div>
    </div>
    <hr />
</div>
<?php endif; ?>