<div class="image_gallery" 
    ng-class="{'active':menu_gallery_opened}">
    <div class="overlay"></div>
    <div class="image_active_container">
        <img galleryimageonload="active" ng-if="current_menu_gallery_image != ''" ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/menu/big/{{current_menu_gallery_image}}.png" alt="" title="" />
    </div>
    <div class="controls">
        <a ng-click="closeMenuGallery()" class="close_gallery_icon"><i class="mebi-close"></i></a>        
    </div>
</div>