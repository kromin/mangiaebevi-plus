<div ng-controller="MapCtrl">
    <div class="directions">
        <h3>Calcola il percorso</h3>
        <form class="form-inline" style="margin-bottom: 15px;">
            <div class="col-sm-6 no-pad">
                <input ng-model="start_direction_address" 
                ng-autocomplete 
                details="details"
                type="text" 
                class="form-control" 
                placeholder="Da dove vuoi partire" ng-enter="calculateDirections('<?php echo remove_quotes_and_tags($restaurant->address);?>')" />
            </div>
            <div class="col-sm-6 small_marginated_btn">
                <a class="btn btn-primary btn-sm ghost" style="font-size: 13px;" ng-click="calculateDirections('<?php echo remove_quotes_and_tags($restaurant->address);?>')">Calcola</a>
            </div>
            <div class="clearfix"></div>
        </form>
        <div class="clearfix"></div>
        <div ng-if="directions_texts != ''" class="col-lg-12">
            <p ng-repeat="text in directions_texts" ng-bind-html="text" class="directions_return"></p>
        </div>
    </div>
    <div id="google_map"></div>
</div>