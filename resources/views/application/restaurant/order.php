<div id="cart_form" class="hidden cart-form">
		<div class="reservation-header">
			Il tuo ordine
		</div>	
		<div class="form-content">
			<p class="text-center" ng-if="current_order.plates.length == 0">Seleziona i piatti da ordinare</p>
			<div class="col-sm-12">
				<ul class="list-unstyled cart_menu_order"  ng-if="menu_restaurant.menu.menus[currentMenu].menu_type == 1 && current_order.menu_qty > 0">
					<li>
						<div class="row order_plate_container">
							<div class="plate_addition">
								<i class="fa fa-chevron-up" ng-click="moreQtyFixedMenu()" ng-if="order_confirmed == 0"></i>
								<i class="fa fa-chevron-down" ng-click="lessQtyFixedMenu()" ng-if="order_confirmed == 0"></i>
							</div>
							<div class="col-sm-9 no-padding">
								
								<p class="plate_name"><span class="light">{{current_order.menu_qty}} x</span> {{current_order.menu_name}}</p>								
							</div>
							<div class="col-sm-3" style="padding-left: 0;">
								<p class="pull-right plate_price">{{menu_restaurant.menu.menus[currentMenu].menu_price|number:2}} € 
									<i class="fa fa-close" ng-click="removeFixedMenu()" ng-if="order_confirmed == 0"></i>
								</p>
							</div>
						</div>
					</li>
				</ul>
				<ul class="list-unstyled cart_menu_order" ng-if="current_order.menu_type == 1">
					<li ng-repeat="(i_plate, plate) in current_order.plates track by $index">
						<div class="row order_plate_container">
							<div class="col-sm-12 no-padding">
								<p class="plate_name">
								{{plate.name}}</p>
								<ul class="list-unstyled option_list_in_chart" ng-repeat="(i_option, option) in plate.options">
									<li ng-repeat="(i_opt, opt) in option.options">
										<i class="fa fa-chevron-up" ng-click="moreQtyOption(i_opt, i_plate, i_option)" ng-if="order_confirmed == 0"></i>
										<i class="fa fa-chevron-down" ng-click="lessQtyOption(i_opt, i_plate, i_option)" ng-if="order_confirmed == 0"></i>
										{{opt.num}} x {{opt.name}} 
										<span ng-if="opt.no_variation == 0">
											<i ng-if="opt.variation == 0 || !opt.variation"> + </i>
											<i ng-if="opt.variation == 1"> - </i>
											{{opt.price}} €
										</span>
									</li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
				<ul class="list-unstyled cart_menu_order" ng-if="current_order.menu_type != 1">
					<li ng-repeat="plate in current_order.plates track by $index">
						<div class="row order_plate_container">
							<div class="plate_addition">
								<i class="fa fa-chevron-up" ng-click="moreQtyOrder($index)" ng-if="order_confirmed == 0"></i>
								<i class="fa fa-chevron-down" ng-click="lessQtyOrder($index)" ng-if="order_confirmed == 0"></i>
							</div>
							<div class="col-sm-9 no-padding">
								<p class="plate_name" ng-click="editPlate(plate)">
								<span class="light">{{plate.qty}} x</span> {{plate.name}}</p>
								<ul class="list-unstyled option_list_in_chart" ng-repeat="option in plate.options">
									<li ng-repeat="opt in option.options" ng-if="opt.selected == 1">{{opt.name}}</li>
								</ul>
							</div>
							<div class="col-sm-3" style="padding-left: 0;">
								<p class="pull-right plate_price">{{plate.total_price|number:2}} € 
									<i class="fa fa-close" ng-click="removePlateFromOrder($index)" ng-if="order_confirmed == 0"></i>
								</p>
							</div>
						</div>
					</li>
				</ul>
				<div class="riepilogo">
					<ul class="list-unstyled col-sm-12">
						<li>
							Subtotale: <span class="pull-right">{{current_order.subtotal|number:2}} €</span>
						</li>
						<?php /*<li>
							Consegna: <span class="pull-right">{{current_order.spedizione|number:2}} €</span>
						</li>*/?>
						
					</ul>
				</div>
			</div>
			
			<div class="clearfix"></div>
			<div class="order_totals">
				<div class="col-sm-6">
					<p class="order-total">{{current_order.price|number:2}} €</p>
				</div>
				<div class="col-sm-6 btn-confirm-container">
					<a class="uppercase text-primary" ng-click="confirmOrder()">
						<span ng-if="order_confirmed == 0">Conferma</span>
						<span ng-if="order_confirmed == 1"><i class="mebi-left-open-big back_arrow"></i></span>
					</a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div><!-- cart -->