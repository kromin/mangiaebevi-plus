<?php
require_once '_head.php';
require_once '_topbar.php';
?>
<!-- Home page -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/s/{search_term_string}/",
    "query-input": "required name=search_term_string"
  }
}
</script>
    <div ng-controller="HomeCtrl" >
    <div class="fixed_bg" back-img img="/img/home/bg.jpg" <?php if(isset($alert)) echo 'ng-init="alertVerification()"';?>>
    </div>
    <div class="section MainImages">
        <div id="home_images" back-img img="/img/home/home.jpg">
            <div class="first-section-overlay"></div>
            <?php /*<div class="embed-responsive embed-responsive-16by9" style="width: 100%;">
                <video autoplay loop poster="/img/copertinavideo.png" id="bgvid" class="embed-responsive-item">
                    <source src="/video/compressed_video.webm" type="video/webm">
                    <source src="/video/compressed_video.mp4" type="video/mp4">
                </video>
            </div>*/?>
            <div id="main_page_content">
                <?php /*<h1 class="main_home_title">Il tuo ristorante preferito<br />dove e quando vuoi tu</h1> */ ?>
                <h1 class="main_home_title">Raccomanda e condividi<br/>i tuoi ristoranti preferiti</h1>
                <?php /*<p class="text-center center_home_text">
                <img src="/img/logo-small_blue.png" alt="mangiaebeviplus" title="Logo MangiaeBevi+" class="menoo_image_logo" />
                    <b class="text-primary">CERCA O PRENOTA</b>
                </p>*/ ?>
                <form ng-submit="goToSearch()">
                    <label class="sr-only" for="search_restaurant_form">Inserisci località, cucina o ristorante</label>
                    <input ng-model="main.search" id="search_restaurant_form" name="address" placeholder="Inserisci località, cucina o ristorante" ng-enter="goToSearch()" />
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
        <div class="go_down" scroll-to="#citiesblock">
            <p> Cerca nella tua città
                <i class="fa fa-chevron-down"></i>
            </p>
        </div>
    </div>
    <div class="section MainCities" id="citiesblock">
        <div class="container">
            <h2>Cerca nella tua città</h2>
        </div>
        <div class="container">
            <div class="row">
            	<?php for($n = 0; $n < 4; $n++) : ?>
                <div class="col-md-3">
                    <a href="/citta/<?php echo $cities[$n]->name;?>/{{adding_str_to_city_link}}">
                        <div class="city_container" back-img img="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_STATIC_DOMAIN');?>/homecity/medium/<?php echo $cities[$n]->image;?>.png">
                            <div class="overlay"></div>
                            <h3><?php echo $cities[$n]->name;?></h3>
                        </div>
                    </a>
                </div>
                <?php
                endfor;
                ?>
            </div>
            <p class="all_cities_link">
                <a ng-click="hidden_cities = !hidden_cities" class="open_cities_link">
    				Vedi le altre città <br /><i class="fa fa-chevron-down"></i></a>
            </p>
            <div class="row hidden_cities" ng-class="{'open':hidden_cities}">
            	<?php for($n = 4; $n < 10; $n++) : ?>
                <div class="col-md-2">
                    <a href="/citta/<?php echo $cities[$n]->name;?>/{{adding_str_to_city_link}}">
                        <div class="city_container" back-img img="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_STATIC_DOMAIN');?>/homecity/small/<?php echo $cities[$n]->image;?>.png">
                            <div class="overlay"></div>
                            <h3><?php echo $cities[$n]->name;?></h3>
                        </div>
                    </a>
                </div>
                <?php
                endfor;
                ?>
            </div>

        </div>
    </div>
    <div class="section MainApp">
        <div class="overlay"></div>
        <div class="container title-container">
            <h2>I migliori ristoranti a portata di mano</h2>
            <p class="hero_text">SCARICA ORA LA NOSTRA APP</p>

        </div>
        <div class="app_btn">
            <div class="container more">
                <a class="btn btn-primary ghost app_button pull-left" href="https://itunes.apple.com/it/app/mangiaebevi/id424492685?mt=8"><i class="fa fa-apple"></i> Iphone</a>
                <a class="btn btn-primary ghost app_button pull-right" href="https://play.google.com/store/apps/details?id=com.digikom.mangiaebevi&hl=it"><i class="fa fa-android"></i> Android</a>
            </div>
        </div>


        <div class="images_bg_container">
            <img ng-src="/img/home/cellulari.png" alt="" title="" class="reveal" />
        </div>
    </div>
</div>

<?php
require_once '_footer.php';
?>