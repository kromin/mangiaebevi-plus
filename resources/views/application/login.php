<?php
require_once '_head.php';
$blue_header = 1;
require_once '_topbar.php';
//$plate_encoded = str_replace("\"","\\\"",str_replace("'","\'",json_encode(json_decode(json_encode($pl),true))));
?>

<div class="white_page_login" ng-controller="LoginCtrl">
	<div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
		<div class="social-log-container">
	    	<h3>Accedi con il tuo social network preferito</h3>
	    	<i class="fa fa-facebook" ng-click="accessWithSocial('facebook')"></i>
			<i class="fa fa-twitter" ng-click="accessWithSocial('twitter')"></i>
			<i class="fa fa-linkedin" ng-click="accessWithSocial('linkedin')"></i>
	    </div>							
		<div class="form-info">			
			<form name="loginForm">
				<h3 class="text-center">O compila il form per accedere</h3>
				<div class="form-group">
					<label for="email" class="sr-only">Email</label>
					<input type="email" ng-model="login.email" id="email" class="form-control" placeholder="Email" required/>
				</div>
				<div class="form-group">
					<label for="password" class="sr-only">Password</label>
					<input type="password" ng-model="login.password" id="password" class="form-control" placeholder="Password" required/>
				</div>
				<p class="text-danger" ng-if="login_form_error_message != ''">{{login_form_error_message}}</p>
				<a class="link_btn" href="/recupera-password/">Recupera la password</a><br />
				<a class="link_btn" href="/attiva-account/">Reinvia il link di attivazione</a>
				<div class="form-group">
					<input type="submit" style="margin: 12px 0;" ng-disabled="loginForm.$invalid" ng-click="login()" class="btn btn-block btn-primary ghost" value="Entra" />
				</div>
			</form>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php
require_once '_footer.php';
?>