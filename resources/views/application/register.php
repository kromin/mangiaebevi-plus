<?php
require_once '_head.php';
$blue_header = 1;
require_once '_topbar.php';
?>

<div class="white_page_login" ng-controller="RegistrationCtrl">
	<div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2" ng-if="!hide_form">
		<div class="social-log-container">
	    	<h3>Registrati con il tuo social network preferito</h3>
	    	<i class="fa fa-facebook" ng-click="accessWithSocial('facebook')"></i>
			<i class="fa fa-twitter" ng-click="accessWithSocial('twitter')"></i>
			<i class="fa fa-linkedin" ng-click="accessWithSocial('linkedin')"></i>
	    </div>
		<div class="form-info">
			<form name="reserveForm" ng-submit="register(reserveFormData)">
				<h3 class="text-center">O compila il form per registrarti</h3>
				<div class="form-group">
					<label for="name" class="sr-only">Nome *</label>
					<input type="text" ng-model="reserveFormData.name" id="name" class="form-control" placeholder="es. Mario" required/>
				</div>
				<div class="form-group">
					<label for="name" class="sr-only">Cognome *</label>
					<input type="text" ng-model="reserveFormData.surname" id="surname" class="form-control" placeholder="es. Rossi" required/>
				</div>
				<div class="form-group">
					<label for="phone" class="sr-only">Telefono *</label>
					<input type="text" ng-model="reserveFormData.phone" id="phone" class="form-control" placeholder="Telefono" required/>
				</div>
				<div class="form-group">
					<label for="email" class="sr-only">Email *</label>
					<input type="email" ng-model="reserveFormData.email" id="email" class="form-control" placeholder="Email" required/>
				</div>
				<div class="form-group">
					<label for="password" class="sr-only">Password * (min. 4 max. 12 caratteri)</label>
					<input type="password" ng-model="reserveFormData.password" id="password" class="form-control" placeholder="Password" required/>
				</div>
				<div class="form-group check">
					<label class="label--checkbox block">
	                    <input class="checkbox" type="checkbox" ng-model="reserveFormData.check" id="check" required> Accetti i termini e condizioni di MangiaeBevi+
	                </label>
				</div>
				<a class="link_btn" href="/accedi/">Sei già registrato?</a>
				<div class="form-group">
					<input type="submit" style="margin: 12px 0;" ng-disabled="reserveForm.$invalid" class="btn btn-block btn-primary ghost" value="Registrati" />
				</div>
			</form>
		</div>
	</div>
	<div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2" ng-if="hide_form">
		<div class="social-log-container">
	    	<h3>Hai ricevuto il link di attivazione al tuo indirizzo email</h3>
	    </div>
	</div>
	<div class="clearfix"></div>
</div>
<?php
require_once '_footer.php';
?>