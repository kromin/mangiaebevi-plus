<?php
require_once '_head.php';
$blue_header = 1;
require_once '_topbar.php';
?>
<div class="not_found">
<div class="container">
	<p class="notfoundtext">Spiacente, si è verificato un errore</p>
	<p class="notfoundsorry">:(</p>
	<p class="backtohomelink"><a href="/">Torna all'home page</a></p>
</div>
</div>
<?php
require_once '_footer.php';
?>