<?php
require_once '_head.php';
$blue_header = 1;
require_once '_topbar.php';
//$plate_encoded = str_replace("\"","\\\"",str_replace("'","\'",json_encode(json_decode(json_encode($pl),true))));
?>
<div id="platePage" class="white_page">
    <div class="container">
        <div class="text-center">
            <img ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/community/square/<?php echo $pl->image;?>.png" alt="" title="" class="img-circle" />
        </div>
        <div>
            <h1 class="text-center"><a href="/piatto/<?php echo $parent->_id;?>/"><?php echo $parent->name;?></a></h1>
            <p class="text-center"><?php echo $user->text;?></p>
            <p class="text-center">
                Caricato da<br/>
                <label><a href="/u/<?php echo $user->_id;?>/"><?php echo $user->name;?> <?php echo $user->surname;?></a></label>
            </p>
        </div>
        <hr />        
        <h4 class="text-center">VOTO</h4>
        <p class="text-center rating_p"><?php echo $pl->rating;?></p>      
                
    </div>
</div>
<?php
require_once '_footer.php';
?>