<?php
$header_class = '';
if(isset($blue_header) && $blue_header === 1) $header_class .= 'blue ';

if(isset($search_header) && $search_header === 1) $header_class .= 'col-md-9';
?>
<div class="hidden show_on_load <?php echo $header_class;?>" id="topbar" ng-if="!remove_top_bar" scroll ng-controller="TopbarCtrl">
    <div class="" ng-class="{'container-fluid':search, 'container':!search}">
        <a href="/"><img class="logo-left" ng-src="/img/logo-text.png" alt="MEB" title="PlusMangiaeBevi" /></a>
        <?php /*<ul class="topbar-navigation pull-left hidden-xs hidden-sm lang">
            <li style="color: #fff;"><a href="/">ITA</li>
        </ul> */ ?>
        <a href="/"><img class="logo" ng-src="/img/logo.png" alt="MEB" title="PlusMangiaeBevi" /></a>
        <ul class="topbar-navigation pull-right hidden-sm hidden-xs" ng-if="current_user.loggedStatus == 0">
            <?php /*<li><a href="https://www.mangiaebevi.it">Magazine</a></li>*/ ?>
            <li><a href="/accedi/">Accedi</a></li>
            <li><a href="/registrati/">Registrati</a></li>
            <li><a class="btn btn-primary" href="/aggiungi-un-ristorante">aggiungi un ristorante</a></li>
        </ul>
        <ul class="topbar-navigation pull-right hidden-xs hidden-sm" id="dropdown-user-menu" ng-if="current_user.loggedStatus == 1">
            <li class="no_b">{{current_user.user.name}} {{current_user.user.surname}}</li>
            <li>
                <img ng-click="openDropUserMenu()" ng-if="current_user.user.avatar && current_user.user.avatar != ''" ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/users/small/{{current_user.user.avatar}}.png" alt="" title="" />
                <div back-img img="/img/default-avatar.jpg" ng-click="openDropUserMenu()" ng-if="!current_user.user.avatar || current_user.user.avatar == ''" class="user-image-container-topbar"></div>
                <ul ng-if="drop_open" class="dropdown_menu_el">
                    <li ng-click="openDropUserMenu()">Chiudi</li>
                    <?php /*<li><a href="https://www.mangiaebevi.it">Blog</a></li>*/?>
                    <li ng-if="current_user.user.role > 2"><a href="https://administration.mangiaebevi.it">amministrazione</a></li>
                    <li><a href="/u/{{current_user.user._id}}/">profilo</a></li>
                    <li ng-if="current_user.user.role > 1"><a href="/aggiungi-un-ristorante">aggiungi un ristorante</a></li>
                    <li><a ng-click="logout()">esci</a></li>
                </ul>
            </li>
        </ul>
        <div id="nav-icon4" class="visible-xs visible-sm hidden-md hidden-lg" ng-click="openMenuMobile()" ng-class="{'open':openmobile}">
          <span></span>
          <span></span>
          <span></span>
        </div>
    </div>
</div>
<div id="mobilemenu" class="visible-xs visible-sm hidden-md hidden-lg" ng-class="{'open':openmobile}">
    <ul class="list-unstyled" ng-if="current_user.loggedStatus == 1">
        <?php /*<li><a href="https://www.mangiaebevi.it">Blog</a></li>*/?>
        <li ng-if="current_user.user.role > 2"><a class="btn btn-primary" href="https://administration.mangiaebevi.it">amministrazione</a></li>
        <li><a class="btn btn-primary" href="/u/{{current_user.user._id}}/">profilo</a></li>
        <li ng-if="current_user.user.role > 1"><a class="btn btn-primary" href="/aggiungi-un-ristorante">aggiungi un ristorante</a></li>
        <li><a class="btn btn-primary" ng-click="logout()">esci</a></li>
    </ul>
    <ul class="list-unstyled" ng-if="current_user.loggedStatus == 0">
        <?php /*<li><a href="https://www.mangiaebevi.it">Blog</a></li>*/?>
        <li><a class="btn btn-primary" href="/accedi/">Accedi</a></li>
        <li><a class="btn btn-primary" href="/registrati/">Registrati</a></li>
        <li><a class="btn btn-primary" href="/aggiungi-un-ristorante">aggiungi un ristorante</a></li>
    </ul>
</div>