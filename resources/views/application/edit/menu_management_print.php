<div class="print_menu_action">
	<h3 class="text-center no_print">Scegli lo stile del tuo menu</h3>
	<div class="select_menu_message no_print" ng-if="((restaurant.menu.menus.length)-1) < current_menu_viewed == -1">
		<p>Seleziona un menu</p>
	</div>
	<div class="minimal_tabs no_print" ng-if="((restaurant.menu.menus.length)-1) >= current_menu_viewed">
        <ul class="list-unstyled">
            <li ng-class="{'active' : !restaurant.menu.menus[current_menu_viewed].menu_style || restaurant.menu.menus[current_menu_viewed].menu_style == 0}">
            	<a ng-click="restaurant.menu.menus[current_menu_viewed].menu_style = 0">Standard</a>
            </li>
            <li ng-class="{'active' : restaurant.menu.menus[current_menu_viewed].menu_style == 1}">
            	<a ng-click="restaurant.menu.menus[current_menu_viewed].menu_style = 1">Centrato</a>
            </li> 
            <li ng-class="{'active' : restaurant.menu.menus[current_menu_viewed].menu_style == 2}">
            	<a ng-click="restaurant.menu.menus[current_menu_viewed].menu_style = 2">Visuale</a>
            </li> 
            <li ng-class="{'active' : restaurant.menu.menus[current_menu_viewed].menu_style == 3}">
            	<a ng-click="restaurant.menu.menus[current_menu_viewed].menu_style = 3">Solo testo</a>
            </li>                             
        </ul>
    </div>
    <div class="print_actions">
    	<p>
    		<a class="text-success" ng-click="printMenu()"><i class="fa fa-print" aria-hidden="true"></i> Stampa il menu</a>
    		<a class="text-danger" ng-click="menuPdf()"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Scarica il pdf</a>
    	</p>
    </div>
	<div class="standalone_menu" ng-if="((restaurant.menu.menus.length)-1) >= current_menu_viewed">
		<div class="menu" 
			ng-class="{'style_one' : !restaurant.menu.menus[current_menu_viewed].menu_style || restaurant.menu.menus[current_menu_viewed].menu_style == 0,
					   'style_two' : restaurant.menu.menus[current_menu_viewed].menu_style == 1,
					   'style_tree' : restaurant.menu.menus[current_menu_viewed].menu_style == 2,
					   'style_four' : restaurant.menu.menus[current_menu_viewed].menu_style == 3}">
			<div class="menu_title">
				<h3>{{restaurant.menu.menus[current_menu_viewed].name}}</h3>
			</div>
			<div class="pre_text">
				<p class="wrap_text">{{restaurant.menu.menus[current_menu_viewed].pre_text}}</p>
			</div>
			<div class="menu_price" ng-if="restaurant.menu.menus[current_menu_viewed].menu_type == 1">
				<p class="menu_price_text">Menu a prezzo fisso</p>
				<p class="menu_price_price">€ {{restaurant.menu.menus[current_menu_viewed].menu_price}}</p>
			</div>
			<div class="menu_category" ng-repeat="(i_cat,cat) in restaurant.menu.menus[current_menu_viewed].categorie">
				<div class="clearfix"></div>
				<div class="menu_category_title">
					<h4>{{cat.name}}</h4>
				</div>
				<div class="menu_plate"
					ng-class-even="'even'"
					ng-repeat="(i_piatto, piatto) in restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti">
					<div class="contain_plate">
						<div class="plate_image" ng-if="piatto.image && piatto.image != ''">
							<img ng-src="{{staticURL}}/menu/small/{{piatto.image}}.png" alt="plate image" />
						</div>
						<div class="plate_content" ng-class="{'with_price' : restaurant.menu.menus[current_menu_viewed].menu_type == 0, 'with_image' : piatto.image && piatto.image != ''}">
							<h5>{{piatto.name}}</h5>
							<p>{{piatto.description}}</p>
							<p class="plate_ingredienti" ng-if="piatto.ingredienti.length > 0"><b>Ingredienti: </b> <span ng-repeat="ing in piatto.ingredienti">{{ing}}{{$last ? '' : ', '}}</span></p>
							<p class="plate_allergeni" ng-if="piatto.allergeni.length > 0"><b>Allergeni: </b> <span ng-repeat="all in piatto.allergeni">{{all}}{{$last ? '' : ', '}}</span></p>
							<div class="plate_options" ng-repeat="(i_opt_cat, opt_cat) in piatto.options">
								<p><b>{{opt_cat.name}}</b></p>
								<ul class="option_style">
									<li ng-repeat="(i_opzione,opzioni) in opt_cat.options">
										<span class="option_name">{{opzioni.name}}</span>
										<span class="option_addition" ng-if="opzioni.no_variation == 0 && opzioni.variation == 0">
											+ {{opzioni.price}} €
										</span>
										<span class="option_addition" ng-if="opzioni.no_variation == 0 && opzioni.variation == 1">
											- {{opzioni.price}} €
										</span>
									</li>
								</ul>
							</div>
						</div>
						<div class="plate_price" ng-class="{'with_image' : piatto.image && piatto.image != ''}" ng-if="restaurant.menu.menus[current_menu_viewed].menu_type == 0 && piatto.price > 0">
							<p><span class="euro">€</span> <span class="price">{{piatto.price}}</span></p>
						</div>
					</div>					
				</div>
				<div class="clearfix"></div>
				<hr class="row_separator" />
			</div>
			<div class="post_text">
				<p class="wrap_text" ng-bind="restaurant.menu.menus[current_menu_viewed].post_text"></p>
			</div>
		</div>
	</div>
</div>