<div ng-show="current_tab == 5">
	<div class="menu_publish" ng-controller="MenuPublishingCtrl">
		<?php /*
		<div class="container plan_container">
			<div class="col-sm-12" ng-init="initPlans()" 
		        ng-if="!view_user.billing_plan || view_user.billing_plan.length == 0 || view_user.billing_plan.active != 1">
		        <p class="text-center">Per poter aggiungere e gestire il tuo database di clienti devi iscriverti ad uno dei piani di abbonamento di MangiaeBevi</p>
		        <div class="col-sm-4" ng-repeat="plan in pacchetti">
		            <div class="plan_table" ng-class="{'featured': plan.featured == 1}">
		                <div class="plan_header">
		                    <h3 class="plan_price text-center" ng-if="selected_plan_period == 0">{{plan.price_month}} €</h3>
		                    <h3 class="plan_price text-center" ng-if="selected_plan_period == 1">{{plan.price_annual}} €</h3>
		                    / {{returnCicleText(selected_plan_period)}}
		                </div>
		                <div ng-if="plan.period == 2" class="plan_fatturazione text-center">
		                    <button ng-click="setPlanPeriod(0)" ng-class="{'text-primary':selected_plan_period==0}">mensile</button>
		                    <button ng-click="setPlanPeriod(1)" ng-class="{'text-primary':selected_plan_period==1}">annuale</button>
		                </div>
		                <div class="plan_services">
		                    <ul class="list-unstyled">
		                        <li ng-repeat="serviceplan in plan.services" class="text-center">{{serviceplan}}</li>
		                    </ul>
		                </div>
		                <div class="plan_button text-center">
		                    <button ng-click="submitPlan(plan._id)" class="outline_btn bprimary">Conferma</button>
		                </div>
		            </div>
		        </div>
		        <div class="clearfix"></div>
		    </div>
		</div>
		*/
		?>
	    <div class="stats_container" <?php if (auth()->user()->role < 3) : ?><?php endif; // ng-if="view_user.billing_plan && view_user.billing_plan.length >= 0 || view_user.billing_plan.active == 1" ?>>
	    	<div class="container" ng-controller="StatRestaurantCtrl" ng-init="loadStats(restaurant._id)">
		    	
		    	<div class="row">		    		
		    		<div class="col-sm-8">
		    			<h3 class="text-center title_stat">Ultimi 30 giorni</h3>
			    		<canvas id="base" class="chart-bar"
							chart-data="month_data" chart-labels="month_labels" chart-colors="colors"
							chart-dataset-override="datasetOverride_days">
						</canvas> 
					</div>
					<div class="col-sm-4">
						<div class="row">
							<div class="stat_block">
								<span>{{reservations}}</span>
								<h4>Prenotazioni ricevute</h4>
							</div>
							<div class="stat_block">
								<span ng-class="{'text-danger':restaurant.price > medium_price,'text-success':restaurant.price <= medium_price}">€ {{medium_price}}</span>
								<h4>Prezzo medio nella tua zona</h4>
							</div>
						</div>
					</div>
		    	</div>

		    	<div class="row">
		    		<h3 class="text-center title_stat">Anno in corso</h3>
		    		<div class="col-sm-6">
		    			<canvas id="base" class="chart-bar"
							chart-data="year_data_views" chart-labels="year_labels" chart-colors="colors"
							chart-dataset-override="datasetOverride_views">
						</canvas> 
					</div>
					<div class="col-sm-6">
						<canvas id="base" class="chart-bar"
							chart-data="year_data_time" chart-labels="year_labels" chart-colors="colors"
							chart-dataset-override="datasetOverride_time">
						</canvas> 
		    		</div>
		    	</div>
		    	
		    </div>
	    </div>
	</div>
</div>