<!-- reservation -->
        <div ng-if="current_tab == 4">
            <div class="white_container" ng-controller="EditReservationsCtrl">
                <div class="container">
                    <div ng-if="!restaurant.struttura || !restaurant.struttura.sale || restaurant.struttura.sale.length == 0">
                        <div class="hero_menu animated fadeIn text-center">
                            <h3>Ancora non hai aggiunto la struttura del tuo ristorante!</h3>
                            <p><a class="btn ghost" ng-click="changeTab(3)">Aggiungi le tue sale</a></p>
                        </div>

                    </div>
                    <div ng-if="restaurant.struttura && restaurant.struttura.sale && restaurant.struttura.sale.length > 0">

                        <div class="minimal_tabs text-center">
                            <span style="width: 250px;" class="fasce_span" ng-click="setReservationTab(0)" ng-class="{'active' : reservation_tab == 0 || !reservation_tab}">
                              Gestisci le prenotazioni
                            </span>
                            <span style="width: 250px;" class="fasce_span" ng-click="setReservationTab(1)" ng-class="{'active' : reservation_tab == 1}">
                              Le prenotazioni sul tuo sito web
                            </span>
                        </div>
                        <div ng-if="!reservation_tab || reservation_tab == 0">
                            <div class="col-lg-3 col-md-3 col-sm-12 ">
                                <div class="" ><!--ng-controller="ProfileCtrl"-->
                                    <div class="col-sm-12">
                                      <h4 class="uppercase size-medium inline-block">Calendario</h4>
                                      <p>Visualizza graficamente il calendario degli ultimi 2 mesi e dei prossimi due per un'immediato feedback sugli ordini ricevuti,
                                        in base al colore della casella corrispondente al giorno cambia il numero di prenotazioni ricevute.
                                        <br />
                                        Clicca sulle caselle per visualizzare le prenotazioni ricevute nella data corrispondente.</p>
                                      <!--<cal-heatmap config="{verticalOrientation:'true'}"></cal-heatmap>  -->
                                      <div id="heatmap" ng-init="InitReservations()"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-9 col-md-9 col-sm-12 container_tables">
                                <!-- Page Widget -->

                                <div class="col-sm-12">
                                    <h4 class="uppercase size-medium inline-block">Giorno corrente: {{current_date|amDateFormat:'DD/MM/YYYY'}}</h4>

                                    <div class="clearfix"></div>

                                    <label ng-if="is_adding_manual == 0">
                                        <a ng-click="setAddingManual(1)">
                                            Aggiungi una prenotazione manualmente
                                        </a>
                                         |
                                        <a ng-click="setAddingManual(2)">
                                            Esporta
                                        </a>
                                    </label>
                                    <div ng-if="is_adding_manual == 1">
                                        <form name="addManualForm" ng-submit="addManualReservation()">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label>Nome</label>
                                                        <input type="text" class="form-control" ng-model="manual_reservation_default.name" required />
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label>Persone</label>
                                                        <input type="number" class="form-control" ng-model="manual_reservation_default.people" required />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Telefono</label>
                                                        <input type="text" class="form-control" ng-model="manual_reservation_default.tel" required />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input type="email" class="form-control" ng-model="manual_reservation_default.email" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Orario</label>
                                                        <select ng-model="manual_reservation_default.fascia" class="form-control" required>
                                                            <option ng-repeat="fascia in restaurant_fasce track by $index" value="{{fascia}}">{{fascia}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <button class="btn btn-primary btn-outline btn-sm" ng-disabled="addManualForm.$invalid">Aggiungi</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    <label><a ng-click="setAddingManual(0)">Annulla</a></label>
                                    </div>
                                    <div ng-if="is_adding_manual == 2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>Inserisci un range di date o seleziona il giorno corrente per scaricare il pdf con il riepilogo dell prenotazioni</p>
                                            </div>
                                        </div>
                                        <div class="row" ng-if="export_reservation_obj.current == 0">
                                        <form>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Dal</label>
                                                    <input class="form-control" mask="39/19/2999" ng-model="export_reservation_obj.from" type="text" placeholder="GG/MM/AAAA"
                                                     />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Al</label>
                                                    <input class="form-control" mask="39/19/2999" ng-model="export_reservation_obj.to" type="text" placeholder="GG/MM/AAAA"
                                                     />
                                                </div>
                                            </div>
                                        </div>
                                        </form>
                                        <label class="label--checkbox block">
                                            <input class="checkbox" type="checkbox" name="useCurrent"
                                            ng-checked="export_reservation_obj.current == 1" ng-click="setCurrentDate()"
                                            />
                                            Usa il giorno corrente
                                        </label>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <button class="btn btn-primary btn-outline btn-sm" ng-click="openExportReservations()">Scarica il pdf</button>
                                                </div>
                                            </div>
                                        </div>

                                        <label><a ng-click="setAddingManual(0)">Annulla</a></label>
                                    </div>
                                    <div class="grey_bg">
                                        <div class="container">
                                            <div class="col-sm-12">
                                                <ul class="list-unstyled dashboard_list">
                                                    <li ng-repeat="reservation in reservations">
                                                        <div class="dashboard_friend">
                                                            <div class="element_header">
                                                                <div class="pull-left">
                                                                    <img
                                                                    ng-if="returnUser(reservation.user).avatar"
                                                                    class="profile_image"
                                                                    ng-src="{{staticURL}}/users/square/{{returnUser(reservation.user).avatar}}.png"
                                                                    />
                                                                    <img class="profile_image"
                                                                    ng-src="/img/default-avatar.png" alt="" title=""
                                                                    ng-if="!returnUser(reservation.user).avatar" />
                                                                    <br />
                                                                    <span class="text-warning" ng-if="reservation.type == 'phone'">
                                                                        <i class="mebi-phone-call"></i>
                                                                    </span>
                                                                    <span class="text-warning" ng-if="reservation.type == 'web'">
                                                                        <img ng-src="/img/logo_blue.png" style="width: 60px;" alt="by mangiaebevi" />
                                                                    </span>
                                                                </div>
                                                                <div class="col-sm-10 col-xs-9">
                                                                        {{reservation.name}}<br />
                                                                    <b>{{reservation.fascia_oraria}}</b>
                                                                    <span class="text-success" ng-if="reservation.confirmed == 1">confermato</span>
                                                                    <span class="text-danger" ng-if="reservation.confirmed == 0">non confermato</span>
                                                                    <span class="text-warning" ng-if="reservation.confirmed == 2">in attesa della conferma del cambio di orario</span>
                                                                    <span class="text-primary" ng-if="reservation.confirmed == 3">approvato il cambio di orario</span>
                                                                    <span class="text-danger" ng-if="reservation.confirmed == 4">annullata</span>
                                                                    <br />
                                                                    <span class="date">
                                                                        x {{reservation.people}} - {{reservation.tel}} - {{reservation.email}}
                                                                    </span>
                                                                    <br />
                                                                    <label ng-if="reservation.confirmed == 1 && reservation.type == 'web'">
                                                                        {{restaurant.struttura.sale[reservation.sala].name}}
                                                                    </label>
                                                                    <div>
                                                                        <span ng-if="reservation.preorder"><a class="text-primary" ng-click="viewPreOrder(reservation)">Preordine</a></span>
                                                                        <span ng-if="reservation.preorder.paid && reservation.preorder.paid == 1"> - pagato con Paypal</span>
                                                                    </div>
                                                                </div>

                                                                <div class="pull-right" ng-if="!reservation.type || reservation.type != 'phone'">
                                                                    <span class="hint--left" ng-if="reservation.confirmed != 4"
                                                                        aria-label="Annulla la prenotazione"
                                                                        ng-click="removeReservation(reservation._id)">
                                                                            <i class="mebi-close text-danger" style="font-size: 16px;"></i>
                                                                    </span>
                                                                    <div class="clearfix"></div>
                                                                    <span class="hint--left"
                                                                    aria-label="Gestisci"
                                                                    ng-click="loadSingleReservation(reservation._id,reservation)">
                                                                        <i class="mebi-calendar-1" style="font-size: 30px;"></i>
                                                                    </span>
                                                                </div>

                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <p class="not_found_message" ng-if="reservations.length == 0 && !loading_reservations">Non ci sono prenotazioni</p>
                                    </div>
                                </div>

                                <?php /*
                                    <p ng-if="!current_fascia">Seleziona una fascia oraria</p>
                                    <span class="fasce_span" ng-repeat="fascia in restaurant_fasce track by $index" ng-click="selectFasciaOraria(fascia)" ng-class="{'active': current_fascia == fascia}">
                                      {{fascia}}
                                    </span>
                                    <div class="clearfix fasce_orarie_block"></div>

                                    <div class="col-lg-9 col-md-8 col-sm-9">

                                      <div ng-repeat="(i_sala,sala) in restaurant.struttura.sale track by $index" ng-if="current_fascia">
                                        <h3 class="uppercase  col-xs-12">{{sala.name}}</h3>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 table_parent"
                                        ng-repeat="(i_table,table) in restaurant.struttura.sale[i_sala].tables"
                                        drop="onDrop($data, {{i_sala}}, {{i_table}}, $event)"
                                        effect-allowed="copy"
                                        drop-effect="copy"
                                        drop-accept="'json/reservation_obj'"
                                        drag-over-class="drag-over-accept" style="height: 238px;">
                                          <div class="single_table">
                                            <span class="table_num">{{table.num}}</span>
                                            <span class="table_posti">x{{table.posti}}</span>
                                          </div>

                                          <div ng-if="table.reservation" class="text-center">
                                            <p class="text-center">
                                                <label>{{returnUser(table.reservation.user).name}} {{returnUser(table.reservation.user).surname}}</label><br />
                                                <label class="text-primary" ng-click="loadSingleReservation(table.reservation._id)">Vedi</label>
                                            </p>
                                          </div>
                                        </div>
                                        <div class="clearfix fasce_orarie_block"></div>
                                      </div>

                                    </div>

                                    <div class="col-lg-3 col-md-4 col-sm-3">
                                      <p>Trascina le prenotazioni sui tavoli per confermarle</p>
                                      <ul class="list-unstyled imaging_list">
                                        <li draggable="true"
                                        effect-allowed="copy"
                                        draggable-type="reservation_obj"
                                        draggable-data="viewed_reservations[$index]"
                                        ng-repeat="reservation in viewed_reservations track by $index"
                                        style="cursor: move"><!-- ng-click="loadSingleReservation(reservation._id)"-->

                                            <div class="row">
                                              <div class="col-xs-12">
                                                <p class="text-center">
                                                    <img
                                                    ng-if="returnUser(reservation.user).avatar"
                                                    class="img-circle"
                                                    ng-src="{{staticURL}}/users/square/{{returnUser(reservation.user).avatar}}.png"
                                                    />
                                                </p>
                                                <p class="text-center">
                                                    <label>{{returnUser(reservation.user).name}} {{returnUser(reservation.user).surname}}</label><br />
                                                    <label class="text-primary" ng-click="loadSingleReservation(reservation._id)">modifica</label>
                                                </p>
                                                <p class="text-center">
                                                    {{reservation.tel}} - {{reservation.email}}
                                                </p>
                                                <div class="row icons_block">
                                                    <div class="text-center">
                                                        <i class="mebi-avatar"></i> {{reservation.people}}
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <hr />
                                        </li>
                                      </ul>
                                    </div>


                                    <div class="clearfix"></div>


                                  </div>
                                </div>
                                <?php */ ?>
                            </div>

                        </div>
                        <div ng-if="reservation_tab == 1">
                            <div class="menu_publish" ng-controller="MenuPublishingCtrl">
                                <?php /*
                                <div class="col-sm-12" ng-init="initPlans()"
                                    ng-if="!view_user.billing_plan || view_user.billing_plan.length == 0 || view_user.billing_plan.active != 1">
                                    <p class="text-center">Per poter aggiungere il widget sul tuo sito o nella tua pagina facebook devi iscriverti ad uno dei piani di abbonamento di MangiaeBevi</p>
                                    <div class="col-sm-4" ng-repeat="plan in pacchetti">
                                        <div class="plan_table" ng-class="{'featured': plan.featured == 1}">
                                            <div class="plan_header">
                                                <h3 class="plan_price text-center" ng-if="selected_plan_period == 0">{{plan.price_month}} €</h3>
                                                <h3 class="plan_price text-center" ng-if="selected_plan_period == 1">{{plan.price_annual}} €</h3>
                                                / {{returnCicleText(selected_plan_period)}}
                                            </div>
                                            <div ng-if="plan.period == 2" class="plan_fatturazione text-center">
                                                <button ng-click="setPlanPeriod(0)" ng-class="{'text-primary':selected_plan_period==0}">mensile</button>
                                                <button ng-click="setPlanPeriod(1)" ng-class="{'text-primary':selected_plan_period==1}">annuale</button>
                                            </div>
                                            <div class="plan_services">
                                                <ul class="list-unstyled">
                                                    <li ng-repeat="serviceplan in plan.services" class="text-center">{{serviceplan}}</li>
                                                </ul>
                                            </div>
                                            <div class="plan_button text-center">
                                                <button ng-click="submitPlan(plan._id)" class="outline_btn bprimary">Conferma</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                */
                                ?>
                                <div <?php if (auth()->user()->role < 3) : ?><?php endif; //ng-if="view_user.billing_plan && view_user.billing_plan.length >= 0 || view_user.billing_plan.active == 1" ?>>
                                    <div class="minimal_tabs no_print">
                                        <ul class="list-unstyled">
                                            <li ng-class="{'active' : !current_publish_action || current_publish_action == 0}">
                                                <a ng-click="publishAction(0)">Sul tuo sito</a>
                                            </li>
                                            <li ng-class="{'active' : current_publish_action == 1}">
                                                <a ng-click="publishAction(1)">Su facebook</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div ng-show="current_publish_action == 0 || !current_publish_action">
                                        <div class="row">
                                            <div class="col-sm-6" ng-init="initWidget()">
                                                <p>Per poter inserire i tuoi menu sul tuo sito web aggiungine il domino (www.nomedeltuosito.it)</p>
                                                <form id="addIframeForm">
                                                    <div class="form-group">
                                                        <label for="url">Dominio del sito web</label>
                                                        <input type="text" name="url" ng-model="add_iframe.url" class="activated_input full" required/>
                                                    </div>

                                                    <button type="submit" class="outline_btn bprimary" ng-disabled="addIframeForm.$invalid"
                                                    ng-click="addIframe()">Aggiungi</button>

                                                </form>
                                            </div>
                                            <div class="col-sm-6" ng-if="hasiframes == 1">
                                                <h4>I tuoi Domini</h4>
                                                <ul class="list-unstyled">
                                                    <li ng-repeat="u in iframe.url track by $index">{{u}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr />
                                        <div class="col-sm-12" ng-if="iframe">
                                            <h3>Codice da incorporare</h3>
                                            <p>Copia e incolla questo codice nel tuo sito web per aggiungere il widget di visualizzazione del menu</p>
                                            <pre>&lt;div id="mangiaebevi_restaurant_iframe"&gt;&lt;/div&gt;<br />&lt;script type="text/javascript" src="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/static/{{iframe.token}}/frameloader"&gt;&lt;/script&gt;
                                            </pre>
                                            <hr />

                                            <h3 class="uppercase size-medium">Esempio di visualizzazione del widget</h3>
                                            <div id="mangiaebevi_restaurant_iframe"></div>
                                            <script type="text/javascript" src="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/static/test/frameloader"></script>

                                        </div>
                                    </div>
                                    <div ng-show="current_publish_action == 1">
                                        <div class="row">
                                            <div class="col-sm-6" ng-if="!restaurant.facebook_page_id || restaurant.facebook_page_id == ''">
                                                <p>Per aggiungere la prenotazione e il menu sulla tua pagina facebook prima associa
                                                    l'ID della pagina al tuo ristorante e poi clicca sul pulsante "Pubblica su facebook"
                                                </p>
                                                <p><a href="https://it.wix.com/support/html5/article/come-faccio-a-trovare-lid-della-pagina-facebook" target="_blank" rel="nofollow">come trovare l'ID della tua pagina facebook</a></p>
                                                <div class="form-group">
                                                    <label for="fb_url">Inserisci l'ID numerico della pagina facebook</label>
                                                    <input type="text" id="fb_url" name="fburl" ng-model="add_facebook_page.page_id" class="activated_input full" required/>
                                                </div>
                                                <button type="submit" class="outline_btn bprimary" ng-disabled="addIframeForm.$invalid"
                                                ng-click="addFacebookID(add_facebook_page.page_id)">Aggiungi</button>
                                            </div>
                                            <div class="col-sm-6" ng-if="restaurant.facebook_page_id && restaurant.facebook_page_id != ''">
                                                <h3>ID della tua pagina facebook: {{restaurant.facebook_page_id}}</h3>
                                                <p><a class="text-danger" ng-click="removeFacebookPage()">rimuovi</a></p>
                                            </div>
                                            <div class="col-sm-6">
                                                <a class="facebook_btn" ng-click="enableFacebook()"><i class="fa fa-facebook"></i> Pubblica su facebook</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- end reservation -->