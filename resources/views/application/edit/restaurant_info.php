 <!-- edit restaurant -->
        <div ng-if="current_tab == 1">
            <div class="white_container">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 edit_fields_title">
                            Modifica le informazioni principali del tuo ristorante
                        </div>
                    <!-- info principali del ristorante -->
                        <div class="col-md-6 col-sm-12">
                            <i class="fa fa-map-marker"></i>                            
                                <input type="text" name="address" class="activated_input full" ng-model="restaurant.address"
                                ng-autocomplete 
                                details="details"
                                class="form-control" 
                                placeholder="Indirizzo" style="max-width: 80%" />
                                <br />
                            <i class="fa fa-envelope-o"></i> <input class="activated_input" type="email" ng-model="restaurant.email" autosize/>                            
                        </div>
                        <div class="col-md-6 col-sm-12">

                            <i class="fa fa-globe"></i> <input class="activated_input" type="text" ng-model="restaurant.web" autosize /><br />
                            
                            <i class="fa fa-phone"></i> 
                                <input class="activated_input" type="text" ng-model="add_restaurant_phone" placeholder="Aggiungi un numero di telefono" 
                                style="width: 238px; margin-right: -32px;" />
                                <i class="mebi-android-add" ng-click="addPhoneNumber(add_restaurant_phone)"></i>
                            <br />
                            <ul class="list-unstyled phones_list">
                                <li ng-repeat="phone in restaurant.phone track by $index"> {{phone}} <i class="mebi-close" ng-click="removeNumber($index)"></i></li>
                            </ul>

                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 col-sm-12">
                            <div class="restaurant-social">
                                <a target="_blank" ng-if="restaurant.social[0].facebook && restaurant.social[0].facebook != ''" href="{{restaurant.social[0].facebook}}" class="social-page-link">
                                    <i class="fa fa-facebook fb"></i>
                                </a>
                                <a target="_blank" ng-if="restaurant.social[1].twitter && restaurant.social[1].twitter != ''" href="{{restaurant.social[1].twitter}}" class="social-page-link">
                                    <i class="fa fa-twitter tw"></i>
                                </a>
                                <a target="_blank" ng-if="restaurant.social[2].pinterest && restaurant.social[2].pinterest != ''" href="{{restaurant.social[2].pinterest}}" class="social-page-link">
                                    <i class="fa fa-pinterest pi"></i>
                                </a>
                                <a target="_blank" ng-if="restaurant.social[3].instagram && restaurant.social[3].instagram && restaurant.social[3].instagram != ''" href="{{restaurant.social[3].instagram}}" class="social-page-link">
                                    <i class="fa fa-instagram in"></i>
                                </a>
                                <a target="_blank" ng-if="restaurant.social[4].googlePlus && restaurant.social[4].googlePlus != ''" href="{{restaurant.social[4].googlePlus}}" class="social-page-link">
                                    <i class="fa fa-google-plus gp"></i>
                                </a>
                                <a target="_blank" ng-if="restaurant.social[5].linkedin && restaurant.social[5].linkedin != ''" href="{{restaurant.social[5].linkedin}}" class="social-page-link">
                                    <i class="fa fa-linkedin li"></i>
                                </a>
                                <a target="_blank" ng-if="restaurant.social[6].youtube && restaurant.social[6].youtube != ''" href="{{restaurant.social[6].youtube}}" class="social-page-link">
                                    <i class="fa fa-youtube yt"></i>
                                </a>
                                <a target="_blank" ng-if="restaurant.social[7].tripadvisor && restaurant.social[7].tripadvisor != ''" href="{{restaurant.social[7].tripadvisor}}" class="social-page-link">
                                    <i class="fa fa-tripadvisor tr"></i>
                                </a>
                            </div>
                            <a ng-click="editSocial()">Modifica / Elimina link alle pagine social</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="grey_container">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- staff -->
                            <div class="col-sm-2">
                                <p class="big_text left_descriptor"><b>Staff:</b></p>
                            </div>
                            <div class="col-sm-10" id="staff_memebers_block">
                                <div class="row">
                                    <div class="col-md-3 col-sm-6">
                                        <ul class="list-unstyled">
                                            <li class="padded"><b>CHEF</b></li>
                                            <li class="padded" ng-repeat="chef in restaurant.staff.chef track by $index">
                                                <i class="icon wb-close-mini" ng-click="remove_staff_member('chef',$index)"></i>
                                                {{chef}}</li>
                                            <li>
                                                <input class="activated_input" type="text" ng-model="add_staff_member.chef_name" 
                                                placeholder="Nome" 
                                                style="width: 80%; margin-right: -5px;" />
                                                <i class="mebi-android-add" ng-click="add_staff_member_to_restaurant ('chef',add_staff_member.chef_name)"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <ul class="list-unstyled">
                                            <li class="padded"><b>SOMMELIER</b></li>
                                            <li class="padded" ng-repeat="sommelier in restaurant.staff.sommelier track by $index">
                                                <i class="icon wb-close-mini" ng-click="remove_staff_member('sommelier',$index)"></i>
                                                {{sommelier}}</li>
                                            <li>
                                                <input class="activated_input" type="text" ng-model="add_staff_member.sommelier_name" 
                                                placeholder="Nome" 
                                                style="width: 80%; margin-right: -5px;" />
                                                <i class="mebi-android-add" ng-click="add_staff_member_to_restaurant ('sommelier',add_staff_member.sommelier_name)"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <ul class="list-unstyled">
                                            <li class="padded"><b>BARMAN</b></li>
                                            <li class="padded" ng-repeat="barman in restaurant.staff.barman track by $index">
                                                <i class="icon wb-close-mini" ng-click="remove_staff_member('barman',$index)"></i>
                                                {{barman}}</li>
                                            <li>
                                                <input class="activated_input" type="text" ng-model="add_staff_member.barman_name" 
                                                placeholder="Nome" 
                                                style="width: 80%; margin-right: -5px;" />
                                                <i class="mebi-android-add" ng-click="add_staff_member_to_restaurant ('barman',add_staff_member.barman_name)"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <ul class="list-unstyled">
                                            <li class="padded"><b>MAITRE</b></li>
                                            <li class="padded" ng-repeat="maitre in restaurant.staff.maitre track by $index">
                                                <i class="icon wb-close-mini" ng-click="remove_staff_member('maitre',$index)"></i>
                                                {{maitre}}
                                            </li>
                                            <li>
                                                <input class="activated_input" type="text" ng-model="add_staff_member.maitre_name" 
                                                placeholder="Nome" 
                                                style="width: 80%; margin-right: -5px;" />
                                                <i class="mebi-android-add" ng-click="add_staff_member_to_restaurant ('maitre',add_staff_member.maitre_name)"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                             
                            </div>
                            <div class="clearfix"></div>
                            <hr />
                            <div class="col-sm-2" id="price_set">
                                <b>Prezzi:</b>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label for="prezzo_max">Prezzo minimo</label>
                                    <div class="range-slider">
                                        <input class="range-slider__range" type="range" ng-change="priceChange()" ng-model="restaurant.priceMin" min="0" max="495" step="5" id="prezzo_min">
                                        <span class="range-slider__value">{{restaurant.priceMin}}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="prezzo_max">Prezzo massimo</label>
                                    <div class="range-slider">
                                        <input class="range-slider__range" type="range" ng-change="priceChange()" ng-model="restaurant.priceMax" min="0" max="500" step="5" id="prezzo_max">
                                        <span class="range-slider__value">{{restaurant.priceMax}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr />
                            <div class="col-sm-2" id="price_set">
                                <b>Descrizione:</b>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">  
                                    <textarea class="form-control" msd-elastic ng-model="restaurant.description"></textarea>
                                </div>
                                <?php
                                    /* 
                                    <text-angular ta-toolbar="taOptions" ng-model="restaurant.description"></text-angular>        
                                    */
                                    ?>   
                            </div>
                            <div class="clearfix"></div>
                            <hr />
                            
                            
                            <div class="col-sm-2">
                                <p class="big_text left_descriptor"><b>Tipologia di locale:</b></p>
                                <a ng-click="openEditing(2)" class="text-primary" ng-if="open_editing != 2">MODIFICA</a>            
                                <a ng-click="openEditing(0)" class="text-success" ng-if="open_editing == 2">CONFERMA</a>     
                            </div>
                            <div class="col-sm-9" id="tipo_locale_block">
                                <div class="notEdit" ng-if="open_editing != 2">
                                    <div class="primary"
                                    drop="onPrimaryDrop($data, 'tipo_locale', $event)"  
                                    effect-allowed="copy" 
                                    drop-effect="copy" 
                                    drop-accept="'json/el_obj'"
                                    drag-over-class="drag_over"
                                    >
                                        {{tipiLocali[restaurant.tipoLocale[0]].name}}
                                    </div>
                                    <div class="clearfix"></div>
                                    <ul class="list-unstyled">
                                        <li 
                                        class="col-md-3" 
                                        draggable="true" 
                                        effect-allowed="copy"
                                        draggable-type="el_obj"  
                                        draggable-data="{index: $index}" 
                                        style="cursor: move"
                                        ng-if="$index > 0"
                                        ng-repeat="tipo_locale in restaurant.tipoLocale track by $index">
                                            {{tipiLocali[tipo_locale].name}}
                                        </li>
                                    </ul>  
                                </div>
                                <div class="edit" ng-if="open_editing == 2">
                                    
                                    <ul class="list-unstyled">
                                        <li class="col-md-3" ng-repeat="tipo_locale in tipiLocali track by $index" 
                                        ng-class="{'text-success' : hasTipoLocale(tipo_locale._id)}" 
                                        ng-click="addRemoveTipoLocale(tipo_locale._id)">{{tipo_locale.name}}</li>
                                    </ul> 
                                    <div class="clearfix"></div>
                                </div>    
                                <div class="clearfix"></div>
                                             
                            </div>
                            <div class="clearfix"></div>
                            <hr />

                            <!-- tipo di cucina -->
                            <div class="col-sm-2">
                                <p class="big_text left_descriptor"><b>Cucina:</b></p>
                                <a ng-click="openEditing(3)" class="text-primary" ng-if="open_editing != 3">MODIFICA</a>            
                                <a ng-click="openEditing(0)" class="text-success" ng-if="open_editing == 3">CONFERMA</a>   
                            </div>
                            <div class="col-sm-9">
                                <div class="notEdit" ng-if="open_editing != 3">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h3>Tipologia</h3>
                                            <div class="primary"
                                            drop="onPrimaryDrop($data, 'tipo_cucina', $event)"  
                                            effect-allowed="copy" 
                                            drop-effect="copy" 
                                            drop-accept="'json/el_obj'"
                                            drag-over-class="drag_over"
                                            >
                                                {{tipiCucine[restaurant.tipoCucina[0]].name}}
                                            </div>
                                            <div class="clearfix"></div>
                                            <ul class="list-unstyled">
                                                <li class="col-md-12" 
                                                draggable="true" 
                                                effect-allowed="copy"
                                                draggable-type="el_obj"  
                                                draggable-data="{index: $index}" 
                                                style="cursor: move"
                                                ng-repeat="tipo_cucina in restaurant.tipoCucina track by $index"
                                                ng-if="$index > 0"
                                                >{{tipiCucine[tipo_cucina].name}}</li>
                                            </ul>   
                                            
                                            
                                        </div> 
                                        <div class="col-sm-6">
                                            <h3>Regione</h3>
                                            <div class="primary"
                                            drop="onPrimaryDrop($data, 'regione_cucina', $event)"  
                                            effect-allowed="copy" 
                                            drop-effect="copy" 
                                            drop-accept="'json/el_obj'"
                                            drag-over-class="drag_over"
                                            >
                                                {{regioniCucine[restaurant.regioneCucina[0]].name}}
                                            </div>
                                            <div class="clearfix"></div>
                                            <ul class="list-unstyled">
                                                <li 
                                                draggable="true" 
                                                effect-allowed="copy"
                                                draggable-type="el_obj"  
                                                draggable-data="{index: $index}" 
                                                style="cursor: move"
                                                class="col-md-12" ng-repeat="regione_cucina in restaurant.regioneCucina track by $index"
                                                ng-if="$index > 0"
                                                >{{regioniCucine[regione_cucina].name}}</li>
                                            </ul>
                                           
                                            
                                        </div>  
                                    </div>      
                                </div>
                                <div class="edit" ng-if="open_editing == 3">
                                    <div class="col-sm-6">
                                        <h3>Tipologia</h3>
                                        <ul class="list-unstyled">
                                            <li class="col-md-6" ng-repeat="tipo_cucina in tipiCucine track by $index" 
                                            ng-class="{'text-success' : hasTipoCucina(tipo_cucina._id)}" 
                                            ng-click="addRemoveTipoCucina(tipo_cucina._id)">{{tipo_cucina.name}}</li>
                                        </ul> 
                                    </div>
                                    <div class="col-sm-6">
                                        <h3>Regione</h3>
                                        <ul class="list-unstyled">
                                            <li class="col-md-6" ng-repeat="regione_cucina in regioniCucine track by $index" 
                                            ng-class="{'text-success' : hasRegioneCucina(regione_cucina._id)}" 
                                            ng-click="addRemoveRegioneCucina(regione_cucina._id)">{{regione_cucina.name}}</li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                              
                            </div>
                            <div class="clearfix"></div>
                            <hr />


                            <div class="col-sm-2">
                                <p class="big_text left_descriptor"><b>Servizi:</b></p>
                                <a ng-click="openEditing(4)" class="text-primary" ng-if="open_editing != 4">MODIFICA</a>            
                                <a ng-click="openEditing(0)" class="text-success" ng-if="open_editing == 4">CONFERMA</a>        
                            </div>
                            <div class="col-sm-9">
                                <div class="notEdit" ng-if="open_editing != 4">
                                    <div class="col-lg-3 col-md-3 col-sm-6 single_service centered_icon_container" ng-repeat="service in restaurant.services">
                                        <i class="{{services[service].icon}} big"></i>
                                        <span>{{services[service].name}}</span>
                                    </div>   
                                </div> 
                                <div class="edit" ng-if="open_editing == 4">  
                                    <div class="col-lg-3 col-md-3 col-sm-6 single_service centered_icon_container" 
                                    ng-repeat="service in services"
                                    ng-click="addRemoveService(service._id)"
                                    >
                                        <i class="{{service.icon}} big" ng-class="{'text-success' : hasService(service._id)}"></i>
                                        <span ng-class="{'text-success' : hasService(service._id)}">{{service.name}}</span>
                                    </div>                          
                                    
                                    <div class="clearfix"></div>
                                </div>    

                                <div class="clearfix"></div>
                                 
                            </div>
                            <div class="clearfix"></div>
                            <hr />

                            <div class="col-sm-2">
                                <p class="big_text left_descriptor"><b>Carte accettate:</b></p>
                                <a ng-click="openEditing(5)" class="text-primary " ng-if="open_editing != 5">MODIFICA</a>            
                                <a ng-click="openEditing(0)" class="text-success " ng-if="open_editing == 5">CONFERMA</a> 
                            </div>
                            <div class="col-sm-9">
                                <div class="notEdit" ng-if="open_editing != 5">
                                    <ul class="list-unstyled">
                                        <li class="col-md-3" ng-repeat="carta in restaurant.carte">{{creditCard[carta].name}}</li>
                                    </ul> 
                                </div>
                                <div class="edit" ng-if="open_editing == 5"> 
                                                            
                                    <ul class="list-unstyled">
                                        <li class="col-md-3" ng-repeat="card in creditCard track by $index" 
                                        ng-class="{'text-success' : hasCard(card._id)}" 
                                        ng-click="addRemoveCard(card._id)">{{card.name}}</li>
                                    </ul> 
                                    <div class="clearfix"></div>
                                </div>  

                                <div class="clearfix"></div>
                                 


                            </div>
                            <div class="clearfix"></div>
                            <hr />

                        </div>
                        <div class="col-sm-2"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>    

            <div class="white_container">
                <div class="container">

                    <div class="orari_ristorante">
                        <div class="col-sm-12">
                            <h3 class="uppercase">Orari di apertura</h3>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="form-group">
                                <label>Giorno</label>
                                <select type="text" ng-model="add_fascia_oraria.day" class="form-control">
                                    <option ng-repeat="day in week_days track by $index" value="{{$index}}">{{day}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label>Dalle</label>
                                        <select ng-model="fascia_corrente.from" class="form-control">
                                            <option value="{{fascia_disponibile}}" ng-repeat="fascia_disponibile in avaibleFasce track by $index">{{fascia_disponibile}}</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-6">
                                        <label>Alle</label>
                                        <select ng-model="fascia_corrente.to" class="form-control">
                                            <option value="{{fascia_disponibile}}" ng-repeat="fascia_disponibile in avaibleFasce track by $index">{{fascia_disponibile}}</option>
                                        </select>
                                    </div>
                                </div>                        
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group" style="margin-top: 25px;">
                                <button ng-click="add_fascia_to_restaurant(add_fascia_oraria.day,add_fascia_oraria.fascia)" class="btn btn-default">Aggiungi</button>
                                <button ng-click="add_fascia_to_restaurant_all_days(add_fascia_oraria.fascia)" class="btn btn-primary">Aggiungi a tutti i giorni</button>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div ng-repeat="day in week_days track by $index" class="col-md-2 col-sm-4 col-xs-6">
                            <h3 class="size-small uppercase spacing"><b>{{day}}</b></h3>
                            <p ng-repeat="fascia in restaurant.fasce[$index] track by $index">{{fascia}}
                                <i class="mebi-close" ng-click="remove_fascia($index,$parent.$index)"></i>
                            </p>
                        </div>

                        <div class="clearfix"></div>
                        <hr />
                        <div class="col-sm-12">
                            <h4 class="uppercase size-medium">Giorni di chiusura/apertura speciali
                          </h4>
                            <p>Puoi inserire giorni speciali in cui il ristorante è aperto o chiuso, ad esempio durante festività ecc...
                            </p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-7">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Giorno</label>
                                        <input id="datepicker" type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Tipo</label>
                                        <select ng-model="specialDates.type" class="form-control">
                                            <option value="0">Chiusura</option>
                                            <option value="1">Apertura</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Orario</label>
                                        <select ng-model="specialDates.all_day" class="form-control">
                                            <option value="1">Tutto il giorno</option>
                                            <option value="0">Fascia oraria</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group" ng-if="specialDates.all_day == 0">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Dalle</label>
                                                <select ng-model="specialDates.from" class="form-control">
                                                    <option value="{{fascia_disponibile}}" ng-repeat="fascia_disponibile in avaibleFasce track by $index">{{fascia_disponibile}}</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Alle</label>
                                                <select ng-model="specialDates.to" class="form-control">
                                                    <option value="{{fascia_disponibile}}" ng-repeat="fascia_disponibile in avaibleFasce track by $index">{{fascia_disponibile}}</option>
                                                </select>   
                                            </div>
                                        </div>                          
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <button ng-click="setSpecialDate()" class="btn btn-primary btn-sm ghost">Aggiungi</button>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-sm-5">
                            <h4 class="uppercase size-medium">Giorni speciali
                          </h4>
                            <ul class="list-unstyled">
                                <li ng-repeat="day in restaurant.special_dates track by $index">
                                    <i class="mebi-close" ng-click="removeSpecialDate(day)"></i>
                                    {{day.day|amDateFormat:'DD/MM/YYYY'}}
                                    <b ng-if="day.type == '0'">Chiusura</b>
                                    <b ng-if="day.type == '1'">Apertura</b>
                                    <span ng-if="day.all_day == '0'">{{day.from}} - {{day.to}}</span>
                                    <span ng-if="day.all_day == '1'">Tutto il giorno</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr />

                    <div class="images_gallery col-md-8">
                        <h3 class="uppercase">Immagine principale</h3>
                        <div class="p_image"
                        <?php /*drop="setImagePrimary($data, $event)"  
                        effect-allowed="copy" 
                        drop-effect="copy" 
                        drop-accept="'json/img_obj'"
                        drag-over-class="drag_over_primary"
                        >
                        <div class="overlay">
                            <div class="txt_overlay">
                                Imposta come primaria
                            </div>
                        </div*/?>>
                            <div class="i cover" back-img img="{{staticURL}}/restaurant/medium/{{restaurant.image}}.png" ng-if="restaurant.image && restaurant.image != ''"></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <h3 class="uppercase">Tutte le immagini</h3>
                        <div class="gallery">                   
                            <div class="single_img_gallery"                             
                                ng-repeat="image in restaurant.images track by $index">
                                <div class="single_img cover col-sm-3" 
                                    back-img img="{{staticURL}}/restaurant/small/{{image}}.png"
                                    >
                                    <div class="image_actions">
                                        <i class="fa fa-trash" ng-click="removeImageOnClick(image)"></i>
                                        <i class="fa fa-star-o" ng-click="setImagePrimaryOnClick(image)"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="gallery_buttons">
                            <div class="upload">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <input class="upload-input" type="file" ngf-select 
                                multiple="multiple"
                                ng-model="restaurant.adding_image" 
                                name="restaurant.adding_image" 
                                ngf-select ngf-multiple="true" ng-change="uploadRestaurantImage()" title="Cambia" />
                            </div>
                            <h5 class="uppercase text-center" >Carica le immagini</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        
            <div class="menu_editing_save">
                <a ng-click="updateInfo()">Salva le modifiche</a>
            </div>
        </div>
        <!-- end edit restaurant -->