<!-- edit menu -->
        <div ng-if="current_tab == 2">
            <div class="white_container" ng-init="InitMenu()" ng-controller="EditMenuManagementCtrl">
                <div class="container">
                    <div ng-if="!restaurant.menu.menus || restaurant.menu.menus.length == 0" class="no_print">
                        <div class="hero_menu animated fadeIn text-center">
                            <h3>Ancora non hai aggiunto un menù al tuo ristorante!</h3>
                            <p><a class="btn ghost" ng-click="addMenu()">Crea il tuo primo menu</a></p>
                            <?php /*<p><a ng-click="integrateScloby('https://login.scloby.com/signin.php?client_id=<?php echo env('SCLOBY_CLIENT');?>&redirect_uri=<?php echo urlencode(env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/scloby/');?>')">O integra il tuo menu da scloby</a></p>*/?>
                        </div>
                    </div>
                    <div ng-if="restaurant.menu.menus && restaurant.menu.menus.length > 0" class="no_print">
                        <div class="hero_menu animated fadeIn text-center">
                            <h3>Modifica i menù del tuo ristorante</h3>

                            <p>Seleziona un menu da modificare o <a ng-click="addMenu()" class="text-warning">aggiungi un menu</a>
                            
                            <?php /*<p><a ng-click="integrateScloby('https://login.scloby.com/signin.php?client_id=<?php echo env('SCLOBY_CLIENT');?>&redirect_uri=<?php echo urlencode(env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/scloby/');?>')">O integra il tuo menu da scloby</a></p>*/?>
                            
                            <p>
                                <span class="fasce_span" 
                                ng-repeat="(menu_i, menu) in restaurant.menu.menus"
                                ng-click="setCurrentMenuOpen(menu_i)" 
                                ng-class="{'active': current_menu_viewed == menu_i}">
                                  {{menu.name}}
                                </span>
                            </p>       

                        </div>

                        <div class="menuManagement_container">
                            <div class="row">
                                <div class="col-md-8 col-sm-12 col-sm-push-12" ng-if="current_menu_management_action == 0" class="no_print">
                                    <?php require_once 'edit_menu.php';?>
                                </div>
                                <div class="col-md-8 col-sm-12 col-sm-push-12" ng-if="current_menu_management_action == 1">
                                    <?php require_once 'menu_management_print.php';?>
                                </div>
                                <div class="col-md-8 col-sm-12 col-sm-push-12" ng-if="current_menu_management_action == 2" class="no_print">
                                    <?php require_once 'publish_menu.php';?>
                                </div>
                                <div class="col-md-4 col-sm-12" class="no_print">
                                    <div class="order_menu_list" ng-if="restaurant.menu.menus.length > 1">
                                        <p>Ordina i menu del tuo ristorante</p>
                                        <ul class="list-unstyled accept_menus" data-as-sortable="menusDrag.dragMenusListeners" data-ng-model="restaurant.menu.menus">
                                            <li ng-repeat="m in restaurant.menu.menus" data-as-sortable-item class="dragging_menu_list">
                                                <label data-as-sortable-item-handle>{{m.name}}</label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="switch_btn">
                                        <a class="switch_button" ng-class="{'active':current_menu_management_action == 0}" ng-click="current_menu_management_action = 0">modifica</a>
                                        <a class="switch_button" ng-class="{'active':current_menu_management_action == 1}" ng-click="current_menu_management_action = 1">stampa</a>
                                        <a class="switch_button" ng-class="{'active':current_menu_management_action == 2}" ng-click="current_menu_management_action = 2">pubblica</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="menu_editing_save">
                            <a ng-click="updateMenu()">Salva le modifiche</a>
                        </div>

                        <?php /*
                        <div class="menu_container">
                            <div class="current_menu_viewed">                           
                                <div class="text-center margin-big-vertical">
                                    <h3 class="edit_parent">
                                    {{restaurant.menu.menus[current_menu_viewed].name}}
                                    </h3>
                                    
                                    <a class="btn btn-sm btn-default adding_btn" ng-click="addCategoryToMenu()">
                                        <i class="mebi-android-add"></i>
                                        Aggiungi una categoria</a>
                                    <a class="btn btn-sm btn-danger adding_btn" ng-click="deleteCurrentMenu()">
                                        <i class="mebi-close"></i>
                                        Cancella il menu</a>
                                </div>

                                <div ng-if="restaurant.menu.menus[current_menu_viewed].menu_type == 1">
                                    <div class="form-group">
                                        <label>Prezzo (menu fisso) €</label>
                                        <input type="text"                                  
                                            ng-model="restaurant.menu.menus[current_menu_viewed].menu_price" 
                                            class="form-control price-input animated bounceIn" pformat prefix="" cents='.' thousands=''/>
                                    </div>
                                </div>
                                <hr />
                                <h2>Categorie del menù</h2>

                                <div class="category_block" 
                                ng-repeat="(i_cat,cat) in restaurant.menu.menus[current_menu_viewed].categorie" 
                                id="menu_{{current_menu_viewed}}_category_{{i_cat}}">
                                    <h3 class="edit_parent category_list_view">
                                        {{cat.name}}
                                        <span 
                                        ng-repeat="categoria in restaurant.menu.menus[current_menu_viewed].categorie track by $index" 
                                        ng-click="scrollTo('menu_'+current_menu_viewed+'_category_'+$index)"
                                        ng-if="$index != i_cat">
                                            {{categoria.name}}
                                        </span>
                                        <br />
                                        <div class="text-center margin-big-v highlighted">
                                            <a class="btn btn-sm btn-default adding_btn" ng-click="addPlateToCategory(i_cat)">
                                                <i class="mebi-android-add"></i>
                                                Aggiungi un piatto
                                            </a>
                                            <a class="btn btn-sm btn-danger adding_btn" ng-click="deleteCurrentCategory(i_cat)">
                                                <i class="mebi-close"></i>
                                                Cancella</a>
                                        </div>
                                    </h3>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Nome della categoria</label><br />                                               
                                                <input type="text" ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].name" 
                                                    class="simple_border_input form-control plate_name" 
                                                    />  
                                                
                                            </div>
                                        </div>                                  
                                    </div>
                                    <hr />
                                    <h2>I piatti</h2>
                                    <div class="sort_plates"
                                    html-sortable="sortable_plates" ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti"
                                    >
                                        <div class="single_plate_container" 
                                            ng-repeat="(i_piatto, piatto) in restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti"
                                            id="menu_{{current_menu_viewed}}_category_{{i_cat}}_plate_{{i_piatto}}"
                                            >
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <!-- image -->
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <!-- icona trascinamento -->
                                                            <i class="mebi-cd-icon-select move_sort_icon"></i>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="plate_image_container">
                                                                <input class="upload-input upload_in_plate" type="file" ngf-select 
                                                                        ng-model="add_plate_image.image" 
                                                                        name="add_plate_image" 
                                                                        ng-change="uploadPlateImage(i_cat,i_piatto)" title="Immagine" />
                                                                <i class="mebi-technology-1 add_plate_icon" ng-if="!piatto.image"></i>
                                                                <img ng-src="{{staticURL}}/menu/small/{{piatto.image}}.png" alt="" title="Cambia immagine" ng-if="piatto.image" class="single_edit_plate_image" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12 marginated_top">
                                                            <a class="btn btn-sm btn-danger adding_btn" ng-click="deleteCurrentPlate(i_cat,i_piatto)">
                                                            <i class="mebi-close"></i>
                                                            Cancella il piatto</a>
                                                        </div>
                                                    </div>
                                                    
                                                
                                                </div>
                                                <div class="col-sm-2">
                                                    <!-- name -->
                                                    <div class="form-group">
                                                        <label>Nome</label>                                             
                                                        <textarea msd-elastic 
                                                            ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].name" 
                                                            class="simple_border_input form-control plate_name" 
                                                            ></textarea>  
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <!-- description, ingredienti, allergeni -->
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Descrizione</label>
                                                                <textarea msd-elastic 
                                                                ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].description"
                                                                class="form-control"
                                                                ></textarea>  
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Ingredienti</label><br />
                                                                <tagger 
                                                                ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].ingredienti" />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Allergeni</label><br />
                                                                <tagger 
                                                                ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].allergeni" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <!-- prezzo -->
                                                    <div class="form-group">
                                                        <label>Prezzo</label>
                                                        € <input type="text" ng-if="!restaurant.menu.menus[current_menu_viewed].menu_type || restaurant.menu.menus[current_menu_viewed].menu_type == 0" 
                                                        ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].price" 
                                                        class="form-control price-input animated bounceIn" pformat prefix="" cents='.' thousands=''/>
                                                    </div>
                                                </div>                                      
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                </div>
                            </div>
                        </div>
                        */ ?>
                    </div>
                </div>
            </div>
            <?php /* 
            <div class="menu_add_by_step custom_scrollbar" ng-class="{'open':editing_menu}">
                <i class="mebi-close menu_step_close" ng-click="closeEditing()"></i>
                <div class="content_menu_adding add_menu_name text-center" ng-if="current_menu_step == 1">
                    <!-- add menu -->
                    <h3 class="uppercase">
                        <span ng-if="!restaurant.menu.menus || restaurant.menu.menus.length == 0">Inizia dando un nome </span> 
                        <span ng-if="restaurant.menu.menus && restaurant.menu.menus.length > 0">Dai un nome </span> 
                            al tuo menù
                    </h3>
                    
                    <p class="text-primary">Scegline uno tra quelli consigliati</p>
                    
                    <p class="options_selections">
                        <a ng-click="setMenuName('Pranzo')" ng-if="!existMenuName('Pranzo')">Pranzo</a>
                        <a ng-click="setMenuName('Cena')" ng-if="!existMenuName('Cena')">Cena</a>
                        <a ng-click="setMenuName('Carta dei vini')" ng-if="!existMenuName('Carta dei vini')">Carta dei vini</a>
                    </p>

                    <p>O aggiungilo manualmente</p>
                    <input type="text" ng-model="add_menu.menu_name" autosize class="simple_border_input form-control" placeholder="Nome del menu" />
                    <hr />  
                    <div class="fixed_price" ng-class="{'fixed_selected': add_menu.menu_type == 1}">
                        <span class="uppercase">Menu a prezzo fisso</span>
                        <i class="mebi-check-1 check_fixed_price" ng-click="setMenuType()"></i>                     
                        <input type="text" ng-model="add_menu.menu_price" class="form-control price-input animated bounceIn" ng-if="add_menu.menu_type == 1" pformat prefix="€ " cents='.' thousands=''/>
                    </div>
                    <p><a class="btn ghost" ng-click="confirmMenuName()" ng-if="add_menu.menu_name != ''">Conferma</a></p>
                </div>
                <div class="content_menu_adding add_menu_category text-center" ng-if="current_menu_step == 2">
                    <!-- add category -->
                    <h3>{{restaurant.menu.menus[current_menu_index].name}}</h3>
                    <h3 class="uppercase" ng-if="(!restaurant.menu.menus[current_menu_index].categorie || restaurant.menu.menus[current_menu_index].categorie.length == 0) && restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">
                        Ora che hai aggiunto un nuovo menu, inserisci una categoria di riferimento per i piatti
                    </h3>
                    <h3 class="uppercase" ng-if="(restaurant.menu.menus[current_menu_index].categorie && restaurant.menu.menus[current_menu_index].categorie.length > 0) && restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">
                        Inserisci una categoria per i piatti del menu
                    </h3>
                    <h3 class="uppercase" ng-if="restaurant.menu.menus[current_menu_index].name == 'Carta dei vini'">
                        Inserisci una categoria per i vini
                    </h3>
                    <p ng-if="restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">Ad esempio 
                        <a class="text-primary" ng-click="setCategoryName('Antipasti')">"antipasti"</a>, 
                        <a class="text-primary" ng-click="setCategoryName('Primi')">"primi"</a>, 
                        <a class="text-primary" ng-click="setCategoryName('Secondi')">"secondi"</a>, 
                        <a class="text-primary" ng-click="setCategoryName('Contorni')">"contorni"</a>...
                    </p>
                    <p ng-if="restaurant.menu.menus[current_menu_index].name == 'Carta dei vini'">Ad esempio 
                        <a class="text-primary" ng-click="setCategoryName('Bianchi')">"bianchi"</a>, 
                        <a class="text-primary" ng-click="setCategoryName('Rossi')">"rossi"</a>...
                    </p>
                    <input type="text" ng-model="add_category.name" class="simple_border_input form-control" placeholder="Nome della categoria" />
                    <p><a class="btn ghost" ng-click="confirmCategoryName()" ng-if="add_category.category_name != ''">Conferma</a></p>
                </div>
                <div class="content_menu_adding text-center" ng-if="current_menu_step == 3">
                    <!-- add plate -->
                    <h3 class="uppercase" ng-if="restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">
                        Aggiungi un piatto
                    </h3>
                    <h3 class="uppercase" ng-if="restaurant.menu.menus[current_menu_index].name == 'Carta dei vini'">
                        Aggiungi il vino
                    </h3>

                    <p ng-if="!add_plate.image">Clicca sull'icona per aggiungere una immagine</p>
                    <div class="add_plate_image_container">
                        <input class="upload-input" type="file" ngf-select 
                                ng-model="add_plate_image.image" 
                                name="add_plate_image" 
                                ng-change="uploadPlateImage()" title="Immagine" />
                        <i class="mebi-technology-1 add_plate_icon" ng-if="!add_plate.image"></i>
                        <img ng-src="{{staticURL}}/menu/small/{{add_plate.image}}.png" alt="" title="" ng-if="add_plate.image" />
                    </div>

                    <p ng-if="restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">Nome del piatto *</p>
                    <p ng-if="restaurant.menu.menus[current_menu_index].name == 'Carta dei vini'">Etichetta *</p>
                    <input type="text" ng-model="add_plate.name" class="simple_border_input form-control plate_name" 
                    autosize />
                    
                    
                    <div class="row" ng-if="restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">
                    <hr />
                        <div class="col-sm-6">
                            <p>Gli ingredienti</p>
                            <tagger ng-model="add_plate.ingredienti" />
                        </div>
                        <div class="col-sm-6">
                            <p>Allergeni</p>
                            <tagger ng-model="add_plate.allergeni" />
                        </div>
                        <div class="clearfix"></div>
                    <hr />
                    </div>              
                    <p>Una descrizione 
                        <span ng-if="restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">del piatto</span>
                        <span ng-if="restaurant.menu.menus[current_menu_index].name == 'Carta dei vini'">del vino</span>
                    </p>
                    <textarea msd-elastic ng-model="add_plate.description"></textarea>  
                    <!-- prezzo -->
                    <div class="fixed_price" ng-if="!restaurant.menu.menus[current_menu_index].menu_type || restaurant.menu.menus[current_menu_index].menu_type == 0">
                        <span class="uppercase">Prezzo</span>
                        <input type="text" ng-model="add_plate.price" class="form-control price-input animated bounceIn" 
                        pformat prefix="€ " cents='.' thousands=''/>
                    </div>
                    <p>* I campi con asterisco sono obbligatori</p>
                    <p><a class="btn ghost" ng-click="insertPlate()" ng-if="add_plate.name != ''">Conferma</a></p>
                </div>
            </div>
            */ ?>
        </div>
        <!-- end edit menu -->