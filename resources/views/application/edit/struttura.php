<!-- edit struttura -->
        <div ng-if="current_tab == 3">
            <div class="white_container" ng-init="InitStructure()" ng-controller="EditStrutturaCtrl">
                <div class="container">
                    <div class="col-sm-6">
                        <h3 class="uppercase spacing size-big pad-big-v">Tempo tra le prenotazioni</h3>
                        <p class="size-small">Seleziona i minuti tra una prenotazione e la successiva</p>                    
                            
                        <span class="fasce_span" ng-click="setTimeShift(15)" ng-class="{'active': restaurant.time_shift == 15}">
                          15
                        </span>
                        <span class="fasce_span" ng-click="setTimeShift(30)" ng-class="{'active': restaurant.time_shift == 30}">
                          30
                        </span>
                        <span class="fasce_span" ng-click="setTimeShift(60)" ng-class="{'active': restaurant.time_shift == 60}">
                          60
                        </span>
                        
                    </div>
                    <div class="col-sm-6">                  
                        <h3 class="uppercase spacing size-big pad-big-v">Aggiungi una sala</h3>
                        <div class="form-group">
                            <label>Nome della sala</label>
                            <input type="text" ng-model="add_struttura_children.sala" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Posti disponibili</label>
                            <input type="number" ng-model="add_struttura_children.posti" class="form-control" min=1 />
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-outline btn-sm" ng-click="add_sala()">Aggiungi una sala</button>
                        </div>                                
                    </div>
                    <div class="clearfix"></div>
                    <hr />
                    <div class="col-lg-12 col-md-12">

                        <h3 class="uppercase spacing size-big pad-big-v">Le sale</h3>
                        <p>Seleziona la sala da modificare</p>
                        
                        <span class="fasce_span" ng-repeat="sala in restaurant.struttura.sale track by $index" 
                        ng-click="setCurrentSala($index)" 
                        ng-class="{'active': current_sala == $index}">
                          {{sala.name}}
                        </span>
                        <div class="clearfix"></div>
                        
                        <div  ng-if="restaurant.struttura.sale.length > 0 && current_sala != null">
                            <p class="text-center">Modifica il nome della sala cambiando il testo qui in basso</p>
                            <h3 style="font-size: 28px; padding: 17px 0;" class="text-center uppercase spacing size-medium margin-medium-v" contenteditable 
                            ng-model="restaurant.struttura.sale[current_sala].name" class="" aria-invalid="false" style="">
                            </h3>
                            <p class="text-center"><b>POSTI DISPONIBILI</b></p>
                            <input type="number" class="input-hidden-editable sala_input" ng-model="restaurant.struttura.sale[current_sala].posti" min=1 />
                        </div>

                        <ul 
                        data-as-sortable="tableDrag.dragTablesListeners" 
                        data-ng-model="restaurant.struttura.sale[current_sala].tables">
                            <li 
                            data-as-sortable-item
                            id="tavolo_{{table.num}}_sala_{{$index}}"
                            class="col-md-3 col-sm-4 col-xs-12" 
                            ng-repeat="(i_table,table) in restaurant.struttura.sale[current_sala].tables" 
                            style="height: 238px;">
                              <div 
                                data-as-sortable-item-handle
                                class="single_table" >
                                <span class="table_num">{{table.num}}</span>
                                <span class="table_posti">x{{table.posti}}</span>
                              </div>
                              
                              <div class="text-center table_actions">
                                <i class="mebi-close text-danger" ng-click="removeTable(current_sala,i_table)"></i>
                              </div>
                            </li>
                        </ul>

                        <div class="clearfix"></div>

                        <p class="text-center" ng-if="restaurant.struttura.sale.length > 0 && current_sala != null">
                            <a ng-click="addTableDialog()" class="btn btn-primary ghost">Aggiungi un tavolo</a>
                        </p>

                        <p class="text-center" ng-if="restaurant.struttura.sale.length > 0 && current_sala != null">
                            <a ng-click="removeSala()" class="btn btn-danger pull-right">Cancella questa sala</a>
                        </p>

                    </div>
                </div>
            
                <div class="menu_editing_save">
                    <a ng-click="updateStructure()">Salva le modifiche</a>
                </div>
            </div>
        </div>
        <!-- end edit struttura -->