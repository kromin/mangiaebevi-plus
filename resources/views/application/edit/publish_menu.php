<div class="menu_publish" ng-controller="MenuPublishingCtrl">
    <?php /*
	<div class="col-sm-12" ng-init="initPlans()" 
        ng-if="!view_user.billing_plan || view_user.billing_plan.length == 0 || view_user.billing_plan.active != 1">
        <p class="text-center">Per poter aggiungere il widget sul tuo sito o nella tua pagina facebook devi iscriverti ad uno dei piani di abbonamento di Menoo</p>
        <div class="col-sm-4" ng-repeat="plan in pacchetti">
            <div class="plan_table" ng-class="{'featured': plan.featured == 1}">
                <div class="plan_header">
                    <h3 class="plan_price text-center" ng-if="selected_plan_period == 0">{{plan.price_month}} €</h3>
                    <h3 class="plan_price text-center" ng-if="selected_plan_period == 1">{{plan.price_annual}} €</h3>
                    / {{returnCicleText(selected_plan_period)}}
                </div>
                <div ng-if="plan.period == 2" class="plan_fatturazione text-center">
                    <button ng-click="setPlanPeriod(0)" ng-class="{'text-primary':selected_plan_period==0}">mensile</button>
                    <button ng-click="setPlanPeriod(1)" ng-class="{'text-primary':selected_plan_period==1}">annuale</button>
                </div>
                <div class="plan_services">
                    <ul class="list-unstyled">
                        <li ng-repeat="serviceplan in plan.services" class="text-center">{{serviceplan}}</li>
                    </ul>
                </div>
                <div class="plan_button text-center">
                    <button ng-click="submitPlan(plan._id)" class="outline_btn bprimary">Conferma</button>
                </div>
            </div>

        </div>
    </div>
    */?>
    <div <?php if (auth()->user()->role < 3) : ?><?php endif; //ng-if="view_user.billing_plan && view_user.billing_plan.length >= 0 || view_user.billing_plan.active == 1" ?>>
    	<div class="minimal_tabs no_print">
	        <ul class="list-unstyled">
	            <li ng-class="{'active' : !current_publish_action || current_publish_action == 0}">
	            	<a ng-click="publishAction(0)">Sul tuo sito</a>
	            </li>
	            <li ng-class="{'active' : current_publish_action == 1}">
	            	<a ng-click="publishAction(1)">Su facebook</a>
	            </li>                           
	        </ul>
	    </div>
    	<div ng-show="current_publish_action == 0 || !current_publish_action">
	    	<div class="row">
		    	<div class="col-sm-6" ng-init="initWidget()">
		            <p>Per poter inserire i tuoi menu sul tuo sito web aggiungine il domino (www.nomedeltuosito.it)</p>
		            <form id="addIframeForm">
		                <div class="form-group">
		                    <label for="url">Dominio del sito web</label>
		                    <input type="text" name="url" ng-model="add_iframe.url" class="activated_input full" required/>
		                </div>
		                
		            	<button type="submit" class="outline_btn bprimary" ng-disabled="addIframeForm.$invalid" 
		            	ng-click="addIframe()">Aggiungi</button>
		            	              
		            </form>
		        </div>        
		        <div class="col-sm-6" ng-if="hasiframes == 1">
			        <h4>I tuoi Domini</h4>
			        <ul class="list-unstyled">
			            <li ng-repeat="u in iframe.url track by $index">{{u}}</li>
			        </ul>
			    </div>
			</div>
		    <div class="clearfix"></div>
	        <hr />
	        <div class="col-sm-12" ng-if="iframe">                                        
	            <h3>Codice da incorporare</h3>
	            <p>Copia e incolla questo codice nel tuo sito web per aggiungere il widget di visualizzazione del menu</p>
	            <pre>&lt;div id="mangiaebevi_restaurant_iframe_menu"&gt;&lt;/div&gt;<br />&lt;script type="text/javascript" src="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/static/{{iframe.token}}/menu/frameloader"&gt;&lt;/script&gt;
	            </pre>
	            <hr />

	            <h3 class="uppercase size-medium">Esempio di visualizzazione del widget</h3>
	            <div id="mangiaebevi_restaurant_iframe_menu"></div>
	            <script type="text/javascript" src="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/static/test/menu/frameloader"></script>
	            
	        </div>   
	    </div>
	    <div ng-show="current_publish_action == 1">
            <div class="row">
                <div class="col-sm-6" ng-if="!restaurant.facebook_page_id || restaurant.facebook_page_id == ''">
                    <p>Per aggiungere la prenotazione e il menu sulla tua pagina facebook prima associa 
                        l'ID della pagina al tuo ristorante e poi clicca sul pulsante "Pubblica su facebook"
                    </p>
                    <p><a href="https://it.wix.com/support/html5/article/come-faccio-a-trovare-lid-della-pagina-facebook" target="_blank" rel="nofollow">come trovare l'ID della tua pagina facebook</a></p>
                    <div class="form-group">
                        <label for="fb_url">Inserisci l'ID numerico della pagina facebook</label>
                        <input type="text" id="fb_url" name="fburl" ng-model="add_facebook_page.page_id" class="activated_input full" required/>
                    </div>                                                
                    <button type="submit" class="outline_btn bprimary" ng-disabled="addIframeForm.$invalid" 
                    ng-click="addFacebookID(add_facebook_page.page_id)">Aggiungi</button>                  
                </div> 
                <div class="col-sm-6" ng-if="restaurant.facebook_page_id && restaurant.facebook_page_id != ''">
                    <h3>ID della tua pagina facebook: {{restaurant.facebook_page_id}}</h3>
                    <p><a class="text-danger" ng-click="removeFacebookPage()">rimuovi</a></p>
                </div>       
                <div class="col-sm-6">
                    <a class="facebook_btn" ng-click="enableFacebook()"><i class="fa fa-facebook"></i> Pubblica su facebook</a>
                </div>
            </div>            
        </div>
    </div>
</div>