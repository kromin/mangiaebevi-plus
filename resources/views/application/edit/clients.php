<div ng-if="current_tab == 6">
	<div class="menu_publish" ng-controller="MenuPublishingCtrl">
		<?php /*<div class="container plan_container">
			<div class="col-sm-12" ng-init="initPlans()"
		        ng-if="!view_user.billing_plan || view_user.billing_plan.length == 0 || view_user.billing_plan.active != 1">
		        <p class="text-center">Per poter aggiungere e gestire il tuo database di clienti devi iscriverti ad uno dei piani di abbonamento di Menoo</p>
		        <div class="col-sm-4" ng-repeat="plan in pacchetti">
		            <div class="plan_table" ng-class="{'featured': plan.featured == 1}">
		                <div class="plan_header">
		                    <h3 class="plan_price text-center" ng-if="selected_plan_period == 0">{{plan.price_month}} €</h3>
		                    <h3 class="plan_price text-center" ng-if="selected_plan_period == 1">{{plan.price_annual}} €</h3>
		                    / {{returnCicleText(selected_plan_period)}}
		                </div>
		                <div ng-if="plan.period == 2" class="plan_fatturazione text-center">
		                    <button ng-click="setPlanPeriod(0)" ng-class="{'text-primary':selected_plan_period==0}">mensile</button>
		                    <button ng-click="setPlanPeriod(1)" ng-class="{'text-primary':selected_plan_period==1}">annuale</button>
		                </div>
		                <div class="plan_services">
		                    <ul class="list-unstyled">
		                        <li ng-repeat="serviceplan in plan.services" class="text-center">{{serviceplan}}</li>
		                    </ul>
		                </div>
		                <div class="plan_button text-center">
		                    <button ng-click="submitPlan(plan._id)" class="outline_btn bprimary">Conferma</button>
		                </div>
		            </div>
		        </div>
		        <div class="clearfix"></div>
		    </div>
		</div>*/
		?>
	    <div class="clients_container" <?php if (auth()->user()->role < 3) : ?><?php endif; /*ng-if="view_user.billing_plan && view_user.billing_plan.length >= 0 || view_user.billing_plan.active == 1"*/?>>
	    	<div class="container">
		    	<div class="row" ng-controller="ClientsRestaurantCtrl">
		    		<div class="col-md-6" ng-init="InitRestaurantClients(restaurant._id)">

		    			<h3>I tuoi Clienti
		    			<span class="pull-right">
		    				<i class="fa fa-list" ng-class="{'text-warning' : current_visualizzazione == 1}" aria-hidden="true" ng-click="setVisualizzazione(1)"></i>
		    				<i class="fa fa-th-list" ng-class="{'text-warning' : current_visualizzazione == 2}" aria-hidden="true" ng-click="setVisualizzazione(2)"></i>

		    			</span>
		    			</h3>
		    			<div class="row">
			    			<form id="searchClientForm" ng-submit="search()">
								<div class="form-group col-md-8">
									<input type="text" class="form-control" placeholder="Nome, cognome, email, telefono" ng-model="search_input" required />
								</div>
								<div class="form-group col-md-4">
									<button type="submit" style="padding: 14px;" ng-disabled="searchClientForm.$invalid" class="btn btn-primary">Cerca</button>
								</div>
							</form>
			    		</div>
			    		<p ng-if="filtered_results == 2">Risultato per la ricerca: {{search_input}} <a ng-click="annullaFiltro()" class="text-danger">x annulla</button></a></p>
			    		<div class="clearfix"></div>
		    			<div class="user_list_with_avatar" ng-if="current_visualizzazione == 1">
		    				<ul class="list-unstyled">
		    					<li class="with_bg selectable" ng-repeat="(i_client, client) in viewed_clients track by $index" ng-click="selectUser(client)">
		    						<div class="image_avatar">
		    							<img ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/users/small/{{client.user.avatar}}.png" alt="" title="" ng-if="client.user.avatar && client.user.avatar != ''" />
		    							<img ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_DOMAIN');?>/img/default-avatar.png" alt="" title="" ng-if="!client.user.avatar || client.user.avatar == ''" />
		    						</div>
		    						<div class="content_element">
		    							<h3 class="main_text" ng-if="client.user">{{client.user.name}} {{client.user.surname}}</h3>
		    							<p class="subtext" ng-if="client.user">
		    							<span><b>Email: </b> {{client.user.email}} </span><br />
		    							<span ng-if="client.user && client.user.phone && client.user.phone != ''"><b>Mobile: </b> {{client.user.phone}}</span>
		    							</p>
		    							<h3 class="main_text" ng-if="!client.user">{{client.name}} {{client.surname}}</h3>
		    							<p class="subtext" ng-if="!client.user">
			    							<span ng-if="client.email && client.email != ''"><b>Email: </b>{{client.email}} </span><br/>
			    							<span ng-if="client.phone && client.phone != ''"><b>Mobile: </b> {{client.phone}}</span>
		    							</p>
		    						</div>
		    						<div class="clearfix"></div>
		    					</li>
		    				</ul>
		    			</div>
		    			<div class="pagination" ng-if="pages.length > 1 && current_visualizzazione == 1">
		    				<ul class="list-unstyled">
		    					<li ng-repeat="p in pages" ng-class="{'active' : current_page == p}" ng-click="setPage(p)">
		    						{{p}}
		    					</li>
		    				</ul>
		    			</div>
		    			<div class="table-responsive" ng-if="current_visualizzazione == 2">
		    				<table class="table">
		    					<thead>
		    						<tr>
		    							<th>Nome</th>
		    							<th>Cognome</th>
		    							<th>Email</th>
		    							<th>Telefono</th>
		    						</tr>
		    					</thead>
		    					<tbody>
		    						<tr ng-class="{'active' : current_client._id && current_client._id == client._id}" ng-repeat="client in clients" ng-click="selectUser(client)">
		    							<td>{{client.name}}</td>
		    							<td>{{client.surname}}</td>
		    							<td>{{client.email}}</td>
		    							<td>{{client.phone}}</td>
		    						</tr>
		    					</tbody>
		    				</table>
		    				<a href="/restaurant_clients/{{restaurant._id}}/" target="_blank" class="btn btn-primary">Esporta</a>
		    			</div>
		    		</div>
		    		<div class="col-md-6">
		    			<h3 ng-if="current_client == 0">Crea Cliente</h3>
		    			<div class="" ng-if="current_client == 0">
		    				<div class="row">
			    				<form id="addClientForm" ng-submit="confirmAdd()">
									<div class="form-group col-md-6">
										<label>Nome *</label>
										<input type="text" class="form-control" placeholder="Nome" ng-model="add_client.name" required />
									</div>
									<div class="form-group col-md-6">
										<label>Cognome *</label>
										<input type="text" class="form-control" placeholder="Cognome" ng-model="add_client.surname" required />
									</div>
									<div class="clearfix"></div>
									<div class="form-group col-md-12">
										<label>Email</label>
										<input type="email" class="form-control" placeholder="Email" ng-model="add_client.email" />
									</div>
									<div class="form-group col-md-12">
										<label>Mobile</label>
										<input type="text" class="form-control" placeholder="Mobile" ng-model="add_client.phone" />
									</div>
									<div class="form-group col-md-12">
										<label>Telefono</label>
										<input type="text" class="form-control" placeholder="Telefono" ng-model="add_client.phone_" />
									</div>
									<div class="form-group col-md-12">
										<label>Indirizzo</label>
										<input type="text" class="form-control" placeholder="Indirizzo" ng-model="add_client.address"  />
									</div>
									<div class="form-group col-md-8">
										<label>Città</label>
										<input type="text" class="form-control" placeholder="Città" ng-model="add_client.city"  />
									</div>
									<div class="form-group col-md-4">
										<label>Provincia</label>
										<input type="text" class="form-control" placeholder="PR" ng-model="add_client.pr" max="2" />
									</div>
									<div class="clearfix"></div>
									<div class="form-group col-md-6">
										<label>Cap</label>
										<input type="text" class="form-control" placeholder="Cap" ng-model="add_client.cap" />
									</div>
									<div class="form-group col-md-6">
										<label>Stato</label>
										<input type="text" class="form-control" placeholder="Stato" ng-model="add_client.state" />
									</div>
									<div class="clearfix"></div>
									<div class="form-group col-md-8">
										<label>Data di nascita</label>
										<input class="form-control" mask="39/19/2999" ng-model="add_client.dt" type="text" placeholder="GG/MM/AAAA"
		                                 />
									</div>
									<div class="form-group col-md-4">
										<label>Lingua</label>
										<input type="text" class="form-control" placeholder="Italiano" ng-model="add_client.language" max=2 />
									</div>
									<div class="clearfix"></div>
									<div class="form-group col-md-12">
										<label>Allergie</label>
										<div class="row">
											<div class="col-sm-6" ng-repeat="(i,allergene) in allergeni">
												<label class="label--checkbox block">
							                    <input class="checkbox" type="checkbox"
							                    ng-checked="add_client.allergeni.indexOf('{{allergene}}') !== -1"
							                    ng-click="addAllergeneToClient(i)"
							                    id="allergene_{{i}}_checked" > {{allergene}}
							                	</label>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<label>Preferenze</label><br />
										<tagger
	                                    ng-model="add_client.preferenze" />
									</div>
									<div class="clearfix"></div>
									<div class="form-group col-md-12">
										<label>Note</label>
										<textarea class="form-control" ng-model="add_client.note"></textarea>
									</div>
									<div class="clearfix"></div>
									<div class="form-group col-md-12">
										<button type="submit" ng-disabled="addClientForm.$invalid" class="btn btn-primary">Conferma</button>
									</div>
									<div class="col-md-12">
									<p><small>I campi con asterisco sono obbligatori, se l'indirizzo email inserito appartiene ad un utente già registrato su mangiaebevi+ il sistema lo identificherà automaticamente</small></p>
									</div>
								</form>
							</div>
		    			</div>

		    			<p ng-if="current_client != 0">
			    			<a ng-click="addClientForm(1)" class="add_client_btn">
			    				<i class="mebi-android-add"></i> Aggiungi un cliente
			    			</a>
			    		</p>
			    		<h3 ng-if="current_client != 0">Scheda di {{current_client.user.name}} {{current_client.user.surname}}
			    			<span class="pull-right">
			    				<a class="hint--bottom" aria-label="Stampa la scheda" href="/scheda_cliente/{{restaurant._id}}/{{current_client._id}}/" target="_blank"><i class="fa fa-print text-primary" aria-hidden="true"></i></a>
			    			</span>
			    		</h3>

		    			<div class="scheda_cliente" ng-if="current_client != 0">
		    				<div class="row">
			    				<div class="col-md-4">
			    					<div class="image_avatar">
				    					<img ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/users/small/{{current_client.user.avatar}}.png" alt="" title="" ng-if="current_client.user.avatar && current_client.user.avatar != ''" />
				    					<img ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_DOMAIN');?>/img/default-avatar.png" alt="" title="" ng-if="!current_client.user.avatar || current_client.user.avatar == ''" />
				    				</div>
			    				</div>
			    				<div class="col-md-8">
				    				<div class="text-left" ng-if="current_client.user">
		    							<h3 class="main_text"><a href="/u/{{current_client.user._id}}/" target="_blank" title="Profilo">{{current_client.user.name}} {{current_client.user.surname}}</a></h3>
		    							<p class="subtext">{{current_client.user.email}} - {{current_client.user.phone}}</p>
		    							<p class="text-primary" ng-if="current_client.infos.reccomended == 1">Ha raccomandato il tuo ristorante</p>
    									<p class="text-danger" ng-if="current_client.infos.reccomended == 0">Non ha raccomandato il tuo ristorante</p>
		    						</div>
		    						<div class="text-left" ng-if="!current_client.user">
		    							<h3 class="main_text">{{current_client.name}} {{current_client.surname}}</h3>
		    							<p class="subtext">{{current_client.email}} - {{current_client.phone}}</p>
		    						</div>
		    					</div>
		    				</div>
	    					<div class="clearfix"></div>
    						<hr />
    						<div class="row">
    							<div class="col-lg-3 col-md-6 col-sm-6">
    								<p class="number_reservation">{{current_client.infos.reservations}}</p>
    								<p class="p_reservations">Prenotazioni</p>
    							</div>
    							<?php /*<!--<div class="col-lg-3 col-md-6 col-sm-6">
    								<p class="number_reservation">{{current_client.infos.confirmed_reservations}}</p>
    								<p class="p_reservations">Prenotazioni confermate</p>
    							</div>-->*/?>
    							<div class="col-lg-3 col-md-6 col-sm-6">
    								<p class="number_reservation">{{current_client.infos.discarded_reservations}}</p>
    								<p class="p_reservations">No shows</p>
    							</div>
    							<div class="col-lg-3 col-md-6 col-sm-6">
    								<p class="number_reservation">{{current_client.infos.preorders}}</p>
    								<p class="p_reservations">Preordini</p>
    							</div>
    							<div class="col-lg-3 col-md-6 col-sm-6">
    								<p class="reservation_date">{{current_client.infos.last}}</p>
    								<p class="p_reservations">Ultima prenotazione</p>
    							</div>
    						</div>
    						<hr />
    						<div class="text-left">
    							<p ng-if="current_client.allergie && current_client.allergie.length > 0 && (!current_client.user.allergeni || current_client.user.allergeni.length <= 0)"><b>Allergie:</b> <span ng-repeat="(indice, allergene) in current_client.allergie">{{allergene}}<i style="font-style: normal;" ng-if="indice < (current_client.allergie.length-1)">, </i></span></p>
    							<!-- allergie utente -->
    							<p ng-if="current_client.user.allergeni && current_client.user.allergeni.length > 0"><b>Allergie:</b> <span ng-repeat="(indice, allergene) in current_client.user.allergeni">{{allergene}}<i style="font-style: normal;" ng-if="indice < (current_client.user.allergeni.length-1)">, </i></span></p>
    							<p ng-if="current_client.preferenze && current_client.preferenze.length > 0"><b>Preferenze:</b> <span ng-repeat="(indice, preferenza) in current_client.preferenze">{{preferenza}}<i style="font-style: normal;" ng-if="indice < (current_client.preferenze.length-1)">, </i></span></p>
    							<p><b>Note:</b> {{current_client.note}}</p>
    						</div>
		    			</div>
		    			<div class="text-left" ng-if="current_client != 0">
		    				<h3>Scheda di {{current_client.name}} {{current_client.surname}} </h3>
							<div class="row">
								<form id="editClientForm" ng-submit="confirmEdit()">
									<div class="form-group col-md-6">
										<label>Nome *</label>
										<input type="text" class="form-control" placeholder="Nome" ng-model="current_client.name" required />
									</div>
									<div class="form-group col-md-6">
										<label>Cognome *</label>
										<input type="text" class="form-control" placeholder="Cognome" ng-model="current_client.surname" required />
									</div>
									<div class="clearfix"></div>
									<div class="form-group col-md-12">
										<label>Email</label>
										<input type="email" class="form-control" placeholder="Email" ng-model="current_client.email" />
									</div>
									<div class="form-group col-md-12">
										<label>Mobile</label>
										<input type="text" class="form-control" placeholder="Mobile" ng-model="current_client.phone" />
									</div>
									<div class="form-group col-md-12">
										<label>Telefono</label>
										<input type="text" class="form-control" placeholder="Telefono" ng-model="current_client.phone_" />
									</div>
									<div class="form-group col-md-12">
										<label>Indirizzo</label>
										<input type="text" class="form-control" placeholder="Indirizzo" ng-model="current_client.address"  />
									</div>
									<div class="form-group col-md-8">
										<label>Città</label>
										<input type="text" class="form-control" placeholder="Città" ng-model="current_client.city"  />
									</div>
									<div class="form-group col-md-4">
										<label>PR</label>
										<input type="text" class="form-control" placeholder="PR" ng-model="current_client.pr" max="2" />
									</div>
									<div class="clearfix"></div>
									<div class="form-group col-md-6">
										<label>Cap</label>
										<input type="text" class="form-control" placeholder="Cap" ng-model="current_client.cap" />
									</div>
									<div class="form-group col-md-6">
										<label>Stato</label>
										<input type="text" class="form-control" placeholder="Stato" ng-model="current_client.state" />
									</div>
									<div class="clearfix"></div>
									<div class="form-group col-md-8">
										<label>Data di nascita</label>
										<input class="form-control" mask="39/19/2999" ng-model="current_client.dt" type="text" placeholder="GG/MM/AAAA"
		                                 />
									</div>
									<div class="form-group col-md-4">
										<label>Lingua</label>
										<input type="text" class="form-control" placeholder="Italiano" ng-model="current_client.language" max=2 />
									</div>
									<div class="clearfix"></div>
									<div class="form-group col-md-12">
										<label>Allergie</label>
										<p><small>Le allergie qui di seguito potrebbero differire da quelle presenti nella scheda, nel caso in cui l'utente sia registrato sulla piattaforma in alto vengono mostrate le allergie che ha impostato nel suo profilo mentre qui di seguito quelle inserite da te, hai la possibilità di far corrispondere i dati selezionando le allergie seguenti</small></p>
										<label class="label--checkbox block" ng-repeat="(i,allergene) in allergeni">
					                    <input class="checkbox" type="checkbox"
					                    ng-checked="current_client.allergie.indexOf('{{allergene}}') !== -1"
					                    ng-click="addAllergeneToCurrentClient(i)"
					                    id="allergene_{{i}}_checked" > {{allergene}}
					                	</label>
									</div>
									<div class="form-group col-md-12">
										<label>Preferenze</label><br />
										<tagger
	                                    ng-model="current_client.preferenze" />
									</div>
									<div class="clearfix"></div>
									<div class="form-group col-md-12">
										<label>Note</label>
										<textarea class="form-control" ng-model="current_client.note"></textarea>
									</div>
									<div class="clearfix"></div>
									<div class="form-group col-md-12">
										<button type="submit" ng-click="removeCurrentClient()" class="btn btn-danger">Elimina</button>
										<button type="submit" ng-disabled="editClientForm.$invalid" class="btn btn-primary">Salva modifiche</button>
									</div>
								</form>
							</div>
						</div>
		    		</div>
		    	</div>
		    </div>
	    </div>
	</div>
</div>