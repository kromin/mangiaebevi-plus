<div ng-if="((restaurant.menu.menus.length)-1) >= current_menu_viewed">
    <h3>
        <label>Nome menu</label>
        <input type="text"                                  
            ng-model="restaurant.menu.menus[current_menu_viewed].name" 
            class="form-control" />
    </h3>
    <label class="label--checkbox block">
        <input 
        class="checkbox" 
        type="checkbox" 
        name="settingMenuOrderReady[]" 
        ng-checked="restaurant.menu.menus[current_menu_viewed].orderready && restaurant.menu.menus[current_menu_viewed].orderready == 1" 
        ng-click="setOrderReady()">
        Permetti le pre-ordinazioni
    </label> <br />
    
    <label class="label--checkbox block">
        <input 
        class="checkbox" 
        type="checkbox" 
        name="settingMenuType[]" 
        ng-checked="restaurant.menu.menus[current_menu_viewed].menu_type == 1" 
        ng-click="setMenuType()">
        Menu a prezzo fisso
    </label> 
    
    
    <div ng-if="restaurant.menu.menus[current_menu_viewed].menu_type == 1">
        <div class="form-group">
            <label>Prezzo (menu fisso) €</label>
            <input type="text"                                  
                ng-model="restaurant.menu.menus[current_menu_viewed].menu_price" 
                class="form-control price-input" pformat prefix="" cents='.' thousands=''/>
        </div>    
    </div>
    <div class="form-group">
        <label>Descrizione opzionale del menu</label>
        <textarea msd-elastic 
        ng-model="restaurant.menu.menus[current_menu_viewed].pre_text" 
        class="simple_border_input form-control plate_name" 
        ></textarea>  
    </div>
    <a class="management_btn text-danger" ng-click="deleteCurrentMenu()">cancella il menu</a>
    <div class="clearfix"></div>
    <hr class="dashed" />
    <a class="management_btn" ng-click="prependCategoryToMenu()"><i class="mebi-android-add add_icon_inline"></i> aggiungi una categoria</a>
    <div class="clearfix"></div>
    <ul class="list-unstyled" data-as-sortable="catsDrag.dragCatsListeners" data-ng-model="restaurant.menu.menus[current_menu_viewed].categorie">
        <li data-as-sortable-item
            ng-repeat="(i_cat,cat) in restaurant.menu.menus[current_menu_viewed].categorie" 
            id="menu_{{current_menu_viewed}}_category_{{i_cat}}" >
            <div class="category_block" 
                ng-class="{'open' : isOpen(i_cat)}" 
                >                                                                        
                <div class="category_name">
                    <h4>
                        <i class="fa fa-arrows-v" data-as-sortable-item-handle></i>
                        <i class="mebi-down-open-big rotate_up" ng-click="openCat(i_cat)" ng-class="{'up' : isOpen(i_cat)}"></i>
                        <span 
                        contenteditable 
                        ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].name">
                        {{restaurant.menu.menus[current_menu_viewed].categorie[i_cat].name}}</span>
                        <a class="pull-right text-danger size_small" ng-click="removeCategory(i_cat)">Cancella</a>
                    </h4>                
                </div>            
                <div class="category_block_content">  
                    <a class="add_plate_row" ng-click="prependPlate(i_cat)">
                        <i class="mebi-android-add"></i>
                         aggiungi un piatto
                    </a>
                    <ul class="list-unstyled" data-as-sortable="plates.dragPlatesListeners" data-ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti">
                        <li
                        ng-repeat="(i_piatto, piatto) in restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti"
                        id="menu_{{current_menu_viewed}}_category_{{i_cat}}_plate_{{i_piatto}}" data-as-sortable-item>
                            <div class="single_plate_row">
                                <div class="row">
                                    <div class="col-lg-1 col-sm-1">
                                        <i class="fa fa-arrows-v" data-as-sortable-item-handle></i>
                                    </div>
                                    <div class="col-lg-9 col-sm-7">
                                        <h5>
                                            <span 
                                            class="plate_name"
                                            contenteditable
                                            ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].name">
                                            {{restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].name}}
                                            </span>
                                        </h5>
                                        <p 
                                        class="plate_desc"
                                        contenteditable 
                                        ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].description"
                                        >{{restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].description}}
                                        </p>
                                        <div class="form-group" ng-if="!restaurant.menu.menus[current_menu_viewed].menu_type || restaurant.menu.menus[current_menu_viewed].menu_type == 0">
                                            <label>Prezzo €</label>
                                            <input type="text"                                  
                                                ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].price" 
                                                class="form-control price-input" pformat prefix="" cents='.' thousands=''/>
                                        </div>  
                                        <p>
                                            <span class="label_extra_info">Ingredienti: </span>
                                            <tagger 
                                            ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].ingredienti" />
                                        </p>
                                        <p>
                                            <span class="label_extra_info">Allergeni: </span>
                                            <tagger 
                                            ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].allergeni"
                                            options="avaible_allergeni" />
                                        </p>
                                        <p>
                                            <a class="text-danger" ng-click="deletePlate(i_cat,i_piatto)"><i class="mebi-close add_icon_inline"></i> Cancella il piatto</a>
                                            <a class="text-primary" ng-click="addOption(i_cat,i_piatto)"><i class="mebi-android-add add_icon_inline"></i> Aggiungi una opzione</a>
                                        </p>
                                        <ul class="list-unstyled">
                                            <li ng-repeat="(i_opt_cat, opt_cat) in restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options">
                                                <div class="main_option_title options_top">
                                                    <span class="col-sm-4 col-xs-12" contenteditable
                                                    ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].name">
                                                    {{restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].name}}
                                                    </span>
                                                    <?php
                                                    /*
                                                        Only for menoo
                                                    <span class="col-sm-8 col-xs-12">
                                                        <label class="label--checkbox block label_options_row">
                                                            <input 
                                                            class="checkbox" 
                                                            type="checkbox" 
                                                            name="settingOption[]" 
                                                            ng-checked="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].type == 0" 
                                                            ng-click="SetOptionCatType(i_cat,i_piatto,i_opt_cat)" />
                                                            opzioni selezionabili contemporaneamente
                                                        </label>
                                                    </span>
                                                    <?php 
                                                    */ ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <a class="management_btn text-danger margin_delete" ng-click="deleteOption(i_cat,i_piatto,i_opt_cat)">cancella la categoria</a>
                                                <div class="clearfix"></div>
                                                <p class="row options_row hidden-sm hidden-xs no_border">
                                                    <span class="col-md-4 col-sm-12 col-xs-12">&nbsp;</span>
                                                    <span class="col-md-2 col-sm-3 col-xs-6">
                                                        <span 
                                                            aria-label="Nessuna variazione sul prezzo"
                                                            class="hint--bottom"><i class="fa fa-info"></i></span>
                                                    </span>
                                                    <span class="col-md-2 col-sm-3 col-xs-6">+/-</span>
                                                    <span class="col-md-2 col-sm-3 col-xs-6">€</span>
                                                    <span class="col-md-2 col-sm-3 col-xs-6">&nbsp;</span>
                                                </p>
                                                <p class="row options_row" ng-repeat="(i_opzione,opzioni) in restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options">

                                                    <span class="col-md-4 col-sm-12 col-xs-12" 
                                                    contenteditable
                                                    ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].name">
                                                        {{restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].name}}
                                                        

                                                    </span>
                                                    <span class="col-md-2 col-sm-3 col-xs-6">
                                                        <label class="label--checkbox block">
                                                            <input 
                                                            class="checkbox" 
                                                            type="checkbox" 
                                                            name="settingOption[]" 
                                                            ng-checked="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].no_variation == 1" 
                                                            ng-click="SetOptionVariation(i_cat,i_piatto,i_opt_cat,i_opzione)">
                                                        </label>                                                                            
                                                    </span>
                                                    <span class="col-md-2 col-sm-3 col-xs-6">
                                                        <span ng-if="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].no_variation == 0">
                                                            <span class="edit-span" ng-class="{'text-primary' : (restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].variation == 0 || !restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].variation)}" ng-click="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].variation = 0"> + </span>
                                                            <span class="edit-span" ng-class="{'text-primary' : restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].variation == 1}" ng-click="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].variation = 1"> - </span>
                                                        </span>
                                                        <!--<select 
                                                        ng-if="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].no_variation == 0"
                                                        aria-label="tipo di variazione sul prezzo"
                                                        class="options_select hint--bottom"
                                                        ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].variation">
                                                            <option value="0">+</option>
                                                            <option value="1">-</option>
                                                        </select>-->
                                                    </span>
                                                    <span class="col-md-2 col-sm-3 col-xs-6">
                                                        <input type="text" ng-if="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].no_variation == 0" 
                                                            ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].price" 
                                                            pformat class="price_options_field" prefix="" cents='.' thousands=''/>
                                                    </span>
                                                    <span class="col-md-2 col-sm-3 col-xs-6">
                                                        <i class="mebi-close" title="Elimina" ng-click="removeOption(i_cat,i_piatto,i_opt_cat,i_opzione)"></i>
                                                    </span>
                                                </p>
                                                <div class="clearfix"></div>
                                                <a class="add_plate_row" ng-click="appendOptions(i_cat,i_piatto,i_opt_cat)">
                                                    <i class="mebi-android-add"></i>
                                                     aggiungi una opzione
                                                </a>
                                            </li>
                                        </ul>                                                            
                                    </div>
                                    <div class="col-lg-2 col-sm-4">
                                        <div class="plate_image_container">
                                            <input class="upload-input upload_in_plate" type="file" ngf-select 
                                                    ng-model="add_plate_image.image" 
                                                    name="add_plate_image" 
                                                    ng-change="uploadPlateImage(i_cat,i_piatto)" title="Immagine" />
                                            <i class="mebi-technology-1 add_plate_icon" ng-if="!piatto.image"></i>
                                            <img ng-src="{{staticURL}}/menu/small/{{piatto.image}}.png" 
                                            alt="" 
                                            title="Cambia immagine" 
                                            ng-if="piatto.image" 
                                            class="single_edit_plate_image" />
                                        </div>
                                    </div>
                                </div>
                            </div>                                                
                        </li>
                    </ul>  
                    <a class="add_plate_row" ng-click="appendPlate(i_cat)">
                        <i class="mebi-android-add"></i>
                         aggiungi un piatto
                    </a>
                </div>
                <label class="label--checkbox block">
                    <input 
                    class="checkbox" 
                    type="checkbox" 
                    name="settingOption[]" 
                    ng-checked="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].line_break == 1" 
                    ng-click="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].line_break = (restaurant.menu.menus[current_menu_viewed].categorie[i_cat].line_break == 1) ? 0 : 1">
                    Inserisci interruzione di pagina (Valido per la stampa)
                </label> 
            </div>
        </li>
    </ul>
    <a class="management_btn last_add_cat" ng-click="appendCategoryToMenu()"><i class="mebi-android-add add_icon_inline"></i> aggiungi una categoria</a>                                
    <div class="form-group">
        <label>Note</label>
        <textarea msd-elastic 
        ng-model="restaurant.menu.menus[current_menu_viewed].post_text" 
        class="simple_border_input form-control plate_name" 
        ></textarea>  
    </div>
</div>