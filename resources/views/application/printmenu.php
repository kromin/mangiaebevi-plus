<?php // prende menu
?>
<!DOCTYPE html>
<html ng-app="app">
    <head>
        <meta charset="utf-8">
        <meta name=viewport content="width=device-width, initial-scale=1">
        <style></style><!-- debug bar in error without this-->
        <link rel="shortcut icon" href="/img/favicon.ico">
        <meta name="robots" content="noindex">
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/app.css" rel='stylesheet' type='text/css'>
        <base href="/" />                
    </head>    
    <body class="bg-white">
	<div class="standalone_menu">
		<div class="menu <?php 
			switch($style){
				case 0:
				echo 'style_one';
				break;
				case 1:
				echo 'style_two';
				break;
				case 2:
				echo 'style_tree';
				break;
				case 3:
				echo 'style_four';
				break;
				default:
				echo 'style_one';
				break;
			}?>">
			<div class="menu_title">
				<h1><?php echo esc_html($restaurant->name);?>
					<small><?php echo esc_html($restaurant->address);?></small>
				</h1>

				<h3><?php echo esc_html($menu['name'])?></h3>
			</div>
			<?php if(isset($menu['pre_text'])): ?>
			<div class="pre_text">
				<p class="wrap_text"><?php echo return_simple_text_br($menu['pre_text'])?></p>
			</div>
			<?php endif; 
			if(!isset($menu['menu_type'])) $menu['menu_type'] = 0;
			?>
			<?php if ( $menu['menu_type'] == 1 ) : ?>
			<div class="menu_price">
				<p class="menu_price_text">Menu a prezzo fisso</p>
				<p class="menu_price_price">€ <?php echo format_price($menu['menu_price']);?></p>
			</div>
			<?php endif; 			
			foreach ( $menu['categorie'] as $categoria ) :
			?>
			<div class="menu_category <?php if(isset($categoria['line_break']) && $categoria['line_break'] == 1) echo 'page-break';?>">
				<div class="clearfix"></div>
				<div class="menu_category_title">
					<h4><?php echo esc_html($categoria['name']);?></h4>
				</div>
				<?php
				$m = 0;
				foreach ( $categoria['piatti'] as $piatto ) :
					?>
				<div class="menu_plate <?php if(($m %2) != 0) echo 'even';?>">
					<div class="contain_plate">
						<?php if(isset($piatto['image']) && $piatto['image'] != ''): ?>
						<div class="plate_image">
							<?php if($style == 0 || $style == 1): 
							$w = ($style == 0) ? 70 : 110;
								// i pdf vogliono i background image per i cerchi
								if($type == 'pdf'):
								?>
									<div class="plate-image" style="border-radius: 50%; width: <?php echo $w;?>px; height: <?php echo $w;?>px; margin: auto; background-image: url('<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/menu/small/<?php echo escape_non_alphanum($piatto['image']);?>.png'); background-size: 100%;" >
									</div>
								<?php 
								else:
									?>
									<img src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/menu/small/<?php echo escape_non_alphanum($piatto['image']);?>.png" class="plate-image" style="border-radius: 50%; width: <?php echo $w;?>px; height: <?php echo $w;?>px; margin: auto;" />
								<?php
								endif;
								?>
							<?php else: ?>
							<img src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/menu/small/<?php echo escape_non_alphanum($piatto['image']);?>.png" alt="plate image" />
							<?php endif; ?>
						</div>
						<?php endif; ?>
						<div class="plate_content<?php if($menu['menu_type'] == 0) echo ' with_price'; if(isset($piatto['image']) && $piatto['image'] != '') echo ' with_image';?>" <?php if($style == 0 && $menu['menu_type']==0 && isset($piatto['image']) && $piatto['image'] != '') echo 'style="width: 55%; overflow: hidden;"';?><?php if($style == 3 && $menu['menu_type']==0) echo 'style="width: 78%; overflow: hidden;"';?>>
							<h5><?php echo esc_html($piatto['name']);?></h5>
							<p><?php echo return_simple_text_br($piatto['description']);?></p>
							<?php if(isset($piatto['ingredienti']) && count($piatto['ingredienti']) > 0): ?>
							<p class="plate_ingredienti"><b>Ingredienti: </b> <?php echo esc_html(implode(", ",$piatto['ingredienti']));?></p>
							<?php endif; 
							if(isset($piatto['allergeni']) && count($piatto['allergeni']) > 0): ?>
							<p class="plate_allergeni"><b>Allergeni: </b> <?php echo esc_html(implode(", ",$piatto['allergeni']));?></p>
							<?php endif; ?>
							<?php 
							if(isset($piatto['options'])) :
								foreach ($piatto['options'] as $opzione_cat) :
								?>
								<div class="plate_options">
									<p><b><?php echo $opzione_cat['name'];?></b></p>
									<?php if(isset($opzione_cat['options']) && count($opzione_cat['options']) > 0): ?>
									<ul class="option_style">
										<?php foreach($opzione_cat['options'] as $opzione): ?>
										<li>
											<span class="option_name"><?php echo esc_html($opzione['name']); ?></span>
											<?php if($opzione['no_variation'] == 0): ?>
											<span class="option_addition">
												<?php echo ($opzione['variation'] == 0) ? '+': '-';?> <?php echo format_price($opzione['price']); ?> €
											</span>
											<?php endif; ?>
										</li>
										<?php endforeach; ?>
									</ul>
									<?php endif; ?>
								</div>
								<?php endforeach;
							endif;
							?>
						</div>
					</div>
					<?php if($menu['menu_type'] == 0 && $piatto['price'] > 0) : ?>
					<div class="plate_price<?php if(isset($piatto['image']) && $piatto['image'] != '') echo ' with_image';?>">
						<p><span class="euro">€</span> <span class="price"><?php echo format_price($piatto['price']);?></span></p>
					</div>
					<?php endif; ?>									
				</div><?php if(($m %2) != 0) echo '<div class="clearfix"></div>';?>	
				<?php $m++; endforeach; ?>
				<div class="clearfix"></div>
				<hr class="row_separator" />				
			</div>
			<?php endforeach; ?>
			<?php if(isset($menu['post_text'])): ?>
			<div class="post_text">
				<p class="wrap_text"><?php echo return_simple_text_br($menu['post_text']);?></p>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<?php 
        if(isset($type) && $type == 'print'):
        	?>
        <script type="text/javascript">
        setTimeout(function(){
        	window.print();
        },3000);
        </script>
        <?php
        endif;
    ?>
    <script type="text/javascript" src="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/script.js"></script>
    </body>
</html>