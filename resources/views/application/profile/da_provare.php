<div ng-controller="UserDaProvareCtrl">
	<div class="row" ng-init="initDaProvare('<?php echo $user_array['_id'];?>')">
		<div class="col-sm-6 col-sm-push-3">
			<h3 class="text-center section_title">Da provare</h3>			
		</div>
	</div>
	<div class="grey_bg">
		<div class="container">			
			<div class="col-sm-6 col-sm-push-3">	
				<ul class="list-unstyled dashboard_list">
					<li ng-repeat="restaurant in daprovare">
						<div class="dashboard_friend">
		            		<div class="element_header">
		            			<div class="pull-left">
				            		<img class="profile_image" ng-src="{{staticURL}}/restaurant/square/{{restaurant.image}}.png" alt="" title="" ng-if="restaurant.image && restaurant.image != ''" />
				                    <img class="profile_image" 
				                    ng-src="/img/default-avatar.png" alt="" title="" 
				                    ng-if="!restaurant.image || restaurant.image == ''" />
				                </div>
			                    <div class="col-sm-10 col-xs-9">
				                    <a href="/{{restaurant.city}}/{{restaurant.slug}}/" target="_blank">
				            			{{restaurant.name}}
				            		</a><br />
				            		<span class="date"><i class="mebi-placeholder-on-map-paper-in-perspective"></i> {{restaurant.address}}</span>
			            		</div>
			            		<?php if($isme):?>
			            		<span class="hint--left pull-right" 
								aria-label="Ristorante provato" 
								ng-click="setProvato(restaurant._id)">
									<i class="mebi-check-1" style="font-size: 30px;"></i>
								</span>
								<?php endif; ?>
			            		<div class="clearfix"></div>
		            		</div>
		            	</div>						
					</li>
				</ul>
			</div>
		</div>
		<div class="clearfix"></div>
		<p class="not_found_message" ng-if="daprovare.length == 0">Non ci sono ristoranti da provare</p>
	</div>
</div>