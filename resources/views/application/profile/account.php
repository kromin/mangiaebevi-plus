<div ng-controller="UserAccountCtrl" ng-init="initAccount()">
	<h2 class="text-center section_title">Cambia le informazioni del tuo profilo</h2>
	<div class="row marginated_bottom">
        <div class="container">
	      	<div class="col-md-6 col-md-push-3 col-sm-12 minimal_tabs">
				<ul class="list-unstyled">
					<li ng-class="{'active' : !profile_tab || profile_tab == 0}">
						<a ng-click="setProfileTab(0)">Profilo</a>
					</li>
					<li ng-class="{'active' : profile_tab == 1}">
						<a ng-click="setProfileTab(1)">Account</a>
					</li>
					<?php if (auth()->user() && auth()->user()->_id == $user_array['_id'] && auth()->user()->role >= 2 && env('OPEN_PAYMENTS')) : ?>
					<li ng-class="{'active' : profile_tab == 2}">
						<a ng-click="setProfileTab(2)">Abbonamento</a>
					</li>
					<?php endif; ?>
				</ul>
			</div>
			<div class="clearfix"></div>
	      	<div class="col-md-6 col-md-push-3 col-sm-12" ng-if="profile_tab == 0 || !profile_tab">
	      		<form id="ProfileInfo" class="" ng-submit="updateInfo()">
	                <div class="form-group">
	                  <label>Nome</label><br />
	                  <input class="activated_input full" type="text" ng-model="current_user.user.name" required/>
	                </div>
	                <div class="form-group">
	                  <label>Cognome</label><br />
	                  <input class="activated_input full" type="text" ng-model="current_user.user.surname" required/>
	                </div>
	                <div class="form-group">
	                  <label>Telefono</label><br />
	                  <input class="activated_input full" type="text" ng-model="current_user.user.phone" required/>
	                </div>
	                <div class="form-group">
	                  <label>Qualcosa di te</label><br />
	                  <textarea msd-elastic class="activated_input full" type="text" ng-model="current_user.user.description" required></textarea>
	                </div>
	                <button ng-disabled="ProfileInfo.$invalid" class="outline_btn bprimary">
	                  	<span>Conferma le modifiche</span>
	                </button>
	          	</form>
	          	<form id="AllergeniInfo" class="" style="margin-top: 20px;" ng-submit="updateAllergeni()">
	                <div class="form-group">
	                  <label>Allergie</label><br />
	                  <label class="label--checkbox block" ng-repeat="(i,allergene) in allergeni">
	                    <input class="checkbox" type="checkbox"
	                    ng-checked="current_user.user.allergeni.indexOf('{{allergene}}') !== -1"
	                    ng-click="addAllergeneToUser(i)"
	                    id="allergene_{{i}}_checked" > {{allergene}}
	                	</label>
	                </div>
	                <button class="outline_btn bprimary">
	                  	<span>Conferma</span>
	                </button>
	          	</form>
	          	<hr class="margin-big-v" />
	          	<form id="completeProfileForm" class="">
	          		<p ng-if="current_user.user.foodcoin <= 500">Completa il tuo profilo per avere i tuoi foodcoin</p>
	                <div class="form-group">
	                  <label>Indirizzo</label><br />
	                  <input type="text" name="address" class="activated_input full" ng-model="current_user.user.address"
						ng-autocomplete
						details="details"
						class="form-control"
						placeholder="Indirizzo" />
	                </div>
	                <div class="form-group relative">
	                  <label>Data di nascita</label><br />
	                  <input type="text" id="datetimepicker-day" class="activated_input full" required />
	                </div>
	            </form>
	            <button ng-disabled="completeProfileForm.$invalid" class="outline_btn bprimary" ng-click="completeUserInfo()">
	            	<?php if ($user_array['profile_completed'] == 0) : ?>
	            	<span>Completa il profilo</span>
	            	<?php else : ?>
	              	<span>Conferma le modifiche</span>
	              	<?php endif; ?>
	            </button>
	    	</div>
	        <div class="col-md-6 col-md-push-3 col-sm-12" ng-if="profile_tab == 1">
	            <h3>Modifica la password per accedere al tuo account</h3>
	            <p>Se hai effettuato l'accesso <b>esclusivamente</b> con un social network questa azione non è disponibile e riceverai un errore</p>
	            <form id="changePWDForm" ng-submit="changePWD()">
	            	<p class="text-danger" ng-if="pwd_error">Le password non coincidono</p>
	                <div class="form-group">
	                  <label>Vecchia password</label><br />
	                  <input type="password" ng-model="change_pwd.old" class="activated_input full" required/>
	                </div>
	                <div class="form-group">
	                  <label>Nuova password</label><br />
	                  <input type="password" ng-model="change_pwd.new_pwd" class="activated_input full" required/>
	                </div>
	                <div class="form-group">
	                  <label>Conferma la nuova password</label><br />
	                  <input type="password" ng-model="change_pwd.new_confirm" class="activated_input full" required/>
	                </div>
	                <div class="form-group">
	                	<button ng-disabled="changePWDForm.$invalid" class="outline_btn bprimary" ng-click="changePWD()">
	                    	<span>Modifica la password</span>
	                    </button>
	                </div>
	            </form>

	            <hr />
	            <h3>Modifica l'indirizzo email del tuo account</h3>
	            <p>Se hai effettuato l'accesso <b>esclusivamente</b> con un social network cambiare l'indirizzo email non ti permetterà più di accedere</p>
	            <form id="changeMAILForm" ng-submit="changeMAIL()">
	            	<p class="text-danger" ng-if="mail_error">Le email non coincidono</p>
	                <div class="form-group">
	                  <label>Nuova email</label><br />
	                  <input type="email" ng-model="change_mail.new_mail" class="activated_input full" required/>
	                </div>
	                <div class="form-group">
	                  <label>Conferma email</label><br />
	                  <input type="email" ng-model="change_mail.confirm_new_mail" class="activated_input full" required/>
	                </div>
	                <div class="form-group">
	                	<button ng-disabled="changeMAILForm.$invalid" class="outline_btn bprimary" ng-click="changeMail()">
	                    	<span>Modifica la mail</span>
	                    </button>
	                </div>
	            </form>
	            <hr />
	            <p>Cancella il tuo account (<b>questa azione è irreversibile</b>, tutti i tuoi dati verranno cancellati)</p>
	          	<button class="outline_btn bdanger text-center" ng-click="cancellaAccount()">Cancellami</button>
	        </div>
	        <?php if (auth()->user() && auth()->user()->_id == $user_array['_id'] && auth()->user()->role >= 2 && env('OPEN_PAYMENTS')) : ?>
	        <div ng-if="profile_tab == 2">
	        	<div ng-init="returnUserPlan()">
	            	<div ng-if="settedPlans">
	                	<div ng-if="billings">
	                		<h3>Il tuo piano di abbonamento</h3>
		                    <p>{{user_billing_plan.name}}</p>
	                        <label ng-if="view_user.billing_plan.period == 0">{{user_billing_plan.price_month}} € al mese</label>
	                        <label ng-if="view_user.billing_plan.period == 1">{{user_billing_plan.price_annual}} € all'anno</label><br />
	                        <button class="outline_btn bdanger text-center" ng-click="cancelBillingPlan()">Cancella il piano di abbonamento</button>
	                    </div>
	                    <div ng-if="!billings">
	                    	<div class="col-sm-12" ng-controller="MenuPublishingCtrl" ng-init="initPlans()">
						        <p class="text-center">Per ottenere il massimo dalle funzionalità di MangiaeBevi+ per il tuo ristorante sottoscrivi un piano di abbonamento</p>
						        <div class="col-sm-4" ng-repeat="plan in pacchetti">
						            <div class="plan_table" ng-class="{'featured': plan.featured == 1}">
						                <div class="plan_header">
						                    <h3 class="plan_price text-center" ng-if="selected_plan_period == 0">{{plan.price_month}} €</h3>
						                    <h3 class="plan_price text-center" ng-if="selected_plan_period == 1">{{plan.price_annual}} €</h3>
						                    / {{returnCicleText(selected_plan_period)}}
						                </div>
						                <div ng-if="plan.period == 2" class="plan_fatturazione text-center">
						                    <button ng-click="setPlanPeriod(0)" ng-class="{'text-primary':selected_plan_period==0}">mensile</button>
						                    <button ng-click="setPlanPeriod(1)" ng-class="{'text-primary':selected_plan_period==1}">annuale</button>
						                </div>
						                <div class="plan_services">
						                    <ul class="list-unstyled">
						                        <li ng-repeat="serviceplan in plan.services" class="text-center">{{serviceplan}}</li>
						                    </ul>
						                </div>
						                <div class="plan_button text-center">
						                    <button ng-click="submitPlan(plan._id)" class="outline_btn bprimary">Conferma</button>
						                </div>
						            </div>
						        </div>
						        <div class="clearfix"></div>
						    </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    	<?php endif; ?>
        </div>
    </div>
</div>