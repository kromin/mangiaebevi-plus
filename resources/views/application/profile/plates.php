<div ng-controller="UserPiattiCtrl">
	<div class="row" ng-init="initPiatti('<?php echo $user_array['_id'];?>')">
		<div class="col-sm-6 col-sm-push-3">
			<h3 class="text-center section_title">Immagini dei piatti</h3>			
		</div>
	</div>
	<div class="grey_bg">
		<div class="container">			
			<div class="col-sm-6 col-sm-push-3">	
				<ul class="list-unstyled dashboard_list">
					<li ng-repeat="image in plate_images track by $index">                        
						<div class="element_header">
	            			<div class="pull-left">
			            		<img class="profile_image" 
			                    ng-src="{{staticURL}}/menu/medium/{{image['plate_obj'].image}}.png" alt="" title="" 
			                    ng-if="image['plate_obj'].image && image['plate_obj'].image != ''" />
			                    <img class="profile_image" 
			                    ng-src="/img/default-avatar.png" alt="" title="" 
			                    ng-if="!image['plate_obj'].image || image['plate_obj'].image == ''" />
			                </div>
		                    <div class="col-sm-10 col-xs-9">
			                    <a href="/piatto/{{image['plate_obj']._id}}/">{{image['plate_obj'].name}}</a><br />
			            		<span class="date">{{image.updated_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
		            		</div>
		            		<div class="clearfix"></div>
		            		<div class="clearfix"></div>
	            		</div>
                        <div class="element_body">
	            			<div class="main_image_action">
		            			<img class="full" 
								ng-src="{{staticURL}}/community/medium/{{image.image}}.png" alt="" title="" />
		            		</div>
		            		<div class="rating pull-left">
		            			<p class="text-center rating_p">{{image.rating}}</p>
		            		</div>
		            		<div class="plate_comment pull-left">
		            			<p>{{image.text}}</p>
		            		</div>
		            		<div class="clearfix"></div>
		            		<p class="text-center plate_share">
                            	<span class="hint--right" aria-label="Condividi il piatto">
                                	<a class="" ng-click="share('facebook_share',image)"><i class="fa fa-facebook fb"></i></a>
                                    <a class="" ng-click="share('twitter',image)"><i class="fa fa-twitter tw"></i></a>
									<a class="" ng-click="share('google_plus',image)"><i class="fa fa-google-plus gp"></i></a>
									<a class="" ng-click="share('linkedin',image)"><i class="fa fa-linkedin li"></i></a>
									<a class="" ng-click="share('facebook_send',image)"><i class="fa fa-comments fb"></i></a>
								</span>
                            </p>
	            		</div>                        

                                
                        <div class="clearfix"></div>
                    </li>
				</ul>
			</div>
			<div class="clearfix"></div>
		    <p class="text-center">
		        <a class="" ng-click="loadPlateImages()" 
		        ng-if="!stop_load_dashboard_pagination && show_button"
		            role="button">Carica altre immagini dei piatti</a>   
		    </p>
		    <p class="not_found_message" ng-if="plate_images.length == 0">Non ci sono immagini dei piatti aggiunte</p>
		</div>
	</div>
</div>