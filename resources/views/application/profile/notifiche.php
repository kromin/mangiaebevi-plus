<div ng-controller="UserNotificationsCtrl">
	<div class="row">
		<div class="col-sm-6 col-sm-push-3">
			<h3 class="text-center section_title">Notifiche</h3>			
		</div>
	</div>
	<div class="grey_bg" ng-init="firstNotificationLoad('<?php echo $user;?>');">
		<div class="container">			
			<div class="col-sm-6 col-sm-push-3">
					<ul class="list-group notifications_list" ng-if="notifications.length > 0">
	                  <li class="list-group-item single_notification" ng-repeat="notification in notifications track by $index">
	                    <div class="media">
	                      <div class="media-left">
	                        <i class="mebi-profile text-success" ng-if="notification.type == 0"></i>
	                        <i class="mebi-list text-primary" ng-if="notification.type == 1"></i>
	                        <i class="mebi-cutlery text-info" ng-if="notification.type == 2"></i>
	                        <i class="mebi-paypal text-success" ng-if="notification.type == 3"></i>
	                        <i class="mebi-phone-call text-warning" ng-if="notification.type == 4"></i>
	                        <i class="mebi-check-1 text-success" ng-if="notification.type == 5"></i>
	                        <i class="mebi-check-1 text-success" ng-if="notification.type == 6"></i>
	                        <i class="mebi-refund text-danger" ng-if="notification.type == 7"></i>
	                        <i class="mebi-refund text-success" ng-if="notification.type == 8"></i>
	                        <i class="mebi-calendar-1 text-info" ng-if="notification.type == 9"></i>
	                        <i class="mebi-delivery text-danger" ng-if="notification.type == 10"></i>
	                        <i class="mebi-check-1 text-success" ng-if="notification.type == 11"></i>
	                        <i class="mebi-check-1 text-primary" ng-if="notification.type == 12"></i>
	                        <i class="mebi-calendar-1 text-danger" ng-if="notification.type == 13"></i>
	                        <i class="mebi-close text-danger" ng-if="notification.type == 14"></i>
	                        <i class="mebi-avatar text-primary" ng-if="notification.type == 15"></i>
	                        <i class="mebi-avatar text-success" ng-if="notification.type == 16"></i>
	                        <i class="mebi-avatar text-warning" ng-if="notification.type == 17"></i>
	                        <i class="mebi-avatar text-primary" ng-if="notification.type == 18"></i>
	                        <i class="mebi-avatar text-success" ng-if="notification.type == 19"></i>
	                      </div>
	                      <div class="media-body">
	                        <h5 class="media-heading" ng-if="notification.type == 0">
	                          <b>Nuova registrazione</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 1">
	                          <b>Nuovo ordine</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 2">
	                          <b>Ristorante aggiunto</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 3">
	                          <b>Nuova iscrizione a un piano di abbonamento</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 4">
	                          <b>Claim</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 5">
	                          <b>Richiesta approvata</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 6">
	                          <b>Ristorante approvato</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 7">
	                          <b>Chiesto rimborso</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 8">
	                          <b>Ordine rimborsato</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 9">
	                          <b>Nuova prenotazione</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 10">
	                          <b>Utente vuole essere un driver</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 11">
	                          <b>Prenotazione confermata</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 12">
	                          <b>Accettato cambio prenotazione</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 13">
	                          <b>Data prenotazione cambiata</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 14">
	                          <b>Rifiutato cambio prenotazione</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 15">
	                          <b>Nuovo follower</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 16">
	                          <b>Richiesta di amicizia accettata</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 17">
	                          <b>Nuova raccomandazione</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 18">
	                          <b>Richiesta di amicizia</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 19">
	                          <b>Risposta alla richiesta di raccomandazione</b>
	                        </h5>
	                        
	                        <small>{{notification.created_at|amDateFormat:'DD/MM/YYYY HH:mm:ss'}}</small>
	                        
	                        <div class="profile-brief" ng-if="notification.type == 0">
	                          <a href="/u/{{notification.parameters[0]}}/">
	                            {{notification.parameters[1]}}
	                          </a> si è iscritto
	                        </div>
	                        
	                        <div class="profile-brief" ng-if="notification.type == 1">
	                          <a href="/u/{{notification.parameters[1]}}/">
	                            {{notification.parameters[2]}}
	                          </a> 
	                            ha effettuato un 
	                            <a ng-click="goToAdmin('#/restaurant/orders/'+notification.parameters[4]+'/'+notification.parameters[0])">
	                            ordine
	                          </a> di 
	                          {{notification.parameters[3]}} € con consegna il 
	                          {{notification.parameters[4].date|amDateFormat:'DD.MM.YYYY'}} 
	                          alle {{notification.parameters[4].date|amDateFormat:'HH:mm'}}                          
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 2">
	                          <a href="/u/{{notification.parameters[0]}}/">
	                            {{notification.parameters[1]}}
	                          </a> 
	                          ha aggiunto il ristorante 
	                          <a href="/modifica-ristorante/{{notification.parameters[2]}}/">
	                            {{notification.parameters[3]}}
	                          </a>
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 3">
	                          <a href="/u/{{notification.parameters[0]}}/">
	                            {{notification.parameters[1]}}
	                          </a> ha sottoscritto il piano {{notification.parameters[2]}}
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 4">
	                          <a href="/u/{{notification.parameters[0]}}/">{{notification.parameters[1]}}
	                          </a> 
	                          reclama il ristorante 
	                          <a href="/modifica-ristorante/{{notification.parameters[2]}}/">{{notification.parameters[3]}}
	                          </a>
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 5">
	                          La tua richiesta di gestire 
	                          <a href="/modifica-ristorante/{{notification.parameters[0]}}/">{{notification.parameters[1]}}
	                          </a> è stata approvata
	                        </div>
	                        
	                        <div class="profile-brief" ng-if="notification.type == 6">
	                          Il ristorante 
	                          <a href="/modifica-ristorante/{{notification.parameters[0]}}/">{{notification.parameters[1]}}
	                          </a> è stato approvato
	                        </div>
	                        
	                        <div class="profile-brief" ng-if="notification.type == 7">&Egrave; stato chiesto un rimborso per l'ordine numero 
	                          <a ng-click="goToAdmin('#/restaurant/orders/'+notification.parameters[1]+'/'+notification.parameters[0])">
	                            #{{notification.parameters[0]}}
	                          </a>
	                        </div>
	                        
	                        <div class="profile-brief" ng-if="notification.type == 8">
	                          L'ordine numero 
	                          #{{notification.parameters[0]}}
	                          è stato rimborsato
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 9">
	                          <a href="/u/{{notification.parameters[0]}}/">
	                            {{notification.parameters[1]}}
	                          </a>  
	                          ha prenotato al
	                          <a href="/modifica-ristorante/{{notification.parameters[2]}}/">
	                            {{notification.parameters[3]}}
	                          </a> 
	                          il {{notification.parameters[4]}} 
	                          per {{notification.parameters[5]}} persone 
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 10">
	                          <a href="/u/{{notification.parameters[0]}}/">
	                            {{notification.parameters[1]}}
	                          </a> vuole essere un driver
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 11">
	                          La tua prenotazione a {{notification.parameters[2]}} ({{notification.parameters[3]}}) è stata confermata
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 12">
	                          Il cambio di data della prenotazione di {{notification.parameters[4]}} ({{notification.parameters[3]}}) è stato accettato
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 13">
	                          La tua prenotazione a {{notification.parameters[2]}} è stata modificata 
	                          ({{notification.parameters[3]}} -> {{notification.parameters[4]}}) 
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 14">
	                        	<a href="/u/{{notification.parameters[0]}}/">
	                        		{{notification.parameters[1]}}</a> 
	                        		ha rifiutato il cambio di orario della prenotazione
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 15">
	                        	<a href="/u/{{notification.parameters[0]}}/">
	                        		{{notification.parameters[1]}}</a> 
	                        		ha iniziato a seguirti
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 16">
	                        	<a href="/u/{{notification.parameters[0]}}/">
	                        		{{notification.parameters[1]}}</a> 
	                        		ha accettato la tua richiesta di amicizia
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 17">
	                        	<a href="/u/{{notification.parameters[0]}}/">
	                        		{{notification.parameters[1]}}</a> 
	                        		ha raccomandato il tuo <a href="/modifica-ristorante/{{notification.parameters[2]}}/">ristorante</a>
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 18">
	                        	<a href="/u/{{notification.parameters[0]}}/">
	                        		{{notification.parameters[1]}}</a> 
	                        		ti ha inviato una richiesta di amicizia
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 19">
	                        	<a href="/u/{{notification.parameters[0]}}/">
	                        		{{notification.parameters[1]}}</a> 
	                        		ha risposto alla tua richiesta di raccomandazione
	                        </div>
	                        
	                      </div>
	                    </div>
	                  </li>                    
	                </ul>
	                <p class="text-center">
				        <a class="" ng-click="loadMoreNotifications()" ng-if="!notification_hide_loader && !loading_notifications">Carica altre notifiche</a>   
				    </p>
            </div>
			<div class="clearfix"></div>
			<p class="not_found_message" ng-if="notifications.length == 0">Non ci sono notiifche</p>
		</div>
	</div>
</div>