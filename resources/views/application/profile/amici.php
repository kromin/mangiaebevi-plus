<div ng-controller="UserFriendsCtrl">
	<div class="row">
		<div class="col-sm-6 col-sm-push-3" ng-init="initFriendsPage('<?php echo $user_array['_id'];?>')">
			<h3 class="text-center section_title">Follower / Following</h3>
		</div>
	</div>
	<div class="grey_bg">
		<div class="container">		
			<div class="col-sm-3 white_contain">						       
				<ul class="list-unstyled favourite_list_list">
					<?php /*<li ng-class="{'active' : current_friend_tab == 0 || !current_friend_tab}">
						<a ng-click="setFriendTab(0)">Amici</a>
					</li>					
					<?php if ($isme) : ?>
					<li ng-class="{'active' : current_friend_tab == 1}">
						<a ng-click="setFriendTab(1)">Richieste <span class="badge badge-danger">{{current_friends_request.length}}</span></a>
					</li>
					<?php endif; */?>
					<li ng-class="{'active' : current_friend_tab == 4 || !current_friend_tab}">
						<a ng-click="setFriendTab(4)">Cerca</a>
					</li>						
					<li ng-class="{'active' : current_friend_tab == 2}">
						<a ng-click="setFriendTab(2)">Follower</a>
					</li>
					<li ng-class="{'active' : current_friend_tab == 3}">
						<a ng-click="setFriendTab(3)">Following</a>
					</li>											
				</ul>
			</div>

			<div class="col-sm-6">
				<ul class="list-unstyled dashboard_list" ng-if="!current_friend_tab || current_friend_tab == 0" ng-init="initViewFriend()">					
					<li ng-repeat="friend in current_friends track by $index">
						<div class="dashboard_friend">
		            		<div class="element_header">
		            			<div class="pull-left">
				            		<img class="profile_image" 
									ng-src="{{staticURL}}/users/square/{{friend.avatar}}.png" alt="" title="" 
									ng-if="friend.avatar && friend.avatar != ''" />
									<img class="profile_image" 
				                    ng-src="/img/default-avatar.png" alt="" title="" 
				                    ng-if="!friend.avatar || friend.avatar == ''" />
				                </div>
			                    <div class="col-sm-10 col-xs-9">
				                    <a class="line_height_header_text" href="/u/{{friend._id}}/">{{friend.name}} {{friend.surname}}
				            		</a>				            		
									<?php if ($isme) : ?>
				            		<span class="line_height_header_text hint--left pull-right" 
									aria-label="Rimuovi dagli amici" 
									ng-click="removeFriend(friend._id)">
										<i class="mebi-close"></i>
									</span>				            		
									<?php endif; ?>
			            		</div>			            		
		            		</div>
		            	</div>															
						<div class="clearfix"></div>
					</li>
				</ul>
				<div class="invite_friends" ng-if="current_friend_tab == 4 || !current_friend_tab">
					<h3>Cerca tra gli utenti</h3>
					<form name="searchUserForm" ng-submit="searchUser(search_user.name)">		
						<div class="form-group">
							<label for="message">Inserisci il nome</label>
							<input type="text" name="confirm_address" ng-model="search_user.name"
								class="form-control" 
								required />
						</div>		
						<div class="form-group">	
							<input type="submit" ng-disabled="searchUserForm.$invalid" class="btn btn-primary ghost btn-no-margin" value="Cerca" />
						</div>
					</form>
					<ul class="list-unstyled dashboard_list" ng-if="search_results.length > 0">
						<li ng-repeat="friend in search_results track by $index">
							<div class="dashboard_friend">
			            		<div class="element_header">
			            			<div class="pull-left">
					            		<img class="profile_image" 
										ng-src="{{staticURL}}/users/square/{{friend.avatar}}.png" alt="" title="" 
										ng-if="friend.avatar && friend.avatar != ''" />
										<img class="profile_image" 
					                    ng-src="/img/default-avatar.png" alt="" title="" 
					                    ng-if="!friend.avatar || friend.avatar == ''" />
					                </div>
				                    <div class="col-sm-10 col-xs-9">
					                    <a class="line_height_header_text" href="/u/{{friend._id}}/" target="_blank">{{friend.name}} {{friend.surname}}
					            		</a>
				            		</div>			            		
			            		</div>
			            	</div>															
							<div class="clearfix"></div>
						</li>					
					</ul>
					<?php /*
					<h3>O invitali a far parte della community</h3>
					<form ng-submit="inviteFriends()">		
						<div class="form-group">
							<label for="message">Inserisci gli indirizzi email</label>
							<div class="clearfix"></div>
							<tagger 
                            ng-model="mail_addresses" />
						</div>		
						<div class="form-group">	
							<input type="submit" class="btn btn-primary ghost btn-no-margin" value="Invia" />
						</div>
					</form>
					<p>o condividi il tuo link personale sui social network</p>
					<p class="invite_friends">
                    	<span>
                        	<a class="hint--bottom" aria-label="Facebook" ng-click="invite('facebook_share')">
                        		<i class="fa fa-facebook fb"></i>
                        	</a>
                            <a class="hint--bottom" aria-label="Twitter" ng-click="invite('twitter')">
                            	<i class="fa fa-twitter tw"></i>
                            </a>
							<a class="hint--bottom" aria-label="Google Plus" ng-click="invite('google_plus')">
								<i class="fa fa-google-plus gp"></i>
							</a>
							<a class="hint--bottom" aria-label="Linkedin" ng-click="invite('linkedin')">
								<i class="fa fa-linkedin li"></i>
							</a>
							<a class="hint--bottom" aria-label="Messenger" ng-click="invite('facebook_send')">
								<i class="fa fa-comments fb"></i>
							</a>
						</span>
                    </p>*/?>
				</div>
				<?php if($isme):?>
				<ul class="list-unstyled dashboard_list" ng-if="current_friend_tab == 1" ng-init="initRequestFriend()">
					<li ng-repeat="friend in current_friends_request track by $index">
						<div class="dashboard_friend">
		            		<div class="element_header">
		            			<div class="pull-left">
				            		<img class="profile_image" 
									ng-src="{{staticURL}}/users/square/{{friend.avatar}}.png" alt="" title="" 
									ng-if="friend.avatar && friend.avatar != ''" />
									<img class="profile_image" 
				                    ng-src="/img/default-avatar.png" alt="" title="" 
				                    ng-if="!friend.avatar || friend.avatar == ''" />
				                </div>
			                    <div class="col-sm-10 col-xs-9">
				                    <a class="line_height_header_text" href="/u/{{friend._id}}/">{{friend.name}} {{friend.surname}}
				            		</a>
			            		</div>			            		
		            		</div>
		            		<div class="element_body">
		            			<div class="change_reservation">
                  					<p class="text-center marginated_vertical">
			                          	<span class="hint--top" aria-label="Accetta">
											<button class="outline_btn bsuccess nomin iconic profile_btn_iconical" ng-click="acceptRequest(friend._id)">
												<i class="mebi-android-done"></i>
											</button>
										</span>
										<span class="hint--top" aria-label="Rifiuta">
											<button class="outline_btn bdanger nomin iconic profile_btn_iconical" ng-click="refuseRequest(friend._id)">
												<i class="mebi-close"></i>
											</button>
										</span>
			                        </p>	
                  				</div>
                  			</div>
		            	</div>															
						<div class="clearfix"></div>
					</li>					
				</ul>
				<?php endif; ?>
				<div ng-if="current_friend_tab == 2">
					<ul class="list-unstyled dashboard_list" ng-init="inFollowers()">
						<li ng-repeat="friend in user_follower track by $index">
							<div class="dashboard_friend">
			            		<div class="element_header">
			            			<div class="pull-left">
					            		<img class="profile_image" 
										ng-src="{{staticURL}}/users/square/{{friend.avatar}}.png" alt="" title="" 
										ng-if="friend.avatar && friend.avatar != ''" />
										<img class="profile_image" 
					                    ng-src="/img/default-avatar.png" alt="" title="" 
					                    ng-if="!friend.avatar || friend.avatar == ''" />
					                </div>
				                    <div class="col-sm-10 col-xs-9">
					                    <a class="line_height_header_text" href="/u/{{friend._id}}/">{{friend.name}} {{friend.surname}}
					            		</a>
				            		</div>			            		
			            		</div>
			            	</div>															
							<div class="clearfix"></div>
						</li>
					</ul>
					<div class="clearfix"></div>
				    <p class="text-center" ng-if="!stop_load_follower_pagination && !loading_btn">
				        <a class="" ng-click="loadFollowers()"  
				        ng-if="!stop_load_dashboard_pagination"
				            role="button">Carica altri</a>   
				    </p>
				</div>
				<div ng-if="current_friend_tab == 3">
					<ul class="list-unstyled dashboard_list" ng-if="current_friend_tab == 3" ng-init="inFollowing()">
						<li ng-repeat="friend in user_following track by $index">
							<div class="dashboard_friend">
			            		<div class="element_header">
			            			<div class="pull-left">
					            		<img class="profile_image" 
										ng-src="{{staticURL}}/users/square/{{friend.avatar}}.png" alt="" title="" 
										ng-if="friend.avatar && friend.avatar != ''" />
										<img class="profile_image" 
					                    ng-src="/img/default-avatar.png" alt="" title="" 
					                    ng-if="!friend.avatar || friend.avatar == ''" />
					                </div>
				                    <div class="col-sm-10 col-xs-9">
					                    <a class="line_height_header_text" href="/u/{{friend._id}}/">{{friend.name}} {{friend.surname}}
					            		</a>	
				            		</div>			            		
			            		</div>
			            	</div>															
							<div class="clearfix"></div>
						</li>						
					</ul>
					<div class="clearfix"></div>
				    <p class="text-center" ng-if="!stop_load_following_pagination">
				        <a class="" ng-click="loadFollowing()"  
				        ng-if="!stop_load_dashboard_pagination && !loading_btn"
				            role="button">Carica altri</a>   
				    </p>
				</div>
			</div>
			<div class="clearfix"></div>			
		</div>
	</div>
</div>