<div ng-controller="UserFavouriteCtrl">
	<div class="row">
		<div class="col-sm-6 col-sm-push-3" ng-init="initFavourite('<?php echo $user_array['_id'];?>')">
			<h3 class="text-center section_title">Preferiti</h3>
		</div>
	</div>
	<div class="grey_bg">
		<div class="container">		
			<div class="col-sm-3 white_contain">
				<?php if(!$isme):?>
				<p>Le liste</p>
				<?php endif; ?>
				<?php if($isme):?>				
				<p>Organizza i tuoi ristoranti preferiti in liste</p>
				<div class="form-group">
	                <label>Nome lista</label><br />
	                <input class="activated_input full" type="text" ng-model="add_list.name" required/>
	            </div>
	            <button ng-click="addList(add_list.name)" class="btn btn-primary btn-block">
	              	<span>Crea</span>
	            </button> 		        			
		       <?php endif; ?>		       
				<ul class="list-unstyled favourite_list_list">
					<li ng-class="{'active' : current_list_val === 0}"><a ng-click="setList(0)">Tutti</a></li>	
					<li ng-class="{'active' : current_list_val == list.name}" 
					ng-repeat="list in favourite_lists track by $index"
					><a ng-click="setList(list.name)">{{list.name}}</a>
					<?php if ($isme) : ?>
					<i class="mebi-close pull-right" title="Rimuovi la lista" ng-if="current_list_val == list.name" ng-click="removeList(current_list_val)"></i>
					<?php endif; ?>
					</li>												
				</ul>
			</div>

			<div class="col-sm-6">
				<ul class="list-unstyled dashboard_list">
					<li ng-repeat="restaurant in current_list.favourite_restaurants track by $index">
						<div class="dashboard_friend">
		            		<div class="element_header">
		            			<div class="pull-left">
				            		<img class="profile_image" ng-src="{{staticURL}}/restaurant/square/{{restaurant.image}}.png" alt="" title="" ng-if="restaurant.image && restaurant.image != ''" />
				                    <img class="profile_image" 
				                    ng-src="/img/default-avatar.png" alt="" title="" 
				                    ng-if="!restaurant.image || restaurant.image == ''" />
				                </div>
			                    <div class="col-sm-10 col-xs-9">
				                    <a href="/{{restaurant.city}}/{{restaurant.slug}}/" target="_blank">
				            			{{restaurant.name}}
				            		</a><br />
				            		<span class="date"><i class="mebi-placeholder-on-map-paper-in-perspective"></i> {{restaurant.address}}</span>
			            		</div>
			            		<?php if ($isme) : ?>
			            		<span class="hint--left pull-right" 
								aria-label="Rimuovi dai preferiti" 
								ng-click="removeFavourite(restaurant._id)">
									<i class="mebi-close"></i>
								</span>
			            		<div class="clearfix"></div>
			            		<div class="favourite_lists">			            		
				            		<span class="list_span" ng-repeat="list in favourite_lists track by $index" 
									ng-class="{'active': isInList(list.restaurants,restaurant._id)}" 
									ng-click="addToList(restaurant._id,list.name)">{{list.name}}</span>
								</div>
								<?php endif; ?>
		            		</div>
		            	</div>															
						<div class="clearfix"></div>
					</li>
				</ul>
				<div class="clearfix"></div>
				<p class="not_found_message" ng-if="current_list.favourite_restaurants.length == 0">Non ci sono ristoranti tra i preferiti</p>
			</div>
			<div class="clearfix"></div>			
		</div>
	</div>
</div>