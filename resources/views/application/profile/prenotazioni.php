<div ng-controller="UserReservationsCtrl">
	<div class="row" ng-init="loadReservations()">
		<div class="col-sm-6 col-sm-push-3">
			<h3 class="text-center section_title">Prenotazioni</h3>			
		</div>
	</div>
	<div class="grey_bg">
		<div class="container">			
			<div class="col-sm-6 col-sm-push-3">
				<ul class="list-unstyled dashboard_list">
					<li ng-repeat="reservation in reservations track by $index">
						<div class="dashboard_friend">
		            		<div class="element_header">
		            			<span ng-if="reservation.type == 'web' && reservation.confirmed != 4"
                                    class="hint--left pull-right" 
                                    aria-label="Annulla la prenotazione" 
                                    ng-click="removeReservation(reservation._id)">
                                        <i class="mebi-close text-danger" style="font-size: 16px;"></i>
                                </span>
                                <div class="pull-left">
				            		<img class="profile_image" 
				            		ng-src="{{staticURL}}/restaurant/square/{{returnReservationRestaurant(reservation.restaurant).image}}.png" 
				            		alt="" title="" 
				            		ng-if="returnReservationRestaurant(reservation.restaurant).image && returnReservationRestaurant(reservation.restaurant).image != ''" />
				                    <img class="profile_image" 
				                    ng-src="/img/default-avatar.png" alt="" title="" 
				                    ng-if="!returnReservationRestaurant(reservation.restaurant).image || returnReservationRestaurant(reservation.restaurant).image == ''" />
				                </div>
			                    <div class="col-sm-10 col-xs-9">
				                    

                                    <a href="/{{returnReservationRestaurant(reservation.restaurant).city}}/{{returnReservationRestaurant(reservation.restaurant).slug}}/" target="_blank">
				            			{{returnReservationRestaurant(reservation.restaurant).name}}
				            		</a><br />
				            		<span class="date"><i class="mebi-placeholder-on-map-paper-in-perspective"></i> {{returnReservationRestaurant(reservation.restaurant).address}}</span>

				            		<div>				            								            		
					            		<span ng-if="!reservation.preorder && canPreOrderNow(reservation.date, reservation.fascia_oraria) && reservation.confirmed == 0"><a class="text-primary" href="/preorder/{{returnReservationRestaurant(reservation.restaurant).slug}}/{{reservation._id}}/">Preordina</a></span>
					            		<span ng-if="reservation.preorder">
					            			<a class="text-primary" ng-click="viewPreOrder(reservation)">Vedi il preordine</a>					            			
					            		</span>
					            	</div>
			            			
			            		</div>			            		
			            		<div class="clearfix"></div>
			            		<div class="element_body">
			            			<p class="text-center text-primary" ng-if="reservation.confirmed == 1">Prenotazione confermata</p>
                      				<p class="text-center text-warning" ng-if="reservation.confirmed == 0 || reservation.confirmed == 3">In attesa di conferma</p>
	                      			<p class="text-center text-danger" ng-if="reservation.confirmed == 4">Prenotazione annullata</p>
	                      			

                      				<div ng-if="reservation.confirmed == 2" class="change_reservation">
                      					<p class="text-center">Il proprietario del ristorante ha cambiato la l'orario della prenotazione</p> 
				                        <p class="text-center marginated_vertical">
				                          	<span class="hint--bottom" aria-label="Accetta">
												<button class="outline_btn bsuccess nomin iconic profile_btn_iconical" ng-click="accettaCambioData(reservation)">
													<i class="mebi-android-done"></i>
												</button>
											</span>
											<span class="hint--bottom" aria-label="Rifiuta">
												<button class="outline_btn bdanger nomin iconic profile_btn_iconical" ng-click="rifiutaCambioData(reservation)">
													<i class="mebi-close"></i>
												</button>
											</span>
				                        </p>	
                      				</div>
                      				<div class="row reservation_icons_block">
										<div class="col-xs-6">
											<div class="col-xs-4"><i class="mebi-avatar"></i></div>
											<div class="col-xs-8 num">{{reservation.people}}</div>
										</div>
										<div class="col-xs-6">
											<div class="col-xs-4"><i class="mebi-time"></i></div>
											<div class="col-xs-8 num normal">{{reservation.date|amDateFormat:'DD/MM/YYYY'}}<br />{{reservation.fascia_oraria}}</div>
										</div>
									</div>
	                      		</div>
		            		</div>
		            	</div>						
					</li>
				</ul>					
			</div>	
		</div>
		<div class="clearfix"></div>
		<p class="not_found_message" ng-if="reservations.length == 0">Non ci sono prenotazioni</p>
	</div>
</div>