<div ng-controller="UserRaccomandazioniCtrl">
	<div class="row" ng-init="initRaccomandazioni('<?php echo $user_array['_id'];?>',<?php if($isme) echo 'true'; else echo 'false';?>)">
		<div class="col-sm-6 col-sm-push-3">
			<h3 class="text-center section_title">Raccomandazioni</h3>
		</div>
		<?php if($isme): ?>
		<div class="clearfix"></div>
		<div class="col-sm-6 col-sm-push-3">
			<div class="minimal_tabs" ng-if="itsme">
				<ul class="list-unstyled">
					<li ng-class="{'active' : !recommendation_tab || recommendation_tab == 0}"><a ng-click="setRecommendationTab(0)">Community</a></li>
					<li ng-class="{'active' : recommendation_tab == 1}"><a ng-click="setRecommendationTab(1)">Le mie</a></li>
					<li ng-class="{'active' : recommendation_tab == 2}"><a ng-click="setRecommendationTab(2)">Richiedi</a></li>
				</ul>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<div class="grey_bg">
		<div class="container">
			<div ng-if="(!recommendation_tab || recommendation_tab == 0) && itsme">
				<div class="col-sm-6" ng-if="lat && lng">

					<ul class="list-unstyled dashboard_list">
						<li ng-repeat="recommendation in community_rec track by $index" ng-if="recommendation.ref_obj">
							<div class="dashboard_following">
			            		<div class="element_header">
			            			<div class="pull-left">
					            		<img class="profile_image"
					                    ng-src="{{staticURL}}/users/square/{{recommendation.user_obj.avatar}}.png" alt="" title=""
										ng-if="recommendation.user_obj.avatar && recommendation.user_obj.avatar != ''" />
					                    <img class="profile_image"
					                    ng-src="/img/default-avatar.png" alt="" title=""
					                    ng-if="!recommendation.user_obj.avatar || recommendation.user_obj.avatar == ''" />
					                </div>
				                    <div class="col-sm-10 col-xs-9" ng-if="recommendation.type == 0 || !recommendation.type">
					                    <span class="text-primary"><a href="/u/{{recommendation.user_obj._id}}/" class="text-primary">{{recommendation.user_obj.name}} {{recommendation.user_obj.surname}}</a></span>
					                    ha raccomandato il ristorante <a href="/{{recommendation.ref_obj.city}}/{{recommendation.ref_obj.slug}}/">
					                    {{recommendation.ref_obj.name}}
										</a><br />
					            		<span class="date">{{recommendation.created_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
					            		<div class="plate_comment">
				            				<p>{{recommendation.text}}</p>
				            			</div>
				            		</div>
				            		<div class="col-sm-10 col-xs-9" ng-if="recommendation.type == 1">
					                    <span class="text-primary"><a href="/u/{{recommendation.user_obj._id}}/" class="text-primary">{{recommendation.user_obj.name}} {{recommendation.user_obj.surname}}</a></span>
					                    ha raccomandato il piatto <a href="/piatto/{{recommendation.ref_obj._id}}/">{{recommendation.ref_obj.name}}
										</a><br />
					            		<span class="date">{{recommendation.updated_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
				            			<div class="plate_comment">
				            				<p>{{recommendation.text}}</p>
				            			</div>
				            		</div>

				            		<div class="clearfix"></div>
			            		</div>
			            		<div class="element_body" ng-if="recommendation.ref_obj.image && recommendation.ref_obj.image != ''">
			            			<div class="main_image_action">
				            			<img class="full"
										ng-src="{{staticURL}}/restaurant/square/{{recommendation.ref_obj.image}}.png" alt="" title=""
										ng-if="recommendation.ref_obj.image && recommendation.ref_obj.image != '' && (recommendation.type == 0 || !recommendation.type)" />
										<img class="full"
										ng-src="{{staticURL}}/menu/square/{{recommendation.ref_obj.image}}.png" alt="" title=""
										ng-if="recommendation.ref_obj.image && recommendation.ref_obj.image != '' && recommendation.type == 1" />
				            		</div>

			            		</div>
			            	</div>
							<div class="clearfix"></div>
						</li>
					</ul>

			        <p class="text-center">
				        <a class="" ng-click="loadRecommendation('loadLeft')"
				        ng-if="!stop_left_pagination"
				            role="button">Altre</a>
				    </p>
				</div>
				<div class="col-sm-6" ng-if="lat && lng">

					<ul class="list-unstyled dashboard_list">
						<li ng-repeat="recommendation in community_req track by $index">
							<div class="dashboard_following">
			            		<div class="element_header">
			            			<div class="pull-left">
					            		<img class="profile_image"
					                    ng-src="{{staticURL}}/users/square/{{recommendation.user_obj.avatar}}.png" alt="" title=""
										ng-if="recommendation.user_obj.avatar && recommendation.user_obj.avatar != ''" />
					                    <img class="profile_image"
					                    ng-src="/img/default-avatar.png" alt="" title=""
					                    ng-if="!recommendation.user_obj.avatar || recommendation.user_obj.avatar == ''" />
					                </div>
				                    <div class="col-sm-10 col-xs-9">
					                    <span class="text-primary"><a href="/u/{{recommendation.user_obj._id}}/" class="text-primary">{{recommendation.user_obj.name}} {{recommendation.user_obj.surname}}</a></span>
					                    sta cercando un
					                    <span ng-if="recommendation.type == 0">ristorante</span>
					                    <span ng-if="recommendation.type == 1">piatto</span><br />
					            		<span class="date">{{recommendation.created_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
					            		<div class="plate_comment">
				            				<p>{{recommendation.text}}</p>
				            			</div>
				            		</div>
				            		<div class="clearfix"></div>
			            		</div>
			            		<div class="clearfix"></div>
			            		<div class="element_body">
			            			<div class="recommendation_answers">
			            				<p class="action_texts">
				            				<a class="open_comments" ng-click="openAnswers('user_req',$index,recommendation._id)">
												<i class="fa fa-comments"></i>{{recommendation.answers_num}}
											</a>
											<a class="open_recommend pull-right" ng-click="pushAnswer('community_req',$index,recommendation._id)">
												Consiglia un
												<span ng-if="recommendation.type==0">Ristorante</span>
												<span ng-if="recommendation.type==1">Piatto</span>
											</a>
										</p>
			            			</div>


			            			<!-- steps per rispondere alle raccomandazioni -->
									<div class="publish_recommendation_request" ng-if="isAskingRequestFor(recommendation._id)">
										<!-- ristorante scelto -->
										<ul class="list-unstyled dashboard_list" ng-if="(current_answering.restaurant || current_answering.restaurant == 0) && current_answering.plate != 0 && !current_answering.plate">
											<li>
												<div class="dashboard_friend">
								            		<div class="element_header">
								            			<div class="pull-left">
										            		<img class="profile_image"
										            		ng-src="{{staticURL}}/restaurant/square/{{answer_restaurants[current_answering.restaurant].image}}.png" alt="" title=""
										            		ng-if="answer_restaurants[current_answering.restaurant].image && answer_restaurants[current_answering.restaurant].image != ''" />
										                    <img class="profile_image"
										                    ng-src="/img/defaultsearch.jpg" alt="" title=""
										                    ng-if="!answer_restaurants[current_answering.restaurant].image || answer_restaurants[current_answering.restaurant].image == ''" />
										                </div>
									                    <div class="col-sm-10 col-xs-9">
										                    {{answer_restaurants[current_answering.restaurant].name}}<br />
										            		<span class="date"><i class="mebi-placeholder-on-map-paper-in-perspective"></i> {{answer_restaurants[current_answering.restaurant].address}}</span>
									            		</div>
									            		<div class="clearfix"></div>
								            		</div>
								            	</div>
											</li>
										</ul>
										<div class="clearfix"></div>
										<!-- piatto scelto -->
										<ul class="list-unstyled dashboard_list"
										ng-if="current_answering.plate || current_answering.plate == 0">
											<li>
												<div class="dashboard_friend">
								            		<div class="element_header">
								            			<div class="pull-left">
										            		<img class="profile_image"
										            		ng-src="{{staticURL}}/menu/square/{{answer_plates[current_answering.plate].image}}.png" alt="" title=""
										            		ng-if="answer_plates[current_answering.plate].image && answer_plates[current_answering.plate].image != ''" />
										                    <img class="profile_image"
										                    ng-src="/img/defaultsearch.jpg" alt="" title=""
										                    ng-if="!answer_plates[current_answering.plate].image || answer_plates[current_answering.plate].image == ''" />
										                </div>
									                    <div class="col-sm-10 col-xs-9">
										                    {{answer_plates[current_answering.plate].name}}<br />
									            		</div>
									            		<div class="clearfix"></div>
								            		</div>
								            	</div>
											</li>
										</ul>



										<div ng-if="answer_step_block == 1">
											<div class="form-group search_restaurant_group">
												<input class="restaurant_search_input"
												ng-model="answer_recommendation_restaurant_input.name" name="ristoname" placeholder="Nome del ristorante" />
												<button class="search_restaurant_button" ng-click="searchRestaurantForAnswer(answer_recommendation_restaurant_input.name)"
												><i class="fa fa-search"></i></button>
											</div>

											<div class="restaurants_answer_list">
												<ul class="list-unstyled dashboard_list">
													<li ng-repeat="restaurant in answer_restaurants track by $index"
														ng-click="selectRestaurant($index)">
														<div class="dashboard_friend">
										            		<div class="element_header">
										            			<div class="pull-left">
												            		<img class="profile_image"
												            		ng-src="{{staticURL}}/restaurant/square/{{restaurant.image}}.png" alt="" title=""
												            		ng-if="restaurant.image && restaurant.image != ''" />
												                    <img class="profile_image"
												                    ng-src="/img/defaultsearch.jpg" alt="" title=""
												                    ng-if="!restaurant.image || restaurant.image == ''" />
												                </div>
											                    <div class="col-sm-10 col-xs-9">
												                    {{restaurant.name}}<br />
												            		<span class="date"><i class="mebi-placeholder-on-map-paper-in-perspective"></i> {{restaurant.address}}</span>
											            		</div>
											            		<div class="clearfix"></div>
										            		</div>
										            	</div>
													</li>
												</ul>

											</div>
										</div>



										<div ng-if="answer_step_block == 2 && answer_type == 1">

											<div class="form-group search_restaurant_group">
												<input class="restaurant_search_input"
												ng-model="answer_recommendation_plate_input.name" name="platename" placeholder="Nome del piatto" />
												<button class="search_restaurant_button" ng-click="searchPlateForAnswer(answer_recommendation_plate_input.name)"
												><i class="fa fa-search"></i></button>
											</div>

											<div class="plates_answer_list">
												<ul class="list-unstyled dashboard_list">
													<li ng-repeat="plate in answer_plates track by $index"
														ng-click="selectPlate($index)">
														<div class="dashboard_friend">
										            		<div class="element_header">
										            			<div class="pull-left">
												            		<img class="profile_image"
												            		ng-src="{{staticURL}}/menu/square/{{plate.image}}.png" alt="" title=""
												            		ng-if="plate.image && plate.image != ''" />
												                    <img class="profile_image"
												                    ng-src="/img/defaultsearch.jpg" alt="" title=""
												                    ng-if="!plate.image || plate.image == ''" />
												                </div>
											                    <div class="col-sm-10 col-xs-9">
												                    {{plate.name}}<br />
												            		<span class="date">{{plate.description}}</span>
											            		</div>
											            		<div class="clearfix"></div>
										            		</div>
										            	</div>
													</li>
												</ul>
											</div>
										</div>



										<div class="clearfix"></div>
										<div ng-if="answer_step_block == 3">
											<label>Messaggio</label>
											<textarea class="activated_input full restaurant_search_message" style="margin-bottom: 10px;"
												ng-model="answer_recommendation_text_input.name" name="textname"></textarea>
											<button class="confirm_searched"
											ng-click="submitRecommendationAnswer(answer_recommendation_text_input.name)"
											>Conferma</button>
											<button class="delete_searched"
											ng-click="resetRecommendationAnswer()"
											>Annulla</button>
										</div>
									</div>
									<!-- /steps per rispondere alle raccomandazioni -->


			            			<!-- lista risposte -->
									<div class="answers_request_list" ng-if="view_requests_answers.recommendation_id == recommendation._id">
										<ul class="list-unstyled">
											<li ng-repeat="answer in current_recommendation_list_view">
												<div>
													<img class="profile_image answers_image"
													ng-src="{{staticURL}}/menu/square/{{answer.ref_obj.image}}.png"
													ng-if="recommendation.type == 1 && answer.ref_obj.image && answer.ref_obj.image != ''"
													/>
													<img class="profile_image answers_image"
													ng-src="{{staticURL}}/restaurant/square/{{answer.ref_obj.image}}.png"
													ng-if="recommendation.type != 1 && answer.ref_obj.image && answer.ref_obj.image != ''"
													/>
													<img class="profile_image answers_image"
								                    ng-src="/img/defaultsearch.jpg" alt="" title=""
								                    ng-if="!answer.ref_obj.image || answer.ref_obj.image == ''" />
								                    <span ng-if="recommendation.type != 1"><a class="main_link" href="/{{answer.ref_obj.city}}/{{answer.ref_obj.slug}}/">{{answer.ref_obj.name}} ({{answer.ref_obj.address}})</a></span>
													<span ng-if="recommendation.type == 1"><a class="main_link" href="/piatto/{{answer.ref_obj._id}}/">{{answer.ref_obj.name}} </a></span>


													<div class="clearfix"></div>

													<div class="answer_user">
														<img
														ng-src="{{staticURL}}/users/square/{{answer.user_obj.avatar}}.png"
														class="profile_image answers_image small_image_answer"
														ng-if="answer.user_obj.avatar && answer.user_obj.avatar != ''"
														/>
														<img
														ng-src="/img/default-avatar.png"
														class="profile_image answers_image small_image_answer"
														ng-if="!answer.user_obj.avatar || answer.user_obj.avatar == ''"
														/>
														<a href="/u/{{answer.user_obj._id}}/">{{answer.user_obj.name}} {{answer.user_obj.surname}}</a>
														<div class="clearfix"></div>
														<div class="plate_comment">
								            				<p>{{answer.text}}</p>
								            			</div>
								            		</div>
												</div>

												<div class="clearfix"></div>
											</li>
										</ul>
										<div class="clearfix"></div>
										<p class="info_and_button" ng-if="load_more_answers_show" ng-click="loadMoreAnswers()">
											Carica altre risposte
										</p>
									</div>
									<!-- /lista risposte -->
			            		</div>
			            	</div>
							<div class="clearfix"></div>

						</li>
					</ul>
					<p class="text-center">
				        <a class="" ng-click="loadRecommendation('loadRight')"
				        ng-if="!stop_right_pagination"
				            role="button">Altre</a>
				    </p>
				</div>
				<div class="col-sm-6 col-sm-push-3" ng-if="!lat || !lng">
					<h3 class="text-center">Imposta la tua posizione per vedere le raccomandazioni</h3>
				</div>
			</div>

			<div ng-if="recommendation_tab == 1 || !itsme">
				<div class="col-sm-6">
					<ul class="list-unstyled dashboard_list">
						<li ng-repeat="recommendation in user_rec track by $index" ng-if="recommendation.ref_obj">
							<div class="dashboard_following">
			            		<div class="element_header">
			            			<div class="pull-left">
					            		<img class="profile_image"
					                    ng-src="{{staticURL}}/users/square/{{recommendation.user_obj.avatar}}.png" alt="" title=""
										ng-if="recommendation.user_obj.avatar && recommendation.user_obj.avatar != ''" />
					                    <img class="profile_image"
					                    ng-src="/img/default-avatar.png" alt="" title=""
					                    ng-if="!recommendation.user_obj.avatar || recommendation.user_obj.avatar == ''" />
					                </div>
				                    <div class="col-sm-10 col-xs-9" ng-if="recommendation.type == 0 || !recommendation.type">
					                    <span class="text-primary"><a href="/u/{{recommendation.user_obj._id}}/" class="text-primary">{{recommendation.user_obj.name}} {{recommendation.user_obj.surname}}</a></span>
					                    ha raccomandato il ristorante <a href="/{{recommendation.ref_obj.city}}/{{recommendation.ref_obj.slug}}/">
					                    {{recommendation.ref_obj.name}}
										</a><br />
					            		<span class="date">{{recommendation.created_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
					            		<div class="plate_comment">
				            				<p>{{recommendation.text}}</p>
				            			</div>
				            		</div>
				            		<div class="col-sm-10 col-xs-9" ng-if="recommendation.type == 1">
					                    <span class="text-primary"><a href="/u/{{recommendation.user_obj._id}}/" class="text-primary">{{recommendation.user_obj.name}} {{recommendation.user_obj.surname}}</a></span>
					                    ha raccomandato il piatto <a href="/piatto/{{recommendation.ref_obj._id}}/">{{recommendation.ref_obj.name}}
										</a><br />
					            		<span class="date">{{recommendation.updated_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
				            			<div class="plate_comment">
				            				<p>{{recommendation.text}}</p>
				            			</div>
				            		</div>

				            		<div class="clearfix"></div>
			            		</div>
			            		<div class="element_body" ng-if="recommendation.ref_obj.image && recommendation.ref_obj.image != ''">
			            			<div class="main_image_action">
				            			<img class="full"
										ng-src="{{staticURL}}/restaurant/square/{{recommendation.ref_obj.image}}.png" alt="" title=""
										ng-if="recommendation.ref_obj.image && recommendation.ref_obj.image != '' && (recommendation.type == 0 || !recommendation.type)" />
										<img class="full"
										ng-src="{{staticURL}}/menu/square/{{recommendation.ref_obj.image}}.png" alt="" title=""
										ng-if="recommendation.ref_obj.image && recommendation.ref_obj.image != '' && recommendation.type == 1" />
				            		</div>

			            		</div>
			            	</div>
							<div class="clearfix"></div>

						</li>
					</ul>
					<div class="clearfix"></div>
					<p class="text-center">
				        <a class="" ng-click="loadRecommendation('loadUser')"
				        ng-if="!stop_load_by_user_pagination"
				            role="button">Altre</a>
				    </p>
				</div>

				<div class="col-sm-6">
					<ul class="list-unstyled dashboard_list">
						<li ng-repeat="recommendation in user_req track by $index">
							<div class="dashboard_following">
			            		<div class="element_header">
			            			<div class="pull-left">
					            		<img class="profile_image"
					                    ng-src="{{staticURL}}/users/square/{{recommendation.user_obj.avatar}}.png" alt="" title=""
										ng-if="recommendation.user_obj.avatar && recommendation.user_obj.avatar != ''" />
					                    <img class="profile_image"
					                    ng-src="/img/default-avatar.png" alt="" title=""
					                    ng-if="!recommendation.user_obj.avatar || recommendation.user_obj.avatar == ''" />
					                </div>
				                    <div class="col-sm-10 col-xs-9">
					                    <span class="text-primary"><a href="/u/{{recommendation.user_obj._id}}/" class="text-primary">{{recommendation.user_obj.name}} {{recommendation.user_obj.surname}}</a></span>
					                    sta cercando un
					                    <span ng-if="recommendation.type == 0">ristorante</span>
					                    <span ng-if="recommendation.type == 1">piatto</span><br />
					            		<span class="date">{{recommendation.created_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
					            		<div class="plate_comment">
				            				<p>{{recommendation.text}}</p>
				            			</div>
				            		</div>
				            		<div class="clearfix"></div>
			            		</div>
			            		<div class="clearfix"></div>
			            		<div class="element_body">
			            			<div class="recommendation_answers">
			            				<p class="action_texts">
				            				<a class="open_comments" ng-click="openAnswers('user_req',$index,recommendation._id)">
												<i class="fa fa-comments"></i>{{recommendation.answers_num}}
											</a>
											<a class="open_recommend pull-right" ng-click="pushAnswer('user_req',$index,recommendation._id)">
												Consiglia un
												<span ng-if="recommendation.type==0">Ristorante</span>
												<span ng-if="recommendation.type==1">Piatto</span>
											</a>
										</p>
			            			</div>


			            			<!-- steps per rispondere alle raccomandazioni -->
									<div class="publish_recommendation_request" ng-if="isAskingRequestFor(recommendation._id)">
										<!-- ristorante scelto -->
										<ul class="list-unstyled dashboard_list" ng-if="(current_answering.restaurant || current_answering.restaurant == 0) && current_answering.plate != 0 && !current_answering.plate">
											<li>
												<div class="dashboard_friend">
								            		<div class="element_header">
								            			<div class="pull-left">
										            		<img class="profile_image"
										            		ng-src="{{staticURL}}/restaurant/square/{{answer_restaurants[current_answering.restaurant].image}}.png" alt="" title=""
										            		ng-if="answer_restaurants[current_answering.restaurant].image && answer_restaurants[current_answering.restaurant].image != ''" />
										                    <img class="profile_image"
										                    ng-src="/img/defaultsearch.jpg" alt="" title=""
										                    ng-if="!answer_restaurants[current_answering.restaurant].image || answer_restaurants[current_answering.restaurant].image == ''" />
										                </div>
									                    <div class="col-sm-10 col-xs-9">
										                    {{answer_restaurants[current_answering.restaurant].name}}<br />
										            		<span class="date"><i class="mebi-placeholder-on-map-paper-in-perspective"></i> {{answer_restaurants[current_answering.restaurant].address}}</span>
									            		</div>
									            		<div class="clearfix"></div>
								            		</div>
								            	</div>
											</li>
										</ul>
										<div class="clearfix"></div>
										<!-- piatto scelto -->
										<ul class="list-unstyled dashboard_list"
										ng-if="current_answering.plate || current_answering.plate == 0">
											<li>
												<div class="dashboard_friend">
								            		<div class="element_header">
								            			<div class="pull-left">
										            		<img class="profile_image"
										            		ng-src="{{staticURL}}/menu/square/{{answer_plates[current_answering.plate].image}}.png" alt="" title=""
										            		ng-if="answer_plates[current_answering.plate].image && answer_plates[current_answering.plate].image != ''" />
										                    <img class="profile_image"
										                    ng-src="/img/defaultsearch.jpg" alt="" title=""
										                    ng-if="!answer_plates[current_answering.plate].image || answer_plates[current_answering.plate].image == ''" />
										                </div>
									                    <div class="col-sm-10 col-xs-9">
										                    {{answer_plates[current_answering.plate].name}}<br />
									            		</div>
									            		<div class="clearfix"></div>
								            		</div>
								            	</div>
											</li>
										</ul>



										<div ng-if="answer_step_block == 1">
											<div class="form-group search_restaurant_group">
												<input class="restaurant_search_input"
												ng-model="answer_recommendation_restaurant_input.name" name="ristoname" placeholder="Nome del ristorante" />
												<button class="search_restaurant_button" ng-click="searchRestaurantForAnswer(answer_recommendation_restaurant_input.name)"
												><i class="fa fa-search"></i></button>
											</div>

											<div class="restaurants_answer_list">
												<ul class="list-unstyled dashboard_list">
													<li ng-repeat="restaurant in answer_restaurants track by $index"
														ng-click="selectRestaurant($index)">
														<div class="dashboard_friend">
										            		<div class="element_header">
										            			<div class="pull-left">
												            		<img class="profile_image"
												            		ng-src="{{staticURL}}/restaurant/square/{{restaurant.image}}.png" alt="" title=""
												            		ng-if="restaurant.image && restaurant.image != ''" />
												                    <img class="profile_image"
												                    ng-src="/img/defaultsearch.jpg" alt="" title=""
												                    ng-if="!restaurant.image || restaurant.image == ''" />
												                </div>
											                    <div class="col-sm-10 col-xs-9">
												                    {{restaurant.name}}<br />
												            		<span class="date"><i class="mebi-placeholder-on-map-paper-in-perspective"></i> {{restaurant.address}}</span>
											            		</div>
											            		<div class="clearfix"></div>
										            		</div>
										            	</div>
													</li>
												</ul>

											</div>
										</div>



										<div ng-if="answer_step_block == 2 && answer_type == 1">

											<div class="form-group search_restaurant_group">
												<input class="restaurant_search_input"
												ng-model="answer_recommendation_plate_input.name" name="platename" placeholder="Nome del piatto" />
												<button class="search_restaurant_button" ng-click="searchPlateForAnswer(answer_recommendation_plate_input.name)"
												><i class="fa fa-search"></i></button>
											</div>

											<div class="plates_answer_list">
												<ul class="list-unstyled dashboard_list">
													<li ng-repeat="plate in answer_plates track by $index"
														ng-click="selectPlate($index)">
														<div class="dashboard_friend">
										            		<div class="element_header">
										            			<div class="pull-left">
												            		<img class="profile_image"
												            		ng-src="{{staticURL}}/menu/square/{{plate.image}}.png" alt="" title=""
												            		ng-if="plate.image && plate.image != ''" />
												                    <img class="profile_image"
												                    ng-src="/img/defaultsearch.jpg" alt="" title=""
												                    ng-if="!plate.image || plate.image == ''" />
												                </div>
											                    <div class="col-sm-10 col-xs-9">
												                    {{plate.name}}<br />
												            		<span class="date">{{plate.description}}</span>
											            		</div>
											            		<div class="clearfix"></div>
										            		</div>
										            	</div>
													</li>
												</ul>
											</div>
										</div>



										<div class="clearfix"></div>
										<div ng-if="answer_step_block == 3">
											<label>Messaggio</label>
											<textarea class="activated_input full restaurant_search_message" style="margin-bottom: 10px;"
												ng-model="answer_recommendation_text_input.name" name="textname"></textarea>
											<button class="confirm_searched"
											ng-click="submitRecommendationAnswer(answer_recommendation_text_input.name)"
											>Conferma</button>
											<button class="delete_searched"
											ng-click="resetRecommendationAnswer()"
											>Annulla</button>
										</div>
									</div>
									<!-- /steps per rispondere alle raccomandazioni -->


			            			<!-- lista risposte -->
									<div class="answers_request_list" ng-if="view_requests_answers.recommendation_id == recommendation._id">
										<ul class="list-unstyled">
											<li ng-repeat="answer in current_recommendation_list_view">
												<div>
													<img class="profile_image answers_image"
													ng-src="{{staticURL}}/menu/square/{{answer.ref_obj.image}}.png"
													ng-if="recommendation.type == 1 && answer.ref_obj.image && answer.ref_obj.image != ''"
													/>
													<img class="profile_image answers_image"
													ng-src="{{staticURL}}/restaurant/square/{{answer.ref_obj.image}}.png"
													ng-if="recommendation.type != 1 && answer.ref_obj.image && answer.ref_obj.image != ''"
													/>
													<img class="profile_image answers_image"
								                    ng-src="/img/defaultsearch.jpg" alt="" title=""
								                    ng-if="!answer.ref_obj.image || answer.ref_obj.image == ''" />
								                    <span ng-if="recommendation.type != 1"><a class="main_link" href="/{{answer.ref_obj.city}}/{{answer.ref_obj.slug}}/">{{answer.ref_obj.name}} ({{answer.ref_obj.address}})</a></span>
													<span ng-if="recommendation.type == 1"><a class="main_link" href="/piatto/{{answer.ref_obj._id}}/">{{answer.ref_obj.name}} </a></span>


													<div class="clearfix"></div>

													<div class="answer_user">
														<img
														ng-src="{{staticURL}}/users/square/{{answer.user_obj.avatar}}.png"
														class="profile_image answers_image small_image_answer"
														ng-if="answer.user_obj.avatar && answer.user_obj.avatar != ''"
														/>
														<img
														ng-src="/img/default-avatar.png"
														class="profile_image answers_image small_image_answer"
														ng-if="!answer.user_obj.avatar || answer.user_obj.avatar == ''"
														/>
														<a href="/u/{{answer.user_obj._id}}/">{{answer.user_obj.name}} {{answer.user_obj.surname}}</a>
														<div class="clearfix"></div>
														<div class="plate_comment">
								            				<p>{{answer.text}}</p>
								            			</div>
								            		</div>
												</div>

												<div class="clearfix"></div>
											</li>
										</ul>
										<div class="clearfix"></div>
										<p class="info_and_button" ng-if="load_more_answers_show" ng-click="loadMoreAnswers()">
											Carica altre risposte
										</p>
									</div>
									<!-- /lista risposte -->
			            		</div>
			            	</div>
							<div class="clearfix"></div>

						</li>
					</ul>
				</div>
			</div>

			<div ng-if="recommendation_tab == 2 && itsme">
				<div class="col-sm-6 col-sm-push-3">
					<div class="ask_recommendation_form">
						<form id="askRequestForm" class="">
				      		<p>Richiedi una raccomandazione alla community di MangiaeBevi+</p>
				      		<p>
				      			<span
				      				ng-click="setNotShow(1)"
				      				ng-class="{'text-primary': not_show_address == 1 || !not_show_address}">
				          			Inserisci l'indirizzo
				          		</span>
				          		<span ng-click="setNotShow(2)"
				          			ng-class="{'text-primary': not_show_address == 2}"
				          			ng-if="current_user.user.loc.coordinates">
				          			<i class="mebi-placeholder-on-map-paper-in-perspective"></i> Usa la tua posizione
				          		</span>
				          	</p>
				            <div class="form-group" ng-if="not_show_address != 2 || !current_user.user.loc.coordinates">
				              <label>Indirizzo</label><br />
				              <input type="text" name="address" class="activated_input full" ng-model="recommendation_request_ask.address"
								ng-autocomplete
								details="details"
								class="form-control"
								placeholder="Indirizzo" />
				            </div>
				            <div class="form-group relative">
				              	<label>Testo</label><br />
				      			<textarea msd-elastic class="activated_input full" type="text" ng-model="recommendation_request_ask.text" required></textarea>
				            </div>
				            <div class="form-group relative">
				              	<label>Cosa cerchi?</label><br />
				      			<span class="fasce_span"
				      			ng-click="setRecType(0)"
				      			ng-class="{'active': !recommendation_request_ask.type || recommendation_request_ask.type == 0}">
							      	Ristorante
							    </span>
							    <span class="fasce_span"
							    ng-click="setRecType(1)"
							    ng-class="{'active': recommendation_request_ask.type == 1}">
							      	Piatto
							    </span>
				            </div>
				        </form>
				        <button ng-disabled="askRequestForm.$invalid" class="outline_btn bprimary" ng-click="askRecommendation()">
				        	<span>Conferma</span>
				        </button>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>