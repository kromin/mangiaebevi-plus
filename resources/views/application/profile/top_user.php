<div ng-controller="UserTopProfileCtrl">
	<div id="main_cover" class="main_image relative hidden show_on_load" style="background-size: cover; background-image: url(<?php echo $user_array['cover'];?>);" ng-init="init('<?php echo $user_array['_id'];?>')">
		<div class="overlay"></div>
		<div class="main_container">			
			<!--mebi-technology-1-->	
			<div class="main_section_menu">
				<div class="container user_info_container">
					<div class="row">						
						<?php if($isme) : ?>
						<div class="col-sm-5 user_location default text-center">
							<?php if($user_array['address'] != ''): ?>
							<p><?php echo htmlspecialchars( $user_array['address'] );?>	</p>
							<?php else: ?>
							<p>Completa il tuo profilo per guadagnare foodcoins</p>
							<?php endif; ?>
						</div>						
						<div class="col-sm-2 text-center">
							<span class="hint--bottom" aria-label="Inserisci/Modifica la tua posizione attuale" 
							ng-click="setEditAddress()">
								<button class="custom_btn round" style="top: -3px;">								
									<i class="mebi-placeholder-on-map-paper-in-perspective"></i>
								</button>
							</span>
						</div>
						<?php endif; 
						$cols = ($isme) ? 'col-sm-5' : 'col-sm-12';
						?>
						<div class="<?php echo $cols;?> user_location current text-center">
							<p id="user_current_address"><?php echo htmlspecialchars( $user_array['current_address'] );?></p>
						</div>

					</div>
					<div class="row user_profile_main_images">
						<div class="col-md-3 hidden-sm hidden-xs">
							<div class="followers rounded_num">
								<span class="number" id="followers_num">
									<?php echo $user_array['followers'];?>								
								<span>
								<?php /*<i class="mebi-android-done pointer" ng-click="addRemoveFollowing()" ng-if="following == 0"></i>
								<i class="mebi-close pointer" ng-click="addRemoveFollowing()" ng-if="following == 1"
								style="line-height: 110px; font-size: 36px;"></i>*/?>
							</div>
							<p class="text-center">Follower</p>
						</div>
						<div class="col-md-6 ">
							<div class="user_avatar">								
								<?php if ($isme) : ?>
								<div class="update_user_avatar_image">
									<img id="avatar_container" class="profile_image img-circle" src="<?php echo $user_array['avatar'];?>" />
									<input class="upload_hidden"
									type="file" 
									ngf-select 
									ng-model="change_avatar.avatar" name="change_avatar" ng-change="uploadAvatar()" title="Cambia" 
									/>
									<div class="upload_overlay"><i class="fa fa-upload"></i></div>
								</div>
								<?php else: ?>
									<img id="avatar_container" class="profile_image img-circle" src="<?php echo $user_array['avatar'];?>" />
								<?php endif; ?>
							</div>
							<?php 
							if($isme):
								?>
								<div class="upload_parent" style="position: relative; margin: 30px 0;">
									<div class="upload_viewed text-center">
										<button class="upload_cover_btn">
					                      	<span>Immagine di copertina</span>
					                    </button>
									</div>
									<input class="upload_hidden"
									type="file" 
									ngf-select 
									ng-model="change_cover.cover" name="change_cover" ng-change="uploadCover()" title="Cambia" 
									style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; opacity: 0; cursor: pointer;" />
								</div>
							<?php
							endif;
							?>
							<h1 class="profile_name"><?php echo htmlspecialchars( $user_array['name'] ).' '.htmlspecialchars( $user_array['surname'] );?></h1>
							<p class="text-center"><?php echo return_simple_text_br( htmlspecialchars( $user_array['description'] ) );?></p>
							<p class="text-center visible-sm visible-xs">follower: <span id="followers_num_string"><?php echo $user_array['followers'];?></span></p>
							<p class="text-center visible-sm visible-xs">following: <span id="following_num_string"><?php echo $user_array['following'];?></span></p>

							<p class="friend_container margin_big_v text-center">
								<hr />
								<?php /*
								<?php if($user_array['friends'] == 1):?>
								<p class="text-center"><span id="friends_num"><?php echo $user_array['friends'];?></span> AMICO</p>
								<?php else : ?>
								<p class="text-center"><span id="friends_num"><?php echo $user_array['friends'];?></span> AMICI</p>
								<?php endif; ?>
								<button ng-if="can_add == 1" 
								ng-click="addToFriends()"
								class="custom_btn outline text-center" 
								style="margin: 5px auto; display: inherit;">
								<i class="mebi-android-add"></i> 
									<span>Aggiungi agli</span>
									amici</button>
								*/?>
							<?php if (!$isme) { ?>
								<button 
									ng-if="following != 2"
									ng-click="addRemoveFollowing()"
									class="custom_btn outline text-center" 
									style="margin: 5px auto; display: inherit;">
									<i class="mebi-android-done pointer" ng-if="following == 0"></i>
									<i class="mebi-close pointer" ng-if="following == 1"></i>
									<span ng-if="following == 0">Segui</span>
									<span ng-if="following == 1">Non seguire più</span>
								</button>
							<?php } ?>
							</p>
						</div>
						<div class="col-md-3 hidden-sm hidden-xs">
							<div class="following rounded_num">
								<span class="number">
									<?php echo $user_array['following'];?>
								</span>
							</div>
							<p class="text-center">Following</p>
						</div>
					</div>
					<div class="row"></div>
				</div>
			</div>
		</div>
	</div>
</div>