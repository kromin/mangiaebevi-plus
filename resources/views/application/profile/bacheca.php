<div ng-controller="UserDashboardCtrl">
	<div class="row">
		<div class="col-sm-8 col-sm-push-2" ng-init="init('<?php echo $user_array['_id'];?>')">
			<h2 class="text-center section_title">Bacheca</h2>
		</div>
	</div>
	<div class="grey_bg">
		<div class="container">			
			<div class="col-sm-6 col-sm-push-3">				
				<ul class="list-unstyled dashboard_list">
		            <li ng-repeat="action in user_dashboard track by $index">	

		            	<div ng-if="action.type == 0 || !action.type" class="dashboard_friend">
		            		<div class="element_header">
		            			<div class="pull-left">
				            		<img class="profile_image" 
				                    ng-src="{{staticURL}}/users/square/{{action.user_obj.avatar}}.png" alt="" title="" 
				                    ng-if="action.user_obj.avatar && action.user_obj.avatar != ''" />
				                    <img class="profile_image" 
				                    ng-src="/img/default-avatar.png" alt="" title="" 
				                    ng-if="!action.user_obj.avatar || action.user_obj.avatar == ''" />
				                </div>
			                    <div class="col-sm-10 col-xs-9">
				                    <span class="text-primary"><a href="/u/{{action.user_obj._id}}/" class="text-primary">{{action.user_obj.name}} {{action.user_obj.surname}}</a></span> 
				                    ha stretto amicizia con <a href="/u/{{action.useradd_obj._id}}/">
				            			{{action.useradd_obj.name}} {{action.useradd_obj.surname}}
				            		</a><br />
				            		<span class="date">{{action.updated_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
			            		</div>
			            		<div class="clearfix"></div>
			            		<div class="action_feedback" ng-if="action.useradd_obj.avatar && action.useradd_obj.avatar != ''">
			            			<i class="mebi-android-add"></i> 
			            			<img ng-src="{{staticURL}}/users/square/{{action.useradd_obj.avatar}}.png" class="small_round_image" />
			            		</div>
			            		<div class="clearfix"></div>
		            		</div>
		            	</div>

		            	<div ng-if="action.type == 5" class="dashboard_following">
		            		<div class="element_header">
		            			<div class="pull-left">
				            		<img class="profile_image" 
				                    ng-src="{{staticURL}}/users/square/{{action.user_obj.avatar}}.png" alt="" title="" 
				                    ng-if="action.user_obj.avatar && action.user_obj.avatar != ''" />
				                    <img class="profile_image" 
				                    ng-src="/img/default-avatar.png" alt="" title="" 
				                    ng-if="!action.user_obj.avatar || action.user_obj.avatar == ''" />
				                </div>
			                    <div class="col-sm-10 col-xs-9">
				                    <span class="text-primary"><a href="/u/{{action.user_obj._id}}/" class="text-primary">{{action.user_obj.name}} {{action.user_obj.surname}}</a></span> 
				                    ha iniziato a seguire <a href="/u/{{action.useradd_obj._id}}/">
				            			{{action.useradd_obj.name}} {{action.useradd_obj.surname}}
				            		</a><br />
				            		<span class="date">{{action.updated_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
			            		</div>
			            		<div class="clearfix"></div>
			            		<div class="action_feedback" ng-if="action.useradd_obj.avatar && action.useradd_obj.avatar != ''">
			            			<i class="mebi-eye"></i> 
			            			<img ng-src="{{staticURL}}/users/square/{{action.useradd_obj.avatar}}.png" class="small_round_image" />
			            		</div>
			            		<div class="clearfix"></div>
		            		</div>
		            	</div>

		            	<div ng-if="action.type == 1" class="dashboard_following">
		            		<div class="element_header">
		            			<div class="pull-left">
				            		<img class="profile_image" 
				                    ng-src="{{staticURL}}/users/square/{{action.user_obj.avatar}}.png" alt="" title="" 
				                    ng-if="action.user_obj.avatar && action.user_obj.avatar != ''" />
				                    <img class="profile_image" 
				                    ng-src="/img/default-avatar.png" alt="" title="" 
				                    ng-if="!action.user_obj.avatar || action.user_obj.avatar == ''" />
				                </div>
			                    <div class="col-sm-10 col-xs-9">
				                    <span class="text-primary"><a href="/u/{{action.user_obj._id}}/" class="text-primary">{{action.user_obj.name}}  {{action.user_obj.surname}}</a></span> 
				                    ha raccomandato il ristorante <a href="/{{action.restaurant_obj.city}}/{{action.restaurant_obj.slug}}/">{{action.restaurant_obj.name}}
									</a><br />
				            		<span class="date">{{action.updated_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
			            		</div>
			            		<div class="clearfix"></div>
			            		<div class="clearfix"></div>
		            		</div>
		            		<div class="element_body" ng-if="action.restaurant_obj.image && action.restaurant_obj.image != ''">
		            			<div class="main_image_action">
			            			<img class="full" 
									ng-src="{{staticURL}}/restaurant/medium/{{action.restaurant_obj.image}}.png" alt="" title="" />
			            		</div>
		            		</div>
		            	</div>


		            	<div ng-if="action.type == 2" class="dashboard_following">
		            		<div class="element_header">
		            			<div class="pull-left">
				            		<img class="profile_image" 
				                    ng-src="{{staticURL}}/users/square/{{action.user_obj.avatar}}.png" alt="" title="" 
				                    ng-if="action.user_obj.avatar && action.user_obj.avatar != ''" />
				                    <img class="profile_image" 
				                    ng-src="/img/default-avatar.png" alt="" title="" 
				                    ng-if="!action.user_obj.avatar || action.user_obj.avatar == ''" />
				                </div>
			                    <div class="col-sm-10 col-xs-9">
				                    <span class="text-primary"><a href="/u/{{action.user_obj._id}}/" class="text-primary">{{action.user_obj.name}}  {{action.user_obj.surname}}</a></span> 
				                    ha raccomandato il piatto <a href="/piatto/{{action.plate_obj._id}}/">{{action.plate_obj.name}}
									</a><br />
				            		<span class="date">{{action.updated_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
			            		</div>
			            		<div class="clearfix"></div>
			            		<div class="clearfix"></div>
		            		</div>
		            		<div class="element_body" ng-if="action.plate_obj.image && action.plate_obj.image != ''">
		            			<div class="main_image_action">
			            			<img class="full" 
									ng-src="{{staticURL}}/menu/medium/{{action.plate_obj.image}}.png" alt="" title="" />
			            		</div>
		            		</div>
		            	</div>
		            	

		            	<div ng-if="action.type == 3" class="dashboard_following">
		            		<div class="element_header">
		            			<div class="pull-left">
				            		<img class="profile_image" 
				                    ng-src="{{staticURL}}/users/square/{{action.user_obj.avatar}}.png" alt="" title="" 
				                    ng-if="action.user_obj.avatar && action.user_obj.avatar != ''" />
				                    <img class="profile_image" 
				                    ng-src="/img/default-avatar.png" alt="" title="" 
				                    ng-if="!action.user_obj.avatar || action.user_obj.avatar == ''" />
				                </div>
			                    <div class="col-sm-10 col-xs-9">
				                    <span class="text-primary"><a href="/u/{{action.user_obj._id}}/" class="text-primary">{{action.user_obj.name}} {{action.user_obj.surname}}</a></span> 
				                    ha aggiunto il ristorante <a href="/{{action.restaurant_obj.city}}/{{action.restaurant_obj.slug}}/">{{action.restaurant_obj.name}}
									</a> ai preferiti<br />
				            		<span class="date">{{action.updated_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
			            		</div>
			            		<div class="clearfix"></div>
			            		<div class="action_feedback" ng-if="action.restaurant_obj.image && action.restaurant_obj.image != ''">
			            			<i class="mebi-android-add"></i> 
			            			<img ng-src="{{staticURL}}/restaurant/square/{{action.restaurant_obj.image}}.png" class="small_round_image" />
			            		</div>
		            		</div>
		            	</div>
		            			            	
		            	
		            	<div ng-if="action.type == 4" class="dashboard_following">
		            		<div class="element_header">
		            			<div class="pull-left">
				            		<img class="profile_image" 
				                    ng-src="{{staticURL}}/users/square/{{action.user_obj.avatar}}.png" alt="" title="" 
				                    ng-if="action.user_obj.avatar && action.user_obj.avatar != ''" />
				                    <img class="profile_image" 
				                    ng-src="/img/default-avatar.png" alt="" title="" 
				                    ng-if="!action.user_obj.avatar || action.user_obj.avatar == ''" />
				                </div>
			                    <div class="col-sm-10 col-xs-9">
				                    <span class="text-primary"><a href="/u/{{action.user_obj._id}}/" class="text-primary">{{action.user_obj.name}} {{action.user_obj.surname}}</a></span> 
				                    ha aggiunto un immagine del piatto <a href="/piatto/{{action.plate_obj._id}}/">{{action.plate_obj.name}}
									</a><br />
				            		<span class="date">{{action.updated_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
			            		</div>
			            		<div class="clearfix"></div>
		            		</div>
		            		<div class="element_body" ng-if="action.plateimage_obj.image && action.plateimage_obj.image != ''">
		            			<div class="main_image_action">
			            			<img class="full" 
									ng-src="{{staticURL}}/community/medium/{{action.plateimage_obj.image}}.png" alt="" title="" />
			            		</div>
			            		<div class="rating pull-left">
			            			<p class="text-center rating_p">{{action.plateimage_obj.rating}}</p>
			            		</div>
			            		<div class="plate_comment pull-left">
			            			<p>{{action.plateimage_obj.text}}</p>
			            		</div>
		            		</div>
		            	</div>	

		                <div class="clearfix"></div>
		            </li>
		        </ul>
			</div>	
			<div class="clearfix"></div>
		    <p class="text-center">
		        <a class="" ng-click="loadDashboard()" 
		        ng-if="!stop_load_dashboard_pagination && show_button"
		            role="button">Carica altre notizie</a>   
		    </p>	
		</div>				
	</div>
</div>