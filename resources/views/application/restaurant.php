<?php
    
require_once '_head.php';
    
require_once '_topbar.php';

?>

<div id="restaurantPage" ng-controller="RestaurantCtrl" ng-init="initPage('<?php echo $restaurant->_id;?>')">
    <script type="application/ld+json">
    	<?php 

        echo $lds;?>
    </script>
    <?php
    
    require_once 'restaurant/main.php';
    ?>
    <div class="pageContent" ng-class="{'slideDown':show_virtual_tour}" ng-init="
                        pushMapObj(
                        '<?php echo remove_quotes_and_tags($restaurant->name);?>',
                        '<?php echo (isset($restaurant->image)) ? escape_non_alphanum($restaurant->image) : '';?>',
                        '<?php echo remove_quotes_and_tags($restaurant->address);?>',
                        '<?php echo floatval($restaurant->loc['coordinates'][1]);?>',
                        '<?php echo floatval($restaurant->loc['coordinates'][0]);?>',
                        '<?php echo $restaurant->slug;?>',
                        '<?php echo urlencode($restaurant->city);?>')"
                        >
                        
        <div class="container">
            <div class="col-lg-12">
                <h1 class="page_title">
					<?php echo esc_html($restaurant->name);?>
					<small>
						<?php if(isset($restaurant_infos['tipiLocale'][0])) : ?><span><?php echo esc_html($restaurant_infos['tipiLocale'][0]);?> |</span><?php endif;?>
						<?php if(isset($restaurant_infos['tipiCucina'][0])) : ?><span> <?php echo esc_html($restaurant_infos['tipiCucina'][0]);?> |</span><?php endif;?>
						<?php if($restaurant->district && count($restaurant->district) > 0 && $restaurant->district[0] != '') :?>
						<span> <?php echo esc_html($restaurant->district[0]);?> |</span>
						<?php endif; ?>
						<span><?php echo $restaurant_infos['europrice'];?></span>
						<span class="hint--bottom" aria-label="Raccomandazioni" ng-click="scrollToRec()"> | <i class="mebi-speaking"></i> <span id="update_recommend"><?php echo $restaurant->recommendations;?></span></span>
					</small>
				</h1>
            </div>
        </div>
        <?php require_once 'restaurant/white_top.php'; ?>
        <!-- identifico il cliente -->
        <div class="grey_content">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <?php require_once 'restaurant/grey_info.php'; ?>
                    </div>


                    <!-- sidebar -->
                    <div class="col-md-4">
                        <!-- prenotazione -->
                        <?php require_once 'restaurant/reservation.php'; ?>
                        <!-- raccomandazione -->
                        <?php require_once 'restaurant/community_functions.php'; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                
            </div>
        </div>

        <div class="white_content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- blocco menu -->
                        <?php require_once 'restaurant/menu.php'; ?>
                    </div>
                    <div class="col-sm-8">
                        <?php require_once 'restaurant/images.php'; ?>

                        <?php require_once 'restaurant/reccomendation.php'; ?>
                        <div class="clearfix"></div>

                        <?php require_once 'restaurant/map.php'; ?>
                        
                        <?php require_once 'restaurant/related.php'; ?>
                        
                    </div>
                </div>
            </div>
        </div>


    </div>
    <?php require_once 'restaurant/gallery.php'; ?>
    <?php require_once 'restaurant/menu_gallery.php'; ?>
</div>
<div class="clearfix"></div>

<?php

require_once '_footer.php';
?>