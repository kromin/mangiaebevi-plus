<?php
require_once '_head.php';
require_once '_topbar.php';
?>
<div id="user-page" class="editing_profile_restaurant" ng-init="loadUser('<?php echo $user;?>','<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>')" ng-controller="UserCtrl">
	<?php require_once 'profile/top_user.php'; /*?>
	<div class="main_image relative hidden show_on_load" back-img img="{{cover_image}}" ng-if="cover_image">
		<div class="overlay"></div>
		<div class="main_container">
				
			<!--mebi-technology-1-->	
			<div class="main_section_menu">
				<div class="container user_info_container">
					<div class="row">
						<div class="col-sm-5 user_location default text-center" ng-if="itsme">
							<p ng-if="view_user.address">{{view_user.address}} 								
							</p>
							<p ng-if="!view_user.address">
								Completa il tuo profilo per guadagnare foodcoins
							</p>
						</div>
						<div class="col-sm-2 text-center" ng-if="itsme">
							<span class="hint--bottom" aria-label="Inserisci/Modifica la tua posizione attuale" 
							ng-click="setEditAddress()">
								<button class="custom_btn round" style="top: -3px;">								
									<i class="mebi-placeholder-on-map-paper-in-perspective"></i>
								</button>
							</span>
						</div>
						<div class="col-sm-5 user_location current text-center" ng-class="{'col-sm-12': !itsme}">
							<p ng-if="view_user.current_address">{{view_user.current_address}}</p>
						</div>
					</div>
					<div class="row user_profile_main_images">
						<div class="col-md-3 hidden-sm hidden-xs">
							<div class="followers rounded_num">
								<span class="number">
									{{view_user.followedby.length}}									
								<span>
								<i class="mebi-android-done pointer" ng-click="addRemoveFollowing()" ng-if="isFollowing() == 0"></i>
								<i class="mebi-close pointer" ng-click="addRemoveFollowing()" ng-if="isFollowing() == 1"
								style="line-height: 110px; font-size: 36px;"></i>
							</div>
							<p class="text-center">Followers</p>
						</div>
						<div class="col-md-6 ">
							<img class="profile_image img-circle" ng-src="{{staticURL}}/users/medium/{{view_user.avatar}}.png" ng-if="view_user.avatar && view_user.avatar != ''" />
							<h1 class="profile_name">{{view_user.name}} {{view_user.surname}}</h1>
							<p class="text-center">{{view_user.description}}</p>
							<p class="text-center visible-sm visible-xs">followers: {{view_user.followedby.length}}</p>
							<p class="text-center visible-sm visible-xs">following: {{view_user.following.length}}</p>

							<p class="friend_container margin_big_v text-center">
								<hr />
								<p class="text-center" ng-if="view_user.friends.length !=  1">{{view_user.friends.length}} AMICI</p>
								<p class="text-center" ng-if="view_user.friends.length ==  1">{{view_user.friends.length}} AMICO</p>

								
								<button ng-if="canAddToFriends() == 4" 
								ng-click="addToFriends()"
								class="custom_btn outline text-center" 
								style="margin: 5px auto; display: inherit;">
								<i class="mebi-android-add"></i> 
									<span>Aggiungi agli</span>
									<!--<span>Rimuovi dagli</span>--> 
									amici</button>
								
							</p>
						</div>
						<div class="col-md-3 hidden-sm hidden-xs">
							<div class="following rounded_num">
								<span class="number">
									{{view_user.following.length}}
								</span>
							</div>
							<p class="text-center">Following</p>
						</div>
					</div>
					<div class="row"></div>
				</div>
			</div>
		</div>
	</div>*/?>
		
	<!-- edit user -->
	<div>
		
		<div class="white_container user_profile hidden show_on_load">
			<div class="container">
				<div class="rounded_num foodcoin warning">
					<span class="number warning" id="userFoodcoin">
						<?php echo $user_array['foodcoin']; ?>								
					</span>
				</div>
				<h3 class="text-center foodcoint_text">Foodcoin</h3>
				<div class="profile_tabs">
					<ul class="list-unstyled">
						<li ng-class="{'active' : !current_tab || current_tab == 0}">
							<a href="/u/{{page_user}}/#/bacheca" class="hint--bottom" aria-label="Bacheca">
								<i class="mebi-right-menu-bars"></i></a>
							</li><!-- se sono io mostra le cose degli amici, sennò le mie -->
						<li ng-class="{'active' : current_tab == 9}" >
							<a class="hint--bottom" aria-label="Preferiti" href="/u/{{page_user}}/#/preferiti">
								<i class="fa fa-heart-o"></i>
							</a>
						</li>
						<?php if ($isme || $is_friend) :  ?>
						<li ng-class="{'active' : current_tab == 4}"><a class="hint--bottom" aria-label="Ristoranti da provare" href="/u/{{page_user}}/#/da-provare"><i class="mebi-bookmark"></i></a></li>
						<?php endif; ?>
						<li ng-class="{'active' : current_tab == 1}"><a class="hint--bottom" aria-label="Raccomandazioni" href="/u/{{page_user}}/#/raccomandazioni"><i class="mebi-speaking"></i></a></li>
						<li ng-class="{'active' : current_tab == 2}"><a class="hint--bottom" aria-label="Follower / Following" href="/u/{{page_user}}/#/follower-following" ><i class="mebi-add-contact"></i></a></li><!-- mostra azioni degli amici insieme a elenco -->
						<li ng-class="{'active' : current_tab == 3}"><a class="hint--bottom" aria-label="Immagini dei piatti" href="/u/{{page_user}}/#/immagini-dei-piatti"><i class="mebi-photo-camera"></i></a></li>
						<?php if ($isme) : ?>
						<li ng-class="{'active' : current_tab == 5}"><a class="hint--bottom" aria-label="Prenotazioni" href="/u/{{page_user}}/#/prenotazioni" ><i class="mebi-list"></i></a></li>
						<li ng-class="{'active' : current_tab == 6}"><a class="hint--bottom" aria-label="Notifiche" href="/u/{{page_user}}/#/notifiche" ><i class="mebi-alarm"></i></a></li>
						<li ng-class="{'active' : current_tab == 7}"><a class="hint--bottom" aria-label="Account" href="/u/{{page_user}}/#/account" ><i class="mebi-avatar"></i></a></li>
							<?php if(auth()->user() && auth()->user()->_id == $user_array['_id'] && auth()->user()->role >= 2) : ?>
							<li ng-class="{'active' : current_tab == 8}"><a class="hint--bottom" aria-label="I tuoi ristoranti" href="/u/{{page_user}}/#/ristoranti" ><i class="mebi-romantic-date"></i></a></li>
							<?php endif; ?>
						<?php endif; ?>
					</ul>
				</div>
			</div>


			<div ng-if="current_tab == 0 || !current_tab">
				<?php require_once 'profile/bacheca.php'; ?>
			</div>
			<?php if ($isme || $is_friend) : //or is friend ?>
			<div ng-if="current_tab == 4">
				<?php require_once 'profile/da_provare.php'; ?>
			</div>
			<?php endif; ?>
			<div ng-if="current_tab == 9" ng-init="initFavourite()">
				<?php require_once 'profile/preferiti.php'; ?>
			</div>
			
			<div ng-if="current_tab == 1">
				<?php require_once 'profile/raccomandazioni.php'; ?>
			</div>

			<div ng-if="current_tab == 2">
				<?php require_once 'profile/amici.php'; ?>
			</div>

			<div ng-if="current_tab == 3">
				<?php require_once 'profile/plates.php'; ?>
			</div>
		
			<?php if ($isme) : ?>
			<div ng-if="current_tab == 5">
				<?php require_once 'profile/prenotazioni.php';?>				
			</div>

			<div ng-if="current_tab == 6">
				<?php require_once 'profile/notifiche.php'; ?>
			</div>

			<div ng-if="current_tab == 7">
				<?php require_once 'profile/account.php'; ?>	                	                
			</div>

			<?php if(auth()->user() && auth()->user()->_id == $user_array['_id'] && auth()->user()->role >= 2) : ?>
			<div ng-if="current_tab == 8">
				<?php require_once 'profile/ristoranti.php'; ?>                	                
			</div>
			<?php 
				endif; 
				endif; 
			?>

			</div>
		</div>
	</div>
	<div>
		<div class="clearfix"></div>
	</div>
</div>


<?php
require_once '_footer.php';
