<?php
require_once '_head.php';
$blue_header = 1;
require_once '_topbar.php';
//$plate_encoded = str_replace("\"","\\\"",str_replace("'","\'",json_encode(json_decode(json_encode($pl),true))));
?>
<div id="platePage" class="white_page" ng-init="initPage('<?php echo $pl->_id;?>')" ng-controller="PlateCtrl">
    <div class="container">
    	<?php if($pl->image && $pl->image != ''): ?>
        <div class="text-center">
            <img ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/menu/small/<?php echo $pl->image;?>.png" alt="" title="" class="" />
        </div>
    	<?php endif; ?>
        <div>
            <h1 class="text-center"><?php echo $pl->name;?></h1>
            <p class="text-center"><?php echo $pl->description;?></p>
            <?php /*if($pl->deleted && $pl->deleted == 1): ?>
            <p class="text-danger text-center">Questo piatto non è più nel menu</p>
        	<?php endif;*/ ?>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-6">  
                <?php if($pl->ingredienti && is_array($pl->ingredienti) && !empty($pl->ingredienti) ): ?>           	
                    <div>
                        <p class="text-center"><label style="display: block;">Ingredienti</label>
                    	<?php 
                    		foreach($pl->ingredienti as $ing):
    	                	?>
    	                    <span class="fasce_span info"><?php echo $ing;?></span>
    	                	<?php endforeach; ?>
    	                	</p> 
                    </div>    
                <?php endif;?>
                <?php if($pl->allergeni && is_array($pl->allergeni) && !empty($pl->allergeni) ): ?>           
                <div>
                    <p class="text-center"><label style="display: block;">Allergeni</label>
                	<?php
            		foreach($pl->allergeni as $all):
                	?>
                        <span class="fasce_span warning"><?php echo $all;?></span>
                    <?php endforeach; ?>
	                </p>	                            
                </div>
                <?php endif;?>
            </div>
            <div class="col-sm-6">
                <p class="text-center"><label>Ristorante</label></p>
                <div class="col-lg-2 col-md-2 col-xs-3">
                	<?php if($restaurant->image && $restaurant->image != '') : ?>
                    <img class="img-circle" style="width: 85px; margin-top: 8px;"
                    ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/restaurant/square/<?php echo $restaurant->image;?>.png" alt="" title="" />
                	<?php endif;?>
                </div>
                <div class="col-lg-10 col-md-10 col-xs-9">
                    <h3><a href="/<?php echo $restaurant->city;?>/<?php echo $restaurant->slug;?>/"><?php echo htmlspecialchars( $restaurant->name );?></a></h3>
                    <p><?php echo htmlspecialchars( $restaurant->address );?></p>                                            
                </div>   
            </div>
        </div>
        <div class="recommend_plate text-center">
            <a class="text-primary" ng-click="recommendPlate()" >Vuoi raccomandare questo piatto?</a>  
        </div>
        <div class="plate_price text-center plate_single_price">
            € <?php echo format_price($pl->price);?>
        </div>
        
    </div>


    <div>
        <div class="row">
            <div class="minimal_tabs">
                <ul class="list-unstyled">
                    <li ng-class="{'active' : !plate_tab || plate_tab == 0}"><a ng-click="setPlateTab(0)">Raccomandazioni</a></li>
                    <li ng-class="{'active' : plate_tab == 1}"><a ng-click="setPlateTab(1)">Dalla community</a></li>                             
                </ul>
            </div>
        </div>
        <div class="grey_bg">
            <div class="container"> 
                <div class="social_elements">                        
                    <div class="recommendation" ng-if="plate_tab == 0 || !plate_tab">
                        <div class="col-sm-6 col-sm-push-3">
                            <ul class="list-unstyled dashboard_list" ng-init="loadPlateRecommendations()">
                                <li ng-repeat="recommendation in plate_rec track by $index">
                                    <div class="dashboard_following">
                                        <div class="element_header">
                                            <div class="pull-left">
                                                <img class="profile_image" 
                                                ng-src="{{staticURL}}/users/square/{{recommendation.user_obj.avatar}}.png" alt="" title="" 
                                                ng-if="recommendation.user_obj.avatar && recommendation.user_obj.avatar != ''" />
                                                <img class="profile_image" 
                                                ng-src="/img/default-avatar.png" alt="" title="" 
                                                ng-if="!recommendation.user_obj.avatar || recommendation.user_obj.avatar == ''" />
                                            </div>
                                            <div class="col-sm-10 col-xs-9" ng-if="recommendation.type == 1">
                                                <span class="text-primary">{{recommendation.user_obj.name}}</span><br />
                                                <span class="date">{{recommendation.updated_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
                                                <div class="plate_comment">
                                                    <p>{{recommendation.text}}</p>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>                                        
                                    </div>                                                  
                                    <div class="clearfix"></div>
                                </li>
                            </ul>   
                            <p class="text-center">
                                <a class="" ng-click="loadRecommendation()" 
                                ng-if="!stop_load_rec_pagination"
                                    role="button">Carica altre raccomandazioni</a>   
                            </p>                             
                            
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="community_plate" ng-if="plate_tab == 1">
                        <div class="text-center">
                            <p class="add_plate_image_p" ng-click="addImage()"><i class="mebi-technology-1 add_plate_icon ng-scope" ng-if="!add_plate_obj.image"></i><br />
                                Carica una immagine del piatto
                            </p>                    
                        </div>
                        <div class="col-sm-6 col-sm-push-3">
                            <ul class="list-unstyled dashboard_list" ng-init="loadPlateImages()">
                                <li ng-repeat="image in plate_images track by $index">                                                       
                                    <div class="element_header">
                                        <div class="pull-left">
                                            <img class="profile_image" 
                                            ng-src="{{staticURL}}/users/square/{{image['user_obj'].avatar}}.png" alt="" title="" 
                                            ng-if="image['user_obj'].avatar && image['user_obj'].avatar != ''" />
                                            <img class="profile_image" 
                                            ng-src="/img/default-avatar.png" alt="" title="" 
                                            ng-if="!image['user_obj'].avatar || image['user_obj'].avatar == ''" />
                                        </div>
                                        <div class="col-sm-10 col-xs-9">
                                            <a href="/piatto/{{image['user_obj']._id}}/">{{image['user_obj'].name}}</a><br />
                                            <span class="date">{{image.updated_at| amDateFormat:'dddd, DD MMMM YYYY, hh:mm'}}</span>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="element_body">
                                        <div class="main_image_action">
                                            <img class="full" 
                                            ng-src="{{staticURL}}/community/medium/{{image.image}}.png" alt="" title="" />
                                        </div>
                                        <div class="rating pull-left">
                                            <p class="text-center rating_p">{{image.rating}}</p>
                                        </div>
                                        <div class="plate_comment pull-left">
                                            <p>{{image.text}}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                    </div>                        

                                            
                                    <div class="clearfix"></div>
                                </li>
                            </ul>
                        </div>
                        <p class="text-center">
                                <a class="" ng-click="loadPlateImagesN()" 
                                ng-if="!stop_load_images_pagination"
                                    role="button">Carica altre immagini</a>   
                            </p>  
                        <div class="clearfix"></div>                        
                    </div>
                </div>                
                <div class="clearfix"></div>
            </div>            
        </div>
    </div>







	<div class="menu_add_by_step custom_scrollbar" ng-class="{'open' : editing_menu}">
	    <i class="mebi-close menu_step_close" ng-click="closeEditing()"></i>
	    
	    <div class="content_menu_adding text-center">
	        <!-- add plate -->
	        <h3 class="uppercase">
	            Aggiungi un'immagine per la community
	        </h3>

	        <p ng-if="!add_plate.image">Clicca sull'icona per aggiungere una immagine</p>
	        <div class="add_plate_image_container">
	            <input class="upload-input" type="file" ngf-select 
	                    ng-model="add_plate_image.image" 
	                    name="add_plate_image" 
	                    ng-change="uploadPlateImage()" title="Immagine" />
	            <i class="mebi-technology-1 add_plate_icon" ng-if="!add_plate_obj.image"></i>
	            <img ng-src="{{staticURL}}/community/small/{{add_plate_obj.image}}.png" alt="" title="" ng-if="add_plate_obj.image" />
	        </div>
	                        
	                   
	        <p>Commento</p>
	        <textarea msd-elastic ng-model="add_plate_obj.text"></textarea>  
	        <!-- prezzo -->
	        <p>Voto</p>
	        <div class="plate_rating">
	            <i class="mebi-minus" ng-click="changeRate('less')"></i>
	            <span>{{add_plate_obj.rating}}</span>
	            <i class="mebi-add-circular-button" ng-click="changeRate('add')"></i>
	        </div>
	        <p><a class="btn ghost" ng-click="confirmAddPlateImage()">
	            Conferma</a>
	        </p>
	    </div>
	</div>
</div>
<?php
require_once '_footer.php';
?>