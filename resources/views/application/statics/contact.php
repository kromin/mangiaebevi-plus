<div id="staticPage" ng-controller="StaticCtrl">
	<section class="bg_image vcenter_parent full-height" back-img img="/img/contact.jpg">
		<div class="overlay"></div>
		<div class="vcenter_child big_width">
			<div class="row">
				<h1 class="center white-text" style="padding: 0;">Contattaci <br /><small>Per informazioni generali o chiarimenti compila il form</small></h1>	
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="col-sm-12">
				<h2 class="center section-title">Invia un messaggio</h2>
			</div>
			<div class="margin-big-v small_block block_auto">				
				<p ng-if="contact_error" class="text-danger">
					Qualcosa è andato storto, ricontrolla tutti i campi
				</p>
				<p ng-if="contact_ok" class="text-success">
					Messaggio inviato
				</p>
				<div class="margin-big-v">
					<form id="contactForm" ng-submit="contactSubmit()">
						<div class="form-group">
							<label for="name">Nome e cognome *</label>
							<input class="form-control" type="text" ng-model="contact.name" id="name" required/>
						</div>
						<div class="form-group">
							<label for="email">Indirizzo email *</label>
							<input class="form-control" type="email" ng-model="contact.email" id="email" required/>
						</div>
						<div class="form-group">
							<label for="phone">Telefono</label>
							<input class="form-control" type="text" ng-model="contact.phone" id="phone" />
						</div>
						<div class="form-group">
							<label for="name">Messaggio *</label>
							<textarea class="form-control" ng-model="contact.message" msd-elastic rows="3" style="line-height: 18px; padding: 12px; resize: none;">
							</textarea>
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary ghost btn-no-margin" value="invia" ng-disabled="contactForm.$invalid" />
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>