<div id="staticPage"
ng-init="
setSEO('Promuovi il tuo ristorante | MangiaeBevi+','/promuovi-il-tuo-ristorante/','/img/pages/ristoratori.jpg','/img/pages/ristoratori.jpg','','Promuovi il tuo ristorante con MangiaeBevi+, fidelizza i tuoi clienti e gestisci le prenotazioni della tua attività')"
>
	<section class="bg_image vcenter_parent full-height" back-img img="/img/pages/ristoratori.jpg">
		<div class="overlay"></div>
		<div class="vcenter_child big_width">
			<h1 class="white-text center">Promuovi il tuo ristorante <br />
				<small style="text-transform: none;">Come attrarre maggiore clientela e ottimizzare il tuo business</small>
			</h1>
		</div>
		<div class="go_down" scroll-to="#who">
			<p>
				<i class="fa fa-chevron-down"></i>
			</p>
		</div>
	</section>
	<section id="who">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 padding-big-v medium_block block_auto">
					<h3 class="center">PERCHE’ MANGIAEBEVI+</h3>
					<p>
						MangiaeBevi+ ti offre la possibilità di trarre il massimo risultato dalla presenza del tuo ristorante su Internet, aiutandoti ad accrescere e ottimizzare il tuo business e ad attrarre maggior clientela.
						<br />Una suite gestionale completa, un’attenta strategia di marketing e l’integrazione dei nostri mezzi pubblicitari con i media digitali: gli strumenti per il successo del tuo ristorante!
						<br />La possibilità di ricevere prenotazioni e pre-ordini tramite il portale MangiaeBevi+ ma anche direttamente tramite il vostro sito web.
					</p>
					<br />
					<p>
						MangiaeBevi+ vi offre non solo uno strumento avanzato di comunicazione, ma anche e soprattutto una piattaforma “business” che vi consente di gestire al meglio - e in totale autonomia - la vostra attività.
						<br /><br />Con un unico pannello di controllo avete la possibilità di gestire la vostra presenza su MangiaeBevi+, sul vostro sito web, sulla vostra pagina Facebook e sulla vostra applicazione.
						<br />
						In particolare potete:
						<br />• editare, aggiornare e pubblicare i vostri menu
						<br />• ricevere prenotazioni dei tavoli
						<br />• ricevere pre - ordinazioni
						<br />• costruire il vostro database clienti
						<br />• consultare le statistiche e conoscere meglio le abitudini della vostra clientela
						<br />• fruire del programma fedeltà per attrarre nuovi clienti

					</p>
				</div>
			</div>
			<hr />
			<div class="row margin-big-v">
				<div class="col-sm-6">
					<h3>INSERITE O RECLAMATE LA VOSTRA ATTIVITA’</h3>
					<p>Se il vostro ristorante non è ancora sul portale MangiaeBevi+ potete chiedere di aggiungerlo, se invece è già presente potete “reclamarlo”, ovvero chiedere le credenziali per gestire la vostra pagina. <br/>Fino al 31 maggio 2017 avrete a disposizione GRATUITAMENTE una piattaforma “self service”, grazie alla quale potrete disporre di tutte le funzionalità previste dal profilo “basic”.
					<br/><br/>
					Per chi volesse usufruire invece di funzionalità avanzate potrà farlo – a partire dal 1° giugno  2017 – sottoscrivendo uno dei quattro pacchetti di abbonamento previsti, effettuando l’upgrade direttamente online.

					</p>
				</div>
				<div class="col-sm-6">
					<img ng-src="/img/pages/reclama.png" alt="reclama" title="" class="" />
				</div>
			</div>
			<div class="row margin-big-v">
				<hr />

				<div class="col-sm-6">
					<img ng-src="/img/pages/reserve.png" alt="reclama" title="" class="" />
				</div>
				<div class="col-sm-6">
					<h3>PRENOTAZIONE ONLINE DEL TAVOLO</h3>
					<p>Fino al 31 maggio 2017 la prenotazione del tavolo potrà essere richiesta unicamente tramite email e il ristoratore dovrà quindi darne conferma.

					<br />A partire dal 1° giugno 2017 - esclusivamente per gli abbonati al servizio - la prenotazione del tavolo sarà “online” e verrà trasmessa in tempo reale al ristorante attraverso il gestionale MangiaeBevi+. Per questo motivo, sul suo pannello di controllo, il ristoratore dovrà destinare dei tavoli alle prenotazioni online, in modo da dare conferma immediata all’utente, automaticamente, via email o sms. La prenotazione online del tavolo è fruibile anche tramite il sito web del ristorante, la propria pagina facebook e, eventualmente, anche tramite la propria app.
					</p>
				</div>
			</div>
			<div class="row margin-big-v">
				<div class="col-sm-6">
					<h3>PRE-ORDINE E PAGAMENTO ANTICIPATO</h3>
					<p>A partire dal 1° giugno 2017 - esclusivamente per gli abbonati al servizio – sarà possibile ricevere anche pre-ordini, ovvero il cliente potrà ordinare il proprio pasto, per una determinata ora, in contemporanea con la prenotazione del tavolo. Il servizio funzionerà esclusivamente con pagamento anticipato (PayPal), garantendo al cliente di trovare il suo pasto pronto in tavola all’orario stabilito e al ristoratore di avere un tavolo garantito e la certezza del pagamento anche nel caso di una mancata presentazione (no-show) del cliente che ha ordinato, dunque un sicuro incremento delle vendite.
					<br/>Il ristoratore, inoltre, potrà in questo modo velocizzare il turn over dei tavoli e sapere in anticipo i piatti che saranno ordinati, potendo così provvedere all’approvvigionamento in caso di mancanza di uno o più ingredienti.


					</p>
				</div>
				<div class="col-sm-6">
					<img ng-src="/img/pages/reserve.png" alt="reclama" title="" class="" />
				</div>
			</div>
		</div>
	</section>
	<section class="bg_image vcenter_parent full-height" back-img img="/img/pages/ristorazione_servizi.jpg">
		<div class="overlay"></div>
		<div class="relative big_width" style="margin: 20px auto; padding: 10px;">
			<h1 class="white-text center" style="margin-bottom: 30px;"><b class="text-primary">SERVIZI</b> PER IL TUO RISTORANTE
			</h1>
			<p class="white-text center">MangiaeBevi è in grado di offrirti diversi servizi complementari per il tuo ristorante:</p>
			<p>
				<ul class=" list-in-image">
					<li>servizio fotografico degli ambienti e dei piatti in menu</li>
					<li>realizzazione video (presentazione del vostro ristorante o video ricetta)</li>
					<li>realizzazione Virtual Tour ristorante (Google Business View)</li>
					<li>creazione e gestione pagine social del ristorante (Facebook, Instagram, TripAdvisor, ecc.)</li>
					<li>realizzazione app iOS e Android, personalizzata per il ristorante</li>
					<li>applicazioni gestionali per la comanda</li>
					<li>realizzazione sito web e mobile per il vostro ristorante</li>
					<?php /*<li>email marketing personalizzato per il vostro ristorante</li>
					<li>promozione online del vostro ristorante sui principali motori di ricerca Internet</li> */ ?>
					<li>ufficio stampa </li>
				</ul>
			</p>
			<p class="white-text center">
				<br /><br />
				Per ulteriori informazioni scrivere a: <a href="mailto:servizi@mangiaebevi.it" class="text-primary">servizi@mangiaebevi.it</a><br />
				<?php /*<a class="btn ghost" href="/file/servizi-ristoranti.pdf" target="_blank">Scarica la brochure</a>*/?>
			</p>

		</div>
	</section>

</div>