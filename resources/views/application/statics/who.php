<div id="staticPage" ng-controller="StaticCtrl">
	<section class="bg_image vcenter_parent full-height" back-img img="/img/pages/chi_siamo.jpg">
		<div class="overlay"></div>
		<div class="vcenter_child big_width">
			<h1 class="white-text center">Chi siamo<br /><small>La nostra azienda e il nostro team</small>
			</h1>
		</div>
		<div class="go_down" scroll-to="#who">
			<p>
				<i class="fa fa-chevron-down"></i>
			</p>
		</div>
	</section>
	<section id="who">
		<div class="container">
			<div class="col-sm-6">
				<h2><i>Company</i></h2>
				<p>
					<i>MangiaeBevi+ è un progetto di Digikom srl, una start up innovativa che ha come focus il “food digital” e ben 11 anni di esperienza nel settore della ristorazione fuori casa con la guida e il portale “MangiaeBevi”, nonchè nei servizi di consulenza per la ristorazione, principalmente comunicazione e marketing.</i>

				</p>
			</div>
			<div class="col-sm-4 col-sm-push-1">
				<img class="img-responsive" ng-src="/img/pages/chi_siamo_home.jpg" alt="mangiaebeviplus" title="" />
			</div>
			<div class="clearfix"></div>
			<h2 class="text-center padding-big-v"><i>Executive Team</i></h2>
			<?php
			$team_members = [
			[
				"name"=> "FABIO CARNEVALI",
				"role"=> "C.E.O. & Founder",
				"description"=>"Giornalista ed editore, Fabio è il fondatore e CEO di <b>MangiaeBevi+</b>. Pioniere del digital publishing (riviste e libri su smartphone e tablet), da oltre 10 anni si occupa di comunicazione digitale, nonché di comunicazione e marketing della ristorazione. Editore e direttore del portale mangiaebevi.it, ha trasformato la celebre guida dei ristoranti “MangiaeBevi” – da lui fondata nel 2006 – in un ambito premio dedicato alle eccellenze enogastronomiche italiane."
			],
			[
				"name"=> "STEFANO FELICETTI",
				"role"=> "C.I.O. & Co-Founder",
				"description"=>"Co-fondatore di <b>MangiaeBevi+</b>, Stefano nasce illustratore, prima di dedicarsi all’Information Technology dove, da oltre 15 anni, è consulente di molte aziende. Ingegnere di sistemi software Apple e Tecnico Certificato Apple, è stato responsabile dell’assistenza dei più importanti centri autorizzati Apple di Roma. "
			],
			[
				"name"=> "IVAN SIGNORILE",
				"role"=> "C.T.O.",
				"description"=>"Ivan dirige e coordina il settore tecnico di <b>MangiaeBevi+</b>, oltre a realizzarne personalmente le applicazioni mobile. Esperto nello sviluppo Front End per portali web, Master nei linguaggi HTML5/CSS6 jQuery e nella Suite Adobe Creative, nonché profondo conoscitore dei sistemi operativi UNIX-based (Mac OS, Linux) e Windows. "
			],
			[
				"name"=> "MARCO NOBILI",
				"role"=> "Web Marketing & CRM Management",
				"description"=>"Marco è il “guru del SEO”, esperto di ricerca organica e web marketing sui motori di ricerca, sia nazionali che internazionali. In <b>MangiaeBevi+</b> ha la responsabilità di SEO e SEM, paid advertising (campagne pay per click, CPM, CPA), web e persuasive copywriting, article marketing e analytical tracking."
			]
			];
			?>
			<div class="team">
			<?php
				$n = 0;
				foreach($team_members as $member):
					$n++;
					$col = ($n <= 3) ? "col-sm-4" : "col-sm-4";
					?>
				<div class="team_member <?php echo $col;?>">
					<h3><?php echo $member['name'];?></h3>
					<h4 class="role"><?php echo $member['role'];?></h4>
					<p><?php echo $member['description'];?></p>
				</div>
				<?php
				if($n%3 == 0) echo '<div class="clearfix"></div>';
				endforeach;
			?>

			</div>
			<?php /*
			<div>
				<p>
					Oggi <b class="text-primary">MangiaeBevi</b> volta pagina. Nasce infatti “la rivista per il gourmet”, un magazine-guida rivolto prevalentemente ad un pubblico di buongustai, un movimento in continua crescita, sempre più attento alla qualità del cibo, alla sperimentazione, alle novità dell’offerta ristorativa, al buon bere.
<br />Una rivista con contenuti di interesse nazionale e locale, al tempo stesso: servizi sui ristoranti e sulle nuove aperture, anteprime di eventi e fiere, itinerari enogastronomici, interviste e sfiziose ricette proposte dai migliori chef.
<br />Una rivista con al suo interno una vera e propria guida - che varia a seconda delle edizioni, es. Roma, Milano, Napoli, ecc. - ricca di informazioni sui ristoranti (tipo di cucina, prezzi, servizi e specialità) e con una periodicità trimestrale, che consente di pubblicare notizie sempre fresche e aggiornate.

				</p>


				<h2 class="big-title-inside">Nuovo sito, nuove app</h2>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<img class="img-responsive" ng-src="/img/pages/main_who.png" alt="mangiaebevi" title="" />
				</div>
				<div class="col-sm-6 padding-big-v">
					<p>
						Il processo di “modernizzazione” (nuovo logo, nuova veste grafica)
						e di “multicanalità” (nuovo sito, nuove applicazioni per iPhone e Android) iniziato nel 2014 fa oggi di <b class="text-primary">MangiaeBevi</b> non più solo
						una guida “cartacea”, ma anche un’utilissima piattaforma web e mobile, con la geolocalizzazione,
						che consente all’utente di ricercare un ristorante secondo le proprie esigenze, consultare il menu,
						visitare il locale con il virtual tour, prenotare un tavolo.
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 padding-big-v medium_block block_auto">
					<p>
						Si sa che la tecnologia, così come i modelli di design e usabilità del web, si evolvono velocemente,
						così è stato necessario “ripensare” <b class="text-primary">MangiaeBevi</b> trasformandolo in un utilissimo strumento che raccoglie insieme più
						funzioni facilitandone l’uso.
				<br />Il risultato lo avete sotto gli occhi.

					</p>
				</div>
			</div>
			<!--<div class="row">
				<div class="icon-services">
					<div class="col-sm-4">
						<h3>Visita il locale</h3>
						<i class="fa fa-expand"></i>
					</div>
					<div class="col-sm-4">
						<h3>Consulta il menu</h3>
						<i class="mebi-menu"></i>
					</div>
					<div class="col-sm-4">
						<h3>Prenota</h3>
						<i class="mebi-list"></i>
					</div>
				</div>
			</div>-->
			*/?>
		</div>
	</section>

	<!--
	<section class="bg_image" back-img img="img/storia.jpg">
		<div class="overlay light"></div>
		<div class="container relative">
			<div class="col-sm-12">
				<h2 class="center section-title white-text">Storia</h2>
			</div>
			<div class="block_auto medium_block padding-big-v white-text">
				<p class="text-center white-text">
<b class="text-primary">Menoo</b> nasce dall’esperienza decennale di <b><a class="text-warning" href="http://mangiaebevi.it/">MangiaeBevi</a></b> - guida dei ristoranti di Roma e provincia allegata al settimanale Panorama – che dal 2006 ad oggi ha pubblicato 15 edizioni e ha avuto oltre 1.000 ristoranti clienti. Una vera e propria istituzione nella Capitale d’Italia.
				</p>
			</div>
		</div>

	</section>
	-->
</div>