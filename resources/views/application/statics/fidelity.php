<div id="staticPage" ng-controller="StaticCtrl">
	<section class="bg_image vcenter_parent full-height" back-img img="/img/pages/fidelity.jpg">
		<div class="overlay"></div>
		<div class="vcenter_child big_width">
			<h1 class="white-text center">Programma fedeltà</h1>
		</div>
		<?php /*<div class="go_down" scroll-to="#who">
			<p>
				<i class="fa fa-chevron-down"></i>
			</p>
		</div>*/?>
	</section>
	<section id="who">
		<div class="container">
			<h2>Programma fedeltà</h2>
			<div class="row padding-big-v">
				<div class="col-sm-12">
					<p class="margin-big-v">Il programma fedeltà di MangiaeBevi+  consente di accumulare crediti (foodcoins) da spendere nei ristoranti aderenti al proprio circuito.<br/>
					Per potervi aderire l’utente deve registrarsi sul portale MangiaeBevi+ al link plus.mangiaebevi.it (o sull’app) e accedere alla propria area dedicata.
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Foodcoins e crediti</h2>

						<ul class="list-unstyled" style="font-size: 16px;">
							<li><b>500</b> foodcoins si guadagnano al momento della registrazione (sul portale o sulle app)</li>
							<li><b>1.000</b> foodcoins nel caso in cui l’utente compili interamente il suo profilo</li>
							<li><b>100</b> foodcoins  per ogni prenotazione effettuata e onorata </li>
							<li><b>100</b> foodcoins per ogni ordinazione effettuata e onorata </li>
							<li><b>100</b> foodcoins per ogni amico “invitato” che si registra al portale</li>
							<li><b>50</b> foodcoins per ogni raccomandazione effettuata</li>
						</ul>
						<p class="margin-big-v">
						Al raggiungimento di determinate soglie di punteggio, l’utente potrà spendere il credito corrispondente ai foodcoins accumulati negli esercizi aderenti all’iniziativa.
						<br/>
						Le soglie di punteggio e il corrispettivo in crediti sono:<br/>
						<br /><b>3.000</b> foodcoins = crediti per <b>25€</b>
						<br /><b>5.000</b> foodcoins = crediti per <b>50€</b>
						<br /><b>8.000</b> foodcoins = crediti per <b>100€</b>
						<br/>
						I foodcoins <b>non hanno durata illimitata</b> e verranno automaticamente annullati in caso di inattività dell’utente protratta
						oltre i 12 mesi, intendendosi per inattività l’assenza di prenotazioni on-line provenienti dal profilo
						personale dell’utente.
						</p>
				</div>
			</div>
		</div>
	</section>

</div>