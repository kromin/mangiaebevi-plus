<div id="staticPage"  ng-controller="StaticCtrl">
	<section class="bg_image vcenter_parent full-height" back-img img="/img/come_funziona.jpg">
		<div class="overlay"></div>
		<div class="vcenter_child big_width">
			<h1 class="white-text center">Come funziona <br />
				<?php /*<small style="text-transform: none;">MangiaeBevi+: prenotazione, pre-ordine e pagamento in un’unica piattaforma, web e mobile. </small> */ ?>
			</h1>
		</div>
		<div class="go_down" scroll-to="#how">
			<p>
				<i class="fa fa-chevron-down"></i>
			</p>
		</div>
	</section>
	<section id="how">
		<div class="container">
			<div class="col-sm-12 padding-big-v medium_block block_auto">
				<p class="text-center"><b>MangiaeBevi+ è una community di gourmet basata sulle raccomandazioni degli utenti,<br/> che ti aiuta a trovare i migliori ristoranti o quelli più vicini a te e a condividerli con i tuoi amici.</b></p>
			</div>
			<div class="row padding-big-v">
				<div class="col-sm-6">
					<p>
						<b>Cerca un ristorante e condividi con gli amici</b>
						<br />Con MangiaeBevi+ puoi facilmente trovare ristoranti, pizzerie e i migliori locali dove mangiare e bere, intorno a te o selezionando vari parametri di ricerca, secondo i tuoi gusti e le tue preferenze e basandoti sulle raccomandazioni degli utenti, veri food lovers.<br/>
Puoi inoltre guardare il virtual tour (se disponibile) e le immagini del posto che hai scelto, con la possibilità di inserire tu stesso – registrandoti – le foto dei tuoi piatti preferiti e condividerle con i tuoi amici. Il tutto in maniera semplice e veloce.<br/>
Inoltre puoi farti guidare dalle ultime notizie e novità che trovi nel nostro magazine online “MangiaeBevi”.<br/>
E se hai uno smartphone, puoi utilizzare le nostre fantastiche applicazioni per iPhone o per Android.
					</p>
				</div>
				<div class="col-sm-6">
					<img src="/img/ricerca.png" alt="prenotazione" title="" class="img-responsive" />

				</div>
			</div>
			<?php /*<div class="row padding-big-v">
				<div class="col-sm-12">
					<p class="text-center">
						<i>
						Particolarmente efficace ed utile si rivela la pre-ordinazione per il pranzo, perché consente a quei clienti che hanno poco tempo di pausa di consumare il pasto al ristorante <span class="text-primary">in meno di 15 minuti</span>.
Condizione indispensabile per il pre-ordine è il pagamento anticipato del pasto, che avviene online tramite Paypal e che garantisce sia il cliente che il ristoratore.
Il Cliente perché potrà essere sicuro di trovare il suo pasto pronto in tavola all’orario stabilito e il ristoratore perché avrà un tavolo garantito e la certezza del pagamento anche nel caso di una mancata presentazione (no-show) del cliente che ha ordinato.
</i>
					</p>
				</div>
				<div class="col-sm-6"></div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<img src="/img/raccomandazione.png" alt="raccomandazione" title="" class="img-responsive" />
				</div>
				<div class="col-sm-6">
					<h3>Le raccomandazioni</h3>
					<p>Con <b>Mangiaebevi+</b> niente più recensioni e profili “fake” ma <b>raccomandazioni</b> fatte da una community di utenti verificati che rendono così affidabile la piattaforma e i loro consigli. Si torna al “passaparola”, la più antica e affidabile forma di pubblicità. Perché fidarsi infatti di recensioni, spesso forzate e fuorvianti, fatte da persone che nemmeno si conoscono? Con <b>Mangiaebevi+</b> l’utente può chiedere o dare “raccomandazioni” ad un amico, indifferentemente, sia di un ristorante che di un piatto.</p>
				</div>
			</div> */ ?>
			<div class="row padding-big-v">
				<div class="col-sm-6">
					<img src="/img/prenotazione.png" alt="raccomandazione" title="" class="img-responsive" />
				</div>
				<div class="col-sm-6">
					<p><b>Prenota un tavolo e pre-ordina</b>
						<br />L’utente, una volta scelto il ristorante di suo gradimento e prenotato il tavolo, può effettuare in anticipo l’ordinazione utilizzando un completo e pratico menu (anche fotografico) – sia dal computer che dal proprio smartphone – che viene inviato al ristorante in tempo reale. Il ristoratore verifica contemporaneamente sia la prenotazione del tavolo che il pre-ordine e ne dà conferma direttamente al cliente.<br/>
Questo consente al cliente di evitare lunghe attese - venendo servito immediatamente al proprio arrivo - e al ristoratore di fornire così un servizio più efficiente alla propria clientela, sapendo in anticipo i piatti che saranno ordinati e permettendogli l’approvvigionamento in caso di mancanza di uno o più ingredienti. E se paghi il conto prima, arrivando al ristorante all’orario stabilito troverai già pronto!<br/>
</p>
				</div>
			</div>
			<div class="row padding-big-v">
				<div class="col-sm-6">
					<p>
						<b>Raccomanda agli amici e premiati</b>
						<br />Con MangiaeBevi+ niente più recensioni e profili “fake”, ma raccomandazioni fatte da una community di buongustai dall’identità verificata che rendono così affidabile la piattaforma e i loro consigli.<br/>Si torna al “passaparola”, la più antica e affidabile forma di pubblicità. Perché fidarsi infatti di recensioni, spesso forzate e fuorvianti, fatte da persone che nemmeno si conoscono?<br/>Con Mangiaebevi+ puoi chiedere o dare “raccomandazioni” ad un amico, di un ristorante o di un piatto, indifferentemente. E con il programma fedeltà puoi accumulare crediti da spendere nei ristoranti aderenti all’iniziativa.

					</p>
				</div>
				<div class="col-sm-6">
					<img src="/img/raccomandazione.png" alt="prenotazione" title="" class="img-responsive" />

				</div>
			</div>
			<div class="row padding-big-v">
				<div class="col-sm-12">
					<p><b>Organizza i tuoi ristoranti preferiti o da visitare</b>
						<br />Grazie a MangiaeBevi+ puoi tenere traccia dei tuoi ristoranti preferiti, ordinarli in liste secondo la tipologia di cucina, il prezzo, la città, ecc. (es. ristoranti di pesce, pizzerie, ecc.), oppure creare un “bookmark” dei ristoranti dei quali hai sentito parlare e che vorresti visitare. Così sarà più facile trovare un ristorante sul tuo smartphone, consultare un menu, prenotare un tavolo.</p>
				</div>
			</div>
		</div>
	</section>

</div>