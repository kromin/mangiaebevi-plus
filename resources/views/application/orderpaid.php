<?php
require_once '_head.php';
$blue_header = 1;
require_once '_topbar.php';
//$plate_encoded = str_replace("\"","\\\"",str_replace("'","\'",json_encode(json_decode(json_encode($pl),true))));
?>
<div class="white_page_login" style="min-height: 70vh;">
    <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
        <div class="social-log-container text-center" style="margin-top: 10vh;">
            <h1 id="orderMessage"><?php echo $title;?></h1>
            <p><?php echo $message;?></p>
        </div>     
    </div>
    <div class="clearfix"></div>
</div>
<?php
require_once '_footer.php';
?>