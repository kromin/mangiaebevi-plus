<?php // prende menu
?>
<!DOCTYPE html>
<html ng-app="app">
    <head>
        <meta charset="utf-8">
        <meta name=viewport content="width=device-width, initial-scale=1">
        <style></style><!-- debug bar in error without this-->
        <meta name="robots" content="noindex">
        <base href="/" />                
    </head>    
    <body class="bg-white">
    	<h3>Prenotazioni <?php echo esc_html($testo_titolo);?></h3>
    	<table class="table table-striped">
    		<thead>
    			<tr>
    				<th class="text-center" style="width: 5%;"><span style="font-size: 9px">P.</span></th>
    				<th style="width: 15%;"><span style="font-size: 9px">Tel.</span></th>
    				<th style="width: 30%;"><span style="font-size: 9px">Email</span></th>
    				<th style="width: 20%;"><span style="font-size: 9px">Nome</span></th>
    				<th style="width: 5%;"><span style="font-size: 9px">C.</span></th>
    				<th style="width: 25%;"><span style="font-size: 9px">Info</span></th>
    			</tr>
    		</thead>
    		<tbody>
    			<?php 
    			$n = 0;
    			foreach ($reservations as $reservation) {
				$n++;
				?>
				<tr style="<?php if($n%2 == 0) echo 'background-color: #f7f7f7;';?>" ng-repeat="reservation in reservations">
		            <td class="text-center" style="<?php if($n%2 == 0) echo 'background-color: #f7f7f7;';?>">
		            	x <?php if(isset($reservation->people)) echo intval($reservation->people);?>
		            </td>
		            <td style="<?php if($n%2 == 0) echo 'background-color: #f7f7f7;';?> height: 50px;">
		            	<?php if(isset($reservation->tel)) echo intval($reservation->tel);?>
		            </td>
		            <td style="<?php if($n%2 == 0) echo 'background-color: #f7f7f7;';?> height: 50px;">
		            	<?php if(isset($reservation->email)) echo esc_html($reservation->email);?>
		            </td>
		            <td style="<?php if($n%2 == 0) echo 'background-color: #f7f7f7;';?> height: 50px;">
		            	<b><?php echo esc_html($reservation->name);?></b>
		            </td>
		            <td style="<?php if($n%2 == 0) echo 'background-color: #f7f7f7;';?> height: 50px;">
		            	<?php if($reservation->confirmed == 1) echo 'V';?>
		            	<?php if($reservation->confirmed == 4) echo 'X';?>
		            </td>		            
		            <td style="<?php if($n%2 == 0) echo 'background-color: #f7f7f7;';?> height: 50px;"> 
                    <p><b>data:</b><?php echo esc_html($reservation->date->format('d/m/Y').' '.$reservation->fascia_oraria);?></p>
                    <?php if ($reservation->confirmed == 1 && $reservation->type == 'web' && isset($struttura['sale'][intval($reservation->sala)]['name'])) : ?>
                    <p><b>Sala:</b><?php echo esc_html($struttura['sale'][intval($reservation->sala)]['name']);?></p>
                    <p><b>Tav.</b></p>
                	<?php endif; ?>
                	
		            </td>		                    		                             
		        </tr>
				<?php
			}
	        ?>
    		</tbody>
    	</table>
    	<?php /*
		<ul class="list-unstyled dashboard_list">
			<?php foreach ($reservations as $reservation) {
				?>
				<li ng-repeat="reservation in reservations">
		            <div class="dashboard_friend">
		                <div class="element_header">
		                    <div>
		                        <?php echo esc_html($reservation->name);?><br />
		                        <?php 
		                    	switch($reservation->type):
		                    	case 'web':
		                    	?>
		                    	<span class="text-warning">
		                            inserimento manuale
		                        </span>
		                    	<?php
		                    	break;
		                    	default:
		                    	?>
		                    	<span class="text-primary" ng-if="reservation.type == 'web'">
		                            by <img src="<?php echo env('APP_PROTOCOL').'://'.env('APP_DOMAIN');?>/img/logo_blue.png" style="width: 60px;" 
		                            alt="by menoo" />
		                        </span>
		                    	<?php
		                    	break;
		                    	endswitch;
		                    	?>		       
		                    	<br />
		                        <b><?php echo esc_html($reservation->fascia_oraria);?></b>
		                        <?php 
		                    	switch($reservation->confirmed):
		                    	case 1:
		                    	echo '<span class="text-success">confermato</span>';
		                    	break;
		                    	case 0:
		                    	echo '<span class="text-danger">non confermato</span>';
		                    	break;
		                    	case 2:
		                    	echo '<span class="text-warning">in attesa della conferma del cambio di orario</span>';
		                    	break;
		                    	case 3:
		                    	echo '<span class="text-primary">approvato il cambio di orario</span>';
		                    	break;
		                    	endswitch;
		                    	?>		  
		                        <br />
		                        <span class="date">
		                            x <?php if(isset($reservation->people)) echo intval($reservation->people);?> - 
		                            <?php if(isset($reservation->tel)) echo esc_html($reservation->tel);?> - 
		                            <?php if(isset($reservation->email)) echo esc_html($reservation->email);?>
		                        </span>
		                        <br />
		                        <?php if ($reservation->confirmed == 1 && $reservation->type == 'web') : ?>
		                        <label>
		                            <?php echo $struttura['sale'][intval($reservation->sala)]['name'];?>
		                        </label>
		                    	<?php endif; ?>
		                    </div>			                    
		                    <div class="clearfix"></div>
		                </div>
		            </div>               
		        </li>
				<?php
			}
	        ?>
	    </ul><?php */ ?>
    </body>
</html>