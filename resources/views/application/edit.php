<?php
require_once '_head.php';
require_once '_topbar.php';
?>
<div id="edit-restaurant-page" class="editing_profile_restaurant" ng-init="loadRestaurant('<?php echo $restaurant;?>')" ng-controller="EditRestaurantCtrl" >
        
    <div class="main_image" back-img img="{{staticURL}}/restaurant/big/{{restaurant.image}}.png" ng-if="restaurant.image && restaurant.image != ''">
        <div class="overlay"></div>
    </div>
    <div class="main_image" back-img img="/img/home/home_1.jpg" ng-if="restaurant.image == '' || !restaurant.image">
        <div class="overlay"></div>
    </div>
    
    <!-- edit restaurant -->
<div style="min-height: 100vh;">
    <div class="hidden show_on_load">
        <div class="main_container">
            <div class="main_section_menu">
                <div class="container">
                    <div class="container_el">
                        <a href="/modifica-ristorante/<?php echo $restaurant;?>/#/statistiche" ng-class="{'current':current_tab == 5}"><i class="fa fa-line-chart hidden-sm hidden-xs"></i> <span>Statistiche</span></a>
                    </div>
                    <div class="container_el">
                        <a href="/modifica-ristorante/<?php echo $restaurant;?>/#/clienti" ng-class="{'current':current_tab == 6}"><i class="mebi-avatar hidden-sm hidden-xs"></i> <span>Clienti</span></a>
                    </div>
                    <div class="container_el">
                        <a href="/modifica-ristorante/<?php echo $restaurant;?>/#/menu" ng-class="{'current':current_tab == 2}"><i class="mebi-list hidden-sm hidden-xs"></i> <span>Menu</span></a>
                    </div>
                    <div class="container_el full">
                        <a href="/modifica-ristorante/<?php echo $restaurant;?>/#/sale" ng-class="{'current':current_tab == 3}"><i class="mebi-romantic-date hidden-sm hidden-xs"></i> <span>Sale</span></a>
                    </div>
                    <div class="container_el full full_full">
                        <a href="/modifica-ristorante/<?php echo $restaurant;?>/#/prenotazioni" ng-class="{'current':current_tab == 4}"><i class="mebi-calendar-1 hidden-sm hidden-xs"></i> <span>Prenotazioni</span></a>
                    </div>
                    <div class="container_el">
                        <a href="/modifica-ristorante/<?php echo $restaurant;?>/#/informazioni" ng-class="{'current':current_tab == 1}"><i class="fa fa-info hidden-sm hidden-xs"></i> <span>Info</span></a>
                    </div>
                </div>
            </div>

            <div class="restaurantTitle" ng-if="current_tab == 1">
                <div class="container">
                    <h1>
                        <span contenteditable ng-model="restaurant.name">{{restaurant.name}} </span>
                    </h1>
                    <span class="restaurant_top_infos">
                        <small ng-if="restaurant.tipoLocale.length > 0" ng-click="scrollTo('tipo_locale_block')">
                            {{tipiLocali[restaurant.tipoLocale[0]].name}} <i class="fa fa-refresh"></i> 
                            |
                        </small>&nbsp;&nbsp;&nbsp;
                        <small ng-click="scrollTo('price_set')"><span ng-repeat="euro in price_euro_add track by $index">{{euro}}</span> <i class="fa fa-refresh"></i></small>
                    </span>             
                </div>
            </div>
        </div>

        <?php require_once 'edit/restaurant_info.php';?>
        <?php require_once 'edit/menu_management.php';?>
        <?php require_once 'edit/struttura.php';?>
        <?php require_once 'edit/reservations.php';?>
        <?php require_once 'edit/stats.php';?>
        <?php require_once 'edit/clients.php';?>
        
    </div>
</div>
<div class="clearfix"></div>


<?php
require_once '_footer.php';
?>