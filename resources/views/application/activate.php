<?php
require_once '_head.php';
$blue_header = 1;
require_once '_topbar.php';
?>

<div class="white_page_login" ng-controller="ResendCtrl">
	<div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
		<div class="form-info">			
			<form name="resendForm">
				<h3 class="text-center">Invia nuovamente il link di attivazione</h3>
				<p>Un nuovo link di attivazione verrà inviato al tuo indirizzo email</p>
				<p ng-if="error_resend" class="text-danger">
					<span ng-if="error_resend == 1">Indirizzo email non trovato</span>
					<span ng-if="error_resend == 2">Sembra che fino ad oggi tu sia sempre entrato con i social</span>
				</p>
				<p ng-if="resend_success" class="text-success">
					Dovresti aver ricevuto una email
				</p>
				<div class="form-group" ng-if="!resend_success">
					<label for="email" class="sr-only">Email</label>
					<input type="email" ng-model="resend.email" id="email" class="form-control" placeholder="Email" required/>
				</div>
				<a class="link_btn" href="/accedi/">Accedi</a>
				<div class="form-group" ng-if="!resend_success">
					<input type="submit" style="margin: 12px 0;" ng-disabled="resendForm.$invalid" ng-click="resend()" 
					class="btn btn-block btn-primary ghost" value="Reinvia" />
				</div>
			</form>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php
require_once '_footer.php';
?>