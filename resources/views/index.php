<?php /*<!DOCTYPE html>
<html ng-app="app" ng-controller="ApplicationCtrl">
    <head>
        <meta charset="utf-8">
        <meta name=viewport content="width=device-width, initial-scale=1">
        <style></style><!-- debug bar in error without this-->
        <link rel="shortcut icon" href="/img/favicon.ico">
                
        <?php 
        //$seo_arr = Seo::returnHeaderVars();
        ?>
        <title>{{seo.pageTitle}}<?php //echo $seo_arr['title'];//{{seo.pageTitle}}?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta  name="description" content="{{seo.pageDescription}}<?php //class="update_desc_value"echo $seo_arr['description'];//{{seo.pageDescription}}?>" />                    

        <!-- Schema.org markup for Google+ -->
        <meta  itemprop="name" content="{{seo.pageTitle}}<?php //echo $seo_arr['title'];//{{seo.pageTitle}}?>">
        <meta  itemprop="description" content="{{seo.pageDescription}}<?php //class="update_desc_content"class="update_title_content"echo $seo_arr['description'];//{{seo.pageDescription}}?>">
        <meta  itemprop="image" content="{{seo.pageMainImage}}<?php //class="update_image_content"echo $seo_arr['image'];//{{seo.pageMainImage}}?>">

        <!-- Twitter Card data -->
        <meta  name="twitter:card" content="{{seo.pageMainImage}}<?php //class="update_image_content"echo $seo_arr['image'];//{{seo.pageMainImage}}?>">
        <meta name="twitter:site" content="@publisher_handle">
        <meta  name="twitter:title" content="{{seo.pageTitle}}<?php //class="update_title_content"echo $seo_arr['title'];//{{seo.pageTitle}}?>">
        <meta  name="twitter:description" content="{{seo.pageDescription}}<?php //class="update_desc_content"echo $seo_arr['description'];//{{seo.pageDescription}}?>">
        <meta name="twitter:creator" content="@author_handle">
        <!-- Twitter summary card with large image must be at least 280x150px -->
        <meta  name="twitter:image:src" content="{{seo.pageSquareImage}}<?php //class="update_image_content"echo $seo_arr['image'];//{{seo.pageSquareImage}}?>">

        <!-- Open Graph data -->
        <meta  property="og:title" content="{{seo.pageTitle}}<?php //class="update_title_content"echo $seo_arr['title'];//{{seo.pageTitle}}?>" />
        <meta  property="og:type" content="{{seo.pageType}}<?php //class="update_type_content"echo $seo_arr['type'];//{{seo.pageType}}?>" />
        <meta property="og:url" content="{{seo.pageUrl}}<?php //class="update_url_content"echo $seo_arr['url'];//{{seo.pageUrl}}?>" />
        <meta  property="og:image" content="{{seo.pageSquareImage}}<?php //class="update_image_content"echo $seo_arr['image'];//{{seo.pageSquareImage}}?>" />
        <meta  property="og:description" content="{{seo.pageDescription}}<?php //class="update_desc_content"echo $seo_arr['description'];//{{seo.pageDescription}}?>" />    
        <meta property="og:site_name" content="MangiaeBevi" />

        
        <base href="/" />        
    </head>    
    <body ng-class="{'bg-white' : bg_w}">
        <div id="fb-root"></div>
        <script type="text/javascript">
            window.prerenderReady = false;
        <?php if(env('APP_ENV') == 'production'): ?>
            (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&appId=<?php echo env('FB_KEY');?>&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            window.twttr = (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0],
            t = window.twttr || {};
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);
            t._e = [];
            t.ready = function(f) {
            t._e.push(f);
            };
            return t;
            }(document, "script", "twitter-wjs"));

            window.___gcfg = {
            lang: 'it-IT'
            };
            <?php endif; ?>
            (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = '//apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();

            (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = '<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/script.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
        </script>



        <div ng-include src="'templates/topbar'" ng-controller="TopbarCtrl"></div> 
        
        <div ng-view>
        </div>   

        <div ng-include src="'templates/map'"></div>    

        <div ng-include src="'templates/footer'"></div>  


        <div class="cookie_law" ng-include src="'templates/cookielaw'" ng-if="!hide_cookies"></div>
        
        <div class="main_page_overlay">
            <div class="cs-loader">
              <div class="cs-loader-inner">
                <label> ●</label>
                <label> ●</label>
                <label> ●</label>
                <label> ●</label>
                <label> ●</label>
                <label> ●</label>
              </div>
            </div>
        </div>
    </body>
</html>
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/app.css" rel='stylesheet' type='text/css'>
*/?>