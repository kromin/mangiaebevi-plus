<!DOCTYPE html>
<html ng-app="FacebookMangiaeBeviApp">
    <head>
        <title>MangiaeBevi+</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <style></style><!-- debug bar in error without this-->
        <link rel="shortcut icon" href="/img/favicon.png">
        <meta name="fragment" content="!">

        <base href="/importframe/facebook/" />

    </head>
    <body style="background: none;">

        <div>
            <?php require_once 'templates/facebook/index.php'; ?>
        </div>

    <script type="text/javascript" src="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/script_facebook.js"></script>

    </body>
</html>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/app.css" rel='stylesheet' type='text/css'>