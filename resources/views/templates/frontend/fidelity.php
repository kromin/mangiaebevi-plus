<div id="staticPage"
ng-init="setSEO('Programma fedeltà | MangiaeBevi','/programma-fedelta/','/img/pages/fidelity.jpg','/img/pages/fidelity.jpg','','A breve online il programma fedeltà di MangiaeBevi, la guida per il palato')"
>
	<section class="bg_image vcenter_parent full-height" back-img img="/img/pages/fidelity.jpg">
		<div class="overlay"></div>
		<div class="vcenter_child big_width">
			<h1 class="white-text center">Programma fedeltà <br /><small>In arrivo</small>
			</h1>			
		</div>
		<?php /*<div class="go_down" scroll-to="#who">
			<p>
				<i class="fa fa-chevron-down"></i>
			</p>
		</div>*/?>
	</section>
	<?php /*<section id="who">
		<div class="container">
			<div class="row padding-big-v">
				<div class="col-sm-8">
					<h2>La Dining Card</h2>
					<p class="margin-big-v">Il programma fedeltà di <b>MangiaeBevi</b> è fondato sull’utilizzo della <b>Dining Card</b>, 
						una speciale carta digitale custodita direttamente nelle sue app per smartphone iPhone e Android, 
						che consente di accumulare crediti (foodcoins) da spendere nei ristoranti aderenti al circuito MangiaeBevi.<br />
						Per poterla attivare ed utilizzare l’utente deve registrarsi sul portale MangiaeBevi.it (o sull’app) e accedere alla propria area dedicata.
					</p>
				</div>
				<div class="col-sm-4">		
				<img class="img-responsive" ng-src="/img/pages/cell.png" alt="dining-card" />			
				</div>
			</div>
			<div class="row padding-big-v">
				<div class="col-sm-4 foodcoin-container">
					<div class="food-badge hidden-xs">
						<span class="plus">+</span><span class="txt">1.000</span>
					</div>
				</div>
				<div class="col-sm-8">	
					<h2>Foodcoins e crediti</h2>
					<p class="margin-big-v">Accumulare foodcoins è semplice e può avvenire in vari modi:
						<ul class="list-unstyled" style="font-size: 16px;">
							<li><b>500</b> foodcoins si guadagnano al momento della registrazione (sul portale o sulle app)</li>
							<li><b>1.000</b> foodcoins nel caso in cui l’utente compili interamente il suo profilo</li>
							<li><b>100</b> foodcoins  per ogni prenotazione effettuata e onorata </li>
							<li><b>100</b> foodcoins per ogni ordinazione effettuata e onorata </li>
							<li><b>100</b> foodcoins per ogni amico “invitato” che si registra al portale www.mangiaebevi.it</li>
							<li><b>50</b> foodcoins per ogni raccomandazione effettuata</li>
						</ul>
					</p>				
				</div>
			</div>
			<div class="row padding-big-v">
				<div class="col-sm-12">	
					<p class="margin-big-v">Nel caso della prenotazione l’assegnazione dei foodcoins viene effettuata dal ristoratore tramite il 
						<b>QR Code</b> posto sulla carta digitale del cliente, dopo aver pagato il conto. 
						<br />Oppure direttamente sul portale MeB business, in corrispondenza della prenotazione del cliente.
					</p>	
					<p>
						Al raggiungimento di determinate soglie di punteggio, il titolare della Dining Card potrà decidere se spendere il credito corrispondente ai foodcoins accumulati negli esercizi aderenti all’iniziativa.
						Le soglie di punteggio e il corrispettivo in crediti sono:
						<br /><b>3.000</b> foodcoins = crediti per <b>25€</b>
						<br /><b>5.000</b> foodcoins = crediti per <b>50€</b>
						<br /><b>8.000</b> foodcoins = crediti per <b>100€</b>
						<br /><br />
						I foodcoins <b>non hanno durata illimitata</b> e verranno automaticamente annullati in caso di inattività dell’utente protratta 
						oltre i 12 mesi, intendendosi per inattività l’assenza di prenotazioni on-line provenienti dal profilo 
						personale dell’utente.

					</p>			
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<hr />
					<h4>REGOLAMENTO</h4>
					<p style="font-size: 14px;">
						<b>MangiaeBevi</b> raccomanda - ai suoi utenti che abbiano effettuato la prenotazione di un tavolo o l’ordinazione di un pasto - sempre la massima puntualità e che la prenotazione/ordinazione effettuata venga onorata.

						<br />Per quanto riguarda la prenotazione del tavolo nel caso di eccessivo ritardo o impossibilità di recarsi al ristorante occorrerà comunicarlo con almeno un’ora di anticipo, direttamente al ristorante interessato, modificandone l’orario (se possibile) o cancellando la propria prenotazione. La tolleranza massima di ritardo sull’orario prefissato è di 15 minuti. Superati i 15 minuti di ritardo la prenotazione non sarà più garantita e il ristorante potrà riassegnare il tavolo ad altri clienti (in questo caso, ovviamente, i foodcoins non verranno assegnati).
						<br />In caso di mancata presentazione saranno detratti 150 foodcoins e assegnata una nota negativa. Al raggiungimento di tre note negative l’utente perderà tutti i suoi foodcoins e gli sarà bloccata la sua Dining Card. 
						<br />MangiaeBevi vuole diffondere tra i propri utenti l’abitudine alla prenotazione online, ma questo presuppone serietà da parte degli stessi e il mantenimento degli impegni presi, dando opportuna comunicazione se si hanno dei contrattempi. Rispetto per se stessi e per chi lavora.

						<br /><b>N.B.: MangiaeBevi</b> si riserva il diritto di modificare il sistema di assegnazione dei foodcoins e dei rispettivi crediti.
					</p>
					<hr />
					<h4>AVVERTENZE</h4>
					<p>
						<ol style="padding-left: 12px; font-size: 14px;">
							<li>La Dining Card deve essere sempre esibita al momento della richiesta del conto e/o prima dell’emissione della ricevuta fiscale o dello scontrino. Il ristoratore si riserva il diritto di richiedere l’esibizione di un documento di identità.</li>
							<li>La Dining Card è strettamente personale.</li>
							<li>La Dining Card non è cumulabile con altre offerte e/o promozioni del locale, salvo diversa indicazione riportata sulla scheda del locale stesso.</li>
							<li>La Dining Card è incompatibile con i Buoni Pasto di terzi, i Menu Fissi, i banchetti e le cerimonie, salvo diversa indicazione riportata sulla scheda del locale.</li>
							<li>Nessuna responsabilità è imputabile alla Digikom S.r.l. per eventuali variazioni dall’elenco dei locali convenzionati dovuti a fattori e circostanze indipendenti dalla volontà della Digikom S.r.l.</li>
						</ol>
					</p>
				</div>
			</div>
		</div>
	</section>	*/?>

</div>