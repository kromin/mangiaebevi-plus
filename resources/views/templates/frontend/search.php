<div id="searchPage">
	<div class="col-md-9">
		<div id="search_form">
			<form class="form-inline">
				<div class="col-sm-6 col-xs-12 no-pad">
					<input ng-model="search.query_string" type="text" class="form-control" placeholder="Cosa vuoi cercare?" ng-enter="setNewSearch()" />
				</div>
				<div class="col-sm-6 col-xs-12 small_marginated_btn">
					<a class="btn btn-primary btn-sm ghost" style="font-size: 13px;" ng-click="setNewSearch()">Cerca</a>
				</div>
			</form>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
		<div id="view_actions">
			<!-- grid or list -->
			<i class="fa fa-th" ng-click="view_type = 'grid'" ng-class="{'active':view_type=='grid'}"></i>
			<i class="fa fa-list" ng-click="view_type = 'list'" ng-class="{'active':view_type=='list'}"></i>
			<a ng-click="openAdvSearch()">Non sei soddisfatto dei risultati, <b>clicca qui</b> per la ricerca avanzata</a>
		</div>		
		<div id="search_results" class="" ng-class="(view_type == 'list') ? 'list_view' : 'grid_view'">
			<div class="col-lg-3 col-md-4 col-sm-6 restaurant-grid-container" ng-repeat="restaurant in search_restaurants_showed">
				<a href="/attivita/{{restaurant.slug}}">
                    <div class="restaurant box_images qt">
                        <div class="img" back-img img="{{staticURL}}/restaurant/small/{{restaurant.image}}.png" ng-if="restaurant.image && restaurant.image.length > 0"></div>
                        <div class="img" back-img img="/img/search_default.jpg"  ng-if="!restaurant.image || restaurant.image.length == 0"></div>
                        <div class="overlay"></div>
                        <div class="text">
                            <h3>{{restaurant.name}}</h3>
                            <span class="type">{{tipiLocali[restaurant.tipoLocale[0]].name}}</span>
                            <span class="address">{{restaurant.address}} 
                            <distance ng-bind="returnDistance(restaurant.loc.coordinates[1],restaurant.loc.coordinates[0])"></distance></span>
                        </div>
                    </div>
                </a>				
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="pagination">
			<ul class="list-unstyled">
				<li ng-repeat="this_page in pages" ng-class="{'active': page == this_page}" ng-click="setPage(this_page)" ng-if="this_page > (page-3) && this_page < (page+3)">{{this_page}}</li>
			</ul>
		</div>
	</div>
	<div class="col-md-3 map_container_right hidden-sm hidden-xs">
		<div ng-controller="MapCtrl">
			<div id="google_map"></div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="clearfix"></div>