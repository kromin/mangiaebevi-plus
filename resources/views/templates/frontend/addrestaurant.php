<div id="staticPage"
ng-init="setSEO('Aggiungi un ristorante | MangiaeBevi','/aggiungi-ristorante/','/img/pages/ristoratore.png','/img/pages/ristoratore.png','food','Aggiungi il tuo ristorante su MangiaeBevi, la guida per il palato')"
>
    <section class="bg_image vcenter_parent full-height" back-img img="/img/pages/ristoratore.png">
        <div class="overlay"></div>
        <div class="vcenter_child big_width">
            <h1 class="white-text center">Aggiungi il tuo ristorante<br />su <span class="text-primary">MangiaeBevi</span></h1>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="col-sm-12">
                <h2 class="center section-title with_bottom_row">Compila il form</h2>
            </div>
            <div class="multistep">
                <div ng-if="current_user.loggedStatus == 0 && current_step == 0">
                    <p class="text-center">Per poter aggiungere un ristorante devi essere registrato</p>
                </div>
                <div ng-if="current_user.loggedStatus == 0 && current_step == 0" ng-include="'/templates/logpanel'"></div>
                <div class="restaurant_panel" ng-if="current_step == 1">
                    <h3>Aggiungi informazioni sul tuo ristorante</h3>
                    <form id="restaurantForm" ng-submit="confirmBaseInfos()">
                        <div class="form-group">
                            <label for="risto_name">Nome</label>
                            <input class="form-control" type="text" id="risto_name" ng-model="addingRestaurant.name" placeholder="Nome" required ng-enter="searchRestaurant()" />
                        </div>
                        <div class="just_present" ng-if="!show_other_restaurant_values">
                            <div class="form-group">
                                <a class="text-primary" ng-click="searchRestaurant()">Cerca il ristorante tra quelli presenti nel nostro database</a>
                            </div>
                            <div ng-if="risto_searched.length > 0">
                                <h3>Seleziona uno dei seguenti ristoranti se credi sia il tuo</h3>
                                <ul class="list-unstyled">
                                    <li ng-repeat="risto in risto_searched" ng-click="selectSearched(risto._id)" ng-if="risto.user == 0 || !risto.user">
                                        {{risto.name}} - {{risto.address}}
                                    </li>
                                </ul>
                                <a ng-click="notInList()">Clicca qui se il tuo ristorante non è nella lista</a>
                            </div>
                        </div>
                        <div ng-show="show_other_restaurant_values">
                            <div class="form-group">
                                <label for="risto_address">Indirizzo</label>
                                <input class="form-control" type="text" id="risto_address" ng-model="addingRestaurant.address" required/>
                            </div>
                            <div class="form-group">
                                <label for="risto_city">Città</label>
                                <input class="form-control" type="text" id="risto_city" ng-model="addingRestaurant.city" required/>
                            </div>
                            <div class="form-group">
                                <label for="risto_state">Provincia (sigla)</label>
                                <input class="form-control" type="text" id="risto_state" ng-model="addingRestaurant.state" required/>
                            </div>
                            <div class="form-group">
                                <label for="risto_zip">CAP</label>
                                <input class="form-control" type="text" id="risto_zip" ng-model="addingRestaurant.zip" required/>
                            </div>
                            <div class="form-group">
                                <label for="risto_email">Indirizzo email</label>
                                <input class="form-control" type="email" id="risto_email" ng-model="addingRestaurant.email" required/>
                            </div>
                            <div class="form-group">
                                <label for="risto_email">Telefono</label>
                                <input class="form-control" type="text" id="risto_phone" ng-model="addingRestaurant.phone" />
                            </div>
                            <div class="form-group">
                                <label for="risto_email">Sito Web</label>
                                <input class="form-control" type="text" id="risto_web" ng-model="addingRestaurant.web" />
                            </div>
                            <div class="form-group">
                                <label for="risto_email">Tipologia principale di locale</label>
                                <select class="form-control" type="text" id="risto_web" ng-model="addingRestaurant.custom" required ng-change="selectTipologia()">
                                    <option value="{{tipo._id}}" ng-repeat="tipo in tipiLocali">{{tipo.name}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary ghost btn-block" ng-disabled="restaurantForm.$invalid" value="Prossimo step" style="margin: 12px 0;" />
                            </div>
                        </div>
                    </form>
                </div>

                <div ng-if="current_step == 2">
                    <h3 class="form-title">Altre informazioni sul tuo ristorante</h3>

                    <div class="row">
                        <div class="col-md-4">
                            <h3>Tipo di cucina</h3>
                            <label class="label--checkbox block" ng-repeat="cucina in tipiCucine">
                                <input class="checkbox" type="checkbox" name="selectedtipoCucina[]" ng-checked="addingRestaurant.tipoCucina.indexOf(cucina._id) != -1" ng-click="selectItem(cucina._id,'tipocucina')"> {{cucina.name}}
                            </label>
                        </div>
                        <div class="col-md-4">
                            <h3>Regione della cucina</h3>
                            <label class="label--checkbox block" ng-repeat="region in regioniCucine">
                                <input class="checkbox" type="checkbox" name="selectedRegioni[]" ng-checked="addingRestaurant.regioneCucina.indexOf(region._id) != -1" ng-click="selectItem(region._id,'regionecucina')"> {{region.name}}
                            </label>
                        </div>
                        <div class="col-md-4">
                            <h3>Servizi</h3>
                            <label class="label--checkbox block" ng-repeat="service in services">
                                <input class="checkbox" type="checkbox" name="selectedServices[]" ng-checked="addingRestaurant.services.indexOf(service._id) != -1" ng-click="selectItem(service._id,'service')"> {{service.name}}
                            </label>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <input type="submit" class="btn btn-primary ghost" ng-disabled="restaurantForm.$invalid" value="Prossimo step" style="margin: 12px 0;" ng-click="confirmOtherInfos()" />
                    </div>
                </div>

                <div class="restaurant_panel" ng-if="current_step == 3">

                    <div class="form-group">
                        <label for="risto_chef">Chef</label>
                        <input class="form-control" type="text" id="risto_chef" ng-model="addingRestaurant.staff.chef[0]" />
                    </div>
                    <div class="form-group">
                        <label for="risto_chef">Sommelier</label>
                        <input class="form-control" type="text" id="risto_chef" ng-model="addingRestaurant.staff.sommelier[0]" />
                    </div>
                    <div class="form-group">
                        <label for="risto_chef">Prezzo minimo</label>
                        <div class="range-slider">
                            <input class="range-slider__range" type="range" ng-model="addingRestaurant.priceMin" min="0" max="100" step="5">
                            <span class="range-slider__value">{{addingRestaurant.priceMin}}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="risto_chef">Prezzo massimo</label>
                        <div class="range-slider">
                            <input class="range-slider__range" type="range" ng-model="addingRestaurant.priceMax" min="0" max="100" step="5">
                            <span class="range-slider__value">{{addingRestaurant.priceMax}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <h3>Carte accettate</h3>
                        <label class="label--checkbox block" ng-repeat="carta in creditCard">
                            <input class="checkbox" type="checkbox" name="selectedCarta[]" ng-checked="addingRestaurant.carte.indexOf(carta._id) != -1" ng-click="selectItem(carta._id,'carta')"> {{carta.name}}
                        </label>
                    </div>

                    <div class="form-group text-center">
                        <input type="submit" class="btn btn-primary ghost" ng-disabled="restaurantForm.$invalid" value="Prossimo step" style="margin: 12px 0;" ng-click="confirmStaff()" />
                    </div>
                </div>


                <div class="restaurant_panel" ng-if="current_step == 4">
                    <h3 class="form-title">Link alle pagine social del tuo ristorante</h3>
                    <div class="form-group with-addon">
                        <label>Tripadvisor</label>
                        <input type="text" class="form-control" ng-model="addingRestaurant.social[7].tripadvisor" placeholder="Tripadvisor" />
                    </div>
                    <div class="form-group">
                        <label>Facebook</label>
                        <input type="text" class="form-control" ng-model="addingRestaurant.social[0].facebook" placeholder="Facebook" />
                    </div>
                    <div class="form-group">
                        <label>Twitter</label>
                        <input type="text" class="form-control" ng-model="addingRestaurant.social[1].twitter" placeholder="Twitter" />
                    </div>
                    <div class="form-group">
                        <label>Google Plus</label>
                        <input type="text" class="form-control" ng-model="addingRestaurant.social[4].googlePlus" placeholder="Google Plus" />
                    </div>
                    <div class="form-group">
                        <label>Pinterest</label>
                        <input type="text" class="form-control" ng-model="addingRestaurant.social[2].pinterest" placeholder="Pinterest" />
                    </div>
                    <div class="form-group">
                        <label>Instagram</label>
                        <input type="text" class="form-control" ng-model="addingRestaurant.social[3].instagram" placeholder="Instagram" />
                    </div>
                    <div class="form-group">
                        <label>Linkedin</label>
                        <input type="text" class="form-control" ng-model="addingRestaurant.social[5].linkedin" placeholder="Linkedin" />
                    </div>
                    <div class="form-group">
                        <label>Youtube</label>
                        <input type="text" class="form-control" ng-model="addingRestaurant.social[6].youtube" placeholder="Youtube" />
                    </div>

                    <div class="form-group text-center">
                        <input type="submit" class="btn btn-primary ghost" ng-disabled="restaurantForm.$invalid" value="Prossimo step" style="margin: 12px 0;" ng-click="confirmSocial()" />
                    </div>
                </div>

                <div ng-if="current_step == 5">
                    <h3 class="form-title">Orari di apertura</h3>
                    <div class="clearfix"></div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Giorno</label>
                            <select type="text" ng-model="add_fascia_oraria.day" class="form-control">
                                <option ng-repeat="day in week_days track by $index" value="{{$index}}">{{day}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Fascia oraria</label>
                            <div class="clearfix"></div>
                            <input type="text" id="datetimepicker-time_from" 			                   
							                   class="form-control semi-date" placeholder="Dalle" style="width: 44%;display: inline-block; margin-right: 1%;" />
		                    <input type="text" id="datetimepicker-time_to" 			                   
							                   class="form-control semi-date" placeholder="Alle" style="width: 44%;display: inline-block;" />
							<div class="clearfix"></div>
                            <span class="size-small">(dalle - alle: hh:mm-hh:mm)</span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group" style="margin-top: 25px;">
                            <button ng-click="add_fascia_to_restaurant(add_fascia_oraria.day,add_fascia_oraria.fascia)" class="btn btn-default">Aggiungi</button>
                            <button ng-click="add_fascia_to_restaurant_all_days(add_fascia_oraria.fascia)" class="btn btn-primary btn-sm ghost">Aggiungi a tutti i giorni</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div ng-repeat="day in week_days track by $index" class="col-sm-2">
                        <h3 class="size-small uppercase spacing">{{day}}</h3>
                        <p ng-repeat="fascia in addingRestaurant.fasce[$index] track by $index">{{fascia}}
                            <i class="fa fa-close" ng-click="remove_fascia($index,$parent.$index)"></i>
                        </p>
                    </div>


                    <div class="form-group text-center">
                        <input type="submit" class="btn btn-primary ghost" value="Prossimo step" style="margin: 12px 0;" ng-click="addRestaurant()" />
                    </div>
                </div>


                <div ng-if="current_step == 6">
                	<h3 class="form-title">Carica una immagine</h3>
                	<div class="contain-image-actions addrestaurant_action">
		                <!--<img ng-src="{{staticURL}}/restaurant/small/{{current_restaurant.image}}.png" alt="">-->
		                <div ng-if="!uploading_image" class="actions">
		                  <div>
		                    <i class="fa fa-upload"></i>
		                    <input class="image-icon" type="file" ngf-select ng-model="current_restaurant.adding_image" 
		                    name="current_restaurant.adding_image" 
		                    ng-change="uploadImage()" title="Cambia" />
		                  </div>
		                </div>
		                <div ng-if="uploading_image" class="always_visible_action">
		                  <div class="spinner spinner-bounce-middle"></div>
		                </div>
		            </div>
                </div>

                <div class="restaurant_panel" ng-if="current_step == 7">
                	<h3 class="form-title">Aggiungi una descrizione del tuo locale</h3>
                	<div class="form-group">
                      <label>Testo</label>
                      <textarea msd-elastic class="form-control" ng-model="current_restaurant.description" rows="3"></textarea>
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" class="btn btn-primary ghost" value="Termina" style="margin: 12px 0;" ng-click="addFinalRestaurant()" />
                    </div>
                </div>


                <div class="confirmed_panel" ng-if="current_step == 9">
                    <h3><i class="mebi-android-done"></i> Congratulazioni {{current_user.name}}, il tuo ristorante è stato inserito ed è in attesa di approvazione</h3>
                </div>
                <div class="confirmed_panel" ng-if="current_step == 8">
                    <h3><i class="mebi-android-done"></i> Congratulazioni {{current_user.name}}, hai reclamato questo ristorante, a breve ti contatteremo per verificare che tu sia il proprietario</h3>
                </div>
            </div>
        </div>

        
        <?php 
/*if (auth()->user() && auth()->user()->role == 9):
?>
        <pre>{{addingRestaurant|json}}</pre> <?php 
endif;*/
?>
    </section>

</div>