<div class="plate_dialog">
	<h3 class="text-center">{{current_plate.name}}</h3>
	<div class="plate_image_container" back-img img="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/menu/small/{{current_plate.image}}.png" ng-if="current_plate.image && current_plate.image != ''">
	</div>
	<div class="col-sm-12 text-center">
		<p class="bigText marginated">{{current_plate.qty|number}}</p>
		<p>
			<a class="btn btn-primary" ng-click="moreQty()"><i class="fa fa-plus"></i></a>
			<a class="btn btn-primary" ng-click="lessQty()"><i class="fa fa-minus"></i></a>
		</p>
		<div class="variations marginated" ng-if="current_plate.options.length > 0">
			<div ng-repeat="(optionCategoryIndex, option) in current_plate.options" class="option_category_block">
				<p class="option_category col-xs-12">{{option.name}}</p>
				<ul class="list-unstyled">
					<li class="single_option_list_item" ng-repeat="single_option in option.options track by $index">
						
						<div class="col-xs-8">
							{{single_option.name}}
							<span ng-if="single_option.no_variation == 0">(
	    						<b class="text-success" ng-if="single_option.variation == 0">+</b> 
	    						<b class="text-danger" ng-if="single_option.variation == 1">-</b> 
	    						<b>{{single_option.price}} €</b>)
	    					</span>
						</div>
						<div class="col-xs-4 qty_options">
							<span class="option_check" ng-click="checkUncheckPlateOption(optionCategoryIndex,$index)" ng-class="{'active' : single_option.selected && single_option.selected == 1}">
								<i class="mebi-android-done check"></i>
								<i class="mebi-close remove"></i>
							</span>
							<!--<i class="fa fa-chevron-up" ng-click="addQtyToSingleOption(optionCategoryIndex,$index,current_plate.qty)"></i>
							<i class="fa fa-chevron-down" ng-click="removeQtyToSingleOption(optionCategoryIndex,$index,single_option)"></i>-->
						</div>
						<div class="clearfix"></div>						
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="marginated">
			<h4>PREZZO €</h4>
			<p class="bigText">{{current_plate.total_price|number:2}}</p>
		</div>
		<div class="marginated">
			<a class="btn btn-primary ghost btn-block" ng-click="closeThisDialog(0)">Conferma</a>
		</div>
	</div>
	<div class="col-sm-6"></div>
	<div class="col-sm-6"></div>
</div>