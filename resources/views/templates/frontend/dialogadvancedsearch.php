<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
			<label for="name">Nome</label>
			<input type="text" name="name" ng-model="advSearch.name" id="name" class="form-control" />
		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label for="type">Tipo di locale</label>
			<select id="type" name="type" class="form-control" ng-model="advSearch.type">
				<option value="">Tutti</option>
				<option ng-repeat="type in tipiLocali track by $index" value="{{type._id}}">{{type.name}}</option>
			</select>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label for="location">Città</label>
			<input type="text" name="location" ng-model="advSearch.location" id="location" class="form-control" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
			<label for="services">Servizi</label>
			<select id="services" name="services" class="form-control" ng-model="custom.one" ng-change="selectItem(custom.one,'service')">
				<option ng-repeat="service in services" value="{{service._id}}">{{service.name}}</option>
			</select>
			<ul class="list-unstyled" style="margin-top: 15px;">
				<li ng-repeat="serv in advSearch.services track by $index"><i class="fa fa-close" ng-click="selectItem(serv,'service')"></i> 
					{{services[serv].name}} 
				</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label for="cucina">Tipo di cucina</label>
			<select id="cucina" name="cucina" class="form-control" ng-model="custom.two" ng-change="selectItem(custom.two,'tipocucina')">
				<option ng-repeat="cucina in tipiCucine" value="{{cucina._id}}">{{cucina.name}}</option>
			</select>
			<ul class="list-unstyled" style="margin-top: 15px;">
				<li ng-repeat="tipo in advSearch.types track by $index"><i class="fa fa-close" ng-click="selectItem(tipo,'tipocucina')"></i> 
					{{tipiCucine[tipo].name}} 
				</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label for="region">Regione della cucina</label>
			<select id="region" name="region" class="form-control" ng-model="custom.three" ng-change="selectItem(custom.three,'regionecucina')">
				<option ng-repeat="region in regioniCucine" value="{{region._id}}">{{region.name}}</option>
			</select>
			<ul class="list-unstyled" style="margin-top: 15px;">
				<li ng-repeat="reg in advSearch.regions track by $index">
					<i class="fa fa-close" ng-click="selectItem(reg,'regionecucina')"></i> 
					{{regioniCucine[reg].name}} 
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<label for="prezzo_max">Prezzo massimo</label>
        <div class="range-slider">
            <input class="range-slider__range" type="range" ng-model="advSearch.maxPrice" min="0" max="200" step="5" id="prezzo_max">
            <span class="range-slider__value">{{advSearch.maxPrice}}</span>
        </div>
	</div>
</div>
<div class="row" style="margin-top: 15px;">
	<!--<div class="col-sm-5">
		<div class="form-group">
			<label for="social">Pagine social</label>
		</div>
		<div class="row">
			<label class="label--checkbox col-sm-4" ng-repeat="(name,account) in advSearch.social">
	            <input class="checkbox" type="checkbox" ng-model="account">
	            	<i class="fa fa-{{name}}" style="font-size: 22px;" ng-if="name != 'googlePlus' && name != 'youtube'" title="{{name}}"></i>
	            	<i class="fa fa-google-plus" style="font-size: 22px;" ng-if="name == 'googlePlus'" title="{{name}}"></i>
	            	<i class="fa fa-youtube-play" style="font-size: 22px;" ng-if="name == 'youtube'" title="{{name}}"></i> 
	        </label>
	    </div>
	</div>-->
	<div class="col-sm-4">
		<div class="form-group">
			<label for="type">Virtual Tour</label>
			<label class="label--checkbox block">
	            <input class="checkbox" type="checkbox" ng-model="advSearch.virtualTour">Si, voglio ristoranti con il virtual tour
	        </label>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label for="location">Prenotazione</label>
			<label class="label--checkbox block">
	            <input class="checkbox" type="checkbox" ng-model="advSearch.prenota">Voglio poter prenotare
	        </label>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label for="type">Menu</label>
			<label class="label--checkbox block">
	            <input class="checkbox" type="checkbox" ng-model="advSearch.menu">Si, voglio ristoranti con il menu
	        </label>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		&nbsp;
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<input class="btn btn-block ghost" value="Cerca" ng-click="SearchAdvanced()" />
		</div>
	</div>
	<div class="col-sm-4">
		&nbsp;
	</div>
</div>