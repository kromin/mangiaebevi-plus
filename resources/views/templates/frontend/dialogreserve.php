<div class="form-info">
	<h3>Confermi la prenotazione?</h3>
	<form name="reserveForm" ng-submit="reserveRestaurant(reserveFormData)">
		<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<label for="name">Nome</label>
			<input type="text" ng-model="reserveFormData.name" id="name" class="form-control" required/>
		</div>
		<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<label for="surname">Cognome</label>
			<input type="text" ng-model="reserveFormData.surname" id="surname" class="form-control" required/>
		</div>
		<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<label for="phone">Telefono</label>
			<input type="text" ng-model="reserveFormData.phone" id="phone" class="form-control" required/>
		</div>
		<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<label for="email">Email</label>
			<input type="email" ng-model="reserveFormData.email" id="email" class="form-control" required/>
		</div>
		<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<label for="password">Password</label>
			<input type="password" ng-model="reserveFormData.password" id="password" class="form-control" required/>
		</div>
		<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<input type="checkbox" ng-model="reserveFormData.check" id="check" class="form-control" required/>
			<span>Confermi di accettare i termini e condizioni di MangiaeBevi+</span>
		</div>
		<div class="form-group">
			<label for="people">Quanti sarete</label>
			<input type="number" style="width: 100px;" ng-model="reserveFormData.people" id="people" class="form-control" required min="1" max="20" />
		</div>
		<div class="form-group">
			<input type="submit" ng-disabled="reserveForm.$invalid" class="btn btn-block btn-primary login-btn" value="Conferma" />
		</div>
		<a ng-click="closeDialog()">No, mi sono sbagliato</a>
	</form>
</div>