<div class="plate_dialog">
	<h3><a href="/{{marker_open.city}}/{{marker_open.slug}}">{{marker_open.name}}</a></h3>
	<p class="uppercase text-warning" style="margin-top: -11px;">{{tipiLocali[marker_open.tipoLocale[0]].name}}</p>
	
	<div class="restaurant_image_container" back-img img="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_STATIC_DOMAIN');?>/restaurant/small/{{marker_open.image}}.png" ng-if="marker_open.image && marker_open.image != ''">
	</div>
	
	<p style="margin-top: 38px"><i class="fa fa-map-marker"></i> {{marker_open.address}}</p>		
	<div class="clearfix"></div>
</div>