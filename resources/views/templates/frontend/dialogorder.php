<div class="cart-form margin-auto">
	<h3>{{dialog_order_plates.menu_name}} <span ng-if="dialog_order_plates.menu_type == 1">{{dialog_order_plates.menu_qty}} x</span></h3>
	<ul class="list-unstyled cart_form cart_menu_order" ng-if="dialog_order_plates.menu_type == 1">
		<li ng-repeat="plate in dialog_order_plates.plates track by $index">
			<div class="row order_plate_container">
				<div class="col-sm-9 no-padding">
					<p class="plate_name">
					{{plate.name}}</p>
					<p>{{plate.description}}</p>
					<ul class="list-unstyled option_list_in_chart" ng-repeat="o in plate.options">
						<li ng-repeat="opt in o.options"><small>{{opt.num}} x {{opt.name}}</small></li>
					</ul>
				</div>			
			</div>
		</li>
	</ul>
	<ul class="list-unstyled cart_form cart_menu_order" ng-if="dialog_order_plates.menu_type == 0 || !dialog_order_plates.menu_type">
		<li ng-repeat="plate in dialog_order_plates.plates track by $index">
			<div class="row order_plate_container">
				<div class="col-sm-9 no-padding">
					<p class="plate_name">
					<span class="light">{{plate.qty}} x</span> {{plate.name}}</p>
					<ul class="list-unstyled option_list_in_chart" ng-repeat="option in plate.options">
						<li ng-repeat="opt in option.options" ng-if="opt.selected == 1">{{opt.name}}</li>
					</ul>
				</div>
				<div class="col-sm-3" style="padding-left: 0;">
					<p class="pull-right plate_price">{{plate.total_price|number:2}} € 																			
					</p>
				</div>
			</div>
		</li>
	</ul>
	<div class="riepilogo">
		<ul class="list-unstyled col-sm-12">
			<li>
				Totale: <span class="pull-right">{{dialog_order_plates.price|number:2}} €</span>
			</li>			
		</ul>
	</div>
</div>
<div class="text-center" ng-if="(!dialog_order_reservation.preorder.paid || dialog_order_reservation.preorder.paid == 0) && !order_from_restaurant" style="padding-top: 20px;">
	<a class="btn btn-primary" ng-click="payReservation(dialog_order_reservation)">Paga con paypal</a>
</div>
<div class="text-center" ng-if="dialog_order_plates.paid && dialog_order_plates.paid == 1" style="padding-top: 20px;">
	Ordine pagato con Paypal
</div>