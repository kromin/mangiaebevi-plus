<div id="profilePage" class="container bg-white"
ng-init="
setSEO('Privacy policy | Mangiaebevi','/privacy-policy/','','','','MangiaeBevi, privacy e cookie policy')"
>

	<div class="panel pad-big-o">

<h2>Trattamento dei Dati Personali per gli Utenti</h2>
<p>MangiaeBevi, parte del Gruppo Digikom srl, con sede legale in via Giunio Antonio Resti 71 – 00143 Roma (RM), riconosce l’importanza di tutelare la privacy e i diritti degli utenti (“Utenti”) che richiedono la registrazione al sito www.mangiaebevi.it (SitoWeb). Internet è potenzialmente uno strumento estremamente potente per la circolazione di dati personali e MangiaeBevi assume il serio impegno di rispettare le regole relative alla tutela dei dati personali e la sicurezza al fine di garantire una navigazione sicura, controllata e confidenziale.<br>
Potranno essere introdotte in futuro delle modifiche a questa informativa al fine di assicurare il costante rispetto delle norme di legge applicabili, in caso di loro cambiamento e/o aggiornamento.<br>
Invitiamo gli Utenti a leggere attentamente le regole che applichiamo per la raccolta e il trattamento dei dati personali e per continuare a fornire un servizio soddisfacente.<br>
La presente informativa, resa ai sensi della vigente normativa in materia di protezione dei dati personali, descrive le modalità di gestione e trattamento dei dati personali di:</p>
<ul>
<li>Utenti che consultano ed utilizzano il Sito Web</li>
<li>Utenti che inseriscono i propri dati personali nel modulo di registrazione presente nel Sito al fine di aderire alle Condizioni.</li>
<li>Utenti che partecipano a Forum tematico sul sito di MangiaeBevi</li>
<li>Utenti che si registrano alla Community MangiaeBevi.</li>
</ul>
<p>L’Utente è tenuto a fornire dati personali veritieri, corretti ed aggiornati. L’Utente riconosce che nel caso in cui non fornisca dati personali veritieri, corretti ed aggiornati, il Titolare sarà impossibilitato a fornire i propri servizi.</p>
<h2>Titolare del Trattamento</h2>
<p>I titolari del trattamento sono MangiaeBevi (di seguito, ” MangiaeBevi ” o il “Titolare”) e le società con essa in rapporto di controllo e collegamento o appartenenti al Gruppo Digikom srl (di seguito i “Titolari”).<br>
L’elenco completo ed aggiornato delle società in rapporto di controllo e collegamento con MangiaeBevi o facenti parte del Gruppo Digikom srl potrà essere richiesto al seguente indirizzo e-mail: <a href="mailto:privacy@mangiaebevi.it">privacy@mangiaebevi.it</a>.</p>
<h2>Modalità di raccolta dei dati</h2>
<p>I dati personali degli Utenti sono raccolti:</p>
<ul>
<li>Presso l’Utente per suo diretto conferimento via telematica mediante compilazione del format di registrazione presente sul SitoWeb. Si tratta di dati quali nome, cognome, indirizzo e-mail, necessari per l’invio degli aggiornamenti periodici relativi alle migliori offerte presenti sul Sito web.</li>
<li>Presso l’Utente nel momento in cui effettua la creazione di un proprio Profilo Personale, visibile a qualunque utente iscritto alla community di MangiaeBevi, sulla quale sono presenti, liberamente visibili e consultabili i seguenti dati: Data di nascita, Sesso, Provincia di residenza (nell’eventualità che questi dati fossero stati comunicati all’atto dell’iscrizione MangiaeBevi o successivamente), più tutti gli ulteriori dati che successivamente verranno inseriti per arricchirla. in tal modo si acconsente che il proprio nome utente sia pubblico e visibile a tutti gli altri utenti, iscritti e non, in qualità di autore degli eventuali contenuti inseriti nel sito (commenti ai locali), ecc.</li>
<li>Mediante i sistemi informatici e le procedure software preposte al funzionamento del SitoWeb nel corso del loro normale esercizio, in modo implicito, a seguito dell’uso dei protocolli di comunicazione di Internet. Si tratta di informazioni che non sono raccolte per essere associate a Utenti identificati, ma che per loro stessa natura potrebbero, attraverso elaborazioni ed associazioni con dati detenuti da terzi, permettere di identificare gli Utenti. In questa categoria di dati rientrano gli indirizzi IP o i nomi a dominio dei computer utilizzati dagli Utenti che si collegano al sito, gli indirizzi in notazione URI (Uniform Resource Identifier) delle risorse richieste, l’orario della richiesta, il metodo utilizzato nel sottoporre la richiesta al server, la dimensione del file ottenuto in risposta, il codice numerico indicante lo stato della risposta data dal server (buon fine, errore, ecc.) ed altri parametri relativi al sistema operativo e all’ambiente informatico dell’Utente. Questi dati vengono utilizzati al solo fine di ricavare informazioni statistiche anonime sull’uso del sito e per controllarne il corretto funzionamento. I dati potrebbero essere utilizzati per l’accertamento di responsabilità in caso di ipotetici reati informatici ai danni del sito.</li>
<li>Dal SitoWeb attraverso l’uso di cookies. I cookies sono informazioni, spesso contenenti un codice identificativo unico anonimo, che vengono inviati al browser da un server web e che vengono successivamente memorizzati sul disco fisso del computer dell’Utente. I cookies vengono poi riletti e riconosciuti dal SitoWeb che li ha inviati in caso di successivi collegamenti.</li>
</ul>
<p>I dati eventualmente ottenuti dai cookies vengono utilizzati dal Titolare direttamente o per tramite di società che collaborano con questo, al fine di assicurare un più semplice, immediato e rapido accesso al SitoWeb e utilizzo del Servizio, una più agevole navigazione sul sito stesso da parte dell’Utente.<br>
La ricezione dei cookies utilizzati in questo SitoWeb potrà essere interrotta in qualsiasi momento dall’Utente modificando le impostazioni del suo browser.<br>
Il trattamento dei dati personali degli Utenti avverrà con modalità manuali o elettroniche idonee a garantirne, in relazione alle finalità per le quali essi sono stati comunicati e raccolti, la sicurezza e la riservatezza, nonché ad evitare accessi non autorizzati ai dati stessi, per il tempo strettamente necessario a conseguire gli scopi per cui sono stati raccolti.</p>
<h2>Luogo del trattamento dei Dati</h2>
<p>Il trattamento dei dati avrà luogo presso la sede legale e operativa dei Titolari e presso la server-farm di MangiaeBevi sita a Vicenza, nonché presso i soggetti ai quali i dati potranno essere comunicati.</p>
<h2>Finalità del trattamento dei tuoi dati personali e comunicazione</h2>
<p>A parte quanto già specificato per i dati di navigazione, i dati personali forniti dagli Utenti sono utilizzati per le seguenti finalità:</p>
<ul>
<li>raccolta,conservazione ed elaborazione ai fini dell’instaurazione e gestione operativa ed amministrativa del rapporto contrattuale connesso all’erogazione dei Servizi;</li>
<li>utilizzo dei dati per effettuare comunicazioni via e-mail, telefono o SMS relative allo svolgimento del rapporto contrattuale instaurato;</li>
<li>adempiere ad obblighi di legge o di regolamenti vigenti;<br>
raccolta, conservazione ed elaborazione dei dati per compiere analisi statistiche in forma anonima e/o aggregata;</li>
<li>invio da parte di MangiaeBevi, salvo diniego dell’Utente, di materiale informativo e pubblicitario via e-mail relativo a prodotti o servizi analoghi o affini ai servizi o prodotti offerti sul SitoWeb;</li>
<li>previo espresso consenso dell’Utente, invio da parte dei Titolari via e-mail, telefono, posta, SMS o MMS di offerte commerciali relative ai prodotti offerti dai Titolari medesimi o dai Partner commerciali di quest’ultimi, facendo salvo quanto previsto alla lettera E) che precede.</li>
</ul>
<p>Il mancato conferimento dei dati personali può comportare per l’Utente l’impossibilità di ottenere le prestazioni richieste in ordine alle finalità di cui ai punti A) e B) e l’impossibilità per il Titolare di fornire i Servizi correttamente e di adempiere alle obbligazioni contrattuali previste nelle condizioni generali di vendita. Il trattamento di tali dati per le finalità indicate sub A) , B), C), D) ed E), non comporta la richiesta di consenso dell’interessato.<br>
Il conferimento dei dati per le finalità di cui al punto F) è facoltativo. Il trattamento di tali dati per la finalità indicata al punto F) comporta la richiesta di consenso dell’interessato. Il mancato conferimento dei dati o la mancata prestazione del consenso da parte dell’Utente per questa finalità comporterà che, fatto salvo quanto disposto alla lettera E), i Titolari non potranno inviare all’Utente offerte commerciali di cui alla lettera F) del presente articolo.<br>
Il Titolare si impegna ad assicurare che i dati personali di cui verrà a conoscenza non saranno diffusi e saranno trattati con modalità idonee a garantirne la sicurezza e la riservatezza, nonché ad evitare accessi non autorizzati ai dati stessi. I dati personali e le informazioni forniti dagli utenti potranno essere comunicati, per le finalità di cui sopra, alle seguenti categorie di soggetti:</p>
<ul>
<li>dipendenti e/o collaboratori che prestino attività di assistenza e consulenza al Titolare per l’area dell’Amministrazione, del Prodotto, del Legale, del Customer Care, dei Sistemi Informativi, nonché da incaricati della manutenzione della rete aziendale e delle apparecchiature hardware e software in uso;</li>
<li>soggetti cui la facoltà di accedere ai dati sia riconosciuta da disposizioni di legge o da ordini delle autorità;</li>
<li>soggetti delegati e/o incaricati dai Titolari all’espletamento delle attività correlate alla erogazione del Servizio.</li>
</ul>
<p>L’elenco nominativo dei soggetti a cui i dati personali degli Utenti potranno essere comunicati è disponibile presso il Titolare scrivendo a <a href="mailto:privacy@mangiaebevi.it">privacy@mangiaebevi.it</a>.</p>
<h2>Diritti dell’interessato</h2>
<p>Ogni utente potrà esercitare, mediante richiesta scritta da inviare al Titolare, i seguenti diritti: (I) ottenere dal Titolare la conferma dell’esistenza di propri dati personali; (II) avere conoscenza dell’origine dei dati, delle finalità e modalità del trattamento, nonché della logica applicata in caso di trattamento effettuato con l’ausilio di strumenti elettronici; (III) ottenere gli estremi identificativi del Titolare e dei responsabili del trattamento, se designati; (IV) conoscere i soggetti o le categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza in qualità di rappresentante designato nel territorio dello Stato, di responsabili o incaricati; (V) ottenere la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, nonché l’aggiornamento, la rettifica o, se vi è interesse, l’integrazione dei dati e (VI) l’attestazione che le operazioni di cui al punto (V) che precede sono state portate a conoscenza, anche per quanto riguarda il loro contenuto, di coloro ai quali i dati sono stati comunicati o diffusi, eccettuato il caso in cui tale operazione si rivela impossibile o comporta un impiego di mezzi sproporzionato rispetto al diritto tutelato; (VII) opporsi al trattamento in tutto o in parte, per motivi legittimi (VIII) opporsi al trattamento per invio di materiale pubblicitario o di vendita diretta o per il compimento di ricerche di mercato o di comunicazione commerciale.<br>
Le richieste di cui sopra vanno rivolte a MangiaeBevi, all’indirizzo privacy@mangiaebevi.it. L’utente ha inoltre il diritto, sempre dietro espressa richiesta scritta via e-mail, di ottenere la rettifica di ogni dato personale inesatto o inaccurato o di ottenerne la cancellazione, nei limiti di quanto previsto dalla legge.<br>
Inoltre per non ricevere più le offerte commerciali e promozionali via e-mail da parte dei Titolari, oltre ad esercitare i diritti di cui sopra, l’Utente può anche seguire la semplice procedura attivata dal link “disiscrizione” disponibile in tutte le comunicazioni commerciali inviate dai Titolari.</p>

<br /><br />

	</div>
</div>