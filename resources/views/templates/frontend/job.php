<div id="staticPage" 
ng-init="
setSEO('Lavora con noi | Mangiaebevi','/lavora-con-noi/','/img/pages/business.jpg','/img/pages/business.jpg','','Lavora con noi, entra nel team di MangiaeBevi, consulta le posizioni aperte ed inviaci il tuo curriculum')"
>
	<section class="bg_image vcenter_parent full-height" back-img img="/img/pages/business.jpg">
		<div class="overlay"></div>
		<div class="vcenter_child big_width main_section_big">
			<div class="row">
				<h1 class="center white-text" style="padding: 0;">Lavora con noi <br /><small>Entra nel nostro team</small></h1>	
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<p>
						MangiaeBevi è un’azienda dinamica, innovativa ed in forte sviluppo, sempre attenta alla valutazione di nuove figure professionali. 
						Se vuoi lavorare con noi e pensi di avere i requisiti necessari, 
						inviaci il tuo curriculum e una breve lettera di presentazione a: <a class="text-primary" href="mailto:job@mangiaebevi.it">job@mangiaebevi.it</a>
					</p>				
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2>Posizioni aperte</h2>
					<h3><b>AGENTI PLURIMANDATARI IN TUTTA ITALIA</b></h3>
					<p>
						La figura si occuperà di sviluppare un portafoglio clienti nell’area di competenza, attraverso una proposta articolata di prodotti/servizi innovativi.
					</p>
					<p>Ricerchiamo venditori brillanti e responsabili con spiccate capacità relazionali e con forte propensione ed abitudine all’attività commerciale che dimostrino costanza e tenacia nel raggiungimento degli obiettivi fissati e che abbiano un forte spirito di iniziativa/pro-attività. È richiesta una precedente esperienza di vendita di prodotti o servizi alle aziende.</p>
				</div>
				<div class="col-sm-6">
					<h3>Cosa offriamo</h3>
					<p>
						<ul style="padding-left: 18px; font-size: 16px;">
							<li>la possibilità di entrare in un’azienda giovane, dinamica, innovativa ed in forte sviluppo</li>
							<li>un’esperienza nel mondo dei new media</li>
							<li>compenso commisurato all’esperienza</li>
							<li>incentivi al raggiungimento degli obiettivi</li>
						</ul>
					</p>
				</div>
				<div class="col-sm-6">
					<h3>Attività</h3>
					<!--<p>Il candidato ideale – a diretto riporto del Direttore Commerciale – sarà in grado di svolgere in autonomia le seguenti attività:</p>-->
					<ul style="padding-left: 18px; font-size: 16px;">
						<li>Sviluppare il portafoglio clienti nell’area assegnata.</li>
						<li>Mantenere con costanza, nel tempo, il rapporto con la clientela attraverso un’alta attenzione alle relazioni.</li>
						<li>Garantire il raggiungimento dei target commerciali stabiliti.</li>
						<li>Interpretare le esigenze del mercato e proporre alla Direzione nuove opportunità commerciali.</li>
					</ul>
				</div>
			</div>
		</div>
		<?php /*
		<div class="container">
			<div class="col-sm-12">
				<h2 class="center section-title">Invia la tua candidatura</h2>
			</div>
			<div class="margin-big-v small_block block_auto">
				<p>
				Compila il form per contattarci
				</p>
				<p ng-if="contact_error" class="text-danger">
					Qualcosa è andato storto, ricontrolla tutti i campi
				</p>
				<p ng-if="contact_ok" class="text-success">
					Messaggio inviato
				</p>
				<div class="margin-big-v">
					<form id="contactForm" ng-submit="contactSubmit()">
						<div class="form-group">
							<label for="name">Nome e cognome *</label>
							<input class="form-control" type="text" ng-model="contact.name" id="name" required/>
						</div>
						<div class="form-group">
							<label for="email">Indirizzo email *</label>
							<input class="form-control" type="email" ng-model="contact.email" id="email" required/>
						</div>
						<div class="form-group">
							<label for="phone">Telefono</label>
							<input class="form-control" type="text" ng-model="contact.phone" id="phone" />
						</div>
						<div class="form-group">
							<label for="name">Messaggio *</label>
							<textarea class="form-control" ng-model="contact.message" msd-elastic rows="3" style="line-height: 18px; padding: 12px; resize: none;" />
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary ghost btn-no-margin" value="invia" ng-disabled="contactForm.$invalid" />
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php */ ?>
	</section>
</div>