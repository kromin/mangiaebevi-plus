<div class="restaurant_social_modal">
    <h3>Link alle pagine social del ristorante</h3>
    <div class="form-group with-addon">

        <input type="text" class="form-control" ng-model="restaurant.social[7].tripadvisor" placeholder="Tripadvisor" />

        <span class="input-addon">
        <button class="btn btn-primary"><i class="fa fa-tripadvisor" aria-hidden="true"></i></button>
    </span>

    </div>
    <div class="form-group with-addon">


        <input type="text" class="form-control" ng-model="restaurant.social[0].facebook" placeholder="Facebook" />
        <span class="input-addon">
        <button class="btn btn-primary"><i class="fa fa-facebook" aria-hidden="true"></i></button>
    </span>

    </div>
    <div class="form-group with-addon">

        <input type="text" class="form-control" ng-model="restaurant.social[1].twitter" placeholder="Twitter" />
        <span class="input-addon">
        <button class="btn btn-primary"><i class="fa fa-twitter" aria-hidden="true"></i></button>
    </span>

    </div>
    <div class="form-group with-addon">

        <input type="text" class="form-control" ng-model="restaurant.social[4].googlePlus" placeholder="Google Plus" />
        <span class="input-addon">
        <button class="btn btn-primary"><i class="fa fa-google-plus" aria-hidden="true"></i></button>
    </span>

    </div>
    <div class="form-group with-addon">

        <input type="text" class="form-control" ng-model="restaurant.social[2].pinterest" placeholder="Pinterest" />
        <span class="input-addon">
        <button class="btn btn-primary"><i class="fa fa-pinterest" aria-hidden="true"></i></button>
    </span>

    </div>
    <div class="form-group with-addon">

        <input type="text" class="form-control" ng-model="restaurant.social[3].instagram" placeholder="Instagram" />
        <span class="input-addon">
        <button class="btn btn-primary"><i class="fa fa-instagram" aria-hidden="true"></i></button>
    </span>

    </div>
    <div class="form-group with-addon">

        <input type="text" class="form-control" ng-model="restaurant.social[5].linkedin" placeholder="Linkedin" />
        <span class="input-addon">
        <button class="btn btn-primary"><i class="fa fa-linkedin" aria-hidden="true"></i></button>
    </span>

    </div>
    <div class="form-group with-addon">

        <input type="text" class="form-control" ng-model="restaurant.social[6].youtube" placeholder="Youtube" />
        <span class="input-addon">
        <button class="btn btn-primary"><i class="fa fa-youtube" aria-hidden="true"></i></button>
    </span>

    </div>
</div>