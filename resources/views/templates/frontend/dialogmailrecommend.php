<div class="form-info">
	<h3>Raccomanda {{current_restaurant.name}} a un amico</h3>
	<form name="reccomendForm" ng-submit="reccomendMail()">
		<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<label for="name">Il tuo nome</label>
			<input type="text" ng-model="reccomendFormData.name" id="name" class="form-control" required/>
		</div>
		<div class="form-group">
			<label for="fname">Il <span class="text-danger">suo</span> nome</label>
			<input type="text" ng-model="reccomendFormData.fname" id="fname" class="form-control" required/>
		</div>
		<div class="form-group">
			<label for="email">Il <span class="text-danger">suo</span> indirizzo email</label>
			<input type="email" ng-model="reccomendFormData.email" id="email" class="form-control" required/>
		</div>
		<div class="form-group">
			<label for="message">Messaggio per {{reccomendFormData.fname}}</label>
			<textarea msd-elastic ng-model="reccomendFormData.message" id="message" rows="3" class="form-control" style="min-height: 72px;" required>
			</textarea>
		</div>
		<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<input type="checkbox" ng-model="reccomendFormData.check" id="check" required/>
			<span>Accetti i termini e condizioni di MangiaeBevi+</span>
		</div>
		<div class="form-group">
			<input type="submit" ng-disabled="reccomendForm.$invalid" class="btn btn-primary ghost btn-no-margin" value="Invia" />
		</div>
	</form>
</div>