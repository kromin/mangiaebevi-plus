<div class="form-info">
	<h3>Segnalaci un errore nella scheda del ristorante</h3>
	<form id="segnalazioneForm" ng-submit="confirmSegnalazione()">
		<div class="form-group">
			<label for="name">Nome e cognome</label>
			<input class="form-control" type="text" ng-model="segnalazione.name" id="name" required/>
		</div>
		<div class="form-group">
			<label for="email">Indirizzo email *</label>
			<input class="form-control" type="email" ng-model="segnalazione.email" id="email" required/>
		</div>
		<div class="form-group">
			<label for="phone">Telefono</label>
			<input class="form-control" type="text" ng-model="segnalazione.phone" id="phone" />
		</div>
		<div class="form-group">
			<label for="name">Messaggio *</label>
			<textarea class="form-control" ng-model="segnalazione.message" msd-elastic rows="3" style="line-height: 18px; padding: 12px; resize: none !important; min-height: 100px;" />
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary ghost btn-no-margin" value="invia" ng-disabled="segnalazioneForm.$invalid" />
		</div>
	</form>
</div>