<!-- Home page -->
<script type="application/ld+json" mantain>
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/s/{search_term_string}/",
    "query-input": "required name=search_term_string"
  }
}
</script>
<div class="fixed_bg" style="background-image: url(/img/home/bg.jpg);"></div>
<div class="section MainImages" ng-init="setSEO('Mangiaebevi','/','/img/homebg.jpg','/img/homebg.jpg','food','MangiaeBevi, la guida per il palato, cerca tra centinaia di ristoranti, scopri la cucina, i menu, gli orari e prenota comodamente online')">

    <div id="home_images">
        <div class="first-section-overlay"></div>
        <?php /*<div class="image-container first_main_image" style="background-image: url(/img/home/home_1.jpg);" ng-class="{'active' : current_image == 1}" ng-init='load()'>
        </div>
        <div class="image-container" style="background-image: url(/img/home/home_2.jpg);" ng-class="{'active' : current_image == 2}">
        </div>
        <div class="image-container" style="background-image: url(/img/home/home_3.jpg);" ng-class="{'active' : current_image == 3}">
        </div>*/?>
        <div class="embed-responsive embed-responsive-16by9" style="width: 100%;">
            <video autoplay loop poster="/img/copertinavideo.png" id="bgvid" class="embed-responsive-item">
                <source src="/video/compressed_video.webm" type="video/webm">
                <source src="/video/compressed_video.mp4" type="video/mp4">
            </video>
        </div>
        <?php /*<div id="main_page_link_footer" class="hidden-sm hidden-xs">
            <div class="container">
                <div class="col-sm-3">
                    <object type="image/svg+xml" data="/img/svg/map.svg"></object>
                    <br />
                    <b>CERCA</b>
                    <br /> un ristorante
                </div>
                <div class="col-sm-3">
                    <object type="image/svg+xml" data="/img/svg/menu.svg"></object>
                    <br />
                    <b>CONSULTA</b>
                    <br /> il menu
                </div>
                <div class="col-sm-3">
                    <object type="image/svg+xml" data="/img/svg/calendar.svg"></object>
                    <br />
                    <b>PRENOTA</b>
                    <br /> un tavolo
                </div>
                <div class="col-sm-3">
                    <object type="image/svg+xml" data="/img/svg/delivery.svg"></object>
                    <br />
                    <b>ORDINA</b>
                    <br /> il tuo pasto
                </div>
            </div>
        </div>*/?>
    </div>
    <div id="main_page_content">
        <?php /*<h1>Il tuo ristorante preferito 			
			<br /><span class="text-primary">dove</span> e <span class="text-primary">quando</span> vuoi tu</h1>
        <p>
            <ul class="action_list">
                <li ng-class="{'active':current_action == 1}" ng-click="current_action = 1"><span class="circle"></span> cerca</li>
                <li ng-class="{'active':current_action == 2}" ng-click="current_action = 2"><span class="circle"></span> prenota</li>
                <li ng-class="{'active':current_action == 3}" ng-click="current_action = 3"><span class="circle"></span> ordina</li>
            </ul>
        </p>*/?>
        <form>
            <input ng-model="main.search" name="address" placeholder="{{placehoder_form_texts[current_action]}}" ng-enter="goToSearch()" />
            <button ng-click="goToSearch()">Cerca un ristorante</button>
        </form>
    </div>
</div>
<div class="section MainCities">
    <div class="container">
        <h2>Cerca nella tua città</h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3" ng-repeat="homeimage in homeCities track by $index" ng-if="$index < 4">
                <a href="/citta/{{homeimage.name}}/">
                    <div class="city_container" back-img img="{{staticURL}}/homecity/big/{{homeimage.image}}.png">
                        <div class="overlay"></div>
                        <h3>{{homeimage.name}}</h3>
                    </div>
                </a>
            </div>
            
        </div>
        <p class="all_cities_link">
            <a ng-click="hidden_cities = !hidden_cities" class="open_cities_link">
				Vedi le altre città <br /><i class="fa fa-chevron-down"></i></a>
        </p>
        <div class="row hidden_cities" ng-class="{'open':hidden_cities}">

            <div class="col-md-2" ng-repeat="homeimage in homeCities track by $index" ng-if="$index > 3">
                <a href="/citta/{{homeimage.name}}/">
                    <div class="city_container" back-img img="{{staticURL}}/homecity/big/{{homeimage.image}}.png">
                        <div class="overlay"></div>
                        <h3>{{homeimage.name}}</h3>
                    </div>
                </a>
            </div>

            

        </div>
        
        <?php 
        /*function returnFibonacci ($n){
            $arr_numbers = [];
            for ($start = 0; $start < $n; $start++):
                $arr_numbers[] = ($start < 2) ? 1 : $arr_numbers[$start-2]+$arr_numbers[$start-1];    
            endfor;
            return $arr_numbers[count($arr_numbers)-1];
        }
        echo "Fibonacci: ".returnFibonacci(1000);
        */
        ?>
        
        
    </div>
</div>
<div class="section MainApp">
    <div class="overlay"></div>
    <div class="container title-container">
        <h2>I migliori ristoranti a portata di mano</h2>
        <p class="hero_text">SCARICA ORA LA NOSTRA APP</p>
        <?php /*<a class="btn btn-primary ghost" style="
    display: block;
    width: 250px;
    margin: 50px auto;
    position: absolute;
    left: 50%;
    z-index: 4;
    margin-left: -125px;
    background: #fff;
    transform: rotate(-20deg);
    color: #00b4cc !important;
">COMING SOON</a>*/?>
    </div>
    <div class="app_btn">
        <div class="container more">
            <a class="btn btn-primary ghost app_button pull-left" href="https://itunes.apple.com/it/app/mangiaebevi/id424492685?mt=8"><i class="fa fa-apple"></i> Iphone</a>
            <a class="btn btn-primary ghost app_button pull-right" href="https://play.google.com/store/apps/details?id=com.digikom.mangiaebevi&hl=it"><i class="fa fa-android"></i> Android</a>
        </div>
    </div>
    
    <!--<div class="container title-container">
		
		<div class="center">
			<div>
				<img ng-src="/img/applestore_btn.png" alt="" title="" />
			</div>
			<div>
				<img ng-src="/img/playstore_btn.png" alt="" title="" />
			</div>
		</div>
	</div>-->
    <div class="images_bg_container">
        <img ng-src="/img/home/cellulari.png" alt="" title="" class="reveal" />
    </div>
</div>
<div class="MainFooter">
</div>