<div id="profilePage" class="container">

	<div class="panel pad-big-o">

		<h4>NOTE LEGALI</h4>
		<p>
		Digikom s.r.l <br />
		<b>Sede Operativa:</b>
		Via Carlo Pirzio Biroli, 4
		00146 Roma <br />
		<b>Sede Legale:</b>
		Via Giunio Antonio Resti, 1
		00143 Roma <br />
		<b>E-Mail:</b> <a href="mailto:info@mangiaebevi.it">info@mangiabevi.it</a><br />
		<b>Amministratore:</b> Fabio Carnevali <br />

		Registro Start Up Innovative CCIAA di Roma 02/03/2016
		<br /><br />
		Partita Iva: 12452701001
		<br /><br />

		Responsabili per i contenuti di questa pagina è: Fabio Carnevali
		<br />
		Risoluzione di controversie online (Art. 14 section 1 ODR-Regulation (EU 524/2013): La commissione Europea offre una piattaforma per la risoluzione di controversie per i consumatori online, puoi trovarla qui <a href="http://ec.europa.eu/consumers/odr/" target="_blank">http://ec.europa.eu/consumers/odr/</a>
		</p>

	</div>
</div>