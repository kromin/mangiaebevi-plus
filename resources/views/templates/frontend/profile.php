 <!-- Page -->
  
  <div id="profilePage" class="container bg-white">
    
      <div class="col-lg-3 margin-big-v">
        <!-- Page Widget -->
        <div class="panel text-center" ><!--ng-controller="ProfileCtrl"-->
          <div class="profile-header margin-big-v pad-big-v">
            <div>              
              
              <div class="contain-image-actions">
                <img ng-src="{{staticURL}}/users/small/{{view_user.user.avatar}}.png" alt="">
                <div class="actions" ng-if="itsme">
                  <div ng-if="!uploading_avatar">
                    <i class="fa fa-upload"></i>
                    <input class="image-icon" type="file" ngf-select ng-model="change_avatar.avatar" name="change_avatar.avatar" ng-change="uploadAvatar()" title="Cambia" />
                  </div>
                </div>
                <div ng-show="uploading_avatar" class="always_visible_action" ng-if="itsme">
                  <div class="spinner spinner-bounce-middle"></div>
                </div>
              </div>
              <h3 class="profile-user uppercase size-medium spacing">{{view_user.user.name}} {{view_user.user.surname}}</h3>
              <!--<p class="profile-job size-small uppercase">
                <?php /*
                switch(auth()->user()->role):
                  case 2:
                  echo '<span class="badge badge-danger">Ristoratore</span>';
                  break;
                  case 3:
                  echo '<span class="badge badge-info">Editore</span>';
                  break;
                  case 4:
                  echo '<span class="badge badge-primary">Amministratore</span>';
                  break;
                  case 9:
                  echo '<span class="badge badge-success">Dio</span>';
                  break;
                endswitch;
                
                if(auth()->user()->role >= 4):
                  ?>

                <?php
                endif;
                */
                ?>

              </p>-->
              <p class="margin-big-v margin-big-o" ng-if="view_user.user.description">{{view_user.user.description}}</p>
              <!--<div class="profile-social">
                <a class="icon bd-twitter" href="javascript:void(0)"></a>
                <a class="icon bd-facebook" href="javascript:void(0)"></a>
                <a class="icon bd-dribbble" href="javascript:void(0)"></a>
                <a class="icon bd-github" href="javascript:void(0)"></a>
              </div>-->
              <!--<button type="button" class="btn btn-primary">Follow</button>-->
            </div>
          </div>
          <div class="profile-footer">
            <div class="row no-space">
              <div class="col-sm-6">
                <strong class="profile-stat-count">
                  {{view_user.user.followedby.length}}</strong>
                <span class="uppercase spacing size-small">Follower</span>
              </div>
              <div class="col-sm-6">
                <strong class="profile-stat-count">
                  {{view_user.user.following.length}}
                </strong>
                <span class="uppercase spacing size-small">Following</span>
              </div>                
            </div>
          </div>
        </div>
        <!-- End Page Widget -->
      </div>
      <div class="col-lg-9 margin-big-v">
        <!-- Panel -->
        <div class="panel">
          <div class="panel-body nav-tabs-animate">
            <ul class="nav nav-tabs nav-tabs-line" ng-if="itsme">
              <li ng-class="{'active':current_tab == 'notifications'}"><a ng-click="setTab('notifications')">Notifiche <!--<span class="badge badge-danger">5</span>--></a></li>
              <li ng-class="{'active':current_tab == 'profile'}"><a ng-click="setTab('profile')">Profilo</a></li>
              <li ng-class="{'active':current_tab == 'favourite'}"><a ng-click="setTab('favourite')">Preferiti</a></li>
              <li ng-class="{'active':current_tab == 'reservations'}"><a ng-click="setTab('reservations')">Prenotazioni</a></li>
            </ul>
            <ul class="nav nav-tabs nav-tabs-line" ng-if="!itsme">
              <li ng-class="{'active':current_tab == 'favourite'}"><a ng-click="setTab('favourite')">Preferiti</a></li>
              </ul>
            <div class="tab-content">
            
            <div ng-if="current_tab == 'notifications'" class="tab-pane"  ng-class="{'active':current_tab == 'notifications'}" id="notifiche">
                <ul class="list-group notifications_list" ng-if="notifications.length > 0">
                  <li class="list-group-item single_notification" ng-repeat="notification in notifications track by $index">
                    <div class="media">
                      <div class="media-left">
                        <i class="mebi-profile text-success" ng-if="notification.type == 0"></i>
                        <i class="mebi-list text-primary" ng-if="notification.type == 1"></i>
                        <i class="mebi-cutlery text-info" ng-if="notification.type == 2"></i>
                        <i class="mebi-paypal text-success" ng-if="notification.type == 3"></i>
                        <i class="mebi-phone-call text-warning" ng-if="notification.type == 4"></i>
                        <i class="mebi-check-1 text-success" ng-if="notification.type == 5"></i>
                        <i class="mebi-check-1 text-success" ng-if="notification.type == 6"></i>
                        <i class="mebi-refund text-danger" ng-if="notification.type == 7"></i>
                        <i class="mebi-refund text-success" ng-if="notification.type == 8"></i>
                        <i class="mebi-calendar-1 text-info" ng-if="notification.type == 9"></i>
                        <i class="mebi-delivery text-danger" ng-if="notification.type == 10"></i>
                        <i class="mebi-check-1 text-success" ng-if="notification.type == 11"></i>
                        <i class="mebi-check-1 text-primary" ng-if="notification.type == 12"></i>
                        <i class="mebi-calendar-1 text-danger" ng-if="notification.type == 13"></i>
                      </div>
                      <div class="media-body">
                        <h5 class="media-heading" ng-if="notification.type == 0">
                          <b>Nuova registrazione</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 1">
                          <b>Nuovo ordine</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 2">
                          <b>Ristorante aggiunto</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 3">
                          <b>Nuova iscrizione a un piano di abbonamento</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 4">
                          <b>Claim</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 5">
                          <b>Richiesta approvata</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 6">
                          <b>Ristorante approvato</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 7">
                          <b>Chiesto rimborso</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 8">
                          <b>Ordine rimborsato</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 9">
                          <b>Nuova prenotazione</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 10">
                          <b>Utente vuole essere un driver</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 11">
                          <b>Prenotazione confermata</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 12">
                          <b>Accettato cambio prenotazione</b>
                        </h5>
                        <h5 class="media-heading" ng-if="notification.type == 13">
                          <b>Data prenotazione cambiata</b>
                        </h5>

                        
                        <small>{{notification.created_at|amDateFormat:'DD/MM/YYYY HH:mm:ss'}}</small>
                        
                        <div class="profile-brief" ng-if="notification.type == 0">
                          <a ng-click="goToAdmin('#/user/{{notification.parameters[0]}}')">
                            {{notification.parameters[1]}}
                          </a> si è iscritto
                        </div>
                        
                        <div class="profile-brief" ng-if="notification.type == 1">
                          <a ng-click="goToAdmin('#/user/'+notification.parameters[1])">
                            {{notification.parameters[2]}}
                          </a> 
                            ha effettuato un 
                            <a ng-click="goToAdmin('#/restaurant/orders/'+notification.parameters[4]+'/'+notification.parameters[0])">
                            ordine
                          </a> di 
                          {{notification.parameters[3]}} € con consegna il 
                          {{notification.parameters[4].date|amDateFormat:'DD.MM.YYYY'}} 
                          alle {{notification.parameters[4].date|amDateFormat:'HH:mm'}}                          
                        </div>

                        <div class="profile-brief" ng-if="notification.type == 2">
                          <a ng-click="goToAdmin('#/user/'+notification.parameters[0])">
                            {{notification.parameters[1]}}
                          </a> 
                          ha aggiunto il ristorante 
                          <a ng-click="goToAdmin('#/restaurant/'+notification.parameters[2])">
                            {{notification.parameters[3]}}
                          </a>
                        </div>

                        <div class="profile-brief" ng-if="notification.type == 3">
                          <a ng-click="goToAdmin('#/user/'+notification.parameters[0])">
                            {{notification.parameters[1]}}
                          </a> ha sottoscritto il piano {{notification.parameters[2]}}
                        </div>

                        <div class="profile-brief" ng-if="notification.type == 4">
                          <a ng-click="goToAdmin('#/user/'+notification.parameters[0])">{{notification.parameters[1]}}
                          </a> 
                          reclama il ristorante 
                          <a ng-click="goToAdmin('#/restaurant/'+notification.parameters[2])">{{notification.parameters[3]}}
                          </a>
                        </div>

                        <div class="profile-brief" ng-if="notification.type == 5">
                          La tua richiesta di gestire 
                          <a ng-click="goToAdmin('#/restaurant/'+notification.parameters[0])">{{notification.parameters[1]}}
                          </a> è stata approvata
                        </div>
                        
                        <div class="profile-brief" ng-if="notification.type == 6">
                          Il ristorante 
                          <a ng-click="goToAdmin('#/restaurant/'+notification.parameters[0])">{{notification.parameters[1]}}
                          </a> è stato approvato
                        </div>
                        
                        <div class="profile-brief" ng-if="notification.type == 7">&Egrave; stato chiesto un rimborso per l'ordine numero 
                          <a ng-click="goToAdmin('#/restaurant/orders/'+notification.parameters[1]+'/'+notification.parameters[0])">
                            #{{notification.parameters[0]}}
                          </a>
                        </div>
                        
                        <div class="profile-brief" ng-if="notification.type == 8">
                          L'ordine numero 
                          #{{notification.parameters[0]}}
                          è stato rimborsato
                        </div>

                        <div class="profile-brief" ng-if="notification.type == 9">
                          <a ng-click="goToAdmin('#/user/'+notification.parameters[0])">
                            {{notification.parameters[1]}}
                          </a>  
                          ha prenotato al
                          <a ng-click="goToAdmin('#/restaurant/'+notification.parameters[2])">
                            {{notification.parameters[3]}}
                          </a> 
                          il {{notification.parameters[4]}} 
                          per {{notification.parameters[5]}} persone 
                        </div>

                        <div class="profile-brief" ng-if="notification.type == 10">
                          <a ng-click="goToAdmin('#/user/'+notification.parameters[0])">
                            {{notification.parameters[1]}}
                          </a> vuole essere un driver
                        </div>

                        <div class="profile-brief" ng-if="notification.type == 11">
                          La tua prenotazione a {{notification.parameters[2]}} ({{notification.parameters[3]}}) è stata confermata
                        </div>

                        <div class="profile-brief" ng-if="notification.type == 12">
                          Il cambio di data della prenotazione di {{notification.parameters[4]}} ({{notification.parameters[3]}}) è stato accettato
                        </div>

                        <div class="profile-brief" ng-if="notification.type == 13">
                          La tua prenotazione a {{notification.parameters[2]}} è stata modificata 
                          ({{notification.parameters[3]}} -> {{notification.parameters[4]}}) 
                        </div>
                        
                      </div>
                    </div>
                  </li>                    
                </ul>

                <a class="btn btn-block btn-default profile-readMore" ng-click="loadMoreNotifications()" ng-if="!notification_hide_loader"
                    role="button">Altre</a>     
                
              </div>
              

              <div ng-if="current_tab == 'profile'" class="tab-pane"  ng-class="{'active':current_tab == 'profile'}" id="profile">
                <div class="row">
                  <div class="col-sm-12">
                    <h4 class="uppercase size-medium inline-block">Cambia le informazioni del tuo profilo</h4>
                    <button ng-disabled="completeProfileForm.$invalid" ng-class="{'loading':updating_user_profile}" ng-click="updateInfo()" class="btn ghost btn-primary btn-sm" style="border: none;">
                      <span ng-if="!updating_user_profile">Conferma le modifiche</span>
                      <div class="spinner spinner-bounce-middle" ng-if="updating_user_profile"></div>
                    </button> 
                    <hr />
                  </div>
                  <div class="col-sm-4">
                    <div class="example-wrap">
                      <form>                        
                        <div class="form-group">
                          <label>Nome</label>
                          <input type="text" ng-model="current_user.user.name" class="form-control" />
                        </div>
                        <div class="form-group">
                          <label>Cognome</label>
                        <input type="text" ng-model="current_user.user.surname" class="form-control" />
                        </div>
                        <div class="form-group">
                          <label>Telefono</label>
                        <input type="text" ng-model="current_user.user.phone" class="form-control" />
                        </div>
                        <div class="form-group">
                          <label>Qualcosa di te</label>
                          <textarea msd-elastic class="form-control" ng-model="current_user.user.description" rows="3" style="line-height: 18px; padding: 12px; resize: none;"></textarea>
                        </div>                           
                      </form>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div>
                      <form id="completeProfileForm">                        
                        <div class="form-group">
                          <label>Indirizzo</label>
                          <input type="text" ng-model="current_user.user.address" class="form-control" required />
                        </div>
                        <div class="form-group">
                          <label>Città</label>
                          <input type="text" ng-model="current_user.user.city" class="form-control" required />
                        </div>
                        <div class="form-group">
                          <label>Provincia (sigla)</label>
                          <input type="text" ng-model="current_user.user.state" class="form-control" required />
                        </div>
                        <div class="form-group">
                          <label>CAP</label>
                          <input type="text" ng-model="current_user.user.zip" class="form-control" required />
                        </div>
                        <div class="form-group relative">
                          <label>Data di nascita</label>
                          <input type="text" id="datetimepicker-day" class="form-control" required />
                        </div> 
                      </form>
                    </div>
                  </div>
                  <div class="col-sm-4">

                  </div>
                </div>
                <hr />
                <div class="row">
                  <div class="col-sm-4">
                    <div class="example-wrap">
                      <p>Modifica la password per accedere al tuo account</p>
                      <form id="changePWDForm" ng-submit="changePWD()">                        
                        <div class="form-group">
                          <label>Vecchia password</label>
                          <input type="password" ng-model="change_pwd.old" class="form-control" required/>
                        </div>
                        <div class="form-group">
                          <label>Nuova password</label>
                          <input type="password" ng-model="change_pwd.new_pwd" class="form-control" required/>
                        </div>
                        <div class="form-group">
                          <label>Conferma la nuova password</label>
                          <input type="password" ng-model="change_pwd.new_confirm" class="form-control" required/>
                        </div>  
                        <div class="form-group">
                          <input type="submit" ng-disabled="changePWDForm.$invalid" class="btn btn-sm ghost btn-block" style="margin-left: 0;" value="Conferma" />
                        </div>                      
                      </form>                      
                    </div>                      
                  </div>
                  <div class="col-sm-4">
                    <div class="example-wrap">                      
                      <p>Modifica l'indirizzo email del tuo account</p>
                      <form id="changeMAILForm" ng-submit="changeMAIL()">                        
                        <div class="form-group">
                          <label>Nuova email</label>
                          <input type="email" ng-model="change_mail.new_mail" class="form-control" required/>
                        </div>
                        <div class="form-group">
                          <input type="submit" ng-disabled="changeMAILForm.$invalid" ng-click="changeMAIL()" class="btn btn-sm ghost btn-block" style="margin-left: 0;" value="Conferma" />
                        </div>                      
                      </form>                      
                    </div>                      
                  </div>
                  <div class="col-sm-4">
                    <div class="example-wrap">
                      <p>Cancella il tuo account (<b>questa azione è irreversibile</b>, tutti i tuoi dati verranno cancellati)</p>
                      <a class="text-danger" ng-click="cancellaAccount()">cancellami</a>
                    </div>                      
                  </div>
                </div>
              </div>


              <div ng-if="current_tab == 'favourite'" class="tab-pane"  ng-class="{'active':current_tab == 'favourite'}" id="favourite">
                <div class="row" ng-if="favourites.length > 0">
                  <div class="col-sm-12">
                    <h4 class="uppercase size-medium inline-block">Ristoranti preferiti</h4>
                    <hr />
                  </div>
                  <div class="col-sm-12">
                    <ul class="list-unstyled simple_list">
                      <li ng-repeat="favourite in favourites track by $index" class="items-list">
                        <img class="float-image-round" ng-src="{{staticURL}}/restaurant/square/{{favourite.image}}.png" alt="" title="" 
                        ng-if="favourite.image && favourite.image != ''" />
                        <span class="uppercase spanimage"><a href="/attivita/{{favourite.slug}}">{{favourite.name}}</a></span>
                        <div class="clearfix"></div>
                      </li>
                    </ul> 
                  </div>
                </div>               
              </div>

              <div ng-if="current_tab == 'reservations'" class="tab-pane"  ng-class="{'active':current_tab == 'reservations'}" id="reservations">
                <div class="row">
                  <div class="col-sm-12">
                    <h4 class="uppercase size-medium inline-block">Le tue prenotazioni</h4>
                    <hr />
                  </div>
                  <div class="col-sm-12">
                    <ul class="list-unstyled simple_list">
                      <li ng-repeat="reservation in reservations track by $index" class="reservation_block">
                        <div class="row">
                          <div class="col-sm-6">
                            <p><b>Ristorante:</b> {{reservation.restaurant_name}}</p>
                            <p><b>Persone:</b> {{reservation.people}}</p>
                            <p><b>Data:</b> {{reservation.date|amDateFormat:'DD/MM/YYYY'}} alle {{reservation.fascia_oraria}}</p>
                          </div>
                          <div class="col-sm-6">
                            <p><b>Status:</b> 
                              <span class="badge-success uppercase" ng-if="reservation.confirmed == 1">confermata</span>
                              <span class="badge-warning uppercase" ng-if="reservation.confirmed == 0 || reservation.confirmed == 3">in attesa di conferma</span>                              
                              <p>
                            <p ng-if="reservation.confirmed == 2">
                              Il proprietario del ristorante ha cambiato la data e/o l'orario della prenotazione, 
                              <a class="badge-primary" ng-click="accettaCambioData(reservation)"><b>clicca qui</b></a> per confermare la modifica
                            </p>
                          </div>
                        </div>
                      </li>
                    </ul> 
                  </div>
                </div>               
              </div>

        <!-- End Panel -->
            </div>
          </div>
        </div>
      </div>
    
  
  