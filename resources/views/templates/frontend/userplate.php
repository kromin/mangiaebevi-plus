<div id="platePage" class="white_page" ng-init="initPage()">
    <div class="container" ng-if="current_plate">
        <div class="text-center" ng-if="current_plate.image && current_plate.image != ''">
            <img ng-src="{{staticURL}}/community/square/{{current_plate.image}}.png" alt="" title="" class="img-circle" />
        </div>
        <div>
            <h1 class="text-center"><a href="/piatto/{{current_plate_parent._id}}/">{{current_plate_parent.name}}</a></h1>
            <p class="text-center">{{current_plate.text}}</p>
            <p class="text-center">
                Caricato da<br/>
                <label><a href="/u/{{current_plate_user._id}}/">{{current_plate_user.name}} {{current_plate_user.surname}}</a></label>
            </p>
        </div>
        <hr />        
        <h4 class="text-center">VOTO</h4>
        <p class="text-center rating_p">{{current_plate.rating}}</p>      
                
    </div>
</div>