<div class="row" ng-controller="DialogRegisterCtrl">
	<div class="col-sm-4 left-col">
		<img src="/img/logo-small.png" alt="" title="MangiaeBevi+ small logo" />
		<h3>Menoo <span class="light">\ registrazione</span></h3>
		<p>{{dialog_custom_text}}</p>
	</div>
	<div class="col-sm-8">
		<p>Compila il form per registrarti</p>
		<div class="form-info">
			<form name="reserveForm" ng-submit="closeThisDialog('registerSubmitted')">
				<div class="form-group">
					<label for="name" class="sr-only">Nome</label>
					<input type="text" ng-model="reserveFormData.name" id="name" class="form-control" placeholder="NOME" required/>
				</div>
				<div class="form-group" ng-if="current_user.loggedStatus == 0">
					<label for="surname" class="sr-only">Cognome</label>
					<input type="text" ng-model="reserveFormData.surname" id="surname" class="form-control" placeholder="COGNOME" required/>
				</div>
				<div class="form-group" ng-if="current_user.loggedStatus == 0">
					<label for="phone" class="sr-only">Telefono</label>
					<input type="text" ng-model="reserveFormData.phone" id="phone" class="form-control" placeholder="TELEFONO" required/>
				</div>
				<div class="form-group" ng-if="current_user.loggedStatus == 0">
					<label for="email" class="sr-only">Email</label>
					<input type="email" ng-model="reserveFormData.email" id="email" class="form-control" placeholder="INDIRIZZO EMAIL" required/>
				</div>
				<div class="form-group" ng-if="current_user.loggedStatus == 0">
					<label for="password" class="sr-only">Password</label>
					<input type="password" ng-model="reserveFormData.password" id="password" class="form-control" placeholder="PASSWORD" required/>
				</div>
				<div class="form-group check" ng-if="current_user.loggedStatus == 0">
					<input type="checkbox" ng-model="reserveFormData.check" id="check" class="form-control" required/>
					<span>Confermi di accettare i termini e condizioni di MangiaeBevi+</span>
				</div>
				<a class="link_btn" ng-click="make_dialog_action('login')">Sei già registrato?</a>
				<div class="form-group">
					<input type="submit" ng-disabled="reserveForm.$invalid" class="btn btn-block btn-primary login-btn" value="Registrati" />
				</div>
			</form>
		</div>
	</div>
</div>