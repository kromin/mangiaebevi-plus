<h3 class="uppercase spacing size-big pad-big-v text-center">Riepilogo prenotazione</h3>
<div class="row reservation_block">
	<p class="text-center">
  		<img 
      	ng-if="returnUser(current_reservation.user).avatar" 
      	class="img-circle" 
      	ng-src="{{staticURL}}/users/square/{{returnUser(current_reservation.user).avatar}}.png"
      	/>
    </p>
    <p class="text-center">
    	<label>{{returnUser(current_reservation.user).name}} {{returnUser(current_reservation.user).surname}}</label>
    </p>
    <p class="text-center">
    	{{current_reservation.tel}} - {{current_reservation.email}}
    </p>
    <div class="row icons_block">
    	
		<div class="col-xs-6">
			<div class="col-xs-4"><i class="mebi-avatar"></i></div>
			<div class="col-xs-8 num">{{current_reservation.people}}</div>
		</div>
		<div class="col-xs-6">
			<div class="col-xs-4"><i class="mebi-time"></i></div>
			<div class="col-xs-8 num">{{current_reservation.date|amDateFormat:'DD/MM/YYYY'}}<br />{{current_reservation.fascia_oraria}}</div>
		</div>
														
	</div>		
	  
  <div class="clearfix"></div>
    <p class="text-center">
	    <span class="fasce_span success" ng-if="current_reservation.confirmed == 1">confermato</span>
	    <span class="fasce_span danger" ng-if="current_reservation.confirmed == 0">non confermato</span>
	    <span class="fasce_span warning" ng-if="current_reservation.confirmed == 2">in attesa della conferma del cambio di orario</span>
	    <span class="fasce_span primary" ng-if="current_reservation.confirmed == 3">approvato il cambio di orario</span>
    </p>

    <div ng-if="current_reservation.confirmed == 0 && date_change == 0 && avaible_tables > 0" class="text-center">
      <p>Posti disponibili: {{avaible_tables}} <span ng-if="avaible_tables < current_reservation.people">puoi comunque confermare la prenotazione</span></p>
      <label class="text-center">Scegli la sala</label>
      <ul class="list-unstyled">
        <li ng-repeat="sala in restaurant.struttura.sale track by $index">
          <span 
          ng-class="{'text-primary' : confirmation_selected_sala == $index}"
          ng-click="setConfirmationSala($index)">
            {{sala.name}}
          </span>
        </li>
      </ul>
      <button class="outline_btn bprimary profile-readMore" ng-click="confirmReservation(current_reservation._id)">
          Conferma la prenotazione
      </button>
    </div>

    <div ng-if="current_reservation.confirmed == 0 && date_change == 0 && avaible_tables <= 0" class="text-center">
      <label class="text-center">I posti sono esauriti in questo orario</label>      
    </div>


    <div ng-if="current_reservation.confirmed == 1 && current_reservation.assigned_foodcoin != 1" class="text-center">
    	<button class="outline_btn bwarning profile-readMore" ng-click="assignFoodcoin(current_reservation._id)">
	      	Conferma la presenza
	    </button>
      <p>
        <a ng-click="removeFoodcoin(current_reservation._id)" class="text-danger">clicca qui se non si è presentato</a>
      </p>
    </div>

    <div ng-if="current_reservation.confirmed == 1 && current_reservation.removed_foodcoin == 1" class="text-center">
      <p class="text-danger">Non si è presentato</p>
    </div>

    <div ng-if="current_reservation.confirmed == 0 && change_orario.avaible_fasce.length > 0">
      
      <p ng-if="date_change == 0" class="text-center">Vuoi modificare l'orario? <a ng-click="changeDate(1)">clicca qui</a></p>
      <div ng-if="date_change == 1" class="text-center">
        <div class="form-group margin-big-v">
          <label>Nuovo orario</label>
          <select ng-model="change_orario.ore" class="form-control">
          	<option ng-repeat="fascia in change_orario.avaible_fasce" value="{{fascia}}">{{fascia}}</option>
          </select>           
        </div>
        <p class="text-center"><a ng-click="changeDate(0)">Annulla</a></p>
      </div>
      <div class="row"></div>
      <div class="text-center">
      	<button ng-if="date_change == 1" class="outline_btn bprimary profile-readMore" ng-click="confirm_date_change()">
	      	Conferma la modifica
	      </button>
	   </div>
    </div>

  <div class="clearfix"></div>
</div>