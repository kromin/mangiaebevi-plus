<div id="platePage" class="white_page" ng-init="initPage()">
    <div class="container" ng-if="current_plate">
        <div class="text-center" ng-if="current_plate.image && current_plate.image != ''">
            <img ng-src="{{staticURL}}/menu/square/{{current_plate.image}}.png" alt="" title="" class="img-circle" />
        </div>
        <div>
            <h1 class="text-center">{{current_plate.name}}</h1>
            <p class="text-center">{{current_plate.description}}</p>
            <p class="text-danger text-center" ng-if="current_plate.deleted && current_plate.deleted == 1">Questo piatto non è più nel menu</p>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-6">
                <div>
                    <p class="text-center"><label>Ingredienti</label><br />
                    <span class="fasce_span info" ng-repeat="ing in current_plate.ingredienti">{{ing}}</span></p>
                    <p class="text-center" ng-if="current_plate.ingredienti.length == 0">Non ci sono ingredienti</p>
                </div>
                <div>
                    <p class="text-center"><label>Allergeni</label><br />
                    <span class="fasce_span warning" ng-repeat="all in current_plate.allergeni">{{all}}</span></p>
                    <p class="text-center" ng-if="current_plate.allergeni.length == 0">Non contiene allergeni</p>
                </div>
            </div>
            <div class="col-sm-6" ng-init="loadRestaurant()">
                <p class="text-center"><label>Ristorante</label></p>
                <div class="col-lg-2 col-md-2 col-xs-3">
                    <img class="img-circle" 
                    ng-src="{{staticURL}}/restaurant/square/{{current_restaurant.image}}.png" alt="" title="" 
                    ng-if="current_restaurant.image && current_restaurant.image != ''" />
                </div>
                <div class="col-lg-10 col-md-10 col-xs-9">
                    <h3><a href="/attivita/{{current_restaurant.slug}}/">{{current_restaurant.name}}</a></h3>
                    <p>{{current_restaurant.address}}</p>                                            
                </div>   
            </div>
        </div>
        <div class="recommend text-center">
            <button class="btn ghost" ng-click="recommendPlate()" 
                    role="button">Vuoi raccomandare questo piatto?</button>  
        </div>
        <div class="plate_price text-center plate_single_price">
            €{{current_plate.price}}
        </div>
        <div class="social_elements">
            <div class="minimal_tabs">
                <ul class="list-unstyled">
                    <li ng-class="{'active' : !plate_tab || plate_tab == 0}"><a ng-click="setPlateTab(0)">Raccomandazioni</a></li>
                    <li ng-class="{'active' : plate_tab == 1}"><a ng-click="setPlateTab(1)">Dalla community</a></li>                             
                </ul>
            </div>
            <div class="recommendation" ng-if="plate_tab == 0 || !plate_tab">
                <div class="col-sm-6 col-sm-push-3">
                    <ul class="list-unstyled imaging_list" ng-init="loadPlateRecommendations()">
                        <li ng-repeat="recommendation in plate_rec track by $index">
                            <div class="clearfix"></div>
                            <div class="col-lg-2 col-md-2 col-xs-3">
                                <img class="img-circle" 
                                ng-src="{{staticURL}}/users/square/{{recommendation['user_obj'].avatar}}.png" alt="" title="" 
                                ng-if="recommendation['user_obj'].avatar && recommendation['user_obj'].avatar != ''" />
                            </div>
                            <div class="col-lg-10 col-md-10 col-xs-9">
                                <h3><a href="/u/{{recommendation['user_obj']._id}}/">{{recommendation['user_obj'].name}} {{recommendation['user_obj'].surname}}</a></h3>
                                <p>{{recommendation.text}}</p>                                            
                            </div>                                  
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                    <p class="text-center">
                        <button class="btn ghost" ng-click="loadRecommendation()" 
                        ng-if="!stop_load_rec_pagination"
                            role="button">Altre</button>   
                    </p>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="community_plate" ng-if="plate_tab == 1">
                <div class="text-center">
                    <button class="btn ghost" ng-click="addImage()" 
                    role="button">Carica una immagine del piatto</button>   
                </div>

                <div class="col-sm-6 col-sm-push-3">
                    <ul class="list-unstyled imaging_list" ng-init="loadPlateImages()">
                        <li ng-repeat="image in plate_images track by $index">
                            <div class="clearfix"></div>
                            <div class="col-lg-12">
                                <img class="full" 
                                ng-src="{{staticURL}}/community/medium/{{image.image}}.png" alt="" title="" />
                                <img class="user_image_minus_top" 
                                ng-src="{{staticURL}}/users/medium/{{image['user_obj'].avatar}}.png" alt="" title="" ng-if="image['user_obj'].avatar && image['user_obj'].avatar != ''" />
                            </div>

                            <div class="user_plate_image_info">
                                <h3>
                                    <a href="/u/{{image['user_obj']._id}}/">{{image['user_obj'].name}} {{image['user_obj'].surname}}</a>
                                </h3>
                                    <p>{{image.text}}</p>
                                    <p class="text-center rating_p">{{image.rating}}</p>                                            
                            </div>                                  
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <p class="text-center">
                    <button class="btn ghost" ng-click="loadPlateImagesN()" 
                    ng-if="!stop_load_images_pagination"
                        role="button">Altre</button>   
                </p>
            </div>
        </div>
    </div>
</div>
<div class="menu_add_by_step custom_scrollbar" ng-class="{'open':editing_menu}">
    <i class="mebi-close menu_step_close" ng-click="closeEditing()"></i>
    
    <div class="content_menu_adding text-center">
        <!-- add plate -->
        <h3 class="uppercase">
            Aggiungi un'immagine per la community
        </h3>

        <p ng-if="!add_plate.image">Clicca sull'icona per aggiungere una immagine</p>
        <div class="add_plate_image_container">
            <input class="upload-input" type="file" ngf-select 
                    ng-model="add_plate_image.image" 
                    name="add_plate_image" 
                    ng-change="uploadPlateImage()" title="Immagine" />
            <i class="mebi-technology-1 add_plate_icon" ng-if="!add_plate_obj.image"></i>
            <img ng-src="{{staticURL}}/community/small/{{add_plate_obj.image}}.png" alt="" title="" ng-if="add_plate_obj.image" />
        </div>
                        
                   
        <p>Commento</p>
        <textarea msd-elastic ng-model="add_plate_obj.text"></textarea>  
        <!-- prezzo -->
        <p>Voto</p>
        <div class="plate_rating">
            <i class="mebi-minus" ng-click="changeRate('less')"></i>
            <span>{{add_plate_obj.rating}}</span>
            <i class="mebi-add-circular-button" ng-click="changeRate('add')"></i>
        </div>
        <p><a class="btn ghost" ng-click="confirmAddPlateImage()">
            Conferma</a>
        </p>
    </div>
</div>