<div id="topbar" ng-if="!remove_top_bar" ng-class="{'blue':blue, 'col-md-9':search }" scroll>
    <div class="" ng-class="{'container-fluid':search, 'container':!search}">
        <a href="/"><img class="logo" ng-src="/img/logo.png" alt="MEB" title="MangiaeBevi" /></a>
        <ul class="topbar-navigation pull-right hidden-sm hidden-xs" ng-if="current_user.loggedStatus == 0">
            <!--<li><a href="/">news</a></li>-->
            <li><a href="http://blog.mangiaebevi.it">Blog</a></li>
            <li><a ng-click="openLogin(1)">Accedi</a></li>
            <li><a ng-click="openLogin(3)">Registrati</a></li>
            <li><a class="btn btn-primary" href="/aggiungi-ristorante/">aggiungi un ristorante</a></li>
        </ul>
        <ul class="topbar-navigation pull-right hidden-xs hidden-sm" id="dropdown-user-menu" ng-if="current_user.loggedStatus == 1">
            <!--<li><a href="/">news</a></li>-->
            <li>
                <img ng-click="openDropUserMenu()" ng-if="current_user.user.avatar && current_user.user.avatar != ''" ng-src="{{staticURL}}/users/small/{{current_user.user.avatar}}.png" alt="" title="" />
                <div back-img img="/img/logo-small.png" ng-click="openDropUserMenu()" ng-if="!current_user.user.avatar || current_user.user.avatar == ''" class="user-image-container-topbar"></div>
                <ul ng-if="drop_open">
                    <li>{{current_user.user.name}} {{current_user.user.surname}}</li>
                    <li><a href="http://blog.mangiaebevi.it">Blog</a></li>
                    <li ng-if="current_user.user.role > 2"><a ng-click="goToAdministration()">amministrazione</a></li>
                    <li><a href="/u/{{current_user.user._id}}/">profilo</a></li>
                    <li ng-if="current_user.user.role > 1"><a href="/aggiungi-ristorante/">aggiungi un ristorante</a></li>
                    <li><a ng-click="logout()">esci</a></li>
                </ul>
            </li>
        </ul>
        <div id="nav-icon4" class="visible-xs visible-sm hidden-md hidden-lg" ng-click="openMenuMobile()" ng-class="{'open':openmobile}">
          <span></span>
          <span></span>
          <span></span>
        </div>
    </div>
</div>
<div id="mobilemenu" class="visible-xs visible-sm hidden-md hidden-lg" ng-class="{'open':openmobile}">
<ul class="list-unstyled" ng-if="current_user.loggedStatus == 1">
    <li><a href="http://blog.mangiaebevi.it">Blog</a></li>
    <li ng-if="current_user.user.role > 2"><a class="btn btn-primary" ng-click="goToAdministration()">amministrazione</a></li>
    <li><a class="btn btn-primary" href="/u/{{current_user.user._id}}/">profilo</a></li>
    <li ng-if="current_user.user.role > 1"><a class="btn btn-primary" href="/aggiungi-ristorante/">aggiungi un ristorante</a></li>
    <li><a class="btn btn-primary" ng-click="logout()">esci</a></li>
</ul>
<ul class="list-unstyled" ng-if="current_user.loggedStatus == 0">
    <li><a href="http://blog.mangiaebevi.it">Blog</a></li>
    <li><a class="btn btn-primary" ng-click="openLogin(1)">Accedi</a></li>
    <li><a class="btn btn-primary" ng-click="openLogin(3)">Registrati</a></li>
    <li><a class="btn btn-primary" href="/aggiungi-ristorante/">aggiungi un ristorante</a></li>
</ul>
</div>