<h3>Aggiungi un tavolo</h3>
<form>
    <div class="row">
        <div class="form-group col-sm-12">
            <label class="text-center">Numero del tavolo</label>
            <input type="number" class="form-control number_table_form" ng-model="add_struttura_children.tavolo" min="1" />
        </div>
        <div class="form-group col-sm-12 pax_table_group">
            <label>Per quante persone</label>
            <div class="col-xs-4">
                <i class="mebi-minus icon" ng-click="removePax()"></i>
            </div>
            <div class="col-xs-4">
                <span class="form-control pax_table_input">
                    {{add_struttura_children.posti}}
                </span>
            </div>
            <div class="col-xs-4">
                <i class="mebi-add-circular-button icon" ng-click="addPax()"></i>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-sm-12">
            <button ng-click="add_tavolo(current_sala)" class="btn btn-primary ghost">
                <i class="icon wb-plus"></i> Conferma
            </button>
        </div>
    </div>
</form>