<div class="orari_dialog">
	<h3>Orari</h3>	
	<div class="col-sm-6" ng-repeat="day in restaurant_hours track by $index">
		<dl class="dl-horizontal">
			<dt>{{day.name}}</dt>
			<dd>
				<p ng-repeat="fascia in day.fasce">
					<span>{{fascia.min}}</span> - <span>{{fascia.max}}</span>
				</p>
			</dd>
		</dl>
	</div>
	<div class="clearfix"></div>	
</div>