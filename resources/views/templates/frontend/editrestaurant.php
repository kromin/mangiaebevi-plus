<div id="edit-restaurant-page" class="editing_profile_restaurant" ng-init="loadRestaurant()">
	<div class="save_btn" ng-if="current_tab == 1">
		<a ng-click="updateInfo()" class="btn btn-primary btn-sm ghost">Salva le modifiche</a>
	</div>
	<div class="save_btn" ng-if="current_tab == 3">
		<a ng-click="updateStructure()" class="btn btn-primary btn-sm ghost">Salva le modifiche</a>
	</div>
	<div class="save_btn" ng-if="current_tab == 2">
		<a ng-click="updateMenu()" class="btn btn-primary btn-sm ghost">Salva le modifiche</a>
	</div>

	<div class="main_image" back-img img="{{staticURL}}/restaurant/big/{{restaurant.image}}.png" ng-if="restaurant.image && restaurant.image != ''">
		<div class="overlay"></div></div>
	<div class="main_image" back-img img="/img/home/home_1.jpg" ng-if="restaurant.image == '' || !restaurant.image"><div class="overlay"></div></div>
	
	<!-- edit restaurant -->
	<div>
		<div class="main_container">
			<div class="main_section_menu">
				<div class="container">
					<div class="col-sm-3 col-xs-6">
						<a ng-click="changeTab(1)" ng-class="{'current':current_tab == 1}"><i class="fa fa-info"></i> <span>Info</span></a>
					</div>
					<div class="col-sm-3 col-xs-6">
						<a ng-click="changeTab(2)" ng-class="{'current':current_tab == 2}"><i class="mebi-list"></i> <span>Menu</span></a>
					</div>
					<div class="col-sm-3 col-xs-6">
						<a ng-click="changeTab(3)" ng-class="{'current':current_tab == 3}"><i class="mebi-romantic-date"></i> <span>Sale</span></a>
					</div>
					<div class="col-sm-3 col-xs-6">
						<a ng-click="changeTab(4)" ng-class="{'current':current_tab == 4}"><i class="mebi-calendar-1"></i> <span>Prenotazioni</span></a>
					</div>
				</div>
			</div>

			<div class="restaurantTitle" ng-if="current_tab == 1">
				<div class="container">
					<h1>
						<span ng-show="open_editing != 1">{{restaurant.name}} <i class="fa fa-pencil" ng-click="openEditing(1)" ng-if="current_tab == 1"></i></span>
						<span class="edit_name" ng-show="open_editing == 1" ng-if="current_tab == 1">
							<input class="activated_input" ng-model="restaurant.name" autosize/>
							<i class="mebi-android-done" ng-click="openEditing(0)"></i>
						</span> 
						
					</h1>
					<span class="restaurant_top_infos">
						<small ng-if="restaurant.tipoLocale.length > 0" ng-click="scrollTo('tipo_locale_block')">
							{{tipiLocali[restaurant.tipoLocale[0]].name}} <i class="fa fa-refresh"></i> 
							|
						</small>&nbsp;&nbsp;&nbsp;
						<small ng-click="scrollTo('price_set')"><span ng-repeat="euro in price_euro_add track by $index">{{euro}}</span> <i class="fa fa-refresh"></i></small>
					</span>				
				</div>
			</div>
		</div>

	<!-- edit restaurant -->
	<div ng-if="current_tab == 1">
		<div class="white_container">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 edit_fields_title">
						Modifica le informazioni principali del tuo ristorante
					</div>
				<!-- info principali del ristorante -->
					<div class="col-sm-4">

						<i class="fa fa-map-marker"></i> 
							<input type="text" name="address" class="activated_input full" ng-model="restaurant.address"
							ng-autocomplete 
							details="details"
							class="form-control" 
							placeholder="Indirizzo" style="width: 280px" />
							<br />
						<i class="fa fa-envelope-o"></i> <input class="activated_input" type="email" ng-model="restaurant.email" autosize/>

						
					</div>
					<div class="col-sm-4">

						<i class="fa fa-globe"></i> <input class="activated_input" type="text" ng-model="restaurant.web" autosize /><br />
						
						<i class="fa fa-phone"></i> 
							<input class="activated_input" type="text" ng-model="add_restaurant_phone" placeholder="Aggiungi un numero di telefono" 
							style="width: 238px; margin-right: -32px;" />
							<i class="mebi-android-add" ng-click="addPhoneNumber(add_restaurant_phone)"></i>
						<br />
						<ul class="list-unstyled phones_list">
							<li ng-repeat="phone in restaurant.phone track by $index"> {{phone}} <i class="mebi-close" ng-click="removeNumber($index)"></i></li>
						</ul>

					</div>
					<div class="col-sm-4">
						<div class="restaurant-social">
	                        <a target="_blank" ng-if="restaurant.social[0].facebook && restaurant.social[0].facebook != ''" href="{{restaurant.social[0].facebook}}" class="social-page-link">
	                            <i class="fa fa-facebook fb"></i>
	                        </a>
	                        <a target="_blank" ng-if="restaurant.social[1].twitter && restaurant.social[1].twitter != ''" href="{{restaurant.social[1].twitter}}" class="social-page-link">
	                            <i class="fa fa-twitter tw"></i>
	                        </a>
	                        <a target="_blank" ng-if="restaurant.social[2].pinterest && restaurant.social[2].pinterest != ''" href="{{restaurant.social[2].pinterest}}" class="social-page-link">
	                            <i class="fa fa-pinterest pi"></i>
	                        </a>
	                        <a target="_blank" ng-if="restaurant.social[3].instagram && restaurant.social[3].instagram && restaurant.social[3].instagram != ''" href="{{restaurant.social[3].instagram}}" class="social-page-link">
	                            <i class="fa fa-instagram in"></i>
	                        </a>
	                        <a target="_blank" ng-if="restaurant.social[4].googlePlus && restaurant.social[4].googlePlus != ''" href="{{restaurant.social[4].googlePlus}}" class="social-page-link">
	                            <i class="fa fa-google-plus gp"></i>
	                        </a>
	                        <a target="_blank" ng-if="restaurant.social[5].linkedin && restaurant.social[5].linkedin != ''" href="{{restaurant.social[5].linkedin}}" class="social-page-link">
	                            <i class="fa fa-linkedin li"></i>
	                        </a>
	                        <a target="_blank" ng-if="restaurant.social[6].youtube && restaurant.social[6].youtube != ''" href="{{restaurant.social[6].youtube}}" class="social-page-link">
	                            <i class="fa fa-youtube yt"></i>
	                        </a>
	                        <a target="_blank" ng-if="restaurant.social[7].tripadvisor && restaurant.social[7].tripadvisor != ''" href="{{restaurant.social[7].tripadvisor}}" class="social-page-link">
	                            <i class="fa fa-tripadvisor tr"></i>
	                        </a>
	                    </div>
	                    <a ng-click="editSocial()">Modifica / Elimina link alle pagine social</a>
					</div>
				</div>
			</div>
		</div>

		<div class="grey_container">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<!-- staff -->
						<div class="col-sm-2">
	                        <p class="big_text left_descriptor"><b>Staff:</b></p>
	                    </div>
	                    <div class="col-sm-10" id="staff_memebers_block">
	                    	<div class="row">
	                            <div class="col-md-3 col-sm-6">
	                                <ul class="list-unstyled">
	                                    <li class="padded"><b>CHEF</b></li>
	                                    <li class="padded" ng-repeat="chef in restaurant.staff.chef track by $index">
	                                    	<i class="icon wb-close-mini" ng-click="remove_staff_member('chef',$index)"></i>
	                                    	{{chef}}</li>
	                                    <li>
	                                    	<input class="activated_input" type="text" ng-model="add_staff_member.chef_name" 
	                                    	placeholder="Nome" 
											style="width: 80%; margin-right: -5px;" />
											<i class="mebi-android-add" ng-click="add_staff_member_to_restaurant ('chef',add_staff_member.chef_name)"></i>
	                                    </li>
	                                </ul>
	                            </div>
	                            <div class="col-md-3 col-sm-6">
	                                <ul class="list-unstyled">
	                                    <li class="padded"><b>SOMMELIER</b></li>
	                                    <li class="padded" ng-repeat="sommelier in restaurant.staff.sommelier track by $index">
	                                    	<i class="icon wb-close-mini" ng-click="remove_staff_member('sommelier',$index)"></i>
	                                    	{{sommelier}}</li>
	                                    <li>
	                                    	<input class="activated_input" type="text" ng-model="add_staff_member.sommelier_name" 
	                                    	placeholder="Nome" 
											style="width: 80%; margin-right: -5px;" />
											<i class="mebi-android-add" ng-click="add_staff_member_to_restaurant ('sommelier',add_staff_member.sommelier_name)"></i>
	                                    </li>
	                                </ul>
	                            </div>
	                            <div class="col-md-3 col-sm-6">
	                                <ul class="list-unstyled">
	                                    <li class="padded"><b>BARMAN</b></li>
	                                    <li class="padded" ng-repeat="barman in restaurant.staff.barman track by $index">
	                                    	<i class="icon wb-close-mini" ng-click="remove_staff_member('barman',$index)"></i>
	                                    	{{barman}}</li>
	                                    <li>
	                                    	<input class="activated_input" type="text" ng-model="add_staff_member.barman_name" 
	                                    	placeholder="Nome" 
											style="width: 80%; margin-right: -5px;" />
											<i class="mebi-android-add" ng-click="add_staff_member_to_restaurant ('barman',add_staff_member.barman_name)"></i>
	                                    </li>
	                                </ul>
	                            </div>
	                            <div class="col-md-3 col-sm-6">
	                                <ul class="list-unstyled">
	                                    <li class="padded"><b>MAITRE</b></li>
	                                    <li class="padded" ng-repeat="maitre in restaurant.staff.maitre track by $index">
	                                    	<i class="icon wb-close-mini" ng-click="remove_staff_member('maitre',$index)"></i>
	                                    	{{maitre}}
	                                    </li>
	                                    <li>
	                                    	<input class="activated_input" type="text" ng-model="add_staff_member.maitre_name" 
	                                    	placeholder="Nome" 
											style="width: 80%; margin-right: -5px;" />
											<i class="mebi-android-add" ng-click="add_staff_member_to_restaurant ('maitre',add_staff_member.maitre_name)"></i>
	                                    </li>
	                                </ul>
	                            </div>
	                        </div>
	                                     
	                    </div>
	                    <div class="clearfix"></div>
	                    <hr />
						<div class="col-sm-2" id="price_set">
							<b>Prezzi:</b>
						</div>
						<div class="col-sm-9">
							<div class="form-group">
								<label for="prezzo_max">Prezzo minimo</label>
						        <div class="range-slider">
						            <input class="range-slider__range" type="range" ng-change="priceChange()" ng-model="restaurant.priceMin" min="0" max="495" step="5" id="prezzo_min">
						            <span class="range-slider__value">{{restaurant.priceMin}}</span>
						        </div>
						    </div>
						    <div class="form-group">
								<label for="prezzo_max">Prezzo massimo</label>
						        <div class="range-slider">
						            <input class="range-slider__range" type="range" ng-change="priceChange()" ng-model="restaurant.priceMax" min="0" max="500" step="5" id="prezzo_max">
						            <span class="range-slider__value">{{restaurant.priceMax}}</span>
						        </div>
						    </div>
						</div>
						<div class="clearfix"></div>
						<hr />
						<div class="col-sm-2" id="price_set">
							<b>Descrizione:</b>
						</div>
						<div class="col-sm-9">
							<div class="form-group">   
								<text-angular ta-toolbar="taOptions" ng-model="restaurant.description"></text-angular>                       
	                        </div> 
						</div>
						<div class="clearfix"></div>
						<hr />
						
						
	                    <div class="col-sm-2">
	                        <p class="big_text left_descriptor"><b>Tipologia di locale:</b></p>
	                        <a ng-click="openEditing(2)" class="text-primary" ng-if="open_editing != 2">MODIFICA</a>            
	                        <a ng-click="openEditing(0)" class="text-success" ng-if="open_editing == 2">CONFERMA</a>     
	                    </div>
	                    <div class="col-sm-9" id="tipo_locale_block">
	                    	<div class="notEdit" ng-if="open_editing != 2">
		                        <div class="primary"
		                        drop="onPrimaryDrop($data, 'tipo_locale', $event)"  
				                effect-allowed="copy" 
				                drop-effect="copy" 
				                drop-accept="'json/el_obj'"
				                drag-over-class="drag_over"
		                        >
		                        	{{tipiLocali[restaurant.tipoLocale[0]].name}}
		                        </div>
								<div class="clearfix"></div>
								<ul class="list-unstyled">
		                            <li 
		                            class="col-md-3" 
		                            draggable="true" 
					                effect-allowed="copy"
					                draggable-type="el_obj"  
					                draggable-data="{index: $index}" 
					                style="cursor: move"
		                            ng-if="$index > 0"
		                            ng-repeat="tipo_locale in restaurant.tipoLocale track by $index">
		                            	{{tipiLocali[tipo_locale].name}}
		                            </li>
		                        </ul>  
	                        </div>
	                        <div class="edit" ng-if="open_editing == 2">
	                        	
	                        	<ul class="list-unstyled">
		                            <li class="col-md-3" ng-repeat="tipo_locale in tipiLocali track by $index" 
		                            ng-class="{'text-success' : hasTipoLocale(tipo_locale._id)}" 
		                            ng-click="addRemoveTipoLocale(tipo_locale._id)">{{tipo_locale.name}}</li>
		                        </ul> 
		                        <div class="clearfix"></div>
	                        </div>    
	                        <div class="clearfix"></div>
	                                     
	                    </div>
	                    <div class="clearfix"></div>
						<hr />

						<!-- tipo di cucina -->
						<div class="col-sm-2">
	                        <p class="big_text left_descriptor"><b>Cucina:</b></p>
	                        <a ng-click="openEditing(3)" class="text-primary" ng-if="open_editing != 3">MODIFICA</a>            
	                        <a ng-click="openEditing(0)" class="text-success" ng-if="open_editing == 3">CONFERMA</a>   
	                    </div>
	                    <div class="col-sm-9">
	                    	<div class="notEdit" ng-if="open_editing != 3">
	                    		<div class="row">
		                    		<div class="col-sm-6">
		                    			<h3>Tipologia</h3>
				                        <div class="primary"
				                        drop="onPrimaryDrop($data, 'tipo_cucina', $event)"  
						                effect-allowed="copy" 
						                drop-effect="copy" 
						                drop-accept="'json/el_obj'"
						                drag-over-class="drag_over"
				                        >
				                        	{{tipiCucine[restaurant.tipoCucina[0]].name}}
				                        </div>
										<div class="clearfix"></div>
				                        <ul class="list-unstyled">
				                            <li class="col-md-12" 
				                            draggable="true" 
							                effect-allowed="copy"
							                draggable-type="el_obj"  
							                draggable-data="{index: $index}" 
							                style="cursor: move"
				                            ng-repeat="tipo_cucina in restaurant.tipoCucina track by $index"
				                            ng-if="$index > 0"
				                            >{{tipiCucine[tipo_cucina].name}}</li>
				                        </ul>   
				                        
				                        
				                    </div> 
			                        <div class="col-sm-6">
			                        	<h3>Regione</h3>
				                        <div class="primary"
				                        drop="onPrimaryDrop($data, 'regione_cucina', $event)"  
						                effect-allowed="copy" 
						                drop-effect="copy" 
						                drop-accept="'json/el_obj'"
						                drag-over-class="drag_over"
						                >
				                        	{{regioniCucine[restaurant.regioneCucina[0]].name}}
				                        </div>
		 								<div class="clearfix"></div>
				                        <ul class="list-unstyled">
				                            <li 
				                            draggable="true" 
							                effect-allowed="copy"
							                draggable-type="el_obj"  
							                draggable-data="{index: $index}" 
							                style="cursor: move"
				                            class="col-md-12" ng-repeat="regione_cucina in restaurant.regioneCucina track by $index"
				                            ng-if="$index > 0"
				                            >{{regioniCucine[regione_cucina].name}}</li>
				                        </ul>
				                       
				                        
			                        </div>  
		                        </div>      
		                    </div>
		                    <div class="edit" ng-if="open_editing == 3">
		                    	<div class="col-sm-6">
		                    		<h3>Tipologia</h3>
			                    	<ul class="list-unstyled">
			                            <li class="col-md-6" ng-repeat="tipo_cucina in tipiCucine track by $index" 
			                            ng-class="{'text-success' : hasTipoCucina(tipo_cucina._id)}" 
			                            ng-click="addRemoveTipoCucina(tipo_cucina._id)">{{tipo_cucina.name}}</li>
			                        </ul> 
			                    </div>
			                    <div class="col-sm-6">
			                    	<h3>Regione</h3>
			                        <ul class="list-unstyled">
			                            <li class="col-md-6" ng-repeat="regione_cucina in regioniCucine track by $index" 
			                            ng-class="{'text-success' : hasRegioneCucina(regione_cucina._id)}" 
			                            ng-click="addRemoveRegioneCucina(regione_cucina._id)">{{regione_cucina.name}}</li>
			                        </ul>
			                    </div>
		                        <div class="clearfix"></div>
	                        </div>
	                        <div class="clearfix"></div>
	                                      
	                    </div>
	                    <div class="clearfix"></div>
						<hr />


						<div class="col-sm-2">
	                        <p class="big_text left_descriptor"><b>Servizi:</b></p>
	                        <a ng-click="openEditing(4)" class="text-primary" ng-if="open_editing != 4">MODIFICA</a>            
	                        <a ng-click="openEditing(0)" class="text-success" ng-if="open_editing == 4">CONFERMA</a>        
	                    </div>
	                    <div class="col-sm-9">
	                    	<div class="notEdit" ng-if="open_editing != 4">
		                    	<div class="col-lg-3 col-md-3 col-sm-6 single_service centered_icon_container" ng-repeat="service in restaurant.services">
		                            <i class="{{services[service].icon}} big"></i>
		                            <span>{{services[service].name}}</span>
		                        </div>   
		                    </div> 
	                        <div class="edit" ng-if="open_editing == 4">  
	                        	<div class="col-lg-3 col-md-3 col-sm-6 single_service centered_icon_container" 
	                        	ng-repeat="service in services"
	                        	ng-click="addRemoveService(service._id)"
	                        	>
		                            <i class="{{service.icon}} big" ng-class="{'text-success' : hasService(service._id)}"></i>
		                            <span ng-class="{'text-success' : hasService(service._id)}">{{service.name}}</span>
		                        </div>                       	
	                        	
		                        <div class="clearfix"></div>
	                        </div>    

	                        <div class="clearfix"></div>
	                         
	                    </div>
	                    <div class="clearfix"></div>
						<hr />

						<div class="col-sm-2">
	                        <p class="big_text left_descriptor"><b>Carte accettate:</b></p>
	                        <a ng-click="openEditing(5)" class="text-primary " ng-if="open_editing != 5">MODIFICA</a>            
	                        <a ng-click="openEditing(0)" class="text-success " ng-if="open_editing == 5">CONFERMA</a> 
	                    </div>
	                    <div class="col-sm-9">
	                    	<div class="notEdit" ng-if="open_editing != 5">
		                        <ul class="list-unstyled">
		                            <li class="col-md-3" ng-repeat="carta in restaurant.carte">{{creditCard[carta].name}}</li>
		                        </ul> 
		                    </div>
	                        <div class="edit" ng-if="open_editing == 5"> 
	                        	                      	
	                        	<ul class="list-unstyled">
		                            <li class="col-md-3" ng-repeat="card in creditCard track by $index" 
		                            ng-class="{'text-success' : hasCard(card._id)}" 
		                            ng-click="addRemoveCard(card._id)">{{card.name}}</li>
		                        </ul> 
		                        <div class="clearfix"></div>
	                        </div>  

	                        <div class="clearfix"></div>
	                         


	                    </div>
	                    <div class="clearfix"></div>
						<hr />

					</div>
					<div class="col-sm-2"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>	

		<div class="white_container">
			<div class="container">

				<div class="orari_ristorante">
					<div class="col-sm-12">
						<h3 class="uppercase">Orari di apertura</h3>
					</div>
					<div class="col-sm-3">
	                    <div class="form-group">
	                        <label>Giorno</label>
	                        <select type="text" ng-model="add_fascia_oraria.day" class="form-control">
	                            <option ng-repeat="day in week_days track by $index" value="{{$index}}">{{day}}</option>
	                        </select>
	                    </div>
	                </div>
	                <div class="col-sm-3">
	                    <div class="form-group">
	                        <div class="row">
	                        	<div class="col-xs-6">
	                        		<label>Dalle</label>
									<select ng-model="fascia_corrente.from" class="form-control">
		                            	<option value="{{fascia_disponibile}}" ng-repeat="fascia_disponibile in avaibleFasce track by $index">{{fascia_disponibile}}</option>
		                        	</select>
	                        	</div>
	                        	<div class="col-xs-6">
	                        		<label>Alle</label>
	                        		<select ng-model="fascia_corrente.to" class="form-control">
	                            		<option value="{{fascia_disponibile}}" ng-repeat="fascia_disponibile in avaibleFasce track by $index">{{fascia_disponibile}}</option>
	                        		</select>
	                        	</div>
	                        </div>                        
	                    </div>
	                </div>
	                <div class="col-sm-6">
	                    <div class="form-group" style="margin-top: 25px;">
	                        <button ng-click="add_fascia_to_restaurant(add_fascia_oraria.day,add_fascia_oraria.fascia)" class="btn btn-default">Aggiungi</button>
	                        <button ng-click="add_fascia_to_restaurant_all_days(add_fascia_oraria.fascia)" class="btn btn-primary">Aggiungi a tutti i giorni</button>
	                    </div>
	                </div>
	                <div class="clearfix"></div>

	                <div ng-repeat="day in week_days track by $index" class="col-md-2 col-sm-4 col-xs-6">
	                    <h3 class="size-small uppercase spacing"><b>{{day}}</b></h3>
	                    <p ng-repeat="fascia in restaurant.fasce[$index] track by $index">{{fascia}}
	                        <i class="mebi-close" ng-click="remove_fascia($index,$parent.$index)"></i>
	                    </p>
	                </div>

	                <div class="clearfix"></div>
	                <hr />
	                <div class="col-sm-12">
	                    <h4 class="uppercase size-medium">Giorni di chiusura/apertura speciali
	                  </h4>
	                    <p>Puoi inserire giorni speciali in cui il ristorante è aperto o chiuso, ad esempio durante festività ecc...
	                    </p>
	                </div>
	                <div class="clearfix"></div>
	                <div class="col-sm-7">
	                	<div class="row">
	                		<div class="col-sm-6">
			                    <div class="form-group">
			                        <label>Giorno</label>
			                        <input id="datepicker" type="text" class="form-control" />
			                    </div>
		                    </div>
		                    <div class="col-sm-6">
			                    <div class="form-group">
			                        <label>Tipo</label>
			                        <select ng-model="specialDates.type" class="form-control">
			                            <option value="0">Chiusura</option>
			                            <option value="1">Apertura</option>
			                        </select>
			                    </div>
			                </div>
			                <div class="col-sm-4">
			                    <div class="form-group">
			                        <label>Orario</label>
			                        <select ng-model="specialDates.all_day" class="form-control">
			                            <option value="1">Tutto il giorno</option>
			                            <option value="0">Fascia oraria</option>
			                        </select>
			                    </div>
			                </div>
			                <div class="col-sm-8">
			                    <div class="form-group" ng-if="specialDates.all_day == 0">
			                    	<div class="row">
				                    	<div class="col-sm-6">
				                    		<label>Dalle</label>
					                        <select ng-model="specialDates.from" class="form-control">
					                            <option value="{{fascia_disponibile}}" ng-repeat="fascia_disponibile in avaibleFasce track by $index">{{fascia_disponibile}}</option>
					                        </select>
					                    </div>
					                    <div class="col-sm-6">
					                    	<label>Alle</label>
					                        <select ng-model="specialDates.to" class="form-control">
					                            <option value="{{fascia_disponibile}}" ng-repeat="fascia_disponibile in avaibleFasce track by $index">{{fascia_disponibile}}</option>
					                        </select>	
				                        </div>
				                    </div>	                        
			                    </div>
			                </div>
			                <div class="clearfix"></div>
			                <div class="form-group">
		                        <button ng-click="setSpecialDate()" class="btn btn-primary btn-sm ghost">Aggiungi</button>
		                    </div>
		                </div>
	                    
	                </div>
	                <div class="col-sm-5">
	                    <h4 class="uppercase size-medium">Giorni speciali
	                  </h4>
	                    <ul class="list-unstyled">
	                        <li ng-repeat="day in restaurant.special_dates track by $index">
	                            <i class="mebi-close" ng-click="removeSpecialDate(day)"></i>
	                            {{day.day|amDateFormat:'DD/MM/YYYY'}}
	                            <b ng-if="day.type == '0'">Chiusura</b>
	                            <b ng-if="day.type == '1'">Apertura</b>
	                            <span ng-if="day.all_day == '0'">{{day.from}} - {{day.to}}</span>
	                            <span ng-if="day.all_day == '1'">Tutto il giorno</span>
	                        </li>
	                    </ul>
	                </div>
				</div>
				<div class="clearfix"></div>
				<hr />

				<div class="images_gallery col-md-8">
					<h3 class="uppercase">Immagine principale</h3>
					<div class="p_image"
					drop="setImagePrimary($data, $event)"  
	                effect-allowed="copy" 
	                drop-effect="copy" 
	                drop-accept="'json/img_obj'"
	                drag-over-class="drag_over_primary"
					>
					<div class="overlay">
						<div class="txt_overlay">
							Imposta come primaria
						</div>
					</div>
						<div class="i cover" back-img img="{{staticURL}}/restaurant/big/{{restaurant.image}}.png" ng-if="restaurant.image && restaurant.image != ''"></div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
					<h3 class="uppercase">Tutte le immagini</h3>
					<div class="gallery">					
						<div class="single_img_gallery" 
						draggable="true" 
		                effect-allowed="copy"
		                draggable-type="img_obj"  
		                draggable-data="{image_id: image}" 
						ng-repeat="image in restaurant.images track by $index" ng-if="$index > 0">
							<div class="single_img cover col-sm-3" 
							back-img img="{{staticURL}}/restaurant/big/{{image}}.png"
							></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-sm-4">
					<div class="gallery_buttons" set-class-when-at-top-buttons="fixed_btn">
						<div class="upload">
							<i class="fa fa-upload" aria-hidden="true"></i>
							<input class="upload-input" type="file" ngf-select 
							ng-model="restaurant.adding_image" 
							name="restaurant.adding_image" 
							ng-change="uploadRestaurantImage()" title="Cambia" />
						</div>
						<div class="trash">
							<i class="fa fa-trash" aria-hidden="true"
							drop="removeImageOnDrop($data, $event)"  
			                effect-allowed="copy" 
			                drop-effect="copy" 
			                drop-accept="'json/img_obj'"
			                drag-over-class="drag_over_trash"></i>
						</div>
						<div class="primary_set">
							<i class="fa fa-star-o"
							drop="setImagePrimary($data, $event)"  
			                effect-allowed="copy" 
			                drop-effect="copy" 
			                drop-accept="'json/img_obj'"
			                drag-over-class="drag_over_primary"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<!-- end edit restaurant -->

	<!-- edit menu -->
	<div ng-if="current_tab == 2">
		<div class="white_container" ng-init="InitMenu()">
			<div class="container">
				<div ng-if="!restaurant.menu.menus || restaurant.menu.menus.length == 0">
					<div class="hero_menu animated fadeIn text-center">
						<h3>Ancora non hai aggiunto un menù al tuo ristorante!</h3>
						<p><a class="btn ghost" ng-click="startAddingMenu()">Crea il tuo primo menù</a></p>
					</div>

				</div>
				<div ng-if="restaurant.menu.menus && restaurant.menu.menus.length > 0">
					<div class="hero_menu animated fadeIn text-center">
						<h3>Modifica i menù del tuo ristorante</h3>
						<p>Seleziona un menu da modificare o <a ng-click="addMenuToList()" class="text-warning">aggiungi un menu</a>
						<p>
							<span class="fasce_span" 
							ng-repeat="(menu_i, menu) in restaurant.menu.menus"
							ng-click="setCurrentMenuOpen(menu_i)" 
							ng-class="{'active': current_menu_viewed == menu_i}">
						      {{menu.name}}
						    </span>
						</p>						 
					</div>
					<div class="menu_container">
						<div class="current_menu_viewed">							
							<div class="text-center margin-big-vertical">
								<h3 class="edit_parent">
								{{restaurant.menu.menus[current_menu_viewed].name}}
								</h3>
								
								<a class="btn btn-sm btn-default adding_btn" ng-click="addCategoryToMenu()">
									<i class="mebi-android-add"></i>
									Aggiungi una categoria</a>
								<a class="btn btn-sm btn-danger adding_btn" ng-click="deleteCurrentMenu()">
									<i class="mebi-close"></i>
									Cancella il menu</a>
							</div>

							<div ng-if="restaurant.menu.menus[current_menu_viewed].menu_type == 1">
								<div class="form-group">
									<label>Prezzo (menu fisso) €</label>
									<input type="text" 									
										ng-model="restaurant.menu.menus[current_menu_viewed].menu_price" 
										class="form-control price-input animated bounceIn" pformat prefix="" cents='.' thousands=''/>
								</div>
							</div>
							<hr />
							<h2>Categorie del menù</h2>

							<div class="category_block" 
							ng-repeat="(i_cat,cat) in restaurant.menu.menus[current_menu_viewed].categorie" 
							id="menu_{{current_menu_viewed}}_category_{{i_cat}}">
								<h3 class="edit_parent category_list_view">
									{{cat.name}}
									<span 
									ng-repeat="categoria in restaurant.menu.menus[current_menu_viewed].categorie track by $index" 
									ng-click="scrollTo('menu_'+current_menu_viewed+'_category_'+$index)"
									ng-if="$index != i_cat">
										{{categoria.name}}
									</span>
									<br />
									<div class="text-center margin-big-v highlighted">
										<a class="btn btn-sm btn-default adding_btn" ng-click="addPlateToCategory(i_cat)">
											<i class="mebi-android-add"></i>
											Aggiungi un piatto
										</a>
										<a class="btn btn-sm btn-danger adding_btn" ng-click="deleteCurrentCategory(i_cat)">
											<i class="mebi-close"></i>
											Cancella</a>
									</div>
								</h3>

								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label>Nome della categoria</label><br />												
											<input type="text" ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].name" 
												class="simple_border_input form-control plate_name" 
												/>  
											
										</div>
									</div>									
								</div>
								<hr />
								<h2>I piatti</h2>
								<div class="sort_plates"
								html-sortable="sortable_plates" ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti"
								>
									<div class="single_plate_container" 
										ng-repeat="(i_piatto, piatto) in restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti"
										id="menu_{{current_menu_viewed}}_category_{{i_cat}}_plate_{{i_piatto}}"
										>
										<div class="row">
											<div class="col-sm-2">
												<!-- image -->
												<div class="row">
													<div class="col-sm-6">
														<!-- icona trascinamento -->
														<i class="mebi-cd-icon-select move_sort_icon"></i>
													</div>
													<div class="col-sm-6">
														<div class="plate_image_container">
															<input class="upload-input upload_in_plate" type="file" ngf-select 
																	ng-model="add_plate_image.image" 
																	name="add_plate_image" 
																	ng-change="uploadPlateImage(i_cat,i_piatto)" title="Immagine" />
															<i class="mebi-technology-1 add_plate_icon" ng-if="!piatto.image"></i>
															<img ng-src="{{staticURL}}/menu/small/{{piatto.image}}.png" alt="" title="Cambia immagine" ng-if="piatto.image" class="single_edit_plate_image" />
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12 marginated_top">
														<a class="btn btn-sm btn-danger adding_btn" ng-click="deleteCurrentPlate(i_cat,i_piatto)">
														<i class="mebi-close"></i>
														Cancella il piatto</a>
													</div>
												</div>
												
											
											</div>
											<div class="col-sm-2">
												<!-- name -->
												<div class="form-group">
													<label>Nome</label>												
													<textarea msd-elastic 
														ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].name" 
														class="simple_border_input form-control plate_name" 
														></textarea>  
												</div>
											</div>
											<div class="col-sm-6">
												<!-- description, ingredienti, allergeni -->
												<div class="row">
													<div class="col-sm-12">
														<div class="form-group">
															<label>Descrizione</label>
															<textarea msd-elastic 
															ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].description"
															class="form-control"
															></textarea>  
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															<label>Ingredienti</label><br />
															<tagger 
															ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].ingredienti" />
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															<label>Allergeni</label><br />
															<tagger 
															ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].allergeni" />
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-2">
												<!-- prezzo -->
												<div class="form-group">
													<label>Prezzo</label>
													€ <input type="text" ng-if="!restaurant.menu.menus[current_menu_viewed].menu_type || restaurant.menu.menus[current_menu_viewed].menu_type == 0" 
													ng-model="restaurant.menu.menus[current_menu_viewed].categorie[i_cat].piatti[i_piatto].price" 
													class="form-control price-input animated bounceIn" pformat prefix="" cents='.' thousands=''/>
												</div>
											</div>										
										</div>
									</div>
								</div>
								<hr />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="menu_add_by_step custom_scrollbar" ng-class="{'open':editing_menu}">
			<i class="mebi-close menu_step_close" ng-click="closeEditing()"></i>
			<div class="content_menu_adding add_menu_name text-center" ng-if="current_menu_step == 1">
				<!-- add menu -->
				<h3 class="uppercase">
					<span ng-if="!restaurant.menu.menus || restaurant.menu.menus.length == 0">Inizia dando un nome </span> 
					<span ng-if="restaurant.menu.menus && restaurant.menu.menus.length > 0">Dai un nome </span> 
						al tuo menù
				</h3>
				
				<p class="text-primary">Scegline uno tra quelli consigliati</p>
				
				<p class="options_selections">
					<a ng-click="setMenuName('Pranzo')" ng-if="!existMenuName('Pranzo')">Pranzo</a>
					<a ng-click="setMenuName('Cena')" ng-if="!existMenuName('Cena')">Cena</a>
					<a ng-click="setMenuName('Carta dei vini')" ng-if="!existMenuName('Carta dei vini')">Carta dei vini</a>
				</p>

				<p>O aggiungilo manualmente</p>
				<input type="text" ng-model="add_menu.menu_name" autosize class="simple_border_input form-control" placeholder="Nome del menu" />
				<hr />	
				<div class="fixed_price" ng-class="{'fixed_selected': add_menu.menu_type == 1}">
					<span class="uppercase">Menu a prezzo fisso</span>
					<i class="mebi-check-1 check_fixed_price" ng-click="setMenuType()"></i> 					
	                <input type="text" ng-model="add_menu.menu_price" class="form-control price-input animated bounceIn" ng-if="add_menu.menu_type == 1" pformat prefix="€ " cents='.' thousands=''/>
	            </div>
				<p><a class="btn ghost" ng-click="confirmMenuName()" ng-if="add_menu.menu_name != ''">Conferma</a></p>
			</div>
			<div class="content_menu_adding add_menu_category text-center" ng-if="current_menu_step == 2">
				<!-- add category -->
				<h3>{{restaurant.menu.menus[current_menu_index].name}}</h3>
				<h3 class="uppercase" ng-if="(!restaurant.menu.menus[current_menu_index].categorie || restaurant.menu.menus[current_menu_index].categorie.length == 0) && restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">
					Ora che hai aggiunto un nuovo menu, inserisci una categoria di riferimento per i piatti
				</h3>
				<h3 class="uppercase" ng-if="(restaurant.menu.menus[current_menu_index].categorie && restaurant.menu.menus[current_menu_index].categorie.length > 0) && restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">
					Inserisci una categoria per i piatti del menu
				</h3>
				<h3 class="uppercase" ng-if="restaurant.menu.menus[current_menu_index].name == 'Carta dei vini'">
					Inserisci una categoria per i vini
				</h3>
				<p ng-if="restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">Ad esempio 
					<a class="text-primary" ng-click="setCategoryName('Antipasti')">"antipasti"</a>, 
					<a class="text-primary" ng-click="setCategoryName('Primi')">"primi"</a>, 
					<a class="text-primary" ng-click="setCategoryName('Secondi')">"secondi"</a>, 
					<a class="text-primary" ng-click="setCategoryName('Contorni')">"contorni"</a>...
				</p>
				<p ng-if="restaurant.menu.menus[current_menu_index].name == 'Carta dei vini'">Ad esempio 
					<a class="text-primary" ng-click="setCategoryName('Bianchi')">"bianchi"</a>, 
					<a class="text-primary" ng-click="setCategoryName('Rossi')">"rossi"</a>...
				</p>
				<input type="text" ng-model="add_category.name" class="simple_border_input form-control" placeholder="Nome della categoria" />
				<p><a class="btn ghost" ng-click="confirmCategoryName()" ng-if="add_category.category_name != ''">Conferma</a></p>
			</div>
			<div class="content_menu_adding text-center" ng-if="current_menu_step == 3">
				<!-- add plate -->
				<h3 class="uppercase" ng-if="restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">
					Aggiungi un piatto
				</h3>
				<h3 class="uppercase" ng-if="restaurant.menu.menus[current_menu_index].name == 'Carta dei vini'">
					Aggiungi il vino
				</h3>

				<p ng-if="!add_plate.image">Clicca sull'icona per aggiungere una immagine</p>
				<div class="add_plate_image_container">
					<input class="upload-input" type="file" ngf-select 
							ng-model="add_plate_image.image" 
							name="add_plate_image" 
							ng-change="uploadPlateImage()" title="Immagine" />
					<i class="mebi-technology-1 add_plate_icon" ng-if="!add_plate.image"></i>
					<img ng-src="{{staticURL}}/menu/small/{{add_plate.image}}.png" alt="" title="" ng-if="add_plate.image" />
				</div>

				<p ng-if="restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">Nome del piatto *</p>
				<p ng-if="restaurant.menu.menus[current_menu_index].name == 'Carta dei vini'">Etichetta *</p>
				<input type="text" ng-model="add_plate.name" class="simple_border_input form-control plate_name" 
				autosize />
				
				
				<div class="row" ng-if="restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">
				<hr />
					<div class="col-sm-6">
						<p>Gli ingredienti</p>
						<tagger ng-model="add_plate.ingredienti" />
					</div>
					<div class="col-sm-6">
						<p>Allergeni</p>
						<tagger ng-model="add_plate.allergeni" />
					</div>
					<div class="clearfix"></div>
				<hr />
				</div>				
				<p>Una descrizione 
					<span ng-if="restaurant.menu.menus[current_menu_index].name != 'Carta dei vini'">del piatto</span>
					<span ng-if="restaurant.menu.menus[current_menu_index].name == 'Carta dei vini'">del vino</span>
				</p>
				<textarea msd-elastic ng-model="add_plate.description"></textarea>  
				<!-- prezzo -->
				<div class="fixed_price" ng-if="!restaurant.menu.menus[current_menu_index].menu_type || restaurant.menu.menus[current_menu_index].menu_type == 0">
					<span class="uppercase">Prezzo</span>
					<input type="text" ng-model="add_plate.price" class="form-control price-input animated bounceIn" 
					pformat prefix="€ " cents='.' thousands=''/>
	            </div>
	            <p>* I campi con asterisco sono obbligatori</p>
	            <p><a class="btn ghost" ng-click="insertPlate()" ng-if="add_plate.name != ''">Conferma</a></p>
			</div>
		</div>
	</div>
	<!-- end edit menu -->

	<!-- edit struttura -->
	<div ng-if="current_tab == 3">
		<div class="white_container" ng-init="InitStructure()">
			<div class="container">
				<div class="col-sm-6">
                    <h3 class="uppercase spacing size-big pad-big-v">Tempo tra le prenotazioni</h3>
                    <p class="size-small">Seleziona i minuti tra una prenotazione e la successiva</p>                    
                        
                    <span class="fasce_span" ng-click="setTimeShift(15)" ng-class="{'active': restaurant.time_shift == 15}">
				      15
				    </span>
				    <span class="fasce_span" ng-click="setTimeShift(30)" ng-class="{'active': restaurant.time_shift == 30}">
				      30
				    </span>
				    <span class="fasce_span" ng-click="setTimeShift(60)" ng-class="{'active': restaurant.time_shift == 60}">
				      60
				    </span>
                    
                </div>
                <div class="col-sm-6">	                
                    <h3 class="uppercase spacing size-big pad-big-v">Aggiungi una sala</h3>
                    <div class="form-group">
                        <label>Nome della sala</label>
                        <input type="text" ng-model="add_struttura_children.sala" class="form-control" />
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-outline btn-sm" ng-click="add_sala()">Aggiungi una sala</button>
                    </div>                                
	            </div>
                <div class="clearfix"></div>
                <hr />
                <div class="col-lg-12 col-md-12">

                	<h3 class="uppercase spacing size-big pad-big-v">Le sale</h3>
                	<p>Seleziona la sala da modificare</p>
                    
                    <span class="fasce_span" ng-repeat="sala in restaurant.struttura.sale track by $index" 
                    ng-click="setCurrentSala($index)" 
                    ng-class="{'active': current_sala == $index}">
				      {{sala.name}}
				    </span>
				    <div class="clearfix"></div>
					
					<div  ng-if="restaurant.struttura.sale.length > 0 && current_sala != null">
						<p class="text-center">Modifica il nome della sala cambiando il testo qui in basso</p>
	                    <h3 class="uppercase spacing size-medium margin-medium-v">
		              		<input type="text" class="input-hidden-editable sala_input" ng-model="restaurant.struttura.sale[current_sala].name" />
		              	</h3>
		            </div>



	              	<div class="col-md-3 col-sm-4 col-xs-12" 
			        ng-repeat="(i_table,table) in restaurant.struttura.sale[current_sala].tables" 
			        style="height: 238px;">
			          <div class="single_table">
			            <span class="table_num">{{table.num}}</span>
			            <span class="table_posti">x{{table.posti}}</span>
			          </div>
			          
			          <div class="text-center table_actions">
			            <i class="mebi-close text-danger" ng-click="removeTable(current_sala,i_table)"></i>
			          </div>
			        </div>
                    
                    <div class="clearfix"></div>

                    <p class="text-center" ng-if="restaurant.struttura.sale.length > 0 && current_sala != null">
                    	<a ng-click="addTableDialog()" class="btn btn-primary ghost">Aggiungi un tavolo</a>
                    </p>

                    <p class="text-center" ng-if="restaurant.struttura.sale.length > 0 && current_sala != null">
                    	<a ng-click="removeSala()" class="btn btn-danger pull-right">Cancella questa sala</a>
                    </p>

                </div>
			</div>
		</div>
	</div>
	<!-- end edit struttura -->

	<!-- reservation -->
	<div ng-if="current_tab == 4">
		<div class="white_container">
			<div class="container">
				<div ng-if="!restaurant.struttura || !restaurant.struttura.sale || restaurant.struttura.sale.length == 0">
					<div class="hero_menu animated fadeIn text-center">
						<h3>Ancora non hai aggiunto la struttura del tuo ristorante!</h3>
						<p><a class="btn ghost" ng-click="changeTab(3)">Aggiungi le tue sale</a></p>
					</div>

				</div>
				<div ng-if="restaurant.struttura && restaurant.struttura.sale && restaurant.struttura.sale.length > 0">
					<div class="minimal_tabs">
						<ul class="list-unstyled">
							<li ng-class="{'active' : reservation_tab == 0 || !reservation_tab}"><a ng-click="setReservationTab(0)">Prenotazioni</a></li>
							<li ng-class="{'active' : reservation_tab == 1}"><a ng-click="setReservationTab(1)">Widget</a></li>								
						</ul>
					</div>
					<div ng-if="!reservation_tab || reservation_tab == 0">
						<div class="col-lg-3 col-md-12 col-sm-12 ">
							<div class="" ><!--ng-controller="ProfileCtrl"-->
								<div class="col-sm-12">
								  <h4 class="uppercase size-medium inline-block">Calendario</h4>
								  <p>Visualizza graficamente il calendario degli ultimi 2 mesi e dei prossimi due per un'immediato feedback sugli ordini ricevuti, 
								    in base al colore della casella corrispondente al giorno cambia il numero di prenotazioni ricevute.
								    <br /> 
								    Clicca sulle caselle per visualizzare le prenotazioni ricevute nella data corrispondente.</p>
								  <!--<cal-heatmap config="{verticalOrientation:'true'}"></cal-heatmap>  -->
								  <div id="heatmap" ng-init="InitReservations()"></div>       
								</div>
							</div>
						</div>

						<div class="col-lg-9 col-md-12 col-sm-12 container_tables">
							<!-- Page Widget -->
							<div class="" ><!--ng-controller="ProfileCtrl"-->
							  <div class="col-sm-12">
							    <h4 class="uppercase size-medium inline-block">Giorno corrente: {{current_date|amDateFormat:'DD/MM/YYYY'}}</h4>
							              
							    <div class="clearfix"></div>
							    <p ng-if="!current_fascia">Seleziona una fascia oraria</p>
							    <span class="fasce_span" ng-repeat="fascia in restaurant_fasce track by $index" ng-click="selectFasciaOraria(fascia)" ng-class="{'active': current_fascia == fascia}">
							      {{fascia}}
							    </span>
							    <div class="clearfix fasce_orarie_block"></div>
							    

							    <div class="col-sm-9">

							      <div ng-repeat="(i_sala,sala) in restaurant.struttura.sale track by $index" ng-if="current_fascia">
							        <h3 class="uppercase  col-xs-12">{{sala.name}}</h3>

							        <div class="col-md-4 col-sm-6 col-xs-12 table_parent" 
							        ng-repeat="(i_table,table) in restaurant.struttura.sale[i_sala].tables" 
							        drop="onDrop($data, {{i_sala}}, {{i_table}}, $event)"  
							        effect-allowed="copy" 
							        drop-effect="copy" 
							        drop-accept="'json/reservation_obj'"
							        drag-over-class="drag-over-accept" style="height: 238px;">
							          <div class="single_table">
							            <span class="table_num">{{table.num}}</span>
							            <span class="table_posti">x{{table.posti}}</span>
							          </div>
							          
							          <div ng-if="table.reservation" class="text-center">
							          	<p class="text-center">
							            	<label>{{returnUser(table.reservation.user).name}} {{returnUser(table.reservation.user).surname}}</label><br />
							            	<label class="text-primary" ng-click="loadSingleReservation(table.reservation._id)">Vedi</label>
							            </p>
							          </div>
							        </div>
							        <div class="clearfix fasce_orarie_block"></div>
							      </div>
							    </div>

							    <div class="col-sm-3">
							      <p>Trascina le prenotazioni sui tavoli per confermarle</p>
							      <ul class="list-unstyled imaging_list">
							        <li draggable="true" 
							        effect-allowed="copy"
							        draggable-type="reservation_obj"  
							        draggable-data="viewed_reservations[$index]" 
							        ng-repeat="reservation in viewed_reservations track by $index" 
							        style="cursor: move"><!-- ng-click="loadSingleReservation(reservation._id)"--> 
							        
							            <div class="row">
							              <div class="col-xs-12">
							              	<p class="text-center">
							              		<img 
								              	ng-if="returnUser(reservation.user).avatar" 
								              	class="img-circle" 
								              	ng-src="{{staticURL}}/users/square/{{returnUser(reservation.user).avatar}}.png"
								              	/>
								            </p>
								            <p class="text-center">
								            	<label>{{returnUser(reservation.user).name}} {{returnUser(reservation.user).surname}}</label><br />
								            	<label class="text-primary" ng-click="loadSingleReservation(reservation._id)">modifica</label>
								            </p>
								            <p class="text-center">
								            	{{reservation.tel}} - {{reservation.email}}
								            </p>
								            <div class="row icons_block">
												<div class="text-center">
													<i class="mebi-avatar"></i> {{reservation.people}}
												</div>											
											</div>							                
							              </div>
							            </div>
							            <div class="clearfix"></div>
							            <hr />
							        </li>
							      </ul>
							    </div>


							    <div class="clearfix"></div>

							   
							  </div>
							</div>
						</div>

					</div>
					<div ng-if="reservation_tab == 1">
						<div class="col-sm-12" ng-init="initWidget()" ng-if="view_user.billing_plan && view_user.billing_plan.length > 0 && view_user.billing_plan.active == 1">
					      <div class="panel pad-big-o margin-big-v">      
					        
					        <div class="panel-body">
					           	<div class="col-sm-6 col-sm-push-3" ng-if="!hasiframes">
					           		<p>Per poter ricevere le prenotazioni dal tuo sito web aggiungi il dominio (www.nomedeltuosito.it)</p>
						      		<form id="addIframeForm">
						      			<div class="form-group">
						      				<label for="url">Dominio del sito web</label>
						      				<input type="text" name="url" ng-model="add_iframe.url" class="activated_input full" required/>
						      			</div>
						      			<p class="text-center"><button type="submit" class="btn btn-primary ghost" ng-disabled="addIframeForm.$invalid" 
						      			ng-click="addIframe()">Aggiungi</button></p>
						      			
						      		</form>
					      		</div>
					            <div class="col-sm-12" ng-if="hasiframes == 1">
						      		
					            	<h3 class="uppercase size-medium">Codice da incorporare</h3>
					            	<p>Copia e incolla questo codice nel tuo sito web per aggiungere il widget</p>
					            	<pre>&lt;div id="mangiaebevi_restaurant_iframe"&gt;&lt;/div&gt;<br />&lt;script type="text/javascript" src="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/static/{{iframe[0].token}}/frameloader.js"&gt;&lt;/script&gt;
					            	</pre>

					            	<p>Per inserirlo nella tua pagina facebook usa questo url: <?php echo env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/reservationframe/facebook/';?>{{restaurant._id}}</p>

					            	<hr />

						      		<h3 class="uppercase size-medium">Esempio di visualizzazione del widget</h3>
					            	<div id="mangiaebevi_restaurant_iframe"></div>
									<script type="text/javascript" src="<?php echo env('APP_PROTOCOL');?>://<?php echo env('APP_DOMAIN');?>/static/test/frameloader.js"></script>
									
					      		</div>                
					        </div>
					        
					      </div>
					    </div>
					    <div class="col-sm-12" ng-init="initPlans()" ng-if="!view_user.billing_plan || view_user.billing_plan.length == 0 || view_user.billing_plan.active != 1">
					    	<p class="text-center">Per poter aggiungere il widget sul tuo sito o nella tua pagina facebook devi iscriverti ad uno dei piani di abbonamento di MangiaeBevi</p>
					    	<div class="col-sm-4" ng-repeat="plan in pacchetti">
					    		<div class="plan_table" ng-class="{'featured': plan.featured == 1}">
					    			<div class="plan_header">
					    				<h3 class="plan_price text-center" ng-if="selected_plan_period == 0">{{plan.price_month}} €</h3>
					    				<h3 class="plan_price text-center" ng-if="selected_plan_period == 1">{{plan.price_annual}} €</h3>
					    				/ {{returnCicleText(selected_plan_period)}}
					    			</div>
					    			<div ng-if="plan.period == 2" class="plan_fatturazione text-center">
					    				<button ng-click="setPlanPeriod(0)" ng-class="{'text-primary':selected_plan_period==0}">mensile</button>
					    				<button ng-click="setPlanPeriod(1)" ng-class="{'text-primary':selected_plan_period==1}">annuale</button>
					    			</div>
					    			<div class="plan_services">
					    				<ul class="list-unstyled">
					    					<li ng-repeat="serviceplan in plan.services" class="text-center">{{serviceplan}}</li>
					    				</ul>
					    			</div>
					    			<div class="plan_button text-center">
					    				<button ng-click="submitPlan(plan._id)" class="outline_btn bprimary">Conferma</button>
					    			</div>
					    		</div>

					    	</div>
					    </div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- end reservation -->
</div>

