 <!-- Page -->
  
<div id="profilePage" class="container bg-white" 
ng-init="
setSEO('Termini e condizioni | Mangiaebevi','/termini-e-condizioni/','','','','MangiaeBevi, termini e condizioni')"
>
	
		<div class="panel pad-big-o">
    		<h2>TERMINI E CONDIZIONI GENERALI</h2>

			 <p>Le condizioni generali di contratto sotto riportate (di seguito, “Condizioni Generali”) si applicano a tutti i servizi e alla comunità (di seguito indicati come “Servizi” o singolarmente “Servizio”).<br>
L’utente dei Servizi (di seguito, “Utente” o “Utenti”), che si registra a, o utilizza uno qualsiasi dei Servizi, deve accettare le Condizioni Generali, le quali saranno applicabili anche ai Servizi, che verranno forniti in futuro agli Utenti, salvo che venga altrimenti previsto all’atto della registrazione o della prima fornitura del nuovo Servizio. MangiaeBevi si riserva il diritto di modificare le presenti condizioni di utilizzo del servizio in ogni momento e senza preavviso.</p>
<p>L’utilizzo dei servizi dopo l’entrata in vigore di tali modifiche comporta la completa accettazione delle modifiche medesime.<br>
L’Utente potrà verificare in qualunque momento il testo in vigore delle condizioni di utilizzo alla pagina descrittiva del servizio. MangiaeBevi si riserva inoltre il diritto di aggiungere, modificare o eliminare, a sua discrezione e in ogni momento, servizi e contenuti di questo sito.</p>
<h3>
Benvenuto nelle comunità e nei servizi di MangiaeBevi.</h3>
<p>MangiaeBevi valorizza la libertà d’espressione dei propri membri, ma richiede che tale libertà sia utilizzata nei limiti del lecito e che non offenda gli altri.<br>
La fruizione dei servizi à ammessa solo a condizione di essere membro di MangiaeBevi. Gli utenti Internet diventano membri a seguito di registrazione. Questa comporta il consenso esplicito degli utenti alle condizioni riportate di seguito che ne regolano l’accesso ed al trattamento dei dati con essa forniti. La registrazione richiede l’inserimento delle informazioni richieste che verranno trattate in assoluta conformità alla “<strong>Legge sulla privacy</strong>”.<br>
In particolare, à richiesto che l’indirizzo di e-mail corrisponda a quello reale, in quanto indispensabile per ricevere le istruzioni necessarie per convalidare la registrazione.<br>
L’Utente si impegna inoltre ad aggiornare tempestivamente i dati registrati affinché questi siano costantemente aggiornati, completi e veritieri. MangiaeBevi potrà in ogni momento, e senza nessun preavviso, sospendere o interrompere definitivamente l’erogazione dei Servizi.<br>
L’Utente è responsabile della conservazione della segretezza della password e dell’identificativo utente. Inoltre l’Utente assume esclusiva responsabilità per ogni sua attività nell’ambito dei Servizi e si impegna a manlevare e tenere indenne MangiaeBevi da qualsiasi rivendicazione, pretesa o minaccia relativa a o derivante dall’uso o dall’abuso della propria partecipazione ai Servizi.</p>
<p>L’Utente si obbliga a notificare immediatamente a MangiaeBevi all’indirizzo di posta elettronica <a href="mailto:helpdesk@mangiaebevi.it">helpdesk@mangiaebevi.it</a>,&nbsp;qualsiasi uso non autorizzato del proprio identificativo utente e/o password o ogni altra violazione della sicurezza di cui venisse a conoscenza. L’Utente può in ogni momento cambiare la propria password seguendo le istruzioni fornite dal sistema.<br>
Come utente registrato accetti di ricevere i nostri messaggi e-mail come parte integrante del servizio gratuito. MangiaeBevi utilizza la e-mail solo per informarti in merito alle caratteristiche e alle prerogative del nostro servizio, per metterti a conoscenza di eventuali problemi presenti sul nostro sito e per comunicarti le offerte speciali riservate ai membri di MangiaeBevi nel pieno rispetto della normativa italiana e comunitaria vigente a tutela dei consumatori.<br>
Il diritto dell’Utente di usare i Servizi è personale e non cedibile.</p>
<p>All’Utente è fatto divieto di rivendere o fare qualsiasi altro uso commerciale dei Servizi, senza il consenso scritto di MangiaeBevi.</p>
<p>MangiaeBevi si riserva il diritto di modificare o rimuovere qualunque contenuto inviato e di revocare l’iscrizione in qualunque momento e per qualunque motivo senza preavviso.<br>
Sono assolutamente vietate e possono causare la revoca immediata della tua iscrizione e/o la cancellazione del tuo Weblog e del tuo profilo le azioni e le attività che comprendono, senza che ciò costituisca un limite, l’inserimento di pagine Web o in tutti gli altri contesti interattivi predisposti da MangiaeBevi contenenti o con riferimenti a:</p>
<ul>
<li>materiale pornografico e a sfondo sessuale di natura oscena;</li>
<li>materiale che viola i diritti d’autore, in particolare software pirata (“WAREZ, CRACK”) file musicali, immagini, video, testi protetti da Copyright;</li>
<li>materiale offensivo o diffamatorio nei confronti di chicchessia, incluse espressioni, di fanatismo, razzismo, odio, irriverenza o minaccia;</li>
<li>materiale che promuove o fornisce informazioni che istruiscano su attività illegali o che possano causare pregiudizio a terzi;</li>
<li>software, informazioni o altro materiale contenente virus o componenti dannosi;</li>
<li>iniziative legate al gioco d’azzardo, concorsi, giochi che richiedono una partecipazione a titolo oneroso;</li>
<li>materiale non adatto ai minori di 18 anni;</li>
<li>pubblicità o sponsorizzazioni a pagamento;</li>
<li>materiale o attività che, in generale, violi o induca a violare una qualsiasi disposizione di legge o di regolamento posta a tutela anche solo di privati od una disposizione legittimamente impartita dalla Pubblica Autorità.</li>
</ul>
<p>Sebbene non sia obbligata a controllare i contenuti immessi nelle proprie pagine Web, MangiaeBevi si riserva il diritto di accedere alle stesse, in ogni momento e senza preavviso, per verificare il rispetto delle condizioni di utilizzo del servizio, l’eventuale violazione, anche solo presunta, dei diritti di terzi, la responsabilità degli utenti a seguito di denunce provenienti anche da privati, e di prendere i dovuti provvedimenti, ivi inclusa l’eventuale sospensione o interruzione del servizio. MangiaeBevi, ove dovesse riscontrare la possibilità che si configuri qualche reato, ne informerà le autorità competenti.<br>
Ai membri di MangiaeBevi à fatto inoltre divieto di:</p>
<ul>
<li>limitare l’accesso alle proprie pagine Web ospitate su MangiaeBevi (salvo che la funzionalità sia offerta da MangiaeBevi) o inserire pagine o file nascosti non raggiungibili tramite un link;</li>
<li>utilizzare lo spazio Web come archivio di file;</li>
<li>utilizzare lo spazio Web come archivio per file utilizzati da altri server.</li>
</ul>
<p>I membri garantiscono a MangiaeBevi e ai suoi partner la licenza illimitata, irrevocabile, esente da royalty di utilizzare, copiare, modificare, trasmettere, distribuire pubblicamente le pagine Web da loro inviate con finalità</p>
<ul>
<li>di ospitare i rispettivi siti Web (weblog),</li>
<li>di marketing e promozione.</li>
</ul>
<p>MangiaeBevi non à in alcun modo responsabile dei contenuti e delle opinioni presenti nelle pagine dei membri nà della effettiva esistenza, distribuzione e consegna, nà e/o conformità della qualità di beni o servizi venduti, commercializzati, pubblicizzati a titolo gratuito attraverso le pagine dei membri.</p>
<p>Nel registrarsi, gli utenti riconoscono la propria esclusiva responsabilità, civile e penale, per tutto ciò che verrà dagli stessi inserito nel contesto interattivo e multimediale messo a loro disposizione da MangiaeBevi che non è in alcun modo responsabile della condotta dei membri che violano i termini di servizio. Pertanto la redazione si riserva il diritto di invalidare, senza doverne dare comunicazione all’utente, qualsiasi commento o altro contenuto che sia ritenuto inappropriato perchè di natura diffamatoria, ingiuriosa, volgare o discriminatoria. Non saranno tollerati inoltre riferimenti diretti ai gestori o al personale che, nel bene o nel male, svolgono il loro lavoro.<br>
MangiaeBevi apprezza la collaborazione di tutti coloro che vogliano segnalare qualunque violazione delle condizioni di utilizzo del servizio della comunità e delle leggi vigenti.<br>
I membri sono responsabili e perseguibili legalmente in relazione al contenuto delle rispettive pagine Web ospitate da MangiaeBevi. Nell’inviare le pagine al sito di MangiaeBevi, i membri dichiarano di attenersi alle regole esposte nelle condizioni di utilizzo del servizio.</p>
<p>Qualora MangiaeBevi o le società ad esso collegate siano destinatarie di azioni giudiziali su iniziativa di terze parti in merito alla condotta o alla modalità di utilizzo dei servizi della comunità da parte di un membro, questi accetta espressamente di tenere indenne MangiaeBevi o le predette società da ogni effetto pregiudizievole connesso all’azione legale e di rimborsare ogni spesa e di risarcire ogni danno diretto o indiretto.</p>
<p>L’uso di questo sito à interamente a rischio dell’utente. I servizi di MangiaeBevi sono erogati senza alcuna garanzia, esplicita o implicita, MangiaeBevi non è in alcun modo responsabile degli eventuali danni derivanti dall’inaccessibilità ai propri servizi, nonché dalla perdita e dall’alterazione delle pagine dei membri.<br>
MangiaeBevi declina inoltre ogni reponsabilità relativa a danni causati da virus, file danneggiati, errori, omissioni, interruzioni del servizio, cancellazioni, problemi di rete, furto, accesso non autorizzato, alterazioni dei dati.</p>
<p>MangiaeBevi declina ogni responsabilità per le eventuali pretese dell’Utente relative all’impossibilità di utilizzare i servizi per qualsiasi ragione. MangiaeBevi non assume responsabilità per danni, pretese o perdite, dirette o indirette, derivanti all’Utente per il mancato e/o difettoso funzionamento delle apparecchiature elettroniche dell’Utente stesso o di terzi, inclusi gli Internet Service Providers, di collegamenti telefonici e/o telematici non gestiti direttamente da 2spaghi o da persone di cui questa debba rispondere. MangiaeBevi non potrà essere ritenuta inadempiente alle proprie obbligazioni né responsabile dei danni conseguenti alla mancata prestazione dei servizi a causa dell’errato o mancato funzionamento del mezzo elettronico di comunicazione per cause al di fuori della sfera del proprio prevedibile controllo, compresi, in via esemplificativa, incendi, disastri naturali, mancanza di energia, indisponibilità delle linee di collegamento telefoniche o di altri fornitori dei servizi di rete, malfunzionamento dei calcolatori ed altri dispositivi elettronici, anche non facenti parte integrante della rete Internet, malfunzionamento dei softwares installati dall’Utente, nonché da azioni di altri utenti o di altre persone aventi accesso alla rete.</p>
		
	</div>    
</div>