<div id="staticPage" 
ng-init="
setSEO('Carica immagini | Mangiaebevi','/uploads/','','','','')"
>
	
	<section>
		<div class="container" ng-if="view_page">
			<div class="row" ng-init="init()">
				<div class="col-sm-6 col-sm-push-3">
					<div ng-repeat="r in restaurants" style="margin-top: 30px;">
						<h3><a href="https://www.google.it/?ion=1&espv=2#q={{r.google_url}}" target="_blank">{{r.name}}</a></h3>
						{{r.address}} - <label>{{r.city}}</label>
						<div class="clearfix"></div>
						<div id="{{r._id}}_{{image}}" class="col-sm-3" ng-repeat="image in r.images track by $index">
							<input class="form-control" 
							type="file" 
							ngf-select 
							ng-model="add_image.image" name="change_image" ng-change="uploadImage(image,r._id)" title="Carica immagine {{$index}}" />
						</div>
						<div class="clearfix"></div>
					</div>				
				</div>
			</div>
		</div>
		
	</section>
	<section ng-if="!view_page">
		<h2>Accedi prima</h2>
	</section>
</div>