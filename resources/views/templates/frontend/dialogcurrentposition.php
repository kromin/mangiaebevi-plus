<div class="form-info">
	<h3 ng-if="!current_plate">Imposta la tua posizione attuale</h3>
	<form name="setPositionForm" ng-submit="editAddress()">		
		<div class="form-group">
			<label for="message">Indirizzo attuale</label>
			<input type="text" name="confirm_address" ng-model="edit_user.current_address"
				ng-autocomplete 
				details="details"
				class="form-control" 
				placeholder="Indirizzo" 
				required />
		</div>		
		<div class="form-group">	
			<input type="submit" ng-disabled="setPositionForm.$invalid" class="btn btn-primary ghost btn-no-margin" value="Conferma" />
		</div>
	</form>
</div>