<footer ng-class="{'col-md-9': search}">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h3>MANGIAEBEVI</h3>
                <ul>
                    <li><a href="/chi-siamo/">Chi siamo</a></li>
                    <li><a href="http://blog.mangiaebevi.it">Blog</a></li>
                    <li><a href="/lavora-con-noi/">Lavora con noi</a></li>
                    <!--<li><a href="/press/">Press</a></li>-->
                    <li><a href="/contattaci/">Contattaci</a></li>
                </ul>

            </div>
            <div class="col-md-3">
                <h3>SCOPRI</h3>
                <ul>
                    <li><a href="/come-funziona/">Come funziona</a></li>
                    <li><a href="/programma-fedelta/">Programma fedeltà</a></li>
                    <!--<li><a href="#">Offerte Speciali</a></li>
                    <li><a href="#">Programma affiliazioni</a></li>-->
                    <li><a href="/termini-e-condizioni/">Termini e Condizioni</a></li>
                    <li><a href="/privacy-policy/">Privacy</a></li>
                </ul>
            </div>

            <div class="col-md-3">
                <h3>PER I RISTORATORI</h3>
                <ul>
                    <li><a href="/promuovi-il-tuo-ristorante/">Promuovi il tuo ristorante</a></li>
                    <li><a href="/aggiungi-ristorante/">Aggiungi un ristorante</a></li>
                    <li><a ng-if="current_user.loggedStatus == 0" ng-click="dialogLogin(1)">
                        Gestisci il tuo ristorante</a>
                    </li>
                    <li><a href="/modifica-ristorante/{{user_footer_restaurants[0].slug}}/"
                        ng-if="current_user.loggedStatus == 1 && current_user.user.role >= 2 && user_footer_restaurants && user_footer_restaurants.length > 0">
                        Gestisci il tuo ristorante</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div ng-if="show_newsletter">
                    <h3>NEWSLETTER</h3>
                    <form ng-submit="subscribe()">
                        <div class="form-group">
                            <!--<label for="newsletter_email">Email</label>-->
                            <input  style="border: none;margin-bottom: 12px" ng-model="newsletter.email" id="newsletter_email" class="form-control" placeholder="Email" />
                            <input  style="border: none;margin-bottom: 12px" ng-model="newsletter.name" id="newsletter_name" class="form-control" placeholder="Il tuo nome" />
                            <button type="submit" class="btn btn-default btn-sm">Iscrivimi</button>
                        </div>
                    </form>
                </div>
                <h3>SEGUICI</h3>
                <div class="social-icon">
                    <a href="https://www.facebook.com/mangiaebeviofficial" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/MangiaeBevi_it" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="https://plus.google.com/+MangiaebeviIt/posts" target="_blank"><i class="fa fa-google-plus"></i> </a>
                    <a href="https://it.pinterest.com/mangiaebevi/" target="_blank"><i class="fa fa-pinterest"></i></a>
                    <a href="https://www.instagram.com/mangiaebevi_it/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.linkedin.com/company/5071934?trk=tyah&trkInfo=tarId%3A1396690958663%2Ctas%3Amangiaebevi%2Cidx%3A1-1-1" target="_blank"><i class="fa fa-linkedin"></i></a>
                    <a href="https://www.youtube.com/channel/UCrusZ14WViKjSZOPUR3YiKQ" target="_blank"><i class="fa fa-youtube-play"></i> </a>
                </div>
            </div>
        </div>
        <p class="info-azienda"><small>&copy; Digikom s.r.l. | P.I. 12452701001</small></p>
    <div>
</footer>