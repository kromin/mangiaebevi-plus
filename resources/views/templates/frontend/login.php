<div class="login_page_bg" style="background-image: url(img/home/bg.jpg);">
	<div class="overlay"></div>
	<div class="login_form_bar">
		<h3>Accedi</h3>
		<p>Inserisci le tue credenziali per accedere</p>
		<form name="loginForm" ng-submit="make_login(login)">
			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" ng-model="login.email" id="email" class="form-control" required/>
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" ng-model="login.password" id="password" class="form-control" required/>
			</div>
			<div class="form-group">
				<input type="submit" ng-disabled="loginForm.$invalid" class="btn btn-block btn-primary login-btn" value="Entra" />
			</div>
		</form>
		<p class="text-danger">{{login_form_error}}</p>
	</div>
	<div class="login_page_content">
		<img class="logo" ng-src="img/logo.png" alt="MEB" title="MangiaeBevi" />
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec orci non elit condimentum rutrum eget ac elit. Suspendisse vestibulum vehicula dui, ut egestas ex elementum dapibus.</p>
	</div>
</div>