<div id="staticPage" 
ng-init="
setSEO('Chi siamo | Mangiaebevi','/chi-siamo/','/img/pages/chi_siamo.jpg','/img/pages/chi_siamo.jpg','','Leggi la storia e l\'evoluzione di MangiaeBevi')"
>
	<section class="bg_image vcenter_parent full-height" back-img img="/img/pages/chi_siamo.jpg">
		<div class="overlay"></div>
		<div class="vcenter_child big_width">
			<h1 class="white-text center">Chi siamo<br /><small>IERI, OGGI, DOMANI</small>
			</h1>			
		</div>
		<div class="go_down" scroll-to="#who">
			<p>
				<i class="fa fa-chevron-down"></i>
			</p>
		</div>
	</section>
	<section id="who">
		<div class="container">
			<div class="col-sm-12 padding-big-v medium_block block_auto">
				<h2>La guida per il palato, <br />la rivista per il gourmet</h2>
				<p>
					<b class="text-primary">Mangia&Bevi</b> da oltre 10 anni racconta la ristorazione di qualità, nel modo più obiettivo e genuino possibile, non fornendo ai lettori recensioni o classifiche, ma indicando e descrivendo i luoghi del mangiar bene, le loro peculiarità e i loro punti di forza.
					<br />Nata nell’ottobre del 2006 come allegato del settimanale “Panorama”, “la guida per il palato” grazie al suo target ha attratto da subito i più importanti ristoranti della Capitale (e oggi d’Italia), come roof-garden, stellati e ristoranti gourmet. Una guida che in 10 anni ha pubblicato quindici edizioni e ha avuto oltre mille ristoranti inserzionisti.

				</p>

				<p>
					Oggi <b class="text-primary">MangiaeBevi</b> volta pagina. Nasce infatti “la rivista per il gourmet”, un magazine-guida rivolto prevalentemente ad un pubblico di buongustai, un movimento in continua crescita, sempre più attento alla qualità del cibo, alla sperimentazione, alle novità dell’offerta ristorativa, al buon bere.
<br />Una rivista con contenuti di interesse nazionale e locale, al tempo stesso: servizi sui ristoranti e sulle nuove aperture, anteprime di eventi e fiere, itinerari enogastronomici, interviste e sfiziose ricette proposte dai migliori chef.
<br />Una rivista con al suo interno una vera e propria guida - che varia a seconda delle edizioni, es. Roma, Milano, Napoli, ecc. - ricca di informazioni sui ristoranti (tipo di cucina, prezzi, servizi e specialità) e con una periodicità trimestrale, che consente di pubblicare notizie sempre fresche e aggiornate.

				</p>


				<h2 class="big-title-inside">Nuovo sito, nuove app</h2>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<img class="img-responsive" ng-src="/img/pages/main_who.png" alt="mangiaebevi" title="" />
				</div>
				<div class="col-sm-6 padding-big-v">
					<p>
						Il processo di “modernizzazione” (nuovo logo, nuova veste grafica) 
						e di “multicanalità” (nuovo sito, nuove applicazioni per iPhone e Android) iniziato nel 2014 fa oggi di <b class="text-primary">MangiaeBevi</b> non più solo 
						una guida “cartacea”, ma anche un’utilissima piattaforma web e mobile, con la geolocalizzazione, 
						che consente all’utente di ricercare un ristorante secondo le proprie esigenze, consultare il menu, 
						visitare il locale con il virtual tour, prenotare un tavolo. 
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 padding-big-v medium_block block_auto">
					<p>
						Si sa che la tecnologia, così come i modelli di design e usabilità del web, si evolvono velocemente, 
						così è stato necessario “ripensare” <b class="text-primary">MangiaeBevi</b> trasformandolo in un utilissimo strumento che raccoglie insieme più 
						funzioni facilitandone l’uso.
<br />Il risultato lo avete sotto gli occhi.

					</p>
				</div>
			</div>
			<!--<div class="row">
				<div class="icon-services">
					<div class="col-sm-4">
						<h3>Visita il locale</h3>
						<i class="fa fa-expand"></i>
					</div>
					<div class="col-sm-4">
						<h3>Consulta il menu</h3>
						<i class="mebi-menu"></i>
					</div>
					<div class="col-sm-4">
						<h3>Prenota</h3>
						<i class="mebi-list"></i>
					</div>
				</div>
			</div>-->
		</div>
	</section>
	
	<!--
	<section class="bg_image" back-img img="img/storia.jpg">		
		<div class="overlay light"></div>
		<div class="container relative">
			<div class="col-sm-12">
				<h2 class="center section-title white-text">Storia</h2>
			</div>			
			<div class="block_auto medium_block padding-big-v white-text">
				<p class="text-center white-text">
<b class="text-primary">Menoo</b> nasce dall’esperienza decennale di <b><a class="text-warning" href="http://mangiaebevi.it/">MangiaeBevi</a></b> - guida dei ristoranti di Roma e provincia allegata al settimanale Panorama – che dal 2006 ad oggi ha pubblicato 15 edizioni e ha avuto oltre 1.000 ristoranti clienti. Una vera e propria istituzione nella Capitale d’Italia. 
				</p>
			</div>
		</div>
		
	</section>
	-->
</div>