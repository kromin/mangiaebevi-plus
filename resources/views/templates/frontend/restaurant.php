<div id="restaurantPage">
    <script mantain type="application/ld+json" ng-bind="ldsContent">
    </script>
    <div class="mainContainer" ng-class="{'active':show_virtual_tour}">
        <div class="virtualTourContainer" ng-if="current_restaurant.virtualTour && current_restaurant.virtualTour != ''">
            <iframe width="100%" height="600" ng-src="{{tourUrl}}" allowfullscreen="" frameborder="0"></iframe>
            <div class="close-tour" ng-if="show_virtual_tour">
                <a class="btn btn-primary ghost dark close-tour-btn" ng-click="showTour()">Chiudi</a>
            </div>
        </div>
        <div ng-if="(!current_restaurant.virtualTour || current_restaurant.virtualTour == '') && (current_restaurant.image && current_restaurant.image != '')" class="imageContainer" back-img img="{{staticURL}}/restaurant/big/{{current_restaurant.image}}.png">
        </div>
        <div ng-if="(!current_restaurant.virtualTour || current_restaurant.virtualTour == '') && (!current_restaurant.image || current_restaurant.image == '')" class="imageContainer" style="background-image: url(/img/home/home_1.jpg)">
        </div>
        <div class="overlay"></div>
        <div class="claim_block" ng-if="!isClient()">
            <div class="container">
                <div class="col-sm-12">
                    <h3>Sei il titolare di questa attività? <a ng-click="claim()"><span class="text-warning">Reclama</span> la tua pagina</a></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="pageContent" ng-class="{'slideDown':show_virtual_tour}">
        <div class="container">
            <div class="col-lg-12">
                <h1 class="page_title">
					{{current_restaurant.name}}
					<small>
						<span ng-if="current_restaurant.tipoLocale.length > 0">{{tipiLocali[current_restaurant.tipoLocale[0]].name}} |</span>
						<span ng-if="current_restaurant.tipoCucina.length > 0"> {{tipiCucine[current_restaurant.tipoCucina[0]].name}} |</span>
						<!--<span ng-if="current_restaurant.regioneCucina.length > 0"> {{regioniCucine[current_restaurant.regioneCucina[0]].name}} |</span>-->
						<span ng-if="current_restaurant.district.length > 0 && current_restaurant.district[0] != ''"> {{current_restaurant.district[0]}} |</span>
						<span ng-repeat="euro in price_euro_add track by $index">{{euro}}</span>
                        <span class="hint--bottom" aria-label="Raccomandazioni" ng-click="scrollToRec()" ng-if="current_restaurant.recommendations > 0"> | <i class="mebi-speaking"></i> {{current_restaurant.recommendations}}</span>
					</small>
				</h1>
            </div>
        </div>
        <div class="pageContentBg">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">

                        <div class="">
                            <div class="col-sm-6">
                                <p ng-if="current_restaurant.address != ''"><b><i class="fa fa-map-marker"></i></b> {{current_restaurant.address}}<span ng-if="current_restaurant.district.length > 0 && current_restaurant.district[0] != ''">, {{current_restaurant.district[0]}}</span></p>
                                <p>
                                    <span class="text-success" ng-if="isOpenRestaurant()">Aperto</span>
                                    <span class="text-danger" ng-if="!isOpenRestaurant()">Chiuso</span>
                                </p>
                                <p ng-if="fasce_oggi.length > 0">Orari di oggi:
                                    <span ng-repeat="fascia_odierna in fasce_oggi track by $index">{{fascia_odierna}} </span></p>
                                <a ng-click="viewHours()">vedi tutti gli orari</a>
                            </div>
                            <div class="col-sm-6">
                                <p ng-if="current_restaurant.contact.phone.length > 0"><b><i class="fa fa-phone"></i></b>
                                    <span ng-repeat="phone in current_restaurant.contact.phone track by $index"> {{phone}} <i ng-if="$index < (current_restaurant.contact.phone.length - 1)">-</i> </span>
                                </p>
                                <p ng-if="current_restaurant.contact.web && current_restaurant.contact.web != ''">
                                    <b><i class="fa fa-globe"></i></b> <a href="{{restaurant_url_website}}" target="_blank" class="web_link">
									{{current_restaurant.view_web}}</a>
                                </p>
                                <div class="restaurant-social">
                                    <a target="_blank" ng-if="current_restaurant.social[0].facebook && current_restaurant.social[0].facebook != ''" href="{{current_restaurant.social[0].facebook}}" class="social-page-link">
                                        <i class="fa fa-facebook fb"></i>
                                    </a>
                                    <a target="_blank" ng-if="current_restaurant.social[1].twitter && current_restaurant.social[1].twitter != ''" href="{{current_restaurant.social[1].twitter}}" class="social-page-link">
                                        <i class="fa fa-twitter tw"></i>
                                    </a>
                                    <a target="_blank" ng-if="current_restaurant.social[2].pinterest && current_restaurant.social[2].pinterest != ''" href="{{current_restaurant.social[2].pinterest}}" class="social-page-link">
                                        <i class="fa fa-pinterest pi"></i>
                                    </a>
                                    <a target="_blank" ng-if="current_restaurant.social[3].instagram && current_restaurant.social[3].instagram && current_restaurant.social[3].instagram != ''" href="{{current_restaurant.social[3].instagram}}" class="social-page-link">
                                        <i class="fa fa-instagram in"></i>
                                    </a>
                                    <a target="_blank" ng-if="current_restaurant.social[4].googlePlus && current_restaurant.social[4].googlePlus != ''" href="{{current_restaurant.social[4].googlePlus}}" class="social-page-link">
                                        <i class="fa fa-google-plus gp"></i>
                                    </a>
                                    <a target="_blank" ng-if="current_restaurant.social[5].linkedin && current_restaurant.social[5].linkedin != ''" href="{{current_restaurant.social[5].linkedin}}" class="social-page-link">
                                        <i class="fa fa-linkedin li"></i>
                                    </a>
                                    <a target="_blank" ng-if="current_restaurant.social[6].youtube && current_restaurant.social[6].youtube != ''" href="{{current_restaurant.social[6].youtube}}" class="social-page-link">
                                        <i class="fa fa-youtube yt"></i>
                                    </a>
                                    <a target="_blank" ng-if="current_restaurant.social[7].tripadvisor && current_restaurant.social[7].tripadvisor != ''" href="{{current_restaurant.social[7].tripadvisor}}" class="social-page-link">
                                        <i class="fa fa-tripadvisor tr"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="reccomendation" set-class-when-at-top="active">

                            <div class="form-content">

                                <div class="cont">
                                    <a class="btn btn-primary ghost dark btn-block" ng-click="favourite()">
                                        <i class="fa fa-heart" ng-class="(current_user.user.favourite && current_user.user.favourite.indexOf(current_restaurant._id) != -1) ? 'fa-heart' : 'fa-heart-o'"></i> Preferiti
                                    </a>
                                    <h3>Raccomanda ad un amico</h3>
                                    <div class="raccomanda_buttons">
                                        <a class="" ng-click="mailReccomendOpenDialog()">Email</a>
                                        <a class="" ng-click="reccommend('facebook_share')">Facebook</a>
                                        <a class="list-container" ng-click="open_drop_menu = !open_drop_menu">Altro...
											<ul class="list-unstyled" ng-class="{'open': open_drop_menu}">
												<li ng-click="reccommend('twitter')">Twitter</li>
												<li ng-click="reccommend('google_plus')">Google plus</li>
												<li ng-click="reccommend('linkedin')">Linkedin</li>
												<li ng-click="reccommend('facebook_send')">Messenger</li>
											</ul>
										</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="clearfix"></div>

                <!--
				mappetta con indicazioni stradali
				claim con modale ng-modal
			-->
            </div>
        </div>
        <!-- identifico il cliente -->
        <div class="grey_content">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">

                        <div class="row linking_btn" ng-if="(current_restaurant.menu.menus && current_restaurant.menu.menus.length > 0) ||
									(current_restaurant.virtualTour && current_restaurant.virtualTour != '') ||
									(current_restaurant.image && current_restaurant.image != '' && current_restaurant.image != null)">
                            <a ng-if="current_restaurant.menu.menus && current_restaurant.menu.menus.length > 0" class="btn btn-primary ghost dark" ng-click="scrollToMenu()">Menu</a>
                            <!--<a ng-if="current_restaurant.menu.menus && current_restaurant.menu.menus.length > 0 && current_restaurant.ordinazione == 1" class="btn btn-primary ghost dark" ng-click="scrollToMenu()">Ordina</a>-->
                            <a ng-if="current_restaurant.virtualTour && current_restaurant.virtualTour != ''" class="btn btn-primary ghost dark" ng-click="showTour()">Tour</a>
                            <a ng-if="current_restaurant.image && current_restaurant.image != '' && current_restaurant.image != null" class="btn btn-primary ghost dark" ng-click="openGallery(0)">Foto</a>
                            <a ng-if="current_restaurant.video && current_restaurant.video != '' " class="btn btn-primary ghost dark" ng-click="goToVideo(0)">Video</a>
                        </div>


                        <!-- se aperto mostra -->
                        <div class="row restaurant_element" ng-if="current_restaurant.description != ''">
                            <div class="col-sm-2">
                                <p class="big_text"><b>Descrizione:</b></p>
                            </div>
                            <div class="col-sm-10">
                                <div class="col-sm-12">
                                    <p class="big_text description" ng-class="{'open':description_opened}" ng-bind-html="current_restaurant.description_html"></p>
                                    <a ng-if="!description_opened" class="open_description" ng-click="open_description()">Leggi di più <i class="fa fa-chevron-down"></i></a>
                                </div>
                            </div>
                        </div>



                        <!-- tipologia -->
                        <div class="row restaurant_element" ng-if="current_restaurant.tipoLocale.length > 0">
                            <div class="col-sm-2">
                                <p class="big_text left_descriptor"><b>Tipologia:</b></p>
                            </div>
                            <div class="col-sm-10">
                                <!--<div class="col-md-3 col-sm-6 col-xs-12" ng-if="current_restaurant.tipoLocale.length > 0">-->
                                <ul class="list-unstyled">
                                    <!--<li class="list-title">Tipo</li>-->
                                    <li class="col-md-3" ng-repeat="tipo_locale in current_restaurant.tipoLocale">{{tipiLocali[tipo_locale].name}}</li>
                                </ul>

                            </div>
                        </div>


                        <!-- cucina -->
                        <div class="row restaurant_element" ng-if="current_restaurant.tipoCucina.length > 0
							|| current_restaurant.regioneCucina.length > 0">
                            <div class="col-sm-2">
                                <p class="big_text left_descriptor"><b>Cucina:</b></p>
                            </div>
                            <div class="col-sm-10">
                                <!--<div class="col-md-3 col-sm-6 col-xs-12" ng-if="current_restaurant.tipoCucina.length > 0">-->
                                <ul class="list-unstyled">
                                    <!--<li class="list-title">Tipo</li>-->
                                    <li class="col-md-3" ng-repeat="tipo_cucina in current_restaurant.tipoCucina">{{tipiCucine[tipo_cucina].name}}</li>
                                    <li class="col-md-3" ng-repeat="regione_cucina in current_restaurant.regioneCucina">{{regioniCucine[regione_cucina].name}}</li>
                                </ul>

                            </div>
                        </div>

                        <!-- staff -->
                        <div class="row restaurant_element" ng-if="
							current_restaurant.staff.chef.length > 0 ||
							current_restaurant.staff.sommelier.length > 0 ||
							current_restaurant.staff.barman.length > 0 ||
							current_restaurant.staff.cameriere.length > 0">
                            <div class="col-sm-2">
                                <p class="big_text left_descriptor"><b>Staff:</b></p>
                            </div>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-3 col-sm-6" ng-if="current_restaurant.staff.chef.length > 0">
                                        <ul class="list-unstyled">
                                            <li class="padded"><b>CHEF</b></li>
                                            <li class="padded" ng-repeat="chef in current_restaurant.staff.chef">{{chef}}</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3 col-sm-6" ng-if="current_restaurant.staff.sommelier.length > 0">
                                        <ul class="list-unstyled">
                                            <li class="padded"><b>SOMMELIER</b></li>
                                            <li class="padded" ng-repeat="sommelier in current_restaurant.staff.sommelier">{{sommelier}}</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3 col-sm-6" ng-if="current_restaurant.staff.barman.length > 0">
                                        <ul class="list-unstyled">
                                            <li class="padded"><b>BARMAN</b></li>
                                            <li class="padded" ng-repeat="barman in current_restaurant.staff.barman">{{barman}}</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3 col-sm-6" ng-if="current_restaurant.staff.maitre.length > 0">
                                        <ul class="list-unstyled">
                                            <li class="padded"><b>MAITRE</b></li>
                                            <li class="padded" ng-repeat="maitre in current_restaurant.staff.maitre">{{maitre}}</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>



                        <!-- servizi -->
                        <div class="row restaurant_element" ng-if="current_restaurant.services.length > 0">
                            <div class="col-sm-2">
                                <p class="big_text left_descriptor"><b>Servizi:</b></p>
                            </div>
                            <div class="col-sm-10">
                                <div class="col-lg-3 col-md-3 col-sm-6 single_service centered_icon_container" ng-repeat="service in current_restaurant.services">
                                    <i class="{{services[service].icon}}" ng-class="{'big': services[service].icon != 'fa fa-beer' }"></i>
                                    <span>{{services[service].name}}</span>
                                </div>
                            </div>
                        </div>


                        <div class="row restaurant_element" ng-if="current_restaurant.carte.length > 0">
                            <div class="col-sm-2">
                                <p class="big_text left_descriptor"><b>Carte accettate:</b></p>
                            </div>
                            <div class="col-sm-10 cards">
                                <ul class="list-unstyled">
                                    <li class="col-md-3 col-sm-6 single_card centered_icon_container" ng-repeat="card in current_restaurant.carte">
                                        <i class="{{creditCard[card].icon}}"></i>
                                        <span>{{creditCard[card].name}}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>


                    </div>


                    <!-- sidebar -->
                    <div class="col-md-4">
                        <!-- prenotazione -->
                        <div class="reservation-form" set-class-when-at-top="active"
                            ng-if="(current_restaurant.user && current_restaurant.user != 0)
                            || (current_restaurant.contact.email && current_restaurant.contact.email != '')">
                            <div class="reservation-header">
                                Prenota un tavolo
                            </div>

                            <div class="form-content" ng-if="!reservation_as_to_add_phone">

                                <div class="cont">
                                    <form class="form-inline">

                                        <div class="form-group">
                                            <input type="text" id="datetimepicker-day" class="form-control semi-date" placeholder="Data" />
                                            <select ng-model="reservation.hour">
                                                <option ng-repeat="fascia_oraria in avaible_fasce" value="{{fascia_oraria}}">{{fascia_oraria}}</option>
                                            </select>
                                            <select ng-model="reservation.people">
                                                <option ng-repeat="person in people" value="{{person}}">{{person}}</option>
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                    </form>

                                    <div class="send_button" ng-if="!reservation_confirmed">
                                        <a class="btn btn-primary ghost dark btn-block disabled" id="reservation_btn" ng-click="reserve()">Prenota</a>
                                    </div>
                                    <p style="margin-top: 11px;" ng-if="reservation_confirmed">{{reservation_confirmed_text}}</p>


                                </div>
                            </div>

                            <div class="form-content" ng-if="reservation_as_to_add_phone">
                                <div class="cont">
                                    <form>
                                        <div class="form-group">
                                            <label>Numero di telefono</label>
                                            <input class="form-control" type="text" ng-model="user_phone.phone" />
                                        </div>
                                        <div class="form-group">
                                            <a class="btn btn-primary btn-block ghost" ng-click="addPhoneNumber()">
												Conferma
											</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- reservation -->
                        <div class="wishlist_recommend text-center">
                            <span class="hint--bottom" aria-label="Aggiungi/rimuovi da ristoranti da provare">
                                <i class="mebi-favorite"
                                ng-class="(current_user.user.wishlist && current_user.user.wishlist.indexOf(current_restaurant._id) != -1) ? 'text-success' : 'text-danger'"
                                ng-click="wishlist()"></i>
                            </span>
                            <span class="hint--bottom" aria-label="Raccomanda">
                                <i class="mebi-speaking"
                                ng-click="recommendRestaurant()"
                                ></i>
                            </span>
                            <div class="clearfix"></div>
                            <a ng-click="segnalaProblema()">Segnala un errore nella scheda del ristorante</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

            </div>
        </div>

        <div class="white_content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <!-- blocco menu -->
                        <div class="menu_container" id="menuContainer" ng-if="current_restaurant.menu.menus && current_restaurant.menu.menus.length > 0">
                            <ul class="list-unstyled menu-titles">
                                <li ng-repeat="menu in current_restaurant.menu.menus track by $index" ng-click="selectMenu($index)" ng-if="menu.categorie.length > 0" ng-class="{'active':currentMenu==$index}">
                                    {{menu.name}}
                                </li>
                            </ul>

                            <ul class="list-unstyled menu_tipe_selection_ul" ng-if="visual_menu == 1">
                                <li ng-click="setMenuType(0)" ng-class="{'active':menu_type==0}"><i class="fa fa fa-list"></i> testuale</li>
                                <li ng-click="setMenuType(1)" ng-class="{'active':menu_type==1}"><i class="fa fa fa-picture-o"></i> visuale</li>
                            </ul>

                            <!-- text menu -->
                            <div class="menu category" ng-repeat="(cIndex, categoria)  in current_restaurant.menu.menus[currentMenu].categorie track by $index" ng-class="{'no-border-bottom' : $index+1==current_restaurant.menu.menus[currentMenu].categorie.length}" ng-if="menu_type==0">
                                <h4 class="col-sm-12 categoria-name">{{categoria.name}}</h4>
                                <div class="row small_bottom_margin" ng-repeat="plate in categoria.piatti track by $index" ng-if="$index%2==0">
                                    <div class="col-sm-6 single_plate" ng-click="addPlateToOrder(cIndex,$index)">
                                        <!--categoria.piatti[$index]-->
                                        <div class="left">
                                            <h5><a href="/piatto/{{plate.id}}/">{{categoria.piatti[$index].name}}</a></h5>
                                            <p class="ingredients" ng-if="categoria.piatti[$index].ingredienti && categoria.piatti[$index].ingredienti.length > 0">
                                                <span><b>Ingredienti:</b> </span> <i ng-repeat="ingrediente in categoria.piatti[$index].ingredienti">{{ingrediente}} </i>
                                            </p>
                                            <p class="ingredients" ng-if="categoria.piatti[$index].allergeni && categoria.piatti[$index].allergeni.length > 0">
                                                <span><b>Allergeni:</b> </span> <i ng-repeat="allergene in categoria.piatti[$index].allergeni">{{allergene}} </i>
                                            </p>
                                            <p class="desc">{{categoria.piatti[$index].description}}</p>

                                        </div>
                                        <div class="right">
                                            <p class="price pull-right">{{categoria.piatti[$index].price | number : 2}} €</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-sm-6 single_plate" ng-click="addPlateToOrder(cIndex,$index+1)" ng-if="categoria.piatti.length > ($index + 1)">
                                        <div class="left">
                                            <h5><a href="/piatto/{{plate.id}}/">{{categoria.piatti[$index+1].name}}</a></h5>
                                            <p class="ingredients" ng-if="categoria.piatti[$index+1].ingredienti && categoria.piatti[$index+1].ingredienti.length > 0">
                                                <span><b>Ingredienti:</b> </span> <i ng-repeat="ingrediente in categoria.piatti[$index+1].ingredienti">{{ingrediente}} </i>
                                            </p>
                                            <p class="ingredients" ng-if="categoria.piatti[$index+1].allergeni && categoria.piatti[$index+1].allergeni.length > 0">
                                                <span><b>Allergeni:</b> </span> <i ng-repeat="allergene in categoria.piatti[$index+1].allergeni">{{allergene}} </i>
                                            </p>
                                            <p class="desc">{{categoria.piatti[$index+1].description}}</p>

                                        </div>
                                        <div class="right">
                                            <p class="price pull-right">{{categoria.piatti[$index+1].price | number : 2}} €</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- visual menu -->
                            <div class="visual_menu category" ng-repeat="(cIndex, categoria)  in current_restaurant.menu.menus[currentMenu].categorie track by $index" ng-class="{'no-border-bottom' : $index+1==current_restaurant.menu.menus[currentMenu].categorie.length}" ng-if="menu_type==1">
                                <h4 class="col-sm-12 categoria-name">{{categoria.name}}</h4>
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 small_pad" ng-click="addPlateToOrder(cIndex,$index)"
                                    ng-repeat="plate in categoria.piatti track by $index" ng-if="plate.image && plate.image != ''">
                                        <!--categoria.piatti[$index]-->
                                        <div class="single_plate">
                                            <div class="plate_image">
                                                <img ng-src="{{staticURL}}/menu/small/{{plate.image}}.png" ng-if="plate.image && plate.image != ''" alt="" title="" />
                                            </div>
                                            <div class="text">
                                                <h5><a href="/piatto/{{plate.id}}/">{{plate.name}}</a></h5>
                                                <div class="price">
                                                    {{categoria.piatti[$index].price | number : 2}} €

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="restaurant_video" ng-if="current_restaurant.video && current_restaurant.video != ''">
                            <hr />
                            <h3>Video</h3>

                            <div class="embed-responsive only_this embed-responsive-16by9">
                              <iframe class="embed-responsive-item" ng-src="{{video_url}}"></iframe>
                            </div>
                        </div>

                        <!-- blocco gallery -->
                        <div ng-if="current_restaurant.images.length > 1">
                            <hr />
                            <h3>Foto del ristorante</h3>
                            <div class="restaurant_gallery">

                                <!-- 2 immagini -->
                                <div ng-if="current_restaurant.images.length == 2">
                                    <div>
                                        <div class="col-sm-6" ng-repeat="img in current_restaurant.images track by $index">
                                            <div class="box_images qt" back-img img="{{staticURL}}/restaurant/small/{{img}}.png" ng-click="openGallery($index)">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- 3 immagini -->
                                <div ng-if="current_restaurant.images.length == 3">
                                    <div class="col-sm-4" ng-repeat="img in current_restaurant.images track by $index">
                                        <div class="box_images sn" back-img img="{{staticURL}}/restaurant/small/{{img}}.png" ng-click="openGallery($index)">
                                        </div>
                                    </div>
                                </div>
                                <!-- 4 immagini -->
                                <div ng-if="current_restaurant.images.length == 4">
                                    <div>
                                        <div class="col-sm-3" ng-repeat="img in current_restaurant.images track by $index">
                                            <div class="box_images sn" back-img img="{{staticURL}}/restaurant/small/{{img}}.png" ng-click="openGallery($index)">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- 5 immagini -->
                                <div ng-if="current_restaurant.images.length > 4">
                                    <div class="right-border">
                                        <div class="col-sm-6" ng-repeat="img in current_restaurant.images track by $index" ng-if="$index < 2">
                                            <div class="box_images qt" back-img img="{{staticURL}}/restaurant/small/{{img}}.png" ng-click="openGallery($index)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="right-padding">
                                        <div class="col-sm-4" ng-repeat="img in current_restaurant.images track by $index" ng-if="$index > 1 && $index < 5">
                                            <div class="box_images sn" back-img img="{{staticURL}}/restaurant/small/{{img}}.png" ng-click="openGallery($index)">
                                                <div class="overlay" ng-if="current_restaurant.images.length > 5 && $index == 4">
                                                    <p>+ {{current_restaurant.images.length -5}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <hr />
                        </div>

                        <div ng-if="current_restaurant.recommendations > 0">
                            <div class="restaurant_recommendations" ng-init="loadRestaurantRecommendations()" id="recContainer">
                                <h3>Raccomandazioni</h3>
                                <ul class="list-unstyled imaging_list">
                                    <li ng-repeat="recommendation in restaurant_rec track by $index">
                                        <div class="clearfix"></div>
                                        <div class="col-lg-2 col-md-2 col-xs-3">
                                            <img class="img-circle"
                                            ng-src="{{staticURL}}/users/square/{{recommendation['user_obj'].avatar}}.png" alt="" title=""
                                            ng-if="recommendation['user_obj'].avatar && recommendation['user_obj'].avatar != ''" />
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-xs-9">
                                            <h3><a href="/u/{{recommendation['user_obj']._id}}/">{{recommendation['user_obj'].name}} {{recommendation['user_obj'].surname}}</a></h3>
                                            <p>{{recommendation.text}}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                </ul>
                                <p class="text-center">
                                    <button class="btn ghost" ng-click="loadRecommendation()"
                                    ng-if="!stop_load_rec_pagination"
                                        role="button">Altre</button>
                                </p>
                            </div>
                            <hr />
                        </div>
                        <div class="clearfix"></div>
                        <div ng-controller="MapCtrl">
                            <div class="directions">
                                <h3>Calcola il percorso</h3>
                                <form class="form-inline" style="margin-bottom: 15px;">
                                    <div class="col-sm-6 no-pad">
                                        <input ng-model="start_direction_address" type="text" class="form-control" placeholder="Da dove vuoi partire" ng-enter="calculateDirections()" />
                                    </div>
                                    <div class="col-sm-6 small_marginated_btn">
                                        <a class="btn btn-primary btn-sm ghost" style="font-size: 13px;" ng-click="calculateDirections()">Calcola</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                                <div class="clearfix"></div>
                                <!--<pre>{{directions_texts|json}}</pre>-->
                                <div ng-if="directions_texts != ''" class="col-lg-12">
                                    <p ng-repeat="text in directions_texts" ng-bind-html="text" class="directions_return"></p>
                                </div>
                            </div>
                            <div id="google_map"></div>
                        </div>

                        <div class="correlati" ng-if="related && related.length > 0">
                            <h3>Potresti essere interessato anche a:</h3>
                            <div class="grid_view related">
                                <div class="col-sm-4 restaurant-grid-container" ng-repeat="restaurant in related">
                                    <a href="/attivita/{{restaurant.obj.slug}}">
                                        <div class="restaurant" back-img img="{{staticURL}}/restaurant/small/{{restaurant.obj.image}}.png">
                                            <div class="overlay"></div>
                                            <div class="text">
                                                <h3>{{restaurant.obj.name}}</h3>
                                                <span class="type">{{tipiLocali[restaurant.obj.tipoLocale[0]].name}}</span>
                                                <span class="address">{{restaurant.obj.address}}</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="image_gallery" ng-class="{'active':gallery_opened}">
        <div class="overlay"></div>
        <div class="image_active_container">
            <img galleryimageonload="active" ng-src="{{staticURL}}/restaurant/big/{{current_restaurant.images[current_image_index]}}.png" alt="" title="" />
        </div>
        <div class="controls">
            <a ng-click="closeGallery()" class="close_gallery_icon"><i class="mebi-close"></i></a>
            <a ng-click="prevImage()" class="left"><i class="mebi-left-open-big"></i></a>
            <a ng-click="nextImage()" class="right"><i class="mebi-right-open-big"></i></a>
        </div>
    </div>
</div>
<div class="clearfix"></div>