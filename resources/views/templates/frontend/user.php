<div id="user-page" class="editing_profile_restaurant" ng-init="loadUser()">
	
	<div class="main_image relative" back-img img="{{cover_image}}" ng-if="cover_image">
		<div class="overlay"></div>
		<div class="main_container">
			<button class="custom_btn round change_cover pointer" ng-if="itsme">				
				<span class="hint--right" aria-label="Cambia immagine di copertina">				
					<i class="mebi-technology-1"></i>
				</span>
				<input class="input_hidden_upload pointer" 
				type="file" 
				ngf-select 
				ng-model="change_cover.cover" name="change_cover" ng-change="uploadCover()" title="Cambia" />
			</button>		
			<!--mebi-technology-1-->	
			<div class="main_section_menu">
				<div class="container user_info_container">
					<div class="row">
						<div class="col-sm-5 user_location default text-center" ng-if="itsme">
							<p ng-if="view_user.address">{{view_user.address}} 								
							</p>
							<p ng-if="!view_user.address">
								Completa il tuo profilo per guadagnare foodcoins
							</p>
						</div>
						<div class="col-sm-2 text-center" ng-if="itsme">
							<span class="hint--bottom" aria-label="Inserisci/Modifica la tua posizione attuale" 
							ng-click="setEditAddress()">
								<button class="custom_btn round">								
									<i class="mebi-placeholder-on-map-paper-in-perspective"></i>
								</button>
							</span>
						</div>
						<div class="col-sm-5 user_location current text-center" ng-class="{'col-sm-12': !itsme}">
							<p ng-if="view_user.current_address && !editing_address">{{view_user.current_address}}</p>							
							<span ng-if="itsme && editing_address">
							<input type="text" name="confirm_address" class="activated_input full" ng-model="view_user.current_address"
									ng-autocomplete 
									details="details"
									class="form-control" 
									placeholder="Indirizzo" 
									style="color: #fff;" />
							</span>
							<span class="hint--bottom pull-right" aria-label="Conferma" ng-if="itsme && editing_address" ng-click="editAddress()">
								<button class="custom_btn round" style="top: -47px;">								
									<i class="mebi-android-add"></i>
								</button>
							</span>
						</div>
					</div>
					<div class="row user_profile_main_images">
						<div class="col-sm-3">
							<div class="followers rounded_num">
								<span class="number">
									{{view_user.followedby.length}}									
								<span>
								<i class="mebi-android-done pointer" ng-click="addRemoveFollowing()" ng-if="isFollowing() == 0"></i>
								<i class="mebi-close pointer" ng-click="addRemoveFollowing()" ng-if="isFollowing() == 1"
								style="line-height: 110px; font-size: 36px;"></i>
							</div>
							<p class="text-center">Followers</p>
						</div>
						<div class="col-sm-6">
							<img class="profile_image img-circle" ng-src="{{staticURL}}/users/medium/{{view_user.avatar}}.png" ng-if="view_user.avatar && view_user.avatar != ''" />
							<h1 class="profile_name">{{view_user.name}} {{view_user.surname}}</h1>
							<p class="text-center">{{view_user.description}}</p>
							<p class="friend_container margin_big_v text-center">
								<hr />
								<p class="text-center" ng-if="view_user.friends.length !=  1">{{view_user.friends.length}} AMICI</p>
								<p class="text-center" ng-if="view_user.friends.length ==  1">{{view_user.friends.length}} AMICO</p>
								<button ng-if="canAddToFriends() == 4" 
								ng-click="addToFriends()"
								class="custom_btn outline text-center" 
								style="margin: 5px auto; display: inherit;">
								<i class="mebi-android-add"></i> 
									<span>Aggiungi agli</span>
									<!--<span>Rimuovi dagli</span>--> 
									amici</button>
								
							</p>
						</div>
						<div class="col-sm-3">
							<div class="following rounded_num">
								<span class="number">
									{{view_user.following.length}}
								</span>
							</div>
							<p class="text-center">Following</p>
						</div>
					</div>
					<div class="row"></div>
				</div>
			</div>
		</div>
	</div>
		
	<!-- edit user -->
	<div>
		
		<div class="white_container">
			<div class="container">
				<div class="rounded_num warning">
					<span class="number warning">
						{{view_user.foodcoin}}									
					<span>
				</div>
				<h3 class="text-center">Foodcoin</h3>
				<div class="profile_tabs">
					<ul class="list-unstyled">
						<li ng-class="{'active' : !current_tab || current_tab == 0}"><a ng-click="setTab(0)"><i class="mebi-right-menu-bars"></i></a></li><!-- se sono io mostra le cose degli amici, sennò le mie -->
						<li ng-class="{'active' : current_tab == 4}"><a ng-click="setTab(4)"><i class="mebi-favorite"></i></a></li>
						<li ng-class="{'active' : current_tab == 1}"><a ng-click="setTab(1)"><i class="mebi-speaking"></i></a></li>
						<li ng-class="{'active' : current_tab == 2}"><a ng-click="setTab(2)" ><i class="mebi-add-contact"></i></a></li><!-- mostra azioni degli amici insieme a elenco -->
						<li ng-class="{'active' : current_tab == 3}"><a ng-click="setTab(3)"><i class="mebi-photo-camera"></i></a></li>
						<li ng-class="{'active' : current_tab == 5}" ng-if="itsme"><a ng-click="setTab(5)" ><i class="mebi-list"></i></a></li>
						<li ng-class="{'active' : current_tab == 6}" ng-if="itsme"><a ng-click="setTab(6)" ><i class="mebi-alarm"></i></a></li>
						<li ng-class="{'active' : current_tab == 7}" ng-if="itsme"><a ng-click="setTab(7)" ><i class="mebi-avatar"></i></a></li>
						<li ng-class="{'active' : current_tab == 8}" ng-if="itsme && view_user.role >= 2"><a ng-click="setTab(8)" ><i class="mebi-romantic-date"></i></a></li>
						<!-- se sono io mostra le mie e degli amici, sennò mostra quelle mie -->
					</ul>
				</div>

				<div ng-if="(current_tab == 0 || !current_tab) && view_user && view_user._id">
					<div class="row">
						<div class="col-sm-8 col-sm-push-2">
							<h2 class="text-center">Bacheca</h2>							
							<ul class="list-unstyled imaging_list" ng-init="initDashboard()">
		                        <li ng-repeat="action in user_dashboard track by $index">
		                        	
		                        	<div ng-if="itsme"><!-- utente solo se la sto vedendo io -->
		                        		<img class="" 
		                                ng-src="{{staticURL}}/users/square/{{action.user_obj.avatar}}.png" alt="" title="" 
		                                ng-if="action.user_obj.avatar && action.user_obj.avatar != ''" />
		                                <span>{{action.user_obj.name}} {{action.user_obj.surname}}</span>
		                                <div class="clearfix"></div>
		                                <hr />
		                        	</div>


		                        	<div ng-if="!action.type || action.type == 0">
		                        		<p>Ha stretto amicizia con <a href="/u/{{action.useradd_obj._id}}/">
		                        			{{action.useradd_obj.name}} {{action.useradd_obj.surname}}
		                        		</a></p>
		                        	</div>
		                        	<div ng-if="action.type == 5">
		                        		<p>Ha iniziato a seguire <a href="/u/{{action.useradd_obj._id}}/">
		                        			{{action.useradd_obj.name}} {{action.useradd_obj.surname}}
		                        		</a></p>
		                        	</div>
		                        	<!-- / amicizia e following -->

		                        	<!-- Raccomandazioni -->
		                        	<div ng-if="action.type == 1">
		                        		<div class="col-lg-12">
											<img class="full" 
											ng-src="{{staticURL}}/restaurant/medium/{{action.restaurant_obj.image}}.png" alt="" title="" 
											ng-if="action.restaurant_obj.image && action.restaurant_obj.image != ''" />
										</div>
										<div class="col-lg-12">
											<p ng-if="recommendation.type == 0 || !recommendation.type">
												Ha raccomandato il ristorante <a href="/attivita/{{action.restaurant_obj.slug}}/">{{action.restaurant_obj.name}}
												</a>
											<br />
												<span class="list_span active">Ristorante</span>
											</p>
										</div>
		                        	</div>
		                        	<!-- / ristorante -->

		                        	<div ng-if="action.type == 2">
		                        		<div class="col-lg-12">
											<img class="full" 
											ng-src="{{staticURL}}/menu/medium/{{action.plate_obj.image}}.png" alt="" title="" 
											ng-if="action.plate_obj.image && action.plate_obj.image != ''" />
										</div>
										<div class="col-lg-12">
											<p ng-if="recommendation.type == 0 || !recommendation.type">
												Ha raccomandato il piatto <a href="/attivita/{{action.plate_obj._id}}/">{{action.plate_obj.name}}
												</a>
											<br />
												<span class="list_span active">Piatto</span>
											</p>
										</div>
		                        	</div>
		                        	<!-- / piatto -->

		                        	<div ng-if="action.type == 3">
		                        		<div>
			                        		<div class="col-lg-2 col-md-2 col-xs-3">
												<img class="img-circle" 
												ng-src="{{staticURL}}/restaurant/square/{{action.restaurant_obj.image}}.png" alt="" title="" 
												ng-if="action.restaurant_obj.image && action.restaurant_obj.image != ''" />
											</div>
											<div class="col-lg-10 col-md-10 col-xs-9">
												<p>
													Ha aggiunto il ristorante <a href="/attivita/{{action.restaurant_obj.slug}}/">{{action.restaurant_obj.name}}
													</a> ai preferiti
												</p>
											</div>
			                        	</div>
		                        	</div>
		                        	<!-- / preferiti -->


		                        	<!-- plate image -->
		                        	<div ng-if="action.type == 4">
		                        		<div class="clearfix"></div>
		                        		<div class="col-lg-12">
			                                <img class="full" 
			                                ng-src="{{staticURL}}/community/medium/{{action.plateimage_obj.image}}.png" alt="" title="" 
			                                ng-if="action.plateimage_obj.image && action.plateimage_obj.image != ''"
			                                />
			                                <img class="user_image_minus_top" 
			                                ng-src="{{staticURL}}/menu/medium/{{action.plate_obj.image}}.png" alt="" title="" 
			                                ng-if="action.plate_obj.image && action.plate_obj.image != ''" />
			                            </div>

			                            <div class="clearfix"></div>		                            

			                            <div class="user_plate_image_info">
			                                <h3>
			                                    <a href="/piatto/{{action.plate_obj._id}}/">{{action.plate_obj.name}}</a>
			                                </h3>
			                                    <p>{{action.plateimage_obj.text}}</p>
			                                    <p class="text-center rating_p">{{action.plateimage_obj.rating}}</p>                                            
			                            </div>                                  
			                            <div class="clearfix"></div>
		                        	</div>
		                        	<!-- / plate image -->

		                            <div class="clearfix"></div>
		                        </li>
		                    </ul>
						</div>	
						<div class="clearfix"></div>
		                <p class="text-center">
		                    <button class="outline_btn bprimary profile-readMore" ng-click="loadDashboard()" 
		                    ng-if="!stop_load_dashboard_pagination"
		                        role="button">Altre notizie</button>   
		                </p>					
					</div>
				</div>

				<div ng-if="current_tab == 4" ng-init="initFavourite()">
					<div class="row">
						<div class="col-sm-6" ng-class="{'col-sm-push-3': !itsme && !isFriend()}">
							<h3 class="text-center">Preferiti</h3>
							<div ng-if="itsme">
								<p>Organizza i tuoi ristoranti preferiti in liste</p>
								<div class="form-group">
		                            <label>Nome lista</label><br />
		                            <input class="activated_input full" type="text" ng-model="add_list.name" required/>
		                        </div>
		                        <button ng-click="addList(add_list.name)" class="outline_btn bprimary">
			                      	<span>Crea</span>
			                    </button> 
			                </div>
							<div class="minimal_tabs">
								<ul class="list-unstyled">
									<li ng-class="{'active' : current_list_val == list.name}" 
									ng-repeat="list in view_user.favourite_list"
									><a ng-click="setList(list.name)">{{list.name}}</a></li>
									<li ng-class="{'active' : current_list_val == 0}"><a ng-click="setList(0)">Tutti</a></li>								
								</ul>
							</div>
							<div class="" ng-if="itsme && current_list_val && current_list_val != 0">
							<button ng-click="removeList(current_list_val)" class="outline_btn bdanger">
			                      	<span>Cancella la lista</span>
			                    </button>
			               	</div>
							<ul class="list-unstyled imaging_list">
								<li ng-repeat="restaurant in current_list.favourite_restaurants">
									<div class="list_action">
										<span class="hint--bottom pull-right" 
										aria-label="Rimuovi dai preferiti" 
										ng-if="itsme" ng-click="removeFavourite(restaurant)">
											<i class="mebi-close"></i>
										</span>
									</div>
									<div class="col-lg-2 col-md-2 col-xs-3">
										<img class="img-circle" 
										ng-src="{{staticURL}}/restaurant/square/{{returnRestaurant(restaurant).image}}.png" alt="" title="" 
										ng-if="returnRestaurant(restaurant).image && returnRestaurant(restaurant).image != ''" />
									</div>
									<div class="col-lg-10 col-md-10 col-xs-9">
										<h3><a href="/attivita/{{returnRestaurant(restaurant).slug}}/">{{returnRestaurant(restaurant).name}}</a></h3>
										<p><i class="mebi-placeholder-on-map-paper-in-perspective"></i> {{returnRestaurant(restaurant).address}}</p>
										<p ng-if="itsme">
											<span class="list_span" ng-repeat="list in view_user.favourite_list track by $index" 
											ng-class="{'active': isInList(list.restaurants,restaurant)}" 
											ng-click="addToList(restaurant,list.name)">{{list.name}}</span>
										</p>
									</div>									
									<div class="clearfix"></div>
								</li>
							</ul>
						</div>
						<div class="col-sm-6" ng-if="isFriend() || itsme">
							<h3 class="text-center">Da provare</h3>
							<ul class="list-unstyled imaging_list">
								<li ng-repeat="restaurant in view_user.wishlist_restaurants">
									<div class="list_action">
										<span class="hint--bottom pull-right" 
										aria-label="Ristorante provato" 
										ng-if="itsme" ng-click="setProvato(restaurant._id)">
											<i class="mebi-check-1" style="font-size: 30px;"></i>
										</span>
									</div>
									<div class="col-lg-2 col-md-2 col-xs-3">
										<img class="img-circle" ng-src="{{staticURL}}/restaurant/square/{{restaurant.image}}.png" alt="" title="" ng-if="restaurant.image && restaurant.image != ''" />
									</div>
									<div class="col-lg-8 col-md-8 col-xs-7">
										<h3><a href="/attivita/{{restaurant.slug}}/">{{restaurant.name}}</a></h3>
										<p><i class="mebi-placeholder-on-map-paper-in-perspective"></i> {{restaurant.address}}</p>
									</div>
									<div class="clearfix"></div>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div ng-if="current_tab == 1">
					<div class="row" ng-init="initRecommendations()">
						<h2 class="text-center">Raccomandazioni</h2>
						
						<div class="minimal_tabs" ng-if="itsme">
							<ul class="list-unstyled">
								<li ng-class="{'active' : !recommendation_tab || recommendation_tab == 0}"><a ng-click="setRecommendationTab(0)">Community</a></li>
								<li ng-class="{'active' : recommendation_tab == 1}"><a ng-click="setRecommendationTab(1)">Le tue</a></li>
								<li ng-class="{'active' : recommendation_tab == 2}"><a ng-click="setRecommendationTab(2)">Richiedi</a></li>
							</ul>
						</div>
						<div class="clearfix"></div>
						
						<div ng-if="(!recommendation_tab || recommendation_tab == 0) && itsme">
							<div class="col-sm-6">
								<h3 class="text-center">Raccomandazioni</h3>
								<ul class="list-unstyled imaging_list">
									<li ng-repeat="recommendation in community_rec track by $index" ng-if="recommendation.ref_obj">
										<div class="clearfix"></div>
										<div class="col-lg-2 col-md-2 col-xs-3">
											<img class="img-circle" 
											ng-src="{{staticURL}}/restaurant/square/{{recommendation.ref_obj.image}}.png" alt="" title="" 
											ng-if="recommendation.ref_obj.image && recommendation.ref_obj.image != '' && (recommendation.type == 0 || !recommendation.type)" />
											<img class="img-circle" 
											ng-src="{{staticURL}}/menu/square/{{recommendation.ref_obj.image}}.png" alt="" title="" 
											ng-if="recommendation.ref_obj.image && recommendation.ref_obj.image != '' && recommendation.type == 1" />
											<img class="img-circle" 
											ng-src="{{staticURL}}/users/square/{{recommendation.user_obj.avatar}}.png" alt="" title="" 
											ng-if="!recommendation.ref_obj.image && recommendation.ref_obj.image != '' && recommendation.user_obj.avatar && recommendation.user_obj.avatar != ''" />
										</div>
										<div class="col-lg-10 col-md-10 col-xs-9">
											<h3><a href="/u/{{recommendation.user_obj._id}}/">{{recommendation.user_obj.name}} {{recommendation.user_obj.surname}}</a></h3>
											<p ng-if="recommendation.type == 0 || !recommendation.type">
												Ha raccomandato il ristorante <a href="/attivita/{{recommendation.ref_obj.slug}}/">{{recommendation.ref_obj.name}}
												</a>
											</p>
											<p ng-if="recommendation.type == 1">
												Ha raccomandato il piatto <a href="/piatto/{{recommendation.ref_obj._id}}/">{{recommendation.ref_obj.name}}
												</a>
											</p>
											<hr />
											<p>{{recommendation.text}}</p>
											<p>
												<span class="list_span active" ng-if="recommendation.type == 0 || !recommendation.type">Ristorante</span>
												<span class="list_span active" ng-if="recommendation.type == 1">Piatto</span>
											</p>
										</div>									
										<div class="clearfix"></div>
									</li>
								</ul>
								<p class="text-center">
					                <button class="outline_btn bprimary profile-readMore" ng-click="loadRecommendation('loadLeft')" 
					                ng-if="!stop_left_pagination"
					                    role="button">Altre</button>   
					            </p>
							</div>
							<div class="col-sm-6">
								<h3 class="text-center">Richieste</h3>
								<ul class="list-unstyled imaging_list">
									<li ng-repeat="recommendation in community_req track by $index">
										<div class="col-lg-2 col-md-2 col-xs-3">
											<img class="img-circle" 
											ng-src="{{staticURL}}/users/square/{{recommendation.user_obj.avatar}}.png" alt="" title="" 
											ng-if="recommendation.user_obj.avatar && recommendation.user_obj.avatar != ''" />
										</div>
										<div class="col-lg-10 col-md-10 col-xs-9">
											<h3><a href="/u/{{recommendation.user_obj._id}}/">{{recommendation.user_obj.name}} {{recommendation.user_obj.surname}}</a></h3>
											<p>{{recommendation.text}}</p>
											<p>
												<span class="list_span active" ng-if="recommendation.type == 0">Ristorante</span>
												<span class="list_span active" ng-if="recommendation.type == 1">Piatto</span>
											</p>


											<div class="recommendation_answers">
												<p class="info_and_button">
													<a ng-click="openAnswers('community_req',$index,recommendation._id)">
														<i class="fa fa-comments"></i>{{recommendation.answers_num}}
													</a>
													 | <a ng-click="pushAnswer('community_req',$index,recommendation._id)">
													 Consiglia un 
													 <span ng-if="recommendation.type==0">Ristorante</span>
													 <span ng-if="recommendation.type==1">Piatto</span>
													</a>
												</p>
												


												<!-- steps per rispondere alle raccomandazioni -->
												<div class="publish_recommendation_request" ng-if="isAskingRequestFor(recommendation._id)">

													<div class="answer_info_block"
														ng-if="(current_answering.restaurant || current_answering.restaurant == 0) && current_answering.plate != 0 && !current_answering.plate">
														<div class="col-sm-3" 
														ng-if="answer_restaurants[current_answering.restaurant].image && answer_restaurants[current_answering.restaurant].image != ''">
															<img
															ng-src="{{staticURL}}/restaurant/square/{{answer_restaurants[current_answering.restaurant].image}}.png" 
															class="img-circle img-repsonsive"
															/>
														</div>
														<div class="col-sm-12">
															<h3>{{answer_restaurants[current_answering.restaurant].name}}</h3>
															<p>{{answer_restaurants[current_answering.restaurant].address}}</p>
														</div>	
														<div class="clearfix"></div>
													</div>

													<div  class="answer_info_block" ng-if="current_answering.plate || current_answering.plate == 0">
														<div class="col-sm-3" ng-if="answer_plates[current_answering.plate].image && answer_plates[current_answering.plate].image != ''">
															<img
															ng-src="{{staticURL}}/menu/square/{{answer_plates[current_answering.plate].image}}.png" 
															class="img-circle img-repsonsive"
															/>
														</div>
														<div class="col-sm-12">
															<h3>{{answer_plates[current_answering.plate].name}}</h3>
														</div>	
														<div class="clearfix"></div>
													</div>



													<div ng-if="answer_step_block == 1">
														<div class="form-group">
															<label>Ristorante</label><br />
															<input class="activated_input full" 
															ng-model="answer_recommendation_restaurant_input.name" name="ristoname" />
														</div>
														<button class="outline_btn bprimary" 
														ng-click="searchRestaurantForAnswer(answer_recommendation_restaurant_input.name)" 
														>Cerca</button>
														<div class="restaurants_answer_list">
															<ul class="list-unstyled">
																<li ng-repeat="restaurant in answer_restaurants track by $index"
																ng-click="selectRestaurant($index)"
																>
																	<div class="col-sm-3" ng-if="restaurant.image && restaurant.image != ''">
																		<img
																		ng-src="{{staticURL}}/restaurant/square/{{restaurant.image}}.png" 
																		class="img-circle img-repsonsive"
																		/>
																	</div>
																	<div class="col-sm-12">
																		<h3>{{restaurant.name}}</h3>
																		<p>{{restaurant.address}}</p>
																	</div>																	
																<li>
															</ul>
														</div>
													</div>



													<div ng-if="answer_step_block == 2 && answer_type == 1">
														<div class="form-group">
															<label>Nome del piatto</label><br />
															<input class="activated_input full" 
															ng-model="answer_recommendation_plate_input.name" name="platename" />
														</div>
														<button class="outline_btn bprimary" 
														ng-click="searchPlateForAnswer(answer_recommendation_plate_input.name)" 
														>Cerca</button>
														<div class="plates_answer_list">
															<ul class="list-unstyled">
																<li ng-repeat="plate in answer_plates track by $index"
																ng-click="selectPlate($index)"
																>
																	<div class="col-sm-3" ng-if="plate.image && plate.image != ''">
																		<img
																		ng-src="{{staticURL}}/menu/square/{{plate.image}}.png" 
																		class="img-circle img-repsonsive"
																		/>
																	</div>
																	<div class="col-sm-12">
																		<h3>{{plate.name}}</h3>
																		<p>{{plate.description}}</p>
																	</div>																	
																<li>
															</ul>
														</div>
													</div>



													<div class="clearfix"></div>
													<div ng-if="answer_step_block == 3">
														<label>Messaggio</label>
														<textarea class="activated_input full" style="margin-bottom: 10px;"
															ng-model="answer_recommendation_text_input.name" name="textname" />
														<button class="outline_btn bprimary" 
														ng-click="submitRecommendationAnswer(answer_recommendation_text_input.name)" 
														>Conferma</button>
														<button class="outline_btn bdanger" 
														ng-click="resetRecommendationAnswer()" 
														>Annulla</button>
													</div>
												</div>
												<!-- /steps per rispondere alle raccomandazioni -->

												<!-- lista risposte -->
												<div class="answers_request_list" ng-if="view_requests_answers.recommendation_id == recommendation._id">
													<ul class="list-unstyled">
														<li ng-repeat="answer in current_recommendation_list_view">
															<div class="col-md-3 col-sm-12" ng-if="answer.ref_obj.image && answer.ref_obj.image != ''">
																<img
																ng-src="{{staticURL}}/menu/square/{{answer.ref_obj.image}}.png" 
																class="img-circle img-repsonsive"
																ng-if="recommendation.type == 1"
																/>
																<img
																ng-src="{{staticURL}}/restaurant/square/{{answer.ref_obj.image}}.png" 
																class="img-circle img-repsonsive"
																ng-if="recommendation.type != 1"
																/>
															</div>
															<div class="col-md-9 col-sm-12">
																<h3 ng-if="recommendation.type != 1"><a href="/attivita/{{answer.ref_obj._id}}/">{{answer.ref_obj.name}}</a></h3>
																<h3 ng-if="recommendation.type == 1"><a href="/piatto/{{answer.ref_obj._id}}/">{{answer.ref_obj.name}}</a></h3>
																<p ng-if="recommendation.type != 1">{{answer.ref_obj.address}}</p>
																<p ng-if="recommendation.type == 1">{{answer.ref_obj.description}}</p>
															</div>	
															<div class="clearfix"></div>
															<div class="col-xs-push-2 col-sm-12 answer_user_info_block">
																<div class="col-md-2" ng-if="answer.user_obj.avatar && answer.user_obj.avatar != ''">
																	<img
																	ng-src="{{staticURL}}/users/square/{{answer.user_obj.avatar}}.png" 
																	class="img-circle img-repsonsive small_user_image"
																	ng-if="recommendation.type != 1"
																	/>
																</div>
																<div class="col-sm-12">
																	<p><a href="/u/{{answer.user_obj._id}}/">{{answer.user_obj.name}} {{answer.user_obj.surname}}</a></p>
																	<p>{{answer.text}}</p>
																</div>	
															</div>
														</li>
													</ul>
													<div class="clearfix"></div>
													<p class="info_and_button" ng-if="load_more_answers_show" ng-click="loadMoreAnswers()">
														Carica altre risposte
													</p>
												</div>
												<!-- /lista risposte -->

											</div>
										</div>									
										<div class="clearfix"></div>
										<hr />
									</li>
								</ul>
								<p class="text-center">
					                <button class="outline_btn bprimary profile-readMore" ng-click="loadRecommendation('loadRight')" 
					                ng-if="!stop_right_pagination"
					                    role="button">Altre</button>   
					            </p>
							</div>
						</div>
						
						<div ng-if="recommendation_tab == 1 || !itsme">
							<div class="col-sm-6">
								<h3 class="text-center" ng-if="itsme">Raccomandazioni</h3>
								<h3 class="text-center" ng-if="!itsme">Raccomandazioni di {{view_user.name}}</h3>
								<ul class="list-unstyled imaging_list">
									<li ng-repeat="recommendation in user_rec track by $index" ng-if="recommendation.ref_obj">
										<div class="clearfix"></div>
										<div class="col-lg-2 col-md-2 col-xs-3">
											<img class="img-circle" 
											ng-src="{{staticURL}}/restaurant/square/{{recommendation.ref_obj.image}}.png" alt="" title="" 
											ng-if="recommendation.ref_obj.image && recommendation.ref_obj.image != '' && (recommendation.type == 0 || !recommendation.type)" />
											<img class="img-circle" 
											ng-src="{{staticURL}}/menu/square/{{recommendation.ref_obj.image}}.png" alt="" title="" 
											ng-if="recommendation.ref_obj.image && recommendation.ref_obj.image != '' && recommendation.type == 1" />
											<img class="img-circle" 
											ng-src="{{staticURL}}/users/square/{{view_user.avatar}}.png" alt="" title="" 
											ng-if="!recommendation.ref_obj.image && recommendation.ref_obj.image != '' && view_user.avatar && view_user.avatar != ''" />
										</div>
										<div class="col-lg-10 col-md-10 col-xs-9">
											<h3><a href="/u/{{view_user._id}}/">{{view_user.name}} {{view_user.surname}}</a></h3>
											<p ng-if="recommendation.type == 0 || !recommendation.type">
												Ha raccomandato il ristorante <a href="/attivita/{{recommendation.ref_obj.slug}}/">{{recommendation.ref_obj.name}}
												</a>
											</p>
											<p ng-if="recommendation.type == 1">
												Ha raccomandato il piatto <a href="/piatto/{{recommendation.ref_obj._id}}/">{{recommendation.ref_obj.name}}
												</a>
											</p>
											<hr />
											<p>{{recommendation.text}}</p>
											<p>
												<span class="list_span active" ng-if="recommendation.type == 0 || !recommendation.type">Ristorante</span>
												<span class="list_span active" ng-if="recommendation.type == 1">Piatto</span>
											</p>
										</div>									
										<div class="clearfix"></div>
									</li>
								</ul>
								<p class="text-center">
					                <button class="outline_btn bprimary profile-readMore" ng-click="loadRecommendation('loadUser')" 
					                ng-if="!stop_load_by_user_pagination"
					                    role="button">Altre</button>   
					            </p>
							</div>
							<div class="col-sm-6">
								<h3 class="text-center" ng-if="itsme">Richieste</h3>
								<h3 class="text-center" ng-if="!itsme">Richieste di raccomandazione di {{view_user.name}}</h3>
								<ul class="list-unstyled imaging_list">
									<li ng-repeat="recommendation in user_req track by $index">
										<div class="col-lg-2 col-md-2 col-xs-3">
											<img class="img-circle" 
											ng-src="{{staticURL}}/users/square/{{view_user.avatar}}.png" alt="" title="" 
											ng-if="view_user.avatar && view_user.avatar != ''" />
										</div>
										<div class="col-lg-10 col-md-10 col-xs-9">
											<h3><a href="/u/{{view_user._id}}/">{{view_user.name}} {{view_user.surname}}</a></h3>
											<p>{{recommendation.text}}</p>
											<p>
												<span class="list_span active" ng-if="recommendation.type == 0">Ristorante</span>
												<span class="list_span active" ng-if="recommendation.type == 1">Piatto</span>
											</p>


											<div class="recommendation_answers">
												<p class="info_and_button">
													<a ng-click="openAnswers('user_req',$index,recommendation._id)">
														<i class="fa fa-comments"></i>{{recommendation.answers_num}}
													</a>
													 | <a ng-click="pushAnswer('user_req',$index,recommendation._id)">
													 Consiglia un 
													 <span ng-if="recommendation.type==0">Ristorante</span>
													 <span ng-if="recommendation.type==1">Piatto</span>
													</a>
												</p>
												


												<!-- steps per rispondere alle raccomandazioni -->
												<div class="publish_recommendation_request" ng-if="isAskingRequestFor(recommendation._id)">

													<div class="answer_info_block"
														ng-if="(current_answering.restaurant || current_answering.restaurant == 0) && current_answering.plate != 0 && !current_answering.plate">
														<div class="col-sm-3" 
														ng-if="answer_restaurants[current_answering.restaurant].image && answer_restaurants[current_answering.restaurant].image != ''">
															<img
															ng-src="{{staticURL}}/restaurant/square/{{answer_restaurants[current_answering.restaurant].image}}.png" 
															class="img-circle img-repsonsive"
															/>
														</div>
														<div class="col-sm-12">
															<h3>{{answer_restaurants[current_answering.restaurant].name}}</h3>
															<p>{{answer_restaurants[current_answering.restaurant].address}}</p>
														</div>	
														<div class="clearfix"></div>
													</div>

													<div  class="answer_info_block" ng-if="current_answering.plate || current_answering.plate == 0">
														<div class="col-sm-3" ng-if="answer_plates[current_answering.plate].image && answer_plates[current_answering.plate].image != ''">
															<img
															ng-src="{{staticURL}}/menu/square/{{answer_plates[current_answering.plate].image}}.png" 
															class="img-circle img-repsonsive"
															/>
														</div>
														<div class="col-sm-12">
															<h3>{{answer_plates[current_answering.plate].name}}</h3>
														</div>	
														<div class="clearfix"></div>
													</div>



													<div ng-if="answer_step_block == 1">
														<div class="form-group">
															<label>Ristorante</label><br />
															<input class="activated_input full" 
															ng-model="answer_recommendation_restaurant_input.name" name="ristoname" />
														</div>
														<button class="outline_btn bprimary" 
														ng-click="searchRestaurantForAnswer(answer_recommendation_restaurant_input.name)" 
														>Cerca</button>
														<div class="restaurants_answer_list">
															<ul class="list-unstyled">
																<li ng-repeat="restaurant in answer_restaurants track by $index"
																ng-click="selectRestaurant($index)"
																>
																	<div class="col-sm-3" ng-if="restaurant.image && restaurant.image != ''">
																		<img
																		ng-src="{{staticURL}}/restaurant/square/{{restaurant.image}}.png" 
																		class="img-circle img-repsonsive"
																		/>
																	</div>
																	<div class="col-sm-12">
																		<h3>{{restaurant.name}}</h3>
																		<p>{{restaurant.address}}</p>
																	</div>																	
																<li>
															</ul>
														</div>
													</div>



													<div ng-if="answer_step_block == 2 && answer_type == 1">
														<div class="form-group">
															<label>Nome del piatto</label><br />
															<input class="activated_input full" 
															ng-model="answer_recommendation_plate_input.name" name="platename" />
														</div>
														<button class="outline_btn bprimary" 
														ng-click="searchPlateForAnswer(answer_recommendation_plate_input.name)" 
														>Cerca</button>
														<div class="plates_answer_list">
															<ul class="list-unstyled">
																<li ng-repeat="plate in answer_plates track by $index"
																ng-click="selectPlate($index)"
																>
																	<div class="col-sm-3" ng-if="plate.image && plate.image != ''">
																		<img
																		ng-src="{{staticURL}}/menu/square/{{plate.image}}.png" 
																		class="img-circle img-repsonsive"
																		/>
																	</div>
																	<div class="col-sm-12">
																		<h3>{{plate.name}}</h3>
																		<p>{{plate.description}}</p>
																	</div>																	
																<li>
															</ul>
														</div>
													</div>



													<div class="clearfix"></div>
													<div ng-if="answer_step_block == 3">
														<label>Messaggio</label>
														<textarea class="activated_input full" style="margin-bottom: 10px;"
															ng-model="answer_recommendation_text_input.name" name="textname" />
														<button class="outline_btn bprimary" 
														ng-click="submitRecommendationAnswer(answer_recommendation_text_input.name)" 
														>Conferma</button>
														<button class="outline_btn bdanger" 
														ng-click="resetRecommendationAnswer()" 
														>Annulla</button>
													</div>
												</div>
												<!-- /steps per rispondere alle raccomandazioni -->

												<!-- lista risposte -->
												<div class="answers_request_list" ng-if="view_requests_answers.recommendation_id == recommendation._id">
													<ul class="list-unstyled">
														<li ng-repeat="answer in current_recommendation_list_view">
															<div class="col-md-3 col-sm-12" ng-if="answer.ref_obj.image && answer.ref_obj.image != ''">
																<img
																ng-src="{{staticURL}}/menu/square/{{answer.ref_obj.image}}.png" 
																class="img-circle img-repsonsive"
																ng-if="recommendation.type == 1"
																/>
																<img
																ng-src="{{staticURL}}/restaurant/square/{{answer.ref_obj.image}}.png" 
																class="img-circle img-repsonsive"
																ng-if="recommendation.type != 1"
																/>
															</div>
															<div class="col-md-9 col-sm-12">
																<h3 ng-if="recommendation.type != 1"><a href="/attivita/{{answer.ref_obj._id}}/">{{answer.ref_obj.name}}</a></h3>
																<h3 ng-if="recommendation.type == 1"><a href="/piatto/{{answer.ref_obj._id}}/">{{answer.ref_obj.name}}</a></h3>
																<p ng-if="recommendation.type != 1">{{answer.ref_obj.address}}</p>
																<p ng-if="recommendation.type == 1">{{answer.ref_obj.description}}</p>
															</div>	
															<div class="clearfix"></div>
															<div class="col-xs-push-2 col-sm-12 answer_user_info_block">
																<div class="col-md-2" ng-if="answer.user_obj.avatar && answer.user_obj.avatar != ''">
																	<img
																	ng-src="{{staticURL}}/users/square/{{answer.user_obj.avatar}}.png" 
																	class="img-circle img-repsonsive small_user_image"
																	ng-if="recommendation.type != 1"
																	/>
																</div>
																<div class="col-sm-12">
																	<p><a href="/u/{{answer.user_obj._id}}/">{{answer.user_obj.name}} {{answer.user_obj.surname}}</a></p>
																	<p>{{answer.text}}</p>
																</div>	
															</div>
														</li>
													</ul>
													<div class="clearfix"></div>
													<p class="info_and_button" ng-if="load_more_answers_show" ng-click="loadMoreAnswers()">
														Carica altre risposte
													</p>
												</div>
												<!-- /lista risposte -->

											</div>
										</div>									
										<div class="clearfix"></div>
										<hr />
									</li>
								</ul>
							</div>
						</div>
						
						<div ng-if="recommendation_tab == 2 && itsme">
							<div class="col-sm-6 col-sm-push-3">
								<form id="askRequestForm" class="">
		                      		<p>Richiedi una raccomandazione alla community di MangiaeBevi</p>
		                      		<p>
		                      			<span ng-click="not_show_address = 1">
			                      			<i class="mebi-placeholder-on-map-paper-in-perspective"></i> Usa la tua posizione
			                      		</span> 
			                      		<span ng-click="not_show_address = 2">
			                      			 | Inserisci l'indirizzo
			                      		</span> 
			                      	</p>                       
			                        <div class="form-group" ng-if="not_show_address != 1">
			                          <label>Indirizzo</label><br />
			                          <input type="text" name="address" class="activated_input full" ng-model="recommendation_request_ask.address"
										ng-autocomplete 
										details="details"
										class="form-control" 
										placeholder="Indirizzo" />
			                        </div>
			                        <div class="form-group relative">
			                          	<label>Testo</label><br />
	                          			<textarea msd-elastic class="activated_input full" type="text" ng-model="recommendation_request_ask.text" required></textarea>
			                        </div> 
			                        <div class="form-group relative">
			                          	<label>Cosa cerchi?</label><br />
	                          			<span class="fasce_span" 
	                          			ng-click="setRecType(0)" 
	                          			ng-class="{'active': !recommendation_request_ask.type || recommendation_request_ask.type == 0}">
									      	Ristorante
									    </span>
									    <span class="fasce_span" 
									    ng-click="setRecType(1)" 
									    ng-class="{'active': recommendation_request_ask.type == 1}">
									      	Piatto
									    </span>
			                        </div> 
			                    </form>
			                    <button ng-disabled="askRequestForm.$invalid" class="outline_btn bprimary" ng-click="askRecommendation()">
			                    	<span>Conferma</span>
			                    </button> 
							</div>
						</div>
						
					</div>
				</div>

				<div ng-if="current_tab == 2" ng-init="initFriends()">
					<div class="row">
						<div class="col-sm-12">
							<h2 class="text-center">Amici</h2>							
							<div class="minimal_tabs">
								<ul class="list-unstyled">
									<li ng-class="{'active' : current_friend_tab == 0 || !current_friend_tab}">
										<a ng-click="setFriendTab(0)">Amici</a>
									</li>
									<li ng-class="{'active' : current_friend_tab == 1}" ng-if="itsme">
										<a ng-click="setFriendTab(1)">Richieste</a>
									</li>	
									<li ng-class="{'active' : current_friend_tab == 2}">
										<a ng-click="setFriendTab(2)">Followers</a>
									</li>
									<li ng-class="{'active' : current_friend_tab == 3}">
										<a ng-click="setFriendTab(3)">Following</a>
									</li>							
								</ul>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-sm-6 col-sm-push-3">
							<ul class="list-unstyled imaging_list" ng-if="!current_friend_tab || current_friend_tab == 0" ng-init="initViewFriend()">
								<li ng-repeat="friend in current_friends track by $index">		
									<div class="text-center">
										
										<img class="img-circle" style="margin-right: 0" 
										ng-src="{{staticURL}}/users/square/{{friend.avatar}}.png" alt="" title="" 
										ng-if="friend.avatar && friend.avatar != ''" />
											
										<h3 class="text-center">
											<a href="/u/{{friend._id}}/">{{friend.name}} {{friend.surname}}</a>
										</h3>
										<button class="outline_btn bdanger" ng-click="removeFriend(friend._id)" ng-if="itsme">
											Rimuovi dagli amici
										</button>
									</div>								
									<div class="clearfix"></div>									
								</li>
							</ul>
							<ul class="list-unstyled imaging_list" ng-if="current_friend_tab == 1" ng-init="initRequestFriend()">
								<li ng-repeat="friend in current_friends_request track by $index">									
									
									<div class="text-center">
										<div class="row">
											<div class="col-sm-4">
												<span class="hint--bottom" aria-label="Accetta">
													<button class="outline_btn bsuccess nomin iconic" ng-click="acceptRequest(friend._id)">
														<i class="mebi-android-done"></i>
													</button>
												</span>
											</div>
											<div class="col-sm-4">
												<img class="img-circle" style="margin-right: 0" 
												ng-src="{{staticURL}}/users/square/{{friend.avatar}}.png" alt="" title="" 
												ng-if="friend.avatar && friend.avatar != ''" />
											</div>
											<div class="col-sm-4">
												<span class="hint--bottom" aria-label="Rifiuta">
													<button class="outline_btn bdanger nomin iconic" ng-click="refuseRequest(friend._id)">
														<i class="mebi-close"></i>
													</button>
												</span>
											</div>
										</div>										
									
										<h3 class="text-center"><a href="/u/{{friend._id}}/">{{friend.name}} {{friend.surname}}</a></h3>
									</div>								
									<div class="clearfix"></div>									
								</li>
							</ul>
							<ul class="list-unstyled imaging_list" ng-if="current_friend_tab == 2" ng-init="inFollowers()">
								<li ng-repeat="friend in user_follower track by $index">		
									<div class="text-center">
										
										<img class="img-circle" style="margin-right: 0" 
										ng-src="{{staticURL}}/users/square/{{friend.avatar}}.png" alt="" title="" 
										ng-if="friend.avatar && friend.avatar != ''" />
											
										<h3 class="text-center">
											<a href="/u/{{friend._id}}/">{{friend.name}} {{friend.surname}}</a>
										</h3>
									</div>								
									<div class="clearfix"></div>									
								</li>
								<li class="text-center" ng-if="!stop_load_follower_pagination">
				                    <button class="outline_btn bprimary profile-readMore" ng-click="loadFollowers()" 
		                    		role="button">Altri</button>   
								</li>
							</ul>
							<ul class="list-unstyled imaging_list" ng-if="current_friend_tab == 3" ng-init="inFollowing()">
								<li ng-repeat="friend in user_following track by $index">		
									<div class="text-center">
										
										<img class="img-circle" style="margin-right: 0" 
										ng-src="{{staticURL}}/users/square/{{friend.avatar}}.png" alt="" title="" 
										ng-if="friend.avatar && friend.avatar != ''" />
											
										<h3 class="text-center">
											<a href="/u/{{friend._id}}/">{{friend.name}} {{friend.surname}}</a>
										</h3>
									</div>								
									<div class="clearfix"></div>									
								</li>
								<li class="text-center" ng-if="!stop_load_following_pagination">
				                    <button class="outline_btn bprimary profile-readMore" ng-click="loadFollowing()" 
		                    		role="button">Altri</button>   
								</li>
							</ul>
						</div>						
					</div>
				</div>

				<div ng-if="current_tab == 3" ng-init="initPlates()">
					<div class="row">
						<div class="col-sm-6 col-sm-push-3">
							<h2 class="text-center">Piatti aggiunti da {{view_user.name}} {{view_user.surname}}</h2>							
							<ul class="list-unstyled imaging_list" ng-init="loadPlateImages()">
		                        <li ng-repeat="image in plate_images track by $index">
		                            <div class="clearfix"></div>
		                            <div class="col-lg-12">
		                                <a href="/community/piatto/{{image._id}}/">
		                                	<img class="full" 
		                                	ng-src="{{staticURL}}/community/medium/{{image.image}}.png" alt="" title="" />
		                                </a>
		                                <img class="user_image_minus_top" 
		                                ng-src="{{staticURL}}/menu/medium/{{image['plate_obj'].image}}.png" alt="" title="" 
		                                ng-if="image['plate_obj'].image && image['plate_obj'].image != ''" />
		                            </div>

		                            <div class="user_plate_image_info">
		                                <h3>
		                                    <a href="/piatto/{{image['plate_obj']._id}}/">{{image['plate_obj'].name}}</a>
		                                </h3>
		                                <p class="text-center">
		                                	<span class="hint--right" aria-label="Condividi il piatto">
			                                	<a class="" ng-click="share('facebook_share',image)"><i class="fa fa-facebook fb"></i></a>
	                                            <a class="" ng-click="share('twitter',image)"><i class="fa fa-twitter tw"></i></a>
												<a class="" ng-click="share('google_plus',image)"><i class="fa fa-google-plus gp"></i></a>
												<a class="" ng-click="share('linkedin',image)"><i class="fa fa-linkedin li"></i></a>
												<a class="" ng-click="share('facebook_send',image)"><i class="fa fa-comments fb"></i></a>
											</span>
										</a>
		                                </p>
		                                    <p>{{image.text}}</p>
		                                    <p class="text-center rating_p">{{image.rating}}</p>                                            
		                            </div>                                  
		                            <div class="clearfix"></div>
		                        </li>
		                    </ul>
						</div>	
						<div class="clearfix"></div>
		                <p class="text-center">
		                    <button class="outline_btn bprimary profile-readMore" ng-click="loadPlateImagesN()" 
		                    ng-if="!stop_load_images_pagination"
		                        role="button">Altre foto</button>   
		                </p>					
					</div>
				</div>
			

				<div ng-if="current_tab == 5">
					<div class="col-sm-6 col-sm-push-3">
						<h2 class="text-center">Le tue prenotazioni</h2>
						<ul class="list-unstyled imaging_list">
							<li ng-repeat="reservation in reservations track by $index" class="reservation_block marginated_vertical bordered">
								<div class="col-lg-2 col-md-2 col-xs-3">
									<img class="img-circle" 
									ng-src="{{staticURL}}/restaurant/square/{{returnReservationRestaurant(reservation.restaurant).image}}.png" alt="" title="" 
									ng-if="returnReservationRestaurant(reservation.restaurant).image && returnReservationRestaurant(reservation.restaurant).image != ''" />
								</div>
								<div class="col-lg-10 col-md-10 col-xs-9">
									<h3><a href="/attivita/{{returnReservationRestaurant(reservation.restaurant).slug}}/">{{returnReservationRestaurant(reservation.restaurant).name}}</a></h3>
									<p><i class="mebi-placeholder-on-map-paper-in-perspective"></i> {{returnReservationRestaurant(reservation.restaurant).address}}</p>								
								</div>		
								<div class="clearfix"></div>
								<p class="text-center text-primary marginated_vertical" ng-if="reservation.confirmed == 1">Prenotazione confermata</p>
                              	<p class="text-center text-warning marginated_vertical" ng-if="reservation.confirmed == 0 || reservation.confirmed == 3">In attesa di conferma</p>
                              	<p ng-if="reservation.confirmed == 2" class="text-center marginated_vertical">
	                              	Il proprietario del ristorante ha cambiato la l'orario della prenotazione<br /> 
	                              	<span class="hint--bottom" aria-label="Accetta">
										<button class="outline_btn bsuccess nomin iconic" ng-click="accettaCambioData(reservation)">
											<i class="mebi-android-done"></i>
										</button>
									</span>
									<span class="hint--bottom" aria-label="Rifiuta">
										<button class="outline_btn bdanger nomin iconic" ng-click="rifiutaCambioData(reservation)">
											<i class="mebi-close"></i>
										</button>
									</span>
	                            </p>					
								<div class="row icons_block">
									<div class="col-xs-6">
										<div class="col-xs-4"><i class="mebi-avatar"></i></div>
										<div class="col-xs-8 num">{{reservation.people}}</div>
									</div>
									<div class="col-xs-6">
										<div class="col-xs-4"><i class="mebi-time"></i></div>
										<div class="col-xs-8 num">{{reservation.date|amDateFormat:'DD/MM/YYYY'}}<br />{{reservation.fascia_oraria}}</div>
									</div>
								</div>		
								<div class="clearfix"></div>
							</li>
						</ul>
					</div>					
				</div>

				<div ng-if="current_tab == 6">
					<h2 class="text-center">Notifiche</h2>
					<ul class="list-group notifications_list" ng-if="notifications.length > 0">
	                  <li class="list-group-item single_notification" ng-repeat="notification in notifications track by $index">
	                    <div class="media">
	                      <div class="media-left">
	                        <i class="mebi-profile text-success" ng-if="notification.type == 0"></i>
	                        <i class="mebi-list text-primary" ng-if="notification.type == 1"></i>
	                        <i class="mebi-cutlery text-info" ng-if="notification.type == 2"></i>
	                        <i class="mebi-paypal text-success" ng-if="notification.type == 3"></i>
	                        <i class="mebi-phone-call text-warning" ng-if="notification.type == 4"></i>
	                        <i class="mebi-check-1 text-success" ng-if="notification.type == 5"></i>
	                        <i class="mebi-check-1 text-success" ng-if="notification.type == 6"></i>
	                        <i class="mebi-refund text-danger" ng-if="notification.type == 7"></i>
	                        <i class="mebi-refund text-success" ng-if="notification.type == 8"></i>
	                        <i class="mebi-calendar-1 text-info" ng-if="notification.type == 9"></i>
	                        <i class="mebi-delivery text-danger" ng-if="notification.type == 10"></i>
	                        <i class="mebi-check-1 text-success" ng-if="notification.type == 11"></i>
	                        <i class="mebi-check-1 text-primary" ng-if="notification.type == 12"></i>
	                        <i class="mebi-calendar-1 text-danger" ng-if="notification.type == 13"></i>
	                        <i class="mebi-close text-danger" ng-if="notification.type == 14"></i>
	                        <i class="mebi-avatar text-primary" ng-if="notification.type == 15"></i>
	                        <i class="mebi-avatar text-success" ng-if="notification.type == 16"></i>
	                        <i class="mebi-avatar text-warning" ng-if="notification.type == 17"></i>
	                      </div>
	                      <div class="media-body">
	                        <h5 class="media-heading" ng-if="notification.type == 0">
	                          <b>Nuova registrazione</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 1">
	                          <b>Nuovo ordine</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 2">
	                          <b>Ristorante aggiunto</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 3">
	                          <b>Nuova iscrizione a un piano di abbonamento</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 4">
	                          <b>Claim</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 5">
	                          <b>Richiesta approvata</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 6">
	                          <b>Ristorante approvato</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 7">
	                          <b>Chiesto rimborso</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 8">
	                          <b>Ordine rimborsato</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 9">
	                          <b>Nuova prenotazione</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 10">
	                          <b>Utente vuole essere un driver</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 11">
	                          <b>Prenotazione confermata</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 12">
	                          <b>Accettato cambio prenotazione</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 13">
	                          <b>Data prenotazione cambiata</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 14">
	                          <b>Rifiutato cambio prenotazione</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 15">
	                          <b>Nuovo follower</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 16">
	                          <b>Richiesta di amicizia accettata</b>
	                        </h5>
	                        <h5 class="media-heading" ng-if="notification.type == 17">
	                          <b>Nuova raccomandazione</b>
	                        </h5>

	                        
	                        <small>{{notification.created_at|amDateFormat:'DD/MM/YYYY HH:mm:ss'}}</small>
	                        
	                        <div class="profile-brief" ng-if="notification.type == 0">
	                          <a href="/u/{{notification.parameters[0]}}/">
	                            {{notification.parameters[1]}}
	                          </a> si è iscritto
	                        </div>
	                        
	                        <div class="profile-brief" ng-if="notification.type == 1">
	                          <a href="/u/{{notification.parameters[1]}}/">
	                            {{notification.parameters[2]}}
	                          </a> 
	                            ha effettuato un 
	                            <a ng-click="goToAdmin('#/restaurant/orders/'+notification.parameters[4]+'/'+notification.parameters[0])">
	                            ordine
	                          </a> di 
	                          {{notification.parameters[3]}} € con consegna il 
	                          {{notification.parameters[4].date|amDateFormat:'DD.MM.YYYY'}} 
	                          alle {{notification.parameters[4].date|amDateFormat:'HH:mm'}}                          
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 2">
	                          <a href="/u/{{notification.parameters[0]}}/">
	                            {{notification.parameters[1]}}
	                          </a> 
	                          ha aggiunto il ristorante 
	                          <a href="/modifica-ristorante/{{notification.parameters[2]}}/">
	                            {{notification.parameters[3]}}
	                          </a>
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 3">
	                          <a href="/u/{{notification.parameters[0]}}/">
	                            {{notification.parameters[1]}}
	                          </a> ha sottoscritto il piano {{notification.parameters[2]}}
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 4">
	                          <a href="/u/{{notification.parameters[0]}}/">{{notification.parameters[1]}}
	                          </a> 
	                          reclama il ristorante 
	                          <a href="/modifica-ristorante/{{notification.parameters[2]}}/">{{notification.parameters[3]}}
	                          </a>
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 5">
	                          La tua richiesta di gestire 
	                          <a href="/modifica-ristorante/{{notification.parameters[0]}}/">{{notification.parameters[1]}}
	                          </a> è stata approvata
	                        </div>
	                        
	                        <div class="profile-brief" ng-if="notification.type == 6">
	                          Il ristorante 
	                          <a href="/modifica-ristorante/{{notification.parameters[0]}}/">{{notification.parameters[1]}}
	                          </a> è stato approvato
	                        </div>
	                        
	                        <div class="profile-brief" ng-if="notification.type == 7">&Egrave; stato chiesto un rimborso per l'ordine numero 
	                          <a ng-click="goToAdmin('#/restaurant/orders/'+notification.parameters[1]+'/'+notification.parameters[0])">
	                            #{{notification.parameters[0]}}
	                          </a>
	                        </div>
	                        
	                        <div class="profile-brief" ng-if="notification.type == 8">
	                          L'ordine numero 
	                          #{{notification.parameters[0]}}
	                          è stato rimborsato
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 9">
	                          <a href="/u/{{notification.parameters[0]}}/">
	                            {{notification.parameters[1]}}
	                          </a>  
	                          ha prenotato al
	                          <a href="/modifica-ristorante/{{notification.parameters[2]}}/">
	                            {{notification.parameters[3]}}
	                          </a> 
	                          il {{notification.parameters[4]}} 
	                          per {{notification.parameters[5]}} persone 
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 10">
	                          <a href="/u/{{notification.parameters[0]}}/">
	                            {{notification.parameters[1]}}
	                          </a> vuole essere un driver
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 11">
	                          La tua prenotazione a {{notification.parameters[2]}} ({{notification.parameters[3]}}) è stata confermata
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 12">
	                          Il cambio di data della prenotazione di {{notification.parameters[4]}} ({{notification.parameters[3]}}) è stato accettato
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 13">
	                          La tua prenotazione a {{notification.parameters[2]}} è stata modificata 
	                          ({{notification.parameters[3]}} -> {{notification.parameters[4]}}) 
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 14">
	                        	<a href="/u/{{notification.parameters[0]}}/">
	                        		{{notification.parameters[1]}}</a> 
	                        		ha rifiutato il cambio di orario della prenotazione
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 15">
	                        	<a href="/u/{{notification.parameters[0]}}/">
	                        		{{notification.parameters[1]}}</a> 
	                        		ha iniziato a seguirti
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 16">
	                        	<a href="/u/{{notification.parameters[0]}}/">
	                        		{{notification.parameters[1]}}</a> 
	                        		ha accettato la tua richiesta di amicizia
	                        </div>

	                        <div class="profile-brief" ng-if="notification.type == 17">
	                        	<a href="/u/{{notification.parameters[0]}}/">
	                        		{{notification.parameters[1]}}</a> 
	                        		ha raccomandato il tuo <a href="/modifica-ristorante/{{notification.parameters[2]}}/">ristorante</a>
	                        </div>
	                        
	                      </div>
	                    </div>
	                  </li>                    
	                </ul>
	                <p class="text-center">
		                <button class="outline_btn bprimary profile-readMore" ng-click="loadMoreNotifications()" ng-if="!notification_hide_loader"
		                    role="button">Altre</button>   
		            </p>
				</div>

				<div ng-if="current_tab == 7 && itsme">
					<h2 class="text-center">Cambia le informazioni del tuo profilo</h2>
					<div class="row">
	                  <div class="col-md-6 col-md-push-3 col-sm-12">
	                  	<div class="minimal_tabs">
							<ul class="list-unstyled">
								<li ng-class="{'active' : !profile_tab || profile_tab == 0}"><a ng-click="profile_tab = 0">Profilo</a></li>
								<li ng-class="{'active' : profile_tab == 1}"><a ng-click="profile_tab = 1">Account</a></li>	
								<li ng-class="{'active' : profile_tab == 2}"><a ng-click="profile_tab = 2">Abbonamento</a></li>								
							</ul>
						</div>
	                  	<div ng-if="profile_tab == 0 || !profile_tab">
	                  		<div class="upload_parent" style="position: relative; margin: 30px 0;">
								<div class="upload_viewed">
									<button class="outline_btn bprimary">
				                      	<span>Immagine del profilo</span>
				                    </button>
								</div>
								<input class="upload_hidden"
								type="file" 
								ngf-select 
								ng-model="change_avatar.avatar" name="change_cover" ng-change="uploadAvatar()" title="Cambia" 
								style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; opacity: 0; cursor: pointer;" />
							</div>
		                    <form id="ProfileInfo" class="" ng-submit="updateInfo()">                        
		                        <div class="form-group">
		                          <label>Nome</label><br />
		                          <input class="activated_input full" type="text" ng-model="current_user.user.name" required/>
		                        </div>
		                        <div class="form-group">
		                          <label>Cognome</label><br />
		                          <input class="activated_input full" type="text" ng-model="current_user.user.surname" required/>
		                        </div>
		                        <div class="form-group">
		                          <label>Telefono</label><br />
		                          <input class="activated_input full" type="text" ng-model="current_user.user.phone" required/>
		                        </div>
		                        <div class="form-group">
		                          <label>Qualcosa di te</label><br />
		                          <textarea msd-elastic class="activated_input full" type="text" ng-model="current_user.user.description" required></textarea>
		                        </div>
		                        <button ng-disabled="ProfileInfo.$invalid" class="outline_btn bprimary">
			                      	<span>Conferma le modifiche</span>
			                    </button>                            
	                      	</form>
	                      	<hr class="margin-big-v" />
	                      	<form id="completeProfileForm" class="">
	                      		<p ng-if="current_user.user.foodcoin <= 500">Completa il tuo profilo per avere i tuoi foodcoin</p>                        
		                        <div class="form-group">
		                          <label>Indirizzo</label><br />
		                          <input type="text" name="address" class="activated_input full" ng-model="current_user.user.address"
									ng-autocomplete 
									details="details"
									class="form-control" 
									placeholder="Indirizzo" />
		                        </div>
		                        <div class="form-group relative">
		                          <label>Data di nascita</label><br />
		                          <input type="text" id="datetimepicker-day" class="activated_input full" required />
		                        </div> 
		                    </form>
		                    <button ng-disabled="completeProfileForm.$invalid" class="outline_btn bprimary" ng-click="completeUserInfo()">
		                    	<span ng-if="view_user.foodcoin <= 500">Completa il profilo</span>
		                      	<span ng-if="view_user.foodcoin > 500">Conferma le modifiche</span>
		                    </button> 
	                	</div>
	                    <div ng-if="profile_tab == 1">
		                    <h3>Modifica la password per accedere al tuo account</h3>
	                        <form id="changePWDForm" ng-submit="changePWD()"> 
	                        	<p class="text-danger" ng-if="pwd_error">Le password non coincidono</p>                           
		                        <div class="form-group">
		                          <label>Vecchia password</label><br />
		                          <input type="password" ng-model="change_pwd.old" class="activated_input full" required/>
		                        </div>
		                        <div class="form-group">
		                          <label>Nuova password</label><br />
		                          <input type="password" ng-model="change_pwd.new_pwd" class="activated_input full" required/>
		                        </div>
		                        <div class="form-group">
		                          <label>Conferma la nuova password</label><br />
		                          <input type="password" ng-model="change_pwd.new_confirm" class="activated_input full" required/>
		                        </div>  
		                        <div class="form-group">
		                        	<button ng-disabled="changePWDForm.$invalid" class="outline_btn bprimary" ng-click="completeUserInfo()">
				                    	<span>Modifica la password</span>
				                    </button>
		                        </div>                      
	                        </form>  

	                        <hr />
	                        <h3>Modifica l'indirizzo email del tuo account</h3>
	                        <form id="changeMAILForm" ng-submit="changeMAIL()">      
	                        	<p class="text-danger" ng-if="mail_error">Le email non coincidono</p>                  
		                        <div class="form-group">
		                          <label>Nuova email</label><br />
		                          <input type="email" ng-model="change_mail.new_mail" class="activated_input full" required/>
		                        </div>
		                        <div class="form-group">
		                          <label>Nuova email</label><br />
		                          <input type="email" ng-model="change_mail.confirm_new_mail" class="activated_input full" required/>
		                        </div>
		                        <div class="form-group">
		                        	<button ng-disabled="changeMAILForm.$invalid" class="outline_btn bprimary" ng-click="completeUserInfo()">
				                    	<span>Modifica la mail</span>
				                    </button>	                          
		                        </div>                      
	                        </form>  
	                        <hr />
	                        <p>Cancella il tuo account (<b>questa azione è irreversibile</b>, tutti i tuoi dati verranno cancellati)</p>
	                      	<button class="outline_btn bdanger text-center" ng-click="cancellaAccount()">Cancellami</button>
	                    </div>
	                    <div ng-if="profile_tab == 2 && billings">
	                    	<h3>Il tuo piano di abbonamento</h3>
		                    <p>{{user_billing_plan.name}}</p>
	                        <label ng-if="view_user.billing_plan.period == 0">{{user_billing_plan.price_month}} € al mese</label>
	                        <label ng-if="view_user.billing_plan.period == 1">{{user_billing_plan.price_annual}} € all'anno</label><br />
	                        <button class="outline_btn bdanger text-center" ng-click="cancelBillingPlan()">Cancella il piano di abbonamento</button>
	                    </div>
	                  </div>
	                  
	                </div>	                	                
				</div>


				<div ng-if="current_tab == 8 && itsme && view_user.role >= 2">
					<h2 class="text-center">I tuoi ristoranti</h2>
					<div class="row">
	                  	<ul class="list-unstyled imaging_list col-sm-6 col-sm-push-3" ng-init="loadRestaurantsUser()">
							<li ng-repeat="restaurant in user_restaurants">
								<div class="col-lg-2 col-md-2 col-xs-3">
									<img class="img-circle" 
									ng-src="{{staticURL}}/restaurant/square/{{restaurant.image}}.png" alt="" title="" 
									ng-if="restaurant.image && restaurant.image != ''" />
								</div>
								<div class="col-lg-10 col-md-10 col-xs-9">
									<h3><a href="/modifica-ristorante/{{restaurant.slug}}/">{{restaurant.name}}</a></h3>
									<p><i class="mebi-placeholder-on-map-paper-in-perspective"></i> {{restaurant.address}}</p>									
								</div>									
								<div class="clearfix"></div>
							</li>
						</ul>	                  
	                </div>	                	                
				</div>
			</div>
		</div>
	</div>
	<div>
		<div class="clearfix"></div>
	</div>
</div>

