<div id="staticPage" 
ng-init="
setSEO('Come funziona | Mangiaebevi','/come-funziona/','/img/pages/cibo.jpg','/img/pages/cibo.jpg','','Leggi come funziona MangiaeBevi, cerca un ristorante o prenota al tuo ristorante preferito')"
>
	<section class="bg_image vcenter_parent full-height" back-img img="/img/pages/cibo.jpg">
		<div class="overlay"></div>
		<div class="vcenter_child big_width">
			<h1 class="white-text center">Come funziona <br />
				<small style="text-transform: none;"><b>MangiaeBevi</b> consente all’utente di poter ricercare un ristorante, in un’unica piattaforma, web e mobile. </small>
			</h1>			
		</div>
		<div class="go_down" scroll-to="#who">
			<p>
				<i class="fa fa-chevron-down"></i>
			</p>
		</div>
	</section>
	<section id="who">
		<div class="container">
			<div class="col-sm-12 padding-big-v medium_block block_auto">
				<p>
					L‘utente ha la possibilità di scegliere – secondo le proprie esigenze – da una lista di ristoranti, 
					selezionati principalmente in base alla zona e alla cucina desiderata, ma anche per fascia di prezzo, servizi, menù, ecc.
				</p>
				<img class="img-responsive padding-big-v" ng-src="/img/pages/ricerca.png" alt="ricerca un ristorante" />
				 <?php /*class="opacity_low"*/?>
				<p>Niente più recensioni e profili “fake” ma <b class="text-primary">raccomandazioni</b> fatte da una community di utenti verificati <b class="text-primary">(in arrivo!) </b>
					che rendono così affidabile la piattaforma e i loro consigli. Si torna al “passaparola”, 
					la più antica e affidabile forma di pubblicità. 
				</p>
				<div class="foodcoin-mechanics">
				</div>
				<p>Per le loro <b class="text-primary">“raccomandazioni”</b> – oltre che per le prenotazioni – gli utenti potranno ricevere a breve dei crediti (foodcoins) utilizzabili nei ristoranti affiliati per fruire di sconti, offerte speciali e partecipare ad eventi enogastronomici. 
<br />Crediti che ogni utente potrà ottenere anche invitando i propri amici ad iscriversi, semplicemente, a MangiaeBevi. 

				</p>
			</div>			
		</div>
	</section>	
	
</div>