<div class="form-info">
	<h3>Confermi di voler gestire questo ristorante?</h3>
	<form name="claimForm" ng-submit="claimRestaurant(claimFormData)">
		<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<label for="name">Nome Completo</label>
			<input type="text" ng-model="claimFormData.name" id="name" class="form-control" placeholder="Es. Mario Rossi" required/>
		</div>
		<?php /*<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<label for="surname">Cognome</label>
			<input type="text" ng-model="claimFormData.surname" id="surname" class="form-control" required/>
		</div>*/?>
		<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<label for="phone">Telefono</label>
			<input type="text" ng-model="claimFormData.phone" id="phone" class="form-control" required/>
		</div>
		<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<label for="email">Email</label>
			<input type="email" ng-model="claimFormData.email" id="email" class="form-control" required/>
		</div>
		<div class="form-group" ng-if="current_user.loggedStatus == 0">
			<label for="password">Password</label>
			<input type="password" ng-model="claimFormData.password" id="password" class="form-control" required/>
		</div>
		<div class="form-group check" ng-if="current_user.loggedStatus == 0">
			<label class="label--checkbox block">
                <input class="checkbox" type="checkbox" ng-model="claimFormData.check" id="check" required> Accetti i termini e condizioni di MangiaeBevi+
            </label>
		</div>
		<a class="link_btn" href="/accedi/" ng-if="current_user.loggedStatus == 0">Sei già registrato?</a>
		<div class="form-group">
			<input type="submit" ng-disabled="claimForm.$invalid" class="btn btn-block btn-primary login-btn" value="Conferma" />
		</div>
		<a ng-click="closeDialog()">No, mi sono sbagliato</a>
	</form>
</div>