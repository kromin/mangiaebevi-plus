<div class="log_panel" ng-controller="LogPanelCtrl">
	<div class="social-log-container" ng-if="login_form != 2">
    	<h3>Accedi con il tuo<br /> social network preferito</h3>
    	<i class="fa fa-facebook" ng-click="accessWithSocial('facebook')"></i>
		<i class="fa fa-twitter" ng-click="accessWithSocial('twitter')"></i>
		<i class="fa fa-linkedin" ng-click="accessWithSocial('linkedin')"></i>
    </div>
	<div class="form-info">
		<form name="reserveForm" ng-submit="register(reserveFormData)" ng-if="login_form == 3">
			<h3>O compila il form<br/>per registrarti</h3>
			<div class="form-group">
				<label for="name" class="sr-only">Nome</label>
				<input type="text" ng-model="reserveFormData.name" id="name" class="form-control" placeholder="es. Mario" required/>
			</div>
			<div class="form-group">
				<label for="surname" class="sr-only">Cognome</label>
				<input type="text" ng-model="reserveFormData.surname" id="surname" class="form-control" placeholder="es. Rossi" required/>
			</div>
			<div class="form-group">
				<label for="phone" class="sr-only">Telefono</label>
				<input type="text" ng-model="reserveFormData.phone" id="phone" class="form-control" placeholder="Telefono" required/>
			</div>
			<div class="form-group">
				<label for="email" class="sr-only">Email</label>
				<input type="email" ng-model="reserveFormData.email" id="email" class="form-control" placeholder="Email" required/>
			</div>
			<div class="form-group">
				<label for="password" class="sr-only">Password</label>
				<input type="password" ng-model="reserveFormData.password" id="password" class="form-control" placeholder="Password" required/>
			</div>
			<!--<div class="form-group">
				<label for="address" class="sr-only">Indirizzo</label>
				<input type="address" ng-model="reserveFormData.address" id="address" class="form-control" placeholder="Indirizzo" required/>
			</div>-->
			<div class="form-group check">
				<label class="label--checkbox block">
                    <input class="checkbox" type="checkbox" ng-model="reserveFormData.check" id="check" required> Accetti i termini e condizioni di MangiaeBevi+
                </label>
			</div>
			<a class="link_btn" ng-click="open_login_form(1)">Sei già registrato?</a>
			<div class="form-group">
				<input type="submit" style="margin: 12px 0;" ng-disabled="reserveForm.$invalid" class="btn btn-block btn-primary ghost" value="Registrati" />
			</div>
		</form>

		<form name="loginForm" ng-if="login_form == 1">
			<h3>O compila il form<br />per accedere</h3>
			<div class="form-group">
				<label for="email" class="sr-only">Email</label>
				<input type="email" ng-model="login.email" id="email" class="form-control" placeholder="Email" required/>
			</div>
			<div class="form-group">
				<label for="password" class="sr-only">Password</label>
				<input type="password" ng-model="login.password" id="password" class="form-control" placeholder="Password" required/>
			</div>
			<p class="text-danger" ng-if="login_form_error_message != ''">{{login_form_error_message}}</p>
			<a class="link_btn" ng-click="open_recovery_form()">Recupera la password</a><br />
			<a class="link_btn" ng-click="open_resend_form()">Reinvia il link di attivazione</a><br />
			<a class="link_btn" ng-click="open_register_form()">Non sei registrato?</a>
			<div class="form-group">
				<input type="submit" style="margin: 12px 0;" ng-disabled="loginForm.$invalid" ng-click="login()" class="btn btn-block btn-primary ghost" value="Entra" />
			</div>
		</form>

		<form name="recoveryForm" ng-if="login_form == 2">
			<h3>Recupera la password<br />per accedere</h3>
			<p>Una nuova password verrà inviata al tuo indirizzo email</p>
			<p ng-if="error_recovery" class="text-danger">
				<span ng-if="error_recovery == 1">Indirizzo email non trovato</span>
				<span ng-if="error_recovery == 2">Sembra che fino ad oggi tu sia sempre entrato con i social</span>
			</p>
			<p ng-if="recovery_success" class="text-success">
				Dovresti aver ricevuto una email
			</p>
			<div class="form-group" ng-if="!recovery_success">
				<label for="email" class="sr-only">Email</label>
				<input type="email" ng-model="recovery.email" id="email" class="form-control" placeholder="Email" required/>
			</div>
			<a class="link_btn" ng-click="open_register_form()" ng-if="!recovery_success">Non sei registrato?</a>
			<div class="form-group" ng-if="!recovery_success">
				<input type="submit" style="margin: 12px 0;" ng-disabled="recoveryForm.$invalid" ng-click="recovery()" class="btn btn-block btn-primary ghost" value="Recupera" />
			</div>
		</form>

		<form name="resendForm" ng-if="login_form == 4">
			<h3>Invia nuovamente<br />il link di attivazione</h3>
			<p>Un nuovo link di attivazione verrà inviato al tuo indirizzo email</p>
			<p ng-if="error_resend" class="text-danger">
				<span ng-if="error_resend == 1">Indirizzo email non trovato</span>
				<span ng-if="error_resend == 2">Sembra che fino ad oggi tu sia sempre entrato con i social</span>
			</p>
			<p ng-if="resend_success" class="text-success">
				Dovresti aver ricevuto una email
			</p>
			<div class="form-group" ng-if="!resend_success">
				<label for="email" class="sr-only">Email</label>
				<input type="email" ng-model="resend.email" id="email" class="form-control" placeholder="Email" required/>
			</div>
			<a class="link_btn" ng-click="open_login_form()">Accedi</a>
			<div class="form-group" ng-if="!resend_success">
				<input type="submit" style="margin: 12px 0;" ng-disabled="resendForm.$invalid" ng-click="resend()"
				class="btn btn-block btn-primary ghost" value="Reinvia" />
			</div>
		</form>

	</div>
</div>