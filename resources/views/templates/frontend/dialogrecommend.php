<div class="form-info">
	<h3 ng-if="!current_plate">Raccomanda {{current_restaurant.name}} alla community</h3>
	<h3 ng-if="current_plate">Raccomanda {{current_plate.name}} alla community</h3>
	<form name="recommendCommunityForm" ng-submit="publishRecommendation()">		
		<div class="form-group">
			<label for="message">Messaggio</label>
			<textarea msd-elastic ng-model="recommendCommunityFormData.text" id="message" rows="3" class="form-control" style="min-height: 72px;" required>
			</textarea>
		</div>		
		<div class="form-group">	
			<input type="submit" ng-disabled="recommendCommunityForm.$invalid" class="btn btn-primary ghost btn-no-margin" value="Conferma" />
		</div>
	</form>
</div>