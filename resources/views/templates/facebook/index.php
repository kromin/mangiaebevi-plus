<div id="restaurantPage" ng-controller="FacebookFrameCtrl" ng-init="initPage('<?php echo $page;?>')">
	<!-- blocco menu -->
    <div class="minimal_tabs no_print">
        <ul class="list-unstyled">
            <li ng-class="{'active' : !current_action || current_action == 0}">
                <a ng-click="current_action = 0">Prenota</a>
            </li>
            <li ng-class="{'active' : current_action == 1}">
                <a ng-click="current_action = 1">Menu</a>
            </li>
        </ul>
    </div>
	<div ng-if="current_action == 0 || !current_action" class=" widget_reservation" set-class-when-at-top="active"
        ng-if="(current_restaurant.user && current_restaurant.user != 0)
        || (current_restaurant.contact.email && current_restaurant.contact.email != '')">
        <div class="reservation-header">
            Prenota un tavolo
        	<div class="text-center">
        		<img class="logo_img" ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/logo.png';?>"
        		alt="MangiaeBevi+" title="" />
        	</div>
        </div>

        <div class="form-content" ng-if="!reservation_as_to_add_phone">

            <div class="cont">
                <form class="form-inline">

                	<div ng-if="!testing">
                        <h4 class="uppercase text-center">{{current_restaurant.name}}</h4>
                	   <p class="text-center">{{current_restaurant.address}}</p>
                    </div>
                    <div ng-if="testing">
                        <h4 class="uppercase text-center">Nome del ristorante</h4>
                       <p class="text-center">Indirizzo del tuo ristorante</p>
                    </div>
                	<div class="picker-container">
                		<div id="datetimepicker-day" style="width: 100%"
                            ng-model="current_picker.date"
                            date-picker
	                		date-change="takeAvaibleFasce"
	                        view="date"
	                        min-view="date"
	                        max-view="date"
	                        format="DD/MM/YYYY"
	                        watch-direct-changes="true"
	                        ></div>

	                </div>

	                <div class="clearfix"></div>

	                    <div class="row">

	                        <!--<input type="text" id="datetimepicker-day" class="form-control semi-date" placeholder="Data" style="position: relative;" />
	                        -->
	                        <div class="col-xs-6">
	                        	<i class="mebi-time"></i>
								<select ng-model="reservation.hour" class="form-control">
		                            <option ng-repeat="fascia_oraria in avaible_fasce" value="{{fascia_oraria}}">{{fascia_oraria}}</option>
		                        </select>
	                        </div>
	                        <div class="col-xs-6">
	                        	<i class="mebi-avatar"></i>
								<select ng-model="reservation.people" class="form-control">
		                            <option ng-repeat="person in people" value="{{person}}">{{person}}</option>
		                        </select>
	                        </div>


	                    </div>

                    <div class="clearfix"></div>
                </form>

                <div class="send_button" ng-if="!reservation_confirmed && !testing">
                    <a class="btn btn-primary ghost dark btn-block disabled" id="reservation_btn" ng-click="reserve()">Prenota</a>
                </div>
                <div class="send_button" ng-if="!reservation_confirmed && testing">
                    <a class="btn btn-primary ghost dark btn-block disabled">Prenota</a>
                </div>
                <p style="margin-top: 11px;" ng-if="reservation_confirmed">{{reservation_confirmed_text}}</p>


            </div>
        </div>

        <div class="form-content" ng-if="reservation_as_to_add_phone">
            <div class="cont">
                <form>
                    <div class="form-group">
                        <label>Numero di telefono</label>
                        <input class="form-control" type="text" ng-model="user_phone.phone" />
                    </div>
                    <div class="form-group">
                        <a class="btn btn-primary btn-block ghost" ng-click="addPhoneNumber()">
							Conferma
						</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div ng-if="current_action == 1">

        <div class="menu_container" id="menuContainer" ng-if="current_restaurant.menu.menus && current_restaurant.menu.menus.length > 0">
            <ul class="list-unstyled menu-titles">
                <li ng-repeat="menu in current_restaurant.menu.menus track by $index" ng-click="selectMenu($index)" ng-if="menu.categorie.length > 0" ng-class="{'active':currentMenu==$index}">
                    {{menu.name}}
                </li>
            </ul>
            <div class="standalone_menu" ng-if="((current_restaurant.menu.menus.length)-1) >= currentMenu">
                <div class="menu"
                    ng-class="{'style_one' : !current_restaurant.menu.menus[currentMenu].menu_style || current_restaurant.menu.menus[currentMenu].menu_style == 0,
                               'style_two' : current_restaurant.menu.menus[currentMenu].menu_style == 1,
                               'style_tree' : current_restaurant.menu.menus[currentMenu].menu_style == 2,
                               'style_four' : current_restaurant.menu.menus[currentMenu].menu_style == 3}">
                    <div class="menu_title">
                        <h3>{{current_restaurant.menu.menus[currentMenu].name}}</h3>
                    </div>
                    <div class="pre_text">
                        <p class="wrap_text">{{current_restaurant.menu.menus[currentMenu].pre_text}}</p>
                    </div>
                    <div class="menu_price" ng-if="current_restaurant.menu.menus[currentMenu].menu_type == 1">
                        <p class="menu_price_text">Menu a prezzo fisso</p>
                        <p class="menu_price_price">€ {{current_restaurant.menu.menus[currentMenu].menu_price}}</p>
                    </div>
                    <div class="menu_category" ng-repeat="(i_cat,cat) in current_restaurant.menu.menus[currentMenu].categorie">
                        <div class="clearfix"></div>
                        <div class="menu_category_title">
                            <h4>{{cat.name}}</h4>
                        </div>
                        <div class="menu_plate"
                            ng-class-even="'even'"
                            ng-repeat="(i_piatto, piatto) in current_restaurant.menu.menus[currentMenu].categorie[i_cat].piatti">
                            <div class="contain_plate">
                                <div class="plate_image" ng-if="piatto.image && piatto.image != ''">
                                    <img ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/menu/small/{{piatto.image}}.png" alt="plate image" />
                                    &nbsp;
                                </div>
                                <div class="plate_content" ng-class="{'with_price' : current_restaurant.menu.menus[currentMenu].menu_type == 0, 'with_image' : piatto.image && piatto.image != ''}">
                                    <h5>{{piatto.name}}</h5>
                                    <p>{{piatto.description}}</p>
                                    <p class="plate_ingredienti" ng-if="piatto.ingredienti.length > 0"><b>Ingredienti: </b> <span ng-repeat="ing in piatto.ingredienti">{{ing}}{{$last ? '' : ', '}}</span></p>
                                    <p class="plate_allergeni" ng-if="piatto.allergeni.length > 0"><b>Allergeni: </b> <span ng-repeat="all in piatto.allergeni">{{all}}{{$last ? '' : ', '}}</span></p>
                                    <div class="plate_options" ng-repeat="(i_opt_cat, opt_cat) in piatto.options">
                                        <p><b>{{opt_cat.name}}</b></p>
                                        <ul class="option_style">
                                            <li ng-repeat="(i_opzione,opzioni) in opt_cat.options">
                                                <span class="option_name">{{opzioni.name}}</span>
                                                <span class="option_addition" ng-if="opzioni.no_variation == 0 && opzioni.variation == 0">
                                                    + {{opzioni.price}} €
                                                </span>
                                                <span class="option_addition" ng-if="opzioni.no_variation == 0 && opzioni.variation == 1">
                                                    - {{opzioni.price}} €
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="plate_price" ng-if="current_restaurant.menu.menus[currentMenu].menu_type == 0">
                                    <p><span class="euro">€</span> <span class="price">{{piatto.price}}</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="row_separator" />
                    </div>
                    <div class="post_text">
                        <p class="wrap_text" ng-bind="current_restaurant.menu.menus[currentMenu].post_text"></p>
                    </div>
                </div>
            </div>
        <?php /*
            <ul class="list-unstyled menu_tipe_selection_ul" ng-if="visual_menu == 1">
                <li ng-click="setMenuType(0)" ng-class="{'active':menu_type==0}"><i class="fa fa fa-list"></i> testuale</li>
                <li ng-click="setMenuType(1)" ng-class="{'active':menu_type==1}"><i class="fa fa fa-picture-o"></i> visuale</li>
            </ul>

            <div class="fixed_price_menu" ng-if="current_restaurant.menu.menus[currentMenu].menu_type == 1">
                <div class="fixed_price_container">
                    <div class="col-xs-4"><label>Menu a prezzo fisso</label></div>
                    <div class="col-xs-8 price_content">{{current_restaurant.menu.menus[currentMenu].menu_price}} €</div>
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- text menu -->
            <div class="menu category" ng-repeat="(cIndex, categoria)  in current_restaurant.menu.menus[currentMenu].categorie track by $index" ng-class="{'no-border-bottom' : $index+1==current_restaurant.menu.menus[currentMenu].categorie.length}" ng-if="menu_type==0">
                <h4 class="col-sm-12 categoria-name">{{categoria.name}}</h4>
                <div class="row small_bottom_margin" ng-repeat="plate in categoria.piatti track by $index" ng-if="$index%2==0">
                    <div class="col-sm-6 single_plate">
                        <!--categoria.piatti[$index]-->
                        <div class="left">
                            <h5>{{categoria.piatti[$index].name}}</h5>
                            <p class="ingredients" ng-if="categoria.piatti[$index].ingredienti && categoria.piatti[$index].ingredienti.length > 0">
                                <span><b>Ingredienti:</b> </span> <i ng-repeat="ingrediente in categoria.piatti[$index].ingredienti">{{ingrediente}} </i>
                            </p>
                            <p class="ingredients" ng-if="categoria.piatti[$index].allergeni && categoria.piatti[$index].allergeni.length > 0">
                                <span><b>Allergeni:</b> </span> <i ng-repeat="allergene in categoria.piatti[$index].allergeni">{{allergene}} </i>
                            </p>
                            <p class="desc">{{categoria.piatti[$index].description}}</p>

                        </div>
                        <div class="right" ng-if="!current_restaurant.menu.menus[currentMenu].menu_type || current_restaurant.menu.menus[currentMenu].menu_type != 1">
                            <p class="price pull-right">{{categoria.piatti[$index].price | number : 2}} €</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-6 single_plate" ng-if="categoria.piatti.length > ($index + 1)">
                        <div class="left">
                            <h5>{{categoria.piatti[$index+1].name}}</h5>
                            <p class="ingredients" ng-if="categoria.piatti[$index+1].ingredienti && categoria.piatti[$index+1].ingredienti.length > 0">
                                <span><b>Ingredienti:</b> </span> <i ng-repeat="ingrediente in categoria.piatti[$index+1].ingredienti">{{ingrediente}} </i>
                            </p>
                            <p class="ingredients" ng-if="categoria.piatti[$index+1].allergeni && categoria.piatti[$index+1].allergeni.length > 0">
                                <span><b>Allergeni:</b> </span> <i ng-repeat="allergene in categoria.piatti[$index+1].allergeni">{{allergene}} </i>
                            </p>
                            <p class="desc">{{categoria.piatti[$index+1].description}}</p>

                        </div>
                        <div class="right" ng-if="!current_restaurant.menu.menus[currentMenu].menu_type || current_restaurant.menu.menus[currentMenu].menu_type != 1">
                            <p class="price pull-right">{{categoria.piatti[$index+1].price | number : 2}} €</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

            <!-- visual menu -->
            <div class="visual_menu category" ng-repeat="(cIndex, categoria)  in current_restaurant.menu.menus[currentMenu].categorie track by $index" ng-class="{'no-border-bottom' : $index+1==current_restaurant.menu.menus[currentMenu].categorie.length}" ng-if="menu_type==1">
                <h4 class="col-sm-12 categoria-name">{{categoria.name}}</h4>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 small_pad"
                    ng-repeat="plate in categoria.piatti track by $index">
                        <div class="single_plate">
                            <div class="plate_image">
                                <img ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN');?>/menu/small/{{plate.image}}.png" ng-if="plate.image && plate.image != ''" alt="" title="" />
                                <img ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_DOMAIN');?>/img/no_plate.png" ng-if="!plate.image || plate.image == ''" alt="" title="" />
                            </div>
                            <div class="text">
                                <h5>{{plate.name}}</h5>
                                <div class="price" ng-if="!current_restaurant.menu.menus[currentMenu].menu_type || current_restaurant.menu.menus[currentMenu].menu_type != 1">
                                    {{categoria.piatti[$index].price | number : 2}} €

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <p ng-repeat="asterisco in current_restaurant.menu.menus[currentMenu].asterischi track by $index">{{asterisco}}</p>
        </div>*/?>
        <!--<div class="iframe_menu_logo" style="background-color: #00b4cc; padding: 15px; color: #fff;">-->
        <p class="text-primary" style="line-height: 50px;">Menu Powered By
            <img class="logo_img" ng-src="<?php echo env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/logo_blue.png';?>"
            alt="Menoo" title="" style="height: 20px; width: auto; display: inline-block;" />
        </p>
        <!--</div>-->
    </div>
</div>

</div>
<div class="clearfix"></div>
