var gulp = require('gulp');

var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglifyjs');
var gutil = require('gulp-util');

var watch = require('gulp-watch');
var less = require('gulp-less');
var path = require('path');
var cleanCSS = require('gulp-clean-css');
var minify = require('gulp-minify');

var ngAnnotate = require('gulp-ng-annotate');

gulp.task('default', function() {
    gulp.watch('js/**/*.js', ['javascript','iframe','facebook']);
    gulp.watch('less/**/*.less', ['less']);
});

gulp.task('watch-js', function() {
    gulp.watch('js/**/*.js', ['javascript']);
});

gulp.task('watch-less', function() {
    gulp.watch('less/**/*.less', ['less']);
});

gulp.task('javascript', function() {
    return gulp.src(
        ['js/lib/moment/moment.js',
        'js/lib/external/modernizr.js',
        'js/lib/d3/d3.js', 
        'js/lib/d3/heatmap.js',         
        'js/lib/jquery/dist/jquery.min.js', 
        'js/lib/external/jquery.mobile.custom.min.js',
        'js/template/vendor/bootstrap/bootstrap.min.js', 
        'js/lib/sweetalert/sweetalert.js',
        'js/lib/external/scrollreveal.js',
        'js/lib/underscore/underscore-min.js',        
        'js/lib/velocity/velocity.min.js',
        'js/lib/velocity/velocity.ui.min.js',
        'js/lib/external/livequery.js',
        'js/lib/scroll/scrollbar.js',
        'js/lib/external/datetimepicker.js',
        'js/lib/markers/markerCluster.js', 
        'js/lib/functions.js',
        'js/lib/angular/angular.js',
        'js/lib/cleave/cleave.min.js',
        'js/lib/cleave/cleave.phone.js', 
        'js/lib/ngmask.js',
        'js/lib/chart/chartlib.js',
        'js/lib/chart/charts.js',
        'js/lib/analytics/ganalytics.js',
        'js/lib/ng-scrollbar/src/ng-scrollbar.js',
        'js/lib/sweetalert/sweetalert-angular.js', 
        'js/lib/priceformat/priceformat.js', 
        'js/lib/tagger/tagger.js', 
        'js/lib/external/sortable.js',
        'js/lib/sortable/ngsortable.js',
        'js/lib/angular-dragndrop/angular-dragndrop.js',
        'js/lib/d3/heatmap-directive.js',
        'js/lib/textangular/textAngular-rangy.min.js',
        'js/lib/textangular/textAngular-sanitize.min.js',
        'js/lib/textangular/textAngular.min.js',
        'js/lib/autocomplete/googleautocomplete.js',
        'js/lib/elastic/elastic.js',
        'js/lib/scroll/scrollto.js',
        'js/lib/angular-route/angular-route.js',
        'js/lib/angular-animate/angular-animate.min.js',
        'js/lib/angular-cookies/angular-cookies.min.js',
        'js/lib/angular-aria/angular-aria.min.js',
        'js/lib/external/datetimepickerDirective.js',
        'js/lib/external/ngfileupload.js',
        'js/lib/alertify/src/js/ngAlertify.js',        
        'js/lib/moment/locale/it.js',         
        'js/lib/angular-moment/angular-moment.min.js', 
        'js/lib/angular-datepicker/picker.js', 
        'js/lib/ng-scrollbar/dist/ng-scrollbar.min.js',
        'js/lib/external/mousewheel.min.js',
        'js/lib/external/scrollspy/duscroll.js',        
        'js/lib/external/dialog.js',
        'js/lib/external/datepicker.js',
        'js/lib/braintree/angular-braintree.js',
        'js/lib/locationUpdate/location.js',
        'js/application/main/frontend/app.js',
        'js/application/main/shared/directives.js',        
        'js/application/services/shared/UserServices.js',
        'js/application/services/shared/NewsLetterServices.js',        
        'js/application/services/shared/ServicesServices.js',
        'js/application/services/shared/RegionsServices.js',
        'js/application/services/shared/TypesServices.js',
        'js/application/services/shared/LocalityServices.js',
        'js/application/services/shared/CreditCardServices.js',
        'js/application/services/shared/NotificationsServices.js',
        'js/application/services/shared/BlogArticlesServices.js',
        'js/application/services/shared/BlogCategoriesServices.js',
        'js/application/services/shared/ImagesServices.js',
        'js/application/services/shared/IframeServices.js',
        'js/application/services/shared/RestaurantServices.js',
        'js/application/services/shared/ToolsServices.js',
        'js/application/services/shared/PositionServices.js',
        'js/application/services/shared/ReservationServices.js',
        'js/application/services/shared/SocialServices.js',
        'js/application/services/shared/RecommendationServices.js',
        'js/application/services/shared/SearchServices.js',
        'js/application/services/shared/PlatesServices.js',
        'js/application/controllers/frontend/BodyController.js',
        'js/application/controllers/frontend/HeaderController.js',
        'js/application/controllers/frontend/HomeController.js',
        'js/application/controllers/frontend/ApplicationController.js',
        'js/application/controllers/frontend/LoginController.js',
        'js/application/controllers/frontend/RegistrationController.js',
        'js/application/controllers/frontend/TopbarController.js',
        'js/application/controllers/frontend/RestaurantController.js',
        'js/application/controllers/frontend/SearchController.js',
        'js/application/services/shared/PaymentServices.js',
        'js/application/controllers/frontend/LogPanelController.js',
        'js/application/controllers/frontend/ProfileController.js',
        'js/application/controllers/frontend/UserController.js',
        'js/application/controllers/frontend/PlateController.js',
        'js/application/controllers/frontend/UserPlateController.js',        
        'js/application/controllers/frontend/UploadController.js',
        'js/application/controllers/frontend/DialogLoginController.js',
        'js/application/controllers/frontend/DialogRegisterController.js',
        'js/application/controllers/frontend/AddRestaurantController.js',
        'js/application/controllers/frontend/EditRestaurantController.js',
        'js/application/controllers/frontend/StaticController.js',       
        'js/application/controllers/frontend/MapController.js',
        'js/application/services/frontend/GoogleMapService.js',
        'js/application/controllers/frontend/MenuManagement.js',
        'js/application/controllers/frontend/StrutturaRestaurant.js',
        'js/application/controllers/frontend/ReservationRestaurant.js',
        'js/application/controllers/frontend/MenuPublishingController.js',
        'js/application/controllers/frontend/RestaurantClientsController.js',
        'js/application/controllers/frontend/StatController.js',
        'js/application/controllers/frontend/UserTopProfile.js',
        'js/application/controllers/frontend/UserDashboard.js',
        'js/application/controllers/frontend/UserFavourite.js',
        'js/application/controllers/frontend/UserDaProvare.js',
        'js/application/controllers/frontend/UserRaccomandazioni.js',
        'js/application/controllers/frontend/UserNotifiche.js',
        'js/application/controllers/frontend/UserPiatti.js',
        'js/application/controllers/frontend/UserFriends.js',
        'js/application/controllers/frontend/UserAccount.js',
        'js/application/controllers/frontend/UserRestaurants.js',
        'js/application/controllers/frontend/UserReservations.js',
        'js/application/controllers/frontend/SclobyController.js',
        'js/application/services/shared/OrdersServices.js',
        'js/application/controllers/frontend/MenuPreOrderController.js'])        
        .pipe(concat('script.js',{newLine: ';'})) 
        .pipe(ngAnnotate())
        //.pipe(uglify().on('error', gutil.log))    
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest('../../public/'));
});

gulp.task('iframe', function() {
    return gulp.src(
        [
        'js/lib/moment/moment.js', 
        'js/lib/external/modernizr.js',         
        'js/lib/jquery/dist/jquery.min.js', 
        'js/lib/external/jquery.mobile.custom.min.js',
        'js/template/vendor/bootstrap/bootstrap.min.js', 
        'js/lib/sweetalert/sweetalert.js',
        'js/lib/underscore/underscore-min.js',        
        'js/lib/external/livequery.js',
        'js/lib/external/datetimepicker.js',
        'js/lib/functions.js',
        'js/lib/angular/angular.js',
        'js/lib/sweetalert/sweetalert-angular.js', 
        'js/lib/angular-route/angular-route.js',
        'js/lib/angular-animate/angular-animate.min.js',
        'js/lib/angular-cookies/angular-cookies.min.js',
        'js/lib/angular-aria/angular-aria.min.js',
        'js/lib/external/datetimepickerDirective.js',
        'js/lib/alertify/src/js/ngAlertify.js',        
        'js/lib/moment/locale/it.js', 
        'js/lib/angular-moment/angular-moment.min.js', 
        'js/lib/angular-datepicker/picker.js', 
        'js/lib/external/mousewheel.min.js',
        'js/lib/external/dialog.js',
        'js/lib/external/datepicker.js',
        'js/application/main/iframe/app.js',
        'js/application/main/shared/directives.js',        
        'js/application/services/shared/UserServices.js',
        'js/application/services/shared/RestaurantServices.js',
        'js/application/services/shared/ToolsServices.js',
        'js/application/services/shared/ReservationServices.js',
        'js/application/services/shared/SearchServices.js',
        'js/application/controllers/frontend/HeaderController.js',
        'js/application/controllers/iframe/RestaurantController.js',
        'js/application/controllers/frontend/LogPanelController.js',
        'js/application/controllers/frontend/DialogLoginController.js',
        'js/application/controllers/frontend/DialogRegisterController.js',
        'js/application/controllers/frontend/LogPanelController.js'])        
        .pipe(concat('script_iframe.js',{newLine: ';'})) 
        .pipe(ngAnnotate())
        .pipe(gulp.dest('../../public/'));
});

gulp.task('facebook', function() {
    return gulp.src(
        [
        'js/lib/moment/moment.js', 
        'js/lib/external/modernizr.js',  
        'js/lib/jquery/dist/jquery.min.js', 
        'js/lib/external/jquery.mobile.custom.min.js',
        'js/template/vendor/bootstrap/bootstrap.min.js', 
        'js/lib/sweetalert/sweetalert.js',
        'js/lib/underscore/underscore-min.js',  
        'js/lib/external/livequery.js',
        'js/lib/external/datetimepicker.js',
        'js/lib/functions.js',
        'js/lib/angular/angular.js',
        'js/lib/sweetalert/sweetalert-angular.js', 
        'js/lib/priceformat/priceformat.js', 
        'js/lib/elastic/elastic.js',
        'js/lib/scroll/scrollto.js',
        'js/lib/angular-route/angular-route.js',
        'js/lib/angular-animate/angular-animate.min.js',
        'js/lib/angular-cookies/angular-cookies.min.js',
        'js/lib/angular-aria/angular-aria.min.js',
        'js/lib/external/datetimepickerDirective.js',
        'js/lib/alertify/src/js/ngAlertify.js',        
        'js/lib/moment/locale/it.js', 
        'js/lib/angular-moment/angular-moment.min.js', 
        'js/lib/angular-datepicker/picker.js', 
        'js/lib/ng-scrollbar/dist/ng-scrollbar.min.js', 
        'js/lib/external/mousewheel.min.js',
        'js/lib/external/scrollspy/duscroll.js',
        'js/lib/external/dialog.js',
        'js/lib/external/datepicker.js',
        'js/lib/braintree/angular-braintree.js',
        'js/lib/locationUpdate/location.js',
        'js/application/main/facebook/main.js',
        'js/application/main/shared/directives.js',        
        'js/application/services/shared/UserServices.js',
        'js/application/services/shared/RestaurantServices.js',
        'js/application/services/shared/ToolsServices.js',
        'js/application/services/shared/PositionServices.js',
        'js/application/services/shared/ReservationServices.js',
        'js/application/services/shared/RecommendationServices.js',
        'js/application/controllers/frontend/ApplicationController.js',
        'js/application/controllers/frontend/LoginController.js',
        'js/application/controllers/facebook/FacebookFrameController.js',
        'js/application/controllers/frontend/LogPanelController.js',
        'js/application/controllers/frontend/ProfileController.js',
        'js/application/controllers/frontend/UserController.js',
        'js/application/controllers/frontend/DialogLoginController.js',
        'js/application/controllers/frontend/DialogRegisterController.js'])        
        .pipe(concat('script_facebook.js',{newLine: ';'})) 
        .pipe(ngAnnotate())
        .pipe(gulp.dest('../../public/'));
});

gulp.task('less', function () {
    return gulp.src('less/app.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('../../public/'));
});




gulp.task('minify-js', function() {
    return gulp.src(
        '../../public/script.js') 
        .pipe(uglify({
            mangle: true,
            outSourceMap: true,
            compress: {
                sequences: true,
                dead_code: true,
                conditionals: true,
                booleans: true,
                unused: true,
                if_return: true,
                join_vars: true,
                drop_console: true
            }
        }))  
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('../../public/'));
});

gulp.task('minify-iframe-js', function() {
    return gulp.src(
        '../../public/script_iframe.js')
        .pipe(uglify({
            mangle: true,
            outSourceMap: true,
            compress: {
                sequences: true,
                dead_code: true,
                conditionals: true,
                booleans: true,
                unused: true,
                if_return: true,
                join_vars: true,
                drop_console: true
            }
        }))  
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('../../public/'));
});

gulp.task('minify-facebook-js', function() {
    return gulp.src(
        '../../public/script_facebook.js')
        .pipe(uglify({
            mangle: true,
            outSourceMap: true,
            compress: {
                sequences: true,
                dead_code: true,
                conditionals: true,
                booleans: true,
                unused: true,
                if_return: true,
                join_vars: true,
                drop_console: true
            }
        }))  
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('../../public/'));
});

gulp.task('minify-css', function() {
    return gulp.src(
        '../../public/app.css')
        .pipe(cleanCSS())    
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('../../public/'));
});


gulp.task('ngdocs', [], function () {
    var gulpDocs = require('gulp-ngdocs');
    var options = {
        html5Mode: true,
        startPage: '/apiDocJS',
        title: "Angular application",
        titleLink: "/apiDocJS"
    };

  return gulp.src('js/application/services/shared/*.js')///*.js')
    .pipe(gulpDocs.process())
    .pipe(gulp.dest('../../public/apiDocJS'));
});