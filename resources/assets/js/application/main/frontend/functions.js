$(function(){
	$(".custom_scrollbar").livequery(function(){
		$(this).mCustomScrollbar({
	        axis: "y",
	        scrollInertia: 0,
	        alwaysShowScrollbar: 0,
	        mouseWheel:{ enable: true },
	        scrollButtons:{ enable: false },
	        contentTouchScroll: 25,
	        theme: "dark-2"
	    }); 
	}) 
})