(function() {
    var app = angular.module('app', 
        [ 
            'ngRoute',
            'ngAnimate',
            'ngAria',
            'ngCookies',
            'angularMoment', 
            'duScroll',
            'ngFileUpload',  
            'ngDialog',
            'ngAlertify', 
            'cleave.js',
            'ngMask',
            '19degrees.ngSweetAlert2',
            'html5.sortable',
            "chart.js",
            'angular-price-format', 
            'scrollto',
            'ngScrollbar',
            'calHeatmap',
            'draganddrop',
            'textAngular',
            'angular-google-analytics',
            'tagger',
            'as.sortable', 
            'datePicker',
            'ngAutocomplete',
            'ngLocationUpdate',
            'braintree-angular',
            'angular-date-picker-polyfill',
            'monospaced.elastic',
            'MEBUserServices', 
            'MEBNewsLetterServices',
            'MEBServicesServices',
            'MEBRegionsServices',
            'MEBTypesServices',
            'MEBLocalityServices',
            'MEBCreditCardServices',
            'MEBRestaurantsServices',
            'MEBBlogArticlesServices',
            'MEBBlogCategoryServices',
            'MEBToolsServices',
            'MEBImagesServices',
            'MEBRestaurantsServices',
            'MEBdirectives',
            'MEBPositionServices',
            'MEBSearchServices',
            'AppApplicationController',
            'AppHeaderController',
            'AppHomeController',
            'AppLoginController',
            'AppRegistrationController',
            'AppRecoveryController',
            'AppResendController',
            'AppTopbarController',
            'AppRestaurantController',
            'AppMapController',
            'MEBGoogleMapServices',
            'MEBReservationServices',
            'AppDialogLoginController',
            'AppDialogRegisterController',
            'AppSearchController',
            'AppLogPanelController',
            'MEBPaymentServices',
            'AppAddRestaurantController',
            'AppProfileController',
            'MEBNotificationsServices',
            'AppStaticController',
            'AppEditRestaurantController',
            'AppUserController',
            'MEBSocialServices',
            'MEBRecommendationServices',
            'AppPlateController',
            'MEBPlatesServices',
            'AppUserPlateController',
            'MEBIframeServices',
            'AppUploadController',
            'AppEditMenuManagementController',
            'AppEditStrutturaController',
            'AppEditReservationsController',
            'AppMenuPublishingController',
            'AppClientsRestaurantController',
            'AppStatRestaurantController',
            'AppUserTopProfileController',
            'AppUserDashboardController',
            'AppUserDaProvareController',
            'AppUserFavouriteController',
            'AppUserPiattiController',
            'AppUserReservationsController',
            'AppUserNotificationsController',
            'AppUserAccountController',
            'AppUserRestaurantsController',
            'AppUserFriendsController',
            'AppUserRaccomandazioniController',
            'AppEditSclobyController',
            'AppOrdersServices', 
            'AppMenuPreOrderController'    
        ])      
        .config([
            '$httpProvider','AnalyticsProvider',
            function($httpProvider, $cookies, AnalyticsProvider) {
                var interceptor = [
                    '$q', '$cookies', '$injector',
                    function($q, $cookies, $injector) {

                        
                        var rendering_end = false;
                        $timeout = $injector.get('$timeout');
                        $rootScope = $injector.get('$rootScope');
                        $rootScope.$on("$locationChangeStart",function(event, next, current){
                            rendering_end = false;
                        });

                        var service = {
                            // run this function before making requests
                            'request': function(config) {
                                //config.headers['X-CSRF-TOKEN'] = $cookies.get('XSRF-TOKEN');		
                                //config.url = (config.url[0] == '/') ? '/attivita' + config.url : '/attivita/' + config.url;
                                console.log(config);
                                return config || $q.when(config);
                            },

                            // On request failure
                            requestError: function(rejection) {
                                // //console.log(rejection); // Contains the data about the error on the request.             
                                // Return the promise rejection.
                                //clearTimeout(timeout);
                                return $q.reject(rejection);
                            },

                            // run this function before getting responses
                            'response': function(response) {
                                var url = response.config.url
                                    //console.warn("URL",url)            
                                    //console.warn("response DATA",response)
                                console.log('response', response)
                                if(response.data.message != 'page_rendered' && !rendering_end){
                                    $http = $injector.get('$http');
                                    if($http.pendingRequests.length < 1) {
                                        $timeout(function(){
                                            if($http.pendingRequests.length < 1){
                                                //$rootScope.htmlReady();
                                                $rootScope.$broadcast('page loaded');
                                                rendering_end = true;
                                            }
                                        }, 700);
                                    }
                                }

                                    //clearTimeout(timeout);
                                return response || $q.when(response);
                            },

                            // optional method
                            'responseError': function(rejection) {
                                // do something on error
                                //if (rejection.data && rejection.data[0]) {
                                //  rejection.data = rejection.data[0];
                                //} 
                                
					//403 viene usato per errori exception:
					//{
					//  "errorException": {
					//    "exception": "NotReadableException in AbstractDecoder.php line 302: Image source not readable",
					//    "exception_message": "Image source not readable"
					//  }
					//}

					//422 errori di validazione:
					//[
					//	{
					//		parametro: 'validation.nome_errore'
					//	}
					//]

		         
                                console.log(rejection)
                                    //clearTimeout(timeout);
                                return $q.reject(rejection);  
                            }
                        };
                        return service;
                    }
                ];

                $httpProvider.interceptors.push(interceptor);

            }
        ]).config(function (AnalyticsProvider) {
          // Add configuration code as desired - see below
          AnalyticsProvider.setAccount({
            tracker: 'UA-40298440-2',
            set: {
                forceSSL: true
              }
            }).logAllCalls(true)
              .trackPages(true);
        }).config(['ChartJsProvider', function (ChartJsProvider) {
            // Configure all charts
            ChartJsProvider.setOptions({
              chartColors: ['#FF5252', '#FF8A80'],
              responsive: true
            });
            // Configure all line charts
            ChartJsProvider.setOptions('line', {
              showLines: true,
              spanGaps: true
            });
        }]);
        

        /*.constant('clientTokenPath', '/api/v1/payments/getToken')
        .run(['$rootScope', '$anchorScroll', '$cookies', 'amMoment', function($rootScope, $anchorScroll, $cookies, amMoment) {
            amMoment.changeLocale($cookies.get('it'));
        }]);*/



})();