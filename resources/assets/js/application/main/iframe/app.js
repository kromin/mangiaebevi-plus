(function(){

var app = angular.module('IframeMangiaeBeviApp',
        [
        'ngRoute',
            'ngAnimate',
            'ngAria',
            'ngCookies',
            'angularMoment',   
            'ngDialog',
            'ngAlertify', 
            '19degrees.ngSweetAlert2',
            'datePicker',
            'angular-date-picker-polyfill',
            'MEBUserServices',
            'MEBRestaurantsServices',
            'MEBToolsServices',
            'MEBRestaurantsServices',
            'MEBSearchServices',
            'MEBReservationServices',
            'MEBdirectives',
            'AppRestaurantController',
            'AppDialogRegisterController',
            'AppDialogLoginController',
            'AppLogPanelController'
    ]).config(['$routeProvider','$locationProvider', // configura gli url
		function($routeProvider,$locationProvider) {
		$routeProvider		
		.when('/:restaurant',{
			pagetitle: 'MangiaeBevi',
			templateUrl: '/templates/iframe/restaurant',
			controller: 'RestaurantCtrl'
		})
            .when('/:restaurant/t/:type',{
                  pagetitle: 'MangiaeBevi',
                  templateUrl: '/templates/iframe/restaurant',
                  controller: 'RestaurantCtrl'
            })
            .when('/:restaurant/:test',{
                  pagetitle: 'MangiaeBevi',
                  templateUrl: '/templates/iframe/restaurant',
                  controller: 'RestaurantCtrl'
            })
            .when('/:restaurant/:test/t/:type',{
                  pagetitle: 'MangiaeBevi',
                  templateUrl: '/templates/iframe/restaurant',
                  controller: 'RestaurantCtrl'
            })					
		.otherwise({redirectTo: '/'})
	;
	$locationProvider
	.html5Mode(true)
	}])
    
	.config([
		'$httpProvider',   
		function($httpProvider) {
			var interceptor = [
			  '$q','$cookies',
			  function($q,$cookies) {
			    
			    var service = {
			      // run this function before making requests
			      'request': function(config) {		      	
			      	//config.headers['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content')		
			      	console.log(config);      	   

			        return config || $q.when(config);
			      },
			      
			      // On request failure
			       requestError: function (rejection) {
			         // //console.log(rejection); // Contains the data about the error on the request.             
			         // Return the promise rejection.
			         //clearTimeout(timeout);
			         return $q.reject(rejection);
			       },

			      // run this function before getting responses
			      'response': function(response) {
			        var url = response.config.url
			        //console.warn("URL",url)            
			        //console.warn("response DATA",response)
			        console.log('response',response)
			        
			        //clearTimeout(timeout);
			        return response || $q.when(response);
			      },         

			       // optional method
			      'responseError': function(rejection) {
			        return $q.reject(rejection);
			       }
			    };
			    return service;
			  }
			];


			$httpProvider.interceptors.push(interceptor);
			
		}
	])
	.constant('clientTokenPath', '/api/v1/payments/getToken')
	.run(['$rootScope','$anchorScroll','$cookies','amMoment', function($rootScope,$anchorScroll,$cookies,amMoment) {
	    amMoment.changeLocale('it');    
	}]);

       

})();
