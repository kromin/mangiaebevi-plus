var MEBapp = angular.module('MEBdirectives',[])
.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {

                var fileTypes = ['jpg', 'jpeg', 'png', 'gif', 'svg'];
                var extension = changeEvent.target.files[0].name.split('.').pop().toLowerCase();
                var size = changeEvent.target.files[0].size;

                isSuccess = fileTypes.indexOf(extension) > -1;

                if(size > 3100000){
                    alert('Immagine non valida, massimo 3Mb');
                    return;
                }
                if(!isSuccess){
                    alert('Immagine non valida, solo jpeg, png e gif');
                    return;
                }
                
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
                
                
            });
        }
    }
}])
.directive('file', function () {
    return {
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var file = event.target.files[0];
                scope.file = file ? file : undefined;
                scope.$apply();
            });
        }
    };
})
.directive('formattaFascia', function(){
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
       modelCtrl.$parsers.push(function (inputValue) {
           // this next if is necessary for when using ng-required on your input. 
           // In such cases, when a letter is typed first, this parser will be called
           // again, and the 2nd time, the value will be undefined
           if (inputValue == undefined) return ''; 
            
            inputValue = ""+inputValue 
                     
            var transformedInput = ''

            if(inputValue.length > 11){
                inputValue = inputValue.substr(0,11);
            }
            if(inputValue.length > 2){
                if(inputValue.substr(2,1) != ':'){
                    inputValue = inputValue.substr(0,2)+':';
                } 
                if(inputValue.length > 5){
                    if(inputValue.substr(5,1) != '-'){
                        inputValue = inputValue.substr(0,5)+'-';
                    }
                    if(inputValue.length > 8){
                        if(inputValue.substr(8,1) != ':'){
                            inputValue = inputValue.substr(0,8)+':';
                        }
                    }
                }   
            }
            inputValue = inputValue.replace("::",":").replace("--","-").replace(/[^\d:-]/g, "0");

            transformedInput = inputValue
             
           
            modelCtrl.$setViewValue(transformedInput);
            modelCtrl.$render();
           
            return transformedInput;         
       });
     }
   };
})
.directive("scroll", function ($window) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
            //scope.visible = false;
            //console.log(document.documentElement.scrollTop)
            //console.log(document.body.scrollTop)
            if(document.documentElement.scrollTop != 0 || document.body.scrollTop != 0){
                element.addClass('fixed');
            } else {
                element.removeClass('fixed');
            }            
            scope.$apply();
        });
    };
})
.directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                        scope.$apply(function(){
                                scope.$eval(attrs.ngEnter);
                        });
                        
                        event.preventDefault();
                }
            });
        };
})
.directive('setClassWhenAtTop', function ($window) {
  var $win = angular.element($window); // wrap window object as jQuery object

  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
        window.scrollTo(0,0);       
        var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
           offsetTop = element.offset().top,
           globalOffsetTop = element.offset().top; // get element's offset top relative to document
       
        
        $win.on('scroll', function (e) {
            offsetTop = element.offset().top;
            if ($win.scrollTop() >= offsetTop && !element.hasClass(topClass)) {
                element.addClass(topClass);
            } else {
                if($win.scrollTop() < 500){
                    
                    element.removeClass(topClass);
                    
                }
            }
        });

       
    }
  };
})
.directive('setClassWhenAtTopButtons', function ($window) {
  var $win = angular.element($window); // wrap window object as jQuery object
   
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
        window.scrollTo(0,0);       
        var topClass = attrs.setClassWhenAtTopButtons, // get CSS class from directive's attribute value
           offsetTop = element.offset().top,
           elheight = element.innerHeight(),
           globalOffsetTop = element.offset().top; // get element's offset top relative to document
       
        
        $win.on('scroll', function (e) {
            offsetTop = element.offset().top;
            if ($win.scrollTop() >= (globalOffsetTop + elheight) && !element.hasClass(topClass)) {
                element.addClass(topClass);
            } else {
                if($win.scrollTop() < (globalOffsetTop + elheight) && element.hasClass(topClass)){                    
                    element.removeClass(topClass);                    
                }
            }
        });

       
    }
  };
})
.directive('backImg', function(){
    return {
        restrict: 'EA',
        scope: {
            img: '@'
        },
        link: function(scope, element, attrs){        
                scope.$watch('img',function(newVal){
                    if(newVal){
                       attrs.$set('style','background-image: url(\''+ newVal +'\')')         
                    }
                }, true)
            }
        }
})
.directive('galleryimageonload', function() {
    return {
        restrict: 'EA',
        link: function(scope, element, attrs) {
            var classToAdd = attrs.galleryimageonload;
            element.bind('load', function() {
                element.addClass(classToAdd);
            });
            scope.$watch('ngSrc',function(newVal){
                element.removeClass(classToAdd);
            }, true)
            
        }
    };
})
.directive('autosize', function($document) {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ctrl) {
      var placeholder, span, resize;
    
      placeholder = element.attr('placeholder') || '';  
      
      span = angular.element('<span></span>');
      span[0].style.cssText = getComputedStyle(element[0]).cssText;
      span.css('display', 'none')
          .css('visibility', 'hidden')
          .css('width', 'auto');
      
      $document.find('body').append(span);
    
      resize = function(value) {
        if (value && value.length < placeholder.length) {
          value = placeholder;
        }
        span.text(value);
        span.css('display', '');
        try {
          element.css('width', span.prop('offsetWidth') + 'px');
        }
        finally {
          span.css('display', 'none');
        }
      };
      
      ctrl.$parsers.unshift(function(value) {
        resize(value);
        return value;
      });
      
      ctrl.$formatters.unshift(function(value) {
        resize(value);
        return value;
      })
    }
  };
})
.directive('formatPriceInput', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;


            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });


            ctrl.$parsers.unshift(function (viewValue) {

          elem.priceFormat({
            prefix: '',
            cents: ',',
            thousands: '.'
        });                

                return elem[0].value;
            });
        }
    };
}])
.directive("contenteditable", function() {
  return {
    restrict: "A",
    require: "ngModel",
    link: function(scope, element, attrs, ngModel) {

      if(ngModel){
        function read() {
          ngModel.$setViewValue(element.text());
        }

        ngModel.$render = function() {
          element.text(ngModel.$viewValue || "");
        };

        element.bind("blur keyup change", function() {
          scope.$apply(read);
        });
      }
    }
  };
});
/*.directive('backendPage', function($window){
    var window_width = window.innerWidth-17, // tolgo larghezza scrollbar browser
        sidebar_width = $("#sidebar").width();
    return {
        restrict: 'EA',
        scope: {
            img: '@'
        },
        link: function(scope, element, attrs){
            
            if(window_width > 1400){
                attrs.$set('style','width: '+parseInt(window_width-sidebar_width)+'px; margin-left: '+sidebar_width+'px;');
            }

            angular.element($window).bind('resize', function(){
                var window_width = window.innerWidth-17;
                if(window_width > 1400){
                    attrs.$set('style','width: '+parseInt(window_width-sidebar_width)+'px; margin-left: '+sidebar_width+'px;');
                    scope.$digest();
                } else {
                    attrs.$set('style','width: 100%; margin-left: 0');
                    scope.$digest();
                }
            });
        }
    };
});
*/