var RestaurantsAppServices    = angular.module('MEBRestaurantsServices',[]);

RestaurantsAppServices
.factory('RestaurantsServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {			
			viewAll: {
				async: function() {		    	
					
			      	var request = $http.post('/api/v1/restaurants/view_all',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAllFiltered: {
				async: function() {		    	
					
			      	var request = $http.post('/api/v1/restaurants/view_all_filtered',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAllClients: {
				async: function() {		    	
					
			      	var request = $http.post('/api/v1/restaurants/view_all_clients',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAllFilteredClients: {
				async: function() {   	
					
			      	var request = $http.post('/api/v1/restaurants/view_all_filtered_clients',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAllMine: {
				async: function() {		    	
					
			      	var request = $http.post('/api/v1/restaurants/view_all_mine',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewByUser: {
				async: function(user) {		    	
					
			      	var request = $http.post('/api/v1/restaurants/view_by_user',
			      	{
			      		user_id: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAllMineClaimed: {
				async: function() {		    	
					
			      	var request = $http.post('/api/v1/restaurants/view_all_mine_claimed',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAllClaimed: {
				async: function(user) {		    	
					
			      	var request = $http.post('/api/v1/restaurants/view_all_claimed',
			      	{
			      		user_id: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAllFilteredMine: {
				async: function() {		    	
					
			      	var request = $http.post('/api/v1/restaurants/view_all_mine_filtered',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewSingle: {
				async: function(id) {   						
			      	var request = $http.post('/api/v1/restaurants/view_single',
			      	{			  
			      		id: id    		
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addSingle: {
				async: function(obj) {   						
			      	var request = $http.post('/api/v1/restaurants/add_single',
			      	{			  
			      		name: obj.name,
			      		description: obj.description,
			      		tipoLocale: obj.tipoLocale,
			      		address: obj.address,
			      		city: obj.city,
			      		state: obj.state,
			      		zip: obj.zip,
			      		web: (obj.web && obj.web != '') ? obj.web : null,
			      		video: (obj.video) ? obj.video : null,
			      		email: obj.email,
			      		phone: obj.phone,
			      		virtualTour: (obj.virtualTour) ? obj.virtualTour : null,
			      		priceMin: obj.priceMin,
			      		priceMax: obj.priceMax,
			      		staff: obj.staff,
			      		social: obj.social,
			      		services: obj.services,
						tipoCucina: obj.tipoCucina,
						regioneCucina: obj.regioneCucina,
						carte: obj.carte,
						fasce: obj.fasce,
						image: (obj.image) ? obj.image : null,
						images: (obj.images) ? obj.images : []
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			editSingle: {
				async: function(id,obj) {   						
			      	var request = $http.post('/api/v1/restaurants/edit_single',
			      	{			
			      		restaurant_id: id,  
			      		name: obj.name,
			      		description: obj.description,
			      		tipoLocale: obj.tipoLocale,
			      		address: obj.address,
			      		city: obj.city,
			      		state: obj.state,
			      		zip: obj.zip,
			      		web: (obj.web) ? obj.web : (obj.contact.web) ? obj.contact.web : null,
			      		video: (obj.video) ? obj.video : null,
			      		email: (obj.email) ? obj.email : (obj.contact.email) ? obj.contact.email : null,
			      		phone: (obj.phone) ? obj.phone : (obj.contact.phone) ? obj.contact.phone : [],
			      		virtualTour: (obj.virtualTour) ? obj.virtualTour : null,
			      		priceMin: obj.priceMin,
			      		priceMax: obj.priceMax,
			      		staff: obj.staff,
			      		social: obj.social,
			      		services: obj.services,
						tipoCucina: obj.tipoCucina,
						regioneCucina: obj.regioneCucina,
						carte: obj.carte,
						image: (obj.image) ? obj.image : null,
						images: (obj.images) ? obj.images : [],
						fasce: (obj.fasce) ? obj.fasce : []
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			editSingleAdmin: {
				async: function(id,obj) {   						
			      	var request = $http.post('/api/v1/restaurants/edit_single_admin',
			      	{			
			      		restaurant_id: id,  
			      		district: obj.district,
			      		address: obj.address,
			      		city: obj.city,
			      		zip: obj.zip,
			      		seodescription: obj.seodescription
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			removeSingle: {
				async: function(id) {   						
			      	var request = $http.post('/api/v1/restaurants/remove_single',
			      	{			
			      		restaurant_id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			approveSingle: {
				async: function(id,status) {   						
			      	var request = $http.post('/api/v1/restaurants/approve_single',
			      	{			
			      		id: id,
			      		status: status
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			changePrenotazione: {
				async: function(id) {   						
			      	var request = $http.post('/api/v1/restaurants/change_prenotazione',
			      	{			
			      		restaurant_id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			changeOrdinazione: {
				async: function(id) { 									
			      	var request = $http.post('/api/v1/restaurants/change_ordinazione',
			      	{			
			      		restaurant_id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			changeTakeAway: {
				async: function(id) { 									
			      	var request = $http.post('/api/v1/restaurants/change_take_away',
			      	{			
			      		restaurant_id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			changeRange: {
				async: function(id,range) { 									
			      	var request = $http.post('/api/v1/restaurants/change_range',
			      	{			
			      		restaurant_id: id,
			      		range: range
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			setZip: {
				async: function(id,delivery_zip) { 								
			      	var request = $http.post('/api/v1/restaurants/set_zip',
			      	{			
			      		restaurant_id: id,
			      		delivery_zip: delivery_zip
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			changeOrari: {
				async: function(id,orari) {   						
			      	var request = $http.post('/api/v1/restaurants/change_orari',
			      	{			
			      		restaurant_id: id,
			      		orari: orari
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addStruttura: {
				async: function(ristorante,time,sale) {   						
			      	var request = $http.post('/api/v1/restaurants/add_struttura',
			      		// ho aggiunto a questo ristorante: 56c2e131bffebc28088b4c99 
			      	{			
			      		restaurant_id: ristorante,
			      		time: time,
			      		struttura: sale
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addMenu: {
				async: function(ristorante,menus) {   						
			      	
					var request = $http.post('/api/v1/restaurants/add_menu',
			      		// ho aggiunto a questo ristorante: 56c2e131bffebc28088b4c99
			      	{			
			      		restaurant_id: ristorante,
			      		menu: menus
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			claim: {
				async: function(ristorante) {   						
			      	var request = $http.post('/api/v1/restaurants/claim',
			      		// ho aggiunto a questo ristorante: 56c2e131bffebc28088b4c99
			      	{			
			      		restaurant_id: ristorante
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			approveClaim: {
				async: function(ristorante,user) {   						
			      	var request = $http.post('/api/v1/restaurants/approve_claim',
			      		// ho aggiunto a questo ristorante: 56c2e131bffebc28088b4c99
			      	{			
			      		restaurant_id: ristorante,
			      		user_id: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			getClaimed: {
				async: function() {   						
			      	var request = $http.post('/api/v1/restaurants/get_claimed',
			      		// ho aggiunto a questo ristorante: 56c2e131bffebc28088b4c99
			      	{						      		
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			uploadImage: {
				async: function(ristorante,immagine) {   						
			      	var formData = new FormData();
				    formData.append('restaurant_id', ristorante);
				    formData.append('image', immagine); 

					var request = $http({
					      	method              : 'POST',
					        url                 : '/api/v1/restaurants/upload_image',
					        data                : formData,
					        headers             : {'Content-Type': undefined},
					        transformRequest    : angular.identity
				    	}       				
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			        
			    }
			},
			setSpecialDate: {
				async: function(restaurant,type,day,from,to,all_day) {   						
			      	var request = $http.post('/api/v1/restaurants/set_special_date',
			      	{	
			      		restaurant_id: restaurant,
			      		type: type,
			      		day: day,
			      		from: from,
			      		to: to,
			      		all_day: all_day
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			removeSpecialDate: {
				async: function(restaurant,type,day,from,to,all_day) {   						
			      	var request = $http.post('/api/v1/restaurants/remove_special_date',
			      	{	
			      		restaurant_id: restaurant,
			      		type: type,
			      		day: day,
			      		from: from,
			      		to: to,
			      		all_day: all_day
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addFacebookPage: {
				async: function(restaurant,page_id) {   						
			      	var request = $http.post('/api/v1/restaurants/add_facebook_page',
			      	{	
			      		restaurant_id: restaurant,
			      		facebook: page_id
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(err){
			      		return err;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			removeFacebookPage: {
				async: function(restaurant,page_id) {   						
			      	var request = $http.post('/api/v1/restaurants/remove_facebook_page',
			      	{	
			      		restaurant_id: restaurant
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(err){
			      		return err;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			},
			getByFacebookPage: {
				async: function(page_id) {   						
			      	var request = $http.post('/api/v1/restaurants/view_by_facebook_page',
			      	{	
			      		page_id: page_id
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(err){
			      		return err;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			},
			addCustomClient: {
				async: function(restaurant,client) {   						
			      	var request = $http.post('/api/v1/restaurants/clients/add_custom_client',
			      	{	
			      		restaurant_id: restaurant,
			      		name: client.name,
			      		surname: client.surname,
			      		email: (client.email && client.email != '') ? client.email : null,
			      		phone: (client.phone && client.phone != '') ? client.phone : null,
			      		phone_: (client.phone_ && client.phone_ != '') ? client.phone_ : null,
						address: (client.address && client.address != '') ? client.address : null,
						cap: (client.cap && client.cap != '') ? client.cap : null,
						city: (client.city && client.city != '') ? client.city : null,
						pr: (client.pr && client.pr != '') ? client.pr : null,
						state: (client.state && client.state != '') ? client.state : null,
						dt: (client.dt && client.dt != '') ? client.dt : null,
						language: (client.language && client.language != '') ? client.language : null,
						allergeni: (client.allergeni.length > 0) ? client.allergeni : null,
						preferenze: (client.preferenze.length > 0) ? client.preferenze : null,
						note: (client.note && client.note != '') ? client.note : null
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(err){
			      		return err;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			},
			editCustomClient: {
				async: function(restaurant,client) {   						
			      	var request = $http.post('/api/v1/restaurants/clients/edit_custom_client',
			      	{	
			      		restaurant_id: restaurant,
			      		client_id: client._id,
			      		name: client.name,
			      		surname: client.surname,
			      		email: (client.email && client.email != '') ? client.email : null,
			      		phone: (client.phone && client.phone != '') ? client.phone : null,
			      		phone_: (client.phone_ && client.phone_ != '') ? client.phone_ : null,
						address: (client.address && client.address != '') ? client.address : null,
						cap: (client.cap && client.cap != '') ? client.cap : null,
						city: (client.city && client.city != '') ? client.city : null,
						pr: (client.pr && client.pr != '') ? client.pr : null,
						state: (client.state && client.state != '') ? client.state : null,
						dt: (client.dt && client.dt != '') ? client.dt : null,
						language: (client.language && client.language != '') ? client.language : null,
						allergie: (client.allergie.length > 0) ? client.allergie : null,
						preferenze: (client.preferenze.length > 0) ? client.preferenze : null,
						note: (client.note && client.note != '') ? client.note : null
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(err){
			      		return err;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			},
			deleteClient: {
				async: function(restaurant,client_id) {   						
			      	var request = $http.post('/api/v1/restaurants/clients/delete_custom_client',
			      	{	
			      		restaurant_id: restaurant,
			      		client_id: client_id
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(err){
			      		return err;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			},
			getRestaurantClients: {
				async: function(restaurant) {   						
			      	var request = $http.post('/api/v1/restaurants/clients/get_restaurant_clients',
			      	{	
			      		restaurant_id: restaurant
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(err){
			      		return err;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			},
			getClientInfo: {
				async: function(restaurant,user) {   						
			      	var request = $http.post('/api/v1/restaurants/clients/get_client_info',
			      	{	
			      		restaurant_id: restaurant,
			      		client_id: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(err){
			      		return err;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			},
			getStat: {
				async: function(restaurant) {   						
			      	var request = $http.post('/api/v1/restaurants/get_stats',
			      	{	
			      		restaurant_id: restaurant
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(err){
			      		return err;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			},
			getRestaurantFasce: {
				async: function(restaurant) {   						
			      	var request = $http.post('/api/v1/restaurants/get_fasce',
			      	{	
			      		restaurant_id: restaurant
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(err){
			      		return err;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			}
		};	
	return serviceCall;
}]);
