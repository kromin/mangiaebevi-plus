var SocialAppServices    = angular.module('MEBSocialServices',[]);

SocialAppServices
.factory('SocialServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			isFollowing: {
				async: function(user) {
					
					var request = $http.post('/api/v1/social/isFollowing',
			      	{
			      		user: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			canAddToFriends: {
				async: function(user) {
					
					var request = $http.post('/api/v1/social/canAddToFriends',
			      	{
			      		user: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addRemoveFollowing: {
				async: function(user) {
					
					var request = $http.post('/api/v1/social/addToFollowing',
			      	{
			      		user: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addFriendRequest: {
				async: function(user) {
					
					var request = $http.post('/api/v1/social/addFriendRequest',
			      	{
			      		user: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			acceptFriendRequest: {
				async: function(user) {
					
					var request = $http.post('/api/v1/social/acceptFriendRequest',
			      	{
			      		user: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			refuseFriendRequest: {
				async: function(user) {
					
					var request = $http.post('/api/v1/social/refuseFriendRequest',
			      	{
			      		user: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			removeFriend: {
				async: function(user) {
					
					var request = $http.post('/api/v1/social/removeFriend',
			      	{
			      		user: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewFriends: {
				async: function(user) {
					
					var request = $http.post('/api/v1/social/viewFriends',
			      	{
			      		user: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewFriendRequest: {
				async: function(user) {
					
					var request = $http.post('/api/v1/social/viewFriendRequest',
			      	{
			      		user_id: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewFollowers: {
				async: function(obj) {
					
					var request = $http.post('/api/v1/social/viewFollowers',
			      	{
			      		user: obj.user,
			      		skip: (obj.skip) ? obj.skip : null,
			      		take: obj.take
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewFollowing: {
				async: function(obj) {
					
					var request = $http.post('/api/v1/social/viewFollowing',
			      	{
			      		user: obj.user,
			      		skip: (obj.skip) ? obj.skip : null,
			      		take: obj.take
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewDashboard: {
				async: function(obj) {
					
					var request = $http.post('/api/v1/social/viewDashboard',
			      	{
			      		user: obj.user,
			      		skip: (obj.skip) ? obj.skip : null,
			      		take: obj.take
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
