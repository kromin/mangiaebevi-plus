var ToolsAppServices    = angular.module('MEBToolsServices',[]);

ToolsAppServices
.factory('ToolsServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			getMainSiteInfo: {
				async: function() {
					// servizio da mettere in cache
					if(!request){
						var request = $http.post('/api/v1/tools/get_main_site',
				      	{	
				      	cache: true		      		
				      	}
				        ).success(function (response) {
				            return response;
				      	})
					}		
			        // Return the promise to the controller
			        return request;
			    }
			},
			googleDirections: {
				async: function(from,to) {			    	
			      	var request = $http.post('/api/v1/tools/get_directions',
			      	{
			      		from: from,
			      		to: to
			      	},
			      	{	
			      	cache: true		      		
			      	}
			      	).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			validateAddress: {
				async: function(address) {			    	
			      	var request = $http.post('/api/v1/tools/validate_address',
			      	{
			      		address: address
			      	}
			      	).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			contact: {
				async: function(formVar) {			    	
			      	var request = $http.post('/api/v1/tools/contact',
			      	{
			      		name: formVar.name,
			      		subject: formVar.subject,
			      		message: formVar.message,
			      		email: formVar.email,
			      		phone: (formVar.phone) ? formVar.phone : null,
			      		risto: (formVar.risto) ? formVar.risto : null,
			      		address: (formVar.address) ? formVar.address : null
			      	}
			      	).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			reccomendMail: {
				async: function(formVar) {			    	
			      	var request = $http.post('/api/v1/tools/reccomendMail',
			      	{
			      		name: formVar.name,
			      		fname: formVar.fname,
			      		email: formVar.email,
			      		message: formVar.message,
			      		restaurant: formVar.restaurant
			      	}
			      	).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			saveSearch: {
				async: function(query,name,email) {				    	
			      	var request = $http.post('/api/v1/tools/saveSearchRequest',
			      	{
			      		email: email,
			      		name: name,
			      		query: query
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(error){
			      		return error;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			isToRender: {
				async: function(url) {			    	
			      	var request = $http.post('/api/v1/tools/isToRender',
			      	{
			      		page_url: url
			      	}
			      	).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			storeStatic: {
				async: function(content,url) {			    	
			      	var request = $http.post('/api/v1/tools/storeStatic',
			      	{
			      		page_content: content,
			      		page_url: url
			      	}
			      	).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			getLDS: {
				async: function(restaurant) {			    	
			      	var request = $http.post('/api/v1/tools/getLDSInformations',
			      	{
			      		restaurant: restaurant
			      	}
			      	).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			getInvalidRestaurants: {
				async: function() {			    	
			      	var request = $http.post('/api/v1/tools/getRestaurantEmpty',
			      	{}
			      	).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			saveRestaurantImage: {
				async: function(id,image_data) {

					var formData = new FormData();
				    formData.append('old_id', id);
				    formData.append('image', image_data); 

					var request = $http({
					      	method              : 'POST',
					        url                 : '/api/v1/tools/saveSingleImage',
					        data                : formData,
					        headers             : {'Content-Type': undefined},
					        transformRequest    : angular.identity
				    	}	    	
			      	).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			returnFasceOrarieDays: {
				returnArray: function(){
					var return_array = [],
						minutes = ['00','15','30','45'];
					for(var x = 0; x < 24; x++){
						if(x<10) x = '0'+x;

						for(n = 0; n < minutes.length; n++){
							return_array.push(""+x+":"+minutes[n]);
						}
						
					}
					return return_array;
				}				
			},
			mainOverlay: {
				show: function(){
					$(".main_page_overlay").fadeIn();
				},
				hide: function(){
					$(".main_page_overlay").fadeOut();
				}
			},
			allergeni: { 
				retrieve: function(){
					var all = [
					'glutine','crostacei','uova','pesce','arachidi','soia','latte','sedano','senape','anidride solforosa','sesamo','lupini','frutta a guscio','molluschi'
					];
					return all; 
				}
			}
		}	
	return serviceCall;
}])
