var PositionAppServices    = angular.module('MEBPositionServices',[]);

PositionAppServices
.factory('PositionService', function($http){    	
	var request;
	var PositionService = {
		    async: function() {			    	
		      	request = $http.get('http://ipinfo.io',{cache: true}
		        ).success(function (response) {
		        	return response.data
		      	})			      	
		      	// Return the promise to the controller
		      	return request;
		    }
		}		  	
  	return PositionService;        
})