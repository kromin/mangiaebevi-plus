var ServicesAppServices    = angular.module('MEBServicesServices',[]);

ServicesAppServices
.factory('ServicesServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			getSingle: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/services/view_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAll: {
				async: function() {				    	
			      	var request = $http.post('/api/v1/services/view_all',
			      	{}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addSingle: {
				async: function(name,image,icon) {	
						  	
			      	var request = $http.post('/api/v1/services/add_single',
			      	{
			      		name: name,
			      		image: (image) ? image : '',
			      		icon: icon
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			editSingle: {
				async: function(id,name,image,icon) {				  	
			      	var request = $http.post('/api/v1/services/edit_single',
			      	{
			      		id: id,
			      		name: name,
			      		image: (image) ? image : '',
			      		icon: icon
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			removeSingle: {
				async: function(id) {				  	
			      	var request = $http.post('/api/v1/services/remove_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			massiveFill: {
				async: function() {				  	
			      	var request = $http.post('/api/v1/services/massive_fill',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			massiveClean: {
				async: function() {				  	
			      	var request = $http.post('/api/v1/services/clean_all',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
