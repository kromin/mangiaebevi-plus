var CreditCardAppServices    = angular.module('MEBCreditCardServices',[]);

CreditCardAppServices
.factory('CreditCardServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			getSingle: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/creditcard/view_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAll: {
				async: function() {				    	
			      	var request = $http.post('/api/v1/creditcard/view_all',
			      	{}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addSingle: {
				async: function(name,image,icon) {	
						  	
			      	var request = $http.post('/api/v1/creditcard/add_single',
			      	{
			      		name: name,
			      		image: (image) ? image : null,
			      		icon: (icon) ? icon : null
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			editSingle: {
				async: function(id,name,image,icon) {				  	
			      	var request = $http.post('/api/v1/creditcard/edit_single',
			      	{
			      		id: id,
			      		name: name,
			      		image: (image) ? image : null,
			      		icon: (icon) ? icon : null
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			removeSingle: {
				async: function(id) {				  	
			      	var request = $http.post('/api/v1/creditcard/remove_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			massiveFill: {
				async: function() {				  	
			      	var request = $http.post('/api/v1/creditcard/massive_fill',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			massiveClean: {
				async: function() {				  	
			      	var request = $http.post('/api/v1/creditcard/clean_all',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
