var BlogArticlesServices    = angular.module('MEBBlogArticlesServices',[]);

BlogArticlesServices
.factory('BlogArticlesServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			getSingle: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/blog/article/view_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAll: {
				async: function() {				    	
			      	var request = $http.post('/api/v1/blog/article/view_all',
			      	{}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addSingle: {
				async: function(name,content,tag,meta_desc,image,category) {	
						  	
			      	var request = $http.post('/api/v1/blog/article/add_single',
			      	{
			      		name: name,
			      		content: content,
			      		tag: tag,
			      		meta: meta_desc,
			      		image: image,
			      		category: category
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			editSingle: {
				async: function(id,name,content,tag,meta_desc,image,category) {				  	
			      	var request = $http.post('/api/v1/blog/article/edit_single',
			      	{
			      		id: id,
			      		name: name,
			      		content: content,
			      		tag: tag,
			      		meta: meta_desc,
			      		image: image,
			      		category: category
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			removeSingle: {
				async: function(id) {				  	
			      	var request = $http.post('/api/v1/blog/article/remove_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
