var ImagesServices    = angular.module('MEBImagesServices',[]);

ImagesServices
.factory('ImagesServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			getSingle: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/images/view_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAll: {
				async: function() {				    	
			      	var request = $http.post('/api/v1/images/view_all',
			      	{}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			getByType: {
				async: function(type,ref) {				    	
			      	var request = $http.post('/api/v1/images/view_by_type',
			      	{
			      		type: type,
			      		ref: ref
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addSingle: {
				async: function(type,alt,title,image,ref_id) {	
					var formData = new FormData();
				    formData.append('type', type);
				    formData.append('alt', alt);
				    formData.append('title',title);
				    formData.append('image',image); 
				    formData.append('ref_id',(ref_id) ? ref_id : null);

					var request = $http({
					      	method              : 'POST',
					        url                 : '/api/v1/images/add_single',
					        data                : formData,
					        headers             : {'Content-Type': undefined},
					        transformRequest    : angular.identity
				    	}	  	
			      	
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			removeSingle: {
				async: function(id) {				  	
			      	var request = $http.post('/api/v1/images/remove_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(){
			      		return;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
