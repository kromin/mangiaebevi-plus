var RegionsAppServices    = angular.module('MEBRegionsServices',[]);

RegionsAppServices
.factory('RegionsServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			getSingle: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/regions/view_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAll: {
				async: function() {				    	
			      	var request = $http.post('/api/v1/regions/view_all',
			      	{}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addSingle: {
				async: function(name,image) {	
						  	
			      	var request = $http.post('/api/v1/regions/add_single',
			      	{
			      		name: name,
			      		image: (image) ? image : null
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			editSingle: {
				async: function(id,name,image) {				  	
			      	var request = $http.post('/api/v1/regions/edit_single',
			      	{
			      		id: id,
			      		name: name,
			      		image: (image) ? image : null
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			removeSingle: {
				async: function(id) {				  	
			      	var request = $http.post('/api/v1/regions/remove_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			massiveFill: {
				async: function() {				  	
			      	var request = $http.post('/api/v1/regions/massive_fill',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			massiveClean: {
				async: function() {				  	
			      	var request = $http.post('/api/v1/regions/clean_all',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
