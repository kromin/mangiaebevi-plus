var PlatesAppServices    = angular.module('MEBPlatesServices',[]);

PlatesAppServices
.factory('PlatesServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {			
			viewAll: {
				async: function() {		    	
					
			      	var request = $http.post('/api/v1/plates/view_all',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewByRestaurant: {
				async: function(restaurant) {		    	
					
			      	var request = $http.post('/api/v1/plates/view_by_restaurant',
			      	{
			      		restaurant_id: restaurant
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewSingle: {
				async: function(plate) {		    	
					
			      	var request = $http.post('/api/v1/plates/viewSingle',
			      	{
			      		plate: plate
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			deletePlate: {
				async: function(restaurant,id) {		    	
					
			      	var request = $http.post('/api/v1/plates/delete',
			      	{
			      		restaurant_id: restaurant,
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addCommunityPlate: {
				async: function(obj) {	
			      	var request = $http.post('/api/v1/plates/community/add',
			      	{
			      		plate: obj.plate,
			      		image: obj.image,
			      		text: obj.text,
			      		rating: obj.rating
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewCommunityPlateByUser: {
				async: function(obj) {	
			      	var request = $http.post('/api/v1/plates/community/viewByUser',
			      	{
			      		user: obj.user,
			      		skip: obj.image,
			      		take: obj.take
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewCommunityPlateByPlate: {
				async: function(obj) {		
			      	var request = $http.post('/api/v1/plates/community/viewByPlate',
			      	{
			      		plate: obj.plate,
			      		skip: obj.image,
			      		take: obj.take
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewCommunitySingle: {
				async: function(plate) {		
			      	var request = $http.post('/api/v1/plates/community/viewSingle',
			      	{
			      		plate: plate
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
