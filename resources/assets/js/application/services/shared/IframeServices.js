var IframeAppServices    = angular.module('MEBIframeServices',[]);

IframeAppServices
.factory('IframeServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			getAll: {
				async: function() {			    	
			      	var request = $http.post('/api/v1/iframe/get_all',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			deleteSingle: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/iframe/delete',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addSingle: {
				async: function(vars) {			    	
			      	var request = $http.post('/api/v1/iframe/add',
			      	{
			      		url: vars.url,
			      		restaurant_id: vars.restaurant
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addSingleUrl: {
				async: function(vars) {			    	
			      	var request = $http.post('/api/v1/iframe/addURL', 
			      	{
			      		url: vars.url,
			      		restaurant_id: vars.restaurant,
			      		iframe: vars.iframe
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			modSingle: {
				async: function(vars) {			    	
			      	var request = $http.post('/api/v1/iframe/mod',
			      	{
			      		id: vars._id,
			      		url: vars.url,
			      		restaurant_id: vars.restaurant
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewByRestaurant: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/iframe/view_by_restaurant',
			      	{
			      		restaurant_id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewMine: {
				async: function() {			    	
			      	var request = $http.post('/api/v1/iframe/view_mine',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
