var CommunityServices    = angular.module('MEBCommunityServices',[]);

CommunityServices
.factory('CommunityServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			viewByRestaurant: {
				async: function(restaurant) {			    	
			      	var request = $http.post('/api/v1/community/view_by_restaurant',
			      	{
			      		restaurant: restaurant
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewByUser: {
				async: function(user) {			    	
			      	var request = $http.post('/api/v1/community/view_by_user',
			      	{
			      		user: user
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			followUser: {
				async: function(user) {			    	
			      	var request = $http.post('/api/v1/community/follow',
			      	{
			      		follow: user
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			unFollowUser: {
				async: function(user) {			    	
			      	var request = $http.post('/api/v1/community/unfollow',
			      	{
			      		unfollow: user
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addRecensione: {
				async: function(name,text,tags,restaurant,image) {			    	
			      	var request = $http.post('/api/v1/community/add_recensione',
			      	{
			      		name: name,
			      		text: text,
			      		tags: tags,
			      		restaurant: restaurant,
			      		image: image
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewToApprove: {
				async: function() {			    	
			      	var request = $http.post('/api/v1/community/view_to_approve',
			      	{}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			approveRecensione: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/community/approve_recensione',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			deleteRecensione: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/community/delete_recensione',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])