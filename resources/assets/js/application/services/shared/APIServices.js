var APIAppServices    = angular.module('MEBAPIServices',[]);

APIAppServices
.factory('APIServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			getAll: {
				async: function() {			    	
			      	var request = $http.post('/api/v1/apis/get_all',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			deleteSingle: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/apis/delete',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addSingle: {
				async: function(name) {			    	
			      	var request = $http.post('/api/v1/apis/add',
			      	{
			      		name: name
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
