var PaymentServices    = angular.module('MEBPaymentServices',[]);

PaymentServices
.factory('PaymentServices', ['$cookies','$location', '$http','$window', '$q', '$rootScope',
	function($cookies,$location,$http,$window, $q, $rootScope){
		var serviceCall = {
			subscribePlan: {   
				async: function(plan,period) {			    	
			      	var request = $http.post('/api/v1/payments/plan',
			      	{
			      		plan: plan,
			      		period: (period) ? period : parseInt(0)
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			cancelPlan: {   
				async: function(profile) {			    	
			      	var request = $http.post('/api/v1/payments/cancelplan',
			      	{
			      		profile: profile
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			suspendPlan: {   
				async: function(profile) {			    	
			      	var request = $http.post('/api/v1/payments/suspendplan',
			      	{
			      		profile: profile
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			reactivatePlan: {   
				async: function(profile) {			    	
			      	var request = $http.post('/api/v1/payments/reactivateplan',
			      	{
			      		profile: profile
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			payOrder: {
				async: function() {			    	
			      	var request = $http.post('/api/v1/payments/payOrder',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			/**
			* Retrieve braintree token
			* @return token string
			* @deprecated
			*/
			getToken: {
				async: function() {			    	
			      	var request = $http.post('/api/v1/payments/getToken',
			      	{			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			/**
			* Initialize Braintree payments loadyng th url async
			* @return promise PaymentServices.initializeBraintree.then(function(){})
			* @deprecated
			*/
			initializeBraintree: {
				initialize: function(){
					var asyncUrl = 'https://js.braintreegateway.com/js/braintree-2.21.0.min.js',
					paymentsDefer = $q.defer();

					$window.BraintreeInitializedDeferred = paymentsDefer.resolve;

					var asyncLoad = function(asyncUrl, callbackName) {
						var script = document.createElement('script');
						//script.type = 'text/javascript';
						script.src = asyncUrl;
						document.body.appendChild(script);		
						$rootScope.$broadcast('braintreegateway');				
				    };
				    asyncLoad(asyncUrl, 'BraintreeInitializedDeferred');
				    
				}
			},
			/**
			* Set order as session to save
			* @param order obj
			* @return message=>'ok'
			*/
			setOrderSession: {
				async: function(order) {			    	
			      	var request = $http.post('/api/v1/payments/setSession',
			      	{
			      		order: order
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			/**
			* Pay the current order in session
			* @param nonce retrieved by braintree
			* @return message=>'ok' if payment ok
			*/
			payOrder: {
				async: function(nonce) {			    	
			      	var request = $http.post('/api/v1/payments/payOrder',
			      	{
			      		nonce: nonce
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			/**
			* Pay the current order in session
			* @param nonce retrieved by braintree
			* @param token retrieved by braintree
			* @param plan plan id alpha_num
			* @param plan billing cicle 0|1
			* @return message=>'ok' if payment ok
			*/
			payPlan: {
				async: function(nonce,type,plan,billing) {			    	
			      	var request = $http.post('/api/v1/payments/payPlan',
			      	{
			      		nonce: nonce,
			      		type: type,
			      		plan: plan,
			      		billing: billing
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
