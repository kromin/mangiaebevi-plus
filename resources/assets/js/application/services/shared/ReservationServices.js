var ReservationAppServices    = angular.module('MEBReservationServices',[]);

ReservationAppServices
.factory('ReservationServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {			
			askReserve: {
				async: function(id,date,time,email,people,name,tel) {		    
			      	var request = $http.post('/api/v1/reservations/add_reservation',
			      	{
			      		restaurant_id: id,
			      		date: moment(date).format('YYYY-MM-DD'),
			      		time: time,
			      		email: email,
			      		people: people,
			      		name: name,
			      		tel: tel
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAll: {
				async: function() {		    
			      	var request = $http.post('/api/v1/reservations/view_all',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewMine: {
				async: function(skip,take) {
					var obj = {}		 
					if(skip || take) obj = {skip: skip, take: take}		    
			      	var request = $http.post('/api/v1/reservations/view_mine',
			      	obj
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewSingle: {
				async: function(id) {		    
			      	var request = $http.post('/api/v1/reservations/view_single',
			      	{		
			      		id: id	      		
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			},
			deleteReservation: {
				async: function(id) {		    
			      	var request = $http.post('/api/v1/reservations/delete_reservation',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewByDate: {
				async: function(restaurant,date) {		    
			      	var request = $http.post('/api/v1/reservations/view_by_date',
			      	{
			      		restaurant_id: restaurant,
			      		date: date
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewByFromToRestaurant: {
				async: function(restaurant,from,to) {		    
			      	var request = $http.post('/api/v1/reservations/view_by_from_to',
			      	{
			      		restaurant_id: restaurant,
			      		from: from,
			      		to: to
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			},
			addManualReserve: {
				async: function(id,date,time,email,people,name,tel) {		    
			      	var request = $http.post('/api/v1/reservations/add_manual_reservation',
			      	{
			      		restaurant_id: id,
			      		date: moment(date).format('YYYY-MM-DD'),
			      		time: time,
			      		email: (email && email !== '') ? email : null,
			      		people: people,
			      		name: name,
			      		tel: tel
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	});
			        // Return the promise to the controller
			        return request;
			    }
			},
			confirmReservation: {
				async: function(restaurant,reservation,sala,tavolo) {		    
			      	var request = $http.post('/api/v1/reservations/confirm_reservation',
			      	{
			      		restaurant_id: restaurant,
			      		id: reservation,
			      		sala: sala,
			      		tavolo: (tavolo) ? tavolo : null
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			changeDate: {
				async: function(restaurant,reservation,date,time) {		    
			      	var request = $http.post('/api/v1/reservations/change_reservation_date',
			      	{
			      		restaurant_id: restaurant,
			      		id: reservation,
			      		date: moment(date).format('YYYY-MM-DD'),
			      		time: time
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			approveChangeDate: {
				async: function(restaurant,reservation,string) {		    
			      	var request = $http.post('/api/v1/reservations/approve_date_change',
			      	{
			      		restaurant_id: restaurant,
			      		id: reservation,
			      		string: string
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			refuseChangeDate: {
				async: function(restaurant,reservation,string) {		    
			      	var request = $http.post('/api/v1/reservations/refuse_date_change',
			      	{
			      		restaurant_id: restaurant,
			      		id: reservation,
			      		string: string
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			assignFoodcoin: {
				async: function(restaurant,reservation) {		    
			      	var request = $http.post('/api/v1/reservations/assign_foodcoin',
			      	{
			      		restaurant_id: restaurant,
			      		id: reservation
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			removeFoodcoin: {
				async: function(restaurant,reservation) {		    
			      	var request = $http.post('/api/v1/reservations/remove_foodcoin',
			      	{
			      		restaurant_id: restaurant,
			      		id: reservation
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			getRestaurantFasce:
			{
				async: function(restaurant,day) {		    
			      	var request = $http.post('/api/v1/reservations/get_restaurant_fasce',
			      	{
			      		restaurant_id: restaurant,
			      		day: day
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			getRestaurantAvaibleFasce:
			{
				async: function(restaurant,day,people) {		    
			      	var request = $http.post('/api/v1/reservations/get_restaurant_avaible_fasce',
			      	{
			      		restaurant_id: restaurant,
			      		day: day,
			      		people: (people) ? parseInt(people) : null
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			payPreorder: 
			{
				async: function(reservation) {		    
			      	var request = $http.post('/api/v1/orders/pay_preorder',
			      	{
			      		reservation: reservation
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
