var NotificationsServices    = angular.module('MEBNotificationsServices',[]);

NotificationsServices
.factory('NotificationsServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			viewAll: {
				async: function() {			    	
			      	var request = $http.post('/api/v1/notifications/view_all',
			      	{
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			deleteOld: {
				async: function() {			    	
			      	var request = $http.post('/api/v1/notifications/delete_old',
			      	{
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewMine: {
				async: function(id,skip,number) {			    	
			      	var request = $http.post('/api/v1/notifications/view_mine',
			      	{
			      		user_id: id,
			      		skip: (skip) ? skip : 0,
			      		number: (number) ? number : null
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewToRead: {
				async: function() {			    	
			      	var request = $http.post('/api/v1/notifications/view_to_read',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			setRead: {
				async: function(id,id_notifica) {			    	
			      	var request = $http.post('/api/v1/notifications/set_read',
			      	{
			      		user_id: id,
			      		id: id_notifica
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			setAllRead: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/notifications/set_all_read',
			      	{
			      		user_id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])