var NewsLetterAppServices    = angular.module('MEBNewsLetterServices',[]);

NewsLetterAppServices
.factory('NewsLetterServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			getSubscriber: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/newsletter/view_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			register: {
				async: function(email,name) {				    	
			      	var request = $http.post('/api/v1/newsletter/register',
			      	{
			      		email: email,
			      		name: name
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			remove_from_mail: {
				async: function(id,stringa) {				  	
			      	var request = $http.post('/api/v1/newsletter/remove_subscriber_from_email',
			      	{
			      		id: id,
			      		str: stringa
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAll: {
				async: function() {				  	
			      	var request = $http.post('/api/v1/newsletter/view_subscribers',
			      	{}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			updateSubscriber: {
				async: function(id,email,name) {				  	
			      	var request = $http.post('/api/v1/newsletter/update_subscriber',
			      	{
			      		id: id,
			      		email: email,
			      		name: name
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			deleteSubscriber: {
				async: function(user) {				  	
			      	var request = $http.post('/api/v1/newsletter/remove_subscriber',
			      	{
			      		id: user
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
