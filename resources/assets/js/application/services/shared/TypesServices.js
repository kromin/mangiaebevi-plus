var TypesAppServices    = angular.module('MEBTypesServices',[]);

TypesAppServices
.factory('TypesServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			getSingle: {
				async: function(id) {			    	
			      	var request = $http.post('/api/v1/types/view_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAll: {
				async: function() {				    	
			      	var request = $http.post('/api/v1/types/view_all',
			      	{}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addSingle: {
				async: function(name,image) {	
						  	
			      	var request = $http.post('/api/v1/types/add_single',
			      	{
			      		name: name,
			      		image: (image) ? image : null
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			editSingle: {
				async: function(id,name,image) {				  	
			      	var request = $http.post('/api/v1/types/edit_single',
			      	{
			      		id: id,
			      		name: name,
			      		image: (image) ? image : null
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			removeSingle: {
				async: function(id) {				  	
			      	var request = $http.post('/api/v1/types/remove_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			massiveFill: {
				async: function() {				  	
			      	var request = $http.post('/api/v1/types/massive_fill',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			massiveClean: {
				async: function() {				  	
			      	var request = $http.post('/api/v1/types/clean_all',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
