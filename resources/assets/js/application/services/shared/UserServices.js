var UserAppServices    = angular.module('MEBUserServices',[]);

UserAppServices
.factory('UserServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {
			/**
			* Ritorna utente loggato da servizio, molto utile per passare i dati tra i controller
			*
			*/
			isCurrentLogged: {
				isLoggedCurrently: {
					loggedStatus: 0
				},
				set: function(data){
					this.isLoggedCurrently = data;
				}				
			},
			getLogged: {
				async: function() {			    	
			      	var request = $http.post('/api/v1/is_logged'
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},			
			register: {
				async: function(email,name,surname,password,phone,role) {
				if(!role) role = 1;			    	
			      	var request = $http.post('/api/v1/register',
			      	{
			      		email: email,
			      		name: name,
			      		surname: surname,
			      		pwd: password,
			      		phone: (phone) ? phone : null,
			      		role: role
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},			
			recovery: {
				async: function(email) {						    	
			      	var request = $http.post('/api/v1/recovery_pwd',
			      	{
			      		email: email
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},			
			resend: {
				async: function(email) {						    	
			      	var request = $http.post('/api/v1/resend_verification',
			      	{
			      		email: email
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			completeProfile: {
				async: function(id,address,born) {							    	
			      	var request = $http.post('/api/v1/complete_profile',
			      	{
			      		user_id: id,
			      		address: address,
			      		born: moment(born).format("YYYY-MM-DD")
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addCurrentAddress: {
				async: function(id,address) {							    	
			      	var request = $http.post('/api/v1/add_current_address',
			      	{
			      		user_id: id,
			      		address: address
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			login: {
				async: function(email,password) {				  	
			      	var request = $http.post('/api/v1/login',
			      	{
			      		email: email,
			      		pwd: password
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			logout: {
				async: function() {				  	
			      	var request = $http.post('/api/v1/logout',
			      	{}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAll: {
				async: function() {				  	
			      	var request = $http.post('/api/v1/view_users',
			      	{}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewSingle: {
				async: function(user) {				  	
			      	var request = $http.post('/api/v1/view_single',
			      	{
			      		id: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			removeSingle: {
				async: function(user) {				  	
			      	var request = $http.post('/api/v1/remove_single',
			      	{
			      		id: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			changeRole: {
				async: function(id,role) {	
					//if(!id || !role) return {};
					var request = $http.post('/api/v1/change_user_role',
			      	{
			      		id: id,
			      		role: parseInt(role)
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			changeInfo: {
				async: function(id,name,surname,phone,description) {	
					
					var request = $http.post('/api/v1/edit_user',
			      	{
			      		user_id: id,
			      		name: name,
			      		surname: surname,
			      		phone: (phone) ? phone : null,
			      		description: description
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			modAvatar: {
				async: function(id,image_data) {

					var formData = new FormData();
				    formData.append('user_id', id);
				    formData.append('image', image_data); 

					var request = $http({
					      	method              : 'POST',
					        url                 : '/api/v1/mod_avatar',
					        data                : formData,
					        headers             : {'Content-Type': undefined},
					        transformRequest    : angular.identity
				    	}
       				/*{
			      		user_id: id,
			      		file: image_data
			      	}*/
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			modCover: {
				async: function(id,image_data) {

					var formData = new FormData();
				    formData.append('user_id', id);
				    formData.append('image', image_data); 

					var request = $http({
					      	method              : 'POST',
					        url                 : '/api/v1/mod_cover',
					        data                : formData,
					        headers             : {'Content-Type': undefined},
					        transformRequest    : angular.identity
				    	}
       				/*{
			      		user_id: id,
			      		file: image_data
			      	}*/
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		console.log(response)
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			changeMail: {
				async: function(id,email) {	
					
					var request = $http.post('/api/v1/mod_email',
			      	{
			      		user_id: id,
			      		email: email
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			changePwd: {
				async: function(id,old,newp,newpc) {	
					
					var request = $http.post('/api/v1/mod_pwd',
			      	{
			      		user_id: id,
			      		oldpwd: old,
			      		password: newp,
			      		confirmpwd: newpc 
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addRemoveFavourite: {
				async: function(restaurant) {
					
					var request = $http.post('/api/v1/addToFavourite',
			      	{
			      		restaurant: restaurant
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			setFavourites: {
				async: function(f,l) {
					
					var request = $http.post('/api/v1/setFavourites',
			      	{
			      		favourites: f,
			      		list: l
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addRemoveWishlist: {
				async: function(restaurant) {
					
					var request = $http.post('/api/v1/addToWishlist',
			      	{
			      		restaurant: restaurant
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			setAllergeni: {
				async: function(allergeni) {
					
					var request = $http.post('/api/v1/setAllergeni',
			      	{
			      		allergeni: allergeni
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			getWishlist: {
				async: function(user) {
					
					var request = $http.post('/api/v1/social/getWishlist',
			      	{
			      		user: user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			getFavourites: {
				async: function(usr) {	
						    	
			      	var request = $http.post('/api/v1/get_favourites',
			      	{
			      		user_id: usr
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			searchUsers: {
				async: function(string) {			    	
			      	var request = $http.post('/api/v1/search_user',
			      	{
			      		string: string
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			inviteFriends: {
				async: function(emails) {			    	
			      	var request = $http.post('/api/v1/invite_friends',
			      	{
			      		emails: emails
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
