var SearchAppServices    = angular.module('MEBSearchServices',[]);

SearchAppServices
.factory('SearchServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {			
			searchByString: {
				async: function(string,lat,lng,city,action) {		    	
					
			      	var request = $http.post('/api/v1/restaurants/search/search_by_string',
			      	{
			      		string: string,
			      		lat: (lat) ? lat : null,
			      		lng: (lng) ? lng : null,
			      		city: city,
			      		action: action
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			searchByCity: {
				async: function(city,action) {		    	
					
			      	var request = $http.post('/api/v1/restaurants/search/search_by_city',
			      	{
			      		city: city,
			      		action: action
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			searchByLatLng: {
				async: function(lat,lng,action) {	    	
					
			      	var request = $http.post('/api/v1/restaurants/search/search_by_lat_lng',
			      	{
			      		lat: lat,
			      		lng: lng,
			      		action: action
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			searchByName: {
				async: function(name) {	    	
					
			      	var request = $http.post('/api/v1/restaurants/search/search_by_name',
			      	{
			      		name: name
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			searchPlateByName: {
				async: function(name,restaurant) {	    	
					
			      	var request = $http.post('/api/v1/plates/search/search_by_name',
			      	{
			      		name: name,
			      		restaurant: (restaurant) ? restaurant : null
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			searchBySlug: {
				async: function(slug) {	    	
					
			      	var request = $http.post('/api/v1/restaurants/search/search_by_slug',
			      	{
			      		slug: slug
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			searchRelated: {
				async: function(restaurant) {	 
			      	var request = $http.post('/api/v1/getRelated',
			      	{
			      		restaurant: restaurant
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(data){
			      		return data;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			searchAdvanced: {
				async: function(obj) {	 
			      	var request = $http.post('/api/v1/searchAdvanced',
			      	{
			      		name     : (obj.name && obj.name != '' && obj.name != []) ? obj.name : null,
			            type     : (obj.type && obj.type != '' && obj.type != []) ? obj.type : null,
			            location : (obj.location && obj.location != '' && obj.location != []) ? obj.location : null,
			            maxPrice : (obj.maxPrice && obj.maxPrice != '' && obj.maxPrice != []) ? obj.maxPrice : null,
			            services : (obj.services && obj.services != '' && obj.services != []) ? obj.services : null,
			            types : (obj.types && obj.types != '' && obj.types != []) ? obj.types : null,
			            regions : (obj.regions && obj.regions != '' && obj.regions != []) ? obj.regions : null,
			            social : (obj.social && obj.social != '' && obj.social != []) ? obj.social : null,
			            recensioni : (obj.recensioni && obj.recensioni != '' && obj.recensioni != []) ? obj.recensioni : null,
			            virtualTour: (obj.virtualTour && obj.virtualTour != '' && obj.virtualTour != []) ? obj.virtualTour : null,
			            prenota: (obj.prenota && obj.prenota != '' && obj.prenota != []) ? obj.prenota : null
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(data){
			      		return data;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
