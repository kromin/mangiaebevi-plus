var OrdersAppServices    = angular.module('AppOrdersServices',[]);

OrdersAppServices
.factory('OrdersServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {			
			addOrderSession: {
				async: function(restaurant,menu,menu_name,plates,subtotal,delivery,price) {	 
			      	var request = $http.post('/api/v1/orders/order_session',
			      	{
			      		restaurant: restaurant,
			      		menu: menu,	
			      		menu_name: menu_name,		      		
			      		plates: plates,
			      		subtotal: subtotal,
			      		delivery: delivery,
			      		price: price
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			addPreOrder: {
				async: function(restaurant,reservation,menu,menu_qty,menu_name,plates,subtotal,price) {
					 
			      	var request = $http.post('/api/v1/orders/add_preorder',
			      	{
			      		restaurant: restaurant,
			      		reservation: reservation,
			      		menu: menu,	
			      		menu_qty: menu_qty,
			      		menu_name: menu_name,		      		
			      		plates: plates,
			      		subtotal: subtotal,
			      		price: price
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(e){
			      		return e;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			setOrderTypeAddress: {
				async: function(address,city,state,zip,phone,date,type) {	 
			      	var request = $http.post('/api/v1/orders/order_set_address_type',
			      	{
			      		address: address,
			      		city: city,
			      		state: state,
			      		zip: zip,
			      		phone: phone,
			      		type: type,
			      		date_string: date
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			setOrderTypeTake: {
				async: function(phone,date,type) {	 
			      	var request = $http.post('/api/v1/orders/order_set_take_type',
			      	{
			      		phone: phone,
			      		type: type,
			      		date_string: date
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			payOrder: {
				async: function(nonce) {	 
			      	var request = $http.post('/api/v1/orders/order_pay',
			      	{
			      		nonce: nonce
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewByDateRestaurant: {
				async: function(restaurant,date) {
			      	var request = $http.post('/api/v1/orders/view_by_date',
			      	{
			      		restaurant_id: restaurant,
			      		date: date
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewByUser: {
				async: function(usr) {
			      	var request = $http.post('/api/v1/orders/view_orders',
			      	{
			      		user_id: usr
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewSingle: {
				async: function(id) {
			      	var request = $http.post('/api/v1/orders/view_single',
			      	{
			      		id: id
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			deletePlate: {
				async: function() {		    	
					
			      	var request = $http.post('/api/v1/orders/',
			      	{
			      		
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			askRefund: {
				async: function(order) {	    	
					
			      	var request = $http.post('/api/v1/orders/ask_refund',
			      	{
			      		order: order
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			refund: {
				async: function(order) {	    	
					
			      	var request = $http.post('/api/v1/orders/refund',
			      	{
			      		order: order
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			setOrderStatus: {
				async: function(order,status) {	    	
					
			      	var request = $http.post('/api/v1/orders/set_status',
			      	{
			      		id: order,
			      		status: status
			      	}
			        ).success(function (response) {
			            return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
