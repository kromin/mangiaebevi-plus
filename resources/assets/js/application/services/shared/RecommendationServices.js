var RecommendationAppServices    = angular.module('MEBRecommendationServices',[]);

RecommendationAppServices
.factory('RecommendationServices', ['$cookies','$location', '$http',
	function($cookies,$location,$http){
		var serviceCall = {			
			publishRecommendation: {
				async: function(obj) {								
					var request = $http.post('/api/v1/recommendation/publishRecommendation',
			      	{
			      		address: (obj.address) ? obj.address : null,
			            text: obj.text,
			            type: parseInt(obj.type), // 0 == ristorante, 1 == piatto
			            ref: obj.ref
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewByUser: {
				async: function(obj) {					
					var request = $http.post('/api/v1/recommendation/viewByUser',
			      	{
			      		user: obj.user,
			            skip: (obj.skip) ? obj.skip : null,
			            take: obj.take
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewByPlate: {
				async: function(obj) {					
					var request = $http.post('/api/v1/recommendation/viewByPlate',
			      	{
			      		ref: obj.ref,
			            skip: (obj.skip) ? obj.skip : null,
			            take: obj.take
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewByRestaurant: {
				async: function(obj) {					
					var request = $http.post('/api/v1/recommendation/viewByRestaurant',
			      	{
			      		ref: obj.ref,
			            skip: (obj.skip) ? obj.skip : null,
			            take: obj.take
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewNear: {
				async: function(obj) {					
					var request = $http.post('/api/v1/recommendation/viewNear',
			      	{
			      		lat: obj.lat,
			            lng: obj.lng,
			            skip: (obj.skip) ? obj.skip : null,
			            limit: obj.limit
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			askRecommendationRequest: {
				async: function(obj) {					
					var request = $http.post('/api/v1/recommendation-request/askRecommendationRequest',
			      	{
			      		address: (obj.address) ? obj.address : null,
			            text: obj.text,
			            type: parseInt(obj.type)
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			answerRecommendationRequest: {
				async: function(obj) {					
					var request = $http.post('/api/v1/recommendation-request/answerRecommendationRequest',
			      	{
			      		request: obj.request,
			            text: obj.text,
			            ref: obj.ref
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewByUserRequests: {
				async: function(obj) {					
					var request = $http.post('/api/v1/recommendation-request/viewByUserRequests',
			      	{
			      		user: obj.user
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewNearRequests: {
				async: function(obj) {					
					var request = $http.post('/api/v1/recommendation-request/viewNearRequests',
			      	{
			      		lat: obj.lat,
			            lng: obj.lng,
			            skip: (obj.skip) ? obj.skip : null,
			            limit: obj.limit
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			},
			viewAnswersRequests: {
				async: function(obj) {					
					var request = $http.post('/api/v1/recommendation-request/viewAnswersRequests',
			      	{
			      		request: obj.request,
			            skip: (obj.skip) ? obj.skip : null,
			            limit: obj.limit
			      	}
			        ).success(function (response) {
			            return response;
			      	}).error(function(response){
			      		return response;
			      	})
			        // Return the promise to the controller
			        return request;
			    }
			}
		}	
	return serviceCall;
}])
