var RestaurantController = angular.module('AppRestaurantController',[]);

RestaurantController
.controller('RestaurantCtrl', function($scope, $rootScope, $cookies, $sce, $location, $route, $routeParams, $document, ngDialog, RestaurantsServices, UserServices, ReservationServices, SearchServices, ToolsServices) {
	"ngInject";
    
    $scope.iframe_type = ($routeParams.type) ? 'menu' : 'reservation';
    

    $scope.testing = ($routeParams.test) ? true : false;
    
	
	$scope.mainUrl = document.location.origin+'/';
	ToolsServices.getMainSiteInfo.async().success(function(data){
		$scope.creditCard = _.indexBy(data.creditCard, '_id');
		$scope.homeCities = _.indexBy(data.homeCities, '_id');
		$scope.regioniCucine = _.indexBy(data.regioniCucine, '_id');
		$scope.tipiCucine = _.indexBy(data.tipiCucine, '_id');
		$scope.tipiLocali = _.indexBy(data.tipiLocali, '_id');
		$scope.services = _.indexBy(data.services, '_id');
		$scope.pacchetti = _.indexBy(data.pacchetti, 'price_month');
		$scope.staticURL = data.staticDomain;// domain for static files		
		
	});

	$scope.init = false;
	RestaurantsServices.viewSingle.async($routeParams.restaurant).success(function(data){
		if(data.message == 'ok'){
			$scope.current_restaurant = data.restaurant;
            $scope.selectMenu(0);
			$scope.initReservation();
		}
	});


    /**
     * Dialog Login e registrazione
     *
     */
    $scope.dialogLogin = function(val) {
        if (val) {
            $scope.login_form = val;
        }
        var loginDialog = ngDialog.open({
            template: '/templates/dialoglogin',
            scope: $scope,
            className: 'ngdialog ngdialog-theme-default small'
        });

        
        return loginDialog;
    }

    $scope.dialogRegister = function() {
        var registerDialog = ngDialog.open({
            template: '/templates/dialogregister',
            scope: $scope,
            className: 'ngdialog ngdialog-theme-default medium'
        });

        registerDialog.closePromise.then(function(data) {
            switch (data.value) {
                case 'toLogin':
                    $scope.dialogLogin();
                    break;
            }
        });
        return registerDialog;
    }




    $scope.takeAvaibleFasce = function(){
        console.log('prendo fasce');
        var date = moment($scope.current_picker.date).format('YYYY-MM-DD');
        ReservationServices.getRestaurantAvaibleFasce.async($scope.current_restaurant._id,date).success(function(f){
            if(f.fasce.length > 0){
                $scope.restaurant_reservation_date = date;
                $scope.avaible_fasce = f.fasce;
                $scope.reservation.hour = f.fasce[0];
                $("#reservation_btn").removeClass("disabled");
                $scope.valid_reservation = true;
            } else {
                $("#reservation_btn").addClass("disabled");
                $scope.avaible_fasce = [];
                $scope.valid_reservation = false;
            }
            
        });
    };

    $scope.reserve = function() {
        if ($scope.valid_reservation) {

            var persona = ($scope.reservation.people == 1) ? 'a' : 'e';
            var user = UserServices.isCurrentLogged.isLoggedCurrently;

            if(!user.user){
                var dialog = $scope.dialogLogin(1);
                dialog.closePromise.then(function(data) {
                    switch (data.value) {
                        case 'registerSubmitted':
                        case 'loginSubmitted':
                            $scope.reserve();
                            break;
                    }
                });
                return;
            }
            
            if( (user.user && (user.user.phone === '' || !user.user.phone)) || !user.user ){
                $scope.reservation_as_to_add_phone = true;
                return;
            } else {
                $scope.reservation_as_to_add_phone = false;
            }

            if (user && user.loggedStatus == 1) {
                // utente loggato, può prenotare
                //console.log('data',$scope.restaurant_reservation_date.format("YYYY-MM-DD"))
                ToolsServices.mainOverlay.show();
                ReservationServices.askReserve
                    .async($scope.current_restaurant._id,
                        moment($scope.restaurant_reservation_date).format("YYYY-MM-DD"),
                        $scope.reservation.hour,
                        user.user.email,
                        $scope.reservation.people,
                        user.user.name + ' ' + user.user.surname,
                        user.user.phone)
                    .success(function(data) {
                        if (data.message == 'ok') {
                            swal({
                                buttonsStyling: false,
                                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                                title: 'Congratulazioni',
                                text: 'Prenotazione confermata, segui sul tuo profilo l\'approvazione da parte del ristoratore',
                                type: 'success'
                            });
                        }
                        ToolsServices.mainOverlay.hide();
                    }).error(function(e){
                        ToolsServices.mainOverlay.hide();
                        swal({
                                buttonsStyling: false,
                                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                                title: 'Spiacenti',
                                text: 'Si è verificato un errore inaspettato, riprova più tardi',
                                type: 'success'
                            });
                    });
            } else {
                // utente non loggato, mostro dialog login / registrazione
                var dialog = $scope.dialogLogin();
                dialog.closePromise.then(function(data) {
                    switch (data.value) {
                        case 'registerSubmitted':
                        case 'loginSubmitted':
                            $scope.reserve();
                            break;
                    }
                });

            }
        } else {
            //alert($scope.valid_reservation)
        }
    };

    $scope.user_phone = {
        phone: ''
    };
    $scope.addPhoneNumber = function(){
        var user = UserServices.isCurrentLogged.isLoggedCurrently;
        if(user.loggedStatus !== 0 ){
            if($scope.user_phone.phone && $scope.user_phone.phone !== ''){
                UserServices.changeInfo
                .async(user.user._id,user.user.name,user.user.surname,$scope.user_phone.phone,user.user.description)
                .success(function(data){
                    if(data.message == 'ok'){
                        UserServices.getLogged.async().success(function(data) {
                            UserServices.isCurrentLogged.set(data);
                            $scope.current_user = data;
                            $scope.reserve();
                        });
                    }
                });
            }
        } else {
            return;
        }
    };

	$scope.initReservation = function(){
		// Reservation

        var hour = moment().hour(moment().add(1, 'hours').format('HH')).minute(0);
        $scope.reservation = {
            day: moment().format('DD'),
            month: moment().format('MMMM'),
            year: moment().format('YYYY'),
            hour: '',
            orario: null,
            date: null,
            people: "2"
        };

        


        $scope.people = [];
        for (var x = 1; x <= 20; x++) {
            $scope.people.push(x);
        }

        $scope.open = false;
        $scope.fasce_oggi = [];
        

        //$scope.current_picker_date = moment();
        
        $scope.current_picker = {
        	date: moment()
        };
        $scope.takeAvaibleFasce();
        $scope.avaible_fasce = [];        
        
        $scope.reservation_confirmed = false;
        $scope.reservation_as_to_add_phone = false;
        
	};

	$scope.current_user = {
	};

	UserServices.getLogged.async().success(function(data){
		UserServices.isCurrentLogged.set(data);
		$scope.current_user = data;
		// fai partire l'evento per prendere utente loggato
		$scope.$broadcast('userLogged','application');
	});

	$scope.$on('userLogged', function(event) { 
		// se l'utente ha fatto il login mettilo in scope
		$scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;
	});

	

    // Menu
    $scope.visual_menu = 0;
    $scope.currentMenu = null; // primo menu selezionato

    $scope.selectMenu = function(indice) {
        $scope.currentMenu = indice;
            if($scope.current_restaurant.menu && $scope.current_restaurant.menu.menus){
                if($scope.current_restaurant.menu.menus && $scope.current_restaurant.menu.menus.length > 0){
                    /**
                     * Impostazioni per visualizzazione menu testuale/visuale
                     */
                $scope.menu_type = 0; // imposto di default quello testuale
                angular.forEach($scope.current_restaurant.menu.menus[indice].categorie, function(v, k) {
                    angular.forEach(v.piatti, function(plate, key) {
                        //console.log('plate', plate);
                        if (plate.image && plate.image !== null) {
                            $scope.visual_menu = 1;
                            return;
                        }
                    });
                });
            }
        }
    };

    $scope.scrollToMenu = function() {
        var menuElement = angular.element(document.getElementById('menuContainer'));
        $document.scrollToElement(menuElement, 100, 500);
    };

    $scope.menu_type = 0;
    $scope.setMenuType = function(type) {
        $scope.menu_type = type;
    };




});