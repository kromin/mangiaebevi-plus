var UserPlateController = angular.module('AppUserPlateController', []);

UserPlateController
    .controller('UserPlateCtrl', function($scope, $rootScope, $cookies, $sce, $location, $route, $routeParams, $document, ngDialog, RestaurantsServices, UserServices, PlatesServices, SearchServices, ToolsServices, RecommendationServices, ImagesServices) {
        "ngInject";


    	if($routeParams.piatto){
    		PlatesServices.viewCommunitySingle.async($routeParams.piatto).success(function(data){
    			$scope.current_plate = data.plate;
    			console.log(data.plate)
    			PlatesServices.viewSingle.async($scope.current_plate.plate).success(function(dat){
    				$scope.current_plate_parent = dat.plate;
    				UserServices.viewSingle.async($scope.current_plate.user).success(function(da){
    					$scope.current_plate_user = da.user



    					var mainImage = ($scope.current_plate.image) ? $scope.$parent.staticURL + '/community/big/' + $scope.current_plate.image + '.png' : $scope.$parent.mainUrl+'/img/home_1.jpg';
                        var smallImage = ($scope.current_plate.image) ? $scope.$parent.staticURL + '/community/big/' + $scope.current_plate.image + '.png' : $scope.$parent.mainUrl+'/img/home_1.jpg';
                        $scope.$parent.setSEO(
                            $scope.current_plate_parent.name+' | MangiaeBevi+',
                            'restaurant',
                            window.location.href,
                            mainImage,
                            smallImage,
                            'Il piatto '+$scope.current_plate_parent.name+' su MangiaeBevi+, caricato da '+$scope.current_plate_user.name+' '+$scope.current_plate_user.surname);



    				})
    			})
    		})
    	} else {
    		$location.path('/');
    	}



        $scope.simpleError = function(){
            swal({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                title: 'Oops!',
                text: 'Qualcosa è andato storto',
                type: 'error'
            });
        }


        $scope.simpleSuccess = function(title,text){
            swal({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                title: title,
                text: text,
                type: 'success'
            });
        }

    });