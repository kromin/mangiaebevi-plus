var UserPiattiController = angular.module('AppUserPiattiController',[]);

UserPiattiController
.controller('UserPiattiCtrl',
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams, SocialServices, ToolsServices, UserServices, PlatesServices){
	"ngInject";

	$scope.user = null;
	$scope.initPiatti = function(user){
		$scope.user = user;
		$scope.initPlates();
	};

	$scope.pagination_number = 5;

	$scope.initPlates = function(){

		$scope.images_pagination = 1;
        $scope.plate_images = [];
        $scope.stop_load_images_pagination = false;
        $scope.loadPlateImages = function(){
            if($scope.images_pagination == 1) $scope.plate_images = [];
            var load_images_obj = {
                user: $scope.user,
                skip: ($scope.images_pagination == 1) ? 0 : parseInt($scope.images_pagination-1)*parseInt($scope.pagination_number),
                take: parseInt($scope.pagination_number)
            };
            PlatesServices.viewCommunityPlateByUser.async(load_images_obj).success(function(data){

                angular.forEach(data.plateimages,function(v,k){
                    $scope.plate_images.push(v);
                });
                if(data.plateimages.length < $scope.pagination_number){
                    $scope.stop_load_images_pagination = true;
                } else {
                    $scope.images_pagination++;
                }
            });

        };
        $scope.loadPlateImages();

        $scope.loadPlateImagesN = function(){
            $scope.loadPlateImages();
        };



        $scope.share = function(provider,plate) {

            var add_user_c = '';
            if ($scope.current_user && $scope.current_user.loggedStatus == 1) {

                // salva la condivisione nel db
                add_user_c = '?ib=' + $scope.current_user.user._id;

            }


            var url = $scope.$parent.mainUrl + '/community/piatto/' + plate._id +'/' + add_user_c;
            var text = 'Condividi il piatto che stai mangiando con MangiaeBevi+';
            var ogimage = (plate.image) ? $scope.staticURL + '/community/big/' + plate.image + '.png' : null;

            switch (provider) {
                case 'facebook_send':
                    FB.ui({
                        method: 'send',
                        link: '' + url,
                    });
                    break;
                case 'facebook_share':
                    FB.ui({
                        method: 'feed',
                        picture: ogimage,
                        name: 'MangiaeBevi+',
                        description: 'Condividi il piatto che stai mangiando con MangiaeBevi+',
                        caption: 'Il portale dei ristoranti in Italia',
                        redirect_uri: '' + url,
                        link: '' + url
                    });
                    break;
                case 'twitter':
                    window.open('https://twitter.com/share?text=' + text + '&url=' + url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
                    break;
                case 'google_plus':
                    window.open('https://plus.google.com/share?url=' + url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                    break;
                case 'linkedin':
                    window.open('https://linkedin.com/shareArticle?mini=false&url=' + url + '&title=Mangiaebevi&summary=' + text + '&source=' + url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                    break;
                case 'email':
                    break;
            }


        };


	};


});