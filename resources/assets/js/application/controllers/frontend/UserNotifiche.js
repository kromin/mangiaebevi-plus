var UserNotificationsController = angular.module('AppUserNotificationsController',[]);
 
UserNotificationsController
.controller('UserNotificationsCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams, NotificationsServices){
	"ngInject";

	
	
	$scope.notification_default_num = 10;
	$scope.notification_page = 0;
	$scope.notification_hide_loader = false;
	$scope.loading_notifications = true;
	
	$scope.loadMoreNotifications = function(){
		if(!$scope.current_user.user) return;
		$scope.loading_notifications = true;
		NotificationsServices.viewMine.async($scope.current_user.user._id,parseInt($scope.notification_page*$scope.notification_default_num),$scope.notification_default_num).success(function(data){
			angular.forEach(data.notifications,function(n,k){
				$scope.notifications.push(n);
			});

			if(data.notifications.length == $scope.notification_default_num){
				$scope.notification_page++;
				$scope.notification_hide_loader = false;
			} else {
				$scope.notification_hide_loader = true;
			}
			$scope.loading_notifications = false;
		});
	};

	$scope.firstNotificationLoad = function(user){
		
		NotificationsServices.viewMine.async(user,parseInt($scope.notification_page*$scope.notification_default_num),$scope.notification_default_num).success(function(data){
			$scope.notifications = data.notifications;
			
			if(data.notifications.length == $scope.notification_default_num){
				$scope.notification_page++;
				$scope.notification_hide_loader = false;
			} else {
				$scope.notification_hide_loader = true;
			}
			$scope.loading_notifications = false;
		});
		
	}

		

	
	
});