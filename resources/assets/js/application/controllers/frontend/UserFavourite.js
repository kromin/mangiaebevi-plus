var UserFavouriteController = angular.module('AppUserFavouriteController',[]);

UserFavouriteController
.controller('UserFavouriteCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,alertify,$routeParams, SocialServices, ToolsServices, UserServices){
	"ngInject";

	$scope.user = null;
	$scope.init = function(user){
		$scope.user = user;
		
	};

	$scope.favourites = [];
	$scope.favourite_lists = [];
	$scope.initFavourite = function(user){
		UserServices.getFavourites.async(user).success(function(data){
			
			$scope.favourites = data.favourites;
			$scope.favourite_lists = (data.lists) ? data.lists : [];
			$scope.initFavouriteDone();
		});
	};

	$scope.initFavouriteDone = function(){

		$scope.current_list_val = 0;

		$scope.current_list = {
			favourite_restaurants: []
		};
		
		$scope.setAllFavourite = function(){
			
			$scope.current_list_val = 0;
			$scope.current_list.favourite_restaurants = $scope.favourites;
		};
		// default load all
		$scope.setAllFavourite();

		$scope.setList = function(name){			
			if(!name || name === 0){
				$scope.setAllFavourite();
			} else {
				$scope.current_list.favourite_restaurants = [];
				angular.forEach($scope.favourite_lists,function(v,k){
					if(v.name == name) {
						$scope.current_list_val = name;
						angular.forEach($scope.favourites,function(r,kr){
							if(v.restaurants.indexOf(r._id) != -1) $scope.current_list.favourite_restaurants.push(r);
						});
					}
				});
			}
		};
		

		$scope.removeFavourite = function(restaurant){

			angular.forEach($scope.favourites,function(v,k){
				if(v == restaurant) $scope.favourites.splice(k,1);
			});

			angular.forEach($scope.favourite_lists,function(v,k){
				angular.forEach(v.restaurants,function(r,kk){
					if(r == restaurant) $scope.favourite_lists[k].restaurants.splice(kk,1);
				});
			});
			$scope.saveList();
		};

		$scope.addList = function(list_name){
			if(!list_name || list_name === '') return;

			if(list_name.length > 23){
				swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Nome troppo lungo',
					text: 'Inserisci un nome per la lista inferiore a 23 caratteri',
					type: 'error'
				});
				return;
			}
			var can = true;
			angular.forEach($scope.favourite_lists,function(v,k){
				if(v.name == list_name) can = false;
			});
			if(can) {
				$scope.add_list = {
								name: ''
							};

				$scope.favourite_lists.push({name: list_name, restaurants: []});
			}
			$scope.saveList();
		};

		$scope.removeList = function(name){
			alertify
			.okBtn("Si")
  			.cancelBtn("No")
  			.confirm("Sicuro di voler eliminare questa lista?", function () {	
				
			    angular.forEach($scope.favourite_lists,function(v,k){
					if(v.name == name) $scope.favourite_lists.splice(k,1);
				});
				$scope.setList(0);
				$scope.saveList();

			}, function() {
			    
			});			
			
		};

		$scope.removeFromList = function(restaurant,list_name){
			angular.forEach($scope.favourite_lists,function(v,k){
				if(v.name == list_name){
					angular.forEach(v.restaurants,function(r,kk){
						if(r == restaurant) $scope.favourite_lists[k].restaurants.splice(kk,1);
					});
				}				
			});
			$scope.saveList();
		};

		$scope.add_list = {
			name: ''
		};
		$scope.addToList = function(restaurant,list_name){
			angular.forEach($scope.favourite_lists,function(v,k){
				if(v.name == list_name){
					var i = v.restaurants.indexOf(restaurant);
					if(i != -1){
						$scope.favourite_lists[k].restaurants.splice(i,1);
					} else {
						$scope.favourite_lists[k].restaurants.push(restaurant);
					}					
				}				
			});
			$scope.saveList();
		};
		$scope.isInList = function(array,restaurant){
			return (array.indexOf(restaurant) != -1) ? true : false;
		};

		$scope.removeFavourite = function(restaurant){
			
			angular.forEach($scope.favourites,function(v,k){
				if(v._id == restaurant) $scope.favourites.splice(k,1);
			});

			angular.forEach($scope.favourite_lists,function(v,k){
				var i = $scope.favourite_lists[k].restaurants.indexOf(restaurant);
				if(i != -1) $scope.favourite_lists[k].restaurants.splice(i,1);
			});
			$scope.saveList();
		}; 

		$scope.saveList = function(){
			var f = [];
			angular.forEach($scope.favourites,function(v,k){
				f.push(v._id);
			});
			UserServices.setFavourites.async(f,$scope.favourite_lists).success(function(d){

			});
		};

		
	};
	
});