var StatRestaurantController = angular.module('AppStatRestaurantController',[]);

StatRestaurantController
.controller('StatRestaurantCtrl', function($scope,$rootScope,$cookies,$sce,$location,$routeParams,$route,$document,$timeout,ngDialog,RestaurantsServices,alertify,ReservationServices){
	"ngInject";
	
	$scope.$watch('restaurant',function(n,o){
		if(n && n._id) $scope.loadStats($scope.restaurant._id);
	})	



	$scope.colors = ['#45b7cd', '#ff6384', '#ff8e72', '#ff6384'];

    $scope.year_labels = [];
    $scope.year_data_views = [
      [], // page_view
      [], // unique page view
    ];
    $scope.year_data_time = [
      [], //session duration
    ];

    
    $scope.datasetOverride_views = [
      {
        label: "Visualizzazioni di pagina",
        borderWidth: 3,
        hoverBackgroundColor: "rgba(255,99,132,0.4)",
        hoverBorderColor: "rgba(255,99,132,1)",
        type: 'line',
        lineTension: 0,
    	steppedLine: true
      },
      {
        label: "Visualizzazioni uniche",
        borderWidth: 1,
        type: 'bar'
      }
    ];

    $scope.datasetOverride_time = [
      {
        label: "Tempo medio sulla pagina (minuti)",
        borderWidth: 3,
        hoverBackgroundColor: "rgba(255,99,132,0.4)",
        hoverBorderColor: "rgba(255,99,132,1)",
        type: 'bar',
        lineTension: 0,
    	steppedLine: true
      }
    ];

    $scope.month_labels = [];
    $scope.month_data = [
    	[],[],[]
    ];
    $scope.datasetOverride_days = [
      {
        label: "Visualizzazioni di pagina",
        borderWidth: 3,
        type: 'line'
      },
      {
        label: "Visualizzazioni uniche",
        borderWidth: 3,
        type: 'line',
        lineTension: 0,
    	steppedLine: true,
      },
      {
        label: "Tempo medio sulla pagina (minuti)",
        borderWidth: 3,
        type: 'line'
      }
    ];


    var setCharts = function(data){
    	var i = 0;
    	angular.forEach(data.year,function(v,k){
    		$scope.year_labels.push(moment(v[0],'MM').format('MMM'));
    		$scope.year_data_views[0].push(v[1]);
    		$scope.year_data_time[0].push(parseInt((v[2]) / 60));
    		$scope.year_data_views[1].push(v[3]);
    		i++;
    	})

    	angular.forEach(data.days,function(v,k){
    		$scope.month_labels.push(moment(v[0],'YYYYMMDD').format('DD - MM'));
    		$scope.month_data[0].push(v[1]);
    		$scope.month_data[2].push(parseInt((v[2]) / 60));
    		$scope.month_data[1].push(v[3]);
    		i++;
    	})
    }

    $scope.reservations = 0;
    $scope.medium_price = 0.00;
	$scope.loadStats = function(restaurant){
		if(!restaurant) return;
		RestaurantsServices.getStat.async(restaurant).success(function(data){
			
			$scope.reservations = data.stats.reservations;
			$scope.medium_price = data.stats.midprice;
			setCharts(data.stats)
		});
	}


	$scope.returnFormattedDifference = function(risto_prezzo,prezzo_medio){
		var ret = '';
		if(prezzo_medio > risto_prezzo){
			ret = '+ '+(prezzo_medio)-(risto_prezzo)+' circa';
		} else 
		if(prezzo_medio < risto_prezzo){
			ret = '- '+(risto_prezzo)-(prezzo_medio)+' circa';
		} else {
			ret = '= al prezzo del tuo ristorante'
		}
		return ret;
	}
});