var HomeController = angular.module('AppHomeController',[]);

HomeController
.controller('HomeCtrl', function($scope,$cookies,$sce,$interval,$timeout,$window,$location){
	"ngInject";
	$scope.main = {
		search: ''
	}
	$scope.goToSearch = function(){
		
		if($scope.main.search != ''){
			var add = '';
			if($cookies.get('lng') && $cookies.get('lat')) add += $cookies.get('lat') + '/' + $cookies.get('lng') + '/';
			window.location = '/s/'+encodeURI($scope.main.search)+'/'+add;
			
		}

	}


	$scope.adding_str_to_city_link = ''; // default per sicurezza
	if($cookies.get('lng') && $cookies.get('lat')) $scope.adding_str_to_city_link = $cookies.get('lat') + '/' + $cookies.get('lng') + '/';


});