var AddRestaurantController = angular.module('AppAddRestaurantController', []);

AddRestaurantController
    .controller('AddRestaurantCtrl', function($scope, $rootScope, $cookies, $sce, $location, $document, ngDialog, RestaurantsServices, UserServices, SearchServices, PaymentServices, SearchServices, ToolsServices) {
        "ngInject";
        window.scrollTo(0, 0);

        ToolsServices.getMainSiteInfo.async().success(function(data) {
            $scope.creditCard = _.indexBy(data.creditCard, '_id');
            $scope.homeCities = _.indexBy(data.homeCities, '_id');
            $scope.regioniCucine = _.indexBy(data.regioniCucine, '_id');
            $scope.tipiCucine = _.indexBy(data.tipiCucine, '_id');
            $scope.tipiLocali = _.indexBy(data.tipiLocali, '_id');
            $scope.services = _.indexBy(data.services, '_id');
            $scope.pacchetti = _.indexBy(data.pacchetti, 'price_month');
            $scope.staticURL = data.staticDomain; // domain for static files    
        }); 

        $scope.current_step = 0; //0->select payment, 1->login, 2-> restaurant, 3-> ciclo fatturazione, 4-> payment, 5->conferma visiva
        $scope.login_form = 1;

        $scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;
        if ($scope.current_user.loggedStatus == 1) {
                $scope.current_step = 1;
            }   

        $scope.$on('userLogged', function(event) {
            // se l'utente ha fatto il login mettilo in scope
            $scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;
            if ($scope.current_user.loggedStatus == 0) {
                $scope.current_step = 0;
                return;
            }
            if ($scope.current_user.loggedStatus == 1) {
                $scope.current_step = 1;
            }   
        });

        
        $scope.is_just_present_restaurant = false;
        $scope.show_other_restaurant_values = false; 
        $scope.addingRestaurant = {
            name: '',
            description: '',
            tipoLocale: [],
            address: '',
            city: '',
            state: '',
            zip: '',
            email: '',
            phone: '',
            web: '',
            priceMin: 10,
            priceMax: 20,
            staff: {
                chef: [],
                sommelier: []
            },
            social: [{
                      facebook: ""
                    },
                    {
                      twitter: ""
                    },
                    {
                      pinterest: ""
                    },
                    {
                      instagram: ""
                    },
                    {
                      googlePlus: ""
                    },
                    {
                      linkedin: ""
                    },
                    {
                      youtube: ""
                    },
                    {
                      tripadvisor: ""
                    }
            ],
            services: [],
            tipoCucina: [],
            regioneCucina: [],
            carte: []
        }
        $scope.just_added_restaurant = null;
        $scope.risto_searched = [];
        $scope.searchRestaurant = function() {
            $scope.risto_searched = [];
            SearchServices.searchByName.async($scope.addingRestaurant.name).success(function(data) {
                angular.forEach(data.restaurants, function(v, k) {
                    if (!v.user || v.user == 0 || v.user == '') {
                        $scope.risto_searched.push(v);
                    }
                });

                if ($scope.risto_searched.length == 0) {
                    $scope.show_other_restaurant_values = true; // mostra altri campi del form				
                }
            })
        }
        $scope.selectSearched = function(id) {
            $scope.is_just_present_restaurant = true;
            $scope.just_added_restaurant = id;
            $scope.isClaiming();
        }
        $scope.notInList = function() {
            $scope.show_other_restaurant_values = true;
        }

        $scope.selectTipologia = function(){
            if($scope.addingRestaurant.custom){
                $scope.addingRestaurant.tipoLocale = [$scope.addingRestaurant.custom]
            }
        }

        //initializeBraintreePayment();
        $scope.isClaiming = function() {
            RestaurantsServices.claim.async($scope.just_added_restaurant).success(function(data) {
                $scope.current_step = 8;
            });
        }

        $scope.addRestaurant = function() {
            var add = false;
            angular.forEach($scope.addingRestaurant.fasce,function(v,k){
                if(v.length > 0){
                    add = true;
                }
            });
            if(add){
                RestaurantsServices.addSingle.async($scope.addingRestaurant).success(function(data) {
                    $scope.just_added_restaurant = data.restaurant._id;
                    $scope.current_step = 6;
                    $scope.current_restaurant = data.restaurant
                });
            }
        }

        $scope.confirmBaseInfos = function() {
            $scope.current_step = 2;
        }

        $scope.confirmOtherInfos = function() {
            $scope.current_step = 3;
        }

        $scope.confirmStaff = function() {
            $scope.current_step = 4;
        }

        $scope.confirmSocial = function() {
            $scope.current_step = 5;
        }

        $scope.confirmOrari = function() {
            $scope.current_step = 7;
        }

        $scope.$watch('addingRestaurant.priceMin',function(val){
            console.log(val);
            if(val >= $scope.addingRestaurant.priceMax){
                $scope.addingRestaurant.priceMax = val;
            }
        })
        



        $scope.selectItem = function(id,type){

            switch(type){
                case 'service':
                    var index = $scope.addingRestaurant.services.indexOf(id)
                    if(index != -1){
                        $scope.addingRestaurant.services.splice(index,1);
                    } else {
                        $scope.addingRestaurant.services.push(id);
                    }
                break;
                case 'tipocucina':
                    var index = $scope.addingRestaurant.tipoCucina.indexOf(id)
                    if(index != -1){
                        $scope.addingRestaurant.tipoCucina.splice(index,1);
                    } else {
                        $scope.addingRestaurant.tipoCucina.push(id);
                    }
                break;
                case 'regionecucina':
                    var index = $scope.addingRestaurant.regioneCucina.indexOf(id)
                    if(index != -1){
                        $scope.addingRestaurant.regioneCucina.splice(index,1);
                    } else {
                        $scope.addingRestaurant.regioneCucina.push(id);
                    }
                break;
                case 'carta':
                    var index = $scope.addingRestaurant.carte.indexOf(id)
                    if(index != -1){
                        $scope.addingRestaurant.carte.splice(index,1);
                    } else {
                        $scope.addingRestaurant.carte.push(id);
                    }
                break;
            }
            
        }


        /**
        * ORARI Ristorante
        **/

        $scope.week_days = ['Lunedì','Martedì','Mercoledì','Giovedì','Venerdì','Sabato','Domenica'];
            $scope.add_fascia_oraria = {}
            if(!$scope.addingRestaurant.fasce){
                $scope.addingRestaurant.fasce = {
                    0:[],1:[],2:[],3:[],4:[],5:[],6:[]
                }
            }
            $scope.add_fascia_oraria = {
                day: 0
            }
            $scope.remove_fascia = function(fascia,day){
                $scope.addingRestaurant.fasce[day].splice(fascia,1);
            }
            $scope.add_fascia_to_restaurant = function(day){
                var fascia = $("#datetimepicker-time_from").val()+'-'+$("#datetimepicker-time_to").val();
                if(fascia && fascia != '' && day && day != ''){
                    $scope.addingRestaurant.fasce[day].push(fascia)                                                           
                }
            }

            $scope.add_fascia_to_restaurant_all_days = function(){
                var fascia = $("#datetimepicker-time_from").val()+'-'+$("#datetimepicker-time_to").val();
                if(fascia && fascia != ''){
                    angular.forEach($scope.addingRestaurant.fasce,function(v,k){
                        $scope.addingRestaurant.fasce[k].push(fascia)
                    })
                }
            }

            $('#datetimepicker-time_from, #datetimepicker-time_to').livequery(function() {
                $(this).datetimepicker({
                    locale: 'it',
                    //minDate: min_hour,
                    //useCurrent: true,
                    //defaultDate: hour,
                    stepping: 15, //$scope.current_restaurant.time_shift,
                    format: 'HH:mm'
                }).on("dp.change", function(e) {
                    
                })
            });



            /**
            * UPload
            */
            if($scope.current_restaurant){
                $scope.current_restaurant.adding_image = false
            }
            
            $scope.uploadImage = function(){
                if($scope.current_restaurant.adding_image && !$scope.uploading_image){
                    $scope.uploading_image = true;
                    RestaurantsServices
                    .uploadImage
                    .async($scope.current_restaurant._id,$scope.current_restaurant.adding_image)
                    .success(function(data){      
                    if($scope.current_restaurant.images){
                        $scope.current_restaurant.images.push(data.image);                        
                    }   else {
                        $scope.current_restaurant.images = [data.image]
                    }                                                   
                        $scope.current_restaurant.image = data.image;
                        $scope.current_restaurant.adding_image = null;
                        $scope.uploading_image = false;
                        $scope.current_step = 7;
                    });
                    
                }
            }


            $scope.addFinalRestaurant = function(){
                if($scope.current_restaurant.description && $scope.current_restaurant.description != ''){
                    RestaurantsServices
                    .editSingle.async($scope.current_restaurant._id,$scope.current_restaurant)
                    .success(function(data){
                        $scope.current_step = 9;
                    })
                }
            }


    });