var TopbarController = angular.module('AppTopbarController',[]);

TopbarController
.controller('TopbarCtrl', function($scope,$rootScope,$route,$cookies,$sce,$location,UserServices,NotificationsServices,$interval,alertify){
	"ngInject";
	$scope.$on('userLogged', function(event, param) { 
		initTopbar();
	});


	var initTopbar = function(){
		$scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;
	}

	$scope.logout = function(){
		UserServices.logout.async().success(function(data){
			if(data.message == 'ok') {
				
				UserServices.getLogged.async().success(function(data){

					$scope.current_user = data;
					UserServices.isCurrentLogged.set($scope.current_user);
					$rootScope.$broadcast('userLogged','logged out');
					window.location = "/";

				});
				
			}
		})
	}

	$scope.openDropUserMenu = function(){
		$scope.drop_open = !$scope.drop_open;
	}

	$scope.openLogin = function(val){
		if(!val) val = 3;
		$scope.dialogLogin(val);
	}

	$scope.openRegistration = function(){
		$scope.dialogRegister();
	}

	$scope.goToAdministration = function(){
		window.location = "/administration/";
	}
	
	$scope.openmobile = false;
	$scope.openMenuMobile = function(){
		$scope.openmobile = !$scope.openmobile;
		angular.element("#mobilemenu").toggleClass("open");
	}
	$scope.$on('$routeChangeSuccess',function(){
		$scope.openmobile = false;
		$scope.drop_open = false;
		angular.element("#mobilemenu").removeClass("open");
	});



	var LoadNotifications = function(){
		
		if(UserServices.isCurrentLogged.isLoggedCurrently.loggedStatus != 1) return;
		NotificationsServices.viewToRead.async().success(function(data){
			
			angular.forEach(data.notifications,function(notification,k){
				// mostra in alertify
				switch(notification.type){
					case 0:
					var icon_class = 'mebi-profile text-success',
						title = 'Nuova registrazione',
						text = notification.parameters[1]+" si è iscritto",
						link = "/u/"+notification.parameters[0]+"/"
					break;
					case 1:
					var icon_class = "mebi-list text-primary",
						title = "Nuovo ordine",
						text = notification.parameters[2] +" ha effettuato un ordine",
						link = "/modifica-ristorante/"+notification.parameters[5]+"/"
					break;
					case 2:
					var icon_class = "mebi-cutlery text-info",
						title = "Ristorante aggiunto",
						text = notification.parameters[1] + " ha aggiunto un ristorante",
						link = "/modifica-ristorante/"+notification.parameters[2]+"/"
					break;
					case 3:
					var icon_class = "mebi-paypal text-success",
						title = "Nuova iscrizione a un piano di abbonamento",
						text = notification.parameters[1]+" ha sottoscritto un piano di abbonamento",
						link = "/u/"+notification.parameters[0]+'/'
					break;
					case 4:
					var icon_class = "mebi-phone-call text-warning",
						title = "Claim",
						text = notification.parameters[1]+" reclama un ristorante",
						link = "/modifica-ristorante/"+notification.parameters[2]+'/'
					break;
					case 5:
					var icon_class = "mebi-check-1 text-success",
						title = "Richiesta approvata",
						text = "La tua richiesta di gestione del ristorante è stata approvata",
						link = "/modifica-ristorante/"+notification.parameters[0]+'/'
					break;
					case 6:
					var icon_class = "mebi-check-1 text-success",
						title = "Ristorante approvato",
						text = "Congratulazioni, il ristorante che hai aggiunto è stato approvato",
						link = "/modifica-ristorante/"+notification.parameters[0]+'/'
					break;
					case 7:
					var icon_class = "mebi-refund text-danger",
						title = "Chiesto rimborso",
						text = "&Egrave; stato chiesto un rimborso per l'ordine numero "+notification.parameters[0],
						link = "/modifica-ristorante/"+notification.parameters[1]+"/"
					break;
					case 8:
					var icon_class = "mebi-refund text-success",
						title = "Ordine rimborsato",
						text = "L'ordine numero "+notification.parameters[0]+" è stato rimborsato",
						link = ""
					break;
					case 9:
					var icon_class = "mebi-calendar-1 text-info",
						title = "Nuova prenotazione",
						text = notification.parameters[1]+ " ha prenotato al tuo ristorante",
						link = "/modifica-ristorante/"+notification.parameters[2]
					break;
					case 10:
					var icon_class = "mebi-delivery text-danger",
						title = "Driver",
						text = "Un utente vuole essere un driver",
						link = "/u/"+notification.parameters[0]
					break;
					case 11:
					var icon_class = "mebi-check-1 text-success",
						title = "Prenotazione confermata",
						text = "La tua prenotazione a "+notification.parameters[2]+" ("+notification.parameters[3]+") è stata confermata",
						link = ""
					break;
					case 12:
					var icon_class = "mebi-check-1 text-primary",
						title = "Accettato cambio prenotazione",
						text = "Il cambio di data della prenotazione di "+notification.parameters[4]+" ("+notification.parameters[3]+") è stato accettato",
						link = ""
					break;
					case 13:
					var icon_class = "mebi-calendar-1 text-danger",
						title = "Data prenotazione cambiata",
						text = "La tua prenotazione a "+notification.parameters[2]+" è stata modificata ("+notification.parameters[3]+" -> "+notification.parameters[3]+") ",
						link = ""
					break;

					case 14:
					var icon_class = "mebi-close text-danger",
						title = "Rifiutato cambio prenotazione",
						text = notification.parameters[1]+" ha rifiutato il cambio di orario della prenotazione",
						link = "/u/"+notification.parameters[0]+'/'
					break;
					case 15:
					var icon_class = "mebi-avatar text-primary",
						title = "Nuovo follower",
						text = notification.parameters[1]+" ha iniziato a seguirti",
						link = "/u/"+notification.parameters[0]+'/'
					break;
					case 16:
					var icon_class = "mebi-avatar text-success",
						title = "Richiesta di amicizia accettata",
						text = notification.parameters[1]+" ha accettato la tua richiesta di amicizia",
						link = "/u/"+notification.parameters[0]+'/'
					break;
					case 17:
					var icon_class = "mebi-avatar text-warning",
						title = "Nuova raccomandazione",
						text = notification.parameters[1]+" ha raccomandato il tuo ristorante",
						link = "/u/"+notification.parameters[0]+'/'
					break;	
					case 18:
					var icon_class = "mebi-avatar text-success",
						title = "Nuova richiesta di amicizia",
						text = notification.parameters[1]+" ti ha inviato una richiesta di amicizia",
						link = "/u/"+notification.parameters[0]+'/'
					break;
					case 19:
					var icon_class = "mebi-avatar text-success",
						title = "Risposta alla richiesta di raccomandazione",
						text = notification.parameters[1]+" ha risposto alla tua richiesta di raccomandazione",
						link = ""
					break;				
				}

				var html_element = '<div class="notification_alert">'+
									'<i class="'+icon_class+'"></i>'+
									'<div><h5>'+title+'</h5>'+
									'<p>'+text+'</p></div>'+
									'</div>';


				alertify.log(html_element);
			});
			
		});
	}
	$interval(LoadNotifications, 1000*20); // carica notifiche ogni 20 secondi

});