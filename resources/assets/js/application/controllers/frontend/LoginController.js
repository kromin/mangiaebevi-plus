var LoginController = angular.module('AppLoginController', []);

LoginController
    .controller('LoginCtrl', function($scope, $rootScope, $cookies, $sce, UserServices, ToolsServices) {
        "ngInject";
        
        $scope.accessWithSocial = function(provider){
            var win = window.open('/socialog/'+provider,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes');   
            var timer = setInterval(function() {   
                if(win.closed) {  
                    clearInterval(timer);  
                    window.location.href = '/';
                }  
            }, 1000);   
        };

        $scope.login_form_error_message = false;
        $scope.login = function(){
            ToolsServices.mainOverlay.show();
            UserServices.login.async($scope.login.email,$scope.login.password).success(function(data){
                // richiama di nuovo il dialog del login
                if(data.message == 'ok'){
                    window.location.href = '/';            
                } else {
                    if(data.error_message){
                        $scope.login_form_error_message = data.error_message;
                    }
                }
                ToolsServices.mainOverlay.hide();
                // messaggio di errore
            }).error(function(d){
                swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Oops!',
                    text: 'Qualcosa è andato storto',
                    type: 'error'
                });
                ToolsServices.mainOverlay.hide();
            });
        };
        
    });

var RecoveryController = angular.module('AppRecoveryController', []);

RecoveryController
    .controller('RecoveryCtrl', function($scope, $rootScope, $cookies, $sce, UserServices, ToolsServices) {
        "ngInject";
        
        $scope.recovery_success = false;
        $scope.error_recovery = false;
        $scope.recovery = function(){
            ToolsServices.mainOverlay.show();
            UserServices.recovery.async($scope.recovery.email).success(function(data){
                // richiama di nuovo il dialog del login
                if(data.message == 'ok'){
                    $scope.recovery_success = 1;    
                } else{
                    switch(data.message){
                        case 'error.empty':
                        $scope.error_recovery = 1;
                        break;
                        case 'error.sociallog':
                        $scope.error_recovery = 2;
                        break;
                    }
                }
                ToolsServices.mainOverlay.hide();
                // messaggio di errore
            });
        };
});

var ResendController = angular.module('AppResendController', []);

ResendController
    .controller('ResendCtrl', function($scope, $rootScope, $cookies, $sce, UserServices, ToolsServices) {
        "ngInject";
        
        $scope.resend_success = false;
        $scope.error_resend = false;
        $scope.resend = function(){
            ToolsServices.mainOverlay.show();
            UserServices.resend.async($scope.resend.email).success(function(data){
                // richiama di nuovo il dialog del login
                if(data.message == 'ok'){
                    $scope.resend_success = 1;    
                } else{
                    switch(data.message){
                        case 'error.empty':
                        $scope.error_resend = 1;
                        break;
                        case 'error.sociallog':
                        $scope.error_resend = 2;
                        break;
                    }
                }
                ToolsServices.mainOverlay.hide();
                // messaggio di errore
            });
        };
});