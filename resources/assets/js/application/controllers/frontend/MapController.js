var MapController = angular.module('AppMapController', []);

MapController
    .controller('MapCtrl', function($scope, $route, $rootScope, $cookies, $sce, Initializer, ToolsServices, ngDialog) {
        "ngInject";
        /**
         *  Crea la mappa quando vengono inizializzati i ristoranti in mappa nel controller padre
         *  @param $scope.map_restaurants
         *
         **/
        /*
        	$scope.$watch('map_restaurants',function(){
        		setMapFromType();
        	});
        */

        $scope.$on('initializeMap', function(event,data) {            
            console.log('mappa',data);
            setMapFromType(data);
        });

        $scope.end_direction_address = '';
        $scope.$watch('current_restaurant', function() {
            if ($scope.current_restaurant && $scope.current_restaurant.address) {
                $scope.end_direction_address = $scope.current_restaurant.address;
            }
        })


        $scope.current_map = {
            center: {
                lat: 0,
                lng: 0
            }
        }

        $scope.markers = []
        $scope.avaible_restaurants_for_directions = [];
        $scope.temporary_avaible_restaurants_for_directions = []


        $scope.start_direction_address = '';

        $scope.directions_texts = '';



        var setMapFromType = function(data) {

            if (data) { //&& $scope.map_restaurants.length > 0					
                //console.log('map_restaurants',$scope.map_restaurants)
                var n = 0;
                $scope.markers = [];

                angular.forEach(data, function(v, k, item) {

                    if (v.loc.coordinates[0] && v.loc.coordinates[1]) {
                        $scope.markers.push({
                            location: {
                                lat: v.loc.coordinates[1],
                                lng: v.loc.coordinates[0]
                            },
                            restaurant: v
                        })


                        if (n == 0) {
                            $scope.current_map.center.lat = v.loc.coordinates[1]
                            $scope.current_map.center.lng = v.loc.coordinates[0]
                        }
                        n++;
                    }
                });
                //console.log('markers', $scope.markers);
                //var new_arr = _.uniq($scope.avaible_restaurants_for_directions);
                //$scope.avaible_restaurants_for_directions = new_arr;
                buildMap();
            }
        }



        var map,
            infowindow,
            gmarkers = [],
            MebMapStyle = null,
            mc,
            myLatlng,
            overlay;




        var setMapStyle = function(map_styles) {
            MebMapStyle = new google.maps.StyledMapType([{
                    //set saturation for the labels on the map
                    elementType: "labels",
                    stylers: [{
                        saturation: map_styles.saturation_value
                    }]
                }, { //poi stands for point of interest - don't show these lables on the map 
                    featureType: "poi",
                    elementType: "labels",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    //don't show highways lables on the map
                    featureType: 'road.highway',
                    elementType: 'labels',
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    //don't show local road lables on the map
                    featureType: "road.local",
                    elementType: "labels.icon",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    //don't show arterial road lables on the map
                    featureType: "road.arterial",
                    elementType: "labels.icon",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    //don't show road lables on the map
                    featureType: "road",
                    elementType: "geometry.stroke",
                    stylers: [{
                        visibility: "off"
                    }]
                },
                //style different elements on the map
                {
                    featureType: "transit",
                    elementType: "geometry.fill",
                    stylers: [{
                        hue: map_styles.main_color
                    }, {
                        visibility: "on"
                    }, {
                        lightness: map_styles.brightness_value
                    }, {
                        saturation: map_styles.saturation_value
                    }]
                }, {
                    featureType: "poi",
                    elementType: "geometry.fill",
                    stylers: [{
                        hue: map_styles.main_color
                    }, {
                        visibility: "on"
                    }, {
                        lightness: map_styles.brightness_value
                    }, {
                        saturation: map_styles.saturation_value
                    }]
                }, {
                    featureType: "poi.government",
                    elementType: "geometry.fill",
                    stylers: [{
                        hue: map_styles.main_color
                    }, {
                        visibility: "on"
                    }, {
                        lightness: map_styles.brightness_value
                    }, {
                        saturation: map_styles.saturation_value
                    }]
                }, {
                    featureType: "poi.sport_complex",
                    elementType: "geometry.fill",
                    stylers: [{
                        hue: map_styles.main_color
                    }, {
                        visibility: "on"
                    }, {
                        lightness: map_styles.brightness_value
                    }, {
                        saturation: map_styles.saturation_value
                    }]
                }, {
                    featureType: "poi.attraction",
                    elementType: "geometry.fill",
                    stylers: [{
                        hue: map_styles.main_color
                    }, {
                        visibility: "on"
                    }, {
                        lightness: map_styles.brightness_value
                    }, {
                        saturation: map_styles.saturation_value
                    }]
                }, {
                    featureType: "poi.business",
                    elementType: "geometry.fill",
                    stylers: [{
                        hue: map_styles.main_color
                    }, {
                        visibility: "on"
                    }, {
                        lightness: map_styles.brightness_value
                    }, {
                        saturation: map_styles.saturation_value
                    }]
                }, {
                    featureType: "transit",
                    elementType: "geometry.fill",
                    stylers: [{
                        hue: map_styles.main_color
                    }, {
                        visibility: "on"
                    }, {
                        lightness: map_styles.brightness_value
                    }, {
                        saturation: map_styles.saturation_value
                    }]
                }, {
                    featureType: "transit.station",
                    elementType: "geometry.fill",
                    stylers: [{
                        hue: map_styles.main_color
                    }, {
                        visibility: "on"
                    }, {
                        lightness: map_styles.brightness_value
                    }, {
                        saturation: map_styles.saturation_value
                    }]
                }, {
                    featureType: "landscape",
                    stylers: [{
                        hue: map_styles.main_color
                    }, {
                        visibility: "on"
                    }, {
                        lightness: map_styles.brightness_value
                    }, {
                        saturation: map_styles.saturation_value
                    }]
                }, {
                    featureType: "road",
                    elementType: "geometry.fill",
                    stylers: [{
                        hue: map_styles.main_color
                    }, {
                        visibility: "on"
                    }, {
                        lightness: map_styles.brightness_value
                    }, {
                        saturation: map_styles.saturation_value
                    }]
                }, {
                    featureType: "road.highway",
                    elementType: "geometry.fill",
                    stylers: [{
                        hue: map_styles.main_color
                    }, {
                        visibility: "on"
                    }, {
                        lightness: map_styles.brightness_value
                    }, {
                        saturation: map_styles.saturation_value
                    }]
                }, {
                    featureType: "water",
                    elementType: "geometry",
                    stylers: [{
                        hue: map_styles.main_color
                    }, {
                        visibility: "on"
                    }, {
                        lightness: map_styles.brightness_value
                    }, {
                        saturation: map_styles.saturation_value
                    }]
                }
            ], {
                name: 'MEB Map'
            });
        }




        var buildMap = function() {

            Initializer.mapsInitialized.then(function() {
                // ho caricato le api
                // imposto il marker
                $scope.markerImage = {
                    url: '/img/marker.png'
                };

                // richiamo le funzioni alla fine dell'animazione
                setTimeout(function() {
                    initMap()
                    $scope.addMarkers();
                }, 300)

            })
        }


        var initMap = function() {
            gmarkers = [];
            
            var map_styles = {
                main_color: '#00b7bb',
                saturation_value: -20,
                brightness_value: 5
            }

            setMapStyle(map_styles); // richiama lo styling della mappa
            map = new google.maps.Map(document.getElementById('google_map'), {
                center: {
                    lat: parseFloat($scope.current_map.center.lat),
                    lng: parseFloat($scope.current_map.center.lng)
                },
                draggable: true,
                scrollwheel: false,
                zoom: 15
            });

            infowindow = new google.maps.InfoWindow({
                content: ''
            });


            map.mapTypes.set('mebmap', MebMapStyle);
            map.setMapTypeId('mebmap');


            // initCustomMarker(); colpa dell'asincrono

        }

        $scope.markerImage = {}
        $scope.currentRestaurantInMap = {} // titolo default
            // Adds a marker to the map.
        var addMarker = function(markerObj, map) { //, markerText
            // Add the marker at the clicked location, and add the next-available label
            // from the array of alphabetical characters.


            //var desc = markerObj.restaurant.description;
            //markerObj.restaurant.description = (desc && desc != '') ? $sce.trustAsHtml(desc) : '';
            var marker = new google.maps.Marker({
                position: markerObj.location,
                restaurant: markerObj,
                icon: $scope.markerImage
            });


            marker.addListener('click', function() {

                // setto il corrente da aprire in info window	
                for (var i = 0; i < gmarkers.length; i++) {
                    // imposta a tutti i marker icona default
                    //gmarkers[i].setIcon($scope.markerImage); da cambiare il bordo del canvas
                }
                //marker.setIcon($scope.markerImageHighlight); 
                $scope.marker_open = markerObj.restaurant;
                ngDialog.open({
                    template: '/templates/dialogmaprestaurant',
                    scope: $scope,
                    className: 'ngdialog ngdialog-theme-default'
                });


                //infowindow.setContent(infoWindowHtml(markerObj.restaurant));
                //infowindow.open(map, marker);
            });
            gmarkers.push(marker);

        }

        var location = {
            lat: 0,
            lon: 0
        }

        $scope.addMarkers = function() {
            angular.forEach($scope.markers, function(m, k) {
                addMarker(m, map);
            })

            /*
             * Raggruppa i marker
             *
             */
            mc = new MarkerClusterer(map, gmarkers);
            // inizializzo la mappa dopo il caricamento di tutti i marker				
        }


        var infoWindowHtml = function(restaurant) {
            var ret = '<div class="google-info-window">'
            if (restaurant.image && restaurant.image != '' && restaurant.image != null) {
                var image = ''
                image = $scope.staticURL + '/restaurant/square/' + restaurant.image + '.png';
                ret += '<img class="small-thumb" src="' + image + '" alt="" title="" />'
            }
            ret += '<div class="map_text"><h3><a href="/restaurant/' + restaurant.slug + '">' + restaurant.name + '</a></h3>' + '<p>' + restaurant.address + '</p></div>' + '</div>';

            return ret;

        }



        $scope.calculateDirections = function(end_direction) {
            //console.log($scope.end_direction_address)
            end_direction = ($scope.end_direction_address) ? $scope.end_direction_address : end_direction; 
            if ($scope.start_direction_address != '' && end_direction != '') {
                var directionsService = new google.maps.DirectionsService;
                var directionsDisplay = new google.maps.DirectionsRenderer;

                var main_color = '#2d313f',
                    saturation_value = -20,
                    brightness_value = 5;


                map = new google.maps.Map(document.getElementById('google_map'), {
                    center: {
                        lat: parseFloat($scope.current_map.center.lat),
                        lng: parseFloat($scope.current_map.center.lng)
                    },
                    zoom: 7
                });

                directionsDisplay.setMap(map);

                map.mapTypes.set('mebmap', MebMapStyle);
                map.setMapTypeId('mebmap');

                calculateAndDisplayRoute(directionsService, directionsDisplay, end_direction);
            }
        }

        function calculateAndDisplayRoute(directionsService, directionsDisplay, end_direction) {
            //console.log(end_direction)

            end_direction = ($scope.end_direction_address) ? $scope.end_direction_address : end_direction; 
            directionsService.route({
                origin: $scope.start_direction_address,
                destination: end_direction,
                travelMode: google.maps.TravelMode.DRIVING
            }, function(response, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);

                    ToolsServices.googleDirections
                        .async($scope.start_direction_address, end_direction)
                        .success(function(data) {
                            
                            //if(data.message == 'ok')
                            $scope.directions_texts = []; //.directions;
                            angular.forEach(data.routes[0].legs[0].steps, function(val, k) {
                                $scope.directions_texts.push($sce.trustAsHtml(val.html_instructions));
                            })
                        })

                } else {
                    window.alert('Inserisci un indirizzo valido: indirizzo, CAP, città');
                }
            });
        }



        /**
         *
         * Custom markers
         * createMarker(25, 25, 4, markerObj)
         */
        function createMarker(width, height, radius, markerObj) {

            // markerObj.restaurant
            var canvas, context, imageObj;

            canvas = document.createElement("canvas");
            canvas.width = width;
            canvas.height = height;



            context = canvas.getContext("2d");

            //context.clearRect(0,0,width,height);

            // background is yellow
            context.fillStyle = "rgba(255,255,255,1)";

            context.strokeStyle = "rgba(0,0,0,1)";

            //context.moveTo(2, 2);

            context.arc(12, 12, 12, 0, 2 * Math.PI);
            context.closePath();

            context.fill();
            context.stroke();

            return canvas.toDataURL();

        }



    });