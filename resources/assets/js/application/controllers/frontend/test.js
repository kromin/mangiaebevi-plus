var TestAppControllers = angular.module('MEBTestControllers',[]);

TestAppControllers
.controller('TestCtrl', ['$scope', '$cookies', '$sce',
	'UserServices', 
	'NewsLetterServices', 
	'ServicesServices', 'RegionsServices', 'TypesServices', 'TipoLocaleServices', 'LocalityServices', 'CreditCardServices',
	'RestaurantsServices',  
	'ReservationServices',
	'PlatesServices','OrdersServices',
	'BlogCategoryServices', 'BlogArticlesServices',
	'NotificationsServices', 'CommunityServices',
	'ImagesServices',
	'FillingServices',
	'AdministrationServices', 'SettingsServices',
	'SearchServices',
	'ToolsServices',
	'$timeout',
	function TestCtrl($scope,$cookies,$sce,UserServices,NewsLetterServices, ServicesServices, RegionsServices, TypesServices, TipoLocaleServices, LocalityServices, CreditCardServices, RestaurantsServices, ReservationServices, PlatesServices, OrdersServices, BlogCategoryServices, BlogArticlesServices, NotificationsServices, CommunityServices, ImagesServices, FillingServices, AdministrationServices, SettingsServices, SearchServices, ToolsServices, $timeout){	
		
		$scope.current_page = 0;

		$scope.return_data = {};
		$scope.return_error = "";


		$scope.isLoggedUser = function(){
			UserServices
			.getLogged
			.async().success(function(data){
				console.log(data)
				$scope.logged_status = data;
			})
		}

		$scope.registerForm = {};
		$scope.register = function(){
			UserServices
			.register
			.async($scope.registerForm.email,$scope.registerForm.name,$scope.registerForm.surname,$scope.registerForm.password,$scope.registerForm.phone,$scope.registerForm.role)
			.success(function(data){				
				$scope.register_status = data;
			})
			.error(function(error){				
				$scope.register_status = error;
			})
		}

		$scope.completeProfileForm = {}
		$scope.completeProfile = function(){
			UserServices
			.completeProfile
			.async($scope.completeProfileForm.ID,$scope.completeProfileForm.address,$scope.completeProfileForm.born)
			.success(function(data){				
				$scope.complete_profile_status = data;
			})
		}

		$scope.loginForm = {};
		$scope.login = function(){
			UserServices
			.login
			.async($scope.loginForm.email,$scope.loginForm.password)
			.success(function(data){				
				$scope.login_status = data;
			})
			.error(function(error){				
				$scope.login_status = error;
			})
		}

		$scope.logout = function(){
			UserServices
			.logout
			.async()
			.success(function(data){				
				$scope.logout_status = data;
			})
		}

		$scope.viewAll = function(){
			UserServices
			.viewAll
			.async()
			.success(function(data){				
				$scope.users = data;
			})
		}

		$scope.userForm = {};
		$scope.viewSingle = function(){
			UserServices
			.viewSingle
			.async($scope.userForm.ID)
			.success(function(data){				
				$scope.user = data;
			})
		}

		$scope.userDeleteForm = {};
		$scope.removeSingle = function(){
			UserServices
			.removeSingle
			.async($scope.userDeleteForm.ID)
			.success(function(data){				
				$scope.user_deleted = data;
			})
		}

		$scope.changeRoleForm = {};
		$scope.cambia_ruolo = function(){
			UserServices
			.changeRole
			.async($scope.changeRoleForm.ID,$scope.changeRoleForm.new_role)
			.success(function(data){				
				$scope.changedrole = data;
			})
		}


		$scope.changeUserInfoForm = {};
		$scope.customUserForm = {};
		$scope.setCustomUser = function(){
			UserServices
			.viewSingle
			.async($scope.customUserForm.ID)
			.success(function(data){				
				$scope.changeUserInfoForm = data.user;
			})
		}
		$scope.cambia_info_utente = function(){
			UserServices
			.changeInfo
			.async($scope.changeUserInfoForm._id,$scope.changeUserInfoForm.name,$scope.changeUserInfoForm.surname,$scope.changeUserInfoForm.phone)
			.success(function(data){				
				$scope.changeuserinfo = data;
			})
		}

		$scope.changeAvatar = {};
		$scope.change_avatar = function(){
			UserServices
			.modAvatar
			.async($scope.changeAvatar.ID,$scope.changeAvatar.image)
			.success(function(data){				
				$scope.avatar_changed = data;
			})
		}

		$scope.changeMail = {};
		$scope.change_email = function(){
			UserServices
			.changeMail
			.async($scope.changeMail.ID,$scope.changeMail.mail)
			.success(function(data){				
				$scope.mail_changed = data;
			})
		}

		$scope.changePwd = {};
		$scope.change_pwd = function(){
			UserServices
			.changePwd
			.async($scope.changePwd.ID,$scope.changePwd.old,$scope.changePwd.newp,$scope.changePwd.newpc)
			.success(function(data){				
				$scope.pwd_changed = data;
			})
		}



		/**
		*	Newsletter functions
		*	
		*/

		$scope.subscriber = {};
		$scope.subscriber_form = {};
		$scope.get_subscriber = function(){
			NewsLetterServices
			.getSubscriber
			.async($scope.subscriber_form.ID)
			.success(function(data){				
				$scope.subscriber = data;
			})
		}
		
		$scope.registered_subscriber = {};
		$scope.register_subscriber_form = {};
		$scope.register_subscriber = function(){
			NewsLetterServices
			.register
			.async($scope.register_subscriber_form.email,$scope.register_subscriber_form.name)
			.success(function(data){				
				$scope.registered_subscriber = data;
			})
		}
		
		$scope.removed_from_mail_subscriber = {};
		$scope.removed_from_mail_subscriber_form = {};
		$scope.remove_with_mail_subscriber = function(){
			NewsLetterServices
			.remove_from_mail
			.async($scope.removed_from_mail_subscriber_form.ID,$scope.removed_from_mail_subscriber_form.stringa)
			.success(function(data){				
				$scope.removed_from_mail_subscriber = data;
			})
		}


		$scope.all_subscriber = {};
		$scope.get_all_subscriber = function(){
			NewsLetterServices
			.viewAll
			.async()
			.success(function(data){				
				$scope.all_subscriber = data;
			})
		}


		$scope.update_subscriber_result = {};
		$scope.update_subscriber_form = {};
		$scope.update_subscriber_form_set_single = {};
		$scope.update_subscriber_set_single = function(){
			NewsLetterServices
			.getSubscriber
			.async($scope.update_subscriber_form_set_single.ID)
			.success(function(data){				
				$scope.update_subscriber_form = data.subscriber;
			})
		}
		$scope.update_subscriber = function(){
			NewsLetterServices
			.updateSubscriber
			.async($scope.update_subscriber_form._id,$scope.update_subscriber_form.email,$scope.update_subscriber_form.name)
			.success(function(data){				
				$scope.update_subscriber_result = data;
			})
		}

		$scope.removed_subscriber = {};
		$scope.removed_subscriber_form = {};
		$scope.delete_subscriber = function(){
			NewsLetterServices
			.deleteSubscriber			
			.async($scope.removed_subscriber_form.ID)
			.success(function(data){				
				$scope.removed_subscriber = data;
			})
		}



		/**
		*
		* Services functions
		*
		*/

		$scope.view_service = {}
		$scope.view_service_single = {}
		$scope.view_single_service = function(){
			ServicesServices
			.getSingle			
			.async($scope.view_service_single.ID)
			.success(function(data){				
				$scope.view_service = data;
			})
		}

		$scope.view_services = {}		
		$scope.view_services = function(){
			ServicesServices
			.viewAll			
			.async()
			.success(function(data){				
				$scope.view_services = data;
			})
		}

		$scope.add_service = {}
		$scope.add_service_single = {}
		$scope.add_single_service = function(){
			ServicesServices
			.addSingle			
			.async($scope.add_service_single.name,$scope.add_service_single.image)
			.success(function(data){				
				$scope.add_service = data;
			})
		}



		$scope.edit_service = {}
		$scope.edit_service_single_view = {}
		$scope.set_single_service_edit = function(){
			ServicesServices
			.getSingle			
			.async($scope.edit_service_single_view.ID)
			.success(function(data){				
				$scope.edit_service_single = data.service;
			})
		}
		$scope.edit_service_single = {}
		$scope.edit_single_service = function(){
			ServicesServices
			.editSingle			
			.async($scope.edit_service_single._id,$scope.edit_service_single.name,$scope.edit_service_single.image)
			.success(function(data){				
				$scope.edit_service = data;
			})
		}


		$scope.remove_service_single_view = {}
		$scope.set_single_service_remove = function(){
			ServicesServices
			.removeSingle			
			.async($scope.remove_service_single_view.ID)
			.success(function(data){				
				$scope.remove_service_single = data;
			})
		}


		$scope.service_massive_fill_result = {}
		$scope.set_service_massive_fill = function(){
			ServicesServices
			.massiveFill			
			.async()
			.success(function(data){				
				$scope.service_massive_fill_result = data;
			})
		}

		$scope.service_massive_clean_result = {}
		$scope.set_service_massive_clean = function(){
			ServicesServices
			.massiveClean			
			.async()
			.success(function(data){				
				$scope.service_massive_clean_result = data;
			})
		}


		/**
		*
		* Regions functions
		*
		*/

		$scope.view_region = {}
		$scope.view_region_single = {}
		$scope.view_single_region = function(){
			RegionsServices
			.getSingle			
			.async($scope.view_region_single.ID)
			.success(function(data){				
				$scope.view_region = data;
			})
		}

		$scope.view_regions = {}		
		$scope.view_regions = function(){
			RegionsServices
			.viewAll			
			.async()
			.success(function(data){				
				$scope.view_regions = data;
			})
		}

		$scope.add_region = {}
		$scope.add_region_single = {}
		$scope.add_single_region = function(){
			RegionsServices
			.addSingle			
			.async($scope.add_region_single.name,$scope.add_region_single.image)
			.success(function(data){				
				$scope.add_region = data;
			})
		}



		$scope.edit_region = {}
		$scope.edit_region_single_view = {}
		$scope.set_single_region_edit = function(){
			RegionsServices
			.getSingle			
			.async($scope.edit_region_single_view.ID)
			.success(function(data){				
				$scope.edit_region_single = data.region;
			})
		}
		$scope.edit_region_single = {}
		$scope.edit_single_region = function(){
			RegionsServices
			.editSingle			
			.async($scope.edit_region_single._id,$scope.edit_region_single.name,$scope.edit_region_single.image)
			.success(function(data){				
				$scope.edit_region = data;
			})
		}


		$scope.remove_region_single_view = {}
		$scope.set_single_region_remove = function(){
			RegionsServices
			.removeSingle			
			.async($scope.remove_region_single_view.ID)
			.success(function(data){				
				$scope.remove_region_single = data;
			})
		}

		$scope.region_massive_fill_result = {}
		$scope.set_region_massive_fill = function(){
			RegionsServices
			.massiveFill			
			.async()
			.success(function(data){				
				$scope.region_massive_fill_result = data;
			})
		}

		$scope.region_massive_clean_result = {}
		$scope.set_region_massive_clean = function(){
			RegionsServices
			.massiveClean			
			.async()
			.success(function(data){				
				$scope.region_massive_clean_result = data;
			})
		}


		/**
		*
		* Types functions
		*
		*/

		$scope.view_type = {}
		$scope.view_type_single = {}
		$scope.view_single_type = function(){
			TypesServices
			.getSingle			
			.async($scope.view_type_single.ID)
			.success(function(data){				
				$scope.view_type = data;
			})
		}

		$scope.view_types = {}		
		$scope.view_types = function(){
			TypesServices
			.viewAll			
			.async()
			.success(function(data){				
				$scope.view_types = data;
			})
		}

		$scope.add_type = {}
		$scope.add_type_single = {}
		$scope.add_single_type = function(){
			TypesServices
			.addSingle			
			.async($scope.add_type_single.name,$scope.add_type_single.image)
			.success(function(data){				
				$scope.add_type = data;
			})
		}



		$scope.edit_type = {}
		$scope.edit_type_single_view = {}
		$scope.set_single_type_edit = function(){
			TypesServices
			.getSingle			
			.async($scope.edit_type_single_view.ID)
			.success(function(data){				
				$scope.edit_type_single = data.type;
			})
		}
		$scope.edit_type_single = {}
		$scope.edit_single_type = function(){
			TypesServices
			.editSingle			
			.async($scope.edit_type_single._id,$scope.edit_type_single.name,$scope.edit_type_single.image)
			.success(function(data){				
				$scope.edit_type = data;
			})
		}


		$scope.remove_type_single_view = {}
		$scope.set_single_type_remove = function(){
			TypesServices
			.removeSingle			
			.async($scope.remove_type_single_view.ID)
			.success(function(data){				
				$scope.remove_type_single = data;
			})
		}

		$scope.type_massive_fill_result = {}
		$scope.set_type_massive_fill = function(){
			TypesServices
			.massiveFill			
			.async()
			.success(function(data){				
				$scope.type_massive_fill_result = data;
			})
		}

		$scope.type_massive_clean_result = {}
		$scope.set_type_massive_clean = function(){
			TypesServices
			.massiveClean			
			.async()
			.success(function(data){				
				$scope.type_massive_clean_result = data;
			})
		}


		/**
		*
		* Tipo Locale functions
		*
		*/

		$scope.view_tipoLocale = {}
		$scope.view_tipoLocale_single = {}
		$scope.view_single_tipoLocale = function(){
			TipoLocaleServices
			.getSingle			
			.async($scope.view_tipoLocale_single.ID)
			.success(function(data){				
				$scope.view_tipoLocale = data;
			})
		}

		$scope.view_tipoLocales = {}		
		$scope.view_tipoLocales = function(){
			TipoLocaleServices
			.viewAll			
			.async()
			.success(function(data){				
				$scope.view_tipoLocales = data;
			})
		}

		$scope.add_tipoLocale = {}
		$scope.add_tipoLocale_single = {}
		$scope.add_single_tipoLocale = function(){
			TipoLocaleServices
			.addSingle			
			.async($scope.add_tipoLocale_single.name,$scope.add_tipoLocale_single.image)
			.success(function(data){				
				$scope.add_tipoLocale = data;
			})
		}



		$scope.edit_tipoLocale = {}
		$scope.edit_tipoLocale_single_view = {}
		$scope.set_single_tipoLocale_edit = function(){
			TipoLocaleServices
			.getSingle			
			.async($scope.edit_tipoLocale_single_view.ID)
			.success(function(data){				
				$scope.edit_tipoLocale_single = data.type;
			})
		}
		$scope.edit_tipoLocale_single = {}
		$scope.edit_single_tipoLocale = function(){
			TipoLocaleServices
			.editSingle			
			.async($scope.edit_tipoLocale_single._id,$scope.edit_tipoLocale_single.name,$scope.edit_tipoLocale_single.image)
			.success(function(data){				
				$scope.edit_tipoLocale = data;
			})
		}


		$scope.remove_tipoLocale_single_view = {}
		$scope.set_single_tipoLocale_remove = function(){
			TipoLocaleServices
			.removeSingle			
			.async($scope.remove_tipoLocale_single_view.ID)
			.success(function(data){				
				$scope.remove_tipoLocale_single = data;
			})
		}


		$scope.tipoLocale_massive_fill_result = {}
		$scope.set_tipoLocale_massive_fill = function(){
			TipoLocaleServices
			.massiveFill			
			.async()
			.success(function(data){				
				$scope.tipoLocale_massive_fill_result = data;
			})
		}

		$scope.tipoLocale_massive_clean_result = {}
		$scope.set_tipoLocale_massive_clean = function(){
			TipoLocaleServices
			.massiveClean			
			.async()
			.success(function(data){				
				$scope.tipoLocale_massive_clean_result = data;
			})
		}



		/**
		*
		* Locality functions
		*
		*/

		$scope.view_locality = {}
		$scope.view_locality_single = {}
		$scope.view_single_locality = function(){
			LocalityServices
			.getSingle			
			.async($scope.view_locality_single.ID)
			.success(function(data){				
				$scope.view_locality = data;
			})
		}

		$scope.view_localitys = {}		
		$scope.view_localitys = function(){
			LocalityServices
			.viewAll			
			.async()
			.success(function(data){				
				$scope.view_localitys = data;
			})
		}

		$scope.add_locality = {}
		$scope.add_locality_single = {}
		$scope.add_single_locality = function(){
			LocalityServices
			.addSingle			
			.async($scope.add_locality_single.name)
			.success(function(data){				
				$scope.add_locality = data;
			})
		}



		$scope.edit_locality = {}
		$scope.edit_locality_single_view = {}
		$scope.set_single_locality_edit = function(){
			LocalityServices
			.getSingle			
			.async($scope.edit_locality_single_view.ID)
			.success(function(data){				
				$scope.edit_locality_single = data.location;
			})
		}
		$scope.edit_locality_single = {}
		$scope.edit_single_locality = function(){
			LocalityServices
			.editSingle			
			.async($scope.edit_locality_single._id,$scope.edit_locality_single.name)
			.success(function(data){				
				$scope.edit_locality = data;
			})
		}


		$scope.remove_locality_single_view = {}
		$scope.set_single_locality_remove = function(){
			LocalityServices
			.removeSingle			
			.async($scope.remove_locality_single_view.ID)
			.success(function(data){				
				$scope.remove_locality_single = data;
			})
		}

		$scope.locality_massive_fill_result = {}
		$scope.set_locality_massive_fill = function(){
			LocalityServices
			.massiveFill			
			.async()
			.success(function(data){				
				$scope.locality_massive_fill_result = data;
			})
		}

		$scope.locality_massive_clean_result = {}
		$scope.set_locality_massive_clean = function(){
			LocalityServices
			.massiveClean			
			.async()
			.success(function(data){				
				$scope.locality_massive_clean_result = data;
			})
		}

		/**
		*
		* Credit Cards functions
		*
		*/

		$scope.view_creditcard = {}
		$scope.view_creditcard_single = {}
		$scope.view_single_creditcard = function(){
			CreditCardServices
			.getSingle			
			.async($scope.view_creditcard_single.ID)
			.success(function(data){				
				$scope.view_creditcard = data;
			})
		}

		$scope.view_creditcards = {}		
		$scope.view_creditcards = function(){
			CreditCardServices
			.viewAll			
			.async()
			.success(function(data){				
				$scope.view_creditcards = data;
			})
		}

		$scope.add_creditcard = {}
		$scope.add_creditcard_single = {}
		$scope.add_single_creditcard = function(){
			CreditCardServices
			.addSingle			
			.async($scope.add_creditcard_single.name,$scope.add_creditcard_single.image)
			.success(function(data){				
				$scope.add_creditcard = data;
			})
		}



		$scope.edit_creditcard = {}
		$scope.edit_creditcard_single_view = {}
		$scope.set_single_creditcard_edit = function(){
			CreditCardServices
			.getSingle			
			.async($scope.edit_creditcard_single_view.ID)
			.success(function(data){				
				$scope.edit_creditcard_single = data.card;
			})
		}
		$scope.edit_creditcard_single = {}
		$scope.edit_single_creditcard = function(){
			CreditCardServices
			.editSingle			
			.async($scope.edit_creditcard_single._id,$scope.edit_creditcard_single.name,$scope.edit_creditcard_single.image)
			.success(function(data){				
				$scope.edit_creditcard = data;
			})
		}


		$scope.remove_creditcard_single_view = {}
		$scope.set_single_creditcard_remove = function(){
			CreditCardServices
			.removeSingle			
			.async($scope.remove_creditcard_single_view.ID)
			.success(function(data){				
				$scope.remove_creditcard_single = data;
			})
		}

		$scope.creditcard_massive_fill_result = {}
		$scope.set_creditcard_massive_fill = function(){
			CreditCardServices
			.massiveFill			
			.async()
			.success(function(data){				
				$scope.creditcard_massive_fill_result = data;
			})
		}

		$scope.creditcard_massive_clean_result = {}
		$scope.set_creditcard_massive_clean = function(){
			CreditCardServices
			.massiveClean			
			.async()
			.success(function(data){				
				$scope.creditcard_massive_clean_result = data;
			})
		}


		/*

		Fill cards

		*/
		$scope.massive_fill_result = {}
		$scope.massive_fill_creditcard = function(){
			FillingServices
			.fillCards			
			.async()
			.success(function(data){				
				$scope.massive_fill_result = data;
			})
		}
		$scope.massive_replace_result = {}
		$scope.massive_replace_creditcard = function(){
			FillingServices
			.replaceCards			
			.async()
			.success(function(data){				
				$scope.massive_replace_result = data;
			})
		}


		/**
		*
		* Restaurant functions
		*
		*/
		
		$scope.view_restaurants_result = {}		
		$scope.view_restaurants = function(){

			RestaurantsServices
			.viewAll			
			.async()
			.success(function(data){				
				$scope.view_restaurants_result = data;
			})
		}
		$scope.clear_view_restaurants = function(){
			$scope.view_restaurants_result = {};
		}

		$scope.view_restaurants_result_filtered = {}		
		$scope.view_restaurants_filtered = function(){

			RestaurantsServices
			.viewAllFiltered			
			.async()
			.success(function(data){				
				$scope.view_restaurants_result_filtered = data;
			})
		}
		$scope.clear_view_restaurants_filtered = function(){
			$scope.view_restaurants_result_filtered = {};
		}

		/**
		* Only Client Restaurant
		*/
		$scope.view_restaurants_result_clients = {}		
		$scope.view_restaurants_clients = function(){

			RestaurantsServices
			.viewAllClients			
			.async()
			.success(function(data){				
				$scope.view_restaurants_result_clients = data;
			})
		}
		$scope.clear_view_restaurants_clients = function(){
			$scope.view_restaurants_result_clients = {};
		}


		/**
		* Only Client Restaurant Filtered
		*/
		$scope.view_restaurants_result_filtered_clients = {}		
		$scope.view_restaurants_filtered_clients = function(){

			RestaurantsServices
			.viewAllFilteredClients			
			.async()
			.success(function(data){				
				$scope.view_restaurants_result_filtered_clients = data;
			})
		}
		$scope.clear_view_restaurants_filtered_clients = function(){
			$scope.view_restaurants_result_filtered_clients = {};
		}



		/**
		* Only Client Restaurant
		*/
		$scope.view_restaurants_result_mine = {}		
		$scope.view_restaurants_mine = function(){

			RestaurantsServices
			.viewAllMine			
			.async()
			.success(function(data){				
				$scope.view_restaurants_result_mine = data;
			})
		}
		$scope.clear_view_restaurants_mine = function(){
			$scope.view_restaurants_result_mine = {};
		}


		/**
		* Only Client Restaurant Filtered
		*/
		$scope.view_restaurants_result_filtered_mine = {}		
		$scope.view_restaurants_filtered_mine = function(){

			RestaurantsServices
			.viewAllFilteredMine			
			.async()
			.success(function(data){				
				$scope.view_restaurants_result_filtered_mine = data;
			})
		}
		$scope.clear_view_restaurants_filtered_mine = function(){
			$scope.view_restaurants_result_filtered_mine = {};
		}


		// id per ristorante singolo "Lo Stil Novo"
		// 56369562bffebc7d078b45ac

		/**
		* View Single Restaurant
		*/
		$scope.view_single_restaurant_obj = {}		
		$scope.view_single_restaurant = function(){

			RestaurantsServices
			.viewSingle			
			.async($scope.view_single_restaurant_obj.ID)
			.success(function(data){				
				$scope.view_single_restaurant_result = data;
			})
		}
		$scope.clear_view_single_restaurant = function(){
			$scope.view_single_restaurant_result = {};
		}


		/**
		* Add Single Restaurant
		*/
		$scope.add_restaurant = {
			types: [
			'Ristorante','Pizzeria','Bar'
			],
			social_accounts: [
			'Facebook','Linkedin','Tripadvisor'
			]
		}
		// richiamo regioni
		$scope.add_restaurant.avaible_regions = {}
		RegionsServices
		.viewAll			
		.async()
		.success(function(data){				
			$scope.add_restaurant.avaible_regions = data.regions;
		})
		// richiamo tipologie cucine
		$scope.add_restaurant.avaible_typologies = {}
		TypesServices
		.viewAll			
		.async()
		.success(function(data){				
			$scope.add_restaurant.avaible_typologies = data.types;
		})
		// richiamo servizi
		$scope.add_restaurant.avaible_services = {}
		ServicesServices
		.viewAll			
		.async()
		.success(function(data){				
			$scope.add_restaurant.avaible_services = data.services;
		})
		$scope.add_restaurant.avaible_cards = {}
		CreditCardServices
		.viewAll			
		.async()
		.success(function(data){				
			$scope.add_restaurant.avaible_cards = data.cards;
		})


		$scope.add_single_restaurant_obj = {
			phone: [],
			staff: {},
			social: {},
			services: [],
			tipoCucina: [],
			regioneCucina: [],
			carte: []
		}		
		$scope.add_single_restaurant = function(){

			RestaurantsServices
			.addSingle			
			.async($scope.add_single_restaurant_obj)
			.success(function(data){				
				$scope.add_single_restaurant_result = data;
			})
		}
		$scope.clear_add_single_restaurant = function(){
			$scope.add_single_restaurant_result = {};
		}

		$scope.add_single_restaurant_phone = function(value){
			$scope.add_single_restaurant_obj.phone.push(value);
		}

		$scope.add_staff_member_to_restaurant = function(role,name){
			if(!$scope.add_single_restaurant_obj.staff[role]){
				$scope.add_single_restaurant_obj.staff[role] = [];
			}
			$scope.add_single_restaurant_obj.staff[role].push(name);
		}

		$scope.add_social_to_restaurant = function(account,link){
			$scope.add_single_restaurant_obj.social[account] = link;
		}

		// aggiungi servizio
		$scope.add_services_to_restaurant = function(value){
			if(value==null) return;
			var index = $scope.add_single_restaurant_obj.services.indexOf(value)
			if(index == -1){
				$scope.add_single_restaurant_obj.services.push(value);
			} else {
				$scope.add_single_restaurant_obj.services.splice(index,1);
			}
		}
		// aggiungi regione cucina
		$scope.add_regions_to_restaurant = function(value){
			if(value==null) return;
			var index = $scope.add_single_restaurant_obj.regioneCucina.indexOf(value)
			if(index == -1){
				$scope.add_single_restaurant_obj.regioneCucina.push(value);
			} else {
				$scope.add_single_restaurant_obj.regioneCucina.splice(index,1);
			}
		}
		// aggiungi tipologia cucina
		$scope.add_typologies_to_restaurant = function(value){
			if(value==null) return;
			var index = $scope.add_single_restaurant_obj.tipoCucina.indexOf(value)
			if(index == -1){
				$scope.add_single_restaurant_obj.tipoCucina.push(value);
			} else {
				$scope.add_single_restaurant_obj.tipoCucina.splice(index,1);
			}
		}
		// aggiungi carta di credito
		$scope.add_cards_to_restaurant = function(value){
			if(value==null) return;
			var index = $scope.add_single_restaurant_obj.carte.indexOf(value)
			if(index == -1){
				$scope.add_single_restaurant_obj.carte.push(value);
			} else {
				$scope.add_single_restaurant_obj.carte.splice(index,1);
			}
		}



		/**
		* Edit Single Restaurant
		*/
		$scope.edit_single_restaurant_obj = {}		
		$scope.edit_single_restaurant = function(){

			RestaurantsServices
			.editSingle			
			.async($scope.edit_single_restaurant_obj.ID,$scope.add_single_restaurant_obj)
			.success(function(data){				
				$scope.view_edit_single_restaurant_result = data;
			})
		}


		/**
		* Remove Single Restaurant
		*/
		$scope.remove_single_restaurant_obj = {}		
		$scope.remove_single_restaurant = function(){

			RestaurantsServices
			.removeSingle			
			.async($scope.remove_single_restaurant_obj.ID)
			.success(function(data){				
				$scope.view_remove_single_restaurant_result = data;
			})
		}

		/**
		* Change prenotazione
		*/
		$scope.change_prenotazione = {}		
		$scope.change_prenotazione_submit = function(){

			RestaurantsServices
			.changePrenotazione			
			.async($scope.change_prenotazione.ID)
			.success(function(data){				
				$scope.change_prenotazione_result = data;
			})
		}

		/**
		* Change ordinazione
		*/
		$scope.change_ordinazione = {}		
		$scope.change_ordinazione_submit = function(){

			RestaurantsServices
			.changeOrdinazione		
			.async($scope.change_ordinazione.ID,$scope.change_ordinazione.range)
			.success(function(data){				
				$scope.change_ordinazione_result = data;
			})
		}


		/**
		* Change orari
		*/
		$scope.change_orari = {
			ID: null,
			orari: {
				0:[],1:[],2:[],3:[],4:[],5:[],6:[]
			}
		}		
		$scope.change_orari_submit = function(){

			RestaurantsServices
			.changeOrari		
			.async($scope.change_orari.ID,$scope.change_orari.orari)
			.success(function(data){				
				$scope.change_orari_result = data;
			})
		}

		$scope.add_fascia_oraria = function(fascia,index){
			var i = $scope.change_orari.orari[index].indexOf(fascia);
			if(i == -1){
				$scope.change_orari.orari[index].push(fascia);
			} else {
				$scope.change_orari.orari[index].splice(i,1);
			}			
		}
		




		/**
		*
		* Images functions
		*
		*/

		$scope.view_image = {}
		$scope.view_image_single = {}
		$scope.view_single_image = function(){
			ImagesServices
			.getSingle			
			.async($scope.view_image_single.ID)
			.success(function(data){				
				$scope.view_image = data;
			})
		}

		$scope.view_images = {}		
		$scope.view_images = function(){
			ImagesServices
			.viewAll			
			.async()
			.success(function(data){				
				$scope.view_images = data;
			})
		}

		$scope.add_image = {}
		$scope.add_image_single = {}
		$scope.add_single_image = function(){
			ImagesServices
			.addSingle			
			.async($scope.add_image_single.type,$scope.add_image_single.alt,$scope.add_image_single.title,$scope.add_image_single.image)
			.success(function(data){				
				$scope.add_image = data;
			})
		}
		


		$scope.remove_image_single_view = {}
		$scope.set_single_image_remove = function(){
			ImagesServices
			.removeSingle			
			.async($scope.remove_image_single_view.ID)
			.success(function(data){				
				$scope.remove_image_single = data;
			})
		}








		/**
		*
		*  Blog
		*
		**/
		
		/**
		*
		* Categories functions
		*
		*/

		$scope.view_blog_category = {}
		$scope.view_blog_category_single = {}
		$scope.view_single_blog_category = function(){
			BlogCategoryServices
			.getSingle			
			.async($scope.view_blog_category_single.ID)
			.success(function(data){				
				$scope.view_blog_category = data;
			})
		}

		$scope.view_blog_categories = {}		
		$scope.view_blog_categories = function(){
			BlogCategoryServices
			.viewAll			
			.async()
			.success(function(data){				
				$scope.view_blog_categories = data;
			})
		}

		$scope.add_blog_category = {}
		$scope.add_blog_category_single = {}
		$scope.add_single_blog_category = function(){
			BlogCategoryServices
			.addSingle			
			.async($scope.add_blog_category_single.name)
			.success(function(data){				
				$scope.add_blog_category = data;
			})
		}



		$scope.edit_blog_category = {}
		$scope.edit_blog_category_single_view = {}
		$scope.set_single_blog_category_edit = function(){
			BlogCategoryServices
			.getSingle			
			.async($scope.edit_blog_category_single_view.ID)
			.success(function(data){				
				$scope.edit_blog_category_single = data.category;
			})
		}
		$scope.edit_blog_category_single = {}
		$scope.edit_single_blog_category = function(){
			BlogCategoryServices
			.editSingle			
			.async($scope.edit_blog_category_single._id,$scope.edit_blog_category_single.name)
			.success(function(data){				
				$scope.edit_blog_category = data;
			})
		}


		$scope.remove_blog_category_single_view = {}
		$scope.set_single_blog_category_remove = function(){
			BlogCategoryServices
			.removeSingle			
			.async($scope.remove_blog_category_single_view.ID)
			.success(function(data){				
				$scope.remove_blog_category_single = data;
			})
		}



		/**
		*
		* Articles functions
		*
		*/

		$scope.view_blog_article = {}
		$scope.view_blog_article_single = {}
		$scope.view_single_blog_article = function(){
			BlogArticlesServices
			.getSingle			
			.async($scope.view_blog_article_single.ID)
			.success(function(data){				
				$scope.view_blog_article = data;
			})
		}

		$scope.view_blog_articles = {}		
		$scope.view_blog_articles = function(){
			BlogArticlesServices
			.viewAll			
			.async()
			.success(function(data){				
				$scope.view_blog_articles = data;
			})
		}

		$scope.add_blog_article = {}
		$scope.add_blog_article_single = {
			name: "",
			content: [],
			meta_desc: "",
			category: [],
			tag: [],
			image: "",
		}

		BlogCategoryServices
		.viewAll			
		.async()
		.success(function(data){				
			$scope.blog_categories = data;
		})

		// gestione categorie
		$scope.add_remove_blog_category = function(id){
			var ind = $scope.add_blog_article_single.category.indexOf(id);
			if(ind == -1){
				$scope.add_blog_article_single.category.push(id);
			} else {
				$scope.add_blog_article_single.category.splice(ind,1);
			}
		}

		// gestione tag
		$scope.add_blog_tag = function(tag){
			$scope.add_blog_article_single.tag.push(tag);
		}

		// Gestione contenuti
		$scope.temporary_blog_content = {
			type: 0,
			string: "",
			image: "",
			gallery: []
		}
		$scope.add_blog_content_image_to_gallery = function(){
			$scope.temporary_blog_content.gallery.push($scope.temporary_blog_content.image_gallery);
		}
		$scope.add_blog_content = function(){
			$scope.add_blog_article_single.content.push($scope.temporary_blog_content);
			$scope.temporary_blog_content = {
				string: "",
				image: "",
				gallery: []
			} 
		}



		$scope.add_single_blog_article = function(){
			BlogArticlesServices
			.addSingle			
			.async($scope.add_blog_article_single.name,$scope.add_blog_article_single.content,$scope.add_blog_article_single.tag,$scope.add_blog_article_single.meta_desc,$scope.add_blog_article_single.image,$scope.add_blog_article_single.category)
			.success(function(data){				
				$scope.add_blog_article = data;
			})
		}

				
		$scope.edit_blog_article_single = {}
		$scope.edit_single_blog_article = function(){
			BlogArticlesServices
			.editSingle			
			.async($scope.edit_blog_article_single._id,$scope.add_blog_article_single.name,$scope.add_blog_article_single.content,$scope.add_blog_article_single.tag,$scope.add_blog_article_single.meta_desc,$scope.add_blog_article_single.image,$scope.add_blog_article_single.category)
			.success(function(data){				
				$scope.edit_blog_article = data;
			})
		}


		$scope.remove_blog_article_single_view = {}
		$scope.set_single_blog_article_remove = function(){
			BlogArticlesServices
			.removeSingle			
			.async($scope.remove_blog_article_single_view.ID)
			.success(function(data){				
				$scope.remove_blog_article_single = data;
			})
		}



		/**
		* Administration functions
		*/

		$scope.admin_import_restaurant_result = {}
		$scope.admin_import_restaurant = function(){
			AdministrationServices
			.importRestaurants			
			.async()
			.success(function(data){				
				$scope.admin_import_restaurant_result = data;
			})
		}

		// città home
		$scope.home_city_services_result = {}
		$scope.add_single_home_city_model = {}
		$scope.delete_single_home_city_model = {}
		$scope.view_home_city_function = function(){
			SettingsServices
			.viewHomeCity			
			.async()
			.success(function(data){				
				$scope.home_city_services_result = data;
			})
		}
		$scope.add_single_home_city_function = function(){
			SettingsServices
			.addSingleHomeCity			
			.async($scope.add_single_home_city_model.name,$scope.add_single_home_city_model.lat,$scope.add_single_home_city_model.lng,$scope.add_single_home_city_model.image,$scope.add_single_home_city_model.order)
			.success(function(data){				
				$scope.home_city_services_result = data;
			})
		}
		$scope.delete_single_home_city_function = function(){
			SettingsServices
			.deleteSingleHomeCity			
			.async($scope.delete_single_home_city_model.ID)
			.success(function(data){				
				$scope.home_city_services_result = data;
			})
		}

		// Pacchetti
		$scope.pacchetti_services_result = {}
		$scope.add_single_pacchetti_model = {
			services: []
		}
		$scope.delete_single_pacchetti_model = {}
		$scope.add_single_pacchetti_model_services = {};
		$scope.view_pacchetti_function = function(){
			SettingsServices
			.viewPacchetti			
			.async()
			.success(function(data){				
				$scope.pacchetti_services_result = data;
			})
		}
		$scope.add_single_pacchetti_function = function(){
			SettingsServices
			.addSinglePacchetto			
			.async($scope.add_single_pacchetti_model.name,$scope.add_single_pacchetti_model.featured,$scope.add_single_pacchetti_model.services,$scope.add_single_pacchetti_model.price_month,$scope.add_single_pacchetti_model.price_annual,$scope.add_single_pacchetti_model.period)
			.success(function(data){				
				$scope.pacchetti_services_result = data;
			})
		}
		$scope.delete_single_pacchetti_function = function(){
			SettingsServices
			.deleteSinglePacchetto			
			.async($scope.delete_single_pacchetti_model.ID)
			.success(function(data){				
				$scope.pacchetti_services_result = data;
			})
		}
		$scope.add_single_pacchetti_services_function = function(){
			if($scope.add_single_pacchetti_model_services.txt && $scope.add_single_pacchetti_model_services.txt != ""){
				$scope.add_single_pacchetti_model.services.push($scope.add_single_pacchetti_model_services.txt);
				$scope.add_single_pacchetti_model_services.txt = "";
			}
		}

		$scope.massive_import_images_result = {}
		$scope.admin_massive_import_images = function(){

			$scope.massive_import_images_result = {}
			
			var add_import = function(){
				AdministrationServices
				.importImages			
				.async()
				.success(function(data){				
					$scope.massive_import_images_result = data;
					if(data.n != 0){
						$timeout(add_import, 1000);
					}
				})
			}
			add_import()
				
		}

		$scope.admin_massive_import_images_remaining = function(){

			$scope.massive_import_images_result = {}
			
			var add_import_remaining = function(){
				AdministrationServices
				.importRemaining			
				.async()
				.success(function(data){				
					$scope.massive_import_images_result = data;
					if(data.n != 0){
						$timeout(add_import_remaining, 1000);
					}
				})
			}
			add_import_remaining()
				
		}



		/**
		* Notifications
		*
		**/

		$scope.notification_services_admin_result = {}
		$scope.notification_admin_view_all = function(){
			NotificationsServices
			.viewAll			
			.async()
			.success(function(data){				
				$scope.notification_services_admin_result = data;
			})
		}
		$scope.notification_admin_delete_all = function(){
			NotificationsServices
			.deleteOld			
			.async()
			.success(function(data){				
				$scope.notification_services_admin_result = data;
			})
		}
		$scope.notification_view_mine_to_read = function(){
			NotificationsServices
			.viewToRead			
			.async()
			.success(function(data){				
				$scope.notification_services_admin_result = data;
			})
		}

		$scope.notification_services_user_result = {}
		$scope.notification_user_set_read_form = {}
		$scope.notification_user_view_mine_form = {}
		$scope.notification_user_set_all_read_form = {}
		$scope.notification_user_view_mine = function(){
			NotificationsServices
			.viewMine		
			.async($scope.notification_user_view_mine_form.ID)
			.success(function(data){				
				$scope.notification_services_user_result = data;
			})
		}

		$scope.notification_user_set_read = function(){
			NotificationsServices
			.setRead		
			.async($scope.notification_user_set_read_form.ID,$scope.notification_user_set_read_form.id_notifica)
			.success(function(data){				
				$scope.notification_services_user_result = data;
			})
		}

		$scope.notification_user_set_all_read = function(){
			NotificationsServices
			.setAllRead		
			.async($scope.notification_user_set_all_read_form.ID)
			.success(function(data){				
				$scope.notification_services_user_result = data;
			})
		}



		/**
		* Community functions
		*/

		$scope.community_all_form = {}
		$scope.community_user_form = {
			tags: []
		}
		$scope.community_user_form_add_tag = {}
		$scope.community_admin_form = {}

		$scope.community_all_form_result = {}
		$scope.community_user_form_result = {}
		$scope.community_admin_form_result = {}

		$scope.community_view_by_restaurant = function(){
			CommunityServices
			.viewByRestaurant	
			.async($scope.community_all_form.risto_ID)
			.success(function(data){				
				$scope.community_all_form_result = data;
			})
		}
		$scope.community_view_by_user = function(){
			CommunityServices
			.viewByUser	
			.async($scope.community_all_form.user_ID)
			.success(function(data){				
				$scope.community_all_form_result = data;
			})
		}

		$scope.community_follow_user = function(){
			CommunityServices
			.followUser	
			.async($scope.community_user_form.user_ID)
			.success(function(data){				
				$scope.community_user_form_result = data;
			})
		}
		$scope.community_unfollow_user = function(){
			CommunityServices
			.unFollowUser	
			.async($scope.community_user_form.user_ID)
			.success(function(data){				
				$scope.community_user_form_result = data;
			})
		}


		$scope.community_add_recensione_user = function(){
			CommunityServices
			.addRecensione	
			.async($scope.community_user_form.name,$scope.community_user_form.text,$scope.community_user_form.tags,$scope.community_user_form.restaurant,$scope.community_user_form.image)
			.success(function(data){				
				$scope.community_user_form_result = data;
			})
		}
		$scope.community_add_recensione_add_tag_user = function(){
			if($scope.community_user_form_add_tag.txt){
				$scope.community_user_form.tags.push($scope.community_user_form_add_tag.txt);
			}
		}

		$scope.community_view_to_approve_admin = function(){
			CommunityServices
			.viewToApprove	
			.async()
			.success(function(data){				
				$scope.community_admin_form_result = data;
			})
		}

		$scope.community_approve_admin = function(){
			CommunityServices
			.approveRecensione	
			.async($scope.community_admin_form.recensione_ID)
			.success(function(data){				
				$scope.community_admin_form_result = data;
			})
		}

		$scope.community_delete_admin = function(){
			CommunityServices
			.deleteRecensione	
			.async($scope.community_admin_form.recensione_ID)
			.success(function(data){				
				$scope.community_admin_form_result = data;
			})
		}


		/***
		*
		*  Prenotazione + management
		*
		**/
		$scope.gestione_prenotazioni_risto = {}
		$scope.add_struttura = {
			sale: []
		}
		$scope.add_struttura_children = {}
		$scope.add_sala = function(){
			if($scope.add_struttura_children.sala && $scope.add_struttura_children.sala != ''){
				var to_add = {
					name: $scope.add_struttura_children.sala,
					tables: []
				}
				$scope.add_struttura.sale.push(to_add);
			}			
		}
		$scope.add_tavolo = function(indice_sala){
			var tavolo = {
				num: $scope.add_struttura_children.tavolo,
				posti: $scope.add_struttura_children.posti
			}
			$scope.add_struttura.sale[indice_sala].tables.push(tavolo);
		}

		$scope.addStrutturaFunction = function(){
			RestaurantsServices
			.addStruttura
			.async($scope.gestione_prenotazioni_risto.restaurant_id,$scope.add_struttura.time_shift,$scope.add_struttura.sale)
			.success(function(data){				
				$scope.add_struttura_result = data;
			})
		}

		$scope.reserve_form = {}
		$scope.reservation_form_result = {}
		$scope.askReserveFunction = function(){
			ReservationServices
			.askReserve
			.async($scope.reserve_form.restaurant_id,$scope.reserve_form.date,$scope.reserve_form.time,$scope.reserve_form.email,$scope.reserve_form.people,$scope.reserve_form.name,$scope.reserve_form.tel)
			.success(function(data){				
				$scope.add_struttura_result = data;
			})
		}



		$scope.reservation_admin_form = {}
		$scope.viewAllReservation = function(){
			ReservationServices
			.viewAll
			.async()
			.success(function(data){				
				$scope.reservation_admin_form_result = data;
			})
		}

		$scope.deleteReservation = function(){
			ReservationServices
			.deleteReservation
			.async($scope.reservation_admin_form.restaurant_id,$scope.reservation_admin_form.reservation)
			.success(function(data){				
				$scope.reservation_admin_form_result = data;
			})
		}

		$scope.viewByDateReservation = function(){
			ReservationServices
			.viewByDate
			.async($scope.reservation_admin_form.restaurant_id,$scope.reservation_admin_form.date)
			.success(function(data){				
				$scope.reservation_admin_form_result = data;
			})
		}

		$scope.reservations_other_result = {}
		$scope.confirmReservation = function(){
			ReservationServices
			.confirmReservation
			.async($scope.reservation_admin_form.restaurant_id,$scope.reservation_admin_form.reservation,$scope.reservation_admin_form.sala,$scope.reservation_admin_form.tavolo)
			.success(function(data){				
				$scope.reservations_other_result = data;
			})
		}

		$scope.changeReservation = function(){
			ReservationServices
			.changeDate
			.async($scope.reservation_admin_form.restaurant_id,$scope.reservation_admin_form.reservation,$scope.reservation_admin_form.date,$scope.reservation_admin_form.time,$scope.reservation_admin_form.sala,$scope.reservation_admin_form.tavolo)
			.success(function(data){				
				$scope.reservations_other_result = data;
			})
		}

		$scope.approveChangeReservation = function(){
			ReservationServices
			.approveChangeDate
			.async($scope.reservation_admin_form.restaurant_id,$scope.reservation_admin_form.reservation,$scope.reservation_admin_form.string)
			.success(function(data){				
				$scope.reservations_other_result = data;
			})
		}

		/***

			Menu

		**/
		$scope.add_menu_result = {}
		$scope.gestione_delivery_risto = {}
		$scope.add_menu_children = {
			fasce: []
		}
		$scope.add_menu_fasce = {}
		$scope.add_menu_children_categoria = {
		}
		$scope.add_menu = {
			menus: []
		}
		$scope.add_menu_children.ingredienti = {}
		$scope.ingredienti_da_aggiungere = []
		$scope.add_menu_menu_fascia = function(){
			$scope.add_menu_children.fasce.push($scope.add_menu_fasce.fascia)
			$scope.add_menu_fasce.fascia = {}
		}
		$scope.add_menu_menu = function(){
			var obj = {
				name: $scope.add_menu_children.name,
				fasce: $scope.add_menu_children.fasce,
				categorie: []
			}
			$scope.add_menu.menus.push(obj)
			$scope.add_menu_children = {}
		}

		$scope.add_menu_menu_categoria = function(indice){
			var obj = {
				name: $scope.add_menu_children.categoria,
				piatti: []
			}
			$scope.add_menu.menus[indice].categorie.push(obj)
			$scope.add_menu_children = {}
		}

		$scope.add_menu_menu_categoria_piatto = function(categoria,menu){
			var obj = {
				name: $scope.add_menu_children.piatto,
				image: ($scope.add_menu_children.image) ? $scope.add_menu_children.image : null,
				ingredienti: $scope.ingredienti_da_aggiungere,
				price: $scope.add_menu_children.price_one+"."+$scope.add_menu_children.price_two
			}
			$scope.add_menu.menus[menu].categorie[categoria].piatti.push(obj)

			$scope.ingredienti_da_aggiungere = []
			$scope.add_menu_children = {}
		}

		$scope.add_ingredienti = function(){
			if($scope.add_menu_children.ingrediente != ""){
				$scope.ingredienti_da_aggiungere.push($scope.add_menu_children.ingrediente);
				$scope.add_menu_children.ingrediente = ""
			}				
		}

		$scope.addMenuFunction = function(){
			RestaurantsServices
			.addMenu
			.async($scope.gestione_delivery_risto.restaurant_id,$scope.add_menu)
			.success(function(data){				
				$scope.add_menu_result = data;
			})
		}


		$scope.plates_form = {}
		$scope.viewAllPlates = function(){
			PlatesServices
			.viewAll
			.async()
			.success(function(data){				
				$scope.add_plates_result = data;
			})
		}
		$scope.viewPlatesByRestaurant = function(){
			PlatesServices
			.viewByRestaurant
			.async($scope.plates_form.restaurant_id)
			.success(function(data){				
				$scope.add_plates_result = data;
			})
		}
		$scope.deletePlate = function(){
			PlatesServices
			.deletePlate
			.async($scope.plates_form.restaurant_id,$scope.plates_form.ID)
			.success(function(data){				
				$scope.add_plates_result = data;
			})
		}


		$scope.orders_form_result = {}
		$scope.user_order_form = {
			plates: []
		}
		$scope.single_plate = {
			id: "", 
			qty: 1
		}
		$scope.add_plates_to_order = function(){
			if($scope.single_plate != ""){
				$scope.user_order_form.plates.push($scope.single_plate)
			} 
			$scope.single_plate = {
				id: "", 
				qty: 1
			}
		}
		$scope.send_order = function(){
			OrdersServices
			.addOrder
			.async(
				$scope.user_order_form.restaurant,
				$scope.user_order_form.date,
				$scope.user_order_form.time,
				$scope.user_order_form.name,
				$scope.user_order_form.surname,
				$scope.user_order_form.email,
				$scope.user_order_form.tel,
				$scope.user_order_form.address,
				$scope.user_order_form.plates)
			.success(function(data){				
				$scope.orders_form_result = data;
			})
		}

		/*
		*
		*  Ricerca Search
		*
		*/
		$scope.search_by_string_form = {}
		$scope.search_by_string = function(){
			SearchServices
			.searchByString
			.async($scope.search_by_string_form.string)
			.success(function(data){				
				$scope.search_restaurants_result = data;
			})
		}
		$scope.search_by_lat_lng = function(){
			SearchServices
			.searchByLatLng
			.async($scope.search_by_string_form.lat,$scope.search_by_string_form.lng)
			.success(function(data){				
				$scope.search_restaurants_result = data;
			})
		}
		$scope.search_by_name = function(){
			SearchServices
			.searchByName
			.async($scope.search_by_string_form.name)
			.success(function(data){				
				$scope.search_restaurants_result = data;
			})
		}
		$scope.search_by_slug = function(){
			SearchServices
			.searchBySlug
			.async($scope.search_by_string_form.slug)
			.success(function(data){				
				$scope.search_restaurants_result = data;
			})
		}


		// Claim restaurant
		$scope.claim_result = {}
		$scope.claim_form = {}
		$scope.claim_restaurant = function(){
			RestaurantsServices
			.claim
			.async($scope.claim_form.restaurant)
			.success(function(data){				
				$scope.claim_result = data;
			})
		}
		$scope.approve_claim_restaurant = function(){
			RestaurantsServices
			.approveClaim
			.async($scope.claim_form.restaurant,$scope.claim_form.user)
			.success(function(data){				
				$scope.claim_result = data;
			})
		}
		$scope.get_claim_restaurant = function(){
			RestaurantsServices
			.getClaimed
			.async()
			.success(function(data){				
				$scope.claim_result = data;
			})
		}


		$scope.get_site_info = function(){
			ToolsServices
			.getMainSiteInfo
			.async()
			.success(function(data){				
				$scope.get_tools_result = data;
			})
		}
		
	}]);