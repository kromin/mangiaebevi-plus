var UserReservationsController = angular.module('AppUserReservationsController',[]);
 
UserReservationsController
.controller('UserReservationsCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams, ngDialog, ReservationServices, alertify, ToolsServices){
	"ngInject";

	
	
	$scope.reservations = [];
	$scope.loadReservations = function(){
		ReservationServices.viewMine.async().success(function(data){
			if(data.message == 'ok'){
				$scope.reservations = data.reservations;
				$scope.reservation_restaurants = data.restaurants;
			}			
		});
	};

	$scope.returnReservationRestaurant = function(restaurant){
		var ret = {};
		angular.forEach($scope.reservation_restaurants,function(v,k){				
			if(v._id == restaurant) {
				ret = v;
			}
		});
		return ret;
	};


	$scope.accettaCambioData = function(reservation){
		ReservationServices
		.approveChangeDate
		.async(reservation.restaurant,reservation._id,reservation.unique_string).success(function(data){
			if(data.message == 'ok'){
				angular.forEach($scope.reservations,function(v,k){
					if(v._id == reservation._id){
						$scope.reservations[k] = data.reservation;
					}
				});
			}
		});
	};

	$scope.rifiutaCambioData = function(reservation){
		ReservationServices
		.refuseChangeDate
		.async(reservation.restaurant,reservation._id,reservation.unique_string).success(function(data){
			if(data.message == 'ok'){
				angular.forEach($scope.reservations,function(v,k){
					if(v._id == reservation._id){
						$scope.reservations.splice(k,1);
					}
				});
			}
		});
	};


	$scope.removeReservation = function(id){
	  	alertify.okBtn("Si")
			.cancelBtn("No")
			.confirm("Sicuro di voler annullare questa prenotazione?", function () {	
			ToolsServices.mainOverlay.show();
			ReservationServices
			.deleteReservation  
			.async(id).success(function(data){
				if(data.message == 'ok'){
					angular.forEach($scope.reservations,function(v,k){
						if(v._id == id){
							$scope.reservations[k].confirmed = 4;
						}
					});
				}
				ToolsServices.mainOverlay.hide();
			}).error(function(){
				ToolsServices.mainOverlay.hide();
			});
		    
		}, function() {
		    
		});			
	};


	$scope.canPreOrderNow = function(dt, fascia_oraria) {
		// substringa di 10
		var date = dt.substring(0, 10) + " " + fascia_oraria;
		return (moment(date,"YYYY-MM-DD HH:mm") > moment()) 
	}


	$scope.viewPreOrder = function(reservation) {
		$scope.dialog_order_plates = reservation.preorder;
		$scope.dialog_order_reservation = reservation;
		var orderDialog = ngDialog.open({
            template: '/templates/dialogorder',
            scope: $scope,
            className: 'ngdialog ngdialog-theme-default small' 
        });	
	}

	$scope.payReservation = function(reservation) {
		if(!reservation.preorder || (reservation.preorder.paid && reservation.preorder.paid != 0)) return;
		ToolsServices.mainOverlay.show();
		ReservationServices
		.payPreorder  
		.async(reservation._id).success(function(data){
			if(data.message == 'ok'){
				window.location = data.paypal_link;
			} else {
				swal({
	                buttonsStyling: false,
	                confirmButtonClass: 'btn btn-primary btn-sm ghost',
	                title: 'Oops!',
	                text: 'Qualcosa è andato storto',
	                type: 'error'
	            });
	            ToolsServices.mainOverlay.hide();
			}			
		}).error(function(){
			swal({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                title: 'Oops!',
                text: 'Qualcosa è andato storto',
                type: 'error'
            });
			ToolsServices.mainOverlay.hide();
		});
	}
	
});