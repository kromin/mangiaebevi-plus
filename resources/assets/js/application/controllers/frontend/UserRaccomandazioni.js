var UserRaccomandazioniController = angular.module('AppUserRaccomandazioniController',[]);

UserRaccomandazioniController
.controller('UserRaccomandazioniCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams, UserServices, RecommendationServices, SearchServices){
	"ngInject";

	$scope.user = null;
	$scope.initRaccomandazioni = function(user,itsme){
		$scope.user = user;
		if($scope.current_user){
			if(itsme){
				$scope.itsme = true;

				$scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;	
				$scope.lat = ($scope.current_user.user && $scope.current_user.user.loc.coordinates[1]) ? $scope.current_user.user.loc.coordinates[1] 
				: (($cookies.get('lat')) ? $cookies.get('lat') : 41.8919300);
				$scope.lng = ($scope.current_user.user && $scope.current_user.user.loc.coordinates[0]) ? $scope.current_user.user.loc.coordinates[0] 
				: (($cookies.get('lng')) ? $cookies.get('lng') : 12.5113300);
			}
			$scope.initRecommendations();
		}
	};

	$scope.initRecommendations = function(){
		

		var loadNear = function(){
			$scope.stop_left_pagination = true;
			var left_obj = {
				lat: $scope.lat,
				lng: $scope.lng,
				skip: ($scope.left_pagination == 1) ? 0 : parseInt($scope.left_pagination-1)*parseInt($scope.pagination_number),
				limit: parseInt($scope.pagination_number)
			}
			RecommendationServices.viewNear.async(left_obj).success(function(data){

				pushDatas('community_rec',data.recommendation);
				if(data.recommendation.length < $scope.pagination_number){
					$scope.stop_left_pagination = true;
				} else {
					$scope.stop_left_pagination = false;
					$scope.left_pagination++;
				}
			});
		};
		var loadNearRequest = function(){
			$scope.stop_right_pagination = true;
			var right_obj = {
				lat: $scope.lat,
				lng: $scope.lng,
				skip: ($scope.left_pagination == 1) ? 0 : parseInt($scope.right_pagination-1)*parseInt($scope.pagination_number),
				limit: parseInt($scope.pagination_number)
			}
			RecommendationServices.viewNearRequests.async(right_obj).success(function(data){
				pushDatas('community_req',data.recommendation);
				if(data.recommendation.length < $scope.pagination_number){
					$scope.stop_right_pagination = true;
				} else {
					$scope.stop_right_pagination = false;
					$scope.right_pagination++;
				}
			})
		};


		var loadbyUser = function(){
			$scope.stop_load_by_user_pagination = true;
			var load_by_user_obj = {
				user: $scope.user,
				skip: ($scope.left_pagination == 1) ? 0 : parseInt($scope.load_by_user_pagination-1)*parseInt($scope.pagination_number),
				take: parseInt($scope.pagination_number)
			};
			RecommendationServices.viewByUser.async(load_by_user_obj).success(function(data){
	
				pushDatas('user_rec',data.recommendation);
				if(data.recommendation.length < $scope.pagination_number){
					$scope.stop_load_by_user_pagination = true;
				} else {
					$scope.stop_load_by_user_pagination = false;
					$scope.load_by_user_pagination++;
				}
			});
		};

		var loadbyUserRequest = function(){
			$scope.stop_load_by_user_request_pagination = true;
			var load_by_user_request_obj = {
				user: $scope.user
			};
			RecommendationServices.viewByUserRequests.async(load_by_user_request_obj).success(function(data){
				pushDatas('user_req',data.recommendation);
				if(data.recommendation.length < $scope.pagination_number){
					$scope.stop_load_by_user_request_pagination = true;
				} else {
					$scope.stop_load_by_user_request_pagination = false;
				}
			});
		};


		var pushDatas = function(scope_val,datas){
			angular.forEach(datas,function(v,k){
				$scope[scope_val].push(v);
			});			
		};


		$scope.recommendation_request_ask = {
			address: '',
			text: '',
			type: 0
		};
		$scope.setRecType = function(n){
			$scope.recommendation_request_ask.type = n;
		};
		$scope.not_show_address = 1;
		$scope.setNotShow = function(n){
			$scope.not_show_address = n;
		};
		$scope.askRecommendation = function(){
			if(!$scope.current_user.user.loc.coordinates && address === '') return; 
			RecommendationServices.askRecommendationRequest.async($scope.recommendation_request_ask).success(function(data){
				$scope.recommendation_request_ask = {
					address: '',
					text: '',
					type: 0
				};	
				swal({
						buttonsStyling: false,
						confirmButtonClass: 'btn btn-primary btn-sm ghost',
						title: 'Tutto ok!',
						text: 'Richiesta inviata, aspetta una risposta dalla community!',
						type: 'success'
					});	
			}).error(function(){
				$scope.simpleError();
			});
		};

		$scope.community_rec = [];
		$scope.community_req = [];
		$scope.user_rec = [];
		$scope.user_req = [];

		$scope.left_recommendations = [];
		$scope.right_recommendations = [];

		$scope.stop_left_pagination = false;
		$scope.stop_right_pagination = false;

		$scope.left_pagination = 1;
		$scope.right_pagination = 1;

		$scope.load_by_user_recommendations = [];
		$scope.load_by_user_request_recommendations = [];

		$scope.stop_load_by_user_pagination = false;
		$scope.stop_load_by_user_request_pagination = false;

		$scope.load_by_user_pagination = 1;
		$scope.load_by_user_request_pagination = 1;

				
		
		
		if($scope.itsme && $scope.lat && $scope.lng){
			
			loadNear();
			loadNearRequest();

		}


		loadbyUser();
		loadbyUserRequest();
		
		$scope.recommendation_tab = 0;
		$scope.setRecommendationTab = function(n){
			$scope.recommendation_tab = n;
		};

		$scope.loadRecommendation = function(type){
			switch(type){
				case 'loadLeft':
				loadNear();
				break;
				case 'loadRight':
				loadNearRequest();
				break;
				case 'loadUser':
				loadbyUser();
				break;
				case 'loadUserRequest':
				loadbyUserRequest();
				break;
			}
		};


		// inizio risposta alle raccomandazioni
		$scope.current_answering = {
			scope_obj: null,
			recommendation_index: null,
			recommendation_id: null,
			restaurant: null,
			plate: null
		};
		$scope.answer_step_block = 0;
		$scope.answer_type = 0;
		$scope.answer_restaurants = [];
		$scope.answer_plates = [];

		
		$scope.pushAnswer = function(scope_obj,recommendation_index,recommendation_id){
			
			$scope.resetRecommendationAnswer();
			$scope.current_answering = {
				scope_obj: scope_obj,
				recommendation_index: recommendation_index,
				recommendation_id: recommendation_id,
				restaurant: null,
				plate: null
			};
			$scope.answer_step_block = 1;
			$scope.answer_type = $scope[scope_obj][recommendation_index].type;
		};

		$scope.isAskingRequestFor = function(id){
			return ($scope.current_answering.recommendation_id == id) ? true : false;
		};


		$scope.searchRestaurantForAnswer = function(name){
			
			if(name === '' || !name) return;
			SearchServices.searchByName.async(name).success(function(data){
				if(data.restaurants.length === 0){
					swal({
						buttonsStyling: false,
						confirmButtonClass: 'btn btn-primary btn-sm ghost',
						title: 'Oops!',
						text: 'Non ci sono ristoranti che soddisfino la ricerca',
						type: 'error'
					});
				} else {
					$scope.answer_restaurants = data.restaurants;
				}				
			});
		};


		$scope.selectRestaurant = function(index){
			$scope.current_answering.restaurant = index; // salvo indice del ristorante per chiamate successive
			switch($scope.answer_type){
				case 0:
				// vai alla fine
				$scope.answer_step_block = 3;
				break;
				case 1:
				// scegli il piatto
				$scope.answer_step_block = 2;
				break;
			}
		};


		$scope.searchPlateForAnswer = function(name){
			
			if(name === '' || !name) return;
			SearchServices.searchPlateByName.async(name,$scope.answer_restaurants[$scope.current_answering.restaurant]._id).success(function(data){
				if(data.plates.length === 0){
					swal({
						buttonsStyling: false,
						confirmButtonClass: 'btn btn-primary btn-sm ghost',
						title: 'Oops!',
						text: 'Non ci sono piatti che soddisfino la ricerca',
						type: 'error'
					});
				} else {
					$scope.answer_plates = data.plates;
				}				
			});
		};

		$scope.selectPlate = function(index){
			$scope.current_answering.plate = index; // salvo indice del ristorante per chiamate successive
			$scope.answer_step_block = 3;
		};


		$scope.submitRecommendationAnswer = function(text){
			//sottoscrivi e resetta
			obj = {
				text: text,
				ref: ($scope.answer_type === 0) ? $scope.answer_restaurants[$scope.current_answering.restaurant]._id : $scope.answer_plates[$scope.current_answering.plate]._id,
				request: $scope.current_answering.recommendation_id
			};
			RecommendationServices.answerRecommendationRequest.async(obj).success(function(data){
				if(data.message == 'ok'){
					$scope[$scope.current_answering.scope_obj][$scope.current_answering.recommendation_index].answers_num++;
					$scope.resetRecommendationAnswer();
					$scope.resetAnswersList();
					//$scope.openAnswers($scope.current_answering.scope_obj,$scope.current_answering.recommendation_index,$scope.current_answering.recommendation_id);
				} else {
					$scope.simpleError();
				}
			}).error(function(){
				$scope.simpleError();
			});
		};

		$scope.resetRecommendationAnswer = function(){
			$scope.current_answering = {
				scope_obj: null,
				recommendation_index: null,
				recommendation_id: null,
				restaurant: null,
				plate: null
			};
			$scope.answer_step_block = 0;
			$scope.answer_type = 0;
			$scope.answer_restaurants = [];
			$scope.answer_plates = [];
		};

		// fine form richieste

		// inizio visualizzazione risposte
		$scope.view_requests_answers = {
			scope_obj: null,
			recommendation_id: null,
			recommendation_index: null
		};
		$scope.current_recommendation_list_view = [];
		$scope.load_more_answers_show = true;
		$scope.openAnswers = function(scope_obj,recommendation_index,recommendation_id)
		{
			
			$scope.resetAnswersList();
			var obj = {
				request: recommendation_id,
				limit: $scope.pagination_number
			};
			RecommendationServices.viewAnswersRequests.async(obj).success(function(data){

				if(data.answers.length > 0){
					$scope.view_requests_answers = {
						scope_obj: scope_obj,
						recommendation_id: recommendation_id,
						recommendation_index: recommendation_index,
						skip: 2
					};
					angular.forEach(data.answers,function(v,k){
						$scope.current_recommendation_list_view.push(v);
					});
				}
				if(data.answers.length < $scope.pagination_number) $scope.load_more_answers_show = false;
			}).error(function(){
				$scope.simpleError();
			});


		};
		$scope.loadMoreAnswers = function(){
			var obj = {
				request: $scope.view_requests_answers.recommendation_id,
				skip: parseInt(($scope.view_requests_answers.skip-1)*$scope.pagination_number),
				limit: $scope.pagination_number
			};
			RecommendationServices.viewAnswersRequests.async(obj).success(function(data){
				if(data.answers.length > 0){
					$scope.view_requests_answers = {
						scope_obj: $scope.view_requests_answers.scope_obj,
						recommendation_id: $scope.view_requests_answers.recommendation_id,
						recommendation_index: $scope.view_requests_answers.recommendation_index,
						skip: ($scope.view_requests_answers.skip+1)
					};
					angular.forEach(data.answers,function(v,k){
						$scope.current_recommendation_list_view.push(v);
					});
				} 

				if(data.answers.length < $scope.pagination_number) $scope.load_more_answers_show = false;
				
			}).error(function(){
				$scope.simpleError();
			});
		};

	};

	$scope.resetAnswersList = function(){
		$scope.view_requests_answers = {
			scope_obj: null,
			recommendation_id: null,
			recommendation_index: null
		};
		$scope.current_recommendation_list_view = [];
		$scope.load_more_answers_show = true;
	};

	$scope.simpleError = function(){
		swal({
			buttonsStyling: false,
			confirmButtonClass: 'btn btn-primary btn-sm ghost',
			title: 'Errore!',
			text: 'Si è verificato un errore',
			type: 'error'
		});	
	};
	
});