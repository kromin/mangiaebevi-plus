var MenuManagementController = angular.module('AppEditMenuManagementController',[]);

MenuManagementController
.controller('EditMenuManagementCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams,$route,$document,$timeout,ngDialog,RestaurantsServices,UserServices,ToolsServices,alertify,ImagesServices,SearchServices,ReservationServices, IframeServices, PaymentServices){
	"ngInject";


		// per ordinamento: https://github.com/a5hik/ng-sortable
		// ricordare direttiva content editable
		// per la stampa in pdf del menu: https://packagist.org/packages/mpdf/mpdf
		$scope.current_menu_management_action = 0;
		$scope.current_menu_viewed = parseInt(0);



		/**
		 * Scloby integration 
		 */
		$scope.redirect_url = encodeURI( $scope.mainURL+'/scloby/'+$scope.restaurant._id+'/' );
		
		$scope.integrateScloby = function(open_url){
			
	    	var win = window.open(open_url, 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes');   
	    	var timer = setInterval(function() {   
			    if(win.closed) {  
			        
			    }  
			}, 1000);  	
	    }



		$scope.InitMenu = function(){

			if(!$scope.restaurant.menu || $scope.restaurant.menu.length == 0) $scope.restaurant.menu = {menus: []};
			if(!$scope.restaurant.menu.menus) $scope.restaurant.menumenus = [];
			/*
				$scope.sortable_plates = {
				  //Only allow draggable when click on handle element
				  handle:'i.mebi-cd-icon-select',
				  //Construct method before sortable code					  
				  //Callback after item is dropped
				  stop:function(list, dropped_index){
				    updateMenu('no');
				  }
				};

				$scope.editing_menu = false;
				$scope.current_menu_step = 0;
				$scope.current_selected_menu = false; // indice del menu che si vuole modificare
				
				$scope.current_menu_viewed = 0;

				$scope.closeEditing = function(){
					$scope.editing_menu = false;
					$scope.current_menu_step = 0;
				}
				
				$scope.startAddingMenu = function(){
					$scope.editing_menu = true;
					$scope.current_menu_step = 1;
				}

				$scope.addMenuToList = function(){
					$scope.editing_menu = true;
					$scope.current_menu_step = 1;
				}

				$scope.add_menu = {
					menu_name: '',
					menu_type: 0, // normale, 1 == prezzo fisso
					menu_price: '0.00'
				}
				$scope.existMenuName = function(name){
					if(!$scope.restaurant.menu || !$scope.restaurant.menu.menus || $scope.restaurant.menu.menus.length == 0) return false;
					angular.forEach($scope.restaurant.menu.menus, function(v,k){
						if(v.name == name) return true;
					});

					return false;
				}

				$scope.setMenuName = function(name){
					if(name != ''){
						$scope.add_menu.menu_name = name
					}							
				}

				$scope.setMenuType = function(){
					$scope.add_menu.menu_type = ($scope.add_menu.menu_type && $scope.add_menu.menu_type == 1) ? 0 : 1;
				}


				
				$scope.confirmMenuName = function(){
					
					if(!$scope.restaurant.menu || !$scope.restaurant.menu.menus || $scope.restaurant.menu.menus.length == 0) $scope.restaurant.menu = {};
					if(!$scope.restaurant.menu.menus) $scope.restaurant.menu.menus = []
						
					
					if($scope.add_menu.menu_name != ""){
						var obj = {
							name: $scope.add_menu.menu_name,
							menu_type: $scope.add_menu.menu_type,
							menu_price: ($scope.add_menu.menu_price) ? $scope.add_menu.menu_price.replace("€ ","").replace("€","") : '',
							fasce: [],
							special_dates: [],
							categorie: [],
							asterischi: []
						}
						$scope.current_menu_index = $scope.restaurant.menu.menus.length;
						// se aggiungo un menu lo mostro
						$scope.current_menu_viewed = $scope.current_menu_index;
						$scope.restaurant.menu.menus.push(obj)
						$scope.add_menu = {
							menu_name: ''
						}

						swal({
							buttonsStyling: false,
							confirmButtonClass: 'btn btn-primary btn-sm ghost',
							title: 'Congratulazioni!',
							text: 'Il nuovo menu è stato aggiunto',
							type: 'success',
							showConfirmButton: false
						});
						
						$timeout(function(){
							swal.close()
							$scope.addMenuCategoryStep($scope.current_menu_index);
						},1000);
						
					}
				}
			*/


			// Drag
			$scope.menusDrag = {
				dragMenusListeners: {
					accept: function (sourceItemHandleScope, destSortableScope) {
					  return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
					}
				}
			}

			$scope.catsDrag = {
				dragCatsListeners: {
					accept: function (sourceItemHandleScope, destSortableScope) {
					  return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
					}
				}
			}

			$scope.plates = {
				dragPlatesListeners: {
					accept: function (sourceItemHandleScope, destSortableScope) {
					  return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
					}
				}
			}

			$scope.default_add_menu = {
				name: 'Nome del menu',
				menu_type: 0, // normale, 1 == prezzo fisso
				menu_price: 0.00,
				pre_text: '',
				post_text: '',
				categorie: []
			}

			$scope.addMenu = function(){
				var add_obj = angular.copy($scope.default_add_menu);

				if(!$scope.restaurant.menu) $scope.restaurant.menu = {menus: []};
				if(!$scope.restaurant.menu.menus) $scope.restaurant.menu.menus = [];

				$scope.restaurant.menu.menus.push(add_obj);
			}

			// richiedi cancellazione menu
			$scope.deleteCurrentMenu = function(){
				alertify
					.okBtn("Si")
		  			.cancelBtn("No")
		  			.confirm("Sicuro di voler cancellare questo menu? L'azione è irreversibile", function () {	
						
						$scope.confirmMenuDeletion();
					    
					}, function() {
					    
					});
			}

			$scope.confirmMenuDeletion = function(){
				// indice del menu da cancellare: $scope.current_menu_viewed
				if(!$scope.current_menu_viewed && $scope.current_menu_viewed != 0) return;
				angular.forEach($scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie,function(c,k){
					angular.forEach(c.piatti,function(p,kp){
						if(p.image) $scope.removeMenuImage(p.image);
					})
				});

				// tolte le immagini, ora cancelliamo il menu
				$scope.restaurant.menu.menus.splice($scope.current_menu_viewed,1);
				$scope.updateMenu('no');
				$scope.current_menu_management_action = 0;
			}

			$scope.setMenuType = function(){

				$scope.restaurant.menu.menus[$scope.current_menu_viewed].menu_type = 
				($scope.restaurant.menu.menus[$scope.current_menu_viewed].menu_type == 0 || !$scope.restaurant.menu.menus[$scope.current_menu_viewed].menu_type)
				? 1 : 0
			}
			$scope.isFixedPriceMenu = function(){
				return ($scope.restaurant.menu.menus[$scope.current_menu_viewed].menu_type == 1) ? true : false;
			}

			$scope.setOrderReady = function() {
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].orderready = 
				(!$scope.restaurant.menu.menus[$scope.current_menu_viewed].orderready || $scope.restaurant.menu.menus[$scope.current_menu_viewed].orderready == 0) 
				? 1 : 0
			}



			/* Start new*/
			$scope.defaultCategory = {
				name: 'Nome categoria',
				piatti: []
			}

			$scope.prependCategoryToMenu = function(){
				/*$scope.editing_menu = true;
				$scope.current_menu_step = 2;*/
				var addobj = angular.copy($scope.defaultCategory);
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie.unshift(addobj);
			}

			$scope.appendCategoryToMenu = function(){
				/*$scope.editing_menu = true;
				$scope.current_menu_step = 2;*/
				var addobj = angular.copy($scope.defaultCategory);
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie.push(addobj);
			}

			$scope.removeCategory = function(i){
				alertify
					.okBtn("Si")
		  			.cancelBtn("No")
		  			.confirm("Sicuro di voler cancellare questa categoria? L'azione è irreversibile", function () {	
						
						$scope.confirmCategoryDeletion(i);
					    
					}, function() {
					    
					});
			}

			$scope.confirmCategoryDeletion = function(cat_id){
				
				if(!$scope.current_menu_viewed && $scope.current_menu_viewed != 0) return;
				angular.forEach($scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[cat_id].piatti,function(p,k){
					if(p.image) $scope.removeMenuImage(p.image);
				});

				// tolte le immagini, ora cancelliamo il menu
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie.splice(cat_id,1);
				$scope.updateMenu('no');
			}


			
			$scope.open_menus = []; 
			$scope.openCat = function(i){
				if($scope.open_menus.indexOf(i) != -1){
					$scope.open_menus.splice($scope.open_menus.indexOf(i),1);
				} else {
					$scope.open_menus.push(i);
				}
			}
			$scope.isOpen = function(i){
				return ($scope.open_menus.indexOf(i) != -1) ? true : false;
			}

			$scope.avaible_allergeni = ToolsServices.allergeni.retrieve();


			$scope.default_plate = {
				name: 'Nome del piatto',
				image: null,
				ingredienti: [],
				options: [],
				custom_info: [],
				allergeni: [],
				description: 'Descrizione del piatto',
				price: '0.00'
			}

			$scope.prependPlate = function(i_cat){
				var add_obj = angular.copy($scope.default_plate);
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[i_cat].piatti.unshift(add_obj);
			}

			$scope.appendPlate = function(i_cat){
				var add_obj = angular.copy($scope.default_plate);
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[i_cat].piatti.push(add_obj);
			}

			$scope.deletePlate = function(i_cat,i_piatto){
				alertify
					.okBtn("Si")
		  			.cancelBtn("No")
		  			.confirm("Sicuro di voler cancellare questo piatto? L'azione è irreversibile", function () {	
						
						$scope.confirmDeletePlate(i_cat,i_piatto);
					    
					}, function() {
					    
					});
			}

			$scope.confirmDeletePlate = function(i_cat,i_piatto){
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[i_cat].piatti.splice(i_piatto,1);
				$scope.updateMenu('no');
			}



			$scope.default_plate_option = {
				name: "Nome categoria di opzioni",
			    type: 0, // complementari, 1 per escludenti
			    options : [
			    	{
			          no_variation: 0, // 1 se il prezzo non cambia
			          category: "Nome categoria di opzioni",
			          name: "Nome opzione",
			          variation: 0, // 0 aggiunge, 1 toglie, null nessun cambiamento
			          price: 0.00
			        }
			    ]
			}

			$scope.default_single_option = {
					no_variation: 0, // 1 se il prezzo non cambia
			        category: "Nome categoria di opzioni",
			        name: "Nome opzione",
			        variation: 0, // 0 aggiunge, 1 toglie, null nessun cambiamento
			        price: 0.00
			}

			$scope.addOption = function(cat,piatto){
				var add_obj = angular.copy($scope.default_plate_option);
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[cat].piatti[piatto].options = ($scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[cat].piatti[piatto].options)
				? $scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[cat].piatti[piatto].options
				: [];

				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[cat].piatti[piatto].options.push(add_obj);
			}

			$scope.deleteOption = function(i_cat,i_piatto,i_opt_cat){
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[i_cat].piatti[i_piatto].options.splice(i_opt_cat,1);
			}

			$scope.SetOptionCatType = function(i_cat,i_piatto,i_opt_cat){
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].type = ($scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].type == 1)
				? 0 : 1
			}

			$scope.SetOptionVariation = function(i_cat,i_piatto,i_opt_cat,i_opzione){
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].no_variation = ($scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options[i_opzione].no_variation == 1)
				? 0 : 1
			}

			$scope.appendOptions = function(i_cat,i_piatto,i_opt_cat){
				var add_obj = angular.copy($scope.default_single_option);
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options.push(add_obj);
			}

			$scope.removeOption = function(i_cat,i_piatto,i_opt_cat,i_opzione){
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[i_cat].piatti[i_piatto].options[i_opt_cat].options.splice(i_opzione,1);
			}


			$scope.addMenuCategoryStep = function(menu_index){
				
				//$scope.$apply(function(){
					// se la apro dalla pagina
					if(menu_index) $scope.current_menu_index = menu_index;
					$scope.editing_menu = true;
					$scope.current_menu_step = 2;
				//});
			}

			$scope.add_category = {
				name: '',
				piatti: []
			}


			$scope.printMenu = function(){
				var menu_style = ($scope.restaurant.menu.menus[$scope.current_menu_viewed].menu_style) ? $scope.restaurant.menu.menus[$scope.current_menu_viewed].menu_style : 0;
				$scope.updateMenu('no');
				window.open('/printMenuPage/'+$scope.restaurant._id+'/'+$scope.current_menu_viewed+'/'+menu_style+'/0/',
					'_blank',
					'fullscreen=yes',false)
			}

			$scope.menuPdf = function(){
				var menu_style = ($scope.restaurant.menu.menus[$scope.current_menu_viewed].menu_style) ? $scope.restaurant.menu.menus[$scope.current_menu_viewed].menu_style : 0;
				$scope.updateMenu('no');
				window.open('/printPDF/'+$scope.restaurant._id+'/'+$scope.current_menu_viewed+'/'+menu_style+'/',
					'_blank',
					'fullscreen=yes',false)
			}


			/* click su suggerimenti
			$scope.setCategoryName = function(name){
				$scope.add_category.name = name;
			}

			// categoria aggiunta
			$scope.confirmCategoryName = function(){
				if(!$scope.current_menu_index) $scope.current_menu_index = parseInt(0);
				if(!$scope.restaurant.menu.menus[$scope.current_menu_index].categorie) return;

				var obj = angular.copy($scope.add_category);
				$scope.category_index = $scope.restaurant.menu.menus[$scope.current_menu_index].categorie.length
				$scope.restaurant.menu.menus[$scope.current_menu_index].categorie.push(obj);

				$timeout(function(){
					$scope.scrollTo('menu_'+$scope.current_menu_index+'_category_'+$scope.category_index);
				},2000);

				//$scope.$apply(function(){
				swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Congratulazioni!',
					text: 'Categoria aggiunta',
					type: 'success',
					showConfirmButton: false
				});
				$timeout(function(){
					swal.close()
					$scope.editing_menu = true;
					$scope.current_menu_step = 3;
				},1200);
				//});
			}

			$scope.deleteCurrentCategory = function(cat_id){
				if(!cat_id && cat_id != 0) return;
				alertify
					.okBtn("Si")
		  			.cancelBtn("No")
		  			.confirm("Sicuro di voler cancellare questa categoria? L'azione è irreversibile", function () {	
						
						$scope.confirmCategoryDeletion(cat_id);
					    
					}, function() {
					    
					});
			}
			$scope.confirmCategoryDeletion = function(cat_id){
				if(!cat_id && cat_id != 0) return;
				// indice del menu da cancellare: $scope.current_menu_viewed
				if(!$scope.current_menu_viewed && $scope.current_menu_viewed != 0) return;
				angular.forEach($scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[cat_id].piatti,function(p,k){
					if(p.image) $scope.removeMenuImage(p.image);
				});

				// tolte le immagini, ora cancelliamo il menu
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie.splice(cat_id,1);
				$scope.updateMenu('no');
			}

			


			$scope.addPlateToCategory = function(category){
				if(!category && category != 0) return;
				$scope.current_menu_index = $scope.current_menu_viewed;
				$scope.category_index = category;
				$scope.editing_menu = true;
				$scope.current_menu_step = 3;
			}


			$scope.add_plate = {
				name: '',
				image: null,
				ingredienti: [],
				options: [],
				custom_info: [],
				allergeni: [],
				description: '',
				price: '0.00'
			}*/

			$scope.add_plate_image = {
				image: null
			};
			$scope.uploadPlateImage = function(category,plate){
				
				ToolsServices.mainOverlay.show();

				ImagesServices.addSingle
				.async('menu',$scope.restaurant.slug,$scope.restaurant.slug,$scope.add_plate_image.image,$scope.restaurant._id)
				.success(function(data){
					
					if(data.message && data.message == 'ok'){
						if((category || category == 0) &&
							plate || plate == 0){
							$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[category].piatti[plate].image = data.image._id;
							$scope.updateMenu('no');
						} else {
							$scope.add_plate.image = data.image._id;
						}
						
					}
					ToolsServices.mainOverlay.hide();
					
				}).error(function(){
					ToolsServices.mainOverlay.hide();
					swal({
						buttonsStyling: false,
						confirmButtonClass: 'btn btn-primary btn-sm ghost',
						title: 'Errore',
						text: 'Siamo spiacenti ma si è verificato un errore',
						type: 'error'
					});

				});
				$scope.add_plate_image.image = null;
			}



			/*
			$scope.insertPlate = function(){
				if((!$scope.category_index && $scope.category_index != 0) 
					|| (!$scope.current_menu_index && $scope.current_menu_index != 0))  return;

				$scope.add_plate.price = $scope.add_plate.price.replace("€ ","").replace("€","");

				var obj = angular.copy($scope.add_plate);
				var plate_index_new = $scope.restaurant.menu.menus[$scope.current_menu_index].categorie[$scope.category_index].piatti.length;
				$scope.restaurant.menu.menus[$scope.current_menu_index].categorie[$scope.category_index].piatti.push(obj);

				
				// resetto
				$scope.add_plate = {
					name: '',
					image: null,
					ingredienti: [],
					options: [],
					custom_info: [],
					allergeni: [],
					description: '',
					price: '0.00'
				}


				$scope.updateMenu('no');
				$scope.closeEditing();

				$timeout(function(){
					$scope.scrollTo('menu_'+$scope.current_menu_index+'_category_'+$scope.category_index+'_plate_'+plate_index_new);
				},2000);
				

			}
			*/

			$scope.deleteCurrentPlate = function(cat_id,plate_id){
				if(!cat_id && cat_id != 0) return;
				alertify
					.okBtn("Si")
		  			.cancelBtn("No")
		  			.confirm("Sicuro di voler cancellare questo piatto? L'azione è irreversibile", function () {	
						
						$scope.confirmPlateDeletion(cat_id,plate_id);
					    
					}, function() {
					    
					});
			}
			
			$scope.confirmPlateDeletion = function(cat_id,plate_id){
				if(!cat_id && cat_id != 0) return;
				if(!plate_id && plate_id != 0) return;
				// indice del menu da cancellare: $scope.current_menu_viewed
				if(!$scope.current_menu_viewed && $scope.current_menu_viewed != 0) return;
				if($scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[cat_id].piatti[plate_id].image) $scope.removeMenuImage($scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[cat_id].piatti[plate_id].image);
				

				// tolte le immagini, ora cancelliamo il menu
				$scope.restaurant.menu.menus[$scope.current_menu_viewed].categorie[cat_id].piatti.splice(plate_id,1);
				$scope.updateMenu('no');
			}




			$scope.updateMenu = function(show_alert){
				
				ToolsServices.mainOverlay.show();

				RestaurantsServices.addMenu
				.async($scope.restaurant._id, $scope.restaurant.menu).success(function(data){
					
					if(data.message && data.message == 'ok'){
			        	
			        	if(!show_alert || show_alert != 'no'){
				        	swal({
								buttonsStyling: false,
								confirmButtonClass: 'btn btn-primary btn-sm ghost',
								title: 'Tutto ok',
								text: 'Il menù è stato aggiornato',
								type: 'success'
							});	
			        	}
			        	$scope.restaurant.menu = data.menu;
			        	
			        }
			        ToolsServices.mainOverlay.hide();
				}).error(function(argument) {
					ToolsServices.mainOverlay.hide();
					swal({
						buttonsStyling: false,
						confirmButtonClass: 'btn btn-primary btn-sm ghost',
						title: 'Errore',
						text: 'Siamo spiacenti ma si è verificato un errore, prova ad accedere nuovamente',
						type: 'error'
					});
				})
			}



			$scope.setCurrentMenuOpen = function(n){
				$scope.current_menu_viewed = n;
			}


			// da lasciare
			$scope.removeMenuImage = function(id){
				ImagesServices.removeSingle.async(id).success(function(data){	
					if(data.message && data.message == 'ok'){
						return true;
					} else {
						return false;
					}							
				}).error(function(){
					return false;
				})
			}



			$scope.$watch('editing_menu',function(){
				if($scope.editing_menu)	$scope.$broadcast('rebuild:scrollbar');
			});
			$scope.$watch('current_menu_step',function(){
				$scope.$broadcast('rebuild:scrollbar');
			});

			
			$scope.returnFormattedArray = function(arr){
				return lodash.join(ing,',');
			}


		}
		/**
		* END Menu
		*/

				

});