var ApplicationController = angular.module('AppApplicationController', []);

ApplicationController
    .controller('ApplicationCtrl', 
        function($scope, $cookies, $sce, $location, $route, $routeParams, $timeout, PositionService, SearchServices, RestaurantsServices, ToolsServices, SearchServices, UserServices, ngDialog, NewsLetterServices) {
        "ngInject";
        $scope.login_form = 0; 
        $scope.can_set_cookies = navigator.cookieEnabled;
        /**
         *
         * Main application controller,
         * used for asking SEO parameters and to set current page
         *
         */
        $scope.mainUrl = document.location.origin; //'http://46.101.239.79/';

        $scope.alertVerification = function(){
            swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Congratulazioni',
                    text: 'Hai verificato il tuo account, ora puoi accedere',
                    type: 'success'
                });
        }
        /**
         *
         * Posizione utente
         * @return $scope.position.lat $scope.position.lng, $cookies 'lat' && 'lng'
         *
         */ 
        $scope.position = {
            lat: 0,
            lng: 0
        } 

        var getUserLocation = function() {
            if(navigator.geolocation){
                if (!navigator.geolocation.getCurrentPosition(setPositionHTML5)) {
                    /*PositionService.async().success(function(d) {
                        var p = d.loc.split(",")
                        $scope.position.lat = p[0];
                        $scope.position.lng = p[1];
                        setUserCookiePosition(p[0], p[1]);
                    })*/
                }
            }
        }
        var setPositionHTML5 = function(position) {
            $scope.position.lat = position.coords.latitude;
            $scope.position.lng = position.coords.longitude;
            setUserCookiePosition(position.coords.latitude, position.coords.longitude);
        }

        // verifico che non abbia già impostato la posizione
        if($scope.can_set_cookies){
            if (!$cookies.get('lat') || !$cookies.get('lng')) {
                getUserLocation();
            } else {
                $scope.position.lat = $cookies.get('lat');
                $scope.position.lng = $cookies.get('lng');
            }
        } 

        // mettere posizione utente nel cookie
        var setUserCookiePosition = function(lat, lng) {
            if($scope.can_set_cookies){
                $cookies.put('lat', lat);
                $cookies.put('lng', lng);
            }            
        }




        /***
         *
         * Setting tools
         * indicizzo tutto in base all'id per rendere più semplice il replacement
         *
         *

        ToolsServices.getMainSiteInfo.async().success(function(data) {
            $scope.creditCard = _.indexBy(data.creditCard, '_id');
            $scope.homeCities = _.indexBy(data.homeCities, '_id');
            $scope.regioniCucine = _.indexBy(data.regioniCucine, '_id');
            $scope.tipiCucine = _.indexBy(data.tipiCucine, '_id');
            $scope.tipiLocali = _.indexBy(data.tipiLocali, '_id');
            $scope.services = _.indexBy(data.services, '_id');
            $scope.pacchetti = _.indexBy(data.pacchetti, 'price_month');
            $scope.staticURL = data.staticDomain; // domain for static files	
        });
        */



        /**
         *
         *
         *
         *   Funzioni per la ricerca con filtro
         *
         *
         *
         *
         *
         *
         ********/






        /**
         *
         *
         * Funzioni pagina singolo ristorante
         *
         *
         *******/

        $scope.hide_cookies = true;

        // nascondi newsletter
        if ($cookies.get('newsletter_subscribed') == 1) {
            $scope.newsletter_set = true;
            $scope.newsletter_name_set = true;
            $scope.show_newsletter = false;
        }

        if (!$cookies.get('cookie_accepted') || $cookies.get('cookie_accepted') != 1) {
            $scope.hide_cookies = true;
        }
        

        $scope.searched_restaurants = false;
        $scope.geo_result = false;
/*
        $scope.$on('$routeChangeSuccess', function() {
            

            $scope.hide_cookies = true;

            // nascondi newsletter
            if ($cookies.get('newsletter_subscribed') == 1) {
                $scope.newsletter_set = true;
                $scope.newsletter_name_set = true;
                $scope.show_newsletter = false;
            }

            if (!$cookies.get('cookie_accepted') || $cookies.get('cookie_accepted') != 1) {
                $scope.hide_cookies = true;
            }

            $scope.blue = false;
            $scope.search = false;
            $scope.bg_w = false;
            switch ($route.current.templateUrl) {
                case '/templates/search':
                    $scope.blue = true;
                    $scope.search = true;
                    break;
                case '/templates/plate':
                    $scope.blue = true;
                    $scope.search = false;
                    break;
                case '/templates/profile': case '/templates/terms': case '/templates/privacy': case '/templates/uploadimages':
                    $scope.blue = true;
                    $scope.search = false;
                    $scope.bg_w = true;
                    break;
                default:
                    $scope.blue = false;
                    $scope.search = false;
                    break;
            }



            if ($routeParams) {
                

                
            } 
        })
*/

        /**
         * Imposta ristoranti nella mappa
         */
        var set_search_map_restaurant = function() {
            $scope.map_restaurants = [];
            angular.forEach($scope.searched_restaurants, function(val, k) {
                if (val.obj) {
                    val.obj.distance = (val.dis / 1000).toFixed(2);
                    $scope.map_restaurants.push(val.obj);
                } else {
                    $scope.map_restaurants.push(val);
                }
            });
            //console.log('map restaurants', $scope.map_restaurants)
            $scope.$broadcast('initializeMap', $scope.map_restaurants);
        }

        /**

        Utente

        **/

        $scope.current_user = {}
        UserServices.getLogged.async().success(function(data) {
            UserServices.isCurrentLogged.set(data);
            $scope.current_user = data
                // fai partire l'evento per prendere utente loggato
            $scope.$broadcast('userLogged', 'application');
        })

        $scope.$on('userLogged', function(event) {
            // se l'utente ha fatto il login mettilo in scope
            $scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;
            $scope.loadUserRestaurants();
        });

        $scope.loadUserRestaurants = function(){
            if($scope.current_user.role && $scope.current_user.role >= 2){
                $scope.user_footer_restaurants = []
                RestaurantsServices.viewAllMine.async().success(function(data){
                    $scope.user_footer_restaurants = data.restaurants
                });
            }
        }

        /**
         * Dialog Login e registrazione
         *
         */
        $scope.dialogLogin = function(val) {
            if (val) {
                $scope.login_form = val;
            }
            var loginDialog = ngDialog.open({
                template: '/templates/dialoglogin',
                scope: $scope,
                className: 'ngdialog ngdialog-theme-default small'
            });

            
            return loginDialog;
        }

        $scope.dialogRegister = function() {
            var registerDialog = ngDialog.open({
                template: '/templates/dialogregister',
                scope: $scope,
                className: 'ngdialog ngdialog-theme-default medium'
            });

            registerDialog.closePromise.then(function(data) {
                switch (data.value) {
                    case 'toLogin':
                        $scope.dialogLogin();
                        break;
                }
            });
            return registerDialog;
        }




       /**
         *
         * Newsletter
         */
        $scope.newsletter_set = false;
        $scope.newsletter_name_set = false;
        $scope.show_newsletter = true;
        $scope.newsletter = {
            name: '',
            email: ''
        }
        $scope.add_newsletter_email = function() {
            if ($scope.newsletter.email != '') {
                $scope.newsletter_set = true;
            }

        }

        $scope.subscribe = function(val) {
            if ($scope.newsletter.email != '' && $scope.newsletter.name != '') {
                NewsLetterServices.register.async($scope.newsletter.email, $scope.newsletter.name).success(function(data) {
                    //if (data.message == 'ok') {
                        // tolto if per evitare che se 1 ha già lasciato la mail non abbia feedback, lato server metto un log
                        $scope.newsletter_name_set = true;
                        $scope.show_newsletter = false;
                        $cookies.put('newsletter_subscribed', 1);

                    //}
                }).error(function(er){
                    
                    $scope.newsletter_name_set = true;
                    $scope.show_newsletter = false;
                    $cookies.put('newsletter_subscribed', 1);

                })
            }
        }


        $scope.acceptCookie = function() {
            if($scope.can_set_cookies){
                $cookies.put('cookie_accepted', 1);
            }
            $scope.hide_cookies = true;
        }



        /**
        * Store page
        */

        $scope.$on('page loaded',function(){            
            ToolsServices.mainOverlay.hide();  
            $(".show_on_load").livequery(function(){$(this).removeClass("hidden")});          
        });
        


        /**
        * Seo
        */
        $scope.seo = {
            
        }
        $scope.setSEO = function(title,type,url,mainI,smallI,desc){
                       
        }
        

        $scope.$on('setSEO',function(event,seoObj){
           
        });

    });  