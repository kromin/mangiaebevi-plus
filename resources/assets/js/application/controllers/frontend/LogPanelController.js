var LogPanelController = angular.module('AppLogPanelController',[]);

LogPanelController
.controller('LogPanelCtrl', function($scope,$rootScope,$cookies,$sce,$location,$document,ngDialog,RestaurantsServices,UserServices,SearchServices){
	"ngInject";
	
	//$scope.login_form = (!$scope.$parent.login_form) ? 0 : $scope.$parent.login_form;
	$scope.login = {};
    $scope.login_error_message = false;
    $scope.login_form_error_message = '';
	$scope.open_login_form = function(){
		$scope.login_form = 1;
	}
	$scope.open_register_form = function(){
		$scope.login_form = 3;
	}
    $scope.open_recovery_form = function(){
        $scope.login_form = 2;
    }
	$scope.open_resend_form = function(){
		$scope.login_form = 4;
	}

	$scope.accessWithSocial = function(provider){
    	var win = window.open('/socialog/'+provider,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes');   
    	var timer = setInterval(function() {   
		    if(win.closed) {  
		        clearInterval(timer);  
		        SetUser();	
		    }  
		}, 1000);  	
    }

    $scope.register = function(reserveFormData){
    	UserServices.register.async(reserveFormData.email,reserveFormData.name,reserveFormData.surname,reserveFormData.password,reserveFormData.phone,1).success(function(data){
			// ora loggalo
			if(data.message == 'ok'){

				swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Congratulazioni',
                    text: 'La registrazione è confermata, hai ricevuto una mail con il link per attivare il tuo account',
                    type: 'success'
                });
                ngDialog.close();

			} else
            if(data.message == 'error.just_present'){
                swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Errore',
                    text: 'Sembra l\'indirizzo email sia già in uso',
                    type: 'error'
                });
            }

		}).error(function(e){
            console.log(e);
            swal({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                title: 'Oops!',
                text: 'Qualcosa è andato storto',
                type: 'error'
            });
        });
    }

    $scope.login = function(){
    	UserServices.login.async($scope.login.email,$scope.login.password).success(function(data){
        	// richiama di nuovo il dialog del login
        	if(data.message == 'ok'){
        		SetUser();          		
        	} else {
                if(data.error_message){
                    $scope.login_form_error_message = data.error_message
                }
            }
        	// messaggio di errore
        }).error(function(d){
            swal({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                title: 'Oops!',
                text: 'Qualcosa è andato storto',
                type: 'error'
            });
        })
    }

    $scope.recovery_success = false;
    $scope.error_recovery = false;
    $scope.recovery = function(){
    	UserServices.recovery.async($scope.recovery.email).success(function(data){
        	// richiama di nuovo il dialog del login
        	if(data.message == 'ok'){
        		$scope.recovery_success = 1;	
        	} else{
        		switch(data.message){
        			case 'error.empty':
        			$scope.error_recovery = 1;
        			break;
        			case 'error.sociallog':
        			$scope.error_recovery = 2;
        			break;
        		}
        	}
        	// messaggio di errore
        });
    }


    $scope.resend_success = false;
    $scope.error_resend = false;
    $scope.resend = function(){
        UserServices.resend.async($scope.resend.email).success(function(data){
            // richiama di nuovo il dialog del login
            if(data.message == 'ok'){
                $scope.resend_success = 1;    
            } else{
                switch(data.message){
                    case 'error.empty':
                    $scope.error_resend = 1;
                    break;
                    case 'error.sociallog':
                    $scope.error_resend = 2;
                    break;
                }
            }
            // messaggio di errore
        });
    }

    var SetUser = function(){
    	UserServices.getLogged.async().success(function(data){
        	$scope.current_user = data
			UserServices.isCurrentLogged.set($scope.current_user);
        	//$scope.$parent.current_user = $scope.current_user;
        	$rootScope.$broadcast('userLogged');
        	if($scope.closeThisDialog){
        		$scope.closeThisDialog('loginSubmitted');
        	}        	
			//$scope.need_login = 0; // imposta il need login a 0 per aprire il pagamento
			//initializeBraintreePayment();
        });	  
    }

});