var MenuPublishingcontroller = angular.module('AppMenuPublishingController',[]);

MenuPublishingcontroller
.controller('MenuPublishingCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams,$route,$document,$timeout,ngDialog,RestaurantsServices,UserServices,ToolsServices,alertify,ImagesServices,SearchServices,ReservationServices, IframeServices, PaymentServices){
	"ngInject";

	// prima vedi se utente ha abbonamento

	// se non ha abbonamento lo deve fare

	// se ha abbonamento dagli i dati per integrare il menu
	$scope.view_user = (UserServices.isCurrentLogged.isLoggedCurrently.user) ? UserServices.isCurrentLogged.isLoggedCurrently.user : {};
	$scope.initPlans = function(){
		$scope.selected_plan_period = parseInt(0);

		$scope.returnCicleText = function(i){
			return ($scope.selected_plan_period == 0) ? 'Mensile' : 'Annuale'
		}
		$scope.setPlanPeriod = function(n){
			$scope.selected_plan_period = n;
		}

		$scope.submitPlan = function(plan){
			ToolsServices.mainOverlay.show();
			PaymentServices.subscribePlan.async(plan,$scope.selected_plan_period).success(function(data){
				if(data.redirect){
					window.location = data.redirect;
				} else {
					ToolsServices.mainOverlay.hide();
				}
			}).error(function(d){
				ToolsServices.mainOverlay.hide();
				$scope.simpleError();
			})
		}
	}

	$scope.current_publish_action = parseInt(0);
	$scope.publishAction = function(n){
		$scope.current_publish_action = parseInt(n);
	}

	$scope.hasiframes = 0;
	$scope.initWidget = function(){
		IframeServices.viewByRestaurant.async($scope.restaurant._id).success(function(data){
			if(data.iframes.length > 0){
				// ha già aggiunto iframe 
				$scope.hasiframes = 1;
				$scope.iframe = data.iframes[0];
				$scope.url_token = $sce.trustAsResourceUrl(/static/+$scope.iframe.token+'/frameloader')
				$scope.url_token_menu = $sce.trustAsResourceUrl(/static/+$scope.iframe.token+'/menu/frameloader')
			}
		});

		$scope.add_iframe = {
			url: '',
			restaurant: $scope.restaurant._id
		}
		$scope.addIframe = function(){
			if($scope.hasiframes == 1){
				// aggiungi solo url
				var obj = { 
					url: $scope.add_iframe.url,
					restaurant: $scope.restaurant._id, 
					iframe: $scope.iframe._id
				}
				IframeServices.addSingleUrl.async(obj).success(function(data){
					if(data.message == 'ok'){
						$scope.hasiframes = 1;
						$scope.iframe = data.iframe
					}								
				})
			} else {
				IframeServices.addSingle.async($scope.add_iframe).success(function(data){
					if(data.message == 'ok'){
						$scope.hasiframes = 1;
						$scope.iframe = data.iframe
					}								
				})
			}
		}
	}


	

	
	$scope.enableFacebook = function(){
		FB.ui({
		  method: 'pagetab',
		  redirect_uri: $scope.mainUrl+'/importframe/facebook/'
		}, function(response){
			//console.log(response.tabs_added)
			if(Object.keys(response.tabs_added)[0]){
				$scope.addFacebookID(Object.keys(response.tabs_added)[0]);
				swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Congratulazioni!',
					text: 'Verifica che la tab sia stata aggiunta alla tua pagina',
					type: 'success'
				});	
			}
			
		});
	}



	$scope.addFacebookID = function(page){
		var page = parseInt(page);
		RestaurantsServices.addFacebookPage.async($scope.restaurant._id,page).success(function(data){
			if(data.message == 'just'){
				swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Errore!',
					text: 'La pagina è già stata aggiunta da un ristorante',
					type: 'error'
				});	
			} else
			if(data.message == 'ok'){
				swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Congratulazioni!',
					text: 'La pagina è stata aggiunta',
					type: 'success'
				});	
				$scope.restaurant.facebook_page_id = page;
			}
		});
	}

	$scope.removeFacebookPage = function(){
		alertify
			.okBtn("Si")
  			.cancelBtn("No")
  			.confirm("Vuoi cancellare la tua pagina facebook?", function () {	
				
				$scope.confirmRemoveFacebookPage();
			    
			}, function() {
			    
			});
					
	}

	$scope.confirmRemoveFacebookPage = function(){
		RestaurantsServices.removeFacebookPage.async($scope.restaurant._id).success(function(data){
			$scope.restaurant.facebook_page_id = '';
		});
	}
 
});