var RegistrationController = angular.module('AppRegistrationController', []);

RegistrationController
    .controller('RegistrationCtrl', function($scope, $rootScope, $cookies, $sce, UserServices, ToolsServices) {
        "ngInject";
        
        $scope.hide_form = false;
        $scope.accessWithSocial = function(provider){
            var win = window.open('/socialog/'+provider,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes');   
            var timer = setInterval(function() {   
                if(win.closed) {  
                    clearInterval(timer);  
                    window.location.href = '/';
                }  
            }, 1000);   
        };

        $scope.register = function(reserveFormData){
            ToolsServices.mainOverlay.show();
            UserServices.register.async(reserveFormData.email,reserveFormData.name,reserveFormData.surname,reserveFormData.password,reserveFormData.phone,1).success(function(data){
                // ora loggalo
                if(data.message == 'ok'){
                    $scope.hide_form = true;
                    swal({
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-primary btn-sm ghost',
                        title: 'Congratulazioni',
                        text: 'Ti è stata inviata una email con il link per attivare il tuo account',
                        type: 'success'
                    });

                } else
                if(data.message == 'error.just_present'){
                    swal({
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-primary btn-sm ghost',
                        title: 'Errore',
                        text: 'Sembra l\'indirizzo email sia già in uso',
                        type: 'error'
                    });
                }

                ToolsServices.mainOverlay.hide();

            }).error(function(e){
                
                swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Oops!',
                    text: 'Qualcosa è andato storto',
                    type: 'error'
                });

                ToolsServices.mainOverlay.hide();
            });
        };

    });