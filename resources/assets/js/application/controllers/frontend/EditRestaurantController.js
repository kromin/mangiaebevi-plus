var RestaurantController = angular.module('AppEditRestaurantController',[]);

RestaurantController
.controller('EditRestaurantCtrl', function($scope,$rootScope,$cookies,$sce,$location,$routeParams,$route,$document,$timeout,ngDialog,RestaurantsServices,UserServices,ToolsServices,alertify,ImagesServices,SearchServices,ReservationServices, IframeServices, PaymentServices){
	"ngInject";
	
	//$scope.user = {};
	$scope.verified = false;
	$scope.hasNotSaved = false; // verifica che abbia salvato
	
	ToolsServices.getMainSiteInfo.async().success(function(data) {
        $scope.creditCard = _.indexBy(data.creditCard, '_id');
        $scope.homeCities = _.indexBy(data.homeCities, '_id');
        $scope.regioniCucine = _.indexBy(data.regioniCucine, '_id');
        $scope.tipiCucine = _.indexBy(data.tipiCucine, '_id');
        $scope.tipiLocali = _.indexBy(data.tipiLocali, '_id');
        $scope.services = _.indexBy(data.services, '_id');
        $scope.pacchetti = _.indexBy(data.pacchetti, 'price_month');
        $scope.staticURL = data.staticDomain; // domain for static files    
        $scope.mainURL = data.main_domain;
    });
	
	
	
	$scope.current_tab = 5;
	$scope.changeTab = function(n){
		$scope.current_tab = n;
	}	

	$scope.taOptions = [
		      //['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre', 'quote'],
		      ['bold', 'italics', 'underline','justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent'/*,'wordcount', 'charcount' 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'*/],
		      //['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent'],
		      //[/*'html', 'insertImage','insertLink', 'insertVideo',*/ 'wordcount', 'charcount']
		  ];

	/*var startWatching = function(){
		// Vediamo se l'oggetto subisce modifiche, da rivedere
		$scope.$watch('restaurant',function(newValue, oldValue){
			// ha cambiato info del ristorante
			if(newValue != oldValue) {
				$scope.hasNotSaved = true;
			} 
		},true);
	}*/

	$scope.scrollTo = function(element_id){
		
		var el = angular.element(document.getElementById(element_id));
		if(el){
			$document.scrollToElement(el, 100, 500);
		} else {
			
		}
        
	}

	$scope.$on('page loaded',function(){
		
		$scope.view_user = (UserServices.isCurrentLogged.isLoggedCurrently.user) ? UserServices.isCurrentLogged.isLoggedCurrently.user : {};

		if(!UserServices.isCurrentLogged.isLoggedCurrently.user 
			|| ($scope.view_user._id != $scope.restaurant.user && $scope.view_user.role <= 2)) {window.location = '/';}
		
		//console.log('user',$scope.view_user) 
	})


//if(UserServices.isCurrentLogged.isLoggedCurrently.loggedStatus != 1 || !UserServices.isCurrentLogged.isLoggedCurrently.loggedStatus) $location.path('/');

	$scope.loadRestaurant = function(restaurant){
		//console.log(UserServices.isCurrentLogged.isLoggedCurrently)
		
			 
			
			alertify.logPosition("bottom right");


			// prendo info sull'utente
			SearchServices.searchBySlug.async(restaurant).success(function(data){				
				

				$scope.convertHashToTab = function(hash){
					var tab = 0;
					switch(hash){
						case '/':
						tab = 5;
						break;
						case '/statistiche':
						tab = 5;
						break;
						case '/clienti':
						tab = 6;
						break;
						case '/menu':
						tab = 2;
						break;
						case '/sale':
						tab = 3;
						break;
						case '/prenotazioni':
						tab = 4;
						break;
						case '/informazioni':
						tab = 1;
						break;					
						default:
						tab = 5;
						break;
					}
					$scope.changeTab(tab);
				}

				$scope.convertHashToTab($location.path());
				/**
				 * Manage Tabs
				 */
				$scope.$on('$locationChangeSuccess', function(){
					$scope.convertHashToTab($location.path());
				});


				if(data.message != 'ok') window.location = '/';
				
				if(!data.restaurants || !data.restaurants[0]) window.location = '/';
				
				if(
					(data.restaurants[0].user && UserServices.isCurrentLogged.isLoggedCurrently.user && UserServices.isCurrentLogged.isLoggedCurrently.user._id != data.restaurants[0].user)
					&& UserServices.isCurrentLogged.isLoggedCurrently.user.role < 3) window.location = '/';

				
				$scope.restaurant = data.restaurants[0];

				
				$scope.restaurant.description_html = $sce.trustAsHtml($scope.restaurant.description);

				$scope.restaurant.web = $scope.restaurant.contact.web
				$scope.restaurant.email = $scope.restaurant.contact.email
				$scope.restaurant.phone = $scope.restaurant.contact.phone

				$scope.loading = false;
				$scope.change_avatar = {}

				// per i ristoranti importati						

				if(!$scope.restaurant.staff || $scope.restaurant.staff.length == 0)	$scope.restaurant.staff = {};
				// inizializzo qui il watching delle info del ristorante

				$scope.initialRestaurant = data.restaurants[0];
				//startWatching();



				// Modifiche al ristorante
				$scope.removeNumber = function(num){
					$scope.restaurant.phone.splice(num,1);
				}
				$scope.addPhoneNumber = function(num){
					if(num && num != ''){
						if(!$scope.restaurant.phone){
							$scope.restaurant.phone = [];
						}
						$scope.restaurant.phone.push(num);
					}
				}

				$scope.editSocial = function(){
					ngDialog.open({
		                template: '/templates/dialogeditsocial',
		                scope: $scope,
		                className: 'ngdialog ngdialog-theme-default'
		            });
				}



				$scope.add_staff_member = {
				}
				$scope.avaible_roles = ['chef','sommelier','barman','maitre'];
				$scope.add_staff_member_to_restaurant = function(role,name){
					if(!name || name == '' || $scope.avaible_roles.indexOf(role) == -1 || !role) return;
					
					if(!$scope.restaurant.staff) $scope.restaurant.staff = {}

					if(!$scope.restaurant.staff[role]){
						//$scope.restaurant.staff.push(role)
						$scope.restaurant.staff[role] = [];								
					}
					
					$scope.restaurant.staff[role].push(name);
					$scope.add_staff_member[role+'_name'] = '';
				}

				$scope.remove_staff_member = function(role,index){
					$scope.restaurant.staff[role].splice(index,1);
				}






				$scope.hasTipoLocale = function(tipo){
					if($scope.restaurant.tipoLocale && $scope.restaurant.tipoLocale.indexOf(tipo) != -1){
						return true;
					} else {
						return false;
					}
				}
				$scope.isPrimaryTipoLocale  = function(tipo){
					return ($scope.restaurant.tipoLocale.indexOf(tipo) == 0) ? true : false;							
				}

				$scope.setPrimaryTipoLocale = function(tipo){
					var index = $scope.restaurant.tipoLocale.indexOf(tipo);
					if(index == -1) {
						$scope.restaurant.tipoLocale.unshift(tipo);
					} else {
						$scope.restaurant.tipoLocale.splice(index,1);
						$scope.restaurant.tipoLocale.unshift(tipo);
					}
				}

				$scope.addRemoveTipoLocale = function(id){
					if($scope.restaurant.tipoLocale){
							if($scope.restaurant.tipoLocale.indexOf(id) != -1){
							$scope.restaurant.tipoLocale.splice($scope.restaurant.tipoLocale.indexOf(id),1)
						} else {
							$scope.restaurant.tipoLocale.push(id);
						}
					} else {
						$scope.restaurant.tipoLocale = [id];
					}
				}


				$scope.hasTipoCucina = function(tipo){
					if($scope.restaurant.tipoCucina && $scope.restaurant.tipoCucina.indexOf(tipo) != -1){
						return true;
					} else {
						return false;
					}
				}
				$scope.isPrimaryTipoCucina  = function(tipo){
					return ($scope.restaurant.tipoCucina && $scope.restaurant.tipoCucina.indexOf(tipo) == 0) ? true : false;							
				}
				$scope.setPrimaryTipoCucina = function(tipo){
					if(!$scope.restaurant.tipoCucina) $scope.restaurant.tipoCucina = [];

					var index = $scope.restaurant.tipoCucina.indexOf(tipo);
					if(index == -1) {
						$scope.restaurant.tipoCucina.unshift(tipo);
					} else {
						$scope.restaurant.tipoCucina.splice(index,1);
						$scope.restaurant.tipoCucina.unshift(tipo);
					}
				}
				$scope.addRemoveTipoCucina = function(id){
					if($scope.restaurant.tipoCucina){
							if($scope.restaurant.tipoCucina.indexOf(id) != -1){
							$scope.restaurant.tipoCucina.splice($scope.restaurant.tipoCucina.indexOf(id),1)
						} else {
							$scope.restaurant.tipoCucina.push(id);
						}
					} else {
						$scope.restaurant.tipoCucina = [id];
					}
				}


				$scope.hasRegioneCucina = function(regione){
					if($scope.restaurant.regioneCucina && $scope.restaurant.regioneCucina.indexOf(regione) != -1){
						return true;
					} else {
						return false;
					}
				}
				$scope.addRemoveRegioneCucina = function(id){
					if($scope.restaurant.regioneCucina){
							if($scope.restaurant.regioneCucina.indexOf(id) != -1){
							$scope.restaurant.regioneCucina.splice($scope.restaurant.regioneCucina.indexOf(id),1)
						} else {
							$scope.restaurant.regioneCucina.push(id);
						}
					} else {
						$scope.restaurant.regioneCucina = [id];
					}
				}


				$scope.hasService = function(service){
					if($scope.restaurant.services && $scope.restaurant.services.indexOf(service) != -1){
						return true;
					} else {
						return false;
					}
				}
				$scope.addRemoveService = function(id){
					if($scope.restaurant.services){
							if($scope.restaurant.services.indexOf(id) != -1){
							$scope.restaurant.services.splice($scope.restaurant.services.indexOf(id),1)
						} else {
							$scope.restaurant.services.push(id);
						}
					} else {
						$scope.restaurant.services = [id];
					}
				}

				$scope.hasCard = function(card){
					if($scope.restaurant.carte && $scope.restaurant.carte.indexOf(card) != -1){
						return true;
					} else {
						return false;
					}
				}
				$scope.addRemoveCard = function(id){
					if($scope.restaurant.carte){
							if($scope.restaurant.carte.indexOf(id) != -1){
							$scope.restaurant.carte.splice($scope.restaurant.carte.indexOf(id),1)
						} else {
							$scope.restaurant.carte.push(id);
						}
					} else {
						$scope.restaurant.carte = [id];
					}
				}






				$scope.onPrimaryDrop = function (data, type) {
				    // imposta primario
				    var el = data['json/el_obj'];
				    
				    switch(type){
				    	case 'tipo_locale':
				    		var index = el.index;
							var id = $scope.restaurant.tipoLocale[index];
							
							$scope.restaurant.tipoLocale.splice(index,1);
							$scope.restaurant.tipoLocale.unshift(id);
				    	break;
				    	case 'tipo_cucina':
					    	if(!$scope.restaurant.tipoCucina) $scope.restaurant.tipoCucina = [];
							
							var index = el.index;
							var id = $scope.restaurant.tipoCucina[index];
							
							$scope.restaurant.tipoCucina.splice(index,1);
							$scope.restaurant.tipoCucina.unshift(id);
							
				    	break;
				    	case 'regione_cucina':
				    		if(!$scope.restaurant.regioneCucina) $scope.restaurant.regioneCucina = [];
							
							var index = el.index;
							var id = $scope.restaurant.regioneCucina[index];
							
							$scope.restaurant.regioneCucina.splice(index,1);
							$scope.restaurant.regioneCucina.unshift(id);
							
				    	break;
				    	
					}
				    
				};





				$scope.setImagePrimary = function(data, type){

					var el = data['json/img_obj'];
					var id = el.image_id;					
					$scope.restaurant.image = id;
					$scope.updateInfo();
				}

				$scope.removeImageOnDrop = function(data, type){
					var el = data['json/img_obj'];
					var id = el.image_id;
					alertify
					.okBtn("Si")
		  			.cancelBtn("No, ho sbagliato")
		  			.confirm("Vuoi cancellare questa immagine?", function () {	
						
						ImagesServices.removeSingle.async(id).success(function(data){	
							if(data.message && data.message == 'ok'){
								var old_index = $scope.restaurant.images.indexOf(id);
								// tolgo ultima immagine
								$scope.restaurant.images.splice(old_index,1);
								if($scope.restaurant.image == id){
									// era la principale
									// devo sostituirla
									if($scope.restaurant.images[0]){
										$scope.restaurant.image = $scope.restaurant.images[0];
									} else {
										$scope.restaurant.image = "";
									}
									

									// ora richiamo il servizio per salvataggio immagini ristorante
									
								}
								$scope.updateInfo();
							}							
						})
					    
					}, function() {
					    
					});
					
				}

				$scope.removeImageOnClick = function(id){
					
					alertify
					.okBtn("Si")
		  			.cancelBtn("No")
		  			.confirm("Vuoi cancellare questa immagine?", function () {	
						
						ImagesServices.removeSingle.async(id).success(function(data){	
							if(data.message && data.message == 'ok'){
								var old_index = $scope.restaurant.images.indexOf(id);
								// tolgo ultima immagine
								$scope.restaurant.images.splice(old_index,1);
								if($scope.restaurant.image == id){
									// era la principale
									// devo sostituirla
									if($scope.restaurant.images[0]){
										$scope.restaurant.image = $scope.restaurant.images[0];
									} else {
										$scope.restaurant.image = "";
									}
									

									// ora richiamo il servizio per salvataggio immagini ristorante
									
								}
								$scope.updateInfo();
							}							
						})
					    
					}, function() {
					    
					});
					
				}

				$scope.setImagePrimaryOnClick = function(id){
										
					$scope.restaurant.image = id;
					$scope.updateInfo();
				}



				$scope.restaurant.adding_image = null;
				$scope.added_images = 0;
				$scope.images_to_add = 0;
				$scope.uploadRestaurantImage = function(){	
					if ($scope.restaurant.adding_image && $scope.restaurant.adding_image.length) {

						ToolsServices.mainOverlay.show();
						$scope.images_to_add = $scope.restaurant.adding_image.length;

				        for (var i = 0; i < $scope.restaurant.adding_image.length; i++) {
				        	
				        	RestaurantsServices
							.uploadImage
							.async($scope.restaurant._id,$scope.restaurant.adding_image[i])
							.success(function(data){
								if(!$scope.restaurant.images) $scope.restaurant.images = [];															
								$scope.restaurant.images.push(data.image);

								$scope.added_images++;
								if($scope.added_images == $scope.images_to_add) {
									$scope.restaurant.adding_image = null;
									$scope.added_images = 0;
									$scope.images_to_add = 0;
									ToolsServices.mainOverlay.hide();
								}
							}); 

				        }
			      	}
					/*RestaurantsServices
					.uploadImage
					.async($scope.restaurant._id,$scope.restaurant.adding_image)
					.success(function(data){
						if(!$scope.restaurant.images) $scope.restaurant.images = [];															
						$scope.restaurant.images.push(data.image);
						$scope.restaurant.adding_image = null;
					});*/
					
				}



				
				$scope.updateInfo = function(){			
					ToolsServices.mainOverlay.show();
					RestaurantsServices.editSingle.async($scope.restaurant._id,$scope.restaurant).success(function(data){
						
						ToolsServices.mainOverlay.hide(); 

						if(data.message && data.message == 'ok'){
							swal({
								buttonsStyling: false,
								confirmButtonClass: 'btn btn-primary btn-sm ghost',
								title: 'Congratulazioni!',
								text: 'Modifiche salvate',
								type: 'success'
							});			
							$scope.hasNotSaved = true;				
				        	//alertify.success('Modifiche salvate');
				        } else {
				        	swal({
								buttonsStyling: false,
								confirmButtonClass: 'btn btn-primary btn-sm ghost',
								title: 'Oops!',
								text: 'Qualcosa è andato storto',
								type: 'error'
							});
				        }

					}).error(function(e){
						
						var message = 'Qualcosa è andato storto';
						if(e.message == 'error.no_valid_address') message = 'Indirizzo non valido';
						swal({
							buttonsStyling: false,
							confirmButtonClass: 'btn btn-primary btn-sm ghost',
							title: 'Oops!',
							text: message,
							type: 'error'
						});
						ToolsServices.mainOverlay.hide(); 
					});
					
				}


				$scope.week_days = ['Lunedì','Martedì','Mercoledì','Giovedì','Venerdì','Sabato','Domenica'];
				$scope.add_fascia_oraria = {}
				if(!$scope.restaurant.fasce){
					$scope.restaurant.fasce = {
						0:[],1:[],2:[],3:[],4:[],5:[],6:[]
					}
				}
				$scope.add_fascia_oraria = {
					day: 0
				}
				$scope.remove_fascia = function(fascia,day){
					$scope.restaurant.fasce[day].splice(fascia,1);
				}


				$scope.avaibleFasce = ToolsServices.returnFasceOrarieDays.returnArray();
				$scope.fascia_corrente = {
					from: $scope.avaibleFasce[0],
					to: $scope.avaibleFasce[1]
				}

				$scope.add_fascia_to_restaurant = function(day){
					if(!day || $scope.avaibleFasce.indexOf($scope.fascia_corrente.from) == -1 || $scope.avaibleFasce.indexOf($scope.fascia_corrente.to) == -1){
						return;
					}
					var fascia = $scope.fascia_corrente.from+'-'+$scope.fascia_corrente.to;
					if(!$scope.restaurant.fasce) $scope.restaurant.fasce = [];
					$scope.restaurant.fasce[day].push(fascia)
				}

				$scope.add_fascia_to_restaurant_all_days = function(){
					if($scope.avaibleFasce.indexOf($scope.fascia_corrente.from) == -1 || $scope.avaibleFasce.indexOf($scope.fascia_corrente.to) == -1){
						return;
					}
					var fascia = $scope.fascia_corrente.from+'-'+$scope.fascia_corrente.to;
					angular.forEach($scope.restaurant.fasce,function(v,k){
						$scope.restaurant.fasce[k].push(fascia)
					})
					
				}
				$scope.updating_orari = false;
				$scope.updateOrari = function(){
					$scope.updating_orari = true;
					RestaurantsServices.changeOrari.async($scope.restaurant._id,$scope.restaurant.fasce).success(function(data){
						$scope.updating_orari = false;
					})
				}


				$('#datepicker').livequery(function(){
			    	$(this).datetimepicker({
				    	locale: 'it',
				    	format: 'YYYY-MM-DD'
				    }).on("dp.change", function (e) {	    	
				    	setPickerDate();
			        })
			    });
			    var setPickerDate = function(){
			    	$scope.specialDates.day = $('#datepicker').val();
			    }

				$scope.specialDates = {
					day: moment().format('YYYY-MM-DD'),
					type: '0',
					from: $scope.avaibleFasce[0],
					to: $scope.avaibleFasce[1],
					all_day: '0'
				}

				$scope.setSpecialDate = function(){

					if(
						( ($scope.specialDates.day == '' || !moment($scope.specialDates.day,'YYYY-MM-DD').isValid() ) 
							&& ( (!$scope.specialDates.from || $scope.specialDates.to) &&  $scope.specialDates.all_day == '0' )) 
						){
						swal({
							buttonsStyling: false,
							confirmButtonClass: 'btn btn-primary btn-sm ghost',
							title: 'Oops!',
							text: 'Giorno non valido',
							type: 'error'
						});
						return;
					}


					RestaurantsServices.setSpecialDate
					.async($scope.restaurant._id,
						$scope.specialDates.type,
						$scope.specialDates.day,
						$scope.specialDates.from,
						$scope.specialDates.to,
						$scope.specialDates.all_day).success(function(data){
							if(data.message == 'ok'){
								if(!$scope.restaurant.special_dates){
									$scope.restaurant.special_dates = []
								}
								var n = angular.copy($scope.specialDates);
								$scope.restaurant.special_dates.push(n)
							}								
					})
				}

				$scope.removeSpecialDate = function(obj){
					RestaurantsServices.removeSpecialDate
					.async($scope.restaurant._id,
						obj.type,
						obj.day,
						obj.from,
						obj.to,
						obj.all_day).success(function(data){
							
							var index = $scope.restaurant.special_dates.indexOf(obj);
							$scope.restaurant.special_dates.splice(index,1);
						
					})
				}








				$scope.priceChange = function(){
					
					$scope.restaurant.price = (parseInt($scope.restaurant.priceMin) + parseInt($scope.restaurant.priceMax)) / 2
					setPriceEuros();
					
				}
				

				var setPriceEuros = function() {
            
		            var start = ($scope.restaurant.price) 
		            ? parseInt($scope.restaurant.price) : 
		            parseInt( ( parseInt($scope.restaurant.priceMin) + parseInt($scope.restaurant.priceMax) ) / 2);
		            $scope.price_euro_add = []
		            
		            var n = 0;
		            if(start < 20){
		                n = 1;
		            } if(start >= 20 && start < 40){
		                n = 2;
		            } else
		            if
		            (start >= 40 && start < 80){
		                n = 3;
		            } else
		            if(start >= 80 && start < 150){
		                n = 4;
		            } else
		            if(start > 150){
		                n = 5;
		            } 
		            for (x = 0; x < n; x++) {
		                $scope.price_euro_add.push("€");
		            }
		        }
		        setPriceEuros();




				

				$scope.hasiframes = 0;
				$scope.initWidget = function(){
					IframeServices.viewByRestaurant.async($scope.restaurant._id).success(function(data){
						if(data.iframes.length > 0){
							// ha già aggiunto iframe 
							$scope.hasiframes = 1;
							$scope.iframe = data.iframes[0];
							$scope.url_token = $sce.trustAsResourceUrl(/static/+$scope.iframe.token+'/frameloader')
						}
					});

					$scope.add_iframe = {
						url: '',
						restaurant: $scope.restaurant._id
					}
					$scope.addIframe = function(){
						if($scope.hasiframes == 1){
							// aggiungi solo url
							var obj = { 
								url: $scope.add_iframe.url,
								restaurant: $scope.restaurant._id, 
								iframe: $scope.iframe._id
							}
							IframeServices.addSingleUrl.async(obj).success(function(data){
								if(data.message == 'ok'){
									$scope.hasiframes = 1;
									$scope.iframe = data.iframe
								}								
							})
						} else {
							IframeServices.addSingle.async($scope.add_iframe).success(function(data){
								if(data.message == 'ok'){
									$scope.hasiframes = 1;
									$scope.iframe = data.iframe
								}								
							})
						}
					}
				}



				$scope.initPlans = function(){
					$scope.selected_plan_period = parseInt(0);

					$scope.returnCicleText = function(i){
						return ($scope.selected_plan_period == 0) ? 'Mensile' : 'Annuale'
					}
					$scope.setPlanPeriod = function(n){
						$scope.selected_plan_period = n;
					}

					$scope.submitPlan = function(plan){
						ToolsServices.mainOverlay.show();
						PaymentServices.subscribePlan.async(plan,$scope.selected_plan_period).success(function(data){
							if(data.redirect){
								window.location = data.redirect;
							} else {
								ToolsServices.mainOverlay.hide();
							}
						}).error(function(d){
							ToolsServices.mainOverlay.hide();
							$scope.simpleError();
						})
					}
				}




				$scope.simpleError = function(){
					swal({
						buttonsStyling: false,
						confirmButtonClass: 'btn btn-primary btn-sm ghost',
						title: 'Oops!',
						text: 'Qualcosa è andato storto',
						type: 'error'
					});
				}

				$scope.open_editing = 0;
				$scope.openEditing = function(n){
					$scope.open_editing = n;
				}

			});


				

	}


});