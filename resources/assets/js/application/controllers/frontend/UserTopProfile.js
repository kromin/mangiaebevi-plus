var UserTopProfileController = angular.module('AppUserTopProfileController',[]);

UserTopProfileController
.controller('UserTopProfileCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams,ngDialog,alertify,UserServices, SocialServices, ToolsServices){
	"ngInject";

	$scope.user = null;
	$scope.init = function(user){
		$scope.user = user;

		$scope.isFollowing();
		$scope.canAddToFriends();
	};

	$scope.following = 2;
	$scope.can_add = 2;

	$scope.isFollowing = function(){
		if(!$scope.user) return;
		SocialServices.isFollowing.async($scope.user).success(function(data){
			$scope.following = data.following;
		});
	};

	$scope.canAddToFriends = function(){
		if(!$scope.user) return;
		SocialServices.canAddToFriends.async($scope.user).success(function(data){
			$scope.can_add = data.can_add;
		});
	};	

	
	$scope.addRemoveFollowing = function(){
		var current_followers = parseInt( angular.element("#followers_num").text() );
		SocialServices.addRemoveFollowing.async($scope.user).success(function(){
			if($scope.following == 1){
				angular.element("#followers_num, #followers_num_string").text( current_followers-1 );
				$scope.following = 0;
			} else {
				angular.element("#followers_num, #followers_num_string").text( current_followers+1);
				$scope.following = 1;
			}
			
		});
	};


	$scope.addToFriends = function(){
		var current_friends = parseInt( angular.element("#friends_num").text() );
		SocialServices.addFriendRequest.async($scope.user).success(function(){
			swal({
				buttonsStyling: false,
				confirmButtonClass: 'btn btn-primary btn-sm ghost',
				title: 'Confermato!',
				text: 'La richiesta di amicizia è stata inoltrata',
				type: 'success'
			});
			$scope.can_add = 2;
		});
	};

	$scope.edit_user = {
		current_address: ''
	};

	$scope.setEditAddress = function(){
		ngDialog.open({
            template: '/templates/dialogcurrentposition',
            scope: $scope,
            className: 'ngdialog ngdialog-theme-default'
        });
	};

	$scope.editAddress = function(){
		// address == $scope.view_user.current_address
		ToolsServices.mainOverlay.show();
		UserServices.addCurrentAddress.async($scope.user,$scope.edit_user.current_address).success(function(data){
			$scope.editing_address = !$scope.editing_address;
			ToolsServices.mainOverlay.hide();
			angular.element("#user_current_address").text($scope.edit_user.current_address);
			ngDialog.close();
		}).error(function(){ 
			$scope.simpleError();
			ToolsServices.mainOverlay.hide();
			ngDialog.close();
		});
	};

	$scope.simpleError = function(){
		swal({
			buttonsStyling: false,
			confirmButtonClass: 'btn btn-primary btn-sm ghost',
			title: 'Oops!',
			text: 'Qualcosa è andato storto',
			type: 'error'
		});
	};


	$scope.uploading_avatar = false;
	$scope.change_avatar = {
		avatar: ''
	};
	$scope.uploadAvatar = function(){
		ToolsServices.mainOverlay.show();
		
		if($scope.change_avatar.avatar){
		
			UserServices
			.modAvatar
			.async($scope.user,$scope.change_avatar.avatar)
			.success(function(data){
				$scope.change_avatar = {};
				$scope.$parent.current_user.user.avatar = data.image;
				$scope.uploading_avatar = false;
				ToolsServices.mainOverlay.hide();

				angular.element("#avatar_container").attr("src",data.image_url);
			}).error(function(data){
				$scope.simpleError();
				ToolsServices.mainOverlay.hide();
			});
		}
	};

	$scope.change_cover = {
		cover: ''
	};
	$scope.uploadCover = function(){ 
		ToolsServices.mainOverlay.show();
		if($scope.change_cover.cover){
			
			UserServices
			.modCover
			.async($scope.user,$scope.change_cover.cover)
			.success(function(data){
				$scope.change_cover = {
					cover: ''
				};
				angular.element("#main_cover").css("background-image","url("+data.image_url+")");
				ToolsServices.mainOverlay.hide();
			}).error(function(){
				ToolsServices.mainOverlay.hide();
			});
		}
	};

});