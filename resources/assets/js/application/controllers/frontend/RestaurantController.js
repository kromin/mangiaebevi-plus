var RestaurantController = angular.module('AppRestaurantController', []);

RestaurantController
    .controller('RestaurantCtrl', function($scope, $rootScope, $cookies, $sce, $location, $route, $routeParams, $document, ngDialog, RestaurantsServices, UserServices, ReservationServices, SearchServices, ToolsServices, RecommendationServices, Analytics) {
        "ngInject";
        /**
         * Prendo da ApplicationController
         *
         * current_restaurant
         *
         * questo lo faccio diventare da evento
         ***/

        $scope.current_restaurant = {
            _id: '',
            fasce: []
        };

        $scope.SetRestaurantOrari = function(restaurant){

            RestaurantsServices.getRestaurantFasce.async(restaurant).success(function(data){
                $scope.current_restaurant.fasce = data.fasce;
                $scope.setCurrentTimeOpen();
                $scope.setRestaurantHours();
            });

        };


        $scope.initPage = function(id){

            $scope.current_restaurant._id = id;


            Analytics.trackPage('/restaurant/'+$scope.current_restaurant._id, ''+$scope.current_restaurant._id);
            // prenotazione
            if (!$scope.current_restaurant.time_shift) {
                $scope.current_restaurant.time_shift = 30;
            }



            // imposto qui il related di default in modo da evitare doppie chiamate
            $scope.selectMenu(0);
            //$scope.initialize_map();
        };


        $scope.$on('page loaded',function(){
            $scope.$broadcast('initializeMap', $scope.map_restaurants);
        });


        $scope.pushMapObj = function(name,image,address,lng,lat,slug,city){
            $scope.map_restaurants.push({
                name: name,
                slug: slug,
                city: city,
                image: (image && image !== '') ? image : null,
                address: address,
                loc: {
                    coordinates:[
                        parseFloat(lat),parseFloat(lng)
                    ]
                }
            });
        };


        // espandi il virtual tour
        $scope.show_virtual_tour = false;
        $scope.showTour = function() {
            $scope.show_virtual_tour = ($scope.show_virtual_tour) ? false : true;
            if ($scope.show_virtual_tour) {
                $("#topbar").hide();
            } else {
                $("#topbar").show();
            }
        };

        $scope.description_opened =false;
        $scope.open_description = function() {
            $scope.description_opened = !$scope.description_opened;
        };

        $scope.gallery = [];
        $scope.initGalleryImages = function(i){
            $scope.gallery = i;
        };


        /**
         *  Favourite e wishlist
         *
         */
        $scope.favourite = function() {
            var user = UserServices.isCurrentLogged.isLoggedCurrently;
            if (user && user.loggedStatus == 1) {
                $scope.current_user = user;
                UserServices.addRemoveFavourite.async($scope.current_restaurant._id).success(function(data) {
                    if (data.message == 'ok') {
                        var index = ($scope.current_user.user.favourite) ? $scope.current_user.user.favourite.indexOf($scope.current_restaurant._id) : -1;
                        if (index == -1) {
                            $scope.current_user.user.favourite.push($scope.current_restaurant._id);
                        } else {
                            $scope.current_user.user.favourite.splice(index, 1);
                        }

                    }
                });
            } else {
                // apri dialog login/registrazione
                var dialog = $scope.dialogLogin(1);
                dialog.closePromise.then(function(data) {
                    switch (data.value) {
                        case 'registerSubmitted':
                            swal({
                                buttonsStyling: false,
                                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                                title: 'Congratulazioni',
                                text: 'Ora che sei registrato attiva il tuo account e potrai aggiungere il ristorante ai preferiti',
                                type: 'success'
                            });
                        break;
                        case 'loginSubmitted':
                            $scope.favourite();
                            break;
                    }
                });
            }
        };

        $scope.wishlist = function() {

            console.log($scope.current_user.user.wishlist);
            var user = UserServices.isCurrentLogged.isLoggedCurrently;
            if (user && user.loggedStatus == 1) {
                $scope.current_user = user;

                UserServices.addRemoveWishlist.async($scope.current_restaurant._id).success(function(data) {
                    if (data.message == 'ok') {
                        var index = ($scope.current_user.user.wishlist) ? $scope.current_user.user.wishlist.indexOf($scope.current_restaurant._id) : -1;
                        if (index == -1) {
                            $scope.current_user.user.wishlist.push($scope.current_restaurant._id);
                        } else {
                            $scope.current_user.user.wishlist.splice(index, 1);
                        }
                    }
                });
            } else {
                // apri dialog login/registrazione
                var dialog = $scope.dialogLogin(1);
                dialog.closePromise.then(function(data) {
                    switch (data.value) {
                        case 'registerSubmitted':
                        swal({
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary btn-sm ghost',
                            title: 'Congratulazioni',
                            text: 'Ora che sei registrato attiva il tuo account e potrai aggiungere il ristorante a quelli da provare',
                            type: 'success'
                        });
                        break;
                        case 'loginSubmitted':
                            $scope.wishlist();
                            break;
                    }
                });
            }
        };


        $scope.recommendRestaurant = function() {
            var user = UserServices.isCurrentLogged.isLoggedCurrently;
            if (user && user.loggedStatus == 1) {
                if(!user.user.loc || !user.user.loc.coordinates){
                    swal({
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-primary btn-sm ghost',
                        title: 'Oops!',
                        text: 'Devi completare il tuo profilo per raccomandare il ristorante',
                        type: 'error'
                    });
                } else {
                    $scope.current_user = user;
                    $scope.dialog_community_recommendation = ngDialog.open({
                        template: '/templates/dialogrecommend',
                        scope: $scope,
                        className: 'ngdialog ngdialog-theme-default'
                    });
                }
            } else {
                // apri dialog login/registrazione

                var dialog = $scope.dialogLogin(1);
                dialog.closePromise.then(function(data) {
                    switch (data.value) {
                        case 'registerSubmitted':
                        swal({
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary btn-sm ghost',
                            title: 'Congratulazioni',
                            text: 'Ora che sei registrato attiva il tuo account e completa il profilo per raccomandare il ristorante',
                            type: 'success'
                        });
                        break;
                        case 'loginSubmitted':
                            $scope.recommendRestaurant();
                            break;
                    }
                });
            }
        };

        $scope.recommendCommunityFormData = {
            address: null,
            text: '',
            type: 0
        };
        $scope.publishRecommendation = function(){
            $scope.recommendCommunityFormData.ref = $scope.current_restaurant._id;
            RecommendationServices.publishRecommendation.async($scope.recommendCommunityFormData).success(function(data){
                $scope.current_restaurant.recommendations = $scope.current_restaurant.recommendations+1;
                $scope.simpleSuccess('Congratulazioni','Raccomandazione confermata');
                $scope.dialog_community_recommendation.close();
            }).error(function(){
                swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Oops!',
                    text: 'Si è verificato un errore, ricorda che non puoi effettuare più di una raccomandazione generica per lo stesso ristorante',
                    type: 'error'
                });
            });
        };



        // Reservation

        var hour = moment().hour(moment().add(1, 'hours').format('HH')).minute(0);
        $scope.reservation = {
            day: moment().format('DD'),
            month: moment().format('MMMM'),
            year: moment().format('YYYY'),
            hour: '',
            orario: null,
            date: null,
            people: "2"
        };

        // return hour not integer
        $scope.formatIntHour = function(intero) {
                return (intero < 10) ? '0' + intero : '' + intero;
            };
            //moment().isValid();


        $scope.people = [];
        for (var x = 1; x <= 20; x++) {
            $scope.people.push(x);
        }

        $scope.open = false;
        $scope.fasce_oggi = [];
        $scope.setCurrentTimeOpen = function() {
            var weekday = moment().weekday(),
                now = moment(),
                open = false;
            if ($scope.current_restaurant.fasce) {
                angular.forEach($scope.current_restaurant.fasce[weekday], function(fascia, indice) {
                    // fascia oraria per mostrare oggi aperto
                    $scope.fasce_oggi.push(fascia);
                    if (!open) {
                        var orari = fascia.split("-");

                        var min = orari[0].split(":"),
                            max = orari[1].split(":");


                        var current_min = moment(),
                            current_max = moment();
                        current_min.hour(min[0]).minute(min[1]);
                        current_max.hour(max[0]).minute(max[1]);

                        if (now >= current_min && now <= current_max) {
                            open = true;
                            $scope.open = open;
                        }
                    }
                });
                $scope.open = open;
            }
        };
        $('#datetimepicker-day').livequery(function() {
            $(this).datetimepicker({
                locale: 'it',
                //useCurrent: true,
                defaultDate: moment(),
                format: 'DD/MM/YYYY'
            }).on("dp.change", function(e) {
                $scope.takeAvaibleFasce();
            });
        });


        $scope.isOpenRestaurant = function(){
            var return_val = false;
            var weekday = moment().weekday(),
                now = moment();
            if($scope.current_restaurant){
                if ($scope.current_restaurant.fasce) {
                    angular.forEach($scope.current_restaurant.fasce[weekday], function(fascia, indice) {
                        // fascia oraria per mostrare oggi aperto


                        var orari = fascia.split("-");

                        var min = orari[0].split(":"),
                            max = orari[1].split(":");


                        var current_min = moment(now.format('YYYYMMDD')+orari[0],"YYYYMMDDHHmm"),
                            current_max = moment(now.format('YYYYMMDD')+orari[1],"YYYYMMDDHHmm");

                        if(current_max < current_min){
                            current_max.add(1,'days');
                        }

                        if(now >= current_min && now <= current_max){
                            return_val = true;
                        }

                        /*current_min.hour(min[0]).minute(min[1]);
                        current_max.hour(max[0]).minute(max[1]);

                        if (now >= current_min && now <= current_max) {
                            open = true
                            $scope.open = open;
                        }*/

                    });

                }
            }
            return return_val;
        };



        $scope.avaible_fasce = [];
        $scope.takeAvaibleFasce = function(first,restaurant){
            if(($scope.current_restaurant._id === '' || !$scope.current_restaurant._id) && !restaurant) return;

            var date = moment().format('YYYY-MM-DD');
            if(!first){
                var day = $("#datetimepicker-day").val().substr(0, 2),
                    month = $("#datetimepicker-day").val().substr(3, 2),
                    year = $("#datetimepicker-day").val().substr(6, 4);
                date = year+'-'+month+'-'+day;
            }

            var risto = (restaurant) ? restaurant : $scope.current_restaurant._id;
            ReservationServices.getRestaurantAvaibleFasce.async(risto,date,$scope.reservation.people).success(function(f){
                if(f.fasce.length > 0){
                    $scope.restaurant_reservation_date = moment(date, "YYYY-MM-DD");
                    $scope.avaible_fasce = f.fasce;
                    $scope.reservation.hour = f.fasce[0];
                    $("#reservation_btn").removeClass("disabled");
                    $scope.valid_reservation = true;
                } else {
                    $("#reservation_btn").addClass("disabled");
                    $scope.avaible_fasce = f.fasce;
                    $scope.valid_reservation = false;
                }

            });
        };


        $scope.reservation_confirmed = false;
        $scope.reservation_as_to_add_phone = false;
        $scope.reserve = function() {
            var dialog = null;
            if ($scope.valid_reservation) {

                var persona = ($scope.reservation.people == 1) ? 'a' : 'e';
                var user = UserServices.isCurrentLogged.isLoggedCurrently;

                if(!user.user){
                    dialog = $scope.dialogLogin(1);
                    dialog.closePromise.then(function(data) {


                        switch (data.value) {
                            case 'registerSubmitted':
                                swal({
                                buttonsStyling: false,
                                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                                title: 'Congratulazioni',
                                text: 'Ora che sei registrato attiva il tuo account e potrai prenotare al ristorante',
                                type: 'success'
                            });
                                break;
                            case 'loginSubmitted':
                                $scope.reserve();
                                break;
                        }
                    });
                    return;
                }

                if( (user.user && (user.user.phone === '' || !user.user.phone)) || !user.user ){
                    $scope.reservation_as_to_add_phone = true;
                    return;
                } else {
                    $scope.reservation_as_to_add_phone = false;
                }

                if (user && user.loggedStatus == 1) {
                    // utente loggato, può prenotare
                    //console.log('data',$scope.restaurant_reservation_date.format("YYYY-MM-DD"))
                    ToolsServices.mainOverlay.show();
                    ReservationServices.askReserve
                        .async($scope.current_restaurant._id,
                            $scope.restaurant_reservation_date.format("YYYY-MM-DD"),
                            $scope.reservation.hour,
                            user.user.email,
                            $scope.reservation.people,
                            user.user.name + ' ' + user.user.surname,
                            user.user.phone)
                        .success(function(data) {
                            if (data.message == 'ok') {

                                var addMessage = ($scope.canpreorder == 1) ?
                                '. <br /><br />Vuoi preordinare il tuo pasto? <a href="/preorder/'+$scope.current_restaurant.slug+'/'+data.reservation._id+'/">Clicca qui</a>' :
                                '';
                                swal({
                                    buttonsStyling: false,
                                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                                    title: 'Congratulazioni',
                                    text: 'Prenotazione confermata, segui sul tuo profilo l\'approvazione da parte del ristoratore'+addMessage,
                                    type: 'success'
                                });
                            }
                            ToolsServices.mainOverlay.hide();
                        }).error(function(e){
                            ToolsServices.mainOverlay.hide();
                            swal({
                                    buttonsStyling: false,
                                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                                    title: 'Spiacenti',
                                    text: 'Si è verificato un errore inaspettato, riprova più tardi',
                                    type: 'error'
                                });
                        });
                } else {
                    // utente non loggato, mostro dialog login / registrazione
                    dialog = $scope.dialogLogin(1);
                    dialog.closePromise.then(function(data) {
                        switch (data.value) {
                            case 'registerSubmitted':
                                swal({
                                    buttonsStyling: false,
                                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                                    title: 'Congratulazioni',
                                    text: 'Ora che sei registrato attiva il tuo account e potrai prenotare al ristorante',
                                    type: 'success'
                                });
                            break;
                            case 'loginSubmitted':
                                $scope.reserve();
                                break;
                        }
                    });

                }
            } else {
                //alert($scope.valid_reservation)
            }
        };

        /**
         * Imposta se è possibile preordinare
         * @type {Number}
         */
        $scope.canpreorder = 0;
        $scope.canPreOrder = function(set, slug){
            $scope.canpreorder = set;
            $scope.current_restaurant.slug = slug;
        }

        $scope.user_phone = {
            phone: ''
        };
        $scope.addPhoneNumber = function(){
            var user = UserServices.isCurrentLogged.isLoggedCurrently;
            if(user.loggedStatus !== 0 ){
                if($scope.user_phone.phone && $scope.user_phone.phone !== ''){
                    UserServices.changeInfo
                    .async(user.user._id,user.user.name,user.user.surname,$scope.user_phone.phone,user.user.description)
                    .success(function(data){
                        if(data.message == 'ok'){
                            UserServices.getLogged.async().success(function(data) {
                                UserServices.isCurrentLogged.set(data);
                                $scope.current_user = data;
                                $scope.reserve();
                            });
                        }
                    });
                }
            } else {
                return;
            }
        };

        /**
         *
         * Imposta orari del ristorante formattati
         *
         *
         ***/
        $scope.restaurant_hours = [];
        $scope.setRestaurantHours = function() {
            $scope.restaurant_hours = [];
            angular.forEach($scope.current_restaurant.fasce, function(val, key) {

                var dayname = moment().isoWeekday(key + 1).format('dddd'); // nome del giorno
                var obj = {
                    name: dayname,
                    fasce: []
                };

                angular.forEach(val, function(fascia, chiave) {
                    var orari = fascia.split("-");
                    obj.fasce.push({
                        min: orari[0],
                        max: orari[1]
                    });

                });
                $scope.restaurant_hours.push(obj);
            });

        };

        $scope.viewHours = function() {
            $scope.setRestaurantHours();
            ngDialog.open({
                template: '/templates/dialogrestauranthours',
                scope: $scope,
                className: 'ngdialog ngdialog-theme-default orari'
            });
        };



        // Menu
        $scope.currentMenu = 0; // primo menu selezionato

        $scope.scrollToMenu = function() {
            var menuElement = angular.element(document.getElementById('menuContainer'));
            $document.scrollToElement(menuElement, 100, 500);
        };

        $scope.goToVideo = function(){
            var videoElement = angular.element(document.getElementById('restaurant_video'));
            $document.scrollToElement(videoElement, 100, 500);
        };



        /**
        	Gallery

        **/
        $scope.current_image_index = 0;
        $scope.gallery_opened = false;
        $scope.openGallery = function(image_index) {
            if ($scope.gallery.length < (image_index - 1) || image_index < 0) {
                $scope.current_image_index = 0;
            } else {
                $scope.current_image_index = image_index;
            }
            $scope.gallery_opened = true;
        };

        $scope.closeGallery = function() {
            $scope.gallery_opened = false;
        };

        $scope.nextImage = function() {
            if (($scope.current_image_index + 2) > $scope.gallery.length) {
                $scope.current_image_index = 0;
            } else {
                $scope.current_image_index++;
            }
        };

        $scope.prevImage = function() {
            if ($scope.current_image_index === 0) {
                $scope.current_image_index = ($scope.gallery.length - 1);
            } else {
                $scope.current_image_index--;
            }
        };


        /**
         * Menu gallery
         */
        $scope.menu_gallery_opened = false;
        $scope.current_menu_gallery_image = '';
        $scope.openMenuGallery = function(i){
            $scope.current_menu_gallery_image = i;
            $scope.menu_gallery_opened = true;
        }
        $scope.closeMenuGallery = function(){
            $scope.current_menu_gallery_image = '';
            $scope.menu_gallery_opened = false;
        }



        $scope.claimFormData = {};
        $scope.claim = function() {
            if ($scope.current_user && $scope.current_user.loggedStatus == 1) {
                ngDialog.open({
                    template: '/templates/dialogclaim',
                    scope: $scope
                });
            } else {
                var dialog = $scope.dialogLogin(1);
                dialog.closePromise.then(function(data) {
                    switch (data.value) {
                        case 'registerSubmitted':
                        swal({
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary btn-sm ghost',
                            title: 'Congratulazioni',
                            text: 'Ora che sei registrato attiva il tuo account e potrai reclamare il ristorante',
                            type: 'success'
                        });
                        break;
                        case 'loginSubmitted':
                            $scope.claim();
                            break;
                    }
                });
            }
        };

        $scope.closeDialog = function() {
            ngDialog.close();
        };

        $scope.claimRestaurant = function(formData) {

            if ($scope.current_user && $scope.current_user.loggedStatus == 1) {

                RestaurantsServices.claim.async($scope.current_restaurant._id).success(function(data) {
                    ngDialog.close();
                    swal({
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary btn-sm ghost',
                            title: 'Congratulazioni',
                            text: 'A breve riceverai un feedback relativo alla tua richiesta',
                            type: 'success'
                        });
                });

            } else {
                // prima registra poi fai il claim
                swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Oops!',
                    text: 'Qualcosa è andato storto',
                    type: 'error'
                });

            }
        };

        $scope.reserveRestaurant = function(formData) {
            var date = $scope.reservation.year + '-' + $scope.reservation.month + '-' + $scope.reservation.day;
            var time = $scope.reservation.hour;

            if ($scope.current_user && $scope.current_user.loggedStatus == 1) {

                ReservationServices.askReserve.async($scope.current_restaurant._id, date, time, $scope.current_user.user.email, formData.people, $scope.current_user.user.name + ' ' + $scope.current_user.user.surname, $scope.current_user.user.phone).success(function(data) {
                    ngDialog.close();
                });

            } else {
                // prima registra poi fai il reserve

                dialog = $scope.dialogLogin(1);
                dialog.closePromise.then(function(data) {
                    switch (data.value) {
                        case 'registerSubmitted':
                            swal({
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary btn-sm ghost',
                            title: 'Congratulazioni',
                            text: 'Ora che sei registrato attiva il tuo account e potrai prenotare al ristorante',
                            type: 'success'
                        });
                            break;
                        case 'loginSubmitted':
                            $scope.reserveRestaurant();
                            break;
                    }
                });

            }
        };


        $scope.escapeRestaurantName = function(name){
            return name.replace(/'/g, "\\'");
        };

        $scope.reccommend = function(provider,url,name,ogimage) {

            var add_user_c = '';
            if ($scope.current_user && $scope.current_user.loggedStatus == 1) {

                // salva la condivisione nel db
                add_user_c = $scope.current_user.user._id + '/';

            }


            url = url + add_user_c;
            var text = 'Vieni a scoprire il ristorante '+name.replace(/'/g, "\\'")+' su MangiaeBevi+';
            //var ogimage = ($scope.current_restaurant.image) ? $scope.staticURL + '/restaurant/big/' + $scope.current_restaurant.image + '.png' : null;

            switch (provider) {
                case 'facebook_send':
                    FB.ui({
                        method: 'send',
                        link: '' + url,
                    });
                    break;
                case 'facebook_share':
                    FB.ui({
                        method: 'feed',
                        picture: ogimage,
                        name: 'Menoo - ' + name.replace(/'/g, "\\'"),
                        description: text,
                        caption: 'Il portale dei ristoranti in Italia',
                        redirect_uri: '' + url,
                        link: '' + url
                    });
                    break;
                case 'twitter':
                    window.open('https://twitter.com/share?text=' + text + '&url=' + url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
                    break;
                case 'google_plus':
                    window.open('https://plus.google.com/share?url=' + url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                    break;
                case 'linkedin':
                    window.open('https://linkedin.com/shareArticle?mini=false&url=' + url + '&title=MangiaeBevi+ - ' + name.replace(/'/g, "\\'") + '&summary=' + text + '&source=' + url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                    break;
                case 'email':
                    break;
            }


        };



        $scope.mailReccomendOpenDialog = function() {

            $scope.reccomendDialog = ngDialog.open({
                template: '/templates/dialogmailrecommend',
                scope: $scope,
                className: 'ngdialog ngdialog-theme-default'
            });

        };

        $scope.reccomendFormData = {};
        $scope.reccomendMail = function() {
            $scope.reccomendFormData.restaurant = $scope.current_restaurant._id;
            ToolsServices.reccomendMail.async($scope.reccomendFormData).success(function(data) {
                if (data.message == 'ok') {
                    $scope.reccomendDialog.close();
                }
            });
        };


        /***
         *
         * Ristoranti correlati
         * Inizializza i ristoranti correlati
         *
         **/

        $scope.getRelated = function() {
            if ($scope.current_restaurant) {
                /**
                 * Inserisce il ristorante corrente nella mappa
                 */

                SearchServices.searchRelated.async($scope.current_restaurant._id).success(function(data) {
                    if (data.message == 'ok') {
                        /**
                         * Inserisce i correlati nella mappa
                         */
                        $scope.map_r = [];
                        $scope.related = data.restaurants;
                        angular.forEach(data.restaurants,function(v,k){
                            if(v.obj){
                                $scope.map_r.push(v.obj);
                            } else {
                                $scope.map_r.push(v);
                            }
                        });

                    }
                    /**
                     * Richiama Evento initializeMap
                     */
                    $scope.map_r.unshift($scope.current_restaurant);
                    //console.log('map restaurants',$scope.map_r);
                    $rootScope.$broadcast('initializeMap', $scope.map_r);

                    return;

                }).error(function() {
                    $rootScope.$broadcast('initializeMap', [$scope.current_restaurant]);
                });
            }
        };

        /**
         *
         * determina se ristorante cliente
         *
         */
        $scope.isClient = function() {
            return ($scope.current_restaurant.user && $scope.current_restaurant.user.length > 0) ? true : false;
        };





        /**
         *
         *  Google Map
         *
         *
         */
        $scope.map_restaurants = [];
        $scope.initialize_map = function(array_restaurant) {

            angular.forEach(array_restaurant, function(restaurant, key) {
                if (restaurant.obj) {
                    $scope.map_restaurants.push(restaurant.obj);
                } else {
                    $scope.map_restaurants.push(restaurant);
                }
            });
        };



        // Menu
        $scope.visual_menu = 0;
        $scope.currentMenu = null; // primo menu selezionato

        $scope.selectMenu = function(indice) {

            $scope.currentMenu = indice;
                if($scope.current_restaurant.menu && $scope.current_restaurant.menu.menus){
                    if($scope.current_restaurant.menu.menus && $scope.current_restaurant.menu.menus.length > 0){
                        /**
                         * Impostazioni per visualizzazione menu testuale/visuale
                         */
                    $scope.menu_type = 0; // imposto di default quello testuale
                    angular.forEach($scope.current_restaurant.menu.menus[indice].categorie, function(v, k) {
                        angular.forEach(v.piatti, function(plate, key) {
                            //console.log('plate', plate);
                            if (plate.image && plate.image !== null) {
                                $scope.visual_menu = 1;
                                return;
                            }
                        });
                    });
                }
            }
        };

        $scope.scrollToMenu = function() {
            var menuElement = angular.element(document.getElementById('menuContainer'));
            $document.scrollToElement(menuElement, 100, 500);
        };

        $scope.menu_type = 0;
        $scope.setMenuType = function(type) {
            $scope.menu_type = type;
        };

        $scope.simpleError = function(){
            swal({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                title: 'Oops!',
                text: 'Qualcosa è andato storto',
                type: 'error'
            });
        };

        $scope.simpleSuccess = function(title,text){
            swal({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                title: title,
                text: text,
                type: 'success'
            });
        };




        /**
        * Raccomandazioni
        */
        $scope.scrollToRec = function() {
            var recElement = angular.element(document.getElementById('recContainer'));
            $document.scrollToElement(recElement, 100, 500);
        };

        $scope.rec_pagination = 1;
        $scope.pagination_number = 10;
        $scope.restaurant_rec = [];
        $scope.loadRestaurantRecommendations = function(ref,rec){
            $scope.stop_load_rec_pagination = true;
            if(rec === 0) return;
            var load_rec_obj = {
                ref: ref,
                skip: ($scope.rec_pagination == 1) ? 0 : parseInt($scope.rec_pagination-1)*parseInt($scope.pagination_number),
                take: parseInt($scope.pagination_number)
            };
            RecommendationServices.viewByRestaurant.async(load_rec_obj).success(function(data){

                angular.forEach(data.recommendation,function(v,k){
                    $scope.restaurant_rec.push(v);
                });
                if(data.recommendation.length < $scope.pagination_number){
                    $scope.stop_load_rec_pagination = true;
                } else {
                    $scope.rec_pagination++;
                    $scope.stop_load_rec_pagination = false;
                }
            });

        };

        $scope.loadRecommendation = function(){
            $scope.loadRestaurantRecommendations();
        };




        /**
        * Segnalazione errore
        */
        $scope.segnalazione = {
            subject: 3,
            name: ($scope.current_user.user) ? $scope.current_user.user.name+' '+$scope.current_user.user.surname : '',
            email: ($scope.current_user.user) ? $scope.current_user.user.email : '',
            phone: ($scope.current_user.user && $scope.current_user.user.phone) ? $scope.current_user.user.phone : '',
            message: ''
        };
        $scope.segnalaProblema = function(){
            $scope.segnalazionedialog = ngDialog.open({
                template: '/templates/dialogsegnalazione',
                scope: $scope,
                className: 'ngdialog ngdialog-theme-default'
            });
        };

        $scope.confirmSegnalazione = function(){
            ToolsServices.mainOverlay.show();
            ToolsServices.contact.async($scope.segnalazione).success(function(data){
                if(data.message == 'ok'){
                    swal({
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-primary btn-sm ghost',
                        title: 'Grazie!',
                        text: 'Il messaggio è stato inviato',
                        type: 'success'
                    });
                    $scope.segnalazionedialog.close();
                }
                ToolsServices.mainOverlay.hide();
            }).error(function(e){
                swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Oops',
                    text: 'Si è verificato un errore',
                    type: 'error'
                });
                ToolsServices.mainOverlay.hide();
            });
        };

    });