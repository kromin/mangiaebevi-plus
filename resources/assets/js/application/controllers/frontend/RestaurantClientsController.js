var ClientsRestaurantController = angular.module('AppClientsRestaurantController',[]);

ClientsRestaurantController 
.controller('ClientsRestaurantCtrl', function($scope,$rootScope,$cookies,$sce,$location,$routeParams,$route,$document,$timeout,ngDialog,RestaurantsServices,alertify,ReservationServices, ToolsServices){
	"ngInject";
	
	$scope.current_client = 0;
	$scope.clients = [];
	$scope.viewed_clients = [];
	
	$scope.allergeni = ToolsServices.allergeni.retrieve();
	$scope.risto_id = 0;
	$scope.current_visualizzazione = 1;
	$scope.filtered_results = 1;

	$scope.InitRestaurantClients = function(restaurant_id){
		$scope.risto_id = restaurant_id;
		RestaurantsServices.getRestaurantClients.async(restaurant_id).success(function(data){
			$scope.clients = data.clients;
			setVisibleClients();
		});
	};
 
	$scope.current_page = 1;
	$scope.page_results = 10;
	$scope.page_num = 1;
	var setVisibleClients = function(){
		var n = $scope.clients.length;
		if( n%$scope.page_results == 0){
			$scope.page_num = n/$scope.page_results;
		} else {
			$scope.page_num = parseInt(n/$scope.page_results) +1;
		}

		setPages();

		$scope.viewed_clients = [];
		var start = ($scope.current_page*$scope.page_results)-$scope.page_results;
		var end = start+$scope.page_results;
		for(var x = start;x < end;x++){
			if($scope.clients[x]) {
				$scope.viewed_clients.push($scope.clients[x])
			}
		}
	}

	$scope.setVisualizzazione = function(n){
		if(n == $scope.current_visualizzazione) return;

		if(n == 1) setVisibleClients();
		$scope.current_visualizzazione = n;

	}

	var setPages = function(){
		$scope.pages = [];
		for(var x = 1;x <= $scope.page_num; x++){
			$scope.pages.push(x);
		}
	}

	$scope.setPage = function(n){
		if(n != $scope.current_page && n > 0){
			$scope.current_page = n;
			setVisibleClients();
		}
	}


	$scope.add_client = {
		name: '',
		surname: '',
		email: '',
		phone: '',
		phone_: '',
		address: '',
		cap: '',
		city: '',
		pr: '',
		state: '',
		dt: '',
		language: '',
		allergeni: [],
		preferenze: [],
		note: ''
	}
	$scope.add_client_form_show = 0;
	$scope.addClientForm = function(n){
		$scope.current_client = 0;
	}

	$scope.confirmAdd = function(){
		RestaurantsServices.addCustomClient.async($scope.risto_id,$scope.add_client).success(function(data){
			if(data.message == 'ok'){
				$scope.clients.push(data.client);
				setVisibleClients();
				$scope.add_client = {
					name: '',
					surname: '',
					email: '',
					phone: '',
					phone_: '',
					address: '',
					cap: '',
					pr: '',
					state: '',
					dt: '',
					language: '',
					allergeni: [],
					preferenze: [],
					note: ''
				}
				$scope.add_client_form_show = 0;
			} else {
				swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Oops!',
					text: 'Qualcosa è andato storto',
					type: 'error'
				});
			}
		}).error(function(){
			swal({
				buttonsStyling: false,
				confirmButtonClass: 'btn btn-primary btn-sm ghost',
				title: 'Oops!',
				text: 'Qualcosa è andato storto',
				type: 'error'
			});
		})
	}

	$scope.selectUser = function(u){
		if(u.user && u.user._id){
			RestaurantsServices.getClientInfo.async($scope.risto_id,u.user._id).success(function(data){
				if(data.message == 'ok'){
					$scope.current_client = u;
					if(!u.allergie) $scope.current_client.allergie = [];
					if(!u.preferenze) $scope.current_client.preferenze = [];
					$scope.current_client.infos = data.infos
				} 
			}).error(function(){
				swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Oops!',
					text: 'Qualcosa è andato storto',
					type: 'error'
				});
			});
		} else {
			$scope.current_client = u;
			if(!u.allergie) $scope.current_client.allergie = [];
			if(!u.preferenze) $scope.current_client.preferenze = [];
			$scope.current_client.infos = []
		}
	}	


	$scope.addAllergeneToClient = function(index){
		var i = $scope.add_client.allergeni.indexOf($scope.allergeni[parseInt(index)]);
		if(i === -1) $scope.add_client.allergeni.push($scope.allergeni[parseInt(index)]);
		else $scope.add_client.allergeni.splice(i,1);
	}

	$scope.addAllergeneToCurrentClient = function(index){
		var i = $scope.current_client.allergie.indexOf($scope.allergeni[parseInt(index)]);
		if(i === -1) $scope.current_client.allergie.push($scope.allergeni[parseInt(index)]);
		else $scope.current_client.allergie.splice(i,1);
	}

	$scope.confirmEdit = function(){
		ToolsServices.mainOverlay.show();
		RestaurantsServices.editCustomClient.async($scope.risto_id,$scope.current_client).success(function(data){
			if(data.message == 'ok'){
				
			} else {
				swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Oops!',
					text: 'Qualcosa è andato storto',
					type: 'error'
				});
			}
			ToolsServices.mainOverlay.hide();
		}).error(function(){
			swal({
				buttonsStyling: false,
				confirmButtonClass: 'btn btn-primary btn-sm ghost',
				title: 'Oops!',
				text: 'Qualcosa è andato storto',
				type: 'error'
			});
			ToolsServices.mainOverlay.hide();
		})
	}

	$scope.removeCurrentClient = function(){
		alertify.okBtn("Si")
			.cancelBtn("No")
			.confirm("Sicuro di voler cancellare questo cliente? L'azione è irreversibile", function () {	
			ToolsServices.mainOverlay.show();
			RestaurantsServices
			.deleteClient  
			.async($scope.risto_id,$scope.current_client._id).success(function(data){
				angular.forEach( $scope.clients, function(v, k) {
					if(v._id == $scope.current_client._id) $scope.clients.splice(k, 1);
				});
				setVisibleClients();
				$scope.current_client = 0;
				ToolsServices.mainOverlay.hide();
			}).error(function(){
				ToolsServices.mainOverlay.hide();
			});
		    
		}, function() {
		    
		});			
	}


	$scope.search_input = '';
	$scope.search = function(){
		if($scope.search_input == '') return;
		$scope.viewed_clients = [];
		var input = $scope.search_input;

		angular.forEach($scope.clients, function(v,k){
			if(v.name+' '+v.surname == input
				|| v.name == input
				|| v.surname == input
				|| (v.user && (v.user.name == input || v.user.surname == input || v.user.name+' '+v.user.surname == input))
				|| v.email == input
				|| v.phone == input){
				$scope.viewed_clients.push(v);
			}
		});
		$scope.filtered_results = 2;
	}
	$scope.annullaFiltro = function(){
		$scope.filtered_results = 1;
		$scope.search_input = '';
		setVisibleClients();
	}

});