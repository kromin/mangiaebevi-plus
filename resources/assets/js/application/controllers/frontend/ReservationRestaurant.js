var ReservationsController = angular.module('AppEditReservationsController',[]);

ReservationsController
.controller('EditReservationsCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams,$route,$document,$timeout,ngDialog,RestaurantsServices,UserServices,ToolsServices,alertify,ImagesServices,SearchServices,ReservationServices, IframeServices, PaymentServices){
	"ngInject";


	$scope.avaible_tables = 0; // tavoli disponibili nella struttura
	
	/**
	* Reservations init
	*/
	var cal = null;
	$scope.calLoaded = false; // evito di richiamare il cal heatmap multiple volte

	$scope.InitReservations = function(){


		$scope.addReservationTab = function(){
			FB.ui({
			  method: 'pagetab',
			  redirect_uri: $scope.mainUrl+'/importframe/facebook/'
			}, function(response){
				//console.log(response);
				$scope.addFacebookID(Object.keys(response.tabs_added)[0]);
				// mi ritorna un tabs_added: [id pagina: true|false]
				// devo salvarli e metterli nel db
				// la pagina frame viene richiamata con ?fb_page_id=123392764395906
				// in base a quello vado a prendere il menu
			});
		};

	$scope.addFacebookID = function(page){
		var page = parseInt(page);
		RestaurantsServices.addFacebookPage.async($scope.restaurant._id,page).success(function(data){
			if(data.message == 'just'){
				swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Errore!',
					text: 'La pagina è già stata aggiunta da un ristorante',
					type: 'error'
				});	
			} else
			if(data.message == 'ok'){
				swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Congratulazioni!',
					text: 'La pagina è stata aggiunta',
					type: 'success'
				});	
				$scope.restaurant.facebook_page_id = page;
			}
		});
	}

		if(cal){
			cal = cal.destroy();
		}
		$scope.current_date = moment().format('YYYY-MM-DD');
		
		$scope.loading_reservations = true;
	    $scope.loadReservation = function(){
	    	$scope.loading_reservations = true;
	    	$scope.reservations = [];
	    	$scope.reservations_users = [];
	    	if($('#datetimepicker').val() && $('#datetimepicker').val() !== ''){
	    		var date = $('#datetimepicker').val();
	    		$scope.current_date = moment(date,"DD/MM/YYYY").format('YYYY-MM-DD');
	    	}
	    	ReservationServices.
	    	viewByDate.async($scope.restaurant._id,$scope.current_date).success(function(data){
	    		if(data.message == 'ok'){
					$scope.reservations = data.reservations;
					$scope.reservations_users = data.users;			        		
	    			initReservationManagement();
	    			$scope.loading_reservations = false;
	    		}
	    		
	    	});
	    };
	    // carica gli ordini
	    $scope.loadReservation();



	    $scope.returnUser = function(id){
	    	var ret = {};
	    	
	    	angular.forEach($scope.reservations_users,function(v,k){
	    		if(v._id == id) {	    			
	    			ret = v;
	    		}
	    	});
	    	return ret;
	    };

	    
	    $scope.getRestaurantAvaibleFasce = function(){
	    	// prendo elenco fasce disponibili da servizio per il giorno
	    	ReservationServices.
	    	getRestaurantFasce.async($scope.restaurant._id,$scope.current_date).success(function(data){
	    		if(data.message == 'ok' && data.fasce.length > 0){
					$scope.restaurant_fasce = data.fasce;
					//$scope.current_fascia = data.fasce[0];
					//resetTables();
					//setViewedReservations();					
	    		}
	    		
	    	});
	    };

	    /*
	    $scope.current_fascia = null;
	    // filtra gli ordini per fascia oraria
	    $scope.selectFasciaOraria = function(fascia){
	    	$scope.current_fascia = fascia;
	    	//resetTables();
	    	//setViewedReservations();
	    };
		*/
	    

	    /**
	    *
		* Dividi prenotazioni tra quelle da mostrare e quelle invece nei tavoli
		*
	    *
	    var returnTableIndex = function(numero,array){
	    	var ret = -1;
	    	angular.forEach(array,function(v,k){
	    		if(v.num == numero) ret = k;
	    	});
	    	return ret;
	    };
	    var returnTableNumber = function(indice,array){
	    	return array[indice].num;
	    };*/

	    /*
	    var setViewedReservations = function(){
	    	$scope.viewed_reservations = [];
	    	//resetTables();
	    	angular.forEach($scope.reservations,function(v,k){
	    		if(v.fascia_oraria == $scope.current_fascia){
	    			if(v.confirmed === 0 || v.confirmed == 2 || v.confirmed == 3){
	    				// metti la prenotazione tra quelle in lista
	    				$scope.viewed_reservations.push(v);
	    			} else {
	    				// mettila nel tavolo
	    				var inserted = false;
	    				var tindex = returnTableIndex(v.tavolo,$scope.restaurant.struttura.sale[v.sala].tables);
	    				//console.log('aggiorno il tavolo:'+ tindex)
	    				if(($scope.restaurant.struttura.sale[v.sala] && $scope.restaurant.struttura.sale[v.sala].tables[tindex])){
	    					inserted = true;
	    					//console.log('inserito')
	    					$scope.restaurant.struttura.sale[v.sala].tables[tindex].reservation = v;
	    				}					    				
	    				if(!inserted) {
	    					//console.log('lo metto anche qui');
	    					$scope.viewed_reservations.push(v);
	    				}
	    				
	    			}
	    		}
	    	});
	    };
	    */
	    

	    /* resetta i tavoli della struttura a cambio di data e fasce
	    var resetTables = function(){
	    	if($scope.restaurant.struttura && $scope.restaurant.struttura.sale){
		    	angular.forEach($scope.restaurant.struttura.sale,function(sala,chiave){
		    		angular.forEach($scope.restaurant.struttura.sale[chiave].tables,function(tavolo,t_chiave){
		    			$scope.restaurant.struttura.sale[chiave].tables[t_chiave].reservation = null;
		    		});
		    	});
	    	}
	    };*/


	    $scope.loadSingleReservation = function(id,reservation_obj){
	    	ReservationServices.viewSingle.async(id).success(function(data){
	    		if(data.message == 'ok'){
	    			setAvaibleFasceForChange();
	    			setAvaiblePosti(reservation_obj);
	    			$scope.current_reservation = data.reservation;

		            var reservationDialog = ngDialog.open({
		                template: '/templates/dialogreservation',
		                scope: $scope,
		                className: 'ngdialog ngdialog-theme-default small'
		            });						            
		            
	    		}					    		
	    	});
	    };

	    $scope.assignFoodcoin = function(reservation){
	    	ReservationServices.assignFoodcoin.async($scope.restaurant._id,reservation).success(function(data){
	    		if(data.message == 'ok'){
	    			$scope.current_reservation = data.reservation;
	    			angular.forEach($scope.reservations,function(v,k){
	    				if(v._id == reservation) $scope.reservations[k] = data.reservation;
	    			});
	    			
	    		}
	    	}).error(function(d){
	    		swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Oops!',
					text: 'Qualcosa è andato storto',
					type: 'error'
				});
	    	});
	    };

	    $scope.removeFoodcoin = function(reservation){
	    	ReservationServices.removeFoodcoin.async($scope.restaurant._id,reservation).success(function(data){
	    		if(data.message == 'ok'){
	    			$scope.current_reservation = data.reservation;
	    			angular.forEach($scope.reservations,function(v,k){
	    				if(v._id == reservation) $scope.reservations[k] = data.reservation;
	    			});
	    			
	    		}
	    	}).error(function(d){
	    		swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Oops!',
					text: 'Qualcosa è andato storto',
					type: 'error'
				});
	    	});
	    };
 
	    var setAvaiblePosti = function(reservation_obj){
	    	var fascia = reservation_obj.fascia_oraria;
	    	var total_avaible = 0;
	    	var total_prenotati = 0;
	    	angular.forEach($scope.reservations,function(v,k){
	    		if(v.fascia_oraria == fascia && v.confirmed == 1 && v.type == 'web') total_prenotati += parseInt(v.people);
	    	});
	    	angular.forEach($scope.restaurant.struttura.sale,function(v,k){
	    		total_avaible += parseInt(v.posti);
	    	});
	    	$scope.avaible_tables = total_avaible - total_prenotati;
	    };

 
	    if($routeParams.reservation){
	    	$scope.loadSingleReservation($routeParams.reservation);
	    }

	    $scope.change_orario = {
	    	avaible_fasce: [],
	    	ore: null
	    };

	    var setAvaibleFasceForChange = function(){
	    	ReservationServices.
	    	getRestaurantAvaibleFasce.async($scope.restaurant._id,$scope.current_date).success(function(data){
	    		if(data.message == 'ok' && data.fasce.length > 0){
					$scope.change_orario.avaible_fasce = data.fasce;
					if(data.fasce.length > 0){
						$scope.change_orario.ore = data.fasce[0];
					}
	    		}
	    		
	    	});
	    };


	    /* Drop handler.
		$scope.onDrop = function (data, sala, tavolo, event) {
		    // Get custom object data.
		    //alert(sala+' - '+tavolo);
		    
		    // ...
		    var reservation = data['json/reservation_obj'];
		    if(reservation.confirmed == 2){
		    	// deve aspettare l'approvazione dell'utente
		    	alert('L\'utente ancora non ha approvato il cambio di orario');
		    } else {

		    	
			    if(!$scope.restaurant.struttura.sale[sala].tables[tavolo].reservation) {
			    	alertify
					.okBtn("Si")
		  			.cancelBtn("No, ho sbagliato")
		  			.confirm("Vuoi confermare questa prenotazione?", function () {	
						
						dropConfirm(reservation,sala,tavolo);
					    
					}, function() {
					    
					});
			    } else {

			    }

			}
		    
		};

		  // Drag over handler.
		  $scope.onDragOver = function (event) {
		    // ...
		  };


		  var dropConfirm = function(reservation,sala,tavolo){
		  	
		  	var tavolo = returnTableNumber(tavolo,$scope.restaurant.struttura.sale[sala].tables);
		  	
		  	ToolsServices.mainOverlay.show();
		  	ReservationServices.confirmReservation
	    	.async(reservation.restaurant,
	    			reservation._id,
	    			parseInt(sala),
	    			parseInt(tavolo))
	    	.success(function(data){
	    		if(data.message == 'ok'){
	    			//$scope.restaurant.struttura.sale[sala].tables[tavolo].reservation = data.reservation;
	    			// aggiorna					    			
	    			
	    			angular.forEach($scope.reservations,function(v,k){
	    				if(v._id == data.reservation._id) {
	    					$scope.reservations[k] = data.reservation;
	    					$scope.reservations[k].confirmed = 1;
	    					//console.log('feedback',$scope.reservations[k])
	    				}
	    			});					    			
	    								    			
	    		}		
	    		//setViewedReservations();		    	
	    		ToolsServices.mainOverlay.hide();	
	    	}).error(function(e){
	    		swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Oops!',
					text: 'Qualcosa è andato storto',
					type: 'error'
				});
	    		ToolsServices.mainOverlay.show();
	    	});
		  };
		*/

		$scope.confirmation_selected_sala = parseInt('0');
		$scope.setConfirmationSala = function(n){
			$scope.confirmation_selected_sala = parseInt(n);
		};

		$scope.confirmReservation = function(reservationid){

			if(!$scope.confirmation_selected_sala) $scope.confirmation_selected_sala = 0;

			ToolsServices.mainOverlay.show();
		  	ReservationServices.confirmReservation
	    	.async($scope.restaurant._id,
	    			reservationid,
	    			$scope.confirmation_selected_sala
	    			)
	    	.success(function(data){
	    		if(data.message == 'ok'){	    					
	    			angular.forEach($scope.reservations,function(v,k){
	    				if(v._id == data.reservation._id) {
	    					$scope.reservations[k] = data.reservation;
	    					$scope.reservations[k].confirmed = 1;
	    					//console.log('feedback',$scope.reservations[k])
	    					ngDialog.close();
	    				}
	    			});					    
	    			ToolsServices.mainOverlay.hide();				
	    								    			
	    		} else {
	    			ToolsServices.mainOverlay.hide();	
	    		}
	    			    	
	    		
	    	}).error(function(e){
	    		swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Oops!',
					text: 'Qualcosa è andato storto',
					type: 'error'
				});
	    		ToolsServices.mainOverlay.hide();
	    	});
		};

		  $scope.confirm_date_change = function(){
		  	if($scope.change_orario.ore != $scope.current_fascia && $scope.change_orario.ore){
		  		ReservationServices.changeDate
		    	.async($scope.current_reservation.restaurant,
		    			$scope.current_reservation._id,
		    			$scope.current_date,
		    			$scope.change_orario.ore)
		    	.success(function(data){
		    		if(data.message == 'ok'){
		    			// aggiorna fascia oraria
		    			$scope.current_reservation.fascia_oraria = $scope.change_orario.ore; 
		    			$scope.current_reservation.confirmed = 2; 
		    			// aggiorna prenotazione nell'array
		    			angular.forEach($scope.reservations,function(v,k){
		    				if(v._id == $scope.current_reservation._id) {
		    					$scope.reservations[k] = angular.copy($scope.current_reservation);
		    					//setViewedReservations();
		    				}
		    			});
		    			
		    		}					    		
		    	});
		  	}
		  	
		  };


		  $scope.removeReservation = function(id){
		  	alertify
			.okBtn("Si")
  			.cancelBtn("No")
  			.confirm("Sicuro di voler annullare questa prenotazione?", function () {	
				ToolsServices.mainOverlay.show();
				ReservationServices
				.deleteReservation  
				.async(id).success(function(data){
					if(data.message == 'ok'){
						angular.forEach($scope.reservations,function(v,k){
							if(v._id == id){
								$scope.reservations[k].confirmed = 4;
							}
						});
					}
					ToolsServices.mainOverlay.hide();
				}).error(function(){
					ToolsServices.mainOverlay.hide();
				});
			    
			}, function() {
			    
			});			
		};

		/*
	    $scope.confirm_form = {
	    	sala: false,
	    	tavolo: false
	    };
	    $scope.current_sala = 0;
	    $scope.confirm = function(){
	    	if($scope.confirm_form.sala && $scope.confirm_form.tavolo){
		    	if($scope.date_change === 0){
			    	ReservationServices.confirmReservation
			    	.async($scope.current_reservation.restaurant,
			    			$scope.current_reservation._id,
			    			$scope.confirm_form.sala,
			    			$scope.confirm_form.tavolo)
			    	.success(function(data){
			    		if(data.message == 'ok'){
			    			$scope.current_reservation = data.reservation;
			    		}					    		
			    	});
		    	} else {
		    		
		    		var new_date = $("#timepicker").val();
		    		var parts = new_date.split(" ");
		    		var date = parts[0].split("/");
		    		
		    		ReservationServices.changeDate
			    	.async($scope.current_reservation.restaurant,
			    			$scope.current_reservation._id,
			    			date[2]+'-'+date[1]+'-'+date[0],
			    			parts[1],
			    			$scope.confirm_form.sala,
			    			$scope.confirm_form.tavolo)
			    	.success(function(data){
			    		if(data.message == 'ok'){
			    			$scope.current_reservation = data.reservation;
			    		}					    		
			    	});
		    	}
    		}
	    };

	    $scope.setSala = function(){
	    	angular.forEach($scope.restaurant.struttura.sale,function(v,k){
	    		if (v.name == $scope.confirm_form.sala) $scope.current_sala = k;
	    	});
	    };
		*/
	    $scope.date_change = 0;
	    $scope.changeDate = function(status){
	    	$scope.date_change = status;
	    };			
		



	    $scope.heatmap_reservations = [];
        var LoadAllReservations = function(from,to){
        	ReservationServices.viewByFromToRestaurant
        	.async($scope.restaurant._id,from.format("YYYY-MM-DD"),to.format("YYYY-MM-DD")).success(function(data){
        		$scope.heatmap_reservations = data.reservations;
        		$scope.loadHeatMap();
        	});
        };
        

		$scope.heatmap_start = moment().add(-2,'months');
        $scope.heatmap_end = moment().add(1,'months');
        
   		LoadAllReservations($scope.heatmap_start,$scope.heatmap_end);
       	

   	};



   	$scope.is_adding_manual = parseInt('0');
   	$scope.setAddingManual = function(n){
   		$scope.is_adding_manual = parseInt(n);
   	};
   	
   	$scope.manual_reservation_default = {
   		name: '',
   		people: '',
   		email: '',
   		tel: '',
   		date: $scope.current_date,
   		fascia: null
   	};

   	$scope.addManualReservation = function(){
   		ToolsServices.mainOverlay.show();
   		ReservationServices.addManualReserve
   		.async(
   			$scope.restaurant._id,
   			$scope.manual_reservation_default.date,
   			$scope.manual_reservation_default.fascia,
   			$scope.manual_reservation_default.email,
   			$scope.manual_reservation_default.people,
   			$scope.manual_reservation_default.name,
   			$scope.manual_reservation_default.tel)
   		.success(function(data){
   			if(data.message == 'ok'){
   				$scope.loadReservation();
   				resetReservationObj();
   			}
   			ToolsServices.mainOverlay.hide();
   		}).error(function(){
   			swal({
				buttonsStyling: false,
				confirmButtonClass: 'btn btn-primary btn-sm ghost',
				title: 'Oops!',
				text: 'Qualcosa è andato storto',
				type: 'error'
			});
    		ToolsServices.mainOverlay.hide();
   		});
   	};

   	var resetReservationObj = function(){
   		$scope.manual_reservation_default = {
	   		name: '',
	   		people: '',
	   		email: '',
	   		tel: '',
	   		date: $scope.current_date,
	   		fascia: null
	   	};
   	}



   	var initReservationManagement = function(){
    	//$scope.current_fascia = null;
    	$scope.getRestaurantAvaibleFasce();
    };
	    
   	$scope.heatmap_reservations = [];
   	//$scope.$watch('heatmap_reservations',function(){
    

    $scope.loadHeatMap = function(){    	
    	if($scope.heatmap_reservations){

    		var datas = {};
    		angular.forEach($scope.heatmap_reservations,function(v,k){

    			var d = moment(v.date);
    			v.date = d.format("X");
    			if(!datas[v.date]){
    				datas[v.date] = 1;
    			} else {
    				datas[v.date]++;
    			}
    		});
    	 		
    	 	var h = document.getElementById('heatmap');
    	 	if(h){
    	 		cal = new CalHeatMap();
		    	
				cal.init({
					itemSelector: "#heatmap",
					domain: "month",
					start: new Date($scope.heatmap_start.format("YYYY"),$scope.heatmap_start.format("M")),
					end: new Date($scope.heatmap_end.format("YYYY"),$scope.heatmap_end.format("M")),
					cellSize: 20,
					range: 4,
					verticalOrientation: true,
					subDomainTextFormat: "%d",
					data: datas,
					onClick: function(date, nb) {
						$scope.current_date = moment(date).format('YYYY-MM-DD');					    	
				    	ReservationServices.viewByDate.async($scope.restaurant._id,$scope.current_date).success(function(data){
				    		if(data.message == 'ok'){
								$scope.reservations = data.reservations;
								$scope.reservations_users = data.users;
				    			initReservationManagement();
				    		}
				    	});
					},
					subDomainDateFormat: function(date) {
						return moment(date).format("DD/MM/YYYY"); // Use the moment library to format the Date
					},
					afterLoad: function(){
						$scope.calLoaded = true;
					}
				});	
    	 	}		        
		        
			
    	}

    };

    $scope.reservation_tab = 0;
	$scope.setReservationTab = function(n){
		$scope.reservation_tab = n;
	};


	/**
	* Esporta PDF prenotazioni 
	*
	*
	*/
	$scope.export_reservation_obj = {
		current_date: $scope.current_date,
		from: '',
		to: '',
		current: parseInt('0') 
	};

	$scope.setCurrentDate = function(){ 
		$scope.export_reservation_obj.current = ($scope.export_reservation_obj.current == 1) ? 0 : 1;		
	};


	$scope.openExportReservations = function(){
		var url = null;
		switch($scope.export_reservation_obj.current){
			case 0:
			if(moment($scope.export_reservation_obj.from,'DD/MM/YYYY').isValid() && moment($scope.export_reservation_obj.to,'DD/MM/YYYY').isValid()) url = '/export/prenotazioni/'+$scope.restaurant._id+'/0/'+moment($scope.export_reservation_obj.from,'DD/MM/YYYY').format('DD-MM-YYYY')+'/'+moment($scope.export_reservation_obj.to,'DD/MM/YYYY').format('DD-MM-YYYY');
			break;
			case 1:
			if(moment($scope.current_date,'YYYY/MM/DD').isValid()) url = '/export/prenotazioni/'+$scope.restaurant._id+'/1/'+moment($scope.current_date,'YYYY/MM/DD').format('DD-MM-YYYY');
			break;
		}
		if(url){
			window.open(url,
			'_blank',
			'fullscreen=yes',false);
		}
	};

	$scope.cleaveOptions = {
		dateOPT: {
			date: true,
	    	datePattern: ['d', 'm', 'Y']
	    }
	};
	
	$scope.order_from_restaurant = 1;
	$scope.viewPreOrder = function(reservation) {
		$scope.dialog_order_plates = reservation.preorder;

		var orderDialog = ngDialog.open({
            template: '/templates/dialogorder',
            scope: $scope,
            className: 'ngdialog ngdialog-theme-default small'
        });	
	}

});