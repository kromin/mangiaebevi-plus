var PlateController = angular.module('AppPlateController', []);

PlateController
    .controller('PlateCtrl', function($scope, $rootScope, $cookies, $sce, $location, $route, $routeParams, $document, ngDialog, RestaurantsServices, UserServices, PlatesServices, SearchServices, ToolsServices, RecommendationServices, ImagesServices) {
        "ngInject";
        $scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;
        $scope.$on('userLogged', function(event) {
            // se l'utente ha fatto il login mettilo in scope
            $scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;
        });

        ToolsServices.getMainSiteInfo.async().success(function(data) {
            $scope.creditCard = _.indexBy(data.creditCard, '_id');
            $scope.homeCities = _.indexBy(data.homeCities, '_id');
            $scope.regioniCucine = _.indexBy(data.regioniCucine, '_id');
            $scope.tipiCucine = _.indexBy(data.tipiCucine, '_id');
            $scope.tipiLocali = _.indexBy(data.tipiLocali, '_id');
            $scope.services = _.indexBy(data.services, '_id');
            $scope.pacchetti = _.indexBy(data.pacchetti, 'price_month');
            $scope.staticURL = data.staticDomain; // domain for static files    
        });
        
        $scope.current_plate = {}
        $scope.initPage = function(plate){
            $scope.current_plate._id = plate;                         
        }

        $scope.loadRestaurant = function(){
            
        }


        $scope.plate_tab = 0;
        $scope.setPlateTab = function(n){
            $scope.plate_tab = n;
        }

          
        

        /**
        * Raccomandazioni
        */
        $scope.rec_pagination = 1;
        $scope.pagination_number = 10;
        $scope.plate_rec = [];
        $scope.stop_load_rec_pagination = false;
        $scope.loadPlateRecommendations = function(){
            $scope.stop_load_rec_pagination = true;
            if($scope.rec_pagination == 1) $scope.plate_rec = [];
            var load_rec_obj = {
                ref: $scope.current_plate._id,
                skip: ($scope.rec_pagination == 1) ? 0 : parseInt($scope.rec_pagination-1)*parseInt($scope.pagination_number),
                take: parseInt($scope.pagination_number)
            }
            RecommendationServices.viewByPlate.async(load_rec_obj).success(function(data){
                
                angular.forEach(data.recommendation,function(v,k){
                    $scope.plate_rec.push(v);
                });
                if(data.recommendation.length < $scope.pagination_number){
                    $scope.stop_load_rec_pagination = true;
                } else {
                    $scope.rec_pagination++;
                    $scope.stop_load_rec_pagination = false;
                }
            })
        
        }

        $scope.loadRecommendation = function(){
            $scope.loadPlateRecommendations();
        }




        $scope.images_pagination = 1;
        $scope.plate_images = [];
        $scope.stop_load_images_pagination = false;
        $scope.loadPlateImages = function(){
            $scope.stop_load_images_pagination = true;
            if($scope.images_pagination == 1) $scope.plate_images = [];
            var load_images_obj = {
                plate: $scope.current_plate._id,
                skip: ($scope.images_pagination == 1) ? 0 : parseInt($scope.images_pagination-1)*parseInt($scope.pagination_number),
                take: parseInt($scope.pagination_number)
            }
            PlatesServices.viewCommunityPlateByPlate.async(load_images_obj).success(function(data){
                
                angular.forEach(data.plateimages,function(v,k){
                    $scope.plate_images.push(v);
                });
                if(data.plateimages.length < $scope.pagination_number){
                    $scope.stop_load_images_pagination = true;
                } else {
                    $scope.images_pagination++;
                    $scope.stop_load_images_pagination = false;
                }
            })
        
        }

        $scope.loadPlateImagesN = function(){
            $scope.loadPlateImages();
        }
        





        $scope.simpleError = function(){
            swal({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                title: 'Oops!',
                text: 'Qualcosa è andato storto',
                type: 'error'
            });
        }



        $scope.add_plate_image = {
            image: null
        }

        $scope.add_plate_obj = {
            plate: null,
            image: null,
            text: '',
            rating: 3 // 1-5
        }

        $scope.initPlateImages = function(){
            $scope.add_plate_obj = {
                plate: $scope.current_plate._id,
                image: null,
                text: '',
                rating: 3 // 1-5
            }
        }

        $scope.uploadPlateImage = function(){
            
                        
            ToolsServices.mainOverlay.show();


            ImagesServices.addSingle
            .async('community','Immagine piatto','Immagine piatto '+$scope.current_plate.name,$scope.add_plate_image.image,$scope.current_plate._id)
            .success(function(data){
                console.log(data)
                if(data.message && data.message == 'ok'){
                    // data.image._id
                    $scope.add_plate_obj.image = data.image._id;
                }
                ToolsServices.mainOverlay.hide();
                
            }).error(function(){
                ToolsServices.mainOverlay.hide();
                swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Errore',
                    text: 'Siamo spiacenti ma si è verificato un errore',
                    type: 'error'
                });

            });
            $scope.add_plate_image.image = null;
                    
        }

        $scope.changeRate = function(type){
            switch(type){
                case 'add':
                if($scope.add_plate_obj.rating < 5) $scope.add_plate_obj.rating++;
                break;
                case 'less':
                if($scope.add_plate_obj.rating > 1) $scope.add_plate_obj.rating--;
                break;
            }
        }


        $scope.confirmAddPlateImage = function(){

            if($scope.add_plate_obj.plate 
                && $scope.add_plate_obj.image != ''
                && $scope.add_plate_obj.image != '' 
                && $scope.add_plate_obj.text != '' 
                && $scope.add_plate_obj.rating > 0 && $scope.add_plate_obj.rating < 6){
                
                PlatesServices.addCommunityPlate.async($scope.add_plate_obj).success(function(data){
                    $scope.add_plate_obj = {
                        plate: $scope.current_plate._id,
                        image: null,
                        text: '',
                        rating: 3 // 1-5
                    }

                    $scope.images_pagination = 1;
                    $scope.plate_images = [];
                    $scope.stop_load_images_pagination = false;
                    $scope.loadPlateImages();

                    $scope.closeEditing();
                }).error(function(a){
                    $scope.simpleError();
                })
            }
            
        }

        
        $scope.addImage = function(){
             
            if($scope.current_user.loggedStatus == 1){                
                $scope.initPlateImages();

                $scope.editing_menu = 1;
                
            } else {
                swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Non puoi!',
                    text: 'Devi accedere per aggiungere una immagine',
                    type: 'error'
                });
            }
        }
        $scope.closeEditing = function(){
            $scope.editing_menu = false;
        }




        /**
        * Recommendation
        */
        $scope.recommendPlate = function() {
            var user = UserServices.isCurrentLogged.isLoggedCurrently;
            if (user && user.loggedStatus == 1) {
                if(!user.user.loc || !user.user.loc.coordinates){
                    swal({
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-primary btn-sm ghost',
                        title: 'Oops!',
                        text: 'Devi completare il tuo profilo per raccomandare il piatto',
                        type: 'error'
                    });
                } else {
                    $scope.current_user = user;
                    $scope.dialog_community_recommendation = ngDialog.open({
                        template: '/templates/dialogrecommend',
                        scope: $scope,
                        className: 'ngdialog ngdialog-theme-default'
                    });
                }
            } else {

                // apri dialog login/registrazione
                var dialog = $scope.dialogLogin(1);
                dialog.closePromise.then(function(data) {
                    switch (data.value) {
                        case 'registerSubmitted':
                        case 'loginSubmitted':
                            $scope.recommendPlate();
                            break;
                    }
                });
            }
        }

        $scope.recommendCommunityFormData = {
            address: null,
            text: '',
            type: 1 
        }
        $scope.publishRecommendation = function(){
            $scope.recommendCommunityFormData.ref = $scope.current_plate._id;
            RecommendationServices.publishRecommendation.async($scope.recommendCommunityFormData).success(function(data){
                $scope.simpleSuccess('Congratulazioni','Raccomandazione confermata');
                $scope.dialog_community_recommendation.close();
            }).error(function(){
                swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Oops!',
                    text: 'Si è verificato un errore, ricorda che non puoi effettuare più di una raccomandazione generica per lo stesso piatto',
                    type: 'error'
                });
            })
        }

        $scope.simpleSuccess = function(title,text){
            swal({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                title: title,
                text: text,
                type: 'success' 
            });
        }


    });