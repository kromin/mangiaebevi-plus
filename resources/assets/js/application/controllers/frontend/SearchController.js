var SearchController = angular.module('AppSearchController', []);

SearchController
    .controller('SearchCtrl', function($scope, $rootScope, $cookies, $sce, $location, $document, $routeParams, $window, ngDialog, RestaurantsServices, UserServices, ReservationServices, SearchServices, ToolsServices) {
        "ngInject";

        $scope.view_type = 'grid';
        $scope.page = ($location.hash()) ? parseInt($location.hash()) : 1;
        
        ToolsServices.getMainSiteInfo.async().success(function(data) {
            $scope.creditCard = _.indexBy(data.creditCard, '_id');
            $scope.homeCities = _.indexBy(data.homeCities, '_id');
            $scope.regioniCucine = _.indexBy(data.regioniCucine, '_id');
            $scope.tipiCucine = _.indexBy(data.tipiCucine, '_id');
            $scope.tipiLocali = _.indexBy(data.tipiLocali, '_id');
            $scope.services = _.indexBy(data.services, '_id');
            $scope.pacchetti = _.indexBy(data.pacchetti, 'price_month');
            $scope.staticURL = data.staticDomain; // domain for static files    
        });
        
        $scope.SetRestaurants = function(obj){
            //$scope.restaurants = JSON.parse(obj);
            //set_search_map_restaurant();
        } 

        var loadMap = function(restaurants){
            
            $rootScope.$on('page loaded',function(){
                
                $scope.$broadcast('initializeMap', restaurants);
                
            })            
            
        }


        $scope.setPage = function(page) {
            $scope.map_restaurants = [];
            
            $location.hash(page); 
            $scope.page = page;      

            $timeout(function(){
                //$scope.$broadcast('initializeMap', $scope.map_restaurants);
            },300);
            
        }

        var hashChange = function(page){
            if(!page || page == '') page = 1;
            $scope.map_restaurants = [];
            $scope.page = parseInt(page);
            $timeout(function(){
                //$scope.$broadcast('initializeMap', $scope.map_restaurants);
            },300);
        }

        
        

        $scope.$watch(function () { 
            return $location.hash()
        }, function (value) {
            // do stuff
            hashChange(value)
        });

        
        $scope.search = {
            query_string: ''
        }
        $scope.setNewSearch = function() {
            if ($scope.search.query_string != '') {
                
                var url = '', add = '';
                if($cookies.get('lng') && $cookies.get('lat')) add += $cookies.get('lat') + '/' + $cookies.get('lng') + '/';
                url += '/s/'+$scope.search.query_string+'/'+add;
                window.location = url;
            }
        }



        var advDialog;
        $scope.openAdvSearch = function(){
            advDialog = ngDialog.open({
                template: '/templates/dialogadvancedsearch',
                scope: $scope,
                className: 'ngdialog ngdialog-theme-default medium',
                closeByNavigation: true
            });
        }


        $scope.advSearch = {
            name     : '',
            type     : '',
            location : '',
            maxPrice : 80,
            services : [],
            types : [],
            regions : [],            
            virtualTour: false,
            prenota: '',
            menu: false
        }


        $scope.selectItem = function(id,type){

            switch(type){
                case 'service':
                    var index = $scope.advSearch.services.indexOf(id)
                    if(index != -1){
                        $scope.advSearch.services.splice(index,1);
                    } else {
                        $scope.advSearch.services.push(id);
                    }
                break;
                case 'tipocucina':
                    var index = $scope.advSearch.types.indexOf(id)
                    if(index != -1){
                        $scope.advSearch.types.splice(index,1);
                    } else {
                        $scope.advSearch.types.push(id);
                    }
                break;
                case 'regionecucina':
                    var index = $scope.advSearch.regions.indexOf(id)
                    if(index != -1){
                        $scope.advSearch.regions.splice(index,1);
                    } else {
                        $scope.advSearch.regions.push(id);
                    }
                break;
                case 'carta':
                    var index = $scope.advSearch.carte.indexOf(id)
                    if(index != -1){
                        $scope.advSearch.carte.splice(index,1);
                    } else {
                        $scope.advSearch.carte.push(id);
                    }
                break;
            }
            
        }


        $scope.SearchAdvanced = function(){
            angular.forEach($scope.advSearch,function(v,k){
                if(v == '' || v == null || !v) delete $scope.advSearch[k];
            });
            window.location = '/ricerca-avanzata/'+encodeURI(JSON.stringify($scope.advSearch));
            //$location.update_path('/ricerca-avanzata/'+encodeURI(JSON.stringify($scope.advSearch)));            
        }


        /**
         * Imposta ristoranti nella mappa
         */
        var set_search_map_restaurant = function() {
            $scope.map_restaurants = [];
            angular.forEach($scope.restaurants, function(val, k) {
                if (val.obj) {
                    val.obj.distance = (val.dis / 1000).toFixed(2);
                    $scope.map_restaurants.push(val.obj);
                } else {
                    $scope.map_restaurants.push(val);
                }
            });
            //$scope.$broadcast('initializeMap', $scope.map_restaurants);
        }



        $scope.returnDistance = function(lat1, lon1) {

            if(!$cookies.get('lat')) return '';
            //console.log(''+lat1+', '+lon1+' - '+$cookies.get('lat')+', '+$cookies.get('lng'));

            var unit = 'K';
            var radlat1 = Math.PI * lat1/180
            var radlat2 = Math.PI * $cookies.get('lat')/180
            var theta = lon1-$cookies.get('lng')
            var radtheta = Math.PI * theta/180
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            dist = Math.acos(dist)
            dist = dist * 180/Math.PI
            dist = dist * 60 * 1.1515
            if (unit=="K") { dist = dist * 1.609344 }
            if (unit=="N") { dist = dist * 0.8684 }
            return '- '+parseFloat(dist).toFixed(2)+' km';
        }


        

        $scope.pushMapObj = function(name,image,address,lng,lat,slug,city,total){
            if(!$scope.map_restaurants) $scope.map_restaurants = [];
            
            $scope.map_restaurants.push({
                name: name,
                slug: slug,
                city: city,
                image: (image && image != '') ? image : null,
                address: address,
                loc: {
                    coordinates:[
                        parseFloat(lat),parseFloat(lng)
                    ]
                }
            });
            
            if($scope.map_restaurants.length == total){
                loadMap($scope.map_restaurants)
            }
        }


    });