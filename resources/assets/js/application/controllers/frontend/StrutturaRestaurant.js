var EditStrutturaController = angular.module('AppEditStrutturaController',[]);

EditStrutturaController
.controller('EditStrutturaCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams,$route,$document,$timeout,ngDialog,RestaurantsServices,UserServices,ToolsServices,alertify,ImagesServices,SearchServices,ReservationServices, IframeServices, PaymentServices){
	"ngInject";

	$scope.tableDrag = {
		dragTablesListeners: {
			accept: function (sourceItemHandleScope, destSortableScope) {
			  return true;
			}
		}
	}

	/**
				* Structure
				*/
				$scope.InitStructure = function(){

					var addTableD = null; // dialog di aggiunta tavolo
					$scope.abilitaPrenotazione = function(){
						RestaurantsServices.changePrenotazione.async($scope.restaurant._id).success(function(data){
							if(data.message && data.message == 'ok'){
								$scope.restaurant.prenotazione = ($scope.restaurant.prenotazione) ? !$scope.restaurant.prenotazione : 1;
					        }
						})
					}

					$scope.setTimeShift = function(n){
						$scope.restaurant.time_shift = n;
					}

					/**
					* Struttura
					*/
					$scope.add_struttura = {
						sale: []
					}
					$scope.add_struttura_children = {}
					$scope.restaurant.time_shift = (!$scope.restaurant.time_shift) ? "30" : $scope.restaurant.time_shift;
					$scope.restaurant.struttura = (!$scope.restaurant.struttura) ? { sale: [] } : $scope.restaurant.struttura;

					$scope.add_sala = function(){
						if($scope.add_struttura_children.sala && $scope.add_struttura_children.sala != ''){
							var to_add = {
								name: $scope.add_struttura_children.sala,
								posti: parseInt($scope.add_struttura_children.posti),
								tables: []
							}
							$scope.restaurant.struttura.sale.push(to_add);
							$scope.add_struttura_children = {}
						}			
					}
					$scope.add_tavolo = function(indice_sala){
						if(!$scope.add_struttura_children.tavolo || $scope.add_struttura_children.tavolo == ''
							|| !$scope.add_struttura_children.posti || $scope.add_struttura_children.posti <= 0
							|| isNaN($scope.add_struttura_children.posti) || isNaN($scope.add_struttura_children.tavolo)
							){
							return;
						}
						var tavolo = {
							num: $scope.add_struttura_children.tavolo,
							posti: $scope.add_struttura_children.posti
						}
						var do_not_proceed = false;
						angular.forEach($scope.restaurant.struttura.sale[indice_sala].tables,function(v,k){
							if(v.num == tavolo.num){
								do_not_proceed = true;
							}
						})
						if( do_not_proceed ){
							alertify.error('Già esiste un tavolo con questo numero');
							return;
						}
						if(addTableD) addTableD.close();
						$scope.restaurant.struttura.sale[indice_sala].tables.push(tavolo);
					}

					$scope.removeTable = function(sala,tavolo){
						
						alertify
						.okBtn("Si")
			  			.cancelBtn("No, ho sbagliato")
			  			.confirm("Vuoi cancellare questo tavolo?", function () {	
							
							//$scope.confirmRemoveTable(sala,tavolo);
						    $scope.$apply(function(){
						    	$scope.restaurant.struttura.sale[sala].tables.splice(tavolo,1);
						    })
						}, function() {
						    
						});
						
					}
					$scope.confirmRemoveTable = function(sala,tavolo){
						$scope.restaurant.struttura.sale[sala].tables.splice(tavolo,1);
					}

					
					$scope.current_sala = null;
					$scope.setCurrentSala = function(index){
						$scope.current_sala = index;
					}

					$scope.updateStructure = function(){									
						//$scope.restaurant.description = $scope.restaurant.description_html;		
						RestaurantsServices
						.addStruttura
						.async($scope.restaurant._id,$scope.restaurant.time_shift,$scope.restaurant.struttura).success(function(data){
							if(data.message && data.message == 'ok'){
								swal({
									buttonsStyling: false,
									confirmButtonClass: 'btn btn-primary btn-sm ghost',
									title: 'Congratulazioni!',
									text: 'Modifiche salvate',
									type: 'success'
								});			
								$scope.hasNotSaved = true;				
					        	//alertify.success('Modifiche salvate');
					        } else {
					        	swal({
									buttonsStyling: false,
									confirmButtonClass: 'btn btn-primary btn-sm ghost',
									title: 'Oops!',
									text: 'Qualcosa è andato storto',
									type: 'error'
								});
					        }
						})
					}

					
					$scope.addTableDialog = function(){
						$scope.add_struttura_children = {
							posti: 0,
							tavolo: ''
						}
						addTableD = ngDialog.open({
			                template: '/templates/dialogAddTable',
			                scope: $scope,
			                className: 'ngdialog ngdialog-theme-default'
			            });
					}
					$scope.removePax = function(){
						if($scope.add_struttura_children.posti > 0){
							$scope.add_struttura_children.posti--
						}
					}
					$scope.addPax = function(){
						$scope.add_struttura_children.posti++
					}

					$scope.removeSala = function(){
						if($scope.current_sala || $scope.current_sala == 0){
							alertify
							.okBtn("Si")
				  			.cancelBtn("No, ho sbagliato")
				  			.confirm("Vuoi cancellare questa sala?", function () {	
								$scope.$apply(function(){
							    	$scope.restaurant.struttura.sale.splice($scope.current_sala,1);
							    	$scope.updateStructure();
							    })
							}, function() {
							    
							});
						}
					}

					

		       	}
		       	/**
				* END Structure
				*/

});