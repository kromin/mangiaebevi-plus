var UserDaProvareController = angular.module('AppUserDaProvareController',[]);

UserDaProvareController
.controller('UserDaProvareCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams, SocialServices, ToolsServices, UserServices){
	"ngInject";

	$scope.user = null;
	$scope.init = function(user){
		$scope.user = user;
		
	};

	$scope.initDaProvare = function(user){
		$scope.loadDaProvare(user);
	}

	$scope.daprovare = [];
	$scope.loadDaProvare = function(user){
		UserServices.getWishlist.async(user).success(function(data){
			$scope.daprovare = data.wishlist;
		});
	};

	$scope.setProvato = function(restaurant){
		UserServices.addRemoveWishlist.async(restaurant).success(function(data) {
            if (data.message == 'ok') {                
                angular.forEach($scope.daprovare,function(v,k){
                	if(v._id == restaurant) $scope.daprovare.splice(k,1);
                });
            }
        });
	};
	
});