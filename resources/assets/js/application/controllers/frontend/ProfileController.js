var ProfileController = angular.module('AppProfileController',[]);

ProfileController
.controller('ProfileCtrl', function($scope,$rootScope,$cookies,$sce,$location,$routeParams,ngDialog,alertify,UserServices,NotificationsServices,ReservationServices){
	"ngInject";
	$scope.favourites = [];	
	$scope.itsme = false;

	if(!$routeParams.user_id){
		$scope.itsme = true;
		$scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;
		$scope.view_user = $scope.current_user;

		if($scope.current_user.loggedStatus == 0){
			$location.path('/');
		} 

		

		$scope.notification_default_num = 10;
		$scope.notification_page = 0;
		$scope.notification_hide_loader = false;
		$scope.loadMoreNotifications = function(){
			NotificationsServices.viewMine.async($scope.current_user.user._id,parseInt($scope.notification_page*$scope.notification_default_num),$scope.notification_default_num).success(function(data){
				angular.forEach(data.notifications,function(n,k){
					$scope.notifications.push(n);
				});

				if(data.notifications.length == $scope.notification_default_num){
					$scope.notification_page++;
					$scope.notification_hide_loader = false;
				} else {
					$scope.notification_hide_loader = true;
				}
			});
		};

		$scope.firstNotificationLoad = function(){
			NotificationsServices.viewMine.async($scope.current_user.user._id,parseInt($scope.notification_page*$scope.notification_default_num),$scope.notification_default_num).success(function(data){
				$scope.notifications = data.notifications;
				//$scope.notifications = data.notifications.length;
				//$scope.$parent.notifications = $scope.notifications;

				if(data.notifications.length == $scope.notification_default_num){
					$scope.notification_page++;
					$scope.notification_hide_loader = false;
				} else {
					$scope.notification_hide_loader = true;
				}
			});
		}

		


		if($scope.current_user.loggedStatus && $scope.current_user.loggedStatus == 1){

			$scope.firstNotificationLoad();

			UserServices.getFavourites.async($scope.current_user.user._id).success(function(data){
				$scope.favourites = data.favourites;
			});

			ReservationServices.viewMine.async().success(function(data){
				if(data.message == 'ok'){
					$scope.reservations = data.reservations;
				}			
			})

		}	

		$scope.$on('userLogged', function(event) { 
			// se l'utente ha fatto il login mettilo in scope
			$scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;
			if($scope.current_user.loggedStatus == 0){
				$location.path('/');
			}		
		});

		$scope.loading = false;
		$scope.current_tab = 'notifications';
		$scope.setTab = function(tab){
			$scope.current_tab = tab;
		}


		


		$scope.updating_user_profile = false;
		$scope.updateInfo = function(){
			$scope.updating_user_profile = true;
			UserServices
			.changeInfo
			.async($scope.current_user.user._id,$scope.current_user.user.name,$scope.current_user.user.surname,$scope.current_user.user.phone,$scope.current_user.user.description)
			.success(function(){
				if($scope.current_user.user.address && $scope.current_user.user.address != '' && $scope.current_user.user.born && $scope.current_user.user.born != ''){
					$scope.completeUserInfo();
				} else {
					$scope.updating_user_profile = false;
					$scope.$parent.profile = $scope.profile_child
				}
				
			})
			
		}

		
		$scope.uploading_avatar = false;
		$scope.change_avatar = {
			avatar: ''
		}
		$scope.uploadAvatar = function(){
			$scope.uploading_avatar = true;
			
			if($scope.change_avatar.avatar){
				$scope.loading = true;
				
				UserServices
				.modAvatar
				.async($scope.current_user.user._id,$scope.change_avatar.avatar)
				.success(function(data){
					$scope.loading = false;
					$scope.change_avatar = {};
					$scope.$parent.current_user.user.avatar = data.image;
					$scope.uploading_avatar = false;
				})
			}
		}


		$('#datetimepicker-day').livequery(function(){
			$(this).datetimepicker({
		    	locale: 'it',
		        //useCurrent: true,
		        defaultDate: ($scope.current_user.user.born && $scope.current_user.user.born != '') ? moment($scope.current_user.user.born).format("DD/MM/YYYY") : '',
		        format: 'DD/MM/YYYY'
		    }).on("dp.change", function (e) {	
		       
		    	updateScope();
	        })
		});
		var updateScope = function(){
			
			$scope.current_user.user.born = moment($('#datetimepicker-day').val(),"DD/MM/YYYY").format("YYYY-MM-DD");

		}

		
		$scope.completeUserInfo = function(){
			
			UserServices
				.completeProfile
				.async(
					$scope.current_user.user._id,
					$scope.current_user.user.address,
					$scope.current_user.user.city,
					$scope.current_user.user.state,
					$scope.current_user.user.zip,
					$scope.current_user.user.born)
				.success(function(data){
					$scope.updating_user_profile = false;
					$scope.$parent.profile = $scope.profile_child
				})
		}


		

		$scope.accettaCambioData = function(reservation){
			ReservationServices
			.approveChangeDate
			.async(reservation.restaurant,reservation._id,reservation.unique_string).success(function(data){
				if(data.message == 'ok'){
					angular.forEach($scope.reservations,function(v,k){
						if(v._id == reservation._id){
							$scope.reservations[k] = data.reservation;
						}
					});
				}
			});
		}

		$scope.change_pwd = {
			old: '',
			new_pwd: '',
			new_confirm: ''
		}
		$scope.change_mail = {
			new_mail: ''
		}

		$scope.changePWD = function(){
			UserServices.changePwd.async($scope.current_user.user._id,$scope.change_pwd.old,$scope.change_pwd.new_pwd,$scope.change_pwd.new_confirm).success(function(data){
				$scope.change_pwd = {
					old: '',
					new_pwd: '',
					new_confirm: ''
				}
			});
		}
		$scope.changeMail = function(){
			UserServices.changePwd.async($scope.current_user.user._id,$scope.change_mail.new_mail).success(function(data){
				$scope.change_mail = {
					new_mail: ''
				}
			});
		}

		$scope.cancellaAccount = function(){
			alertify
			.okBtn("Si, cancellami")
				.cancelBtn("No, ho sbagliato")
				.confirm("Sicuro di voler cancellare il tuo account?", function () {			    
		        
					UserServices.removeSingle.async($scope.current_user.user._id).success(function(data){
						UserServices.logout.async();
						$location.$path("/");
					});
			    
			}, function() {
			    // user clicked "cancel"
			});
		}




	} else {
		// c'è il route param, sto vedendo un altro utente
		$scope.itsme = false;

		$scope.current_tab = 'favourite';
		UserServices.getFavourites.async($routeParams.user_id).success(function(data){
			$scope.favourites = data.favourites;
		});

		UserServices.viewSingle.async($routeParams.user_id).success(function(data){
			$scope.view_user = {user: data.user};
		});

		$scope.current_user

	}



});