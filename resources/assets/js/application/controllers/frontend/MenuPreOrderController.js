var MenuPreOrderController = angular.module('AppMenuPreOrderController', []);

MenuPreOrderController
    .controller('MenuPreOrderCtrl', 
        function($scope, $rootScope, $cookies, $sce, $location, $route, $routeParams, $document, ngDialog, RestaurantsServices, ToolsServices, OrdersServices) {
        "ngInject";
        
        $scope.restaurant = null;
        $scope.reservation = null;
        $scope.user = null;

        $scope.menu_restaurant = null;
        $scope.canPreOrder = true;

        $scope.initMenuPreorder = function(restaurant, reservation, user){

            $scope.restaurant = restaurant;
            $scope.reservation = reservation;
            $scope.user = user;

            RestaurantsServices.viewSingle.async(restaurant).success(function(data){
                
                console.log('ristorante', data);
                $scope.menu_restaurant = data.restaurant;
                $scope.selectMenu(false);

                angular.element("#menoo, #cart_form").removeClass("hidden");

            }).error(function(){
                swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Oops',
                    text: 'Qualcosa è andato storto',
                    type: 'error'
                }); 
            })

            // Menu
            $scope.currentMenu = null; // primo menu selezionato

            
            $scope.selectMenu = function(indice) { 

                if(indice !== 0){
                    // prendi il primo menu ordinabile
                    if(!indice){
                        angular.forEach( $scope.menu_restaurant.menu.menus, function(menu, index){
                            if(menu.orderready == 1 && !indice && indice !== 0) indice = parseInt(index);
                        });
                    }

                    if(!indice && indice !== 0) {
                        $scope.canPreOrder = false;
                        return;
                    }
                }

                $scope.currentMenu = indice;
                $scope.current_order = {
                        price: 0,
                        plates: [],
                        spedizione: 0.00,//3.00, // costo della spedizione perchè un tempo doveva fare il delivery
                        subtotal: 0,
                        menu_qty: 0,
                        menu_type: ($scope.menu_restaurant.menu.menus[indice].menu_type) ? $scope.menu_restaurant.menu.menus[indice].menu_type : 0,
                        menu: indice,
                        menu_name: $scope.menu_restaurant.menu.menus[indice].name,
                        restaurant: $scope.menu_restaurant._id
                    }
                    /**
                     * Impostazioni per visualizzazione menu testuale/visuale
                     */
                $scope.menu_type = 0; // imposto di default quello testuale
                angular.forEach($scope.menu_restaurant.menu.menus[indice].categorie, function(v, k) {
                    angular.forEach(v.piatti, function(plate, key) {
                        //console.log('plate', plate);
                        if (plate.image && plate.image != null) {
                            $scope.visual_menu = 1;
                            return;
                        }
                    });
                });
            }

            
            $scope.scrollToMenu = function() {
                var menuElement = angular.element(document.getElementById('menuContainer'));
                $document.scrollToElement(menuElement, 100, 500);
            }

            $scope.menu_type = 0;
            $scope.setMenuType = function(type) {
                $scope.menu_type = type;
            }





            /**
             * Ordinazioni
             */
            var resetPlate = function() {
                $scope.current_plate = {
                    qty: 1, // quantità di porzioni per piatto corrente
                    total_price: 0
                }; // piatto aperto nel modale
            }

            $scope.editPlate = function(plate) {
                resetPlate();

                $scope.current_plate = plate;
                $scope.plate_dialog = ngDialog.open({
                    template: '/templates/dialogaddplate',
                    scope: $scope,
                    showClose: false
                }).closePromise.then(function(data) {
                    calculateOrderPrice();
                });

            }

            $scope.openModalPlate = function(plate) {
                resetPlate();
                $scope.current_plate = plate;
                $scope.plate_dialog = ngDialog.open({
                    template: '/templates/dialogaddplate',
                    scope: $scope,
                    showClose: false
                }).closePromise.then(function(data) {
                    // quando chiudi aggiungi il piatto
                    $scope.addPlateFromModal();
                });
            }

            $scope.addPlateFromModal = function() {
                // $scope.current_plate
                var pl = angular.copy($scope.current_plate);
                angular.forEach(pl.options, function(v, k) {
                    angular.forEach(v, function(opzione, chiave) {
                        if (opzione.selected && opzione.selected == 1) {
                            // imposta valore per fare check successivo
                            $scope.current_plate.options_selected = 1;
                        }
                    });
                });
                resetPlate();
                $scope.current_order.plates.push(pl);
                calculateOrderPrice();
            }

            $scope.checkUncheckPlateOption = function(cat, opt) {
                //console.log('type', $scope.current_plate.options[cat].type)
                switch (parseInt($scope.current_plate.options[cat].type)) {
                    case 0: // aggiunge opzione
                        console.log('errore')
                        $scope.current_plate.options[cat].options[opt].selected = (!$scope.current_plate.options[cat].options[opt].selected || $scope.current_plate.options[cat].options[opt].selected == 0) ? 1 : 0;
                        break;
                    case 1: // toglie altre opzioni e aggiunge questa
                        console.log('ok')
                        if ($scope.current_plate.options[cat].options[opt].selected == 1) {
                            $scope.current_plate.options[cat].options[opt].selected = 0;
                        } else {
                            console.log('prova')
                            angular.forEach($scope.current_plate.options[cat].options, function(opzione, chiave) {
                                if (opzione.selected == 1) $scope.current_plate.options[cat].options[chiave].selected = 0;
                            });
                            $scope.current_plate.options[cat].options[opt].selected = 1;
                        }
                        break;
                    default:
                        console.log('default')
                        break;
                }
                calculatePlatePrice();
            }

            $scope.addQtyToSingleOption = function(cat, opt, max) {
                if (!$scope.current_plate.options[cat].options[opt].qty) {
                    $scope.current_plate.options[cat].options[opt].qty = 1;
                    calculatePlatePrice();
                    return;
                }
                $scope.current_plate.options[cat].options[opt].qty = ($scope.current_plate.options[cat].options[opt].qty < max) ? $scope.current_plate.options[cat].options[opt].qty + 1 : $scope.current_plate.options[cat].options[opt].qty;
                calculatePlatePrice();
            }

            $scope.removeQtyToSingleOption = function(cat, opt) {
                if (!$scope.current_plate.options[cat].options[opt].qty) {
                    $scope.current_plate.options[cat].options[opt].qty = 0;
                    calculatePlatePrice();
                    return;
                }
                $scope.current_plate.options[cat].options[opt].qty = ($scope.current_plate.options[cat].options[opt].qty > 0) ? $scope.current_plate.options[cat].options[opt].qty - 1 : $scope.current_plate.options[cat].options[opt].qty;
                calculatePlatePrice();
            }



            $scope.moreQty = function() {
                $scope.current_plate.qty++;
                calculatePlatePrice();
            }

            $scope.lessQty = function() {
                $scope.current_plate.qty = ($scope.current_plate.qty > 1) ? $scope.current_plate.qty - 1 : $scope.current_plate.qty;

                angular.forEach($scope.current_plate.options, function(v, k) {
                    angular.forEach(v.options, function(val, key) {
                        if (val.qty > $scope.current_plate.qty) $scope.current_plate.options[k].options[key].qty = $scope.current_plate.qty;
                    })
                })

                calculatePlatePrice();
            }

            
            var calculatePlatePrice = function() {
                $scope.current_plate.total_price = $scope.current_plate.qty * $scope.current_plate.price;
                // calcola variazione
                angular.forEach($scope.current_plate.options, function(val, key) {
                    angular.forEach(val.options, function(single_option, key_val) {
                        if (single_option.selected && single_option.selected == 1) {
                            if (single_option.no_variation === 0) {
                                if (single_option.variation === 1) {
                                    $scope.current_plate.total_price += parseFloat((single_option.price * -1) * $scope.current_plate.qty);
                                } else {
                                    $scope.current_plate.total_price += parseFloat(single_option.price * $scope.current_plate.qty);
                                }
                            }

                        }
                    })
                })
            }

            

            $scope.addPlateToOrder = function(cat, plate) {
                
                // verifica se il piatto è già nel carrello
                var just_added = false,
                    pl = angular.copy($scope.menu_restaurant.menu.menus[$scope.currentMenu].categorie[cat].piatti[plate]);
                pl.qty = 1;
                pl.fasce = null;
                pl.total_price = parseFloat(pl.price);
                resetPlate();

                // se ha delle opzioni apri il modal per farle selezionare
                if (pl.options && pl.options.length > 0) {
                    $scope.openModalPlate(pl);
                    return;
                }

                // se non ha opzioni carica direttamente nel carrello
                angular.forEach($scope.current_order.plates, function(v, k) {
                    if (v.id == pl.id && (!v.options_selected || v.options_selected == 0)) {
                        // sono identici, li sommo tra loro     
                        just_added = true;
                        $scope.current_order.plates[k].qty += 1;
                        $scope.current_order.plates[k].total_price += parseFloat(pl.total_price);
                    }
                });
                if (!just_added) {
                    $scope.current_order.plates.push(pl);
                }
                calculateOrderPrice();


            }

            

            var calculateOrderPlatePrice = function(index) {
                $scope.current_order.plates[index].total_price = $scope.current_order.plates[index].qty * parseFloat($scope.current_order.plates[index].price);

                // calcola le variazioni
                angular.forEach($scope.current_order.plates[index].options, function(val, key) {
                    angular.forEach(val.options, function(single_option, key_val) {
                        if (single_option.selected && single_option.selected == 1) {
                            if (single_option.no_variation == 0) {
                                if (single_option.variation == 1) {
                                    $scope.current_order.plates[index].total_price += parseFloat((single_option.price * -1) * $scope.current_order.plates[index].qty);
                                } else {
                                    $scope.current_order.plates[index].total_price += parseFloat(single_option.price * $scope.current_order.plates[index].qty);
                                }
                            }

                        }
                    })
                })

                calculateOrderPrice();
            }

            var calculateOrderPrice = function() {
                $scope.current_order.subtotal = 0;
                angular.forEach($scope.current_order.plates, function(v, k) {
                    $scope.current_order.subtotal += parseFloat(v.total_price);
                });
                $scope.current_order.price = $scope.current_order.subtotal + $scope.current_order.spedizione;

                // se ordine confermato il prezzo minimo è da impostare
                if ($scope.current_order.price < $scope.minimum_order && $scope.order_confirmed == 1) {
                    $scope.current_order.price = $scope.minimum_order
                }
            }

            $scope.moreQtyOrder = function(index) {
                $scope.current_order.plates[index].qty++;
                calculateOrderPlatePrice(index);
            }
            $scope.lessQtyOrder = function(index) {
                $scope.current_order.plates[index].qty = ($scope.current_order.plates[index].qty > 1) ? $scope.current_order.plates[index].qty - 1 : $scope.current_order.plates[index].qty;
                // riduci quantità delle opzioni se superiori alle porzioni del piatto
                angular.forEach($scope.current_order.plates[index].options, function(v, k) {
                    angular.forEach(v.options, function(val, key) {
                        if (val.qty > $scope.current_order.plates[index].qty) $scope.current_order.plates[index].options[k].options[key].qty = $scope.current_order.plates[index].qty;
                    })
                })

                calculateOrderPlatePrice(index);
            }
            $scope.removePlateFromOrder = function(index) {
                $scope.current_order.plates.splice(index, 1);
                calculateOrderPrice();
            }

            /**
             * Order
             */
            var client;

            $scope.orderStep = 0; // 0 no, 1=>login, 2=>spedizione, 3=>pagamento, 4=>grazie vedi

            $scope.order_confirmed = 0; // open payment
            $scope.need_login = 0; // 0 no nedd, 1 need login
            $scope.login_form = 1;

            $scope.confirmOrder = function() {

                ToolsServices.mainOverlay.show();
                OrdersServices.addPreOrder
                    .async(
                        $scope.restaurant,
                        $scope.reservation,
                        $scope.current_order.menu,
                        $scope.current_order.menu_qty,
                        $scope.current_order.menu_name,
                        $scope.current_order.plates,
                        $scope.current_order.subtotal,
                        $scope.current_order.price
                    ).success(function(data) {
                        
                        ToolsServices.mainOverlay.hide();
                        if (data.message == 'ok') {
                            swal({
                                buttonsStyling: false,
                                confirmButtonClass: 'btn btn-primary btn-sm ghost',
                                title: 'Congratulazioni',
                                text: 'Il preordine è stato salvato ed associato alla tua prenotazione, vai al tuo profilo per i dettagli.<br /><br />Vuoi pagare in anticipo? <a href="/orders/pay_single/'+$scope.reservation+'">Clicca qui</a>',
                                type: 'success'
                            });                             
                        }
                    }).error(function(e) {
                        ToolsServices.mainOverlay.hide();
                        var message = 'Qualcosa è andato storto';
                        if(e.error == 'just_added'){
                            message = 'Già hai effettuato il preordine per questa prenotazione, torna al tuo profilo per i dettagli';
                        } else if(e.error == 'just_confirmed' ){
                            message = 'Non puoi aggiungere un preordine se la tua prenotazione non risulta da confermare';
                        } else if(e.error == 'not_valid_order' ){
                            message = 'Nella verifica del preordine abbiamo riscontrato un errore, per favore contattaci per avere assistenza';
                        }
                        swal({
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary btn-sm ghost',
                            title: 'Oops',
                            text: message,
                            type: 'error' 
                        }); 
                    })

            }

            $scope.payReservation = function(reservation) {
                if(!reservation.preorder || (reservation.preorder.paid && reservation.preorder.paid != 0)) return;
                ToolsServices.mainOverlay.show();
                ReservationServices
                .payPreorder  
                .async(reservation).success(function(data){
                    if(data.message == 'ok'){
                        window.location = data.paypal_link;
                    } else {
                        swal({
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary btn-sm ghost',
                            title: 'Oops!',
                            text: 'Qualcosa è andato storto',
                            type: 'error'
                        });
                        ToolsServices.mainOverlay.hide();
                    }           
                }).error(function(){
                    swal({
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-primary btn-sm ghost',
                        title: 'Oops!',
                        text: 'Qualcosa è andato storto',
                        type: 'error'
                    });
                    ToolsServices.mainOverlay.hide();
                });
            }

            $scope.backToMenu = function() {
                $scope.order_confirmed = 0;
            }

            /**
             * Menu fisso
             * si possono scegliere le opazioni ma non hanno mai variazioni sul prezzo (è un menu fisso)
             * @return {[type]} [description]
             */
            var calculateFixedMenuPrice = function(){
                $scope.current_order.price = ($scope.menu_restaurant.menu.menus[$scope.currentMenu].menu_price*$scope.current_order.menu_qty);
                
                var to_change = calculateOptionsVariations();
                $scope.current_order.price += to_change;
                $scope.current_order.subtotal = $scope.current_order.price;
            }

            var calculateOptionsVariations = function(){
                var price_to_add = 0;
                angular.forEach($scope.current_order.plates, function(plate, i_plate){
                    
                    if(plate.options.length > 0){

                        angular.forEach(plate.options, function(opt, i_opt){
                            if(plate.options[i_opt]['options'].length > 0){
                                angular.forEach(plate.options[i_opt]['options'], function(opzione, indice_opzione){
                                    console.log(opzione);
                                    if(opzione.num > 0 && opzione.no_variation == 0){
                                        if(opzione.variation == 0) price_to_add += opzione.num*opzione.price;
                                        if(opzione.variation == 1) price_to_add -= opzione.num*opzione.price;
                                    }
                                });
                            }    
                        })

                    }
                    /*                    
                        if(plate.options[0]['options'].length > 0){
                            angular.forEach(plate.options[0]['options'], function(opzione, indice_opzione){
                                if(opzione.num > 0 && opzione.no_variation == 0){
                                    if(opzione.variation == 0) price_to_add += opzione.num*opzione.price;
                                    if(opzione.variation == 1) price_to_add -= opzione.num*opzione.price;
                                }
                            });
                        }
                    */

                }); 
                return price_to_add;
            }

            /**
             * Qualcosa qui va in errore, da vedere
             * @param {[type]} plate         [description]
             * @param {[type]} max           [description]
             * @param {[type]} last_modified [description]
             */
            $scope.setPlateOptionsFixedMenu = function(plate, max){
                // ricalcola il numero di opzioni disponibili
                var total = max;                
                
                angular.forEach(plate.options, function(opt, i_opt){
                    var n_scelte = 0;
                    
                    angular.forEach(plate.options[i_opt]['options'], function(option, index){
                        var n = option.num; // quante sono state scelte
                        n_scelte += n;
                        console.log('scelta', n)
                    });

                    var to_edit = 0;                
                
                    if(total > n_scelte){
                        plate.options[i_opt]['options'][to_edit].num += total-n_scelte;
                    } else if(total < n_scelte){
                        while(plate.options[i_opt]['options'][to_edit].num <= 0){ 
                            to_edit++; 
                        }
                        plate.options[i_opt]['options'][to_edit].num -= n_scelte-total;
                    }

                });
                
                return plate;
            } 

            $scope.addMenuFisso = function(menu){

                if($scope.current_order.menu_qty > 0) {
                    $scope.moreQtyFixedMenu();
                    return;
                }

                angular.forEach(menu.categorie, function(val, i_cat){
                    angular.forEach(val.piatti, function(plate, i_plate){
                        pl = angular.copy(plate);
                        
                        if(pl.options.length > 0){
                            angular.forEach(pl.options, function(opt, i_opt){
                                if(pl.options[i_opt]['options'].length > 0){
                                    angular.forEach(pl.options[i_opt]['options'], function(op, n){
                                        pl.options[i_opt]['options'][n].num = 0;
                                    })
                                    pl.options[i_opt]['options'][0].num = 1;
                                }    
                            })
                            
                        }
                        $scope.current_order.plates.push(pl) 
                    }); 
                });
                $scope.current_order.menu_qty++;
                calculateFixedMenuPrice();
            }

            $scope.updateFixedOptions = function(){
                angular.forEach($scope.current_order.plates, function(plate, i_plate){
                    if(plate.options.length > 0){
                        angular.forEach(plate.options, function(opt, i_opt){
                            if(plate.options[i_opt]['options'].length > 0){
                                var pl = $scope.setPlateOptionsFixedMenu(plate, $scope.current_order.menu_qty);
                                $scope.current_order.plates[i_plate] = pl;
                            }
                        })
                    }
                    /*                    
                    if(plate.options[0]['options'].length > 0){
                        var pl = $scope.setPlateOptionsFixedMenu(plate, $scope.current_order.menu_qty);
                        $scope.current_order.plates[i_plate] = pl;
                    }
                    */
                }); 
                calculateFixedMenuPrice();
            }

            $scope.moreQtyFixedMenu = function(){
                $scope.current_order.menu_qty++;
                $scope.updateFixedOptions();
                calculateFixedMenuPrice();
            }
            
            $scope.lessQtyFixedMenu = function(){
                if($scope.current_order.menu_qty > 1){
                    $scope.current_order.menu_qty--;
                    $scope.updateFixedOptions();
                    calculateFixedMenuPrice();
                } else {
                    $scope.current_order.plates = [];
                    $scope.current_order.menu_qty = 0;
                    $scope.current_order.price = 0.00;
                    $scope.current_order.subtotal = 0.00;
                }

            }


            $scope.removeFixedMenu = function(){
                $scope.current_order.plates = [];
                $scope.current_order.menu_qty = 0;
                $scope.current_order.price = 0.00;
                $scope.current_order.subtotal = 0.00;
            }

            $scope.moreQtyOption = function(opzione, piatto, i_opzione) {
                $scope.current_order.plates[piatto].options[i_opzione].options[opzione].num++;
                $scope.updateFixedOptions();
            }

            $scope.lessQtyOption = function(opzione, piatto, i_opzione) {
                if($scope.current_order.plates[piatto].options[i_opzione].options[opzione].num > 0){
                    $scope.current_order.plates[piatto].options[i_opzione].options[opzione].num--;
                }
                $scope.updateFixedOptions();
            }










        }
});