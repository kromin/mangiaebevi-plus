var UserController = angular.module('AppUserController',[]);

UserController
.controller('UserCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams,ngDialog,alertify,UserServices,NotificationsServices,ReservationServices,ToolsServices,SocialServices,RecommendationServices,SearchServices, PlatesServices, RestaurantsServices, PaymentServices){
	"ngInject";
	$scope.favourites = [];	
	$scope.itsme = false;
	$scope.cover_image = null;
	$scope.editing_address = 0;
	$scope.pagination_number = 5;

  
	$scope.page_user = null;

	ToolsServices.getMainSiteInfo.async().success(function(data) {
        $scope.creditCard = _.indexBy(data.creditCard, '_id');
        $scope.homeCities = _.indexBy(data.homeCities, '_id');
        $scope.regioniCucine = _.indexBy(data.regioniCucine, '_id');
        $scope.tipiCucine = _.indexBy(data.tipiCucine, '_id');
        $scope.tipiLocali = _.indexBy(data.tipiLocali, '_id');
        $scope.services = _.indexBy(data.services, '_id');
        $scope.pacchetti = _.indexBy(data.pacchetti, 'price_month');
        $scope.staticURL = data.staticDomain; // domain for static files    
    });

	$scope.loadUser = function(u,domain){
		$scope.page_user = u;		
		//$scope.initPage();
	}	

	$scope.initPage = function(){			
		
		var hash = $location.path();
		if(!hash){
			$location.path("bacheca"); 			
		}

		$scope.convertHashToTab($location.path());
	};

	
	/**
	 * Carica le tab solo se verifichi la loggatura per evitare errore
	 * 
	 */
	$scope.$on('userLogged', function(event) { 
		$scope.initPage();
		$scope.$on('$locationChangeSuccess', function(){
			$scope.initPage();
		});
	});
		
	$scope.convertHashToTab = function(hash){
		var tab = 0;
		switch(hash){
			case '/':
			tab = 0;
			break;
			case '/bacheca':
			tab = 0;
			break;
			case '/preferiti':
			tab = 9;
			break;
			case '/da-provare':
			tab = 4;
			break;
			case '/follower-following':
			tab = 2;
			break;
			case '/raccomandazioni':
			tab = 1;
			break;
			case '/immagini-dei-piatti':
			tab = 3;
			break;
			case '/prenotazioni':
			tab = 5;
			break;
			case '/notifiche':
			tab = 6;
			break;
			case '/account':
			tab = 7;
			break;
			case '/ristoranti':
			tab = 8;
			break;
			default:
			tab = 0;
			break;
		}
		$scope.setTab(tab);
	}

	$scope.loading = false;
	//$scope.current_tab = 0;
	$scope.setTab = function(tab){
		$scope.current_tab = tab;
	}
	

});