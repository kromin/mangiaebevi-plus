var UserRestaurantsController = angular.module('AppUserRestaurantsController',[]);

UserRestaurantsController
.controller('UserRestaurantsCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams, RestaurantsServices){
	"ngInject";

	
	$scope.initRestaurants = function(){
		$scope.loadRestaurantsUser();
	};

	$scope.user_restaurants = [];
	$scope.loadRestaurantsUser = function(){
		
		RestaurantsServices.viewAllMine.async().success(function(data){
			$scope.user_restaurants = data.restaurants;
		});
		
	};
	
});