var UploadController = angular.module('AppUploadController',[]);

UploadController
.controller('UploadCtrl', function($scope,$rootScope,$route,$cookies,$sce,$location,UserServices,ToolsServices){
	"ngInject";
	$scope.view_page = false;

	$scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;
	if($scope.current_user.user && $scope.current_user.user.role > 2){
		$scope.view_page = true;
	}


	$scope.$on('userLogged', function(event, param) { 
		$scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;
		if($scope.current_user.user &&  $scope.current_user.user.role > 2){
			$scope.view_page = true;
		} else {
			$scope.view_page = false;
		}
	});

	$scope.restaurants = [];
	$scope.init = function(){
		ToolsServices.mainOverlay.show();
		ToolsServices.getInvalidRestaurants.async().success(function(d){
			$scope.restaurants = d.restaurants;
			console.log(d)
			ToolsServices.mainOverlay.hide();
		}).error(function(){
			ToolsServices.mainOverlay.hide();
		})
	}


	$scope.add_image = {
		image: ''
	}
	var reset = function(){
		$scope.add_image = {
			image: ''
		}
	}
	$scope.uploadImage = function(old,restaurant){
		ToolsServices.mainOverlay.show();
		if($scope.add_image.image && $scope.add_image.image != ''){
			ToolsServices
			.saveRestaurantImage
			.async(old,$scope.add_image.image)
			.success(function(data){
				if(data.message == 'ok'){
					reset();
				$("#"+restaurant+"_"+old).remove();
				
				
			} else {
				$scope.simpleError();
			}
				ToolsServices.mainOverlay.hide();
			}).error(function(data){
				$scope.simpleError();
				reset();
				ToolsServices.mainOverlay.hide();
			})
		}
	}


	$scope.simpleError = function(){
		swal({
			buttonsStyling: false,
			confirmButtonClass: 'btn btn-primary btn-sm ghost',
			title: 'Oops!',
			text: 'Qualcosa è andato storto',
			type: 'error'
		});
	}

});