var StaticController = angular.module('AppStaticController',[]);

StaticController
.controller('StaticCtrl', function($scope,$rootScope,$route,$cookies,$sce,$location,UserServices,ToolsServices){
	"ngInject";
	$scope.contact_error = false;
	$scope.contact_ok = false;

	$scope.$on("$routeChangeSuccess",function(){
		window.scrollTo(0,0);
	})

	var resetContact = function(){
		$scope.contact = {
			name: null,
			email: null,
			message: null,
			phone: null,
			subject: null,
			address: null,
			risto: null
		}
	}
	resetContact();

	$scope.contactSubmit = function(){
		switch($route.current.templateUrl){
			case '/templates/assistenza': case '/templates/contact':
			$scope.contact.subject = 0; // simple contact
			break;
			case '/templates/job':
			$scope.contact.subject = 1; // menoo driver
			break;
			case '/templates/addrestaurant':
			$scope.contact.subject = 2; // add restaurant
			break;
		}
		ToolsServices.contact.async($scope.contact).success(function(data){
			if(data.message == 'ok'){
				$scope.contact_ok = 1;
				$scope.contact_error = false;
				resetContact();
			} else {
				$scope.contact_error = 1;
				$scope.contact_ok = false;
			}
		}).error(function(e){
			$scope.contact_error = 1;
		})
	}


	
    
    $scope.video_url = $sce.trustAsResourceUrl($scope.mainUrl+'/video/app.mp4');
	
	
	$scope.playVideo = function(){
		$(".video_section").toggleClass("playing");
		var elem = document.getElementById("video_player");
		if (elem.requestFullscreen) {
		  elem.requestFullscreen();
		} else if (elem.mozRequestFullScreen) {
		  elem.mozRequestFullScreen();
		} else if (elem.webkitRequestFullscreen) {
		  elem.webkitRequestFullscreen();
		}
		elem.play();
	} 

	$scope.pauseVideo = function(){
		var elem = document.getElementById("video_player");
		elem.pause();
		$(".video_section").toggleClass("playing");
	} 

	//video.addSource('mp4', $scope.mainUrl+'/video/howto.mp4');
		
});