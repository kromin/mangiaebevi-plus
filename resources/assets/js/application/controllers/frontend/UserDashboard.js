var UserDashboardController = angular.module('AppUserDashboardController',[]);

UserDashboardController
.controller('UserDashboardCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams, SocialServices, ToolsServices){
	"ngInject";

	$scope.user = null;
	$scope.init = function(user){
		$scope.user = user;
		$scope.initDashboard();
	};

	$scope.initDashboard = function(){
		$scope.dashboard_pagination = 1;
		$scope.user_dashboard = [];
		$scope.stop_load_dashboard_pagination = false;
				
		$scope.dashboard_pagination = 1;
		
		$scope.loadDashboard();
	};

	$scope.show_button = false;

	$scope.loadDashboard = function(){
		$scope.show_button = false;
		if($scope.dashboard_pagination == 1) $scope.user_dashboard = [];
		var load_obj = {
            user: $scope.user,
            skip: ($scope.dashboard_pagination == 1) ? 0 : parseInt( $scope.dashboard_pagination-1 )*parseInt( $scope.pagination_number ),
            take: parseInt( $scope.pagination_number )
        };
        SocialServices.viewDashboard.async(load_obj).success(function(data){

            angular.forEach(data.actions,function(v,k){
            	$scope.user_dashboard.push(v);
            });
            if(data.actions.length < $scope.pagination_number){
                $scope.stop_load_dashboard_pagination = true;
                $scope.show_button = false;
            } else {
            	$scope.show_button = true;
                $scope.dashboard_pagination++;
            }
        });
	};

});