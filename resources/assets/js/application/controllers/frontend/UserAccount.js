var UserAccountController = angular.module('AppUserAccountController',[]);

UserAccountController
.controller('UserAccountCtrl', 
	function($scope,$rootScope,$cookies,$sce,$location,$routeParams, ToolsServices, UserServices){
	"ngInject";

	
	$scope.initAccount = function(){
		$scope.current_user = UserServices.isCurrentLogged.isLoggedCurrently;		
		if(!$scope.current_user.user.allergeni) $scope.current_user.user.allergeni = [];
	};

	$scope.allergeni = ToolsServices.allergeni.retrieve();

	$scope.profile_tab = 0;
	$scope.setProfileTab = function(n){
		$scope.profile_tab = n;
	};

	$scope.updateInfo = function(){
		ToolsServices.mainOverlay.show();
		UserServices
		.changeInfo
		.async($scope.current_user.user._id,
			$scope.current_user.user.name,
			$scope.current_user.user.surname,
			$scope.current_user.user.phone,
			$scope.current_user.user.description)
		.success(function(data){			
			ToolsServices.mainOverlay.hide();
		}).error(function(data){
			$scope.simpleError();
			ToolsServices.mainOverlay.hide();
		});
		
	};


	$scope.addAllergeneToUser = function(index){
		console.log($scope.current_user.user.allergeni)
		var i = $scope.current_user.user.allergeni.indexOf($scope.allergeni[parseInt(index)]);
		if(i === -1) $scope.current_user.user.allergeni.push($scope.allergeni[parseInt(index)]);
		else $scope.current_user.user.allergeni.splice(i,1);
	}

	$scope.updateAllergeni = function(){
		ToolsServices.mainOverlay.show();
		UserServices
		.setAllergeni
		.async($scope.current_user.user.allergeni)
		.success(function(data){			
			ToolsServices.mainOverlay.hide();
		}).error(function(data){
			$scope.simpleError();
			ToolsServices.mainOverlay.hide();
		});
		
	};


	$('#datetimepicker-day').livequery(function(){
		$(this).datetimepicker({
	    	locale: 'it',
	        defaultDate: ($scope.current_user.user.born && $scope.current_user.user.born !== '') ? 
	        $scope.current_user.user.born : '',
	        format: 'DD/MM/YYYY'
	    }).on("dp.change", function (e) {	    
	    	updateScope();
        });
	});
	var updateScope = function(date){
		$scope.current_user.user.born = moment($('#datetimepicker-day').val(),"DD/MM/YYYY").format("YYYY-MM-DD");
	};

	
	$scope.completeUserInfo = function(){	
		ToolsServices.mainOverlay.show();	
		UserServices
		.completeProfile
		.async($scope.current_user.user._id,$scope.current_user.user.address,$scope.current_user.user.born)
		.success(function(data){
			ToolsServices.mainOverlay.hide();
			if(data.updated == 1){
				swal({
					buttonsStyling: false,
					confirmButtonClass: 'btn btn-primary btn-sm ghost',
					title: 'Congratulazioni',
					text: 'Profilo completato, ti sono stati accreditati i foodcoin',
					type: 'success'
				});
				angular.element("#userFoodcoin").text(data.foodcoin);
			}
		}).error(function(data){
			$scope.simpleError();
			ToolsServices.mainOverlay.hide();
		});
	};



	$scope.change_pwd = {
		old: '',
		new_pwd: '',
		new_confirm: ''
	};
	$scope.change_mail = {
		new_mail: '',
		confirm_new_mail: '' 
	};

	$scope.pwd_error = false;
	$scope.changePWD = function(){
		if($scope.change_pwd.old != $scope.change_pwd.new_pwd) {
			$scope.pwd_error = true;
		}
		ToolsServices.mainOverlay.show();
		UserServices.changePwd.async($scope.current_user.user._id,$scope.change_pwd.old,$scope.change_pwd.new_pwd,$scope.change_pwd.new_confirm).success(function(data){
			ToolsServices.mainOverlay.hide();
			$scope.pwd_error = false;
			$scope.change_pwd = {
				old: '',
				new_pwd: '',
				new_confirm: ''
			};
			swal({
				buttonsStyling: false,
				confirmButtonClass: 'btn btn-primary btn-sm ghost',
				title: 'Congratulazioni',
				text: 'Password modificata',
				type: 'success'
			});
		}).error(function(){
			$scope.simpleError();
			ToolsServices.mainOverlay.hide();
		});
	};

	$scope.mail_error = false;
	$scope.changeMail = function(){
		if($scope.change_mail.new_mail != $scope.change_mail.confirm_new_mail) {
			$scope.mail_error = true;
		}
		ToolsServices.mainOverlay.show();
		UserServices.changeMail.async($scope.current_user.user._id,$scope.change_mail.new_mail).success(function(data){
			$scope.mail_error = false;
			$scope.change_mail = {
				new_mail: '',
				confirm_new_mail: '' 
			};
			swal({
				buttonsStyling: false,
				confirmButtonClass: 'btn btn-primary btn-sm ghost',
				title: 'Congratulazioni',
				text: 'Email modificata',
				type: 'success'
			});
			ToolsServices.mainOverlay.hide();
		}).error(function(){
			$scope.simpleError();
			ToolsServices.mainOverlay.hide();
		});
	};

	$scope.cancellaAccount = function(){
		alertify
		.okBtn("Si")
			.cancelBtn("No")
			.confirm("Sicuro di voler cancellare il tuo account?", function () {			    
	        	ToolsServices.mainOverlay.show();
				UserServices.removeSingle.async($scope.current_user.user._id).success(function(data){
					UserServices.logout.async();
					ToolsServices.mainOverlay.hide();
					$location.$path("/");
				});
		    
		}, function() {
		    // user clicked "cancel"
		});
	};

	$scope.simpleError = function(){
		swal({
			buttonsStyling: false,
			confirmButtonClass: 'btn btn-primary btn-sm ghost',
			title: 'Oops!',
			text: 'Qualcosa è andato storto',
			type: 'error'
		});
	};




	$scope.settedPlans = false;
	$scope.returnUserPlan = function(){
		$scope.billings = true;

		if($scope.current_user.user){
			$scope.billings = ($scope.current_user.user.role >= 2 && $scope.current_user.user.billing_plan && $scope.current_user.user.billing_plan.active == 1) ? true : false;
			
			$scope.settedPlans = true;
			$scope.cancelBillingPlan = function(){
				alertify
				.okBtn("Si")
					.cancelBtn("No")
					.confirm("Sicuro di volerlo cancellare?", function () {			    
			        	ToolsServices.mainOverlay.show();
			        	PaymentServices.cancelPlan.async($scope.current_user.user.billing_plan.PROFILEID).success(function(d){
			        		$scope.current_user.user.billing_plan = [];
			        		$scope.user_billing_plan = {};
			        		ToolsServices.mainOverlay.hide();
			        	}).error(function(e){
			        		ToolsServices.mainOverlay.hide();
			        		$scope.simpleError();
			        	});
				    
				}, function() {
				    // user clicked "cancel"
				});
			};
		}

		ToolsServices.getMainSiteInfo.async().success(function(data) {
			$scope.pacchetti = _.indexBy(data.pacchetti, 'price_month');
			if($scope.itsme && $scope.current_user.user && $scope.current_user.user.billing_plan && $scope.current_user.user.billing_plan.plan && $scope.current_user.user.billing_plan.active == 1){
				$scope.user_billing_plan = {};
				angular.forEach($scope.pacchetti,function(v,k){
					
					if(v._id == $scope.current_user.user.billing_plan.plan) $scope.user_billing_plan = v;
				});
			}
        });
	};
	
});