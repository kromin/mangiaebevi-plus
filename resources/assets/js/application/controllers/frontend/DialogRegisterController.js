var DialogRegisterController = angular.module('AppDialogRegisterController', []);

DialogRegisterController
    .controller('DialogRegisterCtrl', function($scope, $rootScope, $cookies, $sce, $location, UserServices) {
        "ngInject";
        $scope.reserveFormData = {};
        $scope.make_dialog_action = function(action) {

            switch (action) {
                case 'reg':
                    UserServices.register.async(reserveFormData.email, reserveFormData.name, reserveFormData.surname, reserveFormData.password, reserveFormData.phone, 2).success(function(data) {
                        // ora loggalo
                        if (data.message == 'ok') { 

                            UserServices.login.async(reserveFormData.email, reserveFormData.password).success(function(data) {

                                if (data.message == 'ok') {

                                    UserServices.getLogged.async().success(function(data) {
                                        $scope.current_user = data
                                        $scope.$parent.current_user = $scope.current_user;

                                        // fai partire l'evento per prendere utente loggato
                                        $rootScope.$broadcast('userLogged', 'registration');
                                        $scope.closeThisDialog('registerSubmitted')
                                    })

                                }

                            })

                        }

                    });
                    break;
                case 'login':
                    $scope.closeThisDialog('toLogin')
                    break;
            }

        }
    });