var UserFriendsController = angular.module('AppUserFriendsController',[]);

UserFriendsController
.controller('UserFriendsCtrl',
	function($scope,alertify,SocialServices,UserServices,ToolsServices){
	"ngInject";

	$scope.user = null;
	$scope.initFriendsPage = function(user){
		$scope.user = user;
		$scope.initFriends();
	};

	$scope.loading_btn = true;

	// FRIENDS
	$scope.initFriends = function(){

		SocialServices.viewFriendRequest.async($scope.user).success(function(data){
			$scope.current_friends_request = data.friend_request;
		});


		$scope.current_friend_tab = 0;
		$scope.setFriendTab = function(n){
			$scope.current_friend_tab = n;
		};


		$scope.inFollowers = function(){
			$scope.follower_pagination = 1;
			$scope.user_follower = [];
			$scope.stop_load_follower_pagination = false;
			$scope.loadFollowers();
		};
		$scope.inFollowing = function(){
			$scope.following_pagination = 1;
			$scope.user_following = [];
			$scope.stop_load_following_pagination = false;
			$scope.loadFollowing();
		};

		/*$scope.initRequestFriend = function(){
			SocialServices.viewFriendRequest.async($scope.user).success(function(data){
				$scope.current_friends_request = data.friend_request;
			});
		};*/

		$scope.initViewFriend = function(){
			SocialServices.viewFriends.async($scope.user).success(function(data){
				$scope.current_friends = data.friends;
			});
		};


		$scope.acceptRequest = function(usr){
			SocialServices.acceptFriendRequest.async(usr).success(function(data){
				angular.forEach($scope.current_friends_request,function(v,k){
					if(v._id == usr) $scope.current_friends_request.splice(k,1);
				});
			});
		};
		$scope.refuseRequest = function(usr){
			SocialServices.refuseFriendRequest.async(usr).success(function(data){
				angular.forEach($scope.current_friends_request,function(v,k){
					if(v._id == usr) $scope.current_friends_request.splice(k,1);
				});
			});
		};

		$scope.removeFriend = function(f){
			alertify
			.okBtn("Si")
				.cancelBtn("No")
				.confirm("Sicuro di volerlo cancellare dai tuoi amici?", function () {
		        	SocialServices.removeFriend.async(f).success(function(data){
						angular.forEach($scope.current_friends,function(v,k){
							if(v._id == f) $scope.current_friends.splice(k,1);
						});
					});

			}, function() {
			    // user clicked "cancel"
			});
		};


		$scope.followers_pagination = 1;
		$scope.loadFollowers = function(){
			$scope.loading_btn = true;
			if($scope.follower_pagination == 1) $scope.user_follower = [];
			var load_obj = {
                user: $scope.user,
                skip: ($scope.follower_pagination == 1) ? 0 : parseInt($scope.follower_pagination-1)*parseInt($scope.pagination_number),
                take: parseInt($scope.pagination_number)
            };
            SocialServices.viewFollowers.async(load_obj).success(function(data){

                angular.forEach(data.followers,function(v,k){
                	$scope.user_follower.push(v);
                });
                if(data.followers.length < $scope.pagination_number){
                    $scope.stop_load_follower_pagination = true;
                } else {
                    $scope.follower_pagination++;
                }
                $scope.loading_btn = false;
            });
		};

		$scope.loadFollowing = function(){
			$scope.loading_btn = true;
			if($scope.following_pagination == 1) $scope.user_following = [];
			var load_obj = {
                user: $scope.user,
                skip: ($scope.following_pagination == 1) ? 0 : parseInt($scope.following_pagination-1)*parseInt($scope.pagination_number),
                take: parseInt($scope.pagination_number)
            };
            SocialServices.viewFollowing.async(load_obj).success(function(data){

                angular.forEach(data.following,function(v,k){
                    $scope.user_following.push(v);
                });
                if(data.following.length < $scope.pagination_number){
                    $scope.stop_load_following_pagination = true;
                } else {
                    $scope.following_pagination++;
                }
                $scope.loading_btn = false;
            });
		};

		$scope.search_user = {
			name: ''
		};
		$scope.search_results = [];
		$scope.searchUser = function(string){
			ToolsServices.mainOverlay.show();
			if(string === '') return;
			UserServices.searchUsers.async(string).success(function(data){
				$scope.search_results = data.users;
				ToolsServices.mainOverlay.hide();
			}).error(function(){
				ToolsServices.mainOverlay.hide();
			});
		};



		$scope.mail_addresses = [];
		$scope.inviteFriends = function(){
			if($scope.mail_addresses.length === 0) return;
			ToolsServices.mainOverlay.show();
			UserServices.inviteFriends.async($scope.mail_addresses).success(function(data){
				ToolsServices.mainOverlay.hide();
				swal({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary btn-sm ghost',
                    title: 'Mail inviate!',
                    text: 'I tuoi amici hanno ricevuto il tuo invito per email',
                    type: 'success'
                });
                $scope.mail_addresses = [];

			}).error(function(){
				ToolsServices.mainOverlay.hide();
			});
		};




		$scope.invite = function(provider) {


            var url = $scope.$parent.mainUrl + '/registrati/' + $scope.user +'/';
            var text = 'Entra a far parte della community di MangiaeBevi+';

            switch (provider) {
                case 'facebook_send':
                    FB.ui({
                        method: 'send',
                        link: '' + url,
                    });
                    break;
                case 'facebook_share':
                    FB.ui({
                        method: 'feed',
                        name: 'MangiaeBevi+',
                        description: 'Entra a far parte della community di MangiaeBevi+',
                        caption: 'Il portale dei ristoranti in Italia',
                        redirect_uri: '' + url,
                        link: '' + url
                    });
                    break;
                case 'twitter':
                    window.open('https://twitter.com/share?text=' + text + '&url=' + url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
                    break;
                case 'google_plus':
                    window.open('https://plus.google.com/share?url=' + url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                    break;
                case 'linkedin':
                    window.open('https://linkedin.com/shareArticle?mini=false&url=' + url + '&title=Mangiaebevi&summary=' + text + '&source=' + url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                    break;
            }


        };

	};

});