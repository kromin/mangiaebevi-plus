<?php

namespace MangiaeBevi\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use MangiaeBevi\Http\Controllers\AdministrationController as AdminCall;
use Carbon\Carbon;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \MangiaeBevi\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*$schedule->command('inspire')
                 ->hourly();*/
        // dump del db
        $schedule
        ->exec(
            'mkdir /home/forge/mongobackup/'.Carbon::now('Europe/London')->toDateString().' | mongodump -d menoo -o /home/forge/mongobackup/'.Carbon::now('Europe/London')->toDateString())
        ->daily();


        /**
        *
        * Ogni settimana ricrea la sitemap e verifica gli utenti paganti
        * 
        * */
        $schedule->call(function () {
            $call = new AdminCall();
            $call->generateSitemap();
            $call->verifyPayments();
        })->daily();

    }
}
