<?php
if (! function_exists('escape_mongodb')) {
    /**
     * Escape string for mongodb ' " \ ; { } $
     *
     * @param  mixed  $string
     * @return mixed
     */
    function escape_mongodb($string)
    {
        $chars = ["'"=>"&#39;",
                  "\""=>"&#34;",
                  "\\"=>"&#92;",                  
                  "{"=>"&#123;",
                  "}"=>"&#125;",
                  "$"=>"&#36;",
                  ";"=>"\;"
                  ];
        foreach($chars as $el => $replacement) :
            $string = str_replace($el, $replacement, $string);
        endforeach;        
        return $string;
    }
}


if (! function_exists('getImageMimeType')) {
    /**
     * Validate mime type
     *
     * @param  mixed  $imagestring
     * @return mixed string mime type of false for no valid image
     */
    function getImageMimeType($imagestring)
    {
      $mime = explode(";",$imagestring);
      $mime_type = explode("/",$mime[0]);

      if($mime_type[0] != 'data:image') return false;

      return ($mime_type[1]) ? $mime_type[1] : false;
    }

}

if (! function_exists('return_slug')) {
    /**
     * return a slug from the name
     *
     * @param  mixed  $name (string), $space_replace (optional string for character instead of spaces)
     * @return mixed string mime type of false for no valid image
     */
    function return_slug($name,$space_replace = "_")
    {
      $name = strtolower($name);
      $name = str_replace(" ", "_", $name);
      $name = strip_tags($name);
      $name = htmlentities($name);

      return $name;
    }

}

if (! function_exists('escape_non_alphanum')) {
    /**
     * return a slug from the name
     *
     * @param  mixed  $name (string), $space_replace (optional string for character instead of spaces)
     * @return mixed string mime type of false for no valid image
     */
    function escape_non_alphanum($string)
    {
        return preg_replace("/[^A-Za-z0-9 ]/", '', $string);
    }

}

if (! function_exists('return_static_image_url')) {
    /**
     * return a slug from the name
     *
     * @param  string  $type, 'restaurant,user,plate,userplate'
     * @param string $size
     * @param string $id
     * @return mixed string mime type of false for no valid image
     */
    function return_static_image_url($type,$size,$id)
    {
      echo env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/'.$type.'/'.$size.'/'.$id.'.png';
    }

}


if (! function_exists('return_simple_text_br')) {
    /**
     * return a slug from the name
     *
     * @param  string  $text, 'restaurant,user,plate,userplate'
     * @return string $text
     */
    function return_simple_text_br($text)
    {
      return str_replace(PHP_EOL, '<br />', strip_tags( $text ) );
    }

}

if (! function_exists('return_restaurant_description')) {
    /**
     * return restaurant description
     *
     * @param  string  $text
     * @return string $text
     */
    function return_restaurant_description($text)
    {
      return str_replace(PHP_EOL, '<br />', strip_tags( $text ) );
    }

}

if (! function_exists('esc_html')) {
    /**
     * return a slug from the name
     *
     * @param  string  $text
     * @return string $text
     */
    function esc_html($text)
    {
      return htmlspecialchars( strip_tags( $text ) );
    }

}

if (! function_exists('format_price')) {
    /**
     * format price
     *
     * @param  string  $text
     * @return string $text
     */
    function format_price($text)
    {
      return number_format( floatval ( $text ) , 2 );
    }

}


if (! function_exists('no_quotes_txt')) {
    /**
     * format price
     *
     * @param  string  $text
     * @return string $text
     */
    function no_quotes_txt($text)
    {
      return str_replace("\"","&#34;", str_replace("'", "&#39;",$text) );
    }

}

if (! function_exists('strip_slug')) {
    /**
     * format price
     *
     * @param  string  $text
     * @return string $text
     */
    function strip_slug($text)
    {
      return preg_replace("/[^A-Za-z0-9\-]/", '', $text);
    }

}
if (! function_exists('remove_quotes_and_tags')) {
    /**
     * format price
     *
     * @param  string  $text
     * @return string $text
     */
    function remove_quotes_and_tags($text)
    {
      return str_replace("'", '’', str_replace("\"", '”', strip_tags( $text ) ));
    }

}

if (! function_exists('esc_url')) {
    /**
     * format price
     *
     * @param  string  $text
     * @return string $text
     */
    function esc_url($text)
    {
      return $text;
    }

}

if (! function_exists('esc_attr')) {
    /**
     * format price
     *
     * @param  string  $text
     * @return string $text
     */
    function esc_attr($text)
    {
        $safe_text = wp_check_invalid_utf8( $text );
        $safe_text = _wp_specialchars( $safe_text, ENT_QUOTES );
        return $safe_text;
    }

}



function wp_check_invalid_utf8( $string, $strip = false ) {
    $string = (string) $string;
 
    if ( 0 === strlen( $string ) ) {
        return '';
    }
 
    // Store the site charset as a static to avoid multiple calls to get_option()
    static $is_utf8 = null;
    if ( ! isset( $is_utf8 ) ) {
        $is_utf8 = in_array( env( 'CHARSET', 'UTF8' ), array( 'utf8', 'utf-8', 'UTF8', 'UTF-8' ) );
    }
    if ( ! $is_utf8 ) {
        return $string;
    }
 
    // Check for support for utf8 in the installed PCRE library once and store the result in a static
    static $utf8_pcre = null;
    if ( ! isset( $utf8_pcre ) ) {
        $utf8_pcre = @preg_match( '/^./u', 'a' );
    }
    // We can't demand utf8 in the PCRE installation, so just return the string in those cases
    if ( !$utf8_pcre ) {
        return $string;
    }
 
    // preg_match fails when it encounters invalid UTF8 in $string
    if ( 1 === @preg_match( '/^./us', $string ) ) {
        return $string;
    }
 
    // Attempt to strip the bad chars if requested (not recommended)
    if ( $strip && function_exists( 'iconv' ) ) {
        return iconv( 'utf-8', 'utf-8', $string );
    }
 
    return '';
}


function _wp_specialchars( $string, $quote_style = ENT_NOQUOTES, $charset = false, $double_encode = false ) {
    $string = (string) $string;
 
    if ( 0 === strlen( $string ) )
        return '';
 
    // Don't bother if there are no specialchars - saves some processing
    if ( ! preg_match( '/[&<>"\']/', $string ) )
        return $string;
 
    // Account for the previous behaviour of the function when the $quote_style is not an accepted value
    if ( empty( $quote_style ) )
        $quote_style = ENT_NOQUOTES;
    elseif ( ! in_array( $quote_style, array( 0, 2, 3, 'single', 'double' ), true ) )
        $quote_style = ENT_QUOTES;
 
    // Store the site charset as a static to avoid multiple calls to wp_load_alloptions()
    if ( ! $charset ) {
        static $_charset = null;
        if ( ! isset( $_charset ) ) {
            $_charset = env( 'CHARSET', 'UTF8' );
        }
        $charset = $_charset;
    }
 
    if ( in_array( $charset, array( 'utf8', 'utf-8', 'UTF8' ) ) )
        $charset = 'UTF-8';
 
    $_quote_style = $quote_style;
 
    if ( $quote_style === 'double' ) {
        $quote_style = ENT_COMPAT;
        $_quote_style = ENT_COMPAT;
    } elseif ( $quote_style === 'single' ) {
        $quote_style = ENT_NOQUOTES;
    }
 
    if ( ! $double_encode ) {
        // Guarantee every &entity; is valid, convert &garbage; into &amp;garbage;
        // This is required for PHP < 5.4.0 because ENT_HTML401 flag is unavailable.
        //$string = wp_kses_normalize_entities( $string );
    }
 
    $string = @htmlspecialchars( $string, $quote_style, $charset, $double_encode );
 
    // Back-compat.
    if ( 'single' === $_quote_style )
        $string = str_replace( "'", '&#039;', $string );
 
    return $string;
}

function wp_kses_normalize_entities($string) {
    // Disarm all entities by converting & to &amp;
    $string = str_replace('&', '&amp;', $string);
 
    // Change back the allowed entities in our entity whitelist
    $string = preg_replace_callback('/&amp;([A-Za-z]{2,8}[0-9]{0,2});/', 'wp_kses_named_entities', $string);
    $string = preg_replace_callback('/&amp;#(0*[0-9]{1,7});/', 'wp_kses_normalize_entities2', $string);
    $string = preg_replace_callback('/&amp;#[Xx](0*[0-9A-Fa-f]{1,6});/', 'wp_kses_normalize_entities3', $string);
 
    return $string;
}



?>