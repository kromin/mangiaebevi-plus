<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Reservation extends Eloquent
{
    //
    protected $collection = 'reservation_collection';
    protected $dates = ['day'];
}
