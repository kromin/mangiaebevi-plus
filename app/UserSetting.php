<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;
class UserSetting extends Eloquent
{
    //
    protected $collection = 'user_setting_collection';
}
