<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Notifications extends Eloquent
{
    //
    protected $collection = 'notifications_collection';

    protected $fillable = ['type','text','target_usr','ref','read','for_admin'];
}
