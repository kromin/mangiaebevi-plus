<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Locations extends Eloquent
{
    //
    protected $collection = 'locations_collection';

    protected $fillable = ['name'];
}
