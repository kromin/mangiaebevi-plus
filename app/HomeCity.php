<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class HomeCity extends Eloquent
{
    //
    protected $collection = 'homecity_collection';

    protected $fillable = ['name','lat','lng','image','order'];
}
