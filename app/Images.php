<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Images extends Eloquent
{
    //
    protected $collection = 'images_collection';
    protected $fillable = ['type','alt','title','ref_id']; // type: blog, restaurant | ref_id: referral id (blog post or restaurant)
}
