<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;
class Types extends Eloquent
{
    //
    protected $collection = 'types_collection';

    protected $fillable = ['name','image'];
}
