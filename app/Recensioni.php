<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Recensioni extends Eloquent
{
    //
    protected $collection = 'recensioni_collection';
    protected $fillable = ['name','text','tags','user','restaurant','image','approved'];
}
