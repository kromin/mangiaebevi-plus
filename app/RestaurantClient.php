<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class RestaurantClient extends Eloquent
{
    public function user(){
    	return $this->belongsTo('MangiaeBevi\User', 'user_id');
    }

    public function restaurant(){
    	return $this->belongsTo('MangiaeBevi\Restaurant', 'restaurant_id');
    }
}
