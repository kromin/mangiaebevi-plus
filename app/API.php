<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class API extends Eloquent
{
    //
    protected $collection = 'api_collection';
}
?>