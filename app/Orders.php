<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Orders extends Eloquent
{
    //
    protected $collection = 'orders_collection';
    protected $dates = ['consegnato','day'];

}
