<?php

namespace MangiaeBevi\Events;

use MangiaeBevi\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use MangiaeBevi\Newsletter;

class NewsletterRegistration extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Newsletter $newsletter)
    {
        //
        $this->subscriber = $newsletter;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
