<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class UserActions extends Eloquent
{
    //
    protected $collection = 'user_actions_collection';
}
