<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;
use User;

class TempFavourites extends Eloquent
{
    //
    protected $collection = 'temp_favourites_collection';
}
