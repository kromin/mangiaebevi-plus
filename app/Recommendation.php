<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Recommendation extends Eloquent
{
    //
    protected $collection = 'recommendation_collection';
}
