<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class CreditCard extends Eloquent
{
    //
    protected $collection = 'creditcards_collection';

    protected $fillable = ['name','image'];
}
