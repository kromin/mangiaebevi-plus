<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Pacchetti extends Eloquent
{
    //
    protected $collection = 'pacchetti_collection';

    protected $fillable = ['name','featured','services','price'];
}
