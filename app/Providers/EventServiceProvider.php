<?php

namespace MangiaeBevi\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'MangiaeBevi\Events\UserRegistered' => [
            'MangiaeBevi\Listeners\UserRegistrationListener',
        ],
        'MangiaeBevi\Events\UserChangedEmail' => [
            'MangiaeBevi\Listeners\UserChangedEmailListener',
        ],
        'MangiaeBevi\Events\NewsletterRegistration' => [
            'MangiaeBevi\Listeners\NewUserInNewsletter',
        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
