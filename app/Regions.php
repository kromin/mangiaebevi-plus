<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;
class Regions extends Eloquent
{
    //
    protected $collection = 'regions_collection';
    protected $fillable = ['name','image'];
}
