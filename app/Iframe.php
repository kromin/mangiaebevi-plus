<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Iframe extends Eloquent
{
    //
    protected $collection = 'iframe_collection';
}
