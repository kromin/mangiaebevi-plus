<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Plates extends Eloquent
{
    //
    protected $collection = 'plates_collection';
    protected $fillable = ['name','image','description','ingredienti','price','restaurant','fascia'];
}
