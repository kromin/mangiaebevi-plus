<?php
/**
*
* @deprecated
* 
* **/
namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class HomeMainCity extends Eloquent
{
    //
    protected $collection = 'home_main_cities_collection';
    protected $fillable = ['name','lat','lng','image'];
}
