<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;
class ReservationRequest extends Eloquent
{
    //
    protected $collection = 'reservation_request_collection';
    protected $fillable = ['preorder'];
    protected $dates = ['date'];
}
