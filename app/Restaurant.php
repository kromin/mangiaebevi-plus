<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Restaurant extends Eloquent
{
	
    //
    protected $collection = 'restaurant_collection';

    protected $fillable = array(
    	'ID_EXPORT',
		'name',
		'description',
		'address',
		'loc',
		'contact',
		'virtualTour',
		'streetView',
		'location',
		'price',
		'priceMin',
		'priceMax',
		'services',
		'tipoLocale',
		'tipoCucina',
		'regioneCucina',
		'social',
		'staff',
		'orari',
		'chiusura',
		'note_orari',
		'image',
		'video',
		'images',
		'menu',
		'vini',
		'carte',
		'prenotazione',
		'ordina',
		'valid_address');
    
    protected $dates = ['deleted_at'];

    /**
     * fai un escape della descrizione
     * @param  [type] $value [description]
     * @return [type]        [description]
     
    public function getDescriptionAttribute($value)
    {
        
        
        return return_restaurant_description( $value );
    }*/
    	
    public function clients()
    {
    	return $this->hasMany('MangiaeBevi\RestaurantClient', 'restaurant_id', '_id');
    }		
}
