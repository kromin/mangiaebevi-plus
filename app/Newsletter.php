<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Newsletter extends Eloquent
{
    //
    protected $collection = 'newsletter_collection';
    protected $fillable = ['email','name','delete_string'];
}
