<?php

namespace MangiaeBevi;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Jenssegers\Mongodb\Model as Eloquent;
use Log;
class User extends Eloquent implements AuthenticatableContract, 
                                        AuthorizableContract, 
                                        CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'users_collection';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                        'name',
                        'surname', 
                        'email',
                        'password',
                        'role',
                        'temp_cookie',
                        'favourites',
                        'followedby',
                        'following',
                        'friends_num',
                        'followers_num',
                        'following_num',
                        'wishlist',
                        'friends',
                        'friend_request',
                        'current_address',
                        'address_list',
                        'favourite_list',
                        'social_provider',
                        'social_provider_id',
                        'plan',
                        'recommended_restaurants',
                        'recommended_plates',
                        'allergeni'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
        'password'];//'password',

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     
    protected $casts = [
        'wishlist'=>'array',
        ];*/


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $dates = ['born'];//'password', 

    public function getAuthPassword() {
        return $this->password;
    } 

}
