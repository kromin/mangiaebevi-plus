<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class RecommendationRequest extends Eloquent
{
    //
    protected $collection = 'recommendation_request_collection';
}
