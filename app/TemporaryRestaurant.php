<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class TemporaryRestaurant extends Eloquent
{
    //
    protected $collection = 'temporary_restaurant_collection';

    protected $fillable = array(
    	'ID_EXPORT',
		'name',
		'description',
		'address',
		'loc',
		'contact',
		'virtualTour',
		'streetView',
		'location',
		'price',
		'priceMin',
		'priceMax',
		'services',
		'tipoLocale',
		'tipoCucina',
		'regioneCucina',
		'social',
		'staff',
		'orari',
		'chiusura',
		'note_orari',
		'image',
		'video',
		'images',
		'menu',
		'vini',
		'carte',
		'prenotazione',
		'ordina',
		'valid_address');
    
    protected $dates = ['deleted_at'];
    			
}