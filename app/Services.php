<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Services extends Eloquent
{
    //
    protected $collection = 'services_collection';

    protected $fillable = ['name','image'];
}
