<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class SearchRequestsNewsletter extends Eloquent
{
    //
    protected $collection = 'search_request_newsletter_collection';
    protected $fillable = ['query','name','email'];
}
