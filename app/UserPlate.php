<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class UserPlate extends Eloquent
{
    //
    protected $collection = 'user_plates_collection';
}
