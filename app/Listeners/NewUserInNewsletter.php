<?php

namespace MangiaeBevi\Listeners;

use MangiaeBevi\Events\NewsletterRegistration;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Contracts\Mail\Mailer;
use Mail;
use Log;

class NewUserInNewsletter
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        //
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  NewsletterRegistration  $event
     * @return void
     */
    public function handle(NewsletterRegistration $event)
    {
        //
        $subscriber = $event->subscriber;
        Mail::send('email.newsletterregistration', ['subscriber' => $subscriber],
            function ($m) use ($subscriber) {
            $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
            $m->to($subscriber->email, $subscriber->name)->subject('Registrazione newsletter');
        });
    }
}
