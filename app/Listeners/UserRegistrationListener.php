<?php

namespace MangiaeBevi\Listeners;

use MangiaeBevi\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Contracts\Mail\Mailer;
use Mail;
use Log;

class UserRegistrationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        //
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        // $event->user->email
        $user = $event->user;
        Mail::send('email.registration', ['user' => $user],
            function ($m) use ($user) {
            $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
            $m->to($user->email, $user->name)->subject('Registrazione effettuata');
        });
    }
}
