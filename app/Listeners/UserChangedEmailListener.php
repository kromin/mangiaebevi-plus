<?php

namespace MangiaeBevi\Listeners;

use MangiaeBevi\Events\UserChangedEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Contracts\Mail\Mailer;
use Mail;
use Log;

class UserChangedEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        //
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  UserChangedEmail  $event
     * @return void
     */
    public function handle(UserChangedEmail $event)
    {
        //
        $user = $event->user;
        Mail::send('email.changedemail', ['user' => $user],
            function ($m) use ($user) {
            $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
            $m->to($user->email, $user->name)->subject('Cambio indirizzo email');
        });
    }
}
