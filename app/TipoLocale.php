<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class TipoLocale extends Eloquent
{
    //
    protected $collection = 'tipo_locale_collection';

    protected $fillable = ['name'];
}
