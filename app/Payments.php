<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class Payments extends Eloquent
{
    protected $collection = 'payments_collection';
    protected $fillable = ['reservation','user','details'];

    protected $casts = [
    	"details"=>"array"
    ];
}
