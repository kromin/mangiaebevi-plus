<?php

namespace MangiaeBevi\Http\Middleware;

use Closure;
use Auth;
class isMeOrIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request need the $request->input('user_id')
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check() || !$request->input('user_id') ||
        	( Auth::user()->role < 4 && Auth::user()->_id != $request->input('user_id') )
        	) :
            return response()->json(['message'=>'error.nonpuoi ', 'request'=>file_get_contents('php://input')], 200);
        endif;         
        return $next($request);
    }
}
