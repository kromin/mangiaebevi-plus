<?php

namespace MangiaeBevi\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Contracts\Auth\Guard;

class LoggedMiddleware
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest() || (Auth::check() && Auth::user()->role >= 4) ) :
            return $next($request);
        else :
            return response()->json(['message'=>'error.nonpuoi'], 200);
        endif;
        
    }
}
