<?php

namespace MangiaeBevi\Http\Middleware;

use Closure;
use Auth;

class OnlyLoggedMiddleware
{
  
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()) :
            return response()->json(['message'=>'error.nonpuoi'], 200);
        endif;
        return $next($request);        
    }
}
