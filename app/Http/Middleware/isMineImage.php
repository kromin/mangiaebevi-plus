<?php

namespace MangiaeBevi\Http\Middleware;

use Closure;
use Auth;

use Mangiaebevi\Images;
class isMeOrIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request need the $request->input('user_id')
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()) :
            return response()->json(['message'=>'error.nonpuoi '.$request->input('user_id')], 200);
        endif;         

        $i = Images::find($request->input('id'));
        if(!$i || !$i->uploaded_by || $i->uploaded_by != Auth::user()->_id):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        return $next($request);
    }
}
