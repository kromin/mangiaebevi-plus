<?php

namespace MangiaeBevi\Http\Middleware;

use Closure;
use Log;
class CrawlerCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        Log::info("Agent: ".$_SERVER['HTTP_USER_AGENT']);
        return $next($request);
    }
}
