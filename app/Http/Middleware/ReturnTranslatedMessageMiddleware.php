<?php 
namespace MangiaeBevi\Http\Middleware;

use Closure;
use Log;

class ReturnTranslatedMessageMiddleware {

    public function handle($request, Closure $next)
    {
        $response = $next($request);
        // set response translated
        $response_data = json_decode($response->getContent(),true);
        if(isset($response_data['message']) && $response_data['message'] != 'ok'):
        	$response_data['message'] = 'error';
        	$response->setContent(json_encode($response_data));
        endif;      
    	
        // Perform action

        return $response;
    }
}