<?php

namespace MangiaeBevi\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        // API pubbliche per app
    	'v1/api/public/*',
    	// pagamenti paypal
    	//'payment/*',
        //'socialog/*',
        //'socialogged/*'
    ];
}
