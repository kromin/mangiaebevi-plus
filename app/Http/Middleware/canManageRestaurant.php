<?php

namespace MangiaeBevi\Http\Middleware;

use Closure;
use Auth;
use MangiaeBevi\Restaurants;
class canManageRestaurant
{
    /**
     * Verify if is trying to manage a non personal restaurant, if is admin return next closure, else verification will be done
     *
     * @param  \Illuminate\Http\Request  $request need the $request->input('restaurant_id')
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {    	
        if( Auth::check() && $request->input('restaurant_id') ) :
        	if(Auth::user()->_id < 3): 
        		$restaurant = Restaurants::where('user',Auth::user()->_id)->where('_id',$request->input('restaurant_id'))-get();           
		    	if(count($restaurant) < 1) :
		    		return response()->json(['message'=>'error.nonpuoi'], 200);
	    		endif;
	    	endif;        	
        	return $next($request);        	
        endif;         
        return response()->json(['message'=>'error.nonpuoi'], 200);
    }
}