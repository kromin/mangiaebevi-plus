<?php

namespace MangiaeBevi\Http\Middleware;

use Closure;
use Auth;
class IsGod
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check() || Auth::user()->role < 9){
            return response()->json(['message'=>'error.nonpuoi'], 200);
        }
        return $next($request);
    }
}
