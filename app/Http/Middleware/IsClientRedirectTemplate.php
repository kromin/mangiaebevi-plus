<?php

namespace MangiaeBevi\Http\Middleware;

use Closure;
use Auth;
class IsClientRedirectTemplate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check() || Auth::user()->role < 2){
            return response()->json(['message'=>'error.nonpuoi','redirect'=>'/'], 200);
        }
        return $next($request);
    }
}
