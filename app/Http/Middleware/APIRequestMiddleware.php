<?php

namespace MangiaeBevi\Http\Middleware;

use Closure;
use MangiaeBevi\API;
use Log;

use Validator;

class APIRequestMiddleware
{
    /**
     * Il middleware verifica che sia una richiesta valida 
     * 
     * <p>Per la verifica vengono validati 2 parametri nell'<b>header</b> della richiesta:</p>
     * 
     * <p><b>Valori per app:</b> 
     *  <ul>
     *      <li><b>appheaderkey:</b> ttei5Zh1rUU4yzDtofz72W1xjyUWFYlEtMq6AE2S</li> 
     *      <li><b>appheadersecret:</b> k7CWzoBftNrz7bVKMygX1lC8VdxynPsTvDUpaVSV</li>
     *  </ul>
     * </p>
     * 
     * 
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>appheaderkey => string</li>
     *          <li>appheadersecret => string</li>
     *      </ul>
     * </p> 
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li>'message'=>'invalid_key_secret' statusCode 403 se i parametri non sono validi</li>
     *          <li>'message'=>'invalid_key_secret_validation' statusCode 403 se i parametri non sono stringhe o non sono presenti</li>              
     *      </ul>
     * </p>
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed 
     */
    public function handle($request, Closure $next)
    {

        $params = [
            'appheaderkey' => ($request->header('appheaderkey')) ? $request->header('appheaderkey') : null,
            'appheadersecret' => ($request->header('appheadersecret')) ? $request->header('appheadersecret'): null,
        ];

        $validator = Validator::make($params, [        
            'appheaderkey' => 'required|string',
            'appheadersecret' => 'required|string',
        ]);
        if ($validator->fails()) return response()->json(['message'=>'invalid_key_secret_validation',],403);
        
        $api = API::where('key',$request->header('appheaderkey'))->where('secret',$request->header('appheadersecret'))->get();        
        if(!$api || count($api) == 0) return response()->json(['message'=>'invalid_key_secret'],403);//,'key'=>$request->header()
        
        /**
        *
        * Aggiungi una richiesta all'API code
        * 
        **/
        API::where('key',$request->header('app-header-key'))->where('secret',$request->header('app-header-secret'))->increment('requests');

        
        return $next($request);
    }
}
