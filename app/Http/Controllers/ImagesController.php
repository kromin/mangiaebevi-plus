<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Jenssegers\Mongodb\Model as Eloquent;

use MangiaeBevi\Http\Requests;

use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\Images;
use Image; // intervention Image
use File;
use Validator;
use Log;

use Auth;
use Input;

class ImagesController extends Controller
{
    protected $avaible_mime  = ['image/png','image/jpg','image/jpeg'];
    protected $avaible_types = ['blog','restaurant','menu','homecity','recensioni','users','custom','community'];

    const avaible_types_of_images = ['blog','restaurant','menu','homecity','recensioni','users','custom','community'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $images = Images::all();        
        return response()->json(['message'=>'ok','images'=>$images], 200);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_by_type(Request $request)
    {
        //
        $this->validate($request, [
            'type' => 'required|string',
            'ref' => 'required|alpha_num'
        ]);
        $images = Images::where('ref_id',$request->input('ref'))->where('type',$request->input('type'))->get();        
        return response()->json(['message'=>'ok','images'=>$images], 200);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'type' => 'required|string',
            'alt' => 'required|string',
            'title' => 'required|string',
            'image'=>'required|mimes:jpeg,gif,png,svg',
            'ref_id' => 'alpha_num'
        ]);

        if(!in_array($request->input('type'), ImagesController::avaible_types_of_images)) :
            return false;
        endif;

        $images = new Images();

        $image = \Input::file('image');

        $image->getRealPath();        
        
        $img = Image::make($image);
        if(!$img) :
            return response()->json(['message'=>'error.notvalidimage'], 200);
        endif;

        
        $images->type = $request->input('type');
        $images->alt = $request->input('alt');
        $images->title = $request->input('title');
        if($request->input('ref_id')):
            $images->ref_id = $request->input('ref_id');
        endif;

        if(Auth::check()):
            $images->uploaded_by = Auth::user()->_id;
        endif;
        $images->save();

        $type = $images->type;
        $img_name = $images->_id;

        try{
            self::saveImages($img,$type,$img_name);
        } catch (ErrorException $e){
            $images->delete();
        }
        
        
        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'image'=>$images 
        ];
        return response()->json($response, $statusCode);

    }

    

    /**
     * Add an image to the application from another class
     *
     * @param  \Illuminate\Http\Request  $request
     * @return image_id
     */
    public static function addImages($type,$alt,$title,$image,$ref_id = null)
    {
        //
        $data = [
        'type'=>$type,
        'alt'=>$alt,
        'title'=>$title,
        'image'=>$image,
        'ref_id'=>$ref_id
        ];
        $validator = Validator::make($data, [
            'type' => 'required|string',
            'alt' => 'required|string',
            'title' => 'required|string',
            'image'=>'required|mimes:jpeg,gif,png,svg',
            'ref_id' => 'alpha_num'
        ]);
        
        if ($validator->fails()) :
            return false;
        endif;

        if(!in_array($type, ImagesController::avaible_types_of_images)) :
            return false;
        endif;

        $images = new Images();

        $image->getRealPath();        
        
        $img = Image::make($image);
        if(!$img) :
            return response()->json(['message'=>'error.notvalidimage'], 200);
        endif;

        /*$img_mime = $img->mime();
        if(!in_array($img_mime, $this->avaible_mime)) :
            return false;
        endif;*/

        $images->type = $type;
        $images->alt = $alt;
        $images->title = $title;
        if($ref_id):
            $images->ref_id = $ref_id;
        endif;

        if(Auth::check()):
            $images->uploaded_by = Auth::user()->_id;
        endif;
        $images->save();

        $type = $images->type;
        $img_name = $images->_id;

        try{
            self::saveImages($img,$type,$img_name);
        } catch (ErrorException $e){
            $images->delete();
        }
        
        
        return $images->_id;
    }

    /**
    *
    * Resize and save images in directory
    * 
    * @param \Image $img
    * @return true|false on error
    * 
    * */
    private static function saveImages($img,$type,$img_name){
        $img->encode('png', 100);
        $path = ['big','mobile','medium','small','square'];
        switch($type):
            case 'restaurant';
            $dimensions = [[1200,null],[0,null],[800,null],[400,null],[400,400]];
            break;
            case 'custom'; // sono i servizi, le facciamo piccole
            $dimensions = [[10,null],[0,null],[0,null],[80,null],[80,null]];
            break;
            case 'users';
            $dimensions = [[1200,null],[0,null],[400,400],[250,250],[80,80]];
            break;
            default:
            $dimensions = [[800,null],[0,null],[500,null],[250,250],[150,150]];
            break;
        endswitch;

        $x = 0;
        foreach($dimensions as $dimension):
            if($dimension[0] != 0):                
                // salva solo se ci sono le dimensioni
                if($x > 0):
                    if($dimension[0] != $dimension[1]):
                        $img->resize($dimension[0], $dimension[1], function ($constraint) {
                            $constraint->aspectRatio();            
                        });
                    else:
                        // le quadrate hanno funzione diversa
                        $img->fit($dimension[0], $dimension[1]); 
                    endif;
                endif;
                $img->save('../../'.env('APP_STATIC_PATH').'/'.$type.'/'.$path[$x].'/'.$img_name.'.png', 100);


                //Mangiaebevi/mangiaebevi/app/Http/Controllers
            endif;   
            $x++;         
        endforeach;

        return true;
    }

    /**
     * Remove image from DB and Filesystem, check if exists the small version
     *
     * @param  $id alpha_num
     * @param  $type string
     * @return \Illuminate\Http\Response
     */
    public static function deleteImage($id,$type)
    {
        
        $data = [
        'type'=>$type,
        'id'=>$id
        ];
        $validator = Validator::make($data, [
            'type' => 'required|string',
            'id' => 'required|alpha_num'
        ]);
        
        if ($validator->fails()) :
            return response()->json(['message'=>'error.validation'], 200);
        endif;

        if(!in_array($type, ImagesController::avaible_types_of_images)) :
            return response()->json(['message'=>'error.invalid'], 200);
        endif;

        $image = Images::find($id);
        if(!$image):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        if(Auth::check() && Auth::user()->role > 4 && File::exists('../../'.env('APP_STATIC_PATH').'/'.$image->type.'/small/'.$image->_id.'.png')) :
            $to_delete = [  '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/big/'.$image->_id.'.png',
                            '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/mobile/'.$image->_id.'.png',
                            '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/medium/'.$image->_id.'.png',
                            '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/small/'.$image->_id.'.png',
                            '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/square/'.$image->_id.'.png'];
            File::delete($to_delete);
        endif;
        $image->delete();
        return true;
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);
        $image = Images::find($request->input('id'));
        if(!$image):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        return response()->json(['message'=>'ok','image'=>$image], 200);
    }

   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        //$img = Images::where('type','restaurant')->delete();
        //return response()->json(['message'=>'ok'], 200);
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);
        $image = Images::find($request->input('id'));
        if(!$image):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        if(File::exists('../../'.env('APP_STATIC_PATH').'/'.$image->type.'/big/'.$image->_id.'.png')) :
            $to_delete = [  '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/big/'.$image->_id.'.png',
                            '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/mobile/'.$image->_id.'.png',
                            '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/medium/'.$image->_id.'.png',
                            '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/small/'.$image->_id.'.png',
                            '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/square/'.$image->_id.'.png'];
            File::delete($to_delete);
        endif;
        $image->delete();
        return response()->json(['message'=>'ok'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return string
     */
    public function public_destroy($id)
    {
        //
        $data = [
        'id'=>$id
        ];

        $validator = Validator::make($data, [
            'id' => 'required|alpha_num'
        ]);

        if ($validator->fails()) :
            return 'error.invalid';
        endif;


        $image = Images::find($id);
        if(!$image):
            return 'error.empty';
        endif;
        if(File::exists('../../'.env('APP_STATIC_PATH').'/'.$image->type.'/big/'.$image->_id.'.png')) :
            $to_delete = [  '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/big/'.$image->_id.'.png',
                            '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/mobile/'.$image->_id.'.png',
                            '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/medium/'.$image->_id.'.png',
                            '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/small/'.$image->_id.'.png',
                            '../../'.env('APP_STATIC_PATH').'/'.$image->type.'/square/'.$image->_id.'.png'];
            File::delete($to_delete);
        endif;
        $image->delete();
        return 'ok';
    }


    /**
    *
    * Return image for apps
    * 
    * */
    public function getImageFromUrl($id,$format,$h,$w,$quality){
        $default_image = public_path().'/img/search_default.jpg';//../../'.env('APP_STATIC_PATH').'/'.$image->type.'/big/'.$image->_id.'.png';
        
        $data = [
        'id'=>$id,
        'format'=>$format,
        'h'=>$h,
        'w'=>$w,
        'quality'=>$quality
        ];

                
        $validator = Validator::make($data, [
            'id' => 'required|alpha_num',
            'format'=> 'required|string',
            'h'=>'required|integer',
            'w'=>'required|integer',
            'quality'=>'required|integer|min:0|max:100'
        ]);

        if ($validator->fails() || ($format != 'jpg' && $format != 'png')) :
            Log::info('errore');
            $img = Image::make($default_image)->encode('jpg', 100);
            return Response::make($img, 200, array('Content-Type' => 'image/jpeg'));                   
        
        else:
            
            $i = '../../'.env('APP_STATIC_PATH').'/restaurant/big/'.$id.'.png';
            
            if(!file_exists($i)) $i = '../../'.env('APP_STATIC_PATH').'/menu/big/'.$id.'.png';

            if(!file_exists($i)) $i = '../../'.env('APP_STATIC_PATH').'/users/big/'.$id.'.png';

            if(!file_exists($i)) $i = '../../'.env('APP_STATIC_PATH').'/homecity/big/'.$id.'.png';

            if(!file_exists($i)) $i = '../../'.env('APP_STATIC_PATH').'/community/big/'.$id.'.png';

            if(!file_exists($i)) :
                $img = Image::make($default_image)->encode('jpg', 100);
                return Response::make($img, 200, array('Content-Type' => 'image/jpeg'));
            endif;
            
            $data = [$id,$format,$h,$w,$quality,$i];
            
                             
            $img = Image::cache(function($image) use ($data) {            
                return $image->make($data[5])->fit($data[3], $data[2])->encode($data[1], $data[4]); 
            });

            $content_type = ($format == 'jpg') ? 'image/jpeg' : 'image/png';
            return Response::make($img, 200, array('Content-Type' => $content_type));             

        endif;   
    }

    /**
    *
    * Return blog image for apps
    * 
    * */
    public function getBlogImageFromUrl($year,$month,$name,$format,$h,$w,$quality){
        $default_image = public_path().'/img/search_default.jpg';//../../'.env('APP_STATIC_PATH').'/'.$image->type.'/big/'.$image->_id.'.png';
        
        $data = [
        'name'=>$name,
        'year'=>$year,
        'month'=>$month,
        'format'=>$format,
        'h'=>$h,
        'w'=>$w,
        'quality'=>$quality
        ];

                
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'year'=>'required|numeric',
            'month'=>'required|numeric',
            'format'=> 'required|string',
            'h'=>'required|integer',
            'w'=>'required|integer',
            'quality'=>'required|integer|min:0|max:100'
        ]);

        if ($validator->fails() || ($format != 'jpg' && $format != 'png')) :
            
            $img = Image::make($default_image)->encode('jpg', 100);
            return Response::make($img, 200, array('Content-Type' => 'image/jpeg'));                   
        
        else:
            
            $dangerous_characters = array(" ", '"', "'", "&", "/", "\\", "?", "#");
            $filename = str_replace($dangerous_characters, '_', $name);
            $i = '../../'.env('APP_BLOG_PATH').'/blog/wp-content/uploads/'.urlencode($year).'/'.urlencode($month).'/'.$filename;
            
            if(!file_exists($i)) :
                $img = Image::make($default_image)->encode('jpg', 100);
                return Response::make($img, 200, array('Content-Type' => 'image/jpeg'));
            endif;
            
            $data = [$format,$h,$w,$quality,$i];
            
                             
            $img = Image::cache(function($image) use ($data) {            
                return $image->make($data[4])->fit($data[2], $data[1])->encode($data[0], $data[3]); 
            });

            $content_type = ($format == 'jpg') ? 'image/jpeg' : 'image/png';
            return Response::make($img, 200, array('Content-Type' => $content_type));             

        endif;   
    }
}
