<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Mongodb\Model as Eloquent;


use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\SearchRequests;
use MangiaeBevi\SearchRequestsNewsletter;

class SearchRequestsController extends Controller
{
    
    /**
    * Add a search request to "search_request_collection"
    * 
    * @param string $query
    * @param double $latitude
    * @param double $longitude
    * @param integer $type (0=>city,1=>string,2=>cap,3=>name,4=>user position)
    * @return bool
    * */
    public static function saveRequest($query,$latitude = 0,$longitude = 0,$type = 1, $action = 'order')
    {
        $search_request = new SearchRequests();
        $search_request->query = $query;
        $loc = [
            'type'=> 'Point',
            'coordinates'=>[$longitude,$latitude]
        ];
        $search_request->loc = $loc;
        $search_request->type = $type;
        $search_request->action = $action;
        $search_request->website = 'mangiaebevi';
        
        //if($complex) $search_request->complex = $complex;
        
        $search_request->save();
        return true;
    }

    /**
    * Add a request to "search_request_newsletter_collection" to be informed of new query update
    * 
    * @param Illuminate\Http\Request $request
    * @return Illuminate\Http\Response $response
    * */
    public function saveRequestNewsletter(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string',
            'email'=>'required|email',
            'query'=>'required|string'
            ]);

        $search_request_newsletter = new SearchRequestsNewsletter();
        $search_request_newsletter->query = $request->input('query');
        $search_request_newsletter->name = $request->input('name');
        $search_request_newsletter->email = $request->input('email');
        
        $search_request_newsletter->save();
        return response()->json(['message'=>'ok'],200);
    }
}
