<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\Restaurant;
use MangiaeBevi\Plates;
use MangiaeBevi\User;
use MangiaeBevi\Recommendation;

use Carbon\Carbon;
use Auth;
use MangiaeBevi\Http\Controllers\GoogleAddressController as Address;
use DB;
use Log;
use MangiaeBevi\Http\Controllers\UserActionsController as UserActionStatic;
use MangiaeBevi\Http\Controllers\NotificationsController as Notification;

class RecommendationController extends Controller
{
    
    /**
     * Recommend at buffo
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function publishRecommendation(Request $request)
    {
        //
        $this->validate($request, [
            'address' => 'string',
            'text' => 'required|string',
            'type' => 'required|integer|min:0|max:1', // 0 == ristorante, 1 == piatto
            'ref' => 'required|alpha_num' // id elemento
        ]);

        $u = User::find(Auth::user()->_id);
        if(!$u) return response()->json(['message'=>'error.notfound'],404);

        switch($request->input('type')):
            case 0:
            if(in_array($request->input('ref'), $u->recommended_restaurants)) return response()->json(['message'=>'error.notvalid'],403);
            break;
            case 1:
            if(in_array($request->input('ref'), $u->recommended_plates)) return response()->json(['message'=>'error.notvalid'],403);
            break;
        endswitch;

        if( ( !$u->loc || !$u->loc['coordinates'] ) && !$request->input('address') ) return response()->json(['message'=>'error.noprofilecompleted'],403);

        if($request->input('address')):
            
            $location = Address::returnLocation($request->input('address'));
            if(!$location) :
                return response()->json(['message'=>'error.notvalid'],403);
            else:
                $lat = doubleval($location[0]);
                $lng = doubleval($location[1]);
            endif;

        else:
            $lat = doubleval($u->loc['coordinates'][1]);
            $lng = doubleval($u->loc['coordinates'][0]);
        endif;

        $recommendation = new Recommendation();
        $recommendation->user = Auth::user()->_id;
        $recommendation->loc = ['type'=>"Point","coordinates"=>[$lng,$lat]];
        $recommendation->text = $request->input('text');
        $recommendation->type = $request->input('type');
        $recommendation->ref = $request->input('ref');
        $recommendation->save();

        $u->foodcoin = intval($u->foodcoin) + intval(env('FOODCOIN_RECOMMENDATION'));        
        $u->save();
        switch($recommendation->type):
            case 0:            
                Restaurant::where('_id', $recommendation->ref)->increment('recommendations');
                DB::collection('users_collection')->where('_id', Auth::user()->_id)->push('recommended_restaurants', $recommendation->ref);
                UserActionStatic::addUserAction(1,[$recommendation->ref]);

                $risto = Restaurant::find($recommendation->ref);
                $target = ($risto->user && $risto->user != '') ? $risto->user : 0;
                $parameters = [Auth::user()->_id,Auth::user()->name.' '.Auth::user()->surname,$recommendation->ref];
                Notification::addNotification(17,$parameters,$target);
            break;
            case 1:            
                Plates::where('_id', $recommendation->ref)->increment('recommendations');
                DB::collection('users_collection')->where('_id', Auth::user()->_id)->push('recommended_plates', $recommendation->ref);
                UserActionStatic::addUserAction(2,[$recommendation->ref]);
            break;
        endswitch;

        return response()->json(['message'=>'ok','recommendation'=>$recommendation],200);

    }

    /**
     * Recommend at buffo
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function viewByUser(Request $request)
    {
        //
        $this->validate($request, [
            'user'=> 'required|alpha_num',
            'skip' => 'integer',
            'take' => 'required|integer|max:10'
        ]);

        if(!$request->input('skip') || $request->input('skip') == 0):
            $r = Recommendation::where('user',$request->input('user'))->take($request->input('take'))->orderBy('created_at','desc')->get();
        else:
            $r = Recommendation::where('user',$request->input('user'))->skip($request->input('skip'))->take($request->input('take'))->orderBy('created_at','desc')->get();
        endif;
        //->skip($request->input('skip'))
        //->take($request->input('take'))
        
        
        //if($request->input('take')) $r->take($request->input('take'));
        //if($request->input('skip') && $request->input('skip') > 0) $r->skip($request->input('skip'));
        
        //$r->get();

        $result = [];
        //Log::info(json_encode($r));
        foreach ($r as $recommendation) {

            $result[] = $this->formatRecommendationWithUser($recommendation);
        }

        return response()->json(['message'=>'ok','recommendation'=>$result],200);

    }

    /**
     * View Plate reccomendations
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function viewByPlate(Request $request)
    {
        //
        $this->validate($request, [
            'ref'=> 'required|alpha_num',
            'skip' => 'integer',
            'take' => 'required|integer|max:10'
        ]);

        
        if(!$request->input('skip') || $request->input('skip') == 0):
            $r = Recommendation::where('type',1)->where('ref',$request->input('ref'))->take($request->input('take'))->orderBy('created_at','desc')->get();
        else:
            $r = Recommendation::where('type',1)->where('ref',$request->input('ref'))->skip($request->input('skip'))->take($request->input('take'))->orderBy('created_at','desc')->get();
        endif;

        

        $result = [];
        foreach ($r as $recommendation) {
            $u = User::where('_id',$recommendation['user'])->get(['name','avatar']);
            $recommendation['user_obj'] = (isset($u[0])) ? $u[0] : null;
            $result[] = $recommendation;
        }

        return response()->json(['message'=>'ok','recommendation'=>$result],200);

    }

    /**
     * View Restaurant reccomendations
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function viewByRestaurant(Request $request)
    {
        //
        $this->validate($request, [
            'ref'=> 'required|alpha_num',
            'skip' => 'integer',
            'take' => 'required|integer|max:10'
        ]);
        

        if(!$request->input('skip') || $request->input('skip') == 0):
            $r = Recommendation::where('type',0)->where('ref',$request->input('ref'))->take($request->input('take'))->orderBy('created_at','desc')->get();
        else:
            $r = Recommendation::where('type',0)->where('ref',$request->input('ref'))->skip($request->input('skip'))->take($request->input('take'))->orderBy('created_at','desc')->get();
        endif;

        $result = [];
        foreach ($r as $recommendation) {
            $u = User::where('_id',$recommendation->user)->get(['name','avatar']);
            $recommendation['user_obj'] = (isset($u[0])) ? $u[0] : null;
            $result[] = $recommendation;
        }

        return response()->json(['message'=>'ok','recommendation'=>$result],200);

    }


    private function formatRecommendationWithoutUser($r){        
        switch($r->type):
            case 0:
                $el = Restaurant::where('_id',$r->ref)->get(['name','image','address','slug','city']);
                if(isset($el[0])) $r->ref_obj = $el[0];                
            break;
            case 1:
                $el = Plates::where('_id',$r->ref)->get(['name','image']);
                if(isset($el[0])) $r->ref_obj = $el[0];
            break;
        endswitch;
        return $r;
    }

    private function formatRecommendationWithUser($r){        
        switch($r->type):
            case 0:
                $el = Restaurant::where('_id',$r->ref)->get(['name','image','address','slug','city']);
                if(isset($el[0])) $r->ref_obj = $el[0];
                $r->user_obj = User::where('_id',$r->user)->get(['name','surname','avatar'])[0];
            break;
            case 1:
                $el = Plates::where('_id',$r->ref)->get(['name','image']);
                if(isset($el[0])) $r->ref_obj = $el[0];
                $r->user_obj = User::where('_id',$r->user)->get(['name','surname','avatar'])[0];
            break;
        endswitch;
        return $r;
    }

    
    public function viewNear(Request $request)
    {
        //
        $this->validate($request, [
            'lat'=>'required|numeric',
            'lng'=>'required|numeric',
            'skip'=>'integer',
            'limit'=>'integer'
        ]);

        $id = (Auth::check()) ? [Auth::user()->_id] : [];

        $mongodb = DB::getMongoDB();
        $r = $mongodb->command(
        array(  'geoNear' => "recommendation_collection", // Put here the collection 
            'near' => array( // 
                'type' => "Point", 
                'coordinates' => array(doubleval($request->input('lng')), doubleval($request->input('lat')))
            ), 
            'spherical' => true, 
            'num'=>5000,
            'maxDistance' => 50000, // 5 km
            'query' => ['user'=> ['$nin'=>$id ] ]                       
        ));

        $return_results = [];
        $skip = 0;
        $limit = 0;
        if(isset($r['results'])):
            foreach ($r['results'] as $result) {
                $id = (array) $result['obj']['_id'];
                $obj = [
                    '_id'=>$id['$id'],
                    'text'=>$result['obj']['text'],
                    'distance'=>$result['dis'],
                    'type'=>$result['obj']['type'],
                    'ref'=>$result['obj']['ref'],
                    'user'=>$result['obj']['user'],
                    'created_at'=>$result['obj']['created_at']
                ];
                // paginazione
                $add = true;
                if($request->input('skip')):
                    $skip++;
                    if($skip <= $request->input('skip')):
                        $add = false;
                    endif;
                endif;
                if($request->input('limit') && $add):
                    if($limit >= $request->input('limit')):
                        $add = false;
                    endif;    
                    $limit++;            
                endif;
                
                if($obj['type'] == 1):
                    $o = Plates::where('_id',$obj['ref'])->get(['name','image']);                    
                else:
                    $o = Restaurant::where('_id',$obj['ref'])->get(['name','image','address','slug','city']);
                endif;

                if($add):
                    $obj['ref_obj'] = (isset($o[0])) ? $o[0] : [];
                    $u = User::where('_id',$obj['user'])->get(['name','avatar']);
                    $obj['user_obj'] = (isset($u[0])) ? $u[0] : null;
                    

                    $return_results[] = $obj;
                endif;
            }
        endif;

        return response()->json(['message'=>'ok','recommendation'=>$return_results],200);

    }
}
