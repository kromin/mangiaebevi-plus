<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\HomeCity;
use MangiaeBevi\Http\Controllers\ImagesController as SingleCityImage;


class HomeCityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $citta = HomeCity::whereNotNull('name')->orderBy('order', 'asc')->get();        
        return response()->json(['message'=>'ok','citta'=>$citta], 200);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string',
            'lat' => 'numeric',
            'lng' => 'numeric',
            'image' => 'string',
            'order'=> 'integer',
            'website'=>'integer' // 0 = menoo, 1 = mangiaebevi, 2 = entrambi
        ]);

        $homecity = new HomeCity;
        $homecity->name = $request->input('name');
        $homecity->lat = $request->input('lat');
        $homecity->lng = $request->input('lng');
        $homecity->image = $request->input('image');
        $homecity->website = $request->input('website');

        $homecity->order = ($request->input('order')) ? $request->input('order') : 0;

        $homecity->save();
        
        return response()->json(['message'=>'ok','citta'=>$homecity], 200);
    }

    /**
     * Update a resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num',
            'name' => 'required|string',
            'lat' => 'numeric',
            'lng' => 'numeric',
            'image' => 'string',
            'order'=> 'integer',
            'website'=>'integer' // 0 = menoo, 1 = mangiaebevi, 2 = entrambi
        ]);

        $homecity = HomeCity::find($request->input('id'));
        if(!$homecity):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        $homecity->name = $request->input('name');
        $homecity->lat = $request->input('lat');
        $homecity->lng = $request->input('lng');
        $homecity->image = $request->input('image');
        $homecity->website = $request->input('website');

        $homecity->order = ($request->input('order')) ? $request->input('order') : 0;

        $homecity->save();
        
        return response()->json(['message'=>'ok','citta'=>$homecity], 200);
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);

        $homecity = HomeCity::find($request->input('id'));
        if(!$homecity):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        $img = new SingleCityImage();
        $return_string = $img->public_destroy($homecity->image);

        if($return_string != 'ok'):
            return response()->json(['message'=>$return_string], 200);
        endif;

        $homecity->delete();
        return response()->json(['message'=>'ok'], 200);

    }
}
