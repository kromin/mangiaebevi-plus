<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;
use Jenssegers\Mongodb\Model as Eloquent;

use MangiaeBevi\Types;
use MangiaeBevi\Restaurant;

class TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $types = Types::all();
        return response()->json(['message'=>'Ok','types'=>$types], 200);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string',
            'image' => 'string'
        ]);

        if($request->input('image')):
            $mime = getImageMimeType($request->input('image') );
            if( !$mime ) return response()->json(['message'=>'error.invalid'], 200);
            if( $mime != 'svg+xml') return response()->json(['message'=>'error.invalid'], 200);
        endif;

        $type = new Types();
        $type->name = $request->input('name');
        $type->slug = str_slug($request->input('name'));
        if($request->input('image')) :
            $type->image = $request->input('image');
        endif;
        $type->save();

        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'type'=>$type 
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);

        $type = Types::find($request->input('id'));

        if(!$type):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        
        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'type' => $type 
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request, [
            'id'    => 'required|alpha_num',
            'name'  => 'required|string',
            'image' => 'string'
        ]);

        $type = Types::find($request->input('id'));
        if(!$type):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        $type->name = $request->input('name');
        if($request->input('image')) :
            $mime = getImageMimeType($request->input('image') );
            if( $mime && $mime == 'svg+xml' ) :        
                $type->image = $request->input('image');
            endif;
        endif;
        $type->slug = str_slug($request->input('name'));
        $type->save();
        
        $statusCode = 200;
        $response = [
          'message' => 'ok'
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);
        $type = Types::find($request->input('id'));
        if(!$type):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        $restaurants = Restaurant::whereIn('tipoCucina',[$request->input('id')])->get();
        foreach ($restaurants as $r) {
            $risto = Restaurant::find($r->_id);
            $tipi = [];
            foreach ($risto->tipoCucina as $tipo) {
                if($tipo != $request->input('id')){
                    $tipi[] = $tipo;
                }
            }
            $risto->tipoCucina = $tipi;
            $risto->save();
        }

        $type->delete();
        return response()->json(['message'=>'ok'], 200);
    }


    public function massiveFillTypes()
    {
        $restaurants = Restaurant::all();
        $types = [];
        foreach($restaurants as $restaurant){
            if($restaurant->tipoCucina) :
                foreach($restaurant->tipoCucina as $type){
                    $types[] = $type;
                }
            endif;
        }

        $types = array_unique($types);

        foreach($types as $type){
            $addToCollection = new Types();
            $addToCollection->name = $type;
            $addToCollection->save();
        }
        return response()->json(['message'=>'ok'], 200);
    }

    public function massiveCleanRestaurantsTypes()
    {

        $types = Types::all();

        $restaurants = Restaurant::all();
        foreach($restaurants as $restaurant){
            // take restaurant types 
            $single_types = [];
            // per ogni servizio del ristorante
            if($restaurant->tipoCucina) :
                foreach($restaurant->tipoCucina as $type){
                    // vedi i servizi dell'app e per ognuno di loro
                    foreach($types as $single_type_collection){
                        // se il nome è uguale al servizio del ristorante
                        if($single_type_collection->name == $type){
                            // inseriscilo nell'array che aggiorna il ristorante
                            $single_types[] = $single_type_collection->_id;
                        }
                    }
                }
            endif;
            if(!empty($single_types)) :
                // aggiorniamo il singolo ristorante
                $this->updateRestaurantTypes($restaurant->_id,$single_types);
            endif;
        }
        return response()->json(['message'=>'ok'], 200);
    }

    private function updateRestaurantTypes($restaurant_id,$types)
    {
        $restaurant = Restaurant::find($restaurant_id);
        $restaurant->tipoCucina = $types;
        $restaurant->save();
    }

    /**
    *
    * Return tipi cucina in json
    * 
    * @param  \Illuminate\Http\Request
    * @return \Illuminate\Http\Response
    * 
    * */
    public function returnCucine(Request $request){
        $types = Types::where(1)->get(['name','slug']);

        return response()->json(['types'=>$types], 200);
    }
}
