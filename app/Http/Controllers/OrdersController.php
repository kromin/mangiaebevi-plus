<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;
use Carbon\Carbon;

use MangiaeBevi\Http\Controllers\NotificationsController as Notification;
use MangiaeBevi\Http\Controllers\PaypalController as Paypal;

use MangiaeBevi\Orders;
use MangiaeBevi\Restaurant;
use MangiaeBevi\Plates;
use MangiaeBevi\ReservationRequest;
use MangiaeBevi\Payments;
use MangiaeBevi\User;

use Curl;
use Auth;
use Session;
use Braintree_Transaction; // only for order refund
use Log;
use Route;
USE View;
use Event;
use Mail;

use MangiaeBevi\Events\OrderInWorking;
use MangiaeBevi\Events\OrderReady;
use Srmklive\PayPal\Services\ExpressCheckout;


class OrdersController extends Controller
{
    /**
     * 0->non confermato
     * 1->pagato
     * 2->in lavorazione
     * 3->rifiutato
     * 4->in consegna
     * 5->consegnato
     * 6->chiesto rimborso
     * 7->rimborsato
     * 8->preso in consegna (quando il driver prende l'ordine al ristorante)
     *
     *
     * - l'utente effettua l'ordine (qual'è il tempo minimo per effettuarlo? credo si debba dare il tempo ai ristoratori di prepararlo, io ho messo almeno mezz'ora prima e in ogni caso ad almeno mezz'ora dall'apertura del ristorante) (da web e app)
     * - l'utente paga l'ordine (da web e app)
     *
     * - il ristoratore può se vuole rifiutare l'ordine (da web)
     * - il cliente se vuole può chiedere il rimborso fino a che l'ordine non è in lavorazione (da web e app)
     *
     * - il ristoratore imposta l'ordine come in lavorazione inserendo l'orario in cui pensa che sia pronto (a questo punto credo si possa inviare la notifica ai driver)  (da web)
     *
     * - il primo driver che prende l'ordine viene associato a quell'ordine (da app)
     *
     * - il ristoratore imposta l'ordine come pronto (da web)
     *
     * - il driver prende l'ordine che diventa in consegna (da app)
     *
     * - il driver consegna l'ordine (da app)
     *
     * "tempo tra ordine pronto e ordine preso" e "tempo tra ordine preso e ordine consegnato" vengono conteggiati per tracking delle consegne e delle tempistiche del driver
     *
     * - il cliente vota il driver con punteggio da 1 a 5 (da app)
     *
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $statusCode = 200;
        $response = [
          'orders'  => Orders::all()
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Prendi ordini di un ristorante
     * @param \Illuminate\Http\Request restaurant_id | date
     * @return \Illuminate\Http\Response
     */
    public function indexByDayAndRestaurant(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id' => 'required|alpha_num',
            'date'=>'required|date_format:Y-m-d'
        ]);

        $from = Carbon::createFromFormat('Y-m-d H:i', $request->input('date')." 00:00");
        $to = Carbon::createFromFormat('Y-m-d H:i', $request->input('date')." 24:00");

        $statusCode = 200;
        $response = [
            'message'=>'ok',
            'from'=>$from,
            'to'=>$to,
            'orders'  => Orders::where('restaurant',$request->input('restaurant_id'))
                                    ->where('date','>=',$from)
                                    ->where('date','<=',$to)
                                    ->get()
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Prendi ordini di un ristorante
     * @param \Illuminate\Http\Request restaurant_id | date
     * @return \Illuminate\Http\Response
     */
    public function indexByFromToAndRestaurant(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id' => 'required|alpha_num',
            'from'=>'required|date_format:Y-m-d',
            'to'=>'required|date_format:Y-m-d'
        ]);

        $from = Carbon::createFromFormat('Y-m-d H:i', $request->input('from')." 00:00");
        $to = Carbon::createFromFormat('Y-m-d H:i', $request->input('to')." 24:00");

        $statusCode = 200;
        $response = [
            'message'=>'ok',
            'from'=>$from,
            'to'=>$to,
            'orders'  => Orders::where('restaurant',$request->input('restaurant_id'))
                                    ->where('date','>=',$from)
                                    ->where('date','<=',$to)
                                    ->get()
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Prendi prossimi 10 ordini
     * @param \Illuminate\Http\Request restaurant_id | date
     * @return \Illuminate\Http\Response
     */
    public function indexByNextAndRestaurant(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id' => 'required|alpha_num'
        ]);

        $from = Carbon::now();

        $statusCode = 200;
        $response = [
            'message'=>'ok',
            'from'=>$from,
            'to'=>$to,
            'orders'  => Orders::where('restaurant',$request->input('restaurant_id'))
                                    ->where('date','>=',$from)
                                    ->limit(10)
                                    ->get()
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Prendi ordini di un ristorante in un determinato status
     * @param \Illuminate\Http\Request restaurant_id | date | status
     * @return \Illuminate\Http\Response
     */
    public function indexByDayAndRestaurantAndStatus(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id' => 'required|alpha_num',
            'date'=>'required|date_format:Y-m-d',
            'status'=>'required|integer|min:0|max:4' // avaible: 0 non confermato, 1 confermato, 2 in lavorazione, 3 in consegna, 4 consegnato
        ]);

        $from = Carbon::createFromFormat('Y-m-d H:i', $request->input('date')." 00:00")->toDateTimeString();
        $to = Carbon::createFromFormat('Y-m-d H:i', $request->input('date')." 24:00")->toDateTimeString();

        $statusCode = 200;
        $response = [
            'message'=>'ok',
            'orders'  => Orders::where('restaurant',$request->input('restaurant_id'))
                                    ->where('date','>=',$from)
                                    ->where('date','<=',$to)
                                    ->where('status',$request->input('status'))
                                    ->get()
        ];
        return response()->json($response, $statusCode);
    }


    /**
     * Prendi ordini di un utete, middleware isMeOrIsAdmin
     * @param \Illuminate\Http\Request user_id
     * @return \Illuminate\Http\Response
     */
    public function viewByUser(Request $request)
    {
        //
        $this->validate($request, [
            'user_id' => 'required|alpha_num'
        ]);

        $statusCode = 200;
        $response = [
            'message'=>'ok',
            'orders'  => Orders::where('user',$request->input('user_id'))->orderBy('date','desc')->get()
        ];
        return response()->json($response, $statusCode);
    }


    /**
     * Prendi ordini di un utete, middleware isMeOrIsAdmin
     * @param \Illuminate\Http\Request user_id
     * @return \Illuminate\Http\Response
     */
    public function viewSingle(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);

        $order = Orders::find($request->input('id'));
        if(!$order) return response()->json(['message'=>'error.empty'],200);

        if(Auth::user()->role < 3):
          $restaurant = Restaurant::find($order->restaurant);
          if(!$restaurant) return response()->json(['message'=>'error.empty'],200);
          if(Auth::user()->_id != $order->user &&
            Auth::user()->_id != $restaurant->user):
            return response()->json(['message'=>'error.non_puoi'],200);
          endif;
        endif;

        $statusCode = 200;
        $response = [
            'message'=>'ok',
            'order'  => $order
        ];
        return response()->json($response, $statusCode);
    }



    /**
     * Imposta status ordine
     * 0->non confermato
     * 1->pagato
     * 2->in lavorazione (expected_ready timestamp required)
     * 3->rifiutato
     * 4->in consegna
     * 5->consegnato
     * 6->chiesto rimborso
     * 7->rimborsato
     * 8->ordine pronto
     *
     * @todo expected_ready
     *
     * @param \Illuminate\Http\Request restaurant_id | order | new status
     * @return \Illuminate\Http\Response
     */
    public function setStatusOrder(Request $request)
    {
        //
        $this->validate($request, [
            'id'=>'required|alpha_num',
            'expected_ready' => 'integer|min:5|max:20',
            'status'=>'required|integer' // avaible: 0 non confermato, 1 confermato, 2 in lavorazione, 3 in consegna, 4 consegnato
        ]);


        $statusCode = 200;

        $order = Orders::find($request->input('id'));
        if(!$order) return response()->json(['message'=>'error.empty'],200);

        // se chiesto il rimborso unica azione possibile è quella
        if($order->status == 6) return response()->json(['message'=>'error.non_puoi'],200);

        $restaurant = Restaurant::find($order->restaurant);
        if(Auth::user()->_id != $restaurant->user && Auth::user()->role < 3)  return response()->json(['message'=>'error.non_puoi'],200);

        $s = $request->input('status');

        if($s != 2 && $s != 3) :
            // solo admin può mettere questi status
            if(Auth::user()->role >= 3) :
                $order->status = $request->input('status');
            else:
                return response()->json(['message'=>'error.non_puoi'],200);
            endif;

        endif;

        switch($s):
            case 2:
                if(!$request->input('expected_ready')) return response()->json(['message'=>'error.no_expected'],200);
                $order->expected_ready = time() + ($request->input('expected_ready') * 60);//Carbon::createFromFormat('Y-m-d H:i', $request->input('expected_ready'))->toDateTimeString();
                $order->status = $request->input('status');
                // salvo prima per evitare errori
                //$order->save();
                // Evento ordine in lavorazione
                Event::fire(new OrderInWorking($order));
            break;
            case 8:
                // ordine pronto, notifica il driver con notifica push
                Event::fire(new OrderReady($order));
                $order->status = $request->input('status');
            break;
            default:
                $order->status = $request->input('status');
                $order->save();
            break;
        endswitch;


        /**
        * sta lavorando l'ordine, faccio pagare l'utente
        * @deprecated
        *
        * */
        /*
        if(($s == 2 || $s == 4 || $s == 5) && $order->status < 2):
          $result = Braintree_Transaction::submitForSettlement($order->transaction);
          if(!$result->success) return response()->json(['message'=>'error.settlement_rejected'],200);
        endif;*/


        return response()->json(['message'=>'ok','order'=>$order], $statusCode);
    }


    /**
    *
    * Confirm an order and push the id in the session, in the future will be used in update    *
    * order status will be 0 -> not cofirmed
    *
    * order->date is the delivery time the user selected
    *
    * 'restaurant' => 'required|alpha_num',
    * 'menu'=>'required|integer',
    * 'menu_name'=>'required|string',
    * 'plates'=>'required|array',
    * 'price'=>'required|numeric'
    * 'date'=> 'date_format:Y-m-d H:i'
    * 'type'=>'required|integer'
    *
    *
    *
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response message=>'ok','order'=>$order
    *
    * **/
    public function orderFirstConfirm(Request $request)
    {
      $this->validate($request, [
        'restaurant' => 'required|alpha_num',
        'menu'=>'required|integer',
        'menu_name'=>'required|string',
        'plates'=>'required|array',
        'subtotal'=>'required|numeric',
        'delivery'=>'required|numeric',
        'price'=>'required|numeric',
        'type'=>'required|integer',
        'date_string'=>'date_format:Y-m-d H:i'
      ]);

      // qui è da impostare la validazione dell'ordine


      $order = new Orders();
      $order->restaurant = $request->input('restaurant');
      $order->menu = $request->input('menu');
      $order->menu_name = $request->input('menu_name');
      $order->plates = $request->input('plates');
      $order->subtotal = $request->input('subtotal');
      $order->delivery = $request->input('delivery');
      $order->price = $request->input('price');
      $order->type = $request->input('type');
      $order->date = Carbon::createFromFormat('Y-m-d H:i', $request->input('date_string'))->toDateTimeString();

      $order->status = 0;

      $order->save();

      Session::put('current_order', $order->_id); // put order in session for later Session::get('current_order');
      return response()->json(['message'=>'ok','order'=>$order],200); // return ORDER to take id

    }



    /**
    *
    * Confirm an order and push the id in the session, in the future will be used in update    *
    * order status will be 0 -> not cofirmed, set also the order user
    * 'address' => 'required|string',
    * 'city'=>'required|string',
    * 'state'=>'required|string',
    * 'zip'=>'required|string',
    * 'phone'=>'required|numeric',
    *
    *
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response message=>'ok','order'=>$order
    *
    * **/
    public function orderSetAddressType(Request $request)
    {
      $this->validate($request, [
        'address' => 'required|string',
        'city'=>'required|string',
        'state'=>'required|string',
        'zip'=>'required|string',
        'phone'=>'required|numeric',
        'extra'=>'string'
      ]);

      // qui è da impostare la validazione dell'ordine

      $session_order = Session::get('current_order');
      $order = Orders::find($session_order);
      if(!$order) return response()->json(['message'=>'error.empty'],200);

      $order->address = $request->input('address');
      $order->city = $request->input('city');
      $order->state = $request->input('state');
      $order->zip = $request->input('zip');
      $order->phone = $request->input('phone');
      $order->extra = ($request->input('extra')) ? $request->input('extra') : '';
      $order->user = Auth::user()->_id;
      $order->save();

      return response()->json(['message'=>'ok','order'=>$order],200); // return ORDER to take id

    }

    /**
    *
    * Confirm an order and push the id in the session, in the future will be used in update    *
    * order status will be 0 -> not cofirmed, set also the order user
    * 'phone'=>'required|numeric'
    *
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response message=>'ok','order'=>$order
    *
    * **/
    public function orderSetTakeType(Request $request)
    {

      //return response()->json(['date_string'=>$request->input('date_string')],200);
      $this->validate($request, [
        'phone'=>'required|numeric'
      ]);

      // qui è da impostare la validazione dell'ordine

      $session_order = Session::get('current_order');
      $order = Orders::find($session_order);
      if(!$order) return response()->json(['message'=>'error.empty'],200);

      $order->phone = $request->input('phone');
      $order->user = Auth::user()->_id;

      $order->save();

      return response()->json(['message'=>'ok','order'=>$order],200); // return ORDER to take id

    }

    /**
     * Store a newly created resource in storage.
     * If is a logged user there's no need to add name email ecc...
     *
     * order structure:
     *
     * "ordine": [
     *              {"nome":"nome piatto","porzioni":integer,"prezzo_totale":double},
     *              {"nome":"nome piatto","porzioni":integer,"prezzo_totale":double}...
     * ]
     * @deprecated
     * @todo event order sent
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ordina(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant' => 'required|alpha_num',
            'date'=>'required|date_format:Y-m-d', // data di consegna
            'time'=>'required|string',
            'name'=>'string',
            'surname'=>'string',
            'email'=>'email',
            'tel'=>'required|numeric',
            'address'=>'required|string',
            'ordine'=>'required|array'// array piatti [id,qty]
        ]);

        // effettuato = Carbon->now

        //Carbon::setToStringFormat('Y-m-d');
        $day = Carbon::createFromFormat('Y-m-d H:m', $request->input('date')." ".$request->input('time'))->toDateTimeString();
        //Carbon::parse($request->input('day'));

        $restaurant = Restaurant::find($request->input('restaurant'));
        if($restaurant->ordina == 0 || !$restaurant->ordina):
            return response()->json(['message'=>'error.noorders'], 200);
        endif;

        $order = new Orders();

        $user = [];

        /**
        *
        * Parsing dell'indirizzo
        *
        * **/
        $google_response = Curl::to('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($request->input('address')))->get();
        $response = json_decode($google_response,true);
        if(isset($response['results'][0]['address_components'])) :
            $locations = [];
            foreach ($response['results'][0]['address_components'] as $address_component) {
                $locations[] = $address_component['long_name'];
            }
            $address = $request->input('address');// better to mantain $response['results'][0]['formatted_address'];

            $target_lat = $response['results'][0]['geometry']['location']['lat'];
            $target_lng = $response['results'][0]['geometry']['location']['lng'];

            $order->loc = ['type'=>"Point","coordinates"=>[doubleval($target_lng),doubleval($target_lat)]];
            $order->location = $locations;
        else:
            return response()->json(['message'=>'error.google_address_not_found'], 200);//'response'=>$response['results'][0]
        endif;

        // prendi latitudine e longitudine ristorante e calcola distanza
        if($restaurant->valid_address && $restaurant->valid_address == 1):

            $lng = $restaurant->loc['coordinates'][0];
            $lat = $restaurant->loc['coordinates'][1];

        else:
            // ristorante con indirizzo non verificato
            // lo verifichiamo subito
            // se la chiamata non è valida usciamo
            $google_response = Curl::to('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode( $restaurant->address) )->get();

            $response = json_decode($google_response,true);
            if(isset($response['results'][0]['address_components'])) :
                $locations = [];
                foreach ($response['results'][0]['address_components'] as $address_component) {
                    $locations[] = $address_component['long_name'];
                }
                $restaurant->location = array_unique($locations);
                $restaurant->address = $request->input('address');// better to mantain $response['results'][0]['formatted_address'];

                $lat = $response['results'][0]['geometry']['location']['lat'];
                $lng = $response['results'][0]['geometry']['location']['lng'];

                $restaurant->loc = ['type'=>"Point","coordinates"=>[doubleval($lng),doubleval($lat)]];

                $restaurant->valid_address = 1;// imposto il valid address
                $restaurant->save();
            else:
                return response()->json(['message'=>'error.restaurant_google_address_not_found'], 200);//'response'=>$response['results'][0]
            endif;

        endif;

        // ora ho lat e lng e target_lat e target_lng
        // faccio curl a google per sapere la distanza
        // se maggiore di restaurant range l'ordine non può partire
        $distance = Curl::to('https://maps.googleapis.com/maps/api/distancematrix/json?origins='.urlencode($lat.','.$lng).'&destinations='.urlencode($target_lat.','.$target_lng))->get();
        $distance_response = json_decode($distance,true);

        if(isset($distance_response['rows'][0]['elements'][0]['distance']['value'])) :

            $d = $distance_response['rows'][0]['elements'][0]['distance']['value']; // metri di distanza
            if($d > $restaurant->range):
                return response()->json(['message'=>'error.too_distance','distance'=>$d], 200);
            endif;

        else:
            return response()->json(['message'=>'error.distance_google_not_found','response'=>$distance_response['rows'][0]], 200);
        endif;


        if(Auth::check()) :
            $user['id'] = Auth::user()->_id;
            $user['name'] = Auth::user()->name;
            $user['surname'] = Auth::user()->surname;
            $user['email'] = Auth::user()->email;
        else : // logged user
            $user['name'] = $request->input('name');
            $user['surname'] = $request->input('surname');
            $user['email'] = $request->input('email');
            $user['phone'] = $request->input('phone');
        endif; // not loggedin user

        $order->consegna_desiderata_time = $day;
        $order->consegna = "0";

        $order->user = $user;
        $order->restaurant = $request->input('restaurant');


        $ordine = $request->input('ordine');
        $ordine_formattato = [];
        $total_price = 0;
        foreach($ordine as $piatto):
            // solo gli id e
            // quantita
            $plate = Plates::find($piatto['id']);
            if( $plate && $plate->restaurant == $restaurant->_id ):
                $total_price += $plate->price*intval($piatto['qty']);
                $plate->qty = $piatto['qty'];
                $ordine_formattato[] = [
                                'id'=> $plate->_id,
                                'name'=>$plate->name,
                                'qty'=>$piatto['qty'],
                                'price'=>$plate->price*intval($piatto['qty'])
                                ];
            endif;

        endforeach;
        $order->price = $total_price;


        $order->ordine = $ordine_formattato;
        $order->string = base64_encode(str_random(40)); // stringa di validazione per vedere l'ordine pubblicamente
        $order->status = 0;
        $order->address = $request->input('address');
        $order->save();

        $message = 'ok';


        return response()->json(['message'=>$message,'order'=>$order], 200);
    }



    /**
     * Chiedi rimborso per un ordine, i rimborsi vengono gestiti solo dagli amministratori
     * @param type Request $request
     * @return type
     */
    public function askRefund(Request $request)
    {
      $this->validate($request, [
        'order'=>'required|alpha_num'
      ]);

      $order = Orders::find($request->input('order'));
      if(!$order || $order->user != Auth::user()->_id) return response()->json(['message'=>'error.empty'],200);

      $restaurant = Restaurant::find($order->restaurant);
      $ristoratore = ($restaurant->user) ? $restaurant->user : '0';

      //return response()->json(['message'=>'error.empty', 'order'=>$order],200);
      $order->status = 6;
      $order->save();

      $parameters = [$order->_id,$restaurant->_id];
      Notification::addNotification(7,$parameters,0);

      if($ristoratore != 0):
        Notification::addNotification(7,$parameters,$ristoratore);
      endif;

      return response()->json(['message'=>'ok'],200);
    }

    /**
     * Rimborsa
     * @param type Request $request
     * @return type
     */
    public function refund(Request $request)
    {
      $this->validate($request, [
        'order'=>'required|alpha_num'
      ]);

      $order = Orders::find($request->input('order'));
      if(!$order) return response()->json(['message'=>'error.empty'],200);

      $result = Braintree_Transaction::refund($order->transaction);

      if(!$result->success) return response()->json(['message'=>'error.refund_rejected'],200);

      $order->status = 7;
      $order->save();

      $parameters = [$order->_id,$order->restaurant];
      Notification::addNotification(8,$parameters,$order->user);

      return response()->json(['message'=>'ok'],200);
    }


    /**
    *
    * Ordine pagato con paypal
    *
    * @see MangiaeBevi\Http\Controllers\Paypal;
    *
    * @param Illuminate\Http\Request $request
    * @param string $order_number
    *
    * @return view close payment window
    * */
    public function orderPayedWithPaypal(Request $request, $order_number){

      $requestUri = $request->getRequestUri();

      $id_pagamento = $request->query('paymentId');
      $token = $request->query('token');
      $payer = $request->query('PayerID');

      $ex_pay = new Paypal();
      if(!$ex_pay->executePaypalPayment($payer,$id_pagamento)) return view('closepayment');

      $order = Orders::find($order_number);


      if($order):

        $order->transaction = 'paypal';
        $order->payment = $id_pagamento;
        $order->token = $token;
        $order->payer = $payer;
        $order->status = 1;


        $restaurant = Restaurant::find($order->restaurant);
        $ristoratore = ($restaurant->user) ? $restaurant->user : '0';

        $order->restaurant_info = [
        'image'=>$restaurant->image,
        'name'=>$restaurant->name,
        'slug'=>$restaurant->slug
        ];
        $order->user_info = [
        'image'=>Auth::user()->avatar,
        'name'=>Auth::user()->name . ' '.Auth::user()->surname
        ];
        $order->save();

        // [id_ordine, id utente, nome utente, prezzo totale, data consegna]
        $parameters = [$order->_id,Auth::user()->_id,Auth::user()->name.' '.Auth::user()->surname,$order->price,$order->date,$order->restaurant];
        Notification::addNotification(1,$parameters,$ristoratore);
      endif;

      return view('closepayment');
    }

    /**
    *
    * Ordine rifiutato da paypal
    *
    * L'ordine non è stato completato all'apertura della finestra Paypal
    *
    * @param string order id
    * @return view close payment window
    * */
    public function orderNotPayedWithPaypal($order_number){
      $order = Orders::find($order_number);
      $order->remove();
      return view('closepayment');
    }

    /**
    *
    * Verifica che l'ordine effettuato con Paypal sia stato pagato
    * @param Illuminate\Http\Request $request
    * @return Illuminate\Http\Response $response
    * */
    public function isPaypalPayd(Request $request)
    {
      $this->validate($request,[
        'order'=>'required|alpha_num'
        ]);

      $order = Orders::find($request->input('order'));
      if(!$order || $order->status != 1) return response()->json(['message'=>'ko'],200);

      return response()->json(['message'=>'ok'], 200);
    }


    /**
    *
    * Confirm an order and push the id in the session, in the future will be used in update    *
    * order status will be 0 -> not cofirmed
    *
    * order->date is the delivery time the user selected
    *
    * 'restaurant' => 'required|alpha_num',
    * 'reservation' => 'required|alpha_num',
    * 'menu'=>'required|integer',
    * 'menu_qty'=>'required|integer',
    * 'menu_name'=>'required|string',
    * 'plates'=>'required|array',
    * 'price'=>'required|numeric'
    *
    *
    *
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response message=>'ok','order'=>$order
    *
    * **/
    public function addPreorder(Request $request)
    {
      $this->validate($request, [
        'restaurant' => 'required|alpha_num',
        'reservation' => 'required|alpha_num',
        'menu'=>'required|integer',
        'menu_qty'=>'required|integer',
        'menu_name'=>'required|string',
        'plates'=>'required|array',
        'subtotal'=>'required|numeric',
        'price'=>'required|numeric'
      ]);

      // qui è da impostare la validazione dell'ordine

      $reservation = ReservationRequest::find($request->input('reservation'));
      if(!$reservation) return response()->json(['message'=>'error', 'error'=>'no_reservation'], 404);
      if($reservation->preorder) return response()->json(['message'=>'error', 'error'=>'just_added'], 403);
      if($reservation->confirmed != 0) return response()->json(['message'=>'error', 'error'=>'just_confirmed'], 403);

      if($reservation->user != Auth::user()->_id) return response()->json(['message'=>'error', 'error'=>'invalid_user'], 403);

      $restaurant = Restaurant::find($request->input('restaurant'));
      if(!$restaurant) return response()->json(['message'=>'error', 'error'=>'no_restaurant'], 404);

      $avaible_menu = [];
      $i = 0;
      foreach ($restaurant->menu['menus'] as $menu) {
          if(isset($menu['orderready']) && $menu['orderready'] == 1) $avaible_menu[] = $i;
          $i++;
      }

      if(!in_array( $request->input('menu'), $avaible_menu)) return response()->json(['message'=>'error', 'error'=>'invalid_menu'], 403);

      if($restaurant->menu['menus'][$request->input('menu')]['name'] != $request->input('menu_name')) return response()->json(['message'=>'error', 'error'=>'invalid_menu_name'], 403);

      $order_array = [
        'menu'=>$request->input('menu'),
        'menu_qty'=>($request->input('menu_qty')) ? $request->input('menu_qty') : 0,
        'menu_name'=>$request->input('menu_name'),
        'menu_type'=>(isset($restaurant->menu['menus'][$request->input('menu')]['menu_type'])) ? $restaurant->menu['menus'][$request->input('menu')]['menu_type'] : 0,
        'plates'=>$request->input('plates'),
        'price'=>$request->input('price')
      ];


      if(!self::validateOrder($order_array, $restaurant->menu['menus'])) return response()->json(['message'=>'error', 'error'=>'not_valid_order'],403);
      $reservation->preorder = $order_array;
      $reservation->save();


      return response()->json(['message'=>'ok','reservation'=>$reservation],200); // return ORDER to take id

    }

    /**
     * Verifica che il prezzo totale corrisponda
     * @param  $order array con i piatti
     * @param  $restaurant_menu i menu del ristorante
     *
     * @return  bool true se valido
     */
    private static function validateOrder($order, $restaurant_menu, $app = 0)
    {


        $restaurant_selected = intval($order['menu']);

        if($restaurant_menu[$restaurant_selected]['name'] != $order['menu_name']) return false;

        if($order['menu_type'] == 0) {
            $total_price = 0.00;
        } else {
            $total_price = $restaurant_menu[$restaurant_selected]['menu_price'] * $order['menu_qty'];
            $total_plate_options = $order['menu_qty'];
        }


        foreach ($order['plates'] as $plate_ordered){
            foreach ($restaurant_menu[$restaurant_selected]['categorie'] as $categoria_piatti) {
                foreach ($categoria_piatti['piatti'] as $piatto) {
                    if($piatto['id'] == $plate_ordered['id']){
                        // verifichiamo il singolo piatto
                        $plate_price = ($order['menu_type'] == 0) ? $piatto['price'] * $plate_ordered['qty'] : 0.00; // calcolo il prezzo del piatto

                        $n_categoria_opzione = 0;
                        foreach ($plate_ordered['options'] as $categoria_opzione) {

                            $n_opzione = 0;
                            foreach ($categoria_opzione['options'] as $opzione) {

                                if(
                                    ($order['menu_type'] == 0 && isset($opzione['selected']) && $opzione['selected'] == 1) ||
                                    ($order['menu_type'] == 1 && isset($opzione['num']) && $opzione['num'] > 0)
                                    ){

                                    // l'opzione è selezionata, verifico se ha variazioni di prezzo
                                    // opzione nel menu del ristorante
                                    $opt = $piatto['options'][$n_categoria_opzione]['options'][$n_opzione];
                                    if($opt['no_variation'] == 0){
                                        // cambia in base al tipo di menu
                                        $qty_to_check = ($order['menu_type'] == 0) ? $plate_ordered['qty'] : $opzione['num'];
                                        switch($opt['variation']){
                                            case 0:
                                            // aggiungi prezzo
                                            $plate_price += $opt['price'] * $qty_to_check;
                                            break;
                                            case 1:
                                            // sottrai prezzo
                                            $plate_price -= $opt['price'] * $qty_to_check;
                                            break;
                                        }
                                    } // se c'è variazione di prezzo
                                }
                                $n_opzione++;
                            }
                            $n_categoria_opzione++;
                        }

                        $total_price += $plate_price;
                    } // se è il piatto corrente
                }   // per ogni piatto del ristorante
            } // per ogni categoria di piatti del ristorante
        } // per ogni piatto ordinato


        if($total_price != $order['price']){
            if($app != 0) {
                Log::info('ordine effettuato dall\'app non valido: '.$total_price.' != '.$order['price']);
            } else {
                Log::info('ordine non valido: '.$total_price.' != '.$order['price'].', utente: '.Auth::user()->email);
            }
            return false;
        } else {
            return true;
        }

    }


    /**
     * Paga preordine associato a prenotazione
     * @param  request
     * @return  link url
     */
    public function payPreorder(Request $request)
    {
        $this->validate($request, [
            'reservation' => 'required|alpha_num'
        ]);
        $reservation = ReservationRequest::find($request->input('reservation'));
        if(!$reservation) return response()->json(['message'=>'error', 'error'=>'no_reservation'],404);
        if(!$reservation->preorder || !$reservation->preorder['price']) return response()->json(['message'=>'error', 'error'=>'no_preorder'],404);

        $provider = new ExpressCheckout;


        $options = [
            'BRANDNAME' => 'MangiaeBevi+',
            'LOGOIMG' => env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'img/logo.png',
            'CHANNELTYPE' => 'Merchant'
        ];

        $data = [];
        $data['items'] = [
            [
                'name' => 'Ordine prenotazione #'.$reservation->_id,
                'price' => $reservation->preorder['price'],
                'qty' => 1
            ]
        ];

        $data['invoice_id'] = $reservation->_id;
        $data['invoice_description'] = "Pagamento ordine #".$reservation->_id."";
        $data['return_url'] = url('/orders/confirm_preorder_paid');
        $data['cancel_url'] = url('/orders/cancel');
        $data['total'] = $reservation->preorder['price'];

        $response = $provider->setExpressCheckout($data);

        if(!isset($response['paypal_link'])) return response()->json(['message'=>'error','error'=>'paypal_error'],403);
        return response()->json( ['message'=>'ok', 'paypal_link'=>$response['paypal_link'] ],200);

    }

    /**
     * Fai redirect a Paypal
     * @param  $reservation
     * @return  link url
     */
    public function payFromRedirect($reservation_id)
    {
        $title = "Errore";
        $message = "Qualcosa è andato storto";


        $reservation = ReservationRequest::find($reservation_id);
        if(!$reservation) return View::make('application/orderpaid')->withTitle($title)->withMessage($message);
        if(!$reservation->preorder || !$reservation->preorder['price']) return View::make('application/orderpaid')->withTitle($title)->withMessage($message);

        $provider = new ExpressCheckout;


        $options = [
            'BRANDNAME' => 'MangiaeBevi+',
            'LOGOIMG' => env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'img/logo.png',
            'CHANNELTYPE' => 'Merchant'
        ];

        $data = [];
        $data['items'] = [
            [
                'name' => 'Ordine prenotazione #'.$reservation->_id,
                'price' => $reservation->preorder['price'],
                'qty' => 1
            ]
        ];

        $data['invoice_id'] = $reservation->_id;
        $data['invoice_description'] = "Pagamento ordine #".$reservation->_id."";
        $data['return_url'] = url('/orders/confirm_preorder_paid');
        $data['cancel_url'] = url('/orders/cancel');
        $data['total'] = $reservation->preorder['price'];

        $response = $provider->setExpressCheckout($data);

        if(!isset($response['paypal_link'])) return View::make('application/orderpaid')->withTitle($title)->withMessage($message);

        return redirect($response['paypal_link']);

    }

    public function doPayment(Request $request)
    {
        $title = "Congratulazioni";
        $message = "Il pagamento è andato a buon fine";

        $token = $request->get('token');
        $PayerID = $request->get('PayerID');

        $provider = new ExpressCheckout;

        $response = $provider->getExpressCheckoutDetails($token);

        $reservation = ReservationRequest::find($response["INVNUM"]);
        if(!$reservation) {
            Log::info("Prenotazione non trovata per ordine con token ".$token);
            $title = "Errore";
            $message = "Non trovo la prenotazione associata";
            return View::make('application/orderpaid')->withTitle($title)->withMessage($message);
        }

        if(isset($reservation->order['paid']) && $reservation->order['paid'] == 1){
            return View::make('application/orderpaid')->withTitle($title)->withMessage($message);
        }

        $data = [];
        $data['items'] = [
            [
                'name' => 'Ordine prenotazione #'.$reservation->_id,
                'price' => $reservation->preorder['price'],
                'qty' => 1
            ]
        ];

        $data['invoice_id'] = $reservation->_id;
        $data['invoice_description'] = "Pagamento ordine #".$reservation->_id."";
        $data['return_url'] = url('/orders/confirm_preorder_paid');
        $data['cancel_url'] = url('/orders/cancel');
        $data['total'] = $reservation->preorder['price'];

        $responseDone = $provider->doExpressCheckoutPayment($data, $token, $PayerID);
        if($responseDone['ACK'] != "Success"){
            $title = "Errore";
            $message = "Qualcosa è andato storto col pagamento su Paypal";
            return View::make('application/orderpaid')->withTitle($title)->withMessage($message);
        }


        $preorder = $reservation->preorder;
        $preorder['paid'] = 1;
        $reservation->preorder = $preorder;
        $reservation->save();



        $payment = new Payments();
        $payment->reservation = $reservation->_id;
        $payment->user = $reservation->user;
        $u = User::find($reservation->user);

        $details = [
            'paypal'=>$response,
            'reservation'=>$reservation,
            'userObj'=>($u) ? $u : null
        ];
        $payment->details = $details;
        $payment->save();


        $risto = Restaurant::find($reservation->restaurant);
        $mail_data = [
            'payment'=>$payment->details,
            'restaurant'=>($risto) ? $risto : null,
            'email' => env('ADMIN_EMAIL')
        ];

        Mail::send("email.newpayment", ['mail_data' => $mail_data],
            function ($m) use ($mail_data) {
            $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
            $m->to($mail_data['email'], 'Admin')->subject("Nuovo pagamento");
        });


        return View::make('application/orderpaid')->withTitle($title)->withMessage($message);




    }

    public function cancelPayment(Request $request)
    {
        $title = "Confermato";
        $message = "Il pagamento è stato annullato";

        return View::make('application/orderpaid')->withTitle($title)->withMessage($message);


    }

}
