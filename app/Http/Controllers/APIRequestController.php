<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use Auth;
use Mail;
use Carbon\Carbon;
use DB;
use Log;

use MangiaeBevi\User;
use MangiaeBevi\Restaurant;
use MangiaeBevi\ReservationRequest;
use MangiaeBevi\Http\Controllers\GoogleAddressController as Address;
use MangiaeBevi\Http\Controllers\ImagesController as SingleImage;
use MangiaeBevi\Http\Controllers\NotificationsController as Notification;
use MangiaeBevi\Http\Controllers\ClientController as RestaurantClients;
use MangiaeBevi\Http\Controllers\OrdersController;

// for tools
use MangiaeBevi\Services;
use MangiaeBevi\TipoLocale;
use MangiaeBevi\Types;
use MangiaeBevi\HomeCity;
use MangiaeBevi\CreditCard;
use MangiaeBevi\Regions;
use MangiaeBevi\Pacchetti;

use Event;
use MangiaeBevi\Events\UserRegistered;
use MangiaeBevi\Orders;
use MangiaeBevi\Events\OrderDelivered;



/**
* @todo adding firebase
* */

class APIRequestController extends Controller
{
    /**
    * @var array ['facebook','twitter','linkedin']
    * */
    private $networks = ['facebook','twitter','linkedin'];

    /**
     * Simple test to validate the API middleware and endpoint
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/test</p>
     *
     * @link '/v1/api/public/test'
     * @return \Illuminate\Http\Response
     */
    public function test()
    {
        //
        return response()->json(['message'=>'ok'],200);
    }

    /**
     * Get website main infos     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/getInfo</p>
     *
     * <p>
     * <pre>
     * {
     *  "message": "ok",
     *  "tipiLocali": [
     *    {
     *      "_id": "56c2eb97bffebc89088b4567",
     *      "name": "Ristorante"
     *    },
     *    {
     *      "_id": "56c2eb97bffebc89088b4568",
     *      "name": "Osteria"
     *   },
     *    ...
     *  ],
     *  "homeCities": [],
     *  "pacchetti": [
     *    {
     *      "_id": "56c33e16bffebc500a8b4568",
     *     "name": "Silver",
     *     "featured": 0,
     *      "price_month": 69,
     *      "price_annual": 690,
     *      "period": "2",
     *      "services": [
     *        "prenotazioni sul tuo sito, app e pagina Facebook"
     *      ],
     *      "updated_at": "2016-04-02 09:43:25",
     *      "created_at": "2016-02-16 15:19:50",
     *      "website": "0",
     *      "braintree_plan": "menoo_silver_monthly|menoo_silver_yearly"
     *   },
     *    {
     *      "_id": "56feac05bffebcb80c8b4567",
     *      "name": "Platinum",
     *      "featured": 0,
     *      "price_month": 149,
     *     "price_annual": 1490,
     *      "period": "2",
     *      "services": [
     *        "prenotazioni sul tuo sito, app e pagina Facebook",
     *        "App IOS customizzata",
     *        "App Android customizzata",
     *        "Sito Web"
     *      ],
     *      "updated_at": "2016-04-02 09:42:37",
     *      "created_at": "2016-04-01 17:12:37",
     *      "website": "0",
     *      "braintree_plan": "menoo_platinum_monthly|menoo_platinum_yearly"
     *    },
     *    {
     *      "_id": "56feabe7bffebcbb0c8b4567",
     *      "name": "Gold",
     *      "featured": 1,
     *      "price_month": 99,
     *      "price_annual": 990,
     *      "period": "2",
     *      "services": [
     *        "prenotazioni sul tuo sito, app e pagina Facebook",
     *        "App IOS customizzata",
     *        "App Android customizzata"
     *      ],
     *      "updated_at": "2016-04-02 09:43:26",
     *      "created_at": "2016-04-01 17:12:07",
     *      "website": "0",
     *      "braintree_plan": "menoo_gold_monthly|menoo_gold_yearly"
     *    }
     *  ],
     *  "staticDomain": "static.mangiaebevi.dev",
     *  "delivery_price": "3",
     *  "minimum_order": "15",
     *  "braintree": {
     *      "merchant_id": "",
     *      "public_key": "",
     *      "private_key": ""
     *      }
     *}
     * </pre>
     * </p>
     *
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li>'message'=>'ok', 'user'=> User Object, statusCode 200 if it's all ok</li>
     *          <li>'services'=>Servizi dei ristoranti</li>
     *          <li>'tipiLocali'=>Tipologie di locali, utile per sostituire il "tipoLocale" dei ristoranti</li>
     *          <li>'homeCities'=>Città in home page</li>
     *          <li>'services'=>Servizi</li>
     *          <li>'types'=>Tipi di cucina</li>
     *          <li>'regions'=>Regioni cucina</li>
     *          <li>'cards'=>Carte di credito</li>
     *          <li>'allergeni'=>Array di allergeni</li>
     *          <li>'pacchetti'=>Pacchetti di abbonamento, attualmente Fabio ha voluto disabilitare il processamento automatico quindi sono inutili</li>
     *          <li>'staticDomain'=>Dominio in cui vengono salvati i file statici, utile per le directory delle immagini
     *              <ul>
     *                  <li>Immagini ristoranti: {staticDomain}/restaurant/{big|medium|small|square}/{id_immagine}.png</li>
     *                  <li>Immagini utenti:     {staticDomain}/users/{big|medium|small|square}/{id_immagine}.png</li>
     *                  <li>Immagini piatti:     {staticDomain}/menu/{big|medium|small|square}/{id_immagine}.png</li>
     *              </ul>
     *          </li>
     *          <li>'siteDomain'=>Dominio del sito web</li>
     *          <li>'delivery_price'=>prezzo della spedizione per ordini con delivery</li>
     *          <li>'minimum_order'=>prezzo minimo degli ordini, in frontend se l'ordine è inferiore viene inserito questo</li>
     *          <li>'braintree'=> variabili da utilizzare con le API di Braintree</li>
     *          <li>'paypal'=> variabili da utilizzare con le API di Braintree: ['client'=>client, 'secret'=>secret, 'mode'=>sandbox|live]</li>
     *          <li>'facebook'=> variabili da utilizzare con le API di Facebook: ['app_key'=>app_key, 'app_secret'=>app_secret, 'app_id'=>app_id]</li>
     *      </ul>
     * </p>
     *
     * @return Illuminate\Http\Response $response
    */
    public function getWebsiteInfo(Request $request)
    {
        $response = [
            'message'=>'ok',
            'tipiLocali'=>TipoLocale::orderBy('name', 'asc')->get(['name']),
            'homeCities'=>HomeCity::where('website',0)->orWhere('website',2)->orderBy('order', 'asc')->get(['name','image','lat','lng']),
            'pacchetti'=>Pacchetti::all(),
            'services'=>Services::orderBy('name', 'asc')->get(),
            'types'=>Types::orderBy('name', 'asc')->get(),
            'regions'=>Regions::orderBy('name', 'asc')->get(),
            'cards'=>CreditCard::orderBy('name', 'asc')->get(),
            'allergeni'=>['glutine','crostacei','uova','pesce','arachidi','soia','latte','sedano','senape','anidride solforosa','sesamo','lupini','frutta a guscio','molluschi'],
            'staticDomain'=>env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN'),
            'siteDomain'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN'),
            'delivery_price'=>env('CONSEGNA_PREZZO'),
            'minimum_order'=>env('ORDINE_MINIMO'),
            'braintree'=>[
                'merchant_id'=>env('BRAINTREE_M_ID'),
                'public_key'=>env('BRAINTREE_PUB_K'),
                'private_key'=>env('BRAINTREE_PRI_K')
            ],
            'paypal'=>[
                'client'=>env('PAYPAL_CLIENT'),
                'secret'=>env('PAYPAL_SECRET'),
                'mode'=>env('PAYPAL_MODE')
            ],
            'facebook'=>[
                'app_key'=>env('FB_KEY'),
                'app_secret'=>env('FB_SECRET'),
                'app_id'=>env('FB_ID')
            ]
        ];
        return response()->json($response, 200);

    }


    /**
     * Login to application using email and password
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/mailLogin</p>
     *
     * <p>
     * <pre>
     * Example response
     * {
     *  "message": "ok",
     *  "user": {
     *    "_id": "",
     *    "name": "Fabio",
     *    "email": "fabio.rizzo2009@live.it",
     *    "role": 1,
     *    "updated_at": "2016-04-12 14:57:42",
     *    "created_at": "2016-02-01 17:26:10",
     *    "surname": "Rizzo",
     *    "phone": "3333333333",
     *    "avatar": "57021485bffebc60518b4567",
     *    "verified": 1,
     *    "verification_code": "RmFiaW9SaXp6bzJqaTBNUEs1THBuSTdCZnpnQTlPY0h5TjZBZGJ1YjMxRzA3SHdPT2k=",
     *    "followedby": [],
     *    "following": [],
     *    "location": [
     *      "2A",
     *      "Via Paolo Borsellino",
     *     "Bracciano",
     *      "Città Metropolitana di Roma",
     *     "Lazio",
     *      "Italy",
     *      "00062"
     *    ],
     *    "address": "via paolo borsellino 2c",
     *    "loc": {
     *      "type": "Point",
     *      "coordinates": [
     *       12.1617262,
     *        42.0984325
     *      ]
     *    },
     *    "valid_address": 1,
     *    "born": "1986-03-01 14:49:22",
     *    "description": "",
     *    "favourite": [
     *      "56c2e130bffebc28088b4678"
     *    ],
     *    "wishlist": [],
     *    "social_provider": "facebook",
     *    "social_provider_id": "10206527667900574",
     *    "city": "Bracciano",
     *    "state": "RM",
     *    "zip": "00062",
     *    "customer_id": "13990118",
     *    "payment_plan": "Silver",
     *    "active": 1,
     *    "payment_period": 0,
     *    "transaction": [
     *         ... hidden
     *    ]
     *  }
     *}</pre>
     * </p>
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>email => email|required</li>
     *          <li>pwd => required|string|min:6|max:50</li>
     *          <li> so => required|string</li>
     *          <li> device_token => required|string</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li>'message'=>'ok', 'user'=> User Object, statusCode 200 if it's all ok</li>
     *          <li>'message'=>'nologinmatch' statusCode 403</li>
     *          <li>json statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response $response
     */
    public function accessWithEmail(Request $request)
    {
        //
        $this->validate($request,[
            'email'=>'email|required',
            'pwd'=>'required|string|min:6|max:50',
            'so'=>'required|string',
            'device_token'=>'required|string'
            ]);

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('pwd') ])) :
            $user = User::where('email', $request->input('email'))->firstOrFail();
            return response()->json(['message'=>'ok','user'=>$user], 200);

            $user->device_token = $request->input('device_token');
            $user->so = $request->input('so');
            $user->save();

        else :
            return response()->json(['message'=>'nologinmatch'], 403);
        endif;
    }

    /**
     * Login to application using social network
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/socialLogin</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li> network => required|string only ['facebook','twitter','linkedin']</li>
     *          <li> network_id => required|string</li>
     *          <li> so => required|string</li>
     *          <li> device_token => required|string</li>
     *      </ul>
     * </p>
     *
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'user'=> User Object, statusCode 200 if it's all ok</li>
     *          <li> 'message'=>'no_valid_network' statusCode 403 (not facebook, linkedin or twitter)</li>
     *          <li> 'message'=>'no_user_exists' statusCode 403 (user doesn't exists)</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response $response
     */
    public function accessWithSocial(Request $request)
    {
        //
        $this->validate($request,[
            'network'=>'required|string',
            'network_id'=>'required|string',
            'so'=>'required|string',
            'device_token'=>'required|string'
            ]);
        if(!in_array($request->input('network'), $this->networks)) return response()->json(['message'=>'no_valid_network'], 403);

        $user = User::where('social_provider',$request->input('network'))->where('social_provider_id',$request->input('network_id'))->get();
        if(!$user || count($user) == 0) return response()->json(['message'=>'no_user_exists'], 403);

        $u = User::find($user[0]->_id);
        if($u) :
            $u->device_token = $request->input('device_token');
            $u->so = $request->input('so');
            $u->save();
        endif;

        return response()->json(['message'=>'ok','user'=>$user[0]], 200);
    }

    /**
     * Get user info
     *
     * <p>Get user profile</p>
     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/getUser</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *      <li> user => required|alpha_num,</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'user'=> User Object, statusCode 200 if it's all ok</li>
     *          <li> 'message'=>'no_user' statusCode 404 user not found</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * <p><b>Example Response</b>
     * <pre>
     * {
     *     "message": "ok",
     *     "user": {
     *       "_id": "56af9532bffebc59078b4567",
     *       "name": "Fabio",
     *       "email": "fabio.rizzo2009@live.it",
     *       "role": 9,
     *       "updated_at": "2016-05-10 07:02:49",
     *       "created_at": "2016-02-01 17:26:10",
     *       "surname": "Rizzo",
     *       "phone": "3472706680",
     *       "avatar": "57021485bffebc60518b4567",
     *       "verified": 1,
     *       "verification_code": "RmFiaW9SaXp6bzJqaTBNUEs1THBuSTdCZnpnQTlPY0h5TjZBZGJ1YjMxRzA3SHdPT2k=",
     *       "followedby": [],
     *       "following": [],
     *       "location": [
     *         "2A",
     *         "Via Paolo Borsellino",
     *         "Bracciano",
     *         "Città Metropolitana di Roma",
     *         "Lazio",
     *         "Italy",
     *         "00062"
     *       ],
     *       "address": "via paolo borsellino 2c",
     *       "loc": {
     *        "type": "Point",
     *         "coordinates": [
     *           12.1617262,
     *           42.0984325
     *         ]
     *       },
     *       "valid_address": 1,
     *       "born": "1986-03-01 09:10:20",
     *       "description": "Sono un appassionato di cibo.\nMi piace molto mangiare nei migliori ristoranti di Roma.",
     *       "favourite": [
     *         "56c2e130bffebc28088b4678"
     *       ],
     *       "wishlist": [],
     *       "social_provider": "facebook",
     *       "social_provider_id": "10206527667900574",
     *       "city": "Bracciano",
     *       "state": "RM",
     *       "zip": "00062",
     *       "active": 1
     *     }
     *   }
     * </pre>
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response $response
     */

    public function getUser(Request $request){
        $this->validate($request,[
            'user'=>'required|alpha_num'
            ]);

        $user = User::find($request->input('user'));
        if(!$user) return response()->json(['message'=>'no_user'],404);
        if(!$user->allergeni) $user->allergeni = [];

        if(!$user->favourite):
            $user->favourite = [];
        else:
            if(count($user->favourite) > 0):
                $ristos = Restaurant::whereIn('_id',$user->favourite)->get(['id','name','image','tipoLocale','loc','address']);
                $user->favourite_restaurants = $ristos;
            endif;
        endif;


        return response()->json(['message'=>'ok','user'=>$user],200);
    }


    /**
     * set user favourite
     *
     * <p>Add or remove restaurant from user favourites, doesn't matter if you want to add or remove it from favourite</p>
     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/addRemoveFavourite</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *      <li> user => required|alpha_num, (the user id)</li>
     *      <li> restaurant => required|alpha_num (the restaurant id)</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'user'=> User Object, statusCode 200 if it's all ok</li>
     *          <li> 'message'=>'no_user' statusCode 404 user not found</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response $response
     */
    public function addRemoveFavourite(Request $request){
        $this->validate($request, [
            'user'=>'required|alpha_num',
            'restaurant' => 'required|alpha_num'
        ]);

        $user = User::find($request->input('user'));
        if(!$user) return response()->json(['message'=>'no_user'], 404);

        if(!$user->favourite) $user->favourite = [];

        $favourites = $user->favourite;
        if(in_array($request->input('restaurant'), $favourites)):
          // remove
          $index = array_search($request->input('restaurant'), $favourites);
          unset($favourites[$index]);
        else:
          // add
          $favourites[] = $request->input('restaurant');
        endif;

        //if(!isset($favourites)) $favourites = [];

        $user->favourite = $favourites;
        $user->save();
        return response()->json(['message'=>'ok','user'=>$user], 200);
    }


    /**
     * User registration
     *
     * <p>if using social network network and network_id are required
     * else is required the pwd
     * return the user object with user id</p>
     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/register</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li> email => required|unique:users_collection|max:255,</li>
     *          <li> name => required,</li>
     *          <li> surname => required,</li>
     *          <li> pwd => min:6|max:50,</li>
     *          <li> phone => numeric,</li>
     *          <li> network =>string,</li>
     *          <li> network_id=>string</li>
     *          <li> so => required|string</li>
     *          <li> device_token => required|string</li>
     *          <li> invited_by => alpha_num</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'user'=> User Object, statusCode 200 if it's all ok</li>
     *          <li> 'message'=>'no_valid_network' statusCode 403 (not facebook, linkedin or twitter)</li>
     *          <li> 'message'=>'no_valid_access_method' statusCode 403 (no network and no pwd)</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response $response
     */
    public function register(Request $request)
    {
        //
        $this->validate($request, [
            'email' => 'required|unique:users_collection|max:255',
            'name' => 'required',
            'surname' => 'required',
            'pwd' => 'min:1|max:50',
            'phone' => 'numeric',
            'network' =>'string',
            'network_id'=>'string',
            'so'=>'required|string',
            'device_token'=>'required|string',
            'invited_by'=>'alpha_num',
            'allergeni'=>'allergeni'
        ]);

        $statusCode = 200;

        if($request->input('network') && !in_array($request->input('network'), $this->networks)) return response()->json(['message'=>'no_valid_network'], 403);
        if( (!$request->input('network') || !$request->input('network_id')) && !$request->input('pwd')) return response()->json(['message'=>'no_valid_access_method'], 403);

        $user = new User();
        $user->name = $request->input('name');//($request->input('surname') != '') ? strip_tags($request->input('name') . ' '.$request->input('surname')) : strip_tags($request->input('name'));
        $user->surname = $request->input('surname');
        $user->email = $request->input('email');

        $user->password = ($request->input('pwd')) ? bcrypt( $request->input('pwd') ) : null;
        $user->social_provider = ($request->input('network')) ? $request->input('network') : null;
        $user->social_provider_id = ($request->input('network_id')) ? $request->input('network_id'): null;
        $user->allergeni = ($request->input('allergeni')) ? $request->input('allergeni') : [];

        $user->phone = ($request->input('phone')) ? $request->input('phone') : null;
        $user->verified = 0;
        $user->verification_code = base64_encode( ( $user->name . $request->input('password') . $request->input('surname') . str_random(30) ) );
        $user->role = 1;

        $user->friends = [];
        $user->friend_request = [];
        $user->following = [];
        $user->followedby = [];
        $user->foodcoin = env('FOODCOIN_REGISTRATION');
        $user->current_address = '';
        $user->address_list = [];
        $user->favourite_list = [];
        $user->wishlist = [];
        $user->billing_plan = [];
        $user->recommended_plates = [];
        $user->recommended_restaurants = [];


        if($request->input('invited_by')) $user->invitedby = $request->input('invited_by');

        Event::fire(new UserRegistered($user));

        $user->device_token = $request->input('device_token');
        $user->so = $request->input('so');

        $user->save();

        $parameters = [$user->_id,$user->name.' '.$user->surname];
        Notification::addNotification(0,$parameters,0);

        return response()->json(['message'=>'ok','user'=>$user], $statusCode);
    }

    /**
     * Complete User Profile, add address and born date
     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/completeProfile</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li> user_id   => required|alpha_num,</li>
     *          <li> address   => required|string,</li>
     *          <li> city      => required|string,</li>
     *          <li> state     => required|string,</li>
     *          <li> zip       => required|string,</li>
     *          <li> born      => required|date_format:Y-m-d</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', statusCode 200 if it's all ok</li>
     *          <li> 'message'=>'no_user_exists' statusCode 403 (user doesn't exists)</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response $response
     */
    public function completeProfile(Request $request){
        $this->validate($request, [
            'user_id'=> 'required|alpha_num',
            'address' => 'required|string',
            'city'=>'required|string',
            'state'=>'required|string',
            'zip'=>'required|string',
            'born' => 'required|date_format:Y-m-d',
            'allergeni' => 'array',
            'name'=>'string',
            'surname'=>'string'
        ]);

        $user = User::find($request->input('user_id'));
        if(!$user):
            return response()->json(['message'=>'no_user_exists'], 403);
        endif;

        if($request->input('allergeni')) $user->allergeni = $request->input('allergeni');

        if(!$user->address || $user->valid_address == 0 || !$user->valid_address || $user->address != $request->input('address')):
            // costruisci indirizzo
            $address_built = $request->input('address') .', '.$request->input('zip').' '.$request->input('city'). ' ('.$request->input('state').')';

            $location = Address::returnLocation($address_built);

            $user->location = $location[2];
            $user->loc = ['type'=>"Point","coordinates"=>[doubleval($location[1]),doubleval($location[0])]];
            $user->address = $request->input('address');
            $user->city = $location[3];
            $user->state = $location[4];
            $user->zip = $location[5];
            $user->valid_address = 1;
        endif;


        $data = Carbon::createFromFormat('Y-m-d', $request->input('born'))->toDateTimeString();
        $user->born = $data;

        if($request->input('name')) $user->name = $request->input('name');
        if($request->input('surname')) $user->surname = $request->input('surname');

        $user->save();

        $statusCode = 200;
        $response = [
          'message' => 'ok'
        ];
        return response()->json($response, $statusCode);
    }


    /**
     * Get restaurants sending lat, lng, action and if you want the range in meters
     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/getRestaurants</p>
     * <p>Test Variables:
     *  <ul>
     *      <li>lat: '45.4516647'</li>
     *      <li>lng: '9.2056877'</li>
     *  </ul>
     * </p>
     *
     * <p><b>Example Response</b></p>
     * <p>
     * <pre>
     * {
     * "message": "ok",
     * "restaurants": [
     *   {
     *     "_id": "56c2e130bffebc28088b4678",
     *     "name": "Casa Tua",
     *     "distance": 57.767094595223,
     *     "tipoLocale": [
     *       "56c2eb97bffebc89088b4568"
     *     ],
     *     "image": "56c46a82bffebcdb088b4619",
     *     "ordina": 1,
     *     "take_away": 1
     *   },...
     * ]
     *}
     * </pre>
     * </p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>lat => required|numeric</li>
     *          <li>lng => required|numeric</li>
     *          <li>skip => integer</li>
     *          <li>limit => integer</li>
     *          <li>action => required|integer => 1 for delivery, 2 for take away </li>
     *          <li>range => integer => if you want a custom range in meters from the user position</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'restaurants' => restaurants Object</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response $response
     */
    public function getRestaurantByLatLng(Request $request){

        $this->validate($request, [
            'lat'=>'required|numeric',
            'lng'=>'required|numeric',
            'action'=>'integer',
            'skip'=>'integer',
            'limit'=>'integer',
            'range'=>'integer'
        ]);

        $query = [ 'approved'=>1 ];
        /*switch($request->input('action')):
            case 1:
            $query = [ 'approved'=>1 ];
            break;
            case 2:
            // take away, prendi ristoranti con quel cap
            $query = [ 'take_away'=>1 ];
            break;
        endswitch;*/

        $range = ($request->input('range')) ? intval($request->input('range')) : 4000;

        $mongodb = DB::getMongoDB();
        $r = $mongodb->command(
        array(  'geoNear' => "restaurant_collection", // Put here the collection
            'near' => array( //
                'type' => "Point",
                'coordinates' => array(doubleval($request->input('lng')), doubleval($request->input('lat')))
            ),
            'spherical' => true,
            'num'=>5000,
            'maxDistance' => $range,
            'query' => $query
        ));

        //$results = json_decode($r['results'],true);
        $return_results = [];
        $skip = 0;
        $limit = 0;
        foreach ($r['results'] as $result) {
            $id = (array) $result['obj']['_id'];
            $obj = [
                '_id'=>$id['$id'],
                'name'=>$result['obj']['name'],
                'distance'=>$result['dis'],
                'address'=>$result['obj']['address'],
                'price'=>$result['obj']['price'],
                'tipoLocale'=>$result['obj']['tipoLocale'],
                'loc'=>$result['obj']['loc'],
                'image'=>(isset($result['obj']['image']) && $result['obj']['image'] != '') ? $result['obj']['image']: ''
            ];
            // paginazione
            $add = true;
            if($request->input('skip')):
                $skip++;
                if($skip <= $request->input('skip')):
                    $add = false;
                endif;
            endif;
            if($request->input('limit') && $add):
                if($limit >= $request->input('limit')):
                    $add = false;
                endif;
                $limit++;
            endif;

            if($add):
                $return_results[] = $obj;
            endif;
        }
        return response()->json(['message'=>'ok','restaurants'=>$return_results], 200);   //$r['results']
    }

    /**
     * Get restaurants sending city
     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/getRestaurantsByCity</p>
     * <p>Test Variables:
     *  <ul>
     *      <li>city: 'Roma'</li>
     *      <li>skip: 0</li>
     *      <li>limit: 10</li>
     *  </ul>
     * </p>
     *
     * <p><b>Example Response</b></p>
     * <p>
     * <pre>
     * {
     * "message": "ok",
     * "restaurants": [
     *   {
     *     "_id": "56c2e130bffebc28088b4678",
     *     "name": "Casa Tua",
     *     "tipoLocale": [
     *       "56c2eb97bffebc89088b4568"
     *     ],
     *     "image": "56c46a82bffebcdb088b4619",
     *     "address": "via Lodovico Muratori..."
     *   },...
     * ]
     *}
     * </pre>
     * </p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>city => required|string</li>
     *          <li>skip => integer</li>
     *          <li>limit => integer</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'restaurants' => restaurants Object</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response $response
     */
    public function getRestaurantByCity(Request $request){
        $this->validate($request, [
            'city'=>'required|string',
            'skip'=>'integer',
            'limit'=>'integer'
        ]);
        $query = Restaurant::where('city',$request->input('city'))->where('approved',1)->orderBy('rating','desc');

        if($request->input('skip')) $query->skip($request->input('skip'));
        if($request->input('limit')) $query->take($request->input('limit'));

        return response()->json(['message'=>'ok','type'=>'simple','restaurants'=>$query->get(['id','name','image','tipoLocale','loc','address'])], 200);//$restaurants
    }


    /**
     * Get restaurants for advanced search
     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/getRestaurantsByAdvancedSearch</p>
     * <p>Test Variables:
     *  <ul>
     *      <li></li>
     *  </ul>
     * </p>
     *
     * <p><b>Example Response</b></p>
     * <p>
     * <pre>
     * {
     * "message": "ok",
     * "restaurants": [
     *   {
     *     "_id": "56c2e130bffebc28088b4678",
     *     "name": "Casa Tua",
     *     "tipoLocale": [
     *       "56c2eb97bffebc89088b4568"
     *     ],
     *     "image": "56c46a82bffebcdb088b4619",
     *     "address": "via Lodovico Muratori..."
     *   },...
     * ]
     *}
     * </pre>
     * </p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *       <li>'name'     => 'string', // valid for name and address
     *       <li>'chef'     => 'string',</li>
     *       <li>'type'     => 'string',</li>
     *       <li>'location' => 'string',</li>
     *       <li>'maxPrice' => 'integer',</li>
     *       <li>'services' => 'array',</li>
     *       <li>'types' => 'array',</li>
     *       <li>'region' => 'array',</li>
     *       <li>'regions' => 'array',</li>
     *       <li>'recensioni' => 'integer',</li>
     *       <li>'virtualTour' => 'boolean',</li>
     *       <li>'prenota'=>'integer'</li> </li>
     *       <li>'menu' => 'boolean',</li>
     *       <li>'skip'=>'integer'</li> </li>
     *       <li>'limit'=>'integer'</li> </li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'restaurants' => restaurants Object</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response $response
     */
    public function searchByValues(Request $request)
    {

        //Log::info('api request: '.json_encode($request->all()));


        // others: social account, virtual tour, recensioni, order|prenotation
        $this->validate($request, [
            'name'     => 'string', // valid for name and address
            'chef'     => 'string',
            'type'     => 'string',
            'location' => 'string',
            'maxPrice' => 'integer',
            'services' => 'string',
            'types' => 'string',
            'region' => 'string',
            'regions' => 'string',
            'recensioni' => 'integer',
            'virtualTour' => 'boolean',
            'prenota'=>'integer',
            'menu'=>'boolean',
            'skip'=>'integer',
            'limit'=>'integer'
        ]);

        //Log::info('validation passed');
        $this->restaurants = DB::collection('restaurant_collection');
        $this->restaurants->where('approved',1);


        if($request->input('name')  && $request->input('name') != '') :
            // search in name and address
            $name = $request->input('name');
            $this->restaurants->where('name','like','%'.$name."%");
            //Log::info('cerco nome '.$name);
        endif;

        if($request->input('location') && $request->input('location') != '') :
            // search in name and address
            $city = $request->input('location');
            $this->restaurants->where('city','like','%'.$city."%");
            //Log::info('cerco citta');
        endif;

        if($request->input('type') && $request->input('type') != '') :
            //Log::info('type '.$request->input('type'));
            $this->restaurants->whereIn('tipoLocale',[$request->input( 'type' )]);
            //Log::info('cerco tipo locale');
        endif;

        if($request->input( 'services' ) && $request->input( 'services' ) != '') :
            $q = explode(",", $request->input( 'services' ));
            if(count($q) > 0) $this->restaurants->where('services','all',$q);
            //Log::info('cerco servizio');
        endif;

        if($request->input( 'types' ) && $request->input( 'types' ) != '') :
            $q = explode(",", $request->input( 'types' ));
            if(count($q) > 0) $this->restaurants->where('tipoCucina','all',$q);
            //Log::info('cerco tipo');
        endif;

        if($request->input( 'region' ) && $request->input( 'region' ) != '') :
            $q = explode(",", $request->input( 'region' ));
            if(count($q) > 0) $this->restaurants->where('regioneCucina','all',$q);
            //Log::info('cerco regione');
        endif;

        if($request->input( 'regions' ) && $request->input( 'regions' ) != '') :
            $q = explode(",", $request->input( 'regions' ));
            if(count($q) > 0) $this->restaurants->where('regioneCucina','all',$q);
            //Log::info('cerco regione');
        endif;

        if($request->input( 'maxPrice' ) && $request->input('maxPrice') > 0) :
            //$this->restaurants->where('priceMax','<',$request->input( 'maxPrice' ));
            $this->restaurants->where('price','<',intval($request->input( 'maxPrice' )));
            //Log::info('cerco prezzo massimo '.$request->input( 'maxPrice' ));
        endif;

        if($request->input( 'chef' ) && $request->input( 'chef' ) != '') :
            $chef_name = $request->input( 'chef' );
            $this->restaurants->where('staff.chef','like','%'.$chef_name."%");
            //Log::info('cerco chef');
        endif;


        if($request->input( 'virtualTour' )) :
            $this->restaurants->where('virtualTour','!=',"");
            $this->restaurants->whereNotNull('virtualTour');
            //Log::info('cerco virtual tour');
        endif;

        if($request->input( 'recensioni' ) == 1) :
            $this->restaurants->whereNotNull('recensioni');
            //Log::info('cerco recensioni');
        endif;

        if($request->input( 'prenota' ) && $request->input( 'prenota' ) == 1) :
            $this->restaurants->where('prenotazione',1);
        endif;

        if($request->input('menu')) :
            $this->restaurants->where('menu.menus','exists', true);
        endif;


        if($request->input('skip') && $request->input('skip') > 0) $this->restaurants->skip($request->input('skip'));
        if($request->input('limit') && $request->input('limit') > 0) $this->restaurants->take($request->input('limit'));

        $results = $this->restaurants->get(['id','name','image','slug','tipoLocale','loc','address']);

        //Log::info('risultati: '.json_encode($results));

        return response()->json(['message'=>'ok','restaurants'=>$results], 200);

    }


    /**
     * Get single restaurant with id
     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/getSingleRestaurant</p>
     * <p><b>Test Variables: </b>
     *  <ul>
     *      <li>id: '56c2e130bffebc28088b4678'</li>
     *  </ul>
     * </p>
     *
     * <p><b>Example Response</b></p>
     * <p>
     * <pre>
     * {
     * "message": "ok",
     * "restaurant": {
     *    "_id": "56c2e130bffebc28088b4678",
     *    "ID_EXPORT": 13084,
     *    "name": "Casa Tua",
     *    "description": "<p>A Casa Tua è un'osteria aperta dal 2007 che porta in tavola i piatti tipici della Toscana. Questo angolo di gusto nel cuore di Porta Romana a Milano unisce ai piatti della tradizione, preparati con ingredienti genuini e selezionati, l'amore per questa regione. Due elementi imprescindibili per scoprire i sapori antichi, le ricette tradizionali delle province toscane, in un'atmosfera calda e accogliente.<br/></p>",
     *    "address": "Via Ludovico Muratori 10",
     *    "loc": {
     *    "type": "Point",
     *    "coordinates": [
     *        9.2050804,
     *        45.451961
     *      ]
     *   },
     *    "contact": {
     *    "web": "http://www.casatuaosteria.com",
     *    "email": "prenota@casatuaosteria.com",
     *    "phone": [
     *    "025514269"
     *      ]
     *    },
     *    "virtualTour": "",
     *    "streetView": [
     *    "45.451979",
     *    "9.204829000000018",
     *    "0",
     *    "0",
     *    "0"
     *    ],
     *    "location": [
     *    "10",
     *    "Via Lodovico Muratori",
     *    "Milano",
     *    "Milan",
     *    "Città Metropolitana di Milano",
     *    "Lombardia",
     *    "Italy",
     *    "20135"
     *    ],
     *    "price": 30,
     *    "priceMin": "25",
     *    "priceMax": "35",
     *    "services": [
     *    "56c2f6f2bffebccc088b4569",
     *    "56c2f6f2bffebccc088b456c"
     *    ],
     *    "tipoLocale": [
     *    "56c2eb97bffebc89088b4568"
     *    ],
     *    "tipoCucina": [
     *    "56c2f7fabffebcd5088b4569",
     *    "56c2f7fabffebcd5088b4568",
     *    "56c2f7fabffebcd5088b4567"
     *    ],
     *    "regioneCucina": [
     *    "56c2f5c2bffebcad088b4567",
     *    "56c2f5c2bffebcad088b4570"
     *    ],
     *    "social": [
     *      {
     *    "facebook": "https://www.facebook.com/osteriacasatua"
     *      },
     *      {
     *    "twitter": "https://twitter.com/OsteriaCasaTua"
     *      },
     *      {
     *    "pinterest": "http://www.pinterest.com/osteriacasatua/"
     *      },
     *      {
     *    "instagram": null
     *      },
     *      {
     *    "googlePlus": ""
     *      },
     *      {
     *    "linkedin": ""
     *      },
     *      {
     *    "youtube": ""
     *      },
     *      {
     *    "tripadvisor": "http://www.tripadvisor.it/Restaurant_Review-g187849-d1860767-Reviews-Osteria_Casa_Tua-Milan_Lombardy.html"
     *      }
     *    ],
     *    "staff": {
     *    "chef": null,
     *    "sommelier": null,
     *    "barman": null
     *    },
     *
     *      // -- Questi orari sono i vecchi di mangiaebevi, DA NON CONSIDERARE
     *    "orari": [
     *      [
     *    "12:00-15:00 19:00-23:30"
     *      ],
     *      [
     *    "12:00-15:00 19:00-23:30"
     *      ],
     *      [
     *    "12:00-15:00 19:00-23:30"
     *      ],
     *      [
     *    "12:00-15:00 19:00-23:30"
     *      ],
     *      [
     *    "12:00-15:00 19:00-23:30"
     *      ],
     *      [
     *    "12:00-15:00 19:00-23:30"
     *      ],
     *      [
     *    "12:00-15:00 19:00-23:30"
     *      ]
     *    ],
     *    "chiusura": "",
     *    "note_orari": "",
     *    "image": "56c46a82bffebcdb088b4619",
     *    "video": null,
     *    "images": [
     *    "56c46a78bffebcdb088b4614",
     *    "56c46a7bbffebcdb088b4615",
     *    "56c46a7dbffebcdb088b4616",
     *    "56c46a7fbffebcdb088b4617",
     *    "56c46a80bffebcdb088b4618",
     *    "56c46a82bffebcdb088b4619",
     *    "56c47b64bffebc3d098b45a3"
     *    ],
     *    "menu": {
     *
     *      // I MENU SONO IN ARRAY, OGNI MENU HA UNA SERIE DI FASCE ORARIE IN CUI DISPONIBILE
     *    "menus": [
     *        {
     *    "name": "Menù cena",
     *    "fasce": [
     *            [
     *    "12:30-15:30",  // Lunedì -> indice 0
     *    "18:30-00:00"
     *            ],
     *           [
     *    "12:30-15:30", // Martedì -> indice 1
     *    "18:30-00:00"
     *            ],
     *            [
     *    "12:30-15:30", // Mercoledì -> indice 2
     *    "18:30-00:00"
     *            ],
     *            [
     *    "12:30-15:30", // Giovedì -> indice 3
     *    "18:30-00:00"
     *            ],
     *            [
     *    "12:30-15:30", // Venerdì -> indice 4
     *    "18:30-00:00"
     *            ],
     *            [
     *    "12:30-15:30", // Sabato -> indice 5
     *    "18:30-00:00"
     *            ],
     *            [
     *    "12:30-15:30", // Domenica -> indice 6
     *    "18:30-00:00"
     *            ]
     *          ],
     *    "categorie": [ // OGNI MENU HA UNA LISTA DI CATEGORIE, CIASCUNA DELLE QUALI HA UNA LISTA DI PIATTI
     *            {
     *    "name": "Antipasti",
     *    "piatti": [ // LISTA DEI PIATTI NELLA CATEGORIA
     *                {
     *    "name": "La panzanella Maremmana",
     *    "image": "56f65c07bffebca3078b4567",
     *    "description": "",
     *    "ingredienti": [
     *    "pane",
     *    "pomodoro",
     *    "prezzemolo",
     *    "cipolla",
     *    "basilico",
     *    "aceto",
     *    "olio extravergine d'oliva"
     *                  ],
     *    "custom_info": [],
     *    "price": "6.5",
     *    "options": [
     *
     *      // OPZIONI DEL PIATTO !!!IMPORTANTE, LE OPZIONI SONO CATEGORIZZATE, OGNI CATEGORIA DI OPZIONI PUò ESSERE NORMALE O ESCLUDENTE,
     *      QUANDO ESCLUDENTE NON DEVONO ESSERE SELEZIONATE PIù OPZIONI NELLA CATEGORIA DI RIFERIMENTO, MA SOLO UNA
     *                    {
     *    "name": "Prima",
     *    "type": 0,
     *    "options": [
     *                        {
     *    "no_variation": 1,
     *    "category": "Prima",
     *    "name": "Prova 1",
     *    "variation": null,
     *    "price": null
     *                        },
     *                        {
     *    "no_variation": 1,
     *    "category": "Prima",
     *    "name": "Prova 2",
     *    "variation": null,
     *    "price": null
     *                        }
     *                      ]
     *                    },
     *                    {
     *    "name": "Seconda",
     *    "type": 0,
     *    "options": [
     *                        {
     *    "no_variation": 1,
     *    "category": "Seconda",
     *    "name": "Prova 3",
     *    "variation": null,
     *    "price": null
     *                        },
     *                        {
     *    "no_variation": 0,
     *    "category": "Seconda",
     *    "name": "Prova 4",
     *    "variation": "0",
     *    "price": "2"
     *                        }
     *                      ]
     *                    },
     *                    {
     *    "name": "Escludenti",
     *    "type": "1",
     *    "options": [
     *                        {
     *    "no_variation": 1,
     *    "category": "Escludenti",
     *    "name": "Escludente 1",
     *    "variation": null,
     *    "price": null
     *                        },
     *                        {
     *    "no_variation": 1,
     *    "category": "Escludenti",
     *    "name": "Escludente 2",
     *    "variation": null,
     *    "price": null
     *                        }
     *                      ]
     *                    }
     *                  ],
     *    "id": "56fe26ecbffebc4a088b4567",
     *
     *                },
     *                {
     *    "name": "Millefoglie alla parmigiana",
     *    "image": "56f65c6ebffebca8078b456a",
     *    "description": "",
     *    "ingredienti": [
     *    "melanzane",
     *    "mozzarella",
     *    "pomodoro",
     *    "basilico"
     *                  ],
     *    "custom_info": [],
     *    "price": "8",
     *    "id": "56fe26ecbffebc4a088b4568",
     *
     *                },
     *
     *              ]
     *            }
     *          ]
     *        },
     *        {
     *    "name": "Menu prova",
     *    "fasce": [
     *            [
     *    "12:30-15:30"
     *            ],
     *            [
     *    "12:30-15:30"
     *            ],
     *            [
     *    "12:30-15:30"
     *            ],
     *            [
     *    "12:30-15:30"
     *            ],
     *            [
     *    "12:30-15:30"
     *            ],
     *            [
     *    "12:30-15:30"
     *            ],
     *            [
     *    "12:30-15:30"
     *            ]
     *          ],
     *    "categorie": []
     *        }
     *      ]
     *    },
     *    "vini": [],
     *    "carte": [
     *    "56c2f99dbffebcdc088b456b",
     *    "56c2f99dbffebcdc088b4569",
     *    "56c2f99dbffebcdc088b456a"
     *    ],
     *    "prenotazione": 0,
     *    "ordina": 1,
     *    "updated_at": "2016-04-03 07:49:49",
     *    "created_at": "2016-02-16 08:43:28",
     *    "images_imported": 1,
     *    "slug": "a-casa-tua",
     *    "fasce": [
     *      [
     *    "12:00-15:00",
     *    "18:00-24:00"
     *      ],
     *      [
     *    "12:00-15:00",
     *    "18:00-24:00"
     *      ],
     *      [
     *    "12:00-15:00",
     *    "18:00-24:00"
     *      ],
     *      [
     *    "12:00-15:00",
     *    "18:00-24:00"
     *      ],
     *      [
     *    "12:00-15:00",
     *    "18:00-24:00"
     *      ],
     *      [
     *    "12:00-15:00",
     *    "18:00-24:00"
     *      ],
     *      [
     *    "12:00-15:00",
     *    "18:00-24:00"
     *      ]
     *    ],
     *    "valid_address": 1,
     *    "district": [
     *    "C.so Lodi Via Salmini M3"
     *    ],
     *    "valid_district": 1,
     *    "city": "Milano",
     *    "state": "MI",
     *    "zip": "20135",
     *    "range": 2000,
     *    "delivery_zip": [
     *    "00135",
     *    "00062"
     *    ],
     *    "take_away": 1,
     *    "claimed_by": null,
     *    "user": "56af9532bffebc59078b4567"
     *  }
     *}
     * </pre>
     * </p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>id => required|alpha_num</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'restaurant' => restaurant Object, status 200</li>
     *          <li> 'message'=>'no_restaurant_exists' statusCode 403 </li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response $response
     */
    public function getSingleRestaurant(Request $request){
        $this->validate($request, [
            'id'=>'required|alpha_num'
        ]);

        $restaurant = Restaurant::find($request->input('id'));
        if(!$restaurant) return response()->json(['message'=>'no_restaurant_exists'],403);

        return response()->json(['message'=>'ok','restaurant'=>$restaurant],200);
    }


    /**
     * Reserve at restaurant
     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/reserve</p>
     *
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *       <li>'restaurant_id'  => 'required|alpha_num',</li>
     *       <li>'user_id'  => 'required|alpha_num',</li>
     *       <li>'date' => 'required|date_format:YYYY-MM-DD',</li>
     *       <li>'time' => 'required|string',(HH:mm)</li>
     *       <li>'email' => 'required|email',</li>
     *       <li>'people'=>'required|integer', </li>
     *       <li>'name'=>'required|string',</li>
     *       <li>'tel'=>'required|numeric'</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'restaurants' => restaurants Object</li>
     *          <li> statusCode 404, message = 'nouser' user doesn't exists</li>
     *          <li> statusCode 404, message = 'norestaurant' restaurant doesn't exists</li>
     *          <li> statusCode 403, message = 'noreservationavaible' reservation not avaible</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response $response
     */
    public function reserve(Request $request)
    {
        //
         $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'user_id'  => 'required|alpha_num',
            'date' => 'required|date_format:Y-m-d',
            'time' => 'required|string',
            'email' => 'required|email',
            'people'=>'required|integer',
            'name'=>'required|string',
            'tel'=>'required|numeric',
            'can_preorder' => 'required|integer|min:0|max:1'
        ]);
        $statusCode = 200;

        $risto = Restaurant::find($request->input('restaurant_id'));
        if(!$risto) return response()->json(['message'=>'norestaurant'], 404);

        $user = User::find($request->input('user_id'));
        if(!$user) return response()->json(['message'=>'nouser'], 404);

        if(($risto->prenotazione == 0 || !$risto->prenotazione) && (!$risto->contact['email'][0] || $risto->contact['email'][0] == '')) return response()->json(['message'=>'noreservationavaible'], 403);




        $reservation = new ReservationRequest();
        $reservation->restaurant = $risto->_id;
        $reservation->restaurant_name = $risto->name;
        $reservation->date = $request->input('date');//Carbon::createFromFormat('Y-m-d', $request->input('date'))->toDateString();
        $reservation->fascia_oraria = $request->input('time');
        //$reservation->time = $request->input('time');//Carbon::createFromFormat('Y-m-d', $request->input('time'))->toTimeString();
        $reservation->email = $request->input('email');
        $reservation->people = $request->input('people');
        $reservation->name = $request->input('name');
        $reservation->tel = $request->input('tel');
        $reservation->user = $request->input('user_id');
        $reservation->unique_string = str_random(40); // aggiungi stringa per validazione
        $reservation->type = 'web';
        $reservation->confirmed = 0;
        $reservation->can_preorder = $request->input('can_preorder');


        RestaurantClients::addClientToRestaurant($user->_id,$risto->_id);
        /**
        * Notifica proprietario ristorante, se non cliente invia mail per farlo registrare
        * 9 => nuova prenotazione                 [id utente, nome utente, id ristorante, nome ristorante, data, persone]
        **/
        $parameters = [
        $user->_id,$user->name.' '.$user->surname,$risto->_id,$risto->name,$request->input('date')." ".$request->input('time'), $reservation->people];
        Notification::addNotification(9,$parameters,0);
        if($risto->user && $risto->user != 0):
            Notification::addNotification(9,$parameters,$risto->user);
        endif;
            // il ristorante non è amministrato, invia una mail

        if($risto->contact['email']) :
            $contact_mail = (env('APP_ENV') == 'local') ? env('ADMIN_EMAIL') : $risto->contact['email'];

            $mail_data = [
            'ristoratore'=>$contact_mail,
            'name'=>$risto->name,
            'admin_mail'=>env('ADMIN_EMAIL'),
            'subject'=>'Nuova prenotazione',
            'restaurant'=>$risto->name,
            'detail'=>[$reservation->name,
                        $reservation->people,
                        $request->input('date')." ".$request->input('time'),
                        $request->input('tel')
                        ]
            ];

            Mail::send('email.reservation', ['mail_data' => $mail_data],
                function ($m) use ($mail_data) {
                $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
                $m->to($mail_data['ristoratore'], $mail_data['name'])->subject($mail_data['subject']);
            });

            Mail::send('email.reservation', ['mail_data' => $mail_data],
                function ($m) use ($mail_data) {
                $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
                $m->to($mail_data['admin_mail'], $mail_data['name'])->subject($mail_data['subject']);
            });
        endif;

        $reservation->save();

        return response()->json(['message'=>'ok','reservation'=>$reservation], $statusCode);
    }

    /**
     * Annulla prenotazione
     * @param Request $request [description]
     */
    public function deleteReservation(Request $request)
    {
        //
         $this->validate($request, [
            'reservation'  => 'required|alpha_num',
            'user_id'   =>  'required|alpha_num'
        ]);

        $r = ReservationRequest::find($request->input('reservation'));
        if(!$r) return response()->json(['message'=>'error.empty'], 404);

        $risto = Restaurant::find($r->restaurant);
        if(!$risto) return response()->json(['message'=>'error.empty'], 404);

        if($request->input('user_id') != $r->user && $request->input('user_id') != $risto->user) return response()->json(['message'=>'error.invalid_user'], 403);

        $u = User::find($r->user);
        if(!$u) return response()->json(['message'=>'error.empty'], 404);

        if($u && $u->email) $to_send[] = $u->email;
        if($r->email && $r->email != '') $to_send[] = $r->email;

        if(isset($risto->contact['email']) && env('APP_ENV') != 'local') $to_send[] = $risto->contact['email'];

        $to_send[] = env('ADMIN_EMAIL');

        $r->confirmed = 4;
        $r->save();

        foreach ($to_send as $mail_address) :

            $mail_data = [
            'to'=>$mail_address,
            'name'=>$risto->name,
            'subject'=>'Prenotazione annullata',
            'restaurant'=>$risto->name,
            'detail'=>[
                $r->name,
                $r->date->format('d/m/Y')." ".$r->fascia_oraria,
                $r->tel,
                $r->people
                ]
            ];

            Mail::send('email.reservationcancelled', ['mail_data' => $mail_data],
                function ($m) use ($mail_data) {
                $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
                $m->to($mail_data['to'], $mail_data['name'])->subject($mail_data['subject']);
            });
        endforeach;



        return response()->json(['message'=>'ok'], 200);
    }

    /**
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/get_user_reservations</p>
     *
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>user_id:      alpha_num|required</li>
     *          <li>skip:          integer</li>
     *          <li>limit:        integer</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'reservations' => reservationsRequest Object</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */

    public function getUserReservation(Request $request)
    {
        $this->validate($request, [
            'user_id'  => 'required|alpha_num',
            'skip'=>'integer',
            'limit'=>'integer'
        ]);
        $q = ReservationRequest::where('user',$request->input('user_id'))->orderBy('date','desc');

        if($request->input('limit')) $q->take($request->input('limit'));
        if($request->input('skip')) $q->skip($request->input('skip'));

        $reservations = $q->get();
        return response()->json(['message'=>'ok','reservations'=>$reservations], 200);
    }


    /**
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/getSingleRestaurantReservationAvaibleTime</p>
     *
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>restaurant_id: alpha_num|required</li>
     *          <li>day:           date format: YYYY-MM-DD|required</li>
     *          <li>people:        optional | integer</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'fasce' => fasce orarie Array</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function getRestaurantAvaibleFasce(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'day'  => 'required|date_format:Y-m-d',
            'people' => 'integer'
        ]);

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        if(!$restaurant) return response()->json(['message'=>'error.empty'], 200);

        $time_shift = ($restaurant->time_shift && $restaurant->time_shift != '') ? intval($restaurant->time_shift): 15;


        $day_fasce = [];
        for($hour = 0; $hour < 24; $hour++):
            $current_hour = ($hour < 10) ? "0".$hour : "".$hour;
            for($x = 0;$x < 60; $x+=$time_shift):
                $current_minute = ($x == 0) ? "00" : $x;
                $day_fasce[] = $current_hour.":".$current_minute;
            endfor;
        endfor;


        $dt = Carbon::parse($request->input('day'));


        $from = Carbon::createFromFormat('Y-m-d H:i', $request->input('day')." 00:00");
        $to = Carbon::createFromFormat('Y-m-d H:i', $request->input('day')." 24:00");

        $reservations = ReservationRequest::where('restaurant',$request->input('restaurant_id'))
                                            ->where('date','>=',$from)
                                            ->where('date','<=',$to)
                                            ->orderBy('date','desc')
                                            ->get();


        // filtra in base alle sale
        $struttura = $restaurant->struttura;
        //if($restaurant->struttura && !empty($restaurant->struttura)):

        //endif;

        $not_parsed = $dt->dayOfWeek;
        $weekday = ($not_parsed == 0) ? 6 : $not_parsed-1;

        $fasce = (isset($restaurant->fasce[$weekday])) ? $restaurant->fasce[$weekday] : null;
        $return_fasce = [];
        if($fasce):
            foreach ($fasce as $fascia) {
                $hour_f = explode(":",explode("-",$fascia)[0])[0];
                $minute_f = explode(":",explode("-",$fascia)[0])[1];
                $hour_s = explode(":",explode("-",$fascia)[1])[0];
                $minute_s = explode(":",explode("-",$fascia)[1])[1];

                $put_it = false;

                if($hour_s > $hour_f):

                    foreach ($day_fasce as $fascia_giornaliera) {
                        // start putting

                        if(explode(":",explode("-",$fascia_giornaliera)[0])[0] >= $hour_f &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] >= $minute_f &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[0] <= $hour_s &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] <= $minute_s) {
                            $put_it = true;
                        }

                        if(explode(":",explode("-",$fascia_giornaliera)[0])[0] == $hour_f &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] == $minute_f) $put_it = true;

                        if($put_it &&
                            Carbon::createFromFormat('Y-m-d H:i', $request->input('day').' '.$fascia_giornaliera)
                            <= Carbon::now()
                            ) {
                            $put_it = false;
                        }

                            // verifica che ci siano tavoli liberi
                        if($put_it && $this->hasAvaibleTable($reservations,$fascia_giornaliera,$request->input('people'),$restaurant->struttura)):
                            $return_fasce[] = $fascia_giornaliera;
                        endif;

                        if(explode(":",explode("-",$fascia_giornaliera)[0])[0] == $hour_s &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] == $minute_s) $put_it = false;

                            //return response()->json(['fascia'=>$request->input('day').' '.$fascia_giornaliera],403);

                    }
                else:

                    $current = 0;
                    foreach ($day_fasce as $fascia_giornaliera) {
                        $current++;


                        if(explode(":",explode("-",$fascia_giornaliera)[0])[0] >= $hour_f &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] >= $minute_f) {
                            $put_it = true;
                        }



                        if($put_it &&
                            Carbon::createFromFormat('Y-m-d H:i', $request->input('day').' '.$fascia_giornaliera)
                            >= Carbon::now()
                            ) $put_it = true;

                        if($put_it &&
                            Carbon::createFromFormat('Y-m-d H:i', $request->input('day').' '.$fascia_giornaliera)
                            <= Carbon::now()
                            ) $put_it = false;

                        if($put_it && $this->hasAvaibleTable($reservations,$fascia_giornaliera,$request->input('people'),$restaurant->struttura)) :
                            $return_fasce[] = $fascia_giornaliera;
                        endif;

                        if($current == count($day_fasce)):
                            // ultima fascia del giorno
                            // riparto dall'inizio
                            foreach ($day_fasce as $fascia_giornaliera_da_capo) {

                                if($put_it && $this->hasAvaibleTable($reservations,$fascia_giornaliera,$request->input('people'),$restaurant->struttura)) {
                                    $return_fasce[] = $fascia_giornaliera_da_capo;
                                }

                                if(explode(":",explode("-",$fascia_giornaliera_da_capo)[0])[0] == $hour_s &&
                                explode(":",explode("-",$fascia_giornaliera_da_capo)[0])[1] == $minute_s) {
                                    $put_it = false;
                                }
                            }
                        endif;

                    }
                endif;
            }
        endif;


        return response()->json(['message'=>'ok','fasce'=>$return_fasce], 200);
    }


    private function hasAvaibleTable($reservations,$fascia,$people,$struttura){
        // prendo le sale
        // il resto per ogni sala
        // prendo il numero di tavoli che hanno posto per quelle persone
        // mi segno i numeri e metto in un array il numero
        // prendo tutte le prenotazioni in quella fascia (in quella sala) e metto in un array il numero del tavolo
        // parsing dei tavoli, se il numero è associato a una prenotazione lo tolgo dal primo array (dei tavoli)

        $avaible_posti = 0;
        if(empty($struttura['sale'])) return true;
        foreach ($struttura['sale'] as $sala) {
            $avaible_posti += intval($sala['posti']);
        }

        $total_reserved = 0; // totale posti riservati
        foreach ($reservations as $reservation)
            if($reservation->confirmed == 1 && $reservation->type == 'web' && $reservation->fascia_oraria == $fascia) $total_reserved += intval($reservation->people);

        if(($avaible_posti - $total_reserved) >= $people) return true;
        return false;
    }




    /**
     *  Add an order
     *
     *  <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/addOrder</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li> order   => required|json (a JSON stringified object)</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', statusCode 200 if it's all ok</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     *
     * <p>Example request</p>
     *
     * <pre>
     *       'order':
     *       {
     *          "restaurant": "56c2e130bffebc28088b4678", // ID del ristorante
     *          "menu": 0,
     *          "menu_name": "Menù cena",
     *
     *           // LISTA DEI PIATTI SELEZIONATI
     *           // dai piatti del menu del ristorante è sufficiente metterli direttamente nell'ordine
     *
     *          "plates": [
     *            {
     *              "name": "La panzanella Maremmana",
     *              "image": "56f65c07bffebca3078b4567",
     *              "description": "",
     *              "ingredienti": [
     *                "pane",
     *                "pomodoro",
     *                "prezzemolo",
     *                "cipolla",
     *                "basilico",
     *                "aceto",
     *                "olio extravergine d'oliva"
     *              ],
     *              "custom_info": [],
     *              "price": "6.5",
     *              "options": [
     *                {
     *                  "name": "Prima",
     *                  "type": 0, // categorie complementari hanno type 0
     *                  "options": [
     *                    {
     *                      "no_variation": 1, // il no_variation ci dice se l'opzione comporta una variazione sul prezzo
     *                      "category": "Prima",
     *                      "name": "Prova 1",
     *                      "variation": "", // se 0 il parametro "price" va aggiunto al prezzo del piatto, se 1 va tolto
     *                      "price": "",
     *                      "selected": 1 // Se una opzione è selezionata "selected == 1", se non selezionata il parametro è 0 o non presente
     *                    },
     *                    {
     *                      "no_variation": 1,
     *                      "category": "Prima",
     *                      "name": "Prova 2",
     *                      "variation": "",
     *                      "price": "",
     *                      "selected": 1
     *                    }
     *                  ]
     *                },
     *                {
     *                  "name": "Seconda",
     *                  "type": 0,
     *                  "options": [
     *                    {
     *                      "no_variation": 1,
     *                      "category": "Seconda",
     *                      "name": "Prova 3",
     *                      "variation": "",
     *                      "price": ""
     *                    },
     *                    {
     *                      "no_variation": 0,
     *                      "category": "Seconda",
     *                      "name": "Prova 4",
     *                      "variation": "0",
     *                      "price": "2",
     *                      "selected": 1
     *                    }
     *                  ]
     *                },
     *                {
     *                  "name": "Escludenti",
     *                  "type": "1", // categorie escludenti hanno type 1, le opzioni all'interno non possono essere multiple
     *                  "options": [
     *                    {
     *                      "no_variation": 1,
     *                      "category": "Escludenti",
     *                      "name": "Escludente 1",
     *                      "variation": "",
     *                      "price": "",
     *                      "selected": 1
     *                    },
     *                    {
     *                      "no_variation": 1,
     *                      "category": "Escludenti",
     *                      "name": "Escludente 2",
     *                      "variation": "",
     *                      "price": ""
     *                    }
     *                  ]
     *                }
     *              ],
     *              "id": "572a0012bffebc410b8b4567",
     *              "fasce": null,
     *              "qty": 2, // quantità di questa configurazione (quando ci sono diverse opzioni se le configurazioni dei piatti sono diverse vengono considerati piatti separati, ad esempio avremmo un'altra "panzarella maremmana")
     *              "total_price": 17 // prezzo totale, prezzo del singolo piatto * quantità
     *            },
     *            {
     *              "name": "Millefoglie alla parmigiana",
     *              "image": "56f65c6ebffebca8078b456a",
     *              "description": "",
     *              "ingredienti": [
     *                "melanzane",
     *                "mozzarella",
     *                "pomodoro",
     *                "basilico"
     *              ],
     *              "custom_info": [],
     *              "price": "8",
     *              "id": "572a0012bffebc410b8b4568",
     *              "fasce": null,
     *              "qty": 2,
     *              "total_price": 16
     *            }
     *          ],
     *          "subtotal": 33,      // prezzo dei soli piatti
     *          "delivery": 3,       // orezzo della consegna
     *          "price": 36,         // prezzo totale
     *          "status": 1,         // status pagato
     *
     *           // INDIRIZZO PER LA CONSEGNA
     *           // IMPORTANTE: per il delivery prima dell'ordinazione devi verificare che il CAP per la consegna sia tra i "delivery_zip" del ristorante
     *
     *          "address": "via paolo borsellino 2c",
     *          "city": "Bracciano",
     *          "state": "RM",
     *          "zip": "00062",
     *          "phone": "3472706680",
     *          "type": 0,                            // 0 per delivery, 1 per take away
     *          "date": "2016-05-13 13:00:00",        // data e ora per consegna/take away
     *          "user": "56af9532bffebc59078b4567",   // id utente
     *          "transaction": "paypal",              // tipo di pagamento
     *          "payment": "PAY-0BL295961S391845UK4YYVIY", <b>PayPal Payment ID</b>
     *          "token": "EC-7F532891VL3440415", <b>PayPal Payment token</b>
     *          "payer": "35DB3VQYZ5H72", <b>PayPal Payment payer</b>
     *          "restaurant_info": {
     *            "image": "56c46a82bffebcdb088b4619", // immagine principale del ristorante (image)
     *            "name": "Casa Tua",
     *            "slug": "a-casa-tua"
     *          },
     *          "user_info": {
     *            "image": "57021485bffebc60518b4567", // immagine principale dell'utente (image)
     *            "name": "Fabio Rizzo"
     *         }
     *       }
     * </pre>
     *
    */

    public function addOrder(Request $request){
        $this->validate($request, [
            'order'=>'required|json'
        ]);

        $order = new Orders();
        $order_request = json_decode($request->input('order'),true);
        foreach ($order_request as $key => $value) {
            $order[$key] = $value;
        }

        $u = User::find($order->user);
        if(!$u) return response()->json(['message'=>'error.no_user'],404);

        $r = Restaurant::find($order->restaurant);
        if(!$r) return response()->json(['message'=>'error.no_restaurant'],404);


        $order->save();

        $parameters = [$order->_id,$u->_id,$u->name.' '.$u->surname,$order->price,$order->date,$order->restaurant];
        $ristoratore = ($r->user) ? $r->user : 0;
        Notification::addNotification(1,$parameters,$ristoratore);

        return response()->json(['message'=>'ok','order'=>$order],200);
    }


    /**
     * Take user orders
     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/viewUserOrders</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>user => required|alpha_num</li>
     *          <li>skip => optional|integer (ordini da saltare, per paginazione)</li>
     *          <li>take => optional|integer (numero ordini da prendere, per paginazione)</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'orders' => order Object</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * @param \Illuminate\Http\Request user_id
     * @return \Illuminate\Http\Response
     */
    public function viewUserOrders(Request $request)
    {
        //
        $this->validate($request, [
            'user' => 'required|alpha_num',
            'skip' => 'integer',
            'take' => 'integer'
        ]);


        $orders = [];
        if($request->input('skip') && $request->input('take')) :
            $orders = Orders::where('user',$request->input('user'))->orderBy('date','desc')->skip($request->input('skip'))->take($request->input('take'))->get();
        else:
            $orders = Orders::where('user',$request->input('user'))->orderBy('date','desc')->get();
        endif;

        return response()->json(['message'=>'ok','orders'=>$orders], 200);
    }

    /**
     * Get a single order
     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/viewSingleOrder</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>order => required|alpha_num</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'orders' => order Object</li>
     *          <li> 'message'=>'no_order', statusCode 404 order not found</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * @param \Illuminate\Http\Request user_id
     * @return \Illuminate\Http\Response
     */
    public function viewSingleOrder(Request $request)
    {
        //
        $this->validate($request, [
            'order' => 'required|alpha_num'
        ]);

        $order = Orders::find($request->input('order'));
        if(!$order) return response()->json(['message'=>'no_order'],404);

        return response()->json(['message'=>'ok','order'=>$order], 200);
    }



    /**
     * Change order status
     *
     * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/setOrderStatus</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>order => required|alpha_num</li>
     *          <li>status => required|integer</li>
     *      </ul>
     * </p>
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'order' => order Object</li>
     *          <li> 'message'=>'no_order', statusCode 404 order not found</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
     *
     * <p><b>Avaible status</b></p>
     * <p>
     *      <ul>
     *              <li>0->non confermato (esiste solo su web)</li>
     *              <li>1->pagato  (automatico quando viene creato l'ordine)</li>
     *              <li>2->in lavorazione (lo imposta il ristoratore)</li> -> notifica per i driver
     *              <li>3->rifiutato  (lo imposta il ristoratore)</li>
     *              <li>4->in consegna  (lo imposta il ristoratore)</li>
     *              <li>5->consegnato  (lo imposta il driver)</li>
     *              <li>6->chiesto rimborso  (lo imposta l'utente)</li>
     *              <li>7->rimborsato  (lo imposta admin del sito)</li>
     *              <li>8->preso in consegna (quando il driver prende l'ordine al ristorante)</li>
     *              <li>9->ordine pronto al ristorante (quando il ristoratore ha l'ordine pronto per la consegna)</li>
     *      </ul>
     * </p>
     *
     * @param \Illuminate\Http\Request user_id
     * @return \Illuminate\Http\Response
     */
    public function setOrderStatus(Request $request)
    {
        //
        $this->validate($request, [
            'order' => 'required|alpha_num',
            'status' => 'required|integer'
        ]);


        $order = Orders::find($request->input('order'));
        if(!$order) return response()->json(['message'=>'no_order'], 404);

        $order->status = $request->input('status');

        switch($request->input('status')):
            case 6:
                $restaurant = Restaurant::find($order->restaurant);
                $ristoratore = ($restaurant->user) ? $restaurant->user : '0';

                $parameters = [$order->_id,$restaurant->_id];
                Notification::addNotification(7,$parameters,0);

                if($ristoratore != 0):
                    Notification::addNotification(7,$parameters,$ristoratore);
                endif;
            break;
            case 8:
            // ordine preso dal driver
            // inserisci timestamp della presa in carico
            $order->taken_timestamp = time();
            // calcola i minuti necessari al driver per prendere l'ordine al ristorante da quando è pronto
            $order->taking_time = intval( ($order->taken_timestamp - $order->ready_timestamp) / 60 );
            break;
            case 5:
            // ordine consegnato
            // inserisci timestamp della consegna
            $order->delivered_timestamp = time();
            // minuti necessari per la consegna
            // togli dalla consegna la presa in carico e dividi per 60
            $order->delivery_time = intval( ($order->delivered_timestamp - $order->taken_timestamp) / 60 );

            Event::fire(new OrderDelivered($order));
            break;
            case 9:
            // l'ordine è pronto al ristorante
            // metti il timestamp
            $order->ready_timestamp = time();
            break;
        endswitch;

        $order->save();
        if($request->input('status') == 5):
            // evento ordine consegnato
        endif;

        return response()->json(['message'=>'ok','order'=>$order], 200);
    }

    /**
    *
    * Ask Driver Role
    *
    * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/wannaBeDriver</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>user_id => required|numeric</li>
     *      </ul>
     * </p>
     *
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok'</li>
     *          <li> 'message'=>'error.empty', statusCode 404 user not found</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
    *
    * @param Illuminate\Http\Request $request
    * @param Illuminate\Http\Response $response
    * **/
    public function wannaBeDriver(Request $request){
        $this->validate($request, [
            'user_id'=> 'required|alpha_num'
        ]);
        $u = User::find($request->input('user_id'));
        if(!$u) return response()->json(['message'=>'error.empty'],404);

        $parameters = [$u->_id,$u->name.' '.$u->surname];
        Notification::addNotification(10,$parameters,0);

        return response()->json(['message'=>'ok'],200);
    }



    /**
    *
    * Set driver current position
    *
    * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/setDriverPosition</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>lat => required|numeric</li>
     *          <li>lng => required|numeric</li>
     *      </ul>
     * </p>
     *
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'order' => order Object</li>
     *          <li> 'message'=>'error.empty', statusCode 404 user not found</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
    *
    * @param Illuminate\Http\Request $request
    * @param Illuminate\Http\Response $response
    * **/
    public function setDriverPosition(Request $request){
        $this->validate($request, [
            'lat'=> 'required|numeric',
            'lng'=> 'required|numeric'
        ]);
        $u = User::find($request->input('user_id'));
        if(!$u) return response()->json(['message'=>'error.empty'],404);

        $loc = ['type'=>'Point','coordinates'=>[0,0]];
        if($u->loc) $loc = $u->loc;

        $loc['coordinates'] = [doubleval($request->input('lng')),doubleval($request->input('lat'))];
        $u->loc = $loc;

        $u->save();
        return response()->json(['message'=>'ok','user'=>$u],200);
    }

    /**
    *
    * Assign order to driver
    *
    * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/assignOrderToDriver</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>order => required|alpha_num</li>
     *          <li>driver => required|alpha_num</li>
     *      </ul>
     * </p>
     *
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'order' => order Object</li>
     *          <li> 'message'=>'error.empty', statusCode 404 user or order not found</li>
     *          <li> 'message'=>'error.cannot', statusCode 403 user is not assigned to order</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
    *
    * @param Illuminate\Http\Request $request
    * @param Illuminate\Http\Response $response
    * **/
    public function assignOrderToDriver(Request $request){
        $this->validate($request, [
            'order'=> 'required|alpha_num',
            'driver'=> 'required|alpha_num'
        ]);
        $u = User::find($request->input('driver'));
        $o = Orders::find($request->input('order'));
        if(!$u || !$o) return response()->json(['message'=>'error.empty'],404);

        if(!in_array($u->_id, $o->notified_drivers)) return response()->json(['message'=>'error.cannot'],403);

        $o->assigned_driver = $u->_id;

        $o->save();
        return response()->json(['message'=>'ok','user'=>$u],200);
    }

    /**
    *
    * Get User orders delivered
    *
    * <p><b>Metod:</b> POST</p>
    * <p><b>Endpoint:</b> v1/api/public/getDriverOrders</p>
    *
    * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>skip => required|integer (how much to skip)</li>
     *          <li>take => required|integer (how much to take)</li>
     *          <li>user_id => required|alpha_num</li>
     *      </ul>
     * </p>
    *
    * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'orders' => order Object</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
    *
    * @param Illuminate\Http\Request $request
    * @param Illuminate\Http\Response $response
    * **/
    public function getDriverOrders(Request $request){
        $this->validate($request, [
            'skip'=> 'required|integer',
            'take'=> 'required|integer',
            'user'=> 'required|alpha_num'
        ]);

        return response()->json(
            ['message'=>'ok',
            'orders'=>Orders::where('assigned_driver',$request->input('user_id'))->skip($request->input('skip'))->take($request->input('take'))->orderBy('date','desc')->get()
            ],200);
    }


    /**
    *
    * Assign rating to driver and update stat,
    *
    * <p>Stats updated:
    *       <ul>
     *          <li>voto_medio number</li>
     *          <li>num_voti ratings number</li>
     *          <li>rating array of order id, rating value, timestamp</li>
     *      </ul>
     * </p>
    *
    * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/assignRatingToDriver</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>driver => required|alpha_num</li>
     *          <li>order => required|alpha_num</li>
     *          <li>rating => required|integer|min:1|max:5</li>
     *      </ul>
     * </p>
     *
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'order' => order Object</li>
     *          <li> 'message'=>'error.empty', statusCode 404 user or order not found</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
    *
    * @param Illuminate\Http\Request $request
    * @param Illuminate\Http\Response $response
    * **/
    public function assignRatingToDriver(Request $request){
        $this->validate($request, [
            'driver'=> 'required|alpha_num',
            'order'=> 'required|alpha_num',
            'rating'=> 'required|integer|min:1|max:5'
        ]);
        $u = User::find($request->input('driver'));
        $o = Orders::find($request->input('order'));
        if(!$u || !$o) return response()->json(['message'=>'error.empty'],404);

        $rating = (!$u->rating) ? [] : $u->rating;
        $rating[] = ['order'=>$o->_id,'rating'=>$request->input('rating'),'time'=>time()];

        $voto_medio_driver = ($u->voto_medio) ? $u->voto_medio : 0;
        $num_voti = ($u->num_voti) ? $u->num_voti : 0;

        $voto_totale = $voto_medio_driver*$num_voti;

        $voto_totale += $request->input('rating');
        $num_voti++;
        // calcola nuova media
        $nuovo_voto_medio = $voto_totale/$num_voti;

        // aggiorniamo il driver
        $u->rating = $rating;
        $u->num_voti = $num_voti;
        $u->voto_medio = $nuovo_voto_medio;

        $u->save();
        return response()->json(['message'=>'ok','user'=>$u],200);
    }

    /**
    *
    * Retrieve orders in which the driver has been notified
    *
    *
    * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> v1/api/public/getMineNotified</p>
     *
     * <p><b>Parameters Details</b></p>
     * <p>
     *      <ul>
     *          <li>user_id => required|alpha_num</li>
     *      </ul>
     * </p>
     *
     * <p><b>Return</b></p>
     * <p>
     *      <ul>
     *          <li> 'message'=>'ok', 'orders' => order Object</li>
     *          <li> 'message'=>'error.no_user', statusCode 404 user not found</li>
     *          <li> statusCode 422 on field validation errors</li>
     *      </ul>
     * </p>
    *
    * @param Illuminate\Http\Request $request
    * @param Illuminate\Http\Response $response
    * **/
    public function getMineNotified(Request $request){
        $this->validate($request, [
            'user_id'=> 'required|alpha_num'
        ]);

        $user = User::find($request->input('user_id'));
        if(!$user) return response()->json(['message'=>'error.no_user'],404);

        $d = Carbon::now();
        $o = Orders::where('date','>',$d)->where('status',2)->whereIn('notified_drivers',[$user])->get();

        return response()->json(['message'=>'ok','orders'=>$o],200);
    }

    /**
    *
    * Assign rating to driver and update stat,
    *
    *
    * <p><b>Metod:</b> POST</p>
     * <p><b>Endpoint:</b> https://menooapp.firebaseio.com/orders/{user_id}/{order_id}/</p>
     *
     * Credenziali per app:
     * UID:   V8oM9ujrfuOdhhiOdjyjmg2H2LB3
     * email: ivansignorile@gmail.com
     * pass:  quella che hai resettato
     *
     * Si deve aggiungere un oggetto all'ordine:
     * <pre>
     *  {
     *   lat: latitudine,
     *   lng: longitudine,
     *   order: order_id,
     *   user: user_id
     *  }
     * </pre>
     *
     * Se l'ordine non è presente su firebase va aggiunto
    *
    *
    * **/
    public function setOrderPosition(Request $request){
        // silence is golden
    }



    /**
    *
    * Add an order
    *
    * 'user_id' => 'required|alpha_num',
    * 'restaurant' => 'required|alpha_num',
    * 'reservation' => 'required|alpha_num',
    * 'menu'=>'required|integer',
    * 'menu_qty'=>'required|integer',
    * 'menu_name'=>'required|string',
    * 'plates'=>'required|array',
    * 'price'=>'required|numeric'
    *
    *
    *
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response message=>'ok','order'=>$order
    *
    * **/
    public function addPreorder(Request $request)
    {
      $this->validate($request, [
        'user_id' => 'required|alpha_num',
        'restaurant' => 'required|alpha_num',
        'reservation' => 'required|alpha_num',
        'menu'=>'required|integer',
        'menu_qty'=>'required|integer',
        'menu_name'=>'required|string',
        'plates'=>'required|array',
        'subtotal'=>'required|numeric',
        'price'=>'required|numeric'
      ]);

      // qui è da impostare la validazione dell'ordine
      $user = User::find($request->input('user_id'));

      $reservation = ReservationRequest::find($request->input('reservation'));
      if(!$reservation) return response()->json(['message'=>'error', 'error'=>'no_reservation'], 404);
      if($reservation->preorder) return response()->json(['message'=>'error', 'error'=>'just_added'], 403);
      if($reservation->confirmed != 0) return response()->json(['message'=>'error', 'error'=>'just_confirmed'], 403);

      if($reservation->user != $user->_id) return response()->json(['message'=>'error', 'error'=>'invalid_user'], 403);

      $restaurant = Restaurant::find($request->input('restaurant'));
      if(!$restaurant) return response()->json(['message'=>'error', 'error'=>'no_restaurant'], 404);

      $avaible_menu = [];
      $i = 0;
      foreach ($restaurant->menu['menus'] as $menu) {
          if(isset($menu['orderready']) && $menu['orderready'] == 1) $avaible_menu[] = $i;
          $i++;
      }

      if(!in_array( $request->input('menu'), $avaible_menu)) return response()->json(['message'=>'error', 'error'=>'invalid_menu'], 403);

      if($restaurant->menu['menus'][$request->input('menu')]['name'] != $request->input('menu_name')) return response()->json(['message'=>'error', 'error'=>'invalid_menu_name'], 403);

      $order_array = [
        'menu'=>$request->input('menu'),
        'menu_qty'=>($request->input('menu_qty')) ? $request->input('menu_qty') : 0,
        'menu_name'=>$request->input('menu_name'),
        'menu_type'=>(isset($restaurant->menu['menus'][$request->input('menu')]['menu_type'])) ? $restaurant->menu['menus'][$request->input('menu')]['menu_type'] : 0,
        'plates'=>$request->input('plates'),
        'price'=>$request->input('price')
      ];

      if( !OrdersController::validateOrder($order_array, $restaurant->menu['menus'], 1) ) return response()->json(['message'=>'error', 'error'=>'not_valid_order'], 403);

      $reservation->preorder = $order_array;
      $reservation->save();

      return response()->json(['message'=>'ok','reservation'=>$reservation],200); // return ORDER to take id

    }



}
