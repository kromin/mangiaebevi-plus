<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use Curl;
use Log;

class GoogleAddressController extends Controller
{
    /**
    *
    * Take district by Google lat and lng
    * if there is a neighborood or a point of interest return them in array
    * da integrare con https://developer.here.com/api-explorer/rest/geocoder/reverse-geocode-district:
    * https://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.json?prox=41.9527953%2C12.4107242&mode=retrieveAreas&gen=8&app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg
    * 
    * @param $lat,$lng geocoordinates
    * @return array  [zone, point of interest]
    * 
    * */
    public static function returnDistrict($lat,$lng){
        
        $url = 'https://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.json?prox='.$lat.','.$lng.'&mode=retrieveAreas&gen=8&app_id='.env('HERE_APP_ID').'&app_code='.env('HERE_APP_KEY');
        
        $return_array = Curl::to($url)->get();
        $district = json_decode($return_array,true);        
        return (isset($district['Response']['View'][0]['Result'][0]['Location']['Address']['District'])) 
        ? 
        (
        ($district['Response']['View'][0]['Result'][0]['Location']['Address']['District'] == 'Europa') 
        ? 'Eur' 
        : $district['Response']['View'][0]['Result'][0]['Location']['Address']['District']
        ) 
        : ''; //$return_array;
    }



    /**
    *
    * Take address information using google API
    * 
    * 
    * @param $address
    * @return array  [lat,lng,location=>[all address info]]]
    * @return Illuminate\Http\Response 'message'=>'error.google_address_not_found' if address not found
    * @return array(lat,lng,address_components,city,state,zip)
    * 
    * */
    public static function returnLocation($address){

        $return_array = [];
        
        $google_response = Curl::to('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address))->get();

        $response = json_decode($google_response,true);
        if(isset($response['results'][0]['address_components'])) :
            $locations = [];
            $city = ''; $state = ''; $zip = '';
            foreach ($response['results'][0]['address_components'] as $address_component) {
                $locations[] = $address_component['long_name'];
                
                if(isset($address_component['types'][0]) && $address_component['types'][0] == 'locality' && $address_component['types'][1] == 'political') $city = $address_component['long_name'];
                if(isset($address_component['types'][0]) && $address_component['types'][0] == 'administrative_area_level_3' && $city == '') $city = $address_component['long_name'];
                if(isset($address_component['types'][0]) && $address_component['types'][0] == 'administrative_area_level_2') $state = $address_component['short_name']; 
                if(isset($address_component['types'][0]) && $address_component['types'][0] == 'postal_code') $zip = $address_component['long_name']; 
            }
            $lat = $response['results'][0]['geometry']['location']['lat'];
            $lng = $response['results'][0]['geometry']['location']['lng'];
            
            if($city == '' || $state == '' || $zip == '') return false;
            $return_array =  [$response['results'][0]['geometry']['location']['lat'],
                            $response['results'][0]['geometry']['location']['lng'],
                            array_unique($locations),
                            $city,
                            $state,
                            $zip
                            ];           
            
            //$restaurant->valid_address = 1;// imposto il valid address
        else:
            return false;//response()->json(['message'=>'error.google_address_not_found'], 200);
        endif;
        return $return_array;
    }


    /**
    *
    * Return if an address is valid or not
    * @param address string
    * @return Illuminate\Http\Response 'message'=>'error.google_address_not_found' || 'ok', 'address_components'=> array
    * 
    * */
    public function validateAddress(Request $request)
    {
        $this->validate($request, [
            'address' => 'required|string'
        ]);

        $return_array = [];
        $google_response = Curl::to('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($request->input('address')))->get();

        $response = json_decode($google_response,true);
        if(isset($response['results'][0]['address_components'])) :
            $locations = [];
            $city = ''; $state = ''; $zip = '';
            foreach ($response['results'][0]['address_components'] as $address_component) {
                $locations[] = $address_component['long_name'];
                if($address_component['types'][0] == 'locality' && $address_component['types'][1] == 'political') $city = $address_component['long_name'];
                if($address_component['types'][0] == 'administrative_area_level_3' && $city == '') $city = $address_component['long_name'];
                if($address_component['types'][0] == 'administrative_area_level_2') $state = $address_component['short_name']; 
                if($address_component['types'][0] == 'postal_code') $zip = $address_component['long_name']; 
            }
            $lat = $response['results'][0]['geometry']['location']['lat'];
            $lng = $response['results'][0]['geometry']['location']['lng'];
            
            $return_array =  [$response['results'][0]['geometry']['location']['lat'],
                            $response['results'][0]['geometry']['location']['lng'],
                            array_unique($locations),
                            $city,
                            $state,
                            $zip
                            ];     
        else:
            return response()->json(['message'=>'error.google_address_not_found'], 200);
        endif;
        return response()->json(['message'=>'ok','address_components'=>$return_array], 200);$return_array;
    }



    /**
    *
    * Prendi direzioni con le API di google
    * 
    * @param Illuminate\http\Request
    * @param from required|string
    * @param to required|string
    * 
    * @return Illuminate\Http\Response message 'ok', directions=>array
    * 
    * */
    public function returnDirections(Request $request){

        $this->validate($request, [
            'from' => 'required|string',
            'to' => 'required|string'
        ]);

        $return_array =  Curl::to('https://maps.googleapis.com/maps/api/directions/json?origin='.urlencode($request->input('from')).'&destination='.urlencode($request->input('to')).'&language=it' )->get();
        return $return_array;
    }


    /**
    *
    * Getting the zip code
    * 
    * Validate zip code with zippopotam.us API and return geo coordinates (not accurate like google), example request http://api.zippopotam.us/IT/00136
    * 
    * @param string zip code to validate
    * @return bool true if is a valid zip code
    * 
    * */
    public static function validateCap($zip){
        $return =  json_decode(Curl::to("http://api.zippopotam.us/IT/".$zip )->get(),true);
        
        return (isset($return['places']) && count($return['places']) > 0) ? true : false;
    }



}
