<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\Services;
use MangiaeBevi\TipoLocale;
use MangiaeBevi\Types;
use MangiaeBevi\HomeCity;
use MangiaeBevi\CreditCard;
use MangiaeBevi\Regions;
use MangiaeBevi\Restaurant;
use MangiaeBevi\User;
use MangiaeBevi\Pacchetti;

use Mail;
use File;
use Sunra\PhpSimple\HtmlDomParser;
use Storage;


use Input;
use Image;
use Auth;

class ToolController extends Controller
{
    /**
     * Useful tools for frontend
     *
     * @return \Illuminate\Http\Response
     */
    public function getSiteInfo(Request $request)
    {
        //
        $response = [
            'message'=>'ok',
            'services'=>Services::orderBy('name', 'asc')->get(['name','image','icon']),
            'tipiLocali'=>TipoLocale::orderBy('name', 'asc')->get(['name']),
            'tipiCucine'=>Types::orderBy('name', 'asc')->get(['name']),
            'regioniCucine'=>Regions::orderBy('name', 'asc')->get(['name']),
            'homeCities'=>HomeCity::orderBy('order', 'asc')->get(['name','image','lat','lng']),
            'pacchetti'=>Pacchetti::all(),
            'creditCard'=>CreditCard::orderBy('name', 'asc')->get(['name','image','icon']),
            'staticDomain'=>env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN'),
            'delivery_price'=>env('CONSEGNA_PREZZO'),
            'minimum_order'=>env('ORDINE_MINIMO'),
            'main_domain'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN')
        ];
        return response()->json($response, 200);
    }


    /**
     * Get restaurant with empty images
     *
     * @return \Illuminate\Http\Response
     */
    public function getRestaurantWithoutImages(Request $request)
    {
        //

        $i = Restaurant::where('approved','!=',2)->orderBy("name","asc")->get();
        $ret = '';
        $array_ristoranti = [];
        foreach ($i as $risto) {
            $restaurant = [];
            if(isset($risto['images']) && count($risto['images']) > 0){
                $n = 0;
                $image_single = [];
                foreach ($risto['images'] as $image_id) {
                    if(!file_exists('../../'.env('APP_STATIC_PATH').'/restaurant/small/'.$image_id.'.png')) :
                        $n++;
                        $image_single[] = $image_id;
                    endif;
                }
                if($n > 0){
                    $array_ristoranti[] = [
                    'name'=>$risto['name'],
                    'slug'=>$risto['slug'],
                    'address'=>$risto['address'],
                    'city'=>$risto['city'],
                    'images'=>$image_single,
                    '_id'=>$risto['_id'],
                    'google_url'=>str_replace(" ", "+", "ristorante ".$risto->name." ".$risto['city'])
                    ];
                }
            }

        }


        $response = [
            'message'=>'ok',
            'restaurants'=>$array_ristoranti
        ];
        return response()->json($response, 200);
    }

    /**
     * Get restaurant with empty images
     *
     * @return \Illuminate\Http\Response
     */
    public function sobstituteRestaurantImage(Request $request)
    {
        //
        $this->validate($request, [
            'old_id' => 'required|alpha_num',
            'image' => 'required|mimes:jpeg,gif,png,svg',
        ]);

        $i = \Input::file('image');
        $img = Image::make($i);

        $img->encode('png', 100);
        $path = ['big','mobile','medium','small','square'];
        $dimensions = [[1200,null],[0,null],[800,null],[400,null],[400,400]];

        $x = 0;
        foreach($dimensions as $dimension):
            if($dimension[0] != 0):
                // salva solo se ci sono le dimensioni
                if($x > 0):
                    if($dimension[0] != $dimension[1]):
                        $img->resize($dimension[0], $dimension[1], function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    else:
                        // le quadrate hanno funzione diversa
                        $img->fit($dimension[0], $dimension[1]);
                    endif;
                endif;
                $img->save('../../'.env('APP_STATIC_PATH').'/restaurant/'.$path[$x].'/'.$request->input('old_id').'.png', 100);
            endif;
            $x++;
        endforeach;


        $response = [
            'message'=>'ok'
        ];
        return response()->json($response, 200);
    }


    /**
    *
    * Send a mail of contact
    * @param Request $request
    * @return Response json message=>ok
    *
    * **/
    public function contactMail(Request $request){
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'message' => 'required|string',
            'subject' => 'required|integer', // 0 => contact, 1 => menoodriver
            'phone' => 'numeric',
            'risto' => 'string',
            'address' => 'string'
        ]);

        $subject = 'Richiesta di contatto';
        switch($request->input('subject')):
            case 0:
                $subject = 'Richiesta di contatto';
            break;
            case 1:
                $subject = 'Richiesta di lavoro';
            break;
            case 2:
                $subject = 'Richiesta da ristoratore';
                if(!$request->input('address') || !$request->input('risto') || !$request->input('phone')):
                    return response()->json(['message'=>'error.compila_campi'],200);
                endif;
            break;
            case 3:
                $subject = 'Segnalazione errore';
            break;
        endswitch;

        $mail_data = [
            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'message'=>$request->input('message'),
            'subject'=>$subject,
            'phone'=>($request->input('phone')) ? $request->input('phone') : 0,
            'risto'=>($request->input('risto')) ? $request->input('risto') : 0,
            'address'=>($request->input('address')) ? $request->input('address') : 0
        ];
        Mail::send('email.contact', ['mail_data' => $mail_data],
            function ($m) use ($mail_data) {
            $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
            $m->to(env('ADMIN_EMAIL'), 'Admin')->subject($mail_data['subject']);
        });

        return response()->json(['message'=>'ok','subject'=>$subject],200);

    }

    /**
    *
    * Send a mail to reccomend restaurant
    * @param Request $request
    * @return Response json message=>ok
    *
    * **/
    public function reccomendMail(Request $request){
        $this->validate($request, [
            'name' => 'string',
            'fname' => 'required|string',
            'email' => 'required|email',
            'message' => 'required|string',
            'restaurant' => 'required|alpha_num'
        ]);

        $restaurant = Restaurant::find($request->input('restaurant'));

        if(!$restaurant) return response()->json(['message'=>'error.empty'],200);
        if(!$request->input('name') && !Auth::check()) return response()->json(['message'=>'error.empty'],200);

        $city = ($restaurant->city && $restaurant->city != '') ? urlencode($restaurant->city) : '-';
        $mail_data = [
            'name'=>($request->input('name')) ? $request->input('name') : Auth::user()->name,
            'fname'=>$request->input('fname'),
            'email'=>$request->input('email'),
            'message'=>$request->input('message'),
            'subject'=>$request->input('fname').' ti consiglia '.$restaurant->name,
            'link'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/'.$city.'/'.$restaurant->slug .'/'.Auth::user()->_id
        ];

        Mail::send('email.reccomend', ['mail_data' => $mail_data],
            function ($m) use ($mail_data) {
            $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
            $m->to($mail_data['email'], $mail_data['fname'])->subject($mail_data['subject']);
        });

        return response()->json(['message'=>'ok'],200);
    }

    /**
    *
    * vedi se una pagina è già stata aggiunta
    * @param Request $request
    * @return Response json message=>ok
    *
    * **/
    public function isToRender(Request $request){
        $this->validate($request, [
            'page_url' => 'required|string'
        ]);

        $message = 'render';
        if(File::exists(storage_path('app/static/'.urlencode($request->input('page_url')) )
                )
            ):
            $message = 'no_render';
        endif;

        return response()->json(['message'=>'page_rendered','render'=>$message],200);
    }

    /**
    *
    * Save the page content as static
    * @param Request $request
    * @return Response json message=>ok
    *
    * **/
    public function addStaticPage(Request $request){
        //if(env('APP_ENV') == 'production'){

            $this->validate($request, [
                'page_url' => 'required|string'
            ]);

            $dom = HtmlDomParser::str_get_html( $request->input('page_content') );

            foreach ($dom->find('script, style, link, #rufous-sandbox') as $to_remove) {
                // tolgo gli script senza attributo mantain
                if(!isset($to_remove->mantain)){
                    // è da mantenere
                    $to_remove->outertext = "";
                }
            }

            foreach ($dom->find('[ng-if]') as $to_remove) {
                // tolgo gli script senza attributo mantain
                $to_remove->ngIf = null;
            }


            foreach ($dom->find("#google_map") as $map){
                $map->innertext = '';
            }

            foreach ($dom->find("#fb-root") as $map){
                $map->innertext = '';
            }

            foreach ($dom->find("img") as $img){
                if(preg_match("/chrome-extension:/i", $img->src)):
                    $img->outhertext = '';
                endif;
            }

            // rendilo stringa
            $string = $dom->save();//->plaintext;

            $string = preg_replace('/ng-if=\"([^"]*)\"/','',$string);
            $string = preg_replace("/<!--.*?-->/ms","",$string);
            $page_content = '<!DOCTYPE html>
                            <html lang="it" ng-app="app" ng-controller="ApplicationCtrl">'
                            .$string.
                            '</html>
                            <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
                            <link href="http://'.env('APP_STATIC_DOMAIN').'/css/meb.css" rel="stylesheet" type="text/css">';

            Storage::disk('local')->put('static/'.urlencode($request->input('page_url')), $page_content);

        //}
        return response()->json(['message'=>'page_rendered'],200);
    }
}
