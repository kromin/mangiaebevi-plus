<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\UserActions;
use Auth;

class UserActionsController extends Controller
{
    /**
    *
    * Add a new User action for dashboard
    * 
    * types:
    *   0 => stretto amicizia                   [useradd]
    *   1 => raccomandazione ristorante         [restaurant]
    *   2 => raccomandazione piatto             [plate]
    *   3 => preferiti                          [restaurant]
    *   4 => piatto image                       [plateimage, plate]
    *   5 => follow                             [useeradd]
    * 
    * 
    * @param type integer
    * @param parameters array array of useful parameters to identify variables
    * @return array [message=>'ok|error']
    * */
    public static function addUserAction($type,$parameters,$user_notification = false){
        
        $action = new UserActions();
        if($type > 5 || $type < 0) return;
        $action->type = intval($type);
        $action->user = ($user_notification) ? $user_notification : Auth::user()->_id;
        switch($action->type):
            case 0:
            $action->useradd = $parameters[0];
            break;
            case 1:
            $action->restaurant = $parameters[0];
            break;
            case 2:
            $action->plate = $parameters[0];
            break;
            case 3:
            $action->restaurant = $parameters[0];
            break;
            case 4:
            $action->plateimage = $parameters[0];
            $action->plate = $parameters[1];
            break;
            case 5:
            $action->useradd = $parameters[0];
            break;
        endswitch;

        $action->save();
    }
}
