<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use Jenssegers\Mongodb\Model as Eloquent;

use MangiaeBevi\CreditCard;
use MangiaeBevi\Restaurant;

class CreditCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cards = CreditCard::all();        
        return response()->json(['message'=>'ok','cards'=>$cards], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string',
            'image'=>'string',
            'icon' =>'string'
        ]);

        $card = new CreditCard();
        $card->name = $request->input('name');
        $card->image = ($request->input('image')) ? $request->input('image') : '';
        $card->icon = ($request->input('icon')) ? $request->input('icon') : '';

        $card->save();

        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'card'=>$card 
        ];
        return response()->json($response, $statusCode);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);

        $card = CreditCard::find($request->input('id'));
        if(!$card):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        

        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'card' => $card 
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request, [
            'id'    => 'required|alpha_num',
            'name'  => 'required|string',
            'image' =>'string',
            'icon'  =>'string'
        ]);

        $card = CreditCard::find($request->input('id'));
        if(!$card):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        $card->name = $request->input('name');
        $card->image = ($request->input('image')) ? $request->input('image') : '';
        $card->icon = ($request->input('icon')) ? $request->input('icon') : '';


        $card->save();
        
        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'card'=>$card
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);
        $card = CreditCard::find($request->input('id'));
        if(!$card):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        $card->delete();
        return response()->json(['message'=>'ok'], 200);
    }



    /**
    *
    *  Take all credit cards from restaurants in db
    *  
    * 
    * ***/
    public function massiveFillCreditCard()
    {
        $restaurants = Restaurant::all();
        $cards = [];
        foreach($restaurants as $restaurant){ 
            if($restaurant->carte) :          
                foreach($restaurant->carte as $card){
                    $cards[] = $card;
                }
            endif;
        }

        $cards = array_unique($cards);
        
        foreach($cards as $card){
            $addToCollection = new CreditCard();
            $addToCollection->name = $card;
            $addToCollection->save();
        }
        return response()->json(['message'=>'ok'], 200);
    }

    public function massiveCleanRestaurantsCreditCard()
    {

        $cards = CreditCard::all();

        $restaurants = Restaurant::all();
        foreach($restaurants as $restaurant){
            // take restaurant cards 
            $single_cards = [];
            // per ogni carta del ristorante
            if($restaurant->carte){
                foreach($restaurant->carte as $card){
                    // vedi le carte dell'app e per ognuno di loro
                    foreach($cards as $single_card_collection){
                        // se il nome è uguale alla carta del ristorante
                        if($single_card_collection->name == $card){
                            // inseriscilo nell'array che aggiorna il ristorante
                            $single_cards[] = $single_card_collection->_id;
                        }
                    }
                }
            }
            if(!empty($single_cards)) :
                // aggiorniamo il singolo ristorante
                $this->updateRestaurantCreditCard($restaurant->_id,$single_cards);
            endif;
        }
        return response()->json(['message'=>'ok'], 200);
    }

    private function updateRestaurantCreditCard($restaurant_id,$cards)
    {
        $restaurant = Restaurant::find($restaurant_id);
        $restaurant->carte = $cards;
        $restaurant->save();        
    }
}
