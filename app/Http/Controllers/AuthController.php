<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\Http\Controllers\ImagesController as SingleImage;
use MangiaeBevi\Images;
use Image;

use Auth;
use Socialite;
use Log;
use Cookie;

use MangiaeBevi\User;

class AuthController extends Controller
{
    public function __construct(){
        $this->auth = new Auth;
    }

    public function login(Request $request){
        
        $this->validate($request, [
            'email' => 'required|email',
            'pwd' => 'required|min:2|max:50',
        ]);
        
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('pwd') ])) :
            $u = User::find(Auth::user()->_id);
            if($u->verified != 1) {
                Auth::logout();
                return response()->json(['message'=>'error','error_message'=>'Per accedere devi prima attivare il tuo account'], 200);
            }       
            return response()->json(['message'=>'ok'], 200);
        else :
            return response()->json(['message'=>'error.nologinmatch','error_message'=>'Le credenziali sono errate'], 200);
        endif;
    }


    public function logout(){
      if(Auth::check()) :
        Auth::logout();
    endif;
    return response()->json(['message'=>'ok'], 200);

    }

    /**
    *
    * Return if a user is logged in
    * @return $response loggedStatus integer 0 => user not logged in, 1 => user logged
    *
    */
    public function isLogged(){
      $response = ['loggedStatus'=>0];
      if(Auth::check()) :
        $response = ['loggedStatus'=>1,'role'=>Auth::user()->role,'ID'=>Auth::user()->ID,'user'=>Auth::user()];
      endif;
      return response()->json($response, 200);
    }


     /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from Provider.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        switch($provider){
            case 'facebook':
                $user = Socialite::driver('facebook')
                ->fields([
                    'name', 
                    'first_name', 
                    'last_name', 
                    'email', 
                    'gender', 
                    'verified'
                ])->user();
            break;
            default:
                $user = Socialite::driver($provider)
                ->user();
            break;
        }
        
        // utente loggato dal social
        // vedo se già registrato coi social
        
        // facebook-linkedin: name
        // linkedin: firstName lastName
        $this->IsSocialLoggedJustRegistered($user,$provider);
        return view('social');
        // $user->token;
    }

    /**
    *
    * Logga un utente con le informazioni del social
    * 
    * @param $user istanza di Socialite
    * @param $provider nome del provider usato per l'autenticazione
    * 
    * 
    * **/
    private function IsSocialLoggedJustRegistered($user, $provider){
        
        if(!$user->getEmail() || $user->getEmail() == '') :
            echo 'Accedi con un account che abbia un indirizzo mail associato'; 
            return;
        endif;
        
        $user_id = $user->getId();
       
        // prendo id utente e provider
        $logged = User::where('social_provider',$provider)->where('social_provider_id',$user_id)->get(); 

        $img = Image::make($user->getAvatar());
        
        if(!$logged || count($logged) == 0):
            
            // vedo se c'è un utente con lo stesso indirizzo email
            $usr_just_email = User::where('email',$user->getEmail())->get();
            if(!$usr_just_email || count($usr_just_email) != 1):

                $new_user = new User();
                switch($provider):
                    case 'facebook':
                    $new_user->name = $user->user['first_name'];//$user->getName();
                    $new_user->surname = $user->user['last_name'];
                    
                    //$new_user->gender = $user['gender'];
                    break;
                    case 'twitter':
                    $parts = explode(" ", $user->getName());
                    $lastname = array_pop($parts);                    
                    $new_user->name = implode(" ", $parts);
                    $new_user->surname = $lastname;
                    // avatar
                    break;
                    case 'linkedin':
                    $new_user->name = $user->user['firstName'];
                    $new_user->surname = $user->user['lastName'];                    
                    //$new_user->gender = $user['gender'];
                    break;
                endswitch;                
                $new_user->email = $user->getEmail();               
                $new_user->social_provider = $provider;
                $new_user->social_provider_id = $user->getId();
                $new_user->role = 1; // questo da vedere come gestirlo
                $new_user->verified = 1;

                $new_user->friends = [];
                $new_user->friend_request = [];
                $new_user->following = [];
                $new_user->followedby = [];
                $new_user->foodcoin = env('FOODCOIN_REGISTRATION');
                $new_user->current_address = '';
                $new_user->address_list = [];
                $new_user->favourite_list = [];
                $new_user->wishlist = [];
                $new_user->recommended_plates = [];
                $new_user->recommended_restaurants = []; 
                if(Cookie::get('ib')) $new_user->invitedby = Cookie::get('ib');  

                
                $new_user->save();

                $this->saveSocialImage($new_user->_id,$user->getAvatar());

                Auth::loginUsingId($new_user->_id);                
            else:
                
                $logged = User::find($usr_just_email[0]->_id);
                $logged->social_provider = $provider;
                $logged->social_provider_id = $user->getId();
                $logged->save();

                if(!$logged->avatar || $logged->avatar == ''):
                    $this->saveSocialImage($logged->_id,$user->getAvatar());
                endif;
                
                Auth::loginUsingId($logged->_id);
                
            endif;
            
        else:
            if(!$logged[0]->avatar || $logged[0]->avatar == ''):
                $this->saveSocialImage($logged[0]->_id,$user->getAvatar());
            endif;
            Auth::loginUsingId($logged[0]->_id);
        endif; 

    }

    private function saveSocialImage($user_id,$avatar){

        if(!$avatar || $avatar == '') return;
        $u = User::find($user_id);
        
            // prendi l'immagine dei social
        $img = Image::make($avatar);

        if(!$img) return;

        $images = new Images();
        $images->type = 'users';
        $images->alt = 'avatar';
        $images->title = 'Avatar utente';
        $images->ref_id = $u->_id;
        $images->uploaded_by = $u->_id;
        $images->save();


        $img->encode('png', 100);
        $path = ['big','mobile','medium','small','square'];
        $dimensions = [[1200,null],[0,null],[400,400],[250,250],[80,80]];
        
        $x = 0;
        foreach($dimensions as $dimension):
            if($dimension[0] != 0):                
                // salva solo se ci sono le dimensioni
                if($dimension[0] != $dimension[1]):
                    $img->resize($dimension[0], $dimension[1], function ($constraint) {
                        $constraint->aspectRatio();            
                    });
                else:
                    // le quadrate hanno funzione diversa
                    $img->fit($dimension[0], $dimension[1]); 
                endif;
                $img->save('../../'.env('APP_STATIC_PATH').'/users/'.$path[$x].'/'.$images->_id.'.png', 100);

                //Mangiaebevi/mangiaebevi/app/Http/Controllers
            endif;   
            $x++;         
        endforeach;
        $u->avatar = $images->_id;
        $u->save();

        return true;
    }

}
