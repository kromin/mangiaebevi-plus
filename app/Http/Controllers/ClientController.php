<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\Restaurant;
use MangiaeBevi\User;
use MangiaeBevi\Recommendation;
use MangiaeBevi\ReservationRequest;
use MangiaeBevi\RestaurantClient;
use Auth;

use Log;
class ClientController extends Controller
{
    /**
    *
    * Add client to restaurant
    * 
    * @param string $user_id
    * @param string $restaurant_id
    * */
    public static function addClientToRestaurant($user_id,$restaurant_id){
        $r = Restaurant::where('_id',$restaurant_id)->whereIn('clients',[$user_id])->get(['_id']);
        if(count($r) <= 0) return;

        
        $r = Restaurant::find($restaurant_id);
        if(!$r) return;

        if(!$r->clients) :
            $r->clients = [$user_id];
            return;
        endif;

        $clients = $r->clients;
        $clients[] = $user_id;

        $r->clients = $clients;
        $r->save();

    }


    /**
    *
    * Add a custom non registered client to restaurant 
    * 
    * If is creating user will add added_by_restaurant = 1
    * 
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response 
    * **/
    public function addCustomClient(Request $request){
        $this->validate($request,[
            'restaurant_id'=>'required|alpha_num',
            'name'=>'required|string',
            'surname'=>'required|string',
            'phone'=>'string',
            'phone_'=>'string',
            'email'=>'email',
            "address" => 'string',
            "cap" => 'string',
            "city" => 'string',
            "pr" => 'string',
            "state" => 'string',
            "dt" => 'string',
            "language" => 'string',
            "allergeni" => 'array',
            "preferenze" => 'array',
            "note" => ''
        ]);

        
        $r = Restaurant::find($request->input('restaurant_id'));
        
        if($request->input('email')) {
            
            $u = User::where('email',$request->input('email'))->get();
        
            if(count($u) <= 0):
                
                $u = new User();
                $u->name = strip_tags($request->input('name'));
                $u->surname = strip_tags($request->input('surname'));
                $u->phone = ($request->input('phone')) ? $request->input('phone') : null;
                $u->email = $request->input('email');
                $u->added_by_restaurant = 1;
                $u->save();

            else:
                $just_added = $r->clients()->where('_id', $u[0]->_id)->get();
                if(count($just_added) > 0) return response()->json(['message'=>'ok','user'=>$u[0]],200);
                $u = $u[0];
            endif;

        } 

        $client = new RestaurantClient();
        $client->name = isset($u) ? $u->name : $request->input('name');
        $client->surname =  isset($u) ? $u->surname : $request->input('surname');
        $client->phone = ($request->input('phone')) ? $request->input('phone') : null;
        $client->phone_ = ($request->input('phone_')) ? $request->input('phone_') : null;
        $client->email = ($request->input('email')) ? $request->input('email') : null;


        $client->address  = ($request->input('address')) ? $request->input('address') : null;
        $client->cap  = ($request->input('cap')) ? $request->input('cap') : null;
        $client->city  = ($request->input('city')) ? $request->input('city') : null;
        $client->pr  = ($request->input('pr')) ? $request->input('pr') : null;
        $client->state  = ($request->input('state')) ? $request->input('state') : null;
        $client->dt  = ($request->input('dt')) ? $request->input('dt') : null;
        $client->language  = ($request->input('language')) ? $request->input('language') : null;
        
        $allergie = [];
        if($request->input('allergeni')){
            foreach ($request->input('allergeni') as $allergene) {
                $allergie[] = $allergene;
            }
        }

        $preferenze = [];
        if($request->input('preferenze')){
            foreach ($request->input('preferenze') as $preferenza) {
                $preferenze[] = $preferenza;
            }
        }

        $client->allergie = $allergie;
        $client->preferenze = $preferenze;
        $client->note = ($request->input('note')) ? $request->input('note') : null;

        $client->user_id = (isset($u)) ? $u->_id : null;
        $client->restaurant_id = $r->_id;

        // informazioni extra

        $client->save();

        $client->user = $client->user()->first();

        return response()->json(['message'=>'ok','client'=>$client],200);

    }

    /**
    *
    * Edit a custom non registered client to restaurant 
    * 
    * If is creating user will add added_by_restaurant = 1
    * 
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response 
    * **/
    public function editCustomClient(Request $request){
        $this->validate($request,[
            'restaurant_id'=>'required|alpha_num',
            'client_id'=>'required|alpha_num',
            'name'=>'required|string',
            'surname'=>'required|string',
            'phone'=>'string',
            'phone_'=>'string',
            'email'=>'email',
            "address" => 'string',
            "cap" => 'string',
            "city" => 'string',
            "pr" => 'string',
            "state" => 'string',
            "dt" => 'string',
            "language" => 'string',
            "allergie" => 'array',
            "preferenze" => 'array',
            "note" => ''
        ]);

        
        $r = Restaurant::find($request->input('restaurant_id'));                
        $client = RestaurantClient::find($request->input('client_id'));
        
        if($client->restaurant_id != $r->_id) return response()->json(['message'=>'error'], 403);
        
        $client->name = $request->input('name');
        $client->surname = $request->input('surname');
        $client->phone = ($request->input('phone')) ? $request->input('phone') : null;
        $client->phone_ = ($request->input('phone_')) ? $request->input('phone_') : null;
        $client->email = ($request->input('email')) ? $request->input('email') : null;


        $client->address  = ($request->input('address')) ? $request->input('address') : null;
        $client->cap  = ($request->input('cap')) ? $request->input('cap') : null;
        $client->pr  = ($request->input('pr')) ? $request->input('pr') : null;
        $client->city  = ($request->input('city')) ? $request->input('city') : null;
        $client->state  = ($request->input('state')) ? $request->input('state') : null;
        $client->dt  = ($request->input('dt')) ? $request->input('dt') : null;
        $client->language  = ($request->input('language')) ? $request->input('language') : null;
        
        $allergie = [];
        if($request->input('allergie')){
            foreach ($request->input('allergie') as $allergene) {
                $allergie[] = $allergene;
            }
        }

        $preferenze = [];
        if($request->input('preferenze')){
            foreach ($request->input('preferenze') as $preferenza) {
                $preferenze[] = $preferenza;
            }
        }

        $client->allergie = $allergie;
        $client->preferenze = $preferenze;
        $client->note = ($request->input('note')) ? $request->input('note') : null;

        
        // informazioni extra

        $client->save();

        return response()->json(['message'=>'ok'],200);

    }

    /**
    *
    * delete a custom non registered client to restaurant 
    * 
    * If is creating user will add added_by_restaurant = 1
    * 
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response 
    * **/
    public function deleteCustomClient(Request $request){
        $this->validate($request,[
            'restaurant_id'=>'required|alpha_num',
            'client_id'=>'required|alpha_num'
        ]);

        
        $r = Restaurant::find($request->input('restaurant_id'));                
        $client = RestaurantClient::find($request->input('client_id'));
        
        if($client->restaurant_id != $r->_id) return response()->json(['message'=>'error'], 403);
        
        $client->delete();

        return response()->json(['message'=>'ok'],200);

    }

    /**
    *
    * Get restaurant clients
    * 
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response 
    * **/
    public function getRestaurantClients(Request $request){
        $this->validate($request,[
            'restaurant_id'=>'required|alpha_num'
        ]);

        $r = Restaurant::find($request->input('restaurant_id'));                 
        return response()->json(['message'=>'ok','clients'=>$r->clients()->with('user')->orderBy('name', 'DESC')->orderBy('surname', 'DESC')->get()],200);
    }


    /**
    *
    * Get client schede
    * 
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response 
    * **/
    public function getClientInfo(Request $request){
        $this->validate($request,[
            'restaurant_id'=>'required|alpha_num',
            'client_id'=>'required|alpha_num'
        ]);
        $u = User::find($request->input('client_id'));
        if(!$u) {
            return response()->json(['message'=>'ok','infos'=>[
                    'reccomended'=>0, 
                    'reservations'=>0,
                    'preorders'=>0,
                    'confirmed_reservations'=>0,
                    'discarded_reservations'=>0,
                    'last'=>''
                ]],200);
        }

        $res = ReservationRequest::where('restaurant',$request->input('restaurant_id'))
        ->where('user',$request->input('client_id'))
        ->orderBy('date','desc')
        ->orderBy('time','desc')
        ->get();

        $n_p = 0;
        $n_pc = 0;
        $n_pnc = 0;
        $last = '-';
        $current = 0;
        $preorders = 0;
        foreach ($res as $r) :
            $n_p++;
            if(isset($r->preorder)) $preorders++;
            if($r->assigned_foodcoin) :
                if($r->removed_foodcoin):
                    $n_pnc++;
                else:
                    $n_pc++;                    
                endif;
            endif;

            
            if($current == 0) $last = $r->date->format('d/m/Y').' '.$r->time;
            $current++;

        endforeach;
        $raccomandato = Recommendation::where('user',$request->input('client_id'))->where('ref',$request->input('restaurant_id'))->where('type',0)->get();
        $ret_rac = (count($raccomandato) <= 0) ? 0 : 1;

        $return_data = [
            'reccomended'=>$ret_rac, // 0 no, 1 si
            'reservations'=>$n_p,
            'preorders'=>$preorders,
            'confirmed_reservations'=>$n_pc,
            'discarded_reservations'=>$n_pnc,
            'last'=>$last
        ];

        return response()->json(['message'=>'ok','infos'=>$return_data],200);
    }

}
