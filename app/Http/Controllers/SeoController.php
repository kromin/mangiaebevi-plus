<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;
use Jenssegers\Mongodb\Model as Eloquent;

use Route;
use Log;
use MangiaeBevi\Restaurant;
use MangiaeBevi\User;
use Storage;
use File;
use Auth;

use MangiaeBevi\Services;
use MangiaeBevi\TipoLocale;
use MangiaeBevi\Types;
use MangiaeBevi\Regions;
use MangiaeBevi\CreditCard;

class SeoController extends Controller
{
    
    public static function returnHeaderVars(){
        return;
        //Log::info('restituisco seo variabili: ' .Route::getCurrentRoute()->getPath());
        //Log::info('restituisco url: ' .'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
        $param = Route::current()->parameters();
        $arr = [
            'title'=>'Pagina non trovata | Menoo',
            'description'=>'La pagina richiesta non è stata trovata',
            'image'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'type'=>'food',
            'url'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').urlencode(Route::current()->getName())
            ];
        
        if(isset($param['restaurant_slug'])):
            //Log::info('mostro un ristorante: '.$param['restaurant_slug']);
            $restaurants = Restaurant::where('slug',$param['restaurant_slug'])->get();

            if(isset($restaurants[0])):                
                $arr = [
                'title'=>htmlentities ( $restaurants[0]->name, ENT_COMPAT) . ' | Menoo',
                'description'=>$restaurants[0]->seodescription,
                'image'=>($restaurants[0]->image) ? env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/restaurant/big/'.$restaurants[0]->image.'.png' : '',
                'type'=>'restaurant',
                'url'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/attivita/'.$restaurants[0]->slug
            ];
            endif;
        
        elseif(isset($param['city_name'])):
            $add = (isset($param['page'])) ? ' | Pagina '.intval($param['page']) : '';
            $arr = [
                'title'=>'Ristoranti a ' . htmlentities ( $param['city_name'], ENT_COMPAT) . $add . ' | Menoo',
                'description'=>'Elenco dei ristoranti su enjoymenoo.com nella città '. htmlentities ( $param['city_name'], ENT_COMPAT),
                'image'=> env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
                'type'=>'food',
                'url'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/citta/'.$param['city_name'].'/'
            ];
            
        elseif(isset($param['querystring'])):            
            $add = (isset($param['page'])) ? ' | Pagina '.intval($param['page']) : ''; 
            $arr = [
            'title'=>'Risultati della ricerca ' .htmlentities ( $param['querystring'], ENT_COMPAT). $add . ' | Menoo',
            'description'=>'Elenco dei ristoranti su enjoymenoo.com per la ricerca '.htmlentities ( $param['querystring'], ENT_COMPAT),
            'image'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'type'=>'food',
            'url'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/s/'.urlencode( htmlentities ( $param['querystring'], ENT_COMPAT) )
            ];

        elseif(isset($param['advanced'])):            
            $add = (isset($param['page'])) ? ' | Pagina '.intval($param['page']) : ''; 
            $arr = [
            'title'=>'Risultati della ricerca avanzata '. $add . ' | Menoo',
            'description'=>'Elenco dei ristoranti su enjoymenoo.com per la ricerca avanzata',
            'image'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'type'=>'food',
            'url'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/ricerca-avanzata/'.urlencode( $param['advanced'] ).'/'
            ];

        elseif(Route::is('/profile')):
            
            $arr = [
            'title'=>'Il tuo profilo | Menoo',
            'description'=>'La pagina del tuo profilo',
            'image'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'type'=>'food',
            'url'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/profile'
            ];

        elseif(isset($param['user_id'])):
            $u = User::find($param['user_id']);
            if($u):
                $arr = [
                'title'=>'Pagina del profilo di '.htmlentities ( $u->name, ENT_COMPAT).' '. htmlentities ( $u->surname, ENT_COMPAT) .' | Menoo',
                'description'=>'La pagina del profilo su MangieBevi di '.htmlentities ( $u->name, ENT_COMPAT).' '. htmlentities ( $u->surname, ENT_COMPAT),
                'image'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
                'type'=>'food',
                'url'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/user/'.$param['user_id']
                ];
            endif;

        else:
            $statics = [            
            // la home diventa con 3 slash
            '///'=>[
                'Menoo','/','/img/homebg.jpg','/img/homebg.jpg','food','Menoo, la guida per il palato, cerca tra centinaia di ristoranti, scopri la cucina, i menu, gli orari e prenota comodamente online'
                ],
            '/lavora-con-noi/'=>[
                'Lavora con noi | Menoo','/lavora-con-noi/','/img/pages/business.jpg','/img/pages/business.jpg','','Lavora con noi, entra nel team di Menoo, consulta le posizioni aperte ed inviaci il tuo curriculum'
                ],
            '/chi-siamo/'=>[
                'Chi siamo | Menoo','/chi-siamo/','/img/pages/chi_siamo.jpg','/img/pages/chi_siamo.jpg','','Leggi la storia e l\'evoluzione di Menoo'
                ],
            '/contattaci/'=>[
                'Contattaci | Menoo','/contattaci/','/img/contact.jpg','/img/contact.jpg','','Contatta il supporto di Menoo per informazioni commerciali e assistenza'
                ],
            '/come-funziona/'=>[
                'Come funziona | Menoo','/come-funziona/','/img/pages/cibo.jpg','/img/pages/cibo.jpg','','Leggi come funziona Menoo, cerca un ristorante o prenota al tuo ristorante preferito'
                ],
            '/promuovi-il-tuo-ristorante/'=>[
                'Promuovi il tuo ristorante | Menoo','/promuovi-il-tuo-ristorante/','/img/pages/ristoratori.jpg','/img/pages/ristoratori.jpg','','Promuovi il tuo ristorante con Menoo, fidelizza i tuoi clienti e gestisci le prenotazioni della tua attività'
                ],
            '/termini-e-condizioni/'=>[
                'Termini e condizioni | Menoo','/termini-e-condizioni/','','','','Menoo, termini e condizioni'
                ],
            '/privacy-policy/'=>[
                'Privacy policy | Menoo','/privacy-policy/','','','','Menoo, privacy e cookie policy'
                ],
            '/aggiungi-un-ristorante/'=>[
                'Aggiungi un ristorante | Menoo','/aggiungi-ristorante/','/img/pages/ristoratore.png','/img/pages/ristoratore.png','food','Aggiungi il tuo ristorante su Menoo, la guida per il palato'
                ],
            '/programma-fedelta/'=>[
                'Programma fedeltà | Menoo','/programma-fedelta/','/img/pages/fidelity.jpg','/img/pages/fidelity.jpg','','A breve online il programma fedeltà di Menoo, la guida per il palato'
                ],
            ];
            $route_name = (isset($param['any'])) ? '/'.$param['any'].'/' : '/'.Route::getCurrentRoute()->getPath().'/';
            //Log::info('route name '.$route_name);
            $key = false;
            $n = 0;
            foreach ($statics as $s => $arr) {
                $n++;
                if($s == $route_name) $key = $n;                
            }
            //Log::info($key);
            if($key):
                //Log::info('route trovata');
                $arr = [
                'title'=>$statics[$route_name][0],
                'description'=>htmlentities ( $statics[$route_name][5], ENT_COMPAT),
                'image'=>($statics[$route_name][2] != '') ? env('APP_PROTOCOL').'://'.env('APP_DOMAIN').$statics[$route_name][2] : '',
                'type'=>'food',
                'url'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').$statics[$route_name][1]
                ];

            else:
                $arr = [
                'title'=>'Pagina non trovata | Menoo',
                'description'=>'La pagina richiesta non è stata trovata',
                'image'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
                'type'=>'food',
                'url'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').urlencode(Route::current()->getName())
                ];
            endif;

            
        endif;
        return $arr;
    }


    /**
    *
    * Return variables for lds settings
    * @param string restaurant slug
    * @return array 0 restaurant object, 1 restaurant infos
    * */
    public function returnRestaurantLDS(Request $request){
        
        $restaurant = $request->input('restaurant');        
        
        $restaurant = Restaurant::find($request->input('restaurant'));
        if($restaurant):            
                $restaurant_infos = self::ReturnRestaurantInfos($restaurant);


                $address = [
                $restaurant->address,
                (isset($restaurant->location[2])) ? $restaurant->location[2] : '', 
                (isset($restaurant->location[4])) ? $restaurant->location[4] : '', 
                (isset($restaurant->location[count($restaurant->location)-1])) ? $restaurant->location[count($restaurant->location)-1] : '', 
                ];

                $seo_return = '
            {
              "@context":"http://schema.org",
              "@type":"Restaurant",
              "@id":"'. env('APP_PROTOCOL') .'://'.env('APP_DOMAIN').'/attivita/'.$restaurant->slug.'",
              "name":"'.$restaurant->name.'",
              "address":{
                "@type":"PostalAddress",
                "streetAddress":"'.$address[0].'",
                "addressLocality":"'.$address[1].'",
                "addressRegion":"'.$address[2].'",
                "postalCode":"'.$address[3].'",
                "addressCountry":"IT"
              },
              "geo":{
                "@type":"GeoCoordinates",
                "latitude":'.$restaurant->loc['coordinates'][1].',
                "longitude":'.$restaurant->loc['coordinates'][0].'
              },';
               
              $default_euro_price = "";
                $n = 0;
                if($restaurant->price < 20){
                    $n = 1;
                } if($restaurant->price >= 20 && $restaurant->price < 40){
                    $n = 2;
                } elseif
                ($restaurant->price >= 40 && $restaurant->price < 80){
                    $n = 3;
                } elseif($restaurant->price >= 80 && $restaurant->price < 150){
                    $n = 4;
                } elseif($restaurant->price > 150){
                    $n = 5;
                } 
              for($p = 0;$p < $n; $p++){
                $default_euro_price .= "€";
              }
              $seo_return .= '
              "priceRange": "'.$default_euro_price.'",
              "servesCuisine": [
                '; 
                $n = 0;
                foreach ($restaurant_infos['tipiCucina'] as $tipo) :
                    if($n > 0) $seo_return .=  ',';
                    $seo_return .= '"'.$tipo.'"';
                    $n++;
                endforeach;
                ?>
                <?php 
                foreach ($restaurant_infos['regioniCucina'] as $tipo) :
                    if($n > 0) $seo_return .=  ',';
                    $seo_return .= '"'.$tipo.'"';
                    $n++;
                endforeach;
                $seo_return .= '
              ],
              ';
              if(isset($restaurant->contact['phone'][0]) && $restaurant->contact['phone'][0] != ''):
                $seo_return .= '
              "telephone":"'.$restaurant->contact['phone'][0].'",
                ';
                endif;
                $seo_return .= '
                "paymentAccepted": [
                "Cash",
                '; 
                $n = 0;
                foreach ($restaurant_infos['carte'] as $tipo) :
                    if($n > 0) $seo_return .=  ',';
                    $seo_return .=  '"'.$tipo.'"';
                    $n++;
                endforeach;
                $seo_return .= '
                ],    
                "openingHoursSpecification":[
                '; 
                $schema_days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
                $n = 0; $x = 0;
                if($restaurant->fasce):
                    foreach ($restaurant->fasce as $fascia) {
                      foreach ($fascia as $fascia_oraria) {
                        if($x > 0) $seo_return .=  ',';
                          
                          $seo_return .= '{
                            "@type": "OpeningHoursSpecification",
                            "dayOfWeek": [
                            "'.$schema_days[$n].'"
                            ],
                            "opens": "'.explode("-",$fascia_oraria)[0].'",
                            "closes": "'.explode("-",$fascia_oraria)[1].'"
                          }';
                          
                          $x++;
                      }
                      $n++;
                    }
                endif;
                $seo_return .= '],';
                if(isset($restaurant->contact['email']) && $restaurant->contact['email'] != ''):
                    $seo_return .= '
                    "acceptsReservations": true,
                    "potentialAction":{
                        "@type":"ReserveAction",
                        "target":{
                          "@type":"EntryPoint",
                          "urlTemplate":"'.env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/'.$restaurant->city.'/'.$restaurant->slug.'",
                          "inLanguage":"it-IT",
                          "actionPlatform":[
                            "http://schema.org/DesktopWebPlatform",
                            "http://schema.org/IOSPlatform",
                            "http://schema.org/AndroidPlatform"
                          ]
                        },
                        "result":{
                          "@type":"FoodEstablishmentReservation",
                          "name":"Prenota un tavolo"
                        }
                    }';
                else:
                $seo_return .= '
                "acceptsReservations": false';
                endif;
                if(isset($restaurant->menu['menus']) && count($restaurant->menu['menus']) > 0):        
                    $seo_return .= ',
                "menu": "'.env('APP_PROTOCOL').'://'. env('APP_DOMAIN') .'/'. $restaurant->slug.'"';
                endif;
                $seo_return .= '
            }';
         return response()->json(['message'=>'ok','seo'=>$seo_return],200);
        endif;
                
    }

    /**
    *
    * Return variables for lds settings
    * @param string restaurant slug
    * @return array 0 LDS, 1 restaurant infos
    * */
    public static function returnRestaurantLDSStatic(Restaurant $restaurant){
        
        if($restaurant):            
            $restaurant_infos = self::ReturnRestaurantInfos($restaurant);

            $address = [
            $restaurant->address,
            (isset($restaurant->location[2])) ? $restaurant->location[2] : '', 
            (isset($restaurant->location[4])) ? $restaurant->location[4] : '', 
            (isset($restaurant->location[count($restaurant->location)-1])) ? $restaurant->location[count($restaurant->location)-1] : '', 
            ];

            $i = ($restaurant->image && $restaurant->image != '') ? env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/restaurant/big/'.$restaurant->image.'.png' : env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg';
            
            $zip = ($restaurant->zip) ? $restaurant->zip : $address[3];
                $seo_return = '
            {
              "@context":"http://schema.org",
              "@type":"Restaurant",
              "@id":"'. env('APP_PROTOCOL') .'://'.env('APP_DOMAIN').'/'.urlencode($restaurant->city).'/'.$restaurant->slug.'/",
              "name":"'.remove_quotes_and_tags ( $restaurant->name).'",
              "image": "'.$i.'",
              "address":{
                "@type":"PostalAddress",
                "streetAddress":"'.remove_quotes_and_tags ( $address[0]).'",
                "addressLocality":"'.remove_quotes_and_tags ( $restaurant->city).'",
                "addressRegion":"'.remove_quotes_and_tags ( $address[2]).'",
                "postalCode":"'.$zip.'",
                "addressCountry":"IT"
              },
              "geo":{
                "@type":"GeoCoordinates",
                "latitude":'.floatval($restaurant->loc['coordinates'][1]).',
                "longitude":'.floatval($restaurant->loc['coordinates'][0]).'
              },';
               
              $default_euro_price = "";
                $n = 0;
                if($restaurant->price < 20){
                    $n = 1;
                } if($restaurant->price >= 20 && $restaurant->price < 40){
                    $n = 2;
                } elseif
                ($restaurant->price >= 40 && $restaurant->price < 80){
                    $n = 3;
                } elseif($restaurant->price >= 80 && $restaurant->price < 150){
                    $n = 4;
                } elseif($restaurant->price > 150){
                    $n = 5;
                } 
              for($p = 0;$p < $n; $p++){
                $default_euro_price .= "€";
              }
              
              $restaurant_infos['europrice'] = $default_euro_price;
              $seo_return .= '
              "priceRange": "'.$default_euro_price.'",
              "servesCuisine": [
                '; 
                $n = 0;
                foreach ($restaurant_infos['tipiCucina'] as $tipo) :
                    if($n > 0) $seo_return .=  ',';
                    $seo_return .= '"'.$tipo.'"';
                    $n++;
                endforeach;
                ?>
                <?php 
                foreach ($restaurant_infos['regioniCucina'] as $tipo) :
                    if($n > 0) $seo_return .=  ',';
                    $seo_return .= '"'.$tipo.'"';
                    $n++;
                endforeach;
                $seo_return .= '
              ],
              ';

              if(isset($restaurant->contact['phone'][0]) && $restaurant->contact['phone'][0] != '' && !empty($restaurant->contact['phone'][0])):
                $seo_return .= '
              "telephone":"'.$restaurant->contact['phone'][0].'",
                ';
                endif;
                $seo_return .= '
                "paymentAccepted": [
                "Cash",
                '; 

                $n = 0;
                foreach ($restaurant_infos['carte'] as $tipo) :
                    if($n > 0) $seo_return .=  ',';
                    $seo_return .=  '"'.$tipo.'"';
                    $n++;
                endforeach;

                $seo_return .= '
                ],    
                "openingHoursSpecification":[
                '; 
                $schema_days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
                $n = 0; $x = 0;

                if(isset($restaurant->fasce) && is_array($restaurant->fasce) && !empty($restaurant->fasce)):
                    foreach ($restaurant->fasce as $fascia) {
                      foreach ($fascia as $fascia_oraria) {
                        if($x > 0) $seo_return .=  ',';
                          
                          $seo_return .= '{
                            "@type": "OpeningHoursSpecification",
                            "dayOfWeek": [
                            "'.$schema_days[$n].'"
                            ],
                            "opens": "'.explode("-",$fascia_oraria)[0].'",
                            "closes": "'.explode("-",$fascia_oraria)[1].'"
                          }';
                          
                          $x++;
                      }
                      $n++;
                    }
                endif;

                $seo_return .= '],';
                if(isset($restaurant->contact['email']) && $restaurant->contact['email'] != ''):
                    $seo_return .= '
                    "acceptsReservations": true,
                    "potentialAction":{
                    "@type":"ReserveAction",
                        "target":{
                          "@type":"EntryPoint",
                          "urlTemplate":"'.env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/'.$restaurant->city.'/'.$restaurant->slug.'/",
                          "inLanguage":"it-IT",
                          "actionPlatform":[
                            "http://schema.org/DesktopWebPlatform",
                            "http://schema.org/IOSPlatform",
                            "http://schema.org/AndroidPlatform"
                          ]
                        },
                        "result":{
                          "@type":"FoodEstablishmentReservation",
                          "name":"Prenota un tavolo"
                        }
                    }';
                else:
                    $seo_return .= '
                    "acceptsReservations": false';
                endif;

                if(isset($restaurant->menu['menus']) && count($restaurant->menu['menus']) > 0):        
                    $seo_return .= ',
                "menu": "'.env('APP_PROTOCOL').'://'. env('APP_DOMAIN').'/'.$restaurant->city.'/'.$restaurant->slug.'"/';
                endif;
                $seo_return .= '
            }';
            
         return [$seo_return,$restaurant_infos];
        endif;
                
    }

    /**
    *
    * @deprecated
    * */
    public static function returnStaticPage($uri){
        

        $uri = str_replace("?_escaped_fragment_=", "", $uri);
        $uri = str_replace("?_escaped_fragment_", "", $uri);

        $param = Route::current()->parameters();
        if(isset($param['cookie']) && isset($param['restaurant_slug'])) $uri = '/attivita/'.$param['restaurant_slug'];//str_replace('/'.$param['cookie'], '', $uri);
        //Log::info(urlencode(env('APP_PROTOCOL').'://'.env('APP_DOMAIN').$uri));
        if(File::exists
            (storage_path('app/static/'.urlencode(env('APP_PROTOCOL').'://'.env('APP_DOMAIN').$uri)))
                ):
            //Log::info('url esistente '.$uri);
            $content = Storage::get('static/'.urlencode(env('APP_PROTOCOL').'://'.env('APP_DOMAIN').$uri));        
            return view('static/staticresource', ['content' => $content ]);
        else:
            //Log::info('url inesistente '.$uri);
        endif;

    }


    public static function ReturnRestaurantInfos($restaurant){
        
        $types       = TipoLocale::all();
        $typesc      = Types::all();
        $regions     = Regions::all();
        $services    = Services::all();
        $creditCards = CreditCard::all();

        $tipoLocale = [];
        if(count($restaurant->tipoLocale) > 0):
            foreach ($restaurant->tipoLocale as $value) {
                foreach ($types as $val) {
                    if($val->_id == $value):
                        $tipoLocale[] = $val->name;
                    endif;
                }
            }
        endif;

        $tipoCucina = [];
        if(count($restaurant->tipoCucina) > 0):
            foreach ($restaurant->tipoCucina as $value) {
                foreach ($typesc as $val) {
                    if($val->_id == $value):
                        $tipoCucina[] = $val->name;
                    endif;
                }
            }
        endif;

        $ristoServices = [];
        $ristoServicesIcon = [];
        if(count($restaurant->services) > 0):
            foreach ($restaurant->services as $value) {
                foreach ($services as $val) {
                    if(is_object($val) && $val->_id == $value):
                        $ristoServices[] = $val->name;
                    $ristoServicesIcon[] = $val->icon;
                    endif;
                }
            }
        endif;

        $regioniCucina = [];
        if(count($restaurant->regioneCucina) > 0):
            foreach ($restaurant->regioneCucina as $value) {
                foreach ($regions as $val) {
                    if($val->_id == $value):
                        $regioniCucina[] = $val->name;
                    endif;
                }
            }
        endif;

        $carte = [];
        $carteicon = [];
        if(count($restaurant->carte) > 0):
            foreach ($restaurant->carte as $value) {
                foreach ($creditCards as $val) {
                    if($val->_id == $value):
                        $carte[] = $val->name;
                        $carteicon[] = $val->icon;
                    endif;
                }
            }
        endif;

        return [
            'tipiLocale'=>$tipoLocale,
            'tipiCucina'=>$tipoCucina,
            'services'=>$ristoServices,
            'servicesicons'=>$ristoServicesIcon,
            'regioniCucina'=>$regioniCucina,
            'carte'=>$carte,
            'carteicons'=>$carteicon
        ];
    }


}
