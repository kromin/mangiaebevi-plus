<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;
use Jenssegers\Mongodb\Model as Eloquent;

use MangiaeBevi\TipoLocale;
use MangiaeBevi\Restaurant;
use MangiaeBevi\TemporaryRestaurant;

class TipoLocaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $types = TipoLocale::all();
        return response()->json(['message'=>'ok','tipiLocale'=>$types], 200);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        
        $type = new TipoLocale();
        $type->name = $request->input('name');
        $type->slug = str_slug($request->input('name'));
        $type->save();

        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'tipoLocale' => $type 
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);

        $type = TipoLocale::find($request->input('id'));

        if(!$type):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        
        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'tipoLocale' => $type 
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request, [
            'id'    => 'required|alpha_num',
            'name'  => 'required|string'
        ]);

        $type = TipoLocale::find($request->input('id'));
        if(!$type):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        $type->name = $request->input('name'); 
        $type->slug = str_slug($request->input('name'));       
        $type->save();
        
        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'tipoLocale' => $type
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);
        $type = TipoLocale::find($request->input('id'));
        if(!$type):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        $restaurants = Restaurant::whereIn('tipoLocale',[$request->input('id')])->get();
        foreach ($restaurants as $r) {
            $risto = Restaurant::find($r->_id);
            $tipi = [];
            foreach ($risto->tipoLocale as $tipo) {
                if($tipo != $request->input('id')){
                    $tipi[] = $tipo;
                }
            }
            $risto->tipoLocale = $tipi;
            $risto->save();
        }

        $type->delete();
        return response()->json(['message'=>'ok'], 200);
    }



    public function massiveFillTipoLocale()
    {
        $restaurants = Restaurant::all();
        $types = [];
        foreach($restaurants as $restaurant){
            if($restaurant->tipoLocale) :
                foreach($restaurant->tipoLocale as $type){
                    $types[] = $type;
                }
            endif;
        }

        $types = array_unique($types);

        foreach($types as $type){
            $addToCollection = new TipoLocale();
            $addToCollection->name = $type;
            $addToCollection->save();
        }
        return response()->json(['message'=>'ok'], 200);
    }

    public function massiveCleanRestaurantsTipoLocale()
    {

        $types = TipoLocale::all();

        $restaurants = TemporaryRestaurant::all();
        foreach($restaurants as $restaurant){
            // take restaurant types 
            $single_types = [];
            // per ogni servizio del ristorante
            if($restaurant->tipoLocale) :
                foreach($restaurant->tipoLocale as $type){
                    // vedi i servizi dell'app e per ognuno di loro
                    foreach($types as $single_type_collection){
                        // se il nome è uguale al servizio del ristorante
                        if($single_type_collection->name == $type){
                            // inseriscilo nell'array che aggiorna il ristorante
                            $single_types[] = $single_type_collection->_id;
                        }
                    }
                }
            endif;
            if(!empty($single_types)) :
                // aggiorniamo il singolo ristorante
                $this->updateRestaurantTipoLocale($restaurant->_id,$single_types);
            endif;
        }

        return response()->json(['message'=>'ok'], 200);
    }

    private function updateRestaurantTipoLocale($restaurant_id,$types)
    {
        $restaurant = Restaurant::find($restaurant_id);
        $restaurant->tipoLocale = $types;
        $restaurant->save();
    }

    /**
    *
    * Return tipi locale in json
    * 
    * @param  \Illuminate\Http\Request
    * @return \Illuminate\Http\Response
    * 
    * */
    public function returnLocali(Request $request){
        $tipiLocale = TipoLocale::where(1)->get(['name','slug']);

        return response()->json(['tipiLocale'=>$tipiLocale], 200);
    }

}
