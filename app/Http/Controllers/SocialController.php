<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\User;
use MangiaeBevi\Restaurant;
use MangiaeBevi\Plates;
use MangiaeBevi\UserPlate;
use MangiaeBevi\UserActions;
use Auth;

use MangiaeBevi\Http\Controllers\UserActionsController as UserActionStatic;
use MangiaeBevi\Http\Controllers\NotificationsController as Notification;

use Carbon\Carbon;

class SocialController extends Controller
{
    /**
     * Return if user is follower
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response following: 0|1|2 0 == no, 1 == si, 2 == non può
     */
    public function isFollowing (Request $request){
      $this->validate($request, [
            'user' => 'required|alpha_num'
      ]);

      if(!Auth::check()) return response()->json(['message'=>'ok','following'=>2],200);
      if(Auth::user()->_id == $request->input('user')) return response()->json(['message'=>'ok','following'=>2],200);

      $u = User::find($request->input('user'));
      if(!$u) return response()->json(['message'=>'ok','following'=>2],200);

      $is_following = (in_array(Auth::user()->_id, $u->followedby)) ? 1 : 0;
      return response()->json(['message'=>'ok','following'=>$is_following],200);
    }

    /**
     * Return if user is follower
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response can_add: 0|1|2 0 == no, 1 == si, 2 == non può
     */
    public function canAddToFriends (Request $request){
      $this->validate($request, [
            'user' => 'required|alpha_num'
      ]);

      if(!Auth::check()) return response()->json(['message'=>'ok','can_add'=>2],200);
      if(Auth::user()->_id == $request->input('user')) return response()->json(['message'=>'ok','can_add'=>2],200);

      $u = User::find($request->input('user'));
      if(!$u) return response()->json(['message'=>'ok','can_add'=>2],200);

      $can_add = (in_array(Auth::user()->_id, $u->friends) || in_array(Auth::user()->_id, $u->friend_request)) ? 0 : 1;
      return response()->json(['message'=>'ok','can_add'=>$can_add],200);
    }

    /**
     * Follow/Unfollow user
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response
     */    
    public function addRemoveFollowing(Request $request){
        $this->validate($request, [
            'user' => 'required|alpha_num'
        ]);
        $user = User::find(Auth::user()->_id);
        if(!$user):
          return response()->json(['message'=>'error.empty'], 200);
        endif;

        if(!$user->following):
          $user->following = [];
        endif;

        $followings = $user->following;
        if(in_array($request->input('user'), $followings)):
          // remove
          $index = array_search($request->input('user'), $followings);
          unset($followings[$index]);
          $user->following_num = ($user->following_num) ? intval($user->following_num) -1 : 0;

          $this->removeFollowedBy($request->input('user'));

        else:
          // add
            $followings[] = $request->input('user');
            $this->addFollowedBy($request->input('user'));

            $user->following_num = ($user->following_num) ? intval($user->following_num) +1 : 1;

            UserActionStatic::addUserAction(5,[$request->input('user')]);

            $parameters = [Auth::user()->_id,Auth::user()->name,Auth::user()->surname];
            Notification::addNotification(15,$parameters,$request->input('user'));
        endif;

        if(!isset($followings)) $followings = [];
        $user->following = $followings;
        
        $user->save();
        return response()->json(['message'=>'ok'], 200);
    }

    /**
     * Remove auth user from list of user followers
     *
     * @param  string $followed
     * @return bool true|false
     */    
    private function removeFollowedBy($followed){
        $u = User::find($followed);

        if(!$u) return false;
        
        $followers = $u->followedby;

        if(in_array(Auth::user()->_id, $followers)) unset($followers[array_search(Auth::user()->_id, $followers)]);
        $u->followedby = $followers;
        $u->followers_num = ($u->followers_num) ? intval($u->followers_num) - 1 : 0;

        $u->save();
        return true;
    }

    /**
     * Add auth user in the list of user followers
     *
     * @param  string $followed
     * @return bool true|false
     */    
    private function addFollowedBy($followed){
        $u = User::find($followed);

        if(!$u) return false;
        
        $followers = $u->followedby;

        $followers[] = Auth::user()->_id;
        $u->followedby = $followers;
        $u->followers_num = ($u->followers_num) ? intval($u->followers_num) + 1 : 1;

        $u->save();
        return true;
    }

    /**
     * Send a friend Request
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response
     */
    public function addFriendRequest(Request $request){
        $this->validate($request, [
            'user' => 'required|alpha_num'
        ]);
        $user = User::find($request->input('user')); // Auth::user()->_id
        if(!$user):
          return response()->json(['message'=>'error.empty'], 200);
        endif;

        if(!$user->friend_request):
          $user->friend_request = [];
        endif;

        if(!$user->friends):
          $user->friends = [];
        endif;

        $friend_request = array_merge( $user->friend_request, $user->friends);

        if(in_array(Auth::user()->_id, $friend_request)):
          // remove
          return response()->json(['message'=>'...'], 403);
        else:
          // add
            $friend_request = $user->friend_request;
            $friend_request[] = Auth::user()->_id;
        endif;
        $user->friend_request = $friend_request;
        $user->save();

        $parameters = [Auth::user()->_id,Auth::user()->name.' '.Auth::user()->surname];
        Notification::addNotification(18,$parameters,$request->input('user'));
        return response()->json(['message'=>'ok'], 200);
    }

    /**
     * Accept a friend request
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response
     */
    public function acceptFriendRequest(Request $request){
        $this->validate($request, [
            'user' => 'required|alpha_num'
        ]);
        $user = User::find(Auth::user()->_id);
        if(!$user) return response()->json(['message'=>'error.empty'], 200);
        if(!$user->friend_request) return response()->json(['message'=>'...'], 403);

        if(!$user->friends) $user->friends = [];
        $friends = $user->friends;
        
        $requests = $user->friend_request;
        if(in_array($request->input('user'), $requests)):
          // remove
          $index = array_search($request->input('user'), $requests);
          unset($requests[$index]);
        endif;
        
        $friends[] = $request->input('user');
        $user->friends = $friends;
        $user->friend_request = $requests;
        $user->friends_num = ($user->friends_num) ? intval($user->friends_num)+1 : 1;
        $user->save();

        // mettilo come amico all'altro
        $other = User::find($request->input('user'));
        if($other):
            $f = ($other->friends) ? $other->friends : [];
            $f[] = Auth::user()->_id;
            $other->friends = $f;
            $other->friends_num = ($other->friends_num) ? intval($other->friends_num)+1 : 1;
            $other->save();
            // aggiungilo per bacheche
            UserActionStatic::addUserAction(0,[$other->_id],$user->_id);
            UserActionStatic::addUserAction(0,[$user->_id],$other->_id);
        endif;

        $parameters = [Auth::user()->_id,Auth::user()->name.' '.Auth::user()->surname];
        Notification::addNotification(16,$parameters,$request->input('user'));


        return response()->json(['message'=>'ok'], 200);
    }

    /**
     * refuse a friend Request
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response
     */
    public function refuseFriendRequest(Request $request){
        $this->validate($request, [
            'user' => 'required|alpha_num'
        ]);
        $user = User::find(Auth::user()->_id);
        if(!$user):
          return response()->json(['message'=>'error.empty'], 200);
        endif;

        if(!$user->friend_request):
            return response()->json(['message'=>'...'], 403);
        endif;

        $requests = $user->friend_request;    
        if(in_array($request->input('user'), $requests)):
          // remove
          $index = array_search($request->input('user'), $requests);
          unset($requests[$index]);
        endif;
        
        $user->friend_request = $requests;
        $user->save();
        return response()->json(['message'=>'ok'], 200);
    }

    /**
     * refuse a friend Request
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response
     */
    public function removeFriend(Request $request){
        $this->validate($request, [
            'user' => 'required|alpha_num'
        ]);
        $user = User::find(Auth::user()->_id);
        if(!$user):
          return response()->json(['message'=>'error.empty'], 200);
        endif;

        if(!$user->friends):
            return response()->json(['message'=>'...'], 403);
        endif;

        $friends = $user->friends;    
        if(in_array($request->input('user'), $friends)):
          // remove
          $index = array_search($request->input('user'), $friends);
          unset($friends[$index]);

          // rimuovi l'amicizia dall'altro
          $remove_from_other = User::find($request->input('user'));
          if($remove_from_other):
            $other_friends = $remove_from_other->friends;
            $index = array_search(Auth::user()->_id, $other_friends);
            unset($other_friends[$index]);
            $remove_from_other->friends = $other_friends;
            $remove_from_other->save();
          endif;
        endif;
        
        $user->friends = $friends;
        $user->save();
        return response()->json(['message'=>'ok'], 200);
    }


    /**
     * view user friend
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response
     */
    public function viewFriends(Request $request){
        $this->validate($request, [
            'user' => 'required|alpha_num'
        ]);
        $user = User::find($request->input('user'));
        if(!$user):
          return response()->json(['message'=>'error.empty'], 200);
        endif;

        if(!$user->friends):
            return response()->json(['friends'=>[]], 200);
        endif;
        
        $friend_id = [];
        foreach ($user->friends as $friend) {
            $friend_id[] = $friend;
        }

        $display = User::whereIn('_id',$friend_id)->get(['name','surname','avatar']);
                
        return response()->json(['message'=>'ok','friends'=>$display], 200);
    }

    /**
     * display friend Request users
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response
     */
    public function viewFriendRequest(Request $request){
        $this->validate($request, [
            'user_id' => 'required|alpha_num'
        ]);
        $user = User::find($request->input('user_id'));

        if(!$user):
          return response()->json(['message'=>'error.empty'], 200);
        endif;

        if(!$user->friend_request):
            return response()->json(['friend_request'=>[]], 200);
        endif;
        
        $friend_id = [];
        foreach ($user->friend_request as $friend) {
            $friend_id[] = $friend;
        }

        $display = User::whereIn('_id',$friend_id)->get(['name','surname','avatar']);
                
        return response()->json(['message'=>'ok','friend_request'=>$display], 200);
    }


    /**
     * view user followers
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response
     */
    public function viewFollowers(Request $request){
        $this->validate($request, [
            'user' => 'required|alpha_num',
            'skip' => 'integer',
            'take' => 'required|integer|max:10'
        ]);
        $user = User::find($request->input('user'));
        if(!$user):
          return response()->json(['message'=>'error.empty'], 200);
        endif;

        if(!$user->followedby):
            return response()->json(['followers'=>[]], 200);
        endif;
        
        $return_followers = [];
        $skip = 0;
        $take = 0;
        foreach ($user->followedby as $follower) {
          $add = true;
          if($request->input('skip')):
              $skip++;
              if($skip <= $request->input('skip')):
                  $add = false;
              endif;
          endif;
          if($request->input('take') && $add):
              if($take >= $request->input('take')):
                  $add = false;
              endif;    
              $take++;            
          endif;
          if($add){
            $return_followers[] = $follower;
          }
            
        }

        $result = User::whereIn('_id',$return_followers)->get(['name','surname','avatar']);

        
        return response()->json(['message'=>'ok','followers'=>$result], 200);
    }


    /**
     * view user followers
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response
     */
    public function viewFollowing(Request $request){
        $this->validate($request, [
            'user' => 'required|alpha_num',
            'skip' => 'integer',
            'take' => 'required|integer|max:10'
        ]);
        $user = User::find($request->input('user'));
        if(!$user):
          return response()->json(['message'=>'error.empty'], 200);
        endif;

        if(!$user->following):
            return response()->json(['following'=>[]], 200);
        endif;
        
        $return_followers = [];
        $skip = 0;
        $take = 0;
        foreach ($user->following as $follower) {
          $add = true;
          if($request->input('skip')):
              $skip++;
              if($skip <= $request->input('skip')):
                  $add = false;
              endif;
          endif;
          if($request->input('take') && $add):
              if($take >= $request->input('take')):
                  $add = false;
              endif;    
              $take++;            
          endif;
          if($add){
            $return_followers[] = $follower;
          }
            
        }

        $result = User::whereIn('_id',$return_followers)->get(['name','surname','avatar']);

        
        return response()->json(['message'=>'ok','following'=>$result], 200);
    }

    /**
     * view user dashboard
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response
     */
    public function viewDashboard(Request $request){
        $this->validate($request, [
            'user' => 'required|alpha_num',
            'skip' => 'integer',
            'take' => 'required|integer|max:10'
        ]);

        // mostra la dashboard degli amici e dei followed se l'utente è lo stesso, altrimenti quella dell'utente
        $user_dashboard = false;
        if(Auth::check() && Auth::user()->_id == $request->input('user')) $user_dashboard = true;


        if($user_dashboard){

          $ids = [];
          $u = User::find(Auth::user()->_id);
          $ids = array_merge( $u->friends , $u->following );
          $ids = array_unique($ids);

          if($request->input('skip') && $request->input('skip') > 0){
            $q = UserActions::whereIn('user',$ids)->where('useradd','!=',Auth::user()->_id)->skip($request->input('skip'))->take($request->input('take'))->orderBy('created_at','desc')->get();
          } else {
            $q = UserActions::whereIn('user',$ids)->where('useradd','!=',Auth::user()->_id)->take($request->input('take'))->orderBy('created_at','desc')->get();
          }

        } else {
          // vedi azioni utente
          if($request->input('skip') && $request->input('skip') > 0){
            $q = UserActions::where('user',$request->input('user'))->skip($request->input('skip'))->take($request->input('take'))->orderBy('created_at','desc')->get();
          } else {
            $q = UserActions::where('user',$request->input('user'))->take($request->input('take'))->orderBy('created_at','desc')->get();
          }
        }

        $return = [];
        if(!$user_dashboard) {
          $u = User::find($request->input('user'));
          if($u) $user_obj = ['_id'=>$u->_id,'name'=>$u->name, 'surname'=>$u->surname,'avatar'=>$u->avatar];
        }
        // posso avere un piatto, un ristorante o un plateimage
        foreach ($q as $action) {
          if( isset($action['plateimage']) && $action['plateimage'] != ''){
            $up = UserPlate::find($action['plateimage']);
            if($up) $action['plateimage_obj'] = UserPlate::find($action['plateimage']);
          }
          if( isset($action['plate']) && $action['plate'] != ''){
            $p = Plates::find($action['plate']);
            if($p) $action['plate_obj'] = Plates::find($action['plate']);
          }
          if( isset($action['restaurant']) && $action['restaurant'] != ''){
            $r = Restaurant::find($action['restaurant']);
            if($r) $action['restaurant_obj'] = ['_id'=>$r->_id,'name'=>$r->name,'slug'=>$r->slug,'city'=>$r->city,'image'=>$r->image];
          }
          if( isset($action['useradd']) && $action['useradd'] != ''){
            $u = User::find($action['useradd']);
            if($u) $action['useradd_obj'] = ['_id'=>$u->_id,'name'=>$u->name, 'surname'=>$u->surname,'avatar'=>$u->avatar];
          }
          if($user_dashboard){
            // se sono io metto anche l'utente
            $u = User::find($action['user']);
            if($u) $action['user_obj'] = ['_id'=>$u->_id,'name'=>$u->name, 'surname'=>$u->surname,'avatar'=>$u->avatar];
          } else {
            $action['user_obj'] = (isset($user_obj)) ? $user_obj : null;
          }
          //$action['date'] = Carbon($action['created_at'])->toDateTimeStringFormat();
          $return[] = $action;
        }

        


        return response()->json([
          'message'=>'ok',
          'actions'=>$return
          ],200);

    }

}
