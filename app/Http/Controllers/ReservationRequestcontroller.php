<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\Http\Controllers\NotificationsController as Notification;
use MangiaeBevi\Http\Controllers\ClientController as RestaurantClients;
use MangiaeBevi\ReservationRequest;
use MangiaeBevi\Restaurant;
use MangiaeBevi\User;
use Carbon\Carbon;

use Mail;
use Auth;
use Log;

class ReservationRequestcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $reservations = ReservationRequest::all();
        return response()->json(['message'=>'ok','reservations'=>$reservations], 200);
    }

    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function viewSingle(Request $request)
    {
        //
        $this->validate($request, [
            'id'  => 'required|alpha_num'
        ]);

        $reservation = ReservationRequest::find($request->input('id'));
        if(!$reservation):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        $risto = Restaurant::find($reservation->restaurant);
        if(!$risto):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        if(Auth::user()->role < 3 && $reservation->user != Auth::user()->_id && $risto->user != Auth::user()->_id):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        // tutto ok
        return response()->json(['message'=>'ok','reservation'=>$reservation], 200);
    }

    /**
     * Mostra le mie prenotazioni
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function viewMine(Request $request)
    {
        $this->validate($request, [
            'skip'  => 'numeric',
            'take'  => 'numeric'
        ]);
        $reservations = ReservationRequest::where('user',Auth::user()->_id)->orderBy('date','desc')->get();


        $restaurants = [];
        foreach ($reservations as $reservation) {
            $restaurants[] = $reservation->restaurant;
        }
        $restaurants = array_unique($restaurants);

        $ristos = Restaurant::whereIn('_id',$restaurants)->get(['id','name','image','tipoLocale','loc','address','slug','city']);

        return response()->json(['message'=>'ok','reservations'=>$reservations,'restaurants'=>$ristos], 200);
    }

    /**
     * Vedi le richieste di prenotazione del ristorante divise per giorno
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function viewByDate(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'date' => 'required|date_format:Y-m-d'
        ]);

        $from = Carbon::createFromFormat('Y-m-d H:i', $request->input('date')." 00:00");
        $to = Carbon::createFromFormat('Y-m-d H:i', $request->input('date')." 24:00");

        $reservations = ReservationRequest::where('restaurant',$request->input('restaurant_id'))
                                            ->where('date','>=',$from)
                                            ->where('date','<=',$to)
                                            ->orderBy('date','desc')
                                            ->orderBy('fascia_oraria','asc')
                                            ->get();

        $restaurants = [];
        $users = [];
        foreach ($reservations as $reservation) {
            //$restaurants[] = $reservation->restaurant;
            if($reservation->user && $reservation->user != '') $users[] = $reservation->user;
        }
        //$restaurants = array_unique($restaurants);
        $users = array_unique($users);


        //$ristos = Restaurant::whereIn('_id',$restaurants)->get(['id','name','image','tipoLocale','loc','address','slug']);
        $usrs = User::whereIn('_id',$users)->get(['name','surname','avatar']);

        return response()->json(['message'=>'ok','reservations'=>$reservations,'from'=>$from,'to'=>$to,'users'=>$usrs/*,'restaurants'=>$ristos*/], 200);
    }


    /**
     * Prendi prenotazioni in un range di date
     * @param \Illuminate\Http\Request restaurant_id | date
     * @return \Illuminate\Http\Response
     */
    public function indexByFromToAndRestaurant(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id' => 'required|alpha_num',
            'from'=>'required|date_format:Y-m-d',
            'to'=>'required|date_format:Y-m-d'
        ]);

        $from = Carbon::createFromFormat('Y-m-d H:i', $request->input('from')." 00:00");
        $to = Carbon::createFromFormat('Y-m-d H:i', $request->input('to')." 24:00");

        $statusCode = 200;
        $response = [
            'message'=>'ok',
            'from'=>$from,
            'to'=>$to,
            'reservations'  => ReservationRequest::where('restaurant',$request->input('restaurant_id'))
                                    ->where('date','>=',$from)
                                    ->where('date','<=',$to)
                                    ->get()
        ];
        return response()->json($response, $statusCode);
    }



    /**
     * Richiesta di prenotazione al ristorante
     *
     * Ricordare il type = 'web' per prenotazioni dal sito
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function reserve(Request $request)
    {
        //
         $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'date' => 'required|date_format:Y-m-d',
            'time' => 'required|string',
            'email' => 'required|email',
            'people'=>'required|integer',
            'name'=>'required|string',
            'tel'=>'required|numeric'
        ]);
        $statusCode = 200;

        $risto = Restaurant::find($request->input('restaurant_id'));
        if(!$risto):
            return response()->json(['message'=>'error.empty'], $statusCode);
        endif;
        if(($risto->prenotazione == 0 || !$risto->prenotazione) && (!$risto->contact['email'] || $risto->contact['email'] == '')):
            return response()->json(['message'=>'error.noreservation'], $statusCode);
        endif;

        $reservation = new ReservationRequest();
        $reservation->restaurant = $risto->_id;
        $reservation->restaurant_name = $risto->name;
        $reservation->date = $request->input('date');//Carbon::createFromFormat('Y-m-d', $request->input('date'))->toDateString();
        $reservation->fascia_oraria = $request->input('time');
        //$reservation->time = $request->input('time');//Carbon::createFromFormat('Y-m-d', $request->input('time'))->toTimeString();
        $reservation->email = $request->input('email');
        $reservation->people = $request->input('people');
        $reservation->name = $request->input('name');
        $reservation->tel = $request->input('tel');
        $reservation->user = Auth::user()->_id;
        $reservation->unique_string = str_random(40); // aggiungi stringa per validazione
        $reservation->type = 'web';
        $reservation->confirmed = 0;


        RestaurantClients::addClientToRestaurant(Auth::user()->_id,$risto->_id);
        /**
        * Notifica proprietario ristorante, se non cliente invia mail per farlo registrare
        * 9 => nuova prenotazione                 [id utente, nome utente, id ristorante, nome ristorante, data, persone]
        **/
        $parameters = [Auth::user()->_id,Auth::user()->name.' '.Auth::user()->surname,$risto->_id,$risto->name,$request->input('date')." ".$request->input('time'), $reservation->people];
        Notification::addNotification(9,$parameters,0);
        if($risto->user && $risto->user != 0):
            Notification::addNotification(9,$parameters,$risto->user);
        endif;

        // invia mail a amministratore o proprietario del ristorante
        if($risto->contact['email']) :
            $contact_mail = (env('APP_ENV') == 'local') ? env('ADMIN_EMAIL') : $risto->contact['email'];

            $mail_data = [
            'ristoratore'=>$contact_mail,
            'name'=>$risto->name,
            'admin_mail'=>env('ADMIN_EMAIL'),
            'subject'=>'Nuova prenotazione',
            'restaurant'=>$risto->name,
            'detail'=>[$reservation->name,
                        $reservation->people,
                        $request->input('date')." ".$request->input('time'),
                        $request->input('tel')
                        ]
            ];

            Mail::send('email.reservation', ['mail_data' => $mail_data],
                function ($m) use ($mail_data) {
                $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
                $m->to($mail_data['ristoratore'], $mail_data['name'])->subject($mail_data['subject']);
            });

            Mail::send('email.reservation', ['mail_data' => $mail_data],
                function ($m) use ($mail_data) {
                $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
                $m->to($mail_data['admin_mail'], $mail_data['name'])->subject($mail_data['subject']);
            });
        endif;


        $reservation->save();

        return response()->json(['message'=>'ok','reservation'=>$reservation], $statusCode);
    }


    /**
     * Prenotazione aggiunta dal ristoratore manualmente
     *
     * Ricordare il type = 'phone' per prenotazioni dal sito
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function manualReservation(Request $request)
    {
        //
         $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'date' => 'required|date_format:Y-m-d',
            'time' => 'required|string',
            'email' => 'email',
            'people'=>'required|integer',
            'name'=>'required|string',
            'tel'=>'numeric'
        ]);
        $statusCode = 200;

        $risto = Restaurant::find($request->input('restaurant_id'));
        if(!$risto):
            return response()->json(['message'=>'error.empty'], $statusCode);
        endif;
        if(($risto->prenotazione == 0 || !$risto->prenotazione) && (!$risto->contact['email'] || $risto->contact['email'] == '')):
            return response()->json(['message'=>'error.noreservation'], $statusCode);
        endif;

        $reservation = new ReservationRequest();
        $reservation->restaurant = $risto->_id;
        $reservation->restaurant_name = $risto->name;
        $reservation->date = $request->input('date');
        $reservation->fascia_oraria = $request->input('time');

        $reservation->email = ($request->input('email')) ? $request->input('email') : '';
        $reservation->people = $request->input('people');
        $reservation->name = $request->input('name');
        $reservation->tel = ($request->input('tel')) ? $request->input('tel') : '';

        if($request->input('email')):
            $u = User::where('email',$request->input('email'))->get(['_id']);
            if(count($u) > 0):
                $u = $u[0];
            else:

                $u = new User();
                $u->name = strip_tags($request->input('name'));
                $u->surname = '';
                $u->phone = ($request->input('tel')) ? $request->input('tel') : '';
                $u->email = $request->input('email');
                $u->added_by_restaurant = 1;
                $u->save();

            endif;
            $reservation->user = $u->_id;
            RestaurantClients::addClientToRestaurant($u->_id,$risto->_id);
        endif;

        $reservation->unique_string = str_random(40); // aggiungi stringa per validazione
        $reservation->type = 'phone';
        $reservation->confirmed = 1;

        $reservation->save();

        return response()->json(['message'=>'ok','reservation'=>$reservation], $statusCode);
    }


    /**
     * Confirm reservation
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function confirm(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'id'  => 'required|alpha_num',
            'sala' => 'required|integer',
            'tavolo' => 'integer'
        ]);

        $r = ReservationRequest::find($request->input('id'));
        if(!$r):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        $r->confirmed = 1;
        $r->sala = $request->input('sala');
        $r->tavolo = $request->input('tavolo');

        $r->save();
        /**
        * Notifica cliente
        * [id prenotazione, id ristorante, nome ristorante, data]
        **/
        $risto = Restaurant::find($r->restaurant);
        $parameters = [$r->_id,$risto->_id,$risto->name,Carbon::parse($r->date)->toDateString().' '.$r->fascia_oraria];
        Notification::addNotification(11,$parameters,$r->user);
        /**
        *
        * @todo mail utente
        *
        * */

        return response()->json(['message'=>'ok','reservation'=>$r], 200);
    }

    /**
     * Cambia data alla prenotazione, confirmed == 2
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function changeDate(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'id'  => 'required|alpha_num',
            'date' => 'required|date_format:Y-m-d',
            'time' => 'required|string'
        ]);

        $r = ReservationRequest::find($request->input('id'));
        $old_date = Carbon::parse($r->date)->toDateString(). ' '.$r->fascia_oraria;
        $r->date = $request->input('date');// Carbon::createFromFormat('Y-m-d', $request->input('date'))->toDateString();
        $r->fascia_oraria = $request->input('time');
        $r->date_changed = 1;
        if(!$r->unique_string):
            $r->unique_string = str_random(40);
        endif;
        $r->confirmed = 2;
        /**
        * Notifica cliente
        * [id prenotazione, id ristorante, nome ristorante, vecchia data, nuova data]
        **/
        $risto = Restaurant::find($r->restaurant);
        $parameters = [$r->_id,$risto->_id,$risto->name,$old_date,Carbon::createFromFormat('Y-m-d', $request->input('date'))->toDateString()." ".$request->input('time')];
        Notification::addNotification(13,$parameters,$r->user);

        $r->save();
        /**
        *
        * @todo mail utente
        *
        * */
        return response()->json(['message'=>'ok','reservation'=>$r], 200);
    }

    /**
     * Approva il cambiamento di data, identifico il cambio con confirmed = 3
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function approveChangeDate(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'id'  => 'required|alpha_num',
            'string'  => 'required|string'
        ]);

        $r = ReservationRequest::find($request->input('id'));
        if(!$r || $r->unique_string != $request->input('string') || $r->user != Auth::user()->_id):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        $r->confirmed = 3;

        /**
        * Notifica proprietario ristorante
        * [id prenotazione, id ristorante, nome ristorante, data, nome utente]
        **/
        $risto = Restaurant::find($r->restaurant);
        $parameters = [$r->_id,$risto->_id,$risto->name,Carbon::parse($r->date)->toDateString().' '.$r->fascia_oraria,$r->name];
        Notification::addNotification(12,$parameters,$risto->user);
        /**
        *
        * @todo mail utente
        *
        * */
        $r->save();
        return response()->json(['message'=>'ok','reservation'=>$r], 200);
    }
    /**
     * Rifiuta il cambiamento di data, cancello la prenotazione
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function refuseChangeDate(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'id'  => 'required|alpha_num',
            'string'  => 'required|string'
        ]);

        $r = ReservationRequest::find($request->input('id'));
        if(!$r || $r->unique_string != $request->input('string') || $r->user != Auth::user()->_id):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        /**
        * Notifica proprietario ristorante
        * [id utente, nome utente]
        **/
        $risto = Restaurant::find($r->restaurant);
        $u = User::find(Auth::user()->_id);
        $parameters = [$u->_id,$u->name . ' '.$u->surname];
        Notification::addNotification(14,$parameters,$risto->user);
        /**
        *
        * @todo mail utente
        *
        * */
        $r->delete();
        return response()->json(['message'=>'ok'], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function setRemoved(Request $request)
    {
        //
        $this->validate($request, [
            'id'  => 'required|alpha_num'
        ]);

        $r = ReservationRequest::find($request->input('id'));
        if(!$r) return response()->json(['message'=>'error.empty'], 404);

        if(!Auth::check() || $r->confirmed == 1) return response()->json(['message'=>'error.nocan'], 404);

        $risto = Restaurant::find($r->restaurant);
        if(!$risto) return response()->json(['message'=>'error.empty'], 404);
        if(Auth::user()->role > 2 || Auth::user()->_id == $risto->user || Auth::user()->_id == $r->user) $r->confirmed = 4;

        $r->save();



        // invia email a seconda di chi ha annullato
        $u = User::find($r->user);
        $to_send = [];

        if($u && $u->email) $to_send[] = $u->email;
        if($r->email && $r->email != '') $to_send[] = $r->email;

        if(isset($risto->contact['email']) && env('APP_ENV') != 'local') $to_send[] = $risto->contact['email'];

        $to_send[] = env('ADMIN_EMAIL');

        foreach ($to_send as $mail_address) :

            $mail_data = [
            'to'=>$mail_address,
            'name'=>$risto->name,
            'subject'=>'Prenotazione annullata',
            'restaurant'=>$risto->name,
            'detail'=>[
                $r->name,
                $r->date->format('d/m/Y')." ".$r->fascia_oraria,
                $r->tel,
                $r->people
                ]
            ];

            Mail::send('email.reservationcancelled', ['mail_data' => $mail_data],
                function ($m) use ($mail_data) {
                $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
                $m->to($mail_data['to'], $mail_data['name'])->subject($mail_data['subject']);
            });
        endforeach;



        return response()->json(['message'=>'ok'], 200);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->validate($request, [
            'id'  => 'required|alpha_num'
        ]);

        $r = ReservationRequest::find($request->input('id'));
        if(!$r) return response()->json(['message'=>'error.empty'], 200);

        if(!Auth::check() || $r->confirmed == 1) return response()->json(['message'=>'error.nocan'], 200);

        $risto = $r->restaurant;
        if(Auth::user()->role > 2 || Auth::user()->_id == $risto->user || Auth::user()->_id == $r->user) $r->delete();

        return response()->json(['message'=>'ok'], 200);
    }



    /**
     * Prendi tutte le fasce del ristorante per un determinato giorno
     *
     * @deprecated
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function getRestaurantFasce(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'day'  => 'required|date_format:Y-m-d'
        ]);

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        if(!$restaurant) return response()->json(['message'=>'error.empty'], 200);

        $time_shift = ($restaurant->time_shift && $restaurant->time_shift != '') ? intval($restaurant->time_shift): 15;


        $day_fasce = [];
        for($hour = 0; $hour < 24; $hour++):
            $current_hour = ($hour < 10) ? "0".$hour : "".$hour;
            for($x = 0;$x < 60; $x+=$time_shift):
                $current_minute = ($x == 0) ? "00" : $x;
                $day_fasce[] = $current_hour.":".$current_minute;
            endfor;
        endfor;


        $dt = Carbon::parse($request->input('day'));

        $not_parsed = $dt->dayOfWeek;
        $weekday = ($not_parsed == 0) ? 6 : $not_parsed-1;

        $fasce = (isset($restaurant->fasce[$weekday])) ? $restaurant->fasce[$weekday] : null;
        $return_fasce = [];
        if($fasce):
            foreach ($fasce as $fascia) {
                $hour_f = explode(":",explode("-",$fascia)[0])[0];
                $minute_f = explode(":",explode("-",$fascia)[0])[1];
                $hour_s = explode(":",explode("-",$fascia)[1])[0];
                $minute_s = explode(":",explode("-",$fascia)[1])[1];

                $put_it = false;
                if($hour_s > $hour_f):

                    foreach ($day_fasce as $fascia_giornaliera) {
                        // start putting
                        if(explode(":",explode("-",$fascia_giornaliera)[0])[0] == $hour_f &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] == $minute_f) $put_it = true;

                        if($put_it) $return_fasce[] = $fascia_giornaliera;

                        if(explode(":",explode("-",$fascia_giornaliera)[0])[0] == $hour_s &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] == $minute_s) $put_it = false;
                        /*if( explode(":",explode("-",$fascia_giornaliera)[0])[0] > $hour_f &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[0] < $hour_f
                            ):

                        elseif(explode(":",explode("-",$fascia_giornaliera)[0])[0] == $hour_f ):

                        elseif(explode(":",explode("-",$fascia_giornaliera)[0])[0] == $hour_s ):

                        endif;*/
                    }
                else:
                    $current = 0;
                    foreach ($day_fasce as $fascia_giornaliera) {
                        $current++;
                        if(explode(":",explode("-",$fascia_giornaliera)[0])[0] == $hour_f &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] == $minute_f) $put_it = true;

                        if($put_it) $return_fasce[] = $fascia_giornaliera;

                        if($current == count($day_fasce)):
                            // ultima fascia del giorno
                            // riparto dall'inizio
                            foreach ($day_fasce as $fascia_giornaliera_da_capo) {

                                if($put_it) $return_fasce[] = $fascia_giornaliera_da_capo;
                                if(explode(":",explode("-",$fascia_giornaliera_da_capo)[0])[0] == $hour_s &&
                                explode(":",explode("-",$fascia_giornaliera_da_capo)[0])[1] == $minute_s) $put_it = false;
                            }
                        endif;

                    }
                endif;
                /*
                    $return_fasce[] = [
                    $hour_f,$minute_f,$hour_s,$minute_f
                    ];
                */
            }
        endif;


        return response()->json(['message'=>'ok','fasce'=>$return_fasce], 200);
    }

    /**
     * Prendi tutte le fasce del ristorante per un determinato giorno che sono disponibili, quindi filtrate a seconda delle prenotazioni
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function getRestaurantAvaibleFasce(Request $request)
    {
        //

        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'day'  => 'required|date_format:Y-m-d',
            'people' => 'integer'
        ]);

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        if(!$restaurant) return response()->json(['message'=>'error.empty'], 200);

        $time_shift = ($restaurant->time_shift && $restaurant->time_shift != '') ? intval($restaurant->time_shift): 15;


        $day_fasce = [];
        for($hour = 0; $hour < 24; $hour++):
            $current_hour = ($hour < 10) ? "0".$hour : "".$hour;
            for($x = 0;$x < 60; $x+=$time_shift):
                $current_minute = ($x == 0) ? "00" : $x;
                $day_fasce[] = $current_hour.":".$current_minute;
            endfor;
        endfor;


        $dt = Carbon::parse($request->input('day'));


        $from = Carbon::createFromFormat('Y-m-d H:i', $request->input('day')." 00:00");
        $to = Carbon::createFromFormat('Y-m-d H:i', $request->input('day')." 24:00");

        $reservations = ReservationRequest::where('restaurant',$request->input('restaurant_id'))
                                            ->where('date','>=',$from)
                                            ->where('date','<=',$to)
                                            ->orderBy('date','desc')
                                            ->get();


        // filtra in base alle sale
        $struttura = $restaurant->struttura;
        //if($restaurant->struttura && !empty($restaurant->struttura)):

        //endif;

        $not_parsed = $dt->dayOfWeek;
        $weekday = ($not_parsed == 0) ? 6 : $not_parsed-1;

        $fasce = (isset($restaurant->fasce[$weekday])) ? $restaurant->fasce[$weekday] : null;
        $return_fasce = [];
        if($fasce):
            foreach ($fasce as $fascia) {
                $hour_f = explode(":",explode("-",$fascia)[0])[0];
                $minute_f = explode(":",explode("-",$fascia)[0])[1];
                $hour_s = explode(":",explode("-",$fascia)[1])[0];
                $minute_s = explode(":",explode("-",$fascia)[1])[1];

                $put_it = false;

                if($hour_s > $hour_f):

                    foreach ($day_fasce as $fascia_giornaliera) {
                        // start putting

                        if(explode(":",explode("-",$fascia_giornaliera)[0])[0] >= $hour_f &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] >= $minute_f &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[0] <= $hour_s &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] <= $minute_s) {
                            $put_it = true;
                        }

                        if(explode(":",explode("-",$fascia_giornaliera)[0])[0] == $hour_f &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] == $minute_f) $put_it = true;

                        if($put_it &&
                            Carbon::createFromFormat('Y-m-d H:i', $request->input('day').' '.$fascia_giornaliera)
                            <= Carbon::now()
                            ) {
                            $put_it = false;
                        }

                            // verifica che ci siano tavoli liberi
                        if($put_it && $this->hasAvaibleTable($reservations,$fascia_giornaliera,$request->input('people'),$restaurant->struttura)):
                            $return_fasce[] = $fascia_giornaliera;
                        endif;

                        if(explode(":",explode("-",$fascia_giornaliera)[0])[0] == $hour_s &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] == $minute_s) $put_it = false;

                            //return response()->json(['fascia'=>$request->input('day').' '.$fascia_giornaliera],403);

                    }
                else:

                    $current = 0;
                    foreach ($day_fasce as $fascia_giornaliera) {
                        $current++;


                        if(explode(":",explode("-",$fascia_giornaliera)[0])[0] >= $hour_f &&
                            explode(":",explode("-",$fascia_giornaliera)[0])[1] >= $minute_f) {
                            $put_it = true;
                        }



                        if($put_it &&
                            Carbon::createFromFormat('Y-m-d H:i', $request->input('day').' '.$fascia_giornaliera)
                            >= Carbon::now()
                            ) $put_it = true;

                        if($put_it &&
                            Carbon::createFromFormat('Y-m-d H:i', $request->input('day').' '.$fascia_giornaliera)
                            <= Carbon::now()
                            ) $put_it = false;

                        if($put_it && $this->hasAvaibleTable($reservations,$fascia_giornaliera,$request->input('people'),$restaurant->struttura)) :
                            $return_fasce[] = $fascia_giornaliera;
                        endif;

                        if($current == count($day_fasce)):
                            // ultima fascia del giorno
                            // riparto dall'inizio
                            foreach ($day_fasce as $fascia_giornaliera_da_capo) {

                                if($put_it && $this->hasAvaibleTable($reservations,$fascia_giornaliera,$request->input('people'),$restaurant->struttura)) {
                                    $return_fasce[] = $fascia_giornaliera_da_capo;
                                }

                                if(explode(":",explode("-",$fascia_giornaliera_da_capo)[0])[0] == $hour_s &&
                                explode(":",explode("-",$fascia_giornaliera_da_capo)[0])[1] == $minute_s) {
                                    $put_it = false;
                                }
                            }
                        endif;

                    }
                endif;
            }
        endif;


        return response()->json(['message'=>'ok','fasce'=>$return_fasce], 200);
    }

    private function hasAvaibleTable($reservations,$fascia,$people,$struttura){
        // prendo le sale
        // il resto per ogni sala
        // prendo il numero di tavoli che hanno posto per quelle persone
        // mi segno i numeri e metto in un array il numero
        // prendo tutte le prenotazioni in quella fascia (in quella sala) e metto in un array il numero del tavolo
        // parsing dei tavoli, se il numero è associato a una prenotazione lo tolgo dal primo array (dei tavoli)

        /* Questa è una funzione della madonna, la lascio commentata che è meglio visto i chiari di luna che cambiamo ogni giorno

        $tables_array = [];
        $x = 0;
        if(empty($struttura['sale'])) return true;
        foreach ($struttura['sale'] as $sala) {
            $tables_array[$x] = [];
            foreach ($sala['tables'] as $table) {
                Log::info('posti: '.$table['posti'].' per '.$people.' persone');
                if($table['posti'] >= $people) $tables_array[$x][] = $table['num'];
            }

            $x++;
        }


        foreach ($reservations as $reservation) {

            if($reservation->confirmed == 1 && $reservation->fascia_oraria == $fascia){
                $i = 0;
                foreach ($tables_array as $sala) {
                    $tavolo_indice = array_search($reservation->tavolo, $sala);
                    unset($tables_array[$i][$tavolo_indice]);
                    $i++;
                }
            }
        }

        //Log::info($tables_array);

        foreach ($tables_array as $sala => $tavoli) {
            //Log::info(implode(", ", $sala));
            if(count($tavoli) > 0) {
                Log::info('fascia '.$fascia.' è ok per tavoli '.count($tavoli));
                return true;
            }
        }
        //Log::info('fascia '.$fascia.' non è ok');

        return false;
        */
        $avaible_posti = 0;
        if(empty($struttura['sale'])) return true;
        foreach ($struttura['sale'] as $sala) {
            $avaible_posti += (isset($sala['posti'])) ? intval($sala['posti']) : 0;
        }

        $total_reserved = 0; // totale posti riservati
        foreach ($reservations as $reservation)
            if($reservation->confirmed == 1 && $reservation->type == 'web' && $reservation->fascia_oraria == $fascia) $total_reserved += intval($reservation->people);

        if(($avaible_posti - $total_reserved) >= $people) return true;
        return false;
    }



    /**
     * Cambia data alla prenotazione, confirmed == 2
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function assignFoodcoin(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'id'  => 'required|alpha_num'
        ]);


        $r = ReservationRequest::find($request->input('id'));
        $foodcoin_to_add = env('FOODCOIN_RESERVATION');
        if(isset($r->preorder)) $foodcoin_to_add = env('FOODCOIN_RESERVATION_ORDER');
        if(isset($r->preorder) && isset($r->preorder['paid']) && $r->preorder['paid'] == 1) $foodcoin_to_add = env('FOODCOIN_RESERVATION_ORDER_PAYD');

        if($r->assigned_foodcoin != 1){
            $u = User::find($r->user);
            $u->foodcoin = $u->foodcoin+intval($foodcoin_to_add);
            $u->save();
            $r->assigned_foodcoin = 1;
            $r->save();
        } else {
            return response()->json(['message'=>'error.added'], 403);
        }

        return response()->json(['message'=>'ok','reservation'=>$r], 200);
    }

    /**
     * Cambia data alla prenotazione, confirmed == 2
     *
     * !!! user->unconfirmed_reservations per prenotazioni non confermate
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function removeFoodcoin(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'id'  => 'required|alpha_num'
        ]);

        $r = ReservationRequest::find($request->input('id'));
        $foodcoin_to_remove = env('FOODCOIN_RESERVATION_TOGGLE');
        if(isset($r->preorder)) $foodcoin_to_remove = env('FOODCOIN_RESERVATION_TOGGLE_ORDER');

        if($r->assigned_foodcoin != 1){
            $u = User::find($r->user);
            // reset dei foodcoin
            $unconfirmed = (!$u->unconfirmed_reservations) ? 1 : intval($u->unconfirmed_reservations)+1;
            if($unconfirmed == 3) $foodcoin_to_remove = $u->foodcoin;

            $u->foodcoin = ($u->foodcoin > intval($foodcoin_to_remove)) ? $u->foodcoin-intval($foodcoin_to_remove) : 0;
            $u->unconfirmed_reservations = (!$u->unconfirmed_reservations) ? 1 : intval($u->unconfirmed_reservations)+1;
            $u->save();
            $r->assigned_foodcoin = 1;
            $r->removed_foodcoin = 1;
            $r->save();
        } else {
            return response()->json(['message'=>'error.added'], 403);
        }

        return response()->json(['message'=>'ok','reservation'=>$r], 200);
    }
}
