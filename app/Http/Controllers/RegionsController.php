<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;
use Jenssegers\Mongodb\Model as Eloquent;

use MangiaeBevi\Regions;
use MangiaeBevi\Restaurant;
class RegionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $regions = Regions::all();
        return response()->json(['message'=>'ok','regions'=>$regions], 200);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string',
            'image' => 'string'
        ]);

        if($request->input('image')):
            $mime = getImageMimeType($request->input('image') );
            if( !$mime ) return response()->json(['message'=>'error.invalid'], 200);
            if( $mime != 'svg+xml') return response()->json(['message'=>'error.invalid'], 200);
        endif;

        $region = new Regions();
        $region->name = $request->input('name');
        $region->slug = str_slug($request->input('name'));
        if($request->input('image')) :
            $region->image = $request->input('image');
        endif;
        $region->save();

        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'region'=>$region 
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);

        $region = Regions::find($request->input('id'));
        if(!$region):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        
        $statusCode = 200;
        $response = [
          'message' => 'Ok',
          'region' => $region 
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request, [
            'id'    => 'required|alpha_num',
            'name'  => 'required|string',
            'image' => 'string'
        ]);

        $region = Regions::find($request->input('id'));

        if(!$region):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        
        $region->name = $request->input('name');
        $region->slug = str_slug($request->input('name'));
        if($request->input('image')) :
            $mime = getImageMimeType($request->input('image') );
            if( $mime && $mime == 'svg+xml' ) :        
                $region->image = $request->input('image');
            endif;
        endif;
        $region->save();
        
        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'region'=>$region
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);
        $region = Regions::findOrFail($request->input('id'));
        if(!$region):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        $restaurants = Restaurant::whereIn('regioneCucina',[$request->input('id')])->get();
        foreach ($restaurants as $r) {
            $risto = Restaurant::find($r->_id);
            $regioni = [];
            foreach ($risto->regioneCucina as $regione) {
                if($regione != $request->input('id')){
                    $regioni[] = $regione;
                }
            }
            $risto->regioneCucina = $regioni;
            $risto->save();
        }

        $region->delete();
        return response()->json(['message'=>'ok'], 200);
    }


    public function massiveFillRegions()
    {
        $restaurants = Restaurant::all();
        $regions = [];
        foreach($restaurants as $restaurant){
            if($restaurant->regioneCucina) :
                foreach($restaurant->regioneCucina as $region){
                    $regions[] = $region;
                }
            endif;
        }

        $regions = array_unique($regions);

        foreach($regions as $region){
            $addToCollection = new Regions();
            $addToCollection->name = $region;
            $addToCollection->save();
        }
        return response()->json(['message'=>'ok'], 200);
    }

    public function massiveCleanRestaurantsRegions()
    {

        $regions = Regions::all();

        $restaurants = Restaurant::all();
        foreach($restaurants as $restaurant){
            // take restaurant regions 
            $single_regions = [];
            // per ogni servizio del ristorante
            if($restaurant->regioneCucina) :
                foreach($restaurant->regioneCucina as $region){
                    // vedi i servizi dell'app e per ognuno di loro
                    foreach($regions as $single_region_collection){
                        // se il nome è uguale al servizio del ristorante
                        if($single_region_collection->name == $region){
                            // inseriscilo nell'array che aggiorna il ristorante
                            $single_regions[] = $single_region_collection->_id;
                        }
                    }
                }
            endif;
            if(!empty($single_regions)) :
                // aggiorniamo il singolo ristorante
                $this->updateRestaurantRegion($restaurant->_id,$single_regions);
            endif;
        }
        return response()->json(['message'=>'ok'], 200);
    }

    private function updateRestaurantRegion($restaurant_id,$regions)
    {
        $restaurant = Restaurant::find($restaurant_id);
        $restaurant->regioneCucina = $regions;
        $restaurant->save();
    }

    /**
    *
    * Return regioni cucina in json
    * 
    * @param  \Illuminate\Http\Request
    * @return \Illuminate\Http\Response
    * 
    * */
    public function returnRegioniCucine(Request $request){
        $regions = Regions::where(1)->get(['name','slug']);

        return response()->json(['regions'=>$regions], 200);
    }
}
