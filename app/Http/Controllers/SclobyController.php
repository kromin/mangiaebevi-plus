<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;
use Curl;

class SclobyController extends Controller
{
    
    /**
     * Prendi il code per prendere il token
     *
     * @param  alpha_num  $restaurant
     * @return View
     */
    public function getSclobyCode(Request $request)
    {
        $scloby_code = $request->input('code');
        // curl a https://login.scloby.com/accesstoken.php
        // client_id // the client id we assigned to your app
        // client_secret // the client secret we assigned to your app
        // redirect_uri // your redirect uri
        // code // code we sent to you in the callback
        // 
        $data = [
                
                    'client_id'=>env('SCLOBY_CLIENT'),
                    'client_secret'=>env('SCLOBY_SECRET'),
                    'redirect_uri'=>env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/scloby/',
                    'code'=>$scloby_code
                    
                ];


        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://login.scloby.com/accesstoken.php",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"client_id\"\r\n\r\n".$data['client_id']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"client_secret\"\r\n\r\n".$data['client_secret']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"redirect_uri\"\r\n\r\n".$data['redirect_uri']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"code\"\r\n\r\n".$data['code']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          $response = "cURL Error #:" . $err;
        } 
        echo $response;
    }

    
}
