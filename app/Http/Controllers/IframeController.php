<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\Iframe;
use View;
use Auth;
use Log;

class IframeController extends Controller
{

    /**
    *
    * Ritorna il file che genera l'iframe
    * prima usando il token vedi qual'è il ristorante corrispondente
    * passa il ristorante alla view che in questo caso è un file javascript
    * la view carica la pagina in iframe
    * 
    * @param $token
    * @return \Illuminate\Http\Response
    * 
    **/
    public function returnLoaderFile($token)
    {
        
        if($token == 'test'):
            $restaurant = '56c2e130bffebc28088b4678';
            $url = env('APP_DOMAIN');
            return View::make('loader')->with('restaurant', $restaurant)->with('url', $url)->with('test', true);
        endif;
        
        $restaurant = Iframe::where('token',$token)->firstOrFail();
        if(!$restaurant) return;

        return View::make('loader')->with('restaurant', $restaurant->restaurant)->with('url', $restaurant->url);
    }

    /**
    *
    * Ritorna il file che genera l'iframe del menu
    * prima usando il token vedi qual'è il ristorante corrispondente
    * passa il ristorante alla view che in questo caso è un file javascript
    * la view carica la pagina in iframe
    * 
    * @param $token
    * @return \Illuminate\Http\Response
    * 
    **/
    public function returnLoaderMenuFile($token)
    {
        
        if($token == 'test'):
            $restaurant = '56c2e130bffebc28088b4678';
            $url = env('APP_DOMAIN');
            return View::make('loader')->with('restaurant', $restaurant)->with('url', $url)->with('test', true)->with('type',1);
        endif;
        
        $restaurant = Iframe::where('token',$token)->firstOrFail();
        if(!$restaurant) return;

        return View::make('loader')->with('restaurant', $restaurant->restaurant)->with('url', $restaurant->url)->with('type',1);
    }


    /**
     * Display all iframes, only for administrator
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['message'=>'ok','iframes'=>Iframe::all()],200);
    }

    
    /**
     * Aggiungi un token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'url'=>'required|string',
            'restaurant_id'=>'required|alpha_num'
            ]);

        $token = str_random(40); // token da utilizzare per richiamare il frame
        $iframe = new Iframe();

        $iframe->token = $token;
        $iframe->restaurant = $request->input('restaurant_id');
        $iframe->url = [$request->input('url')];
        $iframe->user = Auth::user()->_id;
        $iframe->save();

        return response()->json(['message'=>'ok','iframe'=>$iframe],200);
    }

    /**
     * Aggiungi un url all'iframe
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeURL(Request $request)
    {
        //
        $this->validate($request,[
            'url'=>'required|string',
            'restaurant_id'=>'required|alpha_num',
            'iframe'=>'required|alpha_num'
            ]);

        $iframe = Iframe::find($request->input('iframe'));
        if(!$iframe) return response()->json(['message'=>'error.noframe'],200);
 
        $urls = $iframe->url;
        $urls[] = $request->input('url');
        $iframe->url = $urls;
        $iframe->save();

        return response()->json(['message'=>'ok','iframe'=>$iframe],200);
    }

    /**
     * Modifica un token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request,[
            'id'=>'required|string',
            'url'=>'required|string',
            'restaurant_id'=>'required|alpha_num'
        ]);
        
        $iframe = new Iframe();

        $iframe->restaurant = $request->input('restaurant_id');
        $iframe->url = $request->input('url');
        $iframe->save();

        return response()->json(['message'=>'ok','iframe'=>$iframe],200);
    }

    /**
     * Mostra i miei personali
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 'message','iframes'
     */
    public function returnMine(Request $request)
    {
        //       
        return response()->json(['message'=>'ok','iframes'=>Iframe::where('user',Auth::user()->_id)->get()],200);
    }

    /**
     * Mostra in base al ristorante
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 'message','iframes'
     */
    public function returnByRestaurant(Request $request)
    {
        //  
         $this->validate($request,[
            'restaurant_id'=>'required|alpha_num'
            ]);     
        return response()->json(['message'=>'ok','iframes'=>Iframe::where('restaurant',$request->input('restaurant_id'))->get()],200);
    }

    

    /**
     * Cancella token
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        //
        $this->validate($request,[
            'id'=>'required|alpha_num'
        ]);

        Iframe::find($request->input('id'))->delete();
        return response()->json(['message'=>'ok'],200);
    }

}