<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\Restaurant;
use MangiaeBevi\Plates;
use MangiaeBevi\User;
use MangiaeBevi\RecommendationRequest;

use Carbon\Carbon;
use Auth;
use MangiaeBevi\Http\Controllers\GoogleAddressController as Address;
use DB;
use Log;
use MangiaeBevi\Http\Controllers\NotificationsController as Notification;

class RecommendationRequestController extends Controller
{
    /**
     * Ask a recommendation
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function askRecommendationRequest(Request $request)
    {
        //
        $this->validate($request, [
            'address' => 'string',
            'text' => 'required|string',
            'type' => 'required|integer|min:0|max:1' // 0 ristorante, 1 piatto
        ]);

        $u = User::find(Auth::user()->_id);
        if(!$u) return response()->json(['message'=>'error.notfound'],404);

        if( ( !$u->loc || !$u->loc['coordinates'] ) && !$request->input('address') ) return response()->json(['message'=>'error.notvalid'],403);

        if($request->input('address')):
            
            $location = Address::returnLocation($request->input('address'));
            if(!$location) :
                return response()->json(['message'=>'error.notvalid'],403);
            else:
                $lat = doubleval($location[0]);
                $lng = doubleval($location[1]);
            endif;

        else:
            $lat = doubleval($u->loc['coordinates'][1]);
            $lng = doubleval($u->loc['coordinates'][0]);
        endif;

        $recommendation_request = new RecommendationRequest();
        $recommendation_request->user = Auth::user()->_id;
        $recommendation_request->loc = ['type'=>"Point","coordinates"=>[$lng,$lat]];
        $recommendation_request->address = ($request->input('address')) ? $request->input('address') : '';
        $recommendation_request->text = $request->input('text');
        $recommendation_request->type = $request->input('type');
        $recommendation_request->answers_num = 0;
        $recommendation_request->answers = [];
        $recommendation_request->save();

        return response()->json(['message'=>'ok','recommendation'=>$recommendation_request],200);

    }


    /**
     * Answer to request
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function answerRecommendationRequest(Request $request)
    {
        //
        $this->validate($request, [
        	'request'=>'required|alpha_num',
            'text' => 'required|string',
            'ref' => 'required|alpha_num' // 0 ristorante, 1 piatto
        ]);

        $u = User::find(Auth::user()->_id);
        if(!$u) return response()->json(['message'=>'error.notfound'],404);

        
        DB::collection('recommendation_request_collection')
        ->where('_id', $request->input('request'))
        ->push('answers', [
        	'ref' => $request->input('ref'), 
        	'text' => $request->input('text'),
        	'user' => Auth::user()->_id
        	]);

        $rec_request = RecommendationRequest::find($request->input('request'));
        switch($rec_request->type):
        	case 0:
                Restaurant::where('_id', $request->input('ref'))->increment('recommendations');
        	break;
        	case 1:
                Plates::where('_id', $request->input('ref'))->increment('recommendations');
        	break;
        endswitch;

        RecommendationRequest::where('_id', $request->input('request'))->increment('answers_num');

        //[id utente, nome utente, testo_richiesta_raccomandazione]
        $parameters = [
            Auth::user()->_id,
            Auth::user()->name.' '.Auth::user()->surname,
            $rec_request->text
            ];
        Notification::addNotification(19,$parameters,$rec_request->user);

        return response()->json(['message'=>'ok'],200);

    }


    /**
     * Answer to request
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function viewByUserRequests(Request $request)
    {
        //
        $this->validate($request, [
        	'user'=>'required|alpha_num'            
        ]);

        $r = RecommendationRequest::where('user',$request->input('user'))->orderBy('created_at','desc')->get(['text','type','answers_num','created_at','user']);
        $result = [];
        foreach ($r as $recommendation) {
            $recommendation['user_obj'] = User::where('_id',$recommendation['user'])->get(['name','surname','avatar'])[0];
            $result[] = $recommendation;
        }

        return response()->json(['message'=>'ok','recommendation'=>$result],200);

    }



    /**
     * Answer to request
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function viewNearRequests(Request $request)
    {
        //
        $this->validate($request, [
        	'lat'=>'required|numeric',
            'lng'=>'required|numeric',
            'skip'=>'integer',
            'limit'=>'integer'       
        ]);
        
        $id = (Auth::check()) ? [Auth::user()->_id] : [];
        $mongodb = DB::getMongoDB();
        $r = $mongodb->command(
        array(  'geoNear' => "recommendation_request_collection", // Put here the collection 
            'near' => array( // 
                'type' => "Point", 
                'coordinates' => array(doubleval($request->input('lng')), doubleval($request->input('lat')))
            ), 
            'spherical' => true, 
            'num'=>5000,
            'maxDistance' => 50000, // 5 km
            'query' => ['user'=> ['$nin'=>$id ] ]                     
        ));

        $return_results = [];
        $skip = 0;
        $limit = 0;

        if(isset($r['results'])):
	        foreach ($r['results'] as $result) {
	            $id = (array) $result['obj']['_id'];
	            $obj = [
	                '_id'=>$id['$id'],
	                'text'=>$result['obj']['text'],
	                'type'=>$result['obj']['type'],
	                'answers_num'=>$result['obj']['answers_num']
	            ];
	            // paginazione
	            $add = true;
	            if($request->input('skip')):
	                $skip++;
	                if($skip <= $request->input('skip')):
	                    $add = false;
	                endif;
	            endif;
	            if($request->input('limit') && $add):
	                if($limit >= $request->input('limit')):
	                    $add = false;
	                endif;    
	                $limit++;            
	            endif;
	            
	            if($add):
                    $u = User::where('_id',$result['obj']['user'])->get(['name','surname','avatar']);
                    $obj['user_obj'] = (isset($u[0])) ? $u[0] : [];
	                $return_results[] = $obj;
	            endif;
	        }
	    endif;

        return response()->json(['message'=>'ok','recommendation'=>$return_results],200);

    }


    /**
     * View request answers
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function viewAnswersRequests(Request $request)
    {
        //
        $this->validate($request, [
        	'request'=>'required|alpha_num',
        	'skip'=>'integer',
            'limit'=>'integer'             
        ]);

        $r = RecommendationRequest::find($request->input('request'));

        if(!$r) return response()->json(['message'=>'error.empty'],404);

        $return_results = [];
        $skip = 0;
        $limit = 0;
        $answers = array_reverse($r->answers);
        foreach ($answers as $result) {
            
            // paginazione
            $add = true;
            if($request->input('skip')):
                $skip++;
                if($skip <= $request->input('skip')):
                    $add = false;
                endif;
            endif;
            if($request->input('limit') && $add):
                if($limit >= $request->input('limit')):
                    $add = false;
                    break;
                endif;    
                $limit++;            
            endif;
        

            if($add):
                if($r->type == 1):
                    $p = Plates::where('_id',$result['ref'])->get(['name','image']); 
                    $o = (isset($p[0])) ? $p[0] : [];                    
                else:
                    $ri = Restaurant::where('_id',$result['ref'])->get(['name','image','address','slug','city']); 
                    $o = (isset($ri[0])) ? $ri[0] : [];
                endif;
                $u = User::where('_id',$result['user'])->get(['name','surname','avatar']);
                $result['user_obj'] = (isset($u[0])) ? $u[0] : [];
            	$result['ref_obj'] = $o;
                $return_results[] = $result;
            endif;
        }

        return response()->json(['message'=>'ok','answers'=>$return_results],200);

    }

}
