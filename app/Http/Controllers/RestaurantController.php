<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use Jenssegers\Mongodb\Model as Eloquent;
use DB;

use MangiaeBevi\Http\Controllers\ImagesController as SingleCityImage;
use MangiaeBevi\Http\Controllers\GoogleAddressController as Address;

use MangiaeBevi\Restaurant;
use MangiaeBevi\Plates;

use Storage;
use Auth;
use Curl;
use Log;

use MangiaeBevi\TipoLocale;
use MangiaeBevi\Types;
use MangiaeBevi\Regions;
use MangiaeBevi\Services;
use MangiaeBevi\User;

use Image; // intervention Image
use File;
use Input;
use MangiaeBevi\Http\Controllers\ImagesController as SingleImage;
use MangiaeBevi\Http\Controllers\NotificationsController as Notification;


use Validator;
class RestaurantController extends Controller
{

    /*
        public function __construct(Restaurant $restaurant)
        {
            $this->restaurant = $restaurant;
        }
    */
    protected $guide = ['gambero','michelin'];


    /**
     * Get all restaurants
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $restaurants = Restaurant::all();           
        return response()->json(['message'=>'ok','restaurants'=>$restaurants], 200);//,
        
    }

    /**
     * Get restaurants claimed by a user
     * @param \Illuminate\Http\Request user_id
     * @return \Illuminate\Http\Response message=>ok, restaurants=>array
     */
    public function getClaimedBy(Request $request)
    {
        //
        $this->validate($request, [
            'user_id' => 'required|alpha_num',
        ]);
        $restaurants = Restaurant::whereIn('claimed_by',[$request->input('user_id')])->get();
        
        return response()->json(['message'=>'ok', 'restaurants'=>$restaurants], 200);//,
        
    }

    /**
     * Get restaurant fasce
     * @param \Illuminate\Http\Request 
     * @return \Illuminate\Http\Response message=>ok, restaurants=>array
     */
    public function getFasce(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id' => 'required|alpha_num',
        ]);
        $restaurant = Restaurant::find($request->input('restaurant_id'));
        
        return response()->json(['message'=>'ok', 'fasce'=>$restaurant->fasce], 200);//,
        
    }

    /**
     * Get restaurants claimed by a user
     * 
     * @return \Illuminate\Http\Response message=>ok, restaurants=>array
     */
    public function getClaimedByMe(Request $request)
    {
        
        $restaurants = Restaurant::whereIn('claimed_by',[Auth::user()->_id])->get();
        
        return response()->json(['message'=>'ok', 'restaurants'=>$restaurants], 200);//,
        
    }


    /**
     * Get restaurants administrated by user
     * @param \Illuminate\Http\Request user_id
     * @return \Illuminate\Http\Response message=>ok, restaurants=>array
     */
    public function getByUser(Request $request)
    {
        //
        $this->validate($request, [
            'user_id' => 'required|alpha_num',
        ]);
        $restaurants = Restaurant::where('user',$request->input('user_id'))->get();
        
        return response()->json(['message'=>'ok', 'restaurants'=>$restaurants], 200);//,
        
    }



    /**
     * Claim a restaurant
     * @param \Illuminate\Http\Request restaurant_id
     * @return \Illuminate\Http\Response
     */
    public function claim(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id' => 'required|alpha_num',
        ]);
        $restaurant = Restaurant::find($request->input('restaurant_id'));
        if($restaurant->user && $restaurant->user != 0):
            return response()->json(['message'=>'error.nonpuoi'], 200);//
        endif; 

        if(!$restaurant->claimed_by):
            $restaurant->claimed_by = [Auth::user()->_id];
        else:
            $claimers = $restaurant->claimed_by;
            
            foreach ($claimers as $claimer) :
                if($claimer == Auth::user()->_id) return response()->json(['message'=>'ok'], 200);
            endforeach;

            $claimers[] = Auth::user()->_id;
            $restaurant->claimed_by = $claimers;
        endif; 
        $restaurant->save();

        /**
        *
        * Add notifica
        * 
        **/
        $parameters = [Auth::user()->_id,Auth::user()->name,$restaurant->_id,$restaurant->name];
        Notification::addNotification(4,$parameters,0);
        return response()->json(['message'=>'ok'], 200);//,
        
    }

    /**
     * Approve restaurant 
     * @param \Illuminate\Http\Request id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request){
        $this->validate($request, [
            'id' => 'required|alpha_num',
            'status' => 'required|integer'
        ]);
        $restaurant = Restaurant::find($request->input('id'));
        if(!$restaurant) return response()->json(['message'=>'error.empty'], 200);

        $restaurant->approved = $request->input('status');
        $restaurant->save();

        if($restaurant->user && $restaurant->user != '' && $restaurant->user != 0) :
            $parameters = [$restaurant->_id,$restaurant->name];
            Notification::addNotification(6,$parameters,$restaurant->user);
        endif;

        return response()->json(['message'=>'ok'], 200);

    }

    /**
     * Approve restaurant claim
     * @param \Illuminate\Http\Request restaurant_id user_id
     * @return \Illuminate\Http\Response
     */
    public function approveClaim(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id' => 'required|alpha_num',
            'user_id' => 'required|alpha_num'
        ]);
        $restaurant = Restaurant::find($request->input('restaurant_id'));
        if($restaurant->user && $restaurant->user != 0):
            return response()->json(['message'=>'error.nonpuoi'], 200);//
        endif; 

        $restaurant->user = $request->input('user_id');
        $restaurant->claimed_by = null;
        $restaurant->save();

        $u = User::find($request->input('user_id'));
        if($u->role < 2):
            $u->role = 2;
            $u->save();
        endif;

        /**
        *
        * Add notifica a utente
        * 
        **/
        $parameters = [$restaurant->_id,$restaurant->name];
        Notification::addNotification(5,$parameters,$restaurant->user);
        return response()->json(['message'=>'ok'], 200);//,
        
    }

    /**
     * Get restaurants claimed
     * @param \Illuminate\Http\Request restaurant_id user_id
     * @return \Illuminate\Http\Response
     */
    public function getClaimed(Request $request)
    {
        //
        
        $restaurants = Restaurant::whereNotNull('claimed_by')->get();
        return response()->json(['message'=>'ok','restaurants'=>$restaurants], 200);        
    }

    /**
     * Display a listing of the resource, retrieve restaurant name, address, geo coordinates and image.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_for_simple_results()
    {
        //
        $restaurants = Restaurant::all(['name','address','loc','image','services','tipoLocale','tipoCucina','regioneCucina','carte','approved']);        
        return response()->json(['message'=>'ok','restaurants'=>$restaurants], 200);//$restaurants        
    }


    /**
     * Display restaurant clients, retrieve restaurant name, address, geo coordinates and image.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_clients()
    {
        //
        $restaurants = Restaurant::whereNotNull('user')->get();        
        return response()->json(['message'=>'ok','restaurants'=>$restaurants], 200);//$restaurants        
    }

    /**
     * Display clients but only retrieve restaurant name, address, geo coordinates and image.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_filtered_clients()
    {
        //
        $restaurants = Restaurant::whereNotNull('user')->get(['name','address','loc','image']);        
        return response()->json(['message'=>'ok','restaurants'=>$restaurants], 200);//$restaurants        
    }

    /**
     * Display mine restaurants, retrieve only mine restaurants, if admin take all.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_mine()
    {
        
        $restaurants = Restaurant::where('user',Auth::user()->_id)->get();        
        return response()->json(['message'=>'ok','restaurants'=>$restaurants], 200);
                      
    }

    /**
     * Display a listing of the resource, retrieve only mine restaurants, if admin take all.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_mine_filtered()
    {
        //
        if(Auth::user()->role >= 4):
            $restaurants = Restaurant::all(['name','address','loc','image']);        
            return response()->json(['message'=>'ok','restaurants'=>$restaurants], 200); 
        else:
            $restaurants = Restaurant::where('user',Auth::user()->_id)->get(['name','address','loc','image']);        
            return response()->json(['message'=>'ok','restaurants'=>$restaurants], 200);
        endif;                
    }



    private function canAddToRestaurantFromInside($restaurant_client = 0)
    {
        return (Auth::user()->_id == $restaurant_client || Auth::user()->role >= 4) ? true : false;
    }
    
    /**
     * Add a restaurant
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(Auth::user()->role < 3):            
            $this->validate($request,[ 
                'name' => 'required|string',
                'description' => 'string',
                'tipoLocale' => 'array',
                'email'=>'required|email',
                'video'=>'url',
                'virtualTour'=>'url',
                'priceMin'=>'numeric',
                'priceMax'=>'numeric',
                'address'=>'required|string',
                'city'=>'required|string',
                'state'=>'required|string',
                'zip'=>'required|string',
                'staff'=> 'array',
                'social'=> 'array',
                'services'=> 'array',
                'tipoCucina'=> 'array',
                'regioneCucina'=> 'array',
                'carte'=> 'array',
                'fasce'=>'array'
            ]);
        endif;

        $restaurant = new Restaurant();
        
        $restaurant->name = strip_tags($request->input( 'name' ));
        

        $restaurant->slug = $this->returnRestaurantSlug($restaurant->name);


        $restaurant->description = strip_tags($request->input( 'description' ));
        $restaurant->seodescription = utf8_encode( substr( strip_tags( $restaurant->description ),0,130).'...');

        $restaurant->tipoLocale = $request->input( 'tipoLocale' );

        $contact = [];
        $contact = (object) $contact;
        if($request->input('web')) $contact->web = $this->escapeSingleUrl($request->input( 'web' ));
        if($request->input('email')) $contact->email = $this->escapeSingleEmail($request->input( 'email' ));
        
        $contact->phone = [];
        if(is_array($request->input( 'phone' )) && !empty($request->input( 'phone' ) )):
            foreach($request->input( 'phone' ) as $phone_number):
                $contact->phone[] = strip_tags($phone_number);
            endforeach;
        elseif(is_string($request->input( 'phone' )) && $request->input( 'phone' ) != ''):
            $contact->phone[] = strip_tags($request->input( 'phone' ));
        endif;
        $restaurant->contact = $contact;
        
        if($request->input('virtualTour')) $restaurant->virtualTour = $this->escapeSingleUrl($request->input( 'virtualTour' ));
        if($request->input('video')) $restaurant->video = $this->escapeSingleUrl($request->input( 'video' ));

        //$restaurant->price = ($request->input('price')) ? $request->input( 'price' ) : null;
        $restaurant->priceMin = ($request->input( 'priceMin' )) ? intval($request->input( 'priceMin' )) : 20;
        $restaurant->priceMax = ($request->input( 'priceMax' )) ? intval($request->input( 'priceMax' )) : 30;
        $restaurant->price = intval( ( intval( $request->input('priceMin') ) + intval( $request->input('priceMax') ) )  / 2);

        $address_built = strip_tags($request->input('address')) .', '
                        .strip_tags($request->input('zip')).' '
                        .strip_tags($request->input('city')). ' ('
                        .strip_tags($request->input('state')).')';
        $location = Address::returnLocation($address_built);
        if(!$location) return response()->json(['message'=>'error.no_valid_address'], 403);

        $restaurant->location = $location[2];
        $restaurant->loc = ['type'=>"Point","coordinates"=>[doubleval($location[1]),doubleval($location[0])]];
        $restaurant->address = $address_built;//$request->input('address');
        $restaurant->city = $location[3];
        $restaurant->state = $location[4];
        $restaurant->zip = $location[5];
        $restaurant->valid_address = 1;

        
        $zona = Address::returnDistrict($location[0],$location[1]);
        $restaurant->district = $zona;
        $restaurant->valid_district = ($zona != '') ? 1 : 0;


        if($request->input('services') && count($request->input('services')) > 0):
            $services = [];
            foreach($request->input('services') as $service){
                $services[] = escape_non_alphanum($service);
            }
            $restaurant->services = $services;
        endif;
        if($request->input('tipoCucina') && count($request->input('tipoCucina')) > 0):
            $tipoCucina = [];
            foreach($request->input('tipoCucina') as $type){
                $tipoCucina[] = escape_non_alphanum($type);
            }
            $restaurant->tipoCucina = $tipoCucina;
        endif;
        if($request->input('regioneCucina') && count($request->input('regioneCucina')) > 0):
            $regioneCucina = [];
            foreach($request->input('regioneCucina') as $region){
                $regioneCucina[] = escape_non_alphanum($region);
            }
            $restaurant->regioneCucina = $regioneCucina;
        endif;
        if($request->input('carte') && count($request->input('carte')) > 0):
            $carte = [];
            foreach($request->input('carte') as $carta){
                $carte[] = escape_non_alphanum($carta);
            }
            $restaurant->carte = $carte;
        endif;

        if($request->input('fasce')):
            $restaurant->fasce = $request->input('fasce');
        endif;


        if(Auth::user()->role < 2):
            $user = User::find(Auth::user()->_id);
            $restaurant->user = Auth::user()->_id;
            $user->role = 2;
            $user->save();
        endif;

        if(Auth::user()->role >= 2) :
            $restaurant->user = Auth::user()->_id;
        endif;
        
       
        $restaurant->social = $this->escapeSocialArray($request->input('social'));
        $restaurant->staff = ($request->input('staff')) ? $this->escapeStaff($request->input('staff')): [];
        $restaurant->video =( $request->input('video')) ? $this->escapeSingleUrl($request->input('video')) : '';

        // se non sono admin il ristorante non è approvato
        $restaurant->approved = (Auth::user()->role < 4) ? 0 : 1;;

        $restaurant->save();

        $this->mapRestaurantTag($restaurant);

        $parameters = [Auth::user()->_id,Auth::user()->name.' '.Auth::user()->surname,$restaurant->_id,$restaurant->name];
        Notification::addNotification(2,$parameters,0);

        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'restaurant'=>$restaurant 
        ];
        return response()->json($response, $statusCode);

    }

    /**
     * Store a newly created resource in storage or update an existing one.
     * If is set request _id the restaurant will be updated
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        if(Auth::user()->role < 3):
            $this->validate($request, [
                'restaurant_id' => 'alpha_num',
                'name' => 'required|string',
                'description' => 'required|string',
                'tipoLocale' => 'required|array',
                'web' => 'url',
                'video' =>'url',
                'email'=>'required|email',
                'phone'=>'array',
                'virtualTour'=>'url',
                'priceMin'=>'required|numeric',
                'priceMax'=>'required|numeric',
                'address'=>'required|string',
                'city'=>'required|string',
                'state'=>'required|string',
                'zip'=>'required|string',
                'staff'=> 'array',
                'social'=> 'array',
                'services'=> 'array',
                'tipoCucina'=> 'array',
                'regioneCucina'=> 'array',
                'carte'=> 'array',
                'image'=>'alpha_num',
                'images'=>'array',
                'fasce'=>'array'
            ]);
        endif;

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        if(!$restaurant) return response()->json(['message'=>'error.empty'], 200);
        
        $restaurant->name = strip_tags($request->input( 'name' ));
        $restaurant->description = strip_tags($request->input( 'description' ));
        $restaurant->seodescription = utf8_encode( substr( $restaurant->description ,0,130).'...');
        $restaurant->tipoLocale = $request->input( 'tipoLocale' );

        $contact = [];
        $contact = (object) $contact;
        if($request->input('web')) $contact->web = $this->escapeSingleUrl($request->input( 'web' ));
        if($request->input('email')) $contact->email = $this->escapeSingleEmail($request->input( 'email' ));
        Log::info($restaurant->description);
        $contact->phone = [];        
        if(is_array($request->input( 'phone' )) && !empty($request->input( 'phone' ))):
            foreach($request->input( 'phone' ) as $phone_number):
                $contact->phone[] = strip_tags($phone_number);
            endforeach;
        elseif(is_string($request->input( 'phone' )) && $request->input( 'phone' ) != ''):
            $contact->phone[] = strip_tags($request->input( 'phone' ));
        endif;
        $restaurant->contact = $contact;
        
        $restaurant->virtualTour = $this->escapeSingleUrl($request->input( 'virtualTour' ));

        //$restaurant->price = ($request->input('price')) ? $request->input( 'price' ) : null;
        $restaurant->priceMin = ($request->input( 'priceMin' )) ? strip_tags($request->input( 'priceMin' )) : 20;
        $restaurant->priceMax = ($request->input( 'priceMax' )) ? strip_tags($request->input( 'priceMax' )) : 30;
        $restaurant->price = intval( ( intval( $request->input('priceMin') ) + intval( $request->input('priceMax') ) )  / 2);

        // fai la curl solo se necessaria
        if( $request->input('address') != $restaurant->address || ($restaurant->valid_address == 0 || !$restaurant->valid_address) ) :
            
            $address_built = 
                            strip_tags($request->input('address')) .', '.
                            strip_tags($request->input('zip')).' '.
                            strip_tags($request->input('city')). ' ('.
                            strip_tags($request->input('state')).')';

            $location = Address::returnLocation($address_built);
            if(!$location) return response()->json(['message'=>'error.no_valid_address'], 403);
            
            $restaurant->location = $location[2];
            $restaurant->loc = ['type'=>"Point","coordinates"=>[doubleval($location[1]),doubleval($location[0])]];
            $restaurant->address = $request->input('address');
            $restaurant->city = $location[3];
            $restaurant->state = $location[4];
            $restaurant->zip = $location[5];
            $restaurant->valid_address = 1;
            

        //else:
        //    $location = [$restaurant->loc['coordinates'][0],$restaurant->loc['coordinates'][1]];
        endif;
        
        //if(!$restaurant->valid_district || $restaurant->valid_district == 0):
            $lat = $restaurant->loc['coordinates'][1];
            $lng = $restaurant->loc['coordinates'][0];

            $zona = Address::returnDistrict($lat,$lng);
            $restaurant->district = [$zona];
            $restaurant->valid_district = ($zona != '') ? 1 : 0;
            
        //endif;


        if($request->input('services') && count($request->input('services')) > 0):
            $services = [];
            foreach($request->input('services') as $service){
                $services[] = escape_non_alphanum($service);
            }
            $restaurant->services = $services;
        endif;
        if($request->input('tipoCucina') && count($request->input('tipoCucina')) > 0):
            $tipoCucina = [];
            foreach($request->input('tipoCucina') as $type){
                $tipoCucina[] = escape_non_alphanum($type);
            }
            $restaurant->tipoCucina = $tipoCucina;
        endif;
        if($request->input('regioneCucina') && count($request->input('regioneCucina')) > 0):
            $regioneCucina = [];
            foreach($request->input('regioneCucina') as $region){
                $regioneCucina[] = escape_non_alphanum($region);
            }
            $restaurant->regioneCucina = $regioneCucina;
        endif;
        if($request->input('carte') && count($request->input('carte')) > 0):
            $carte = [];
            foreach($request->input('carte') as $carta){
                $carte[] = escape_non_alphanum($carta);
            }
            $restaurant->carte = $carte;
        endif;

        if(Auth::user()->role == 2) :
            $restaurant->user = Auth::user()->_id;
        endif;

        
        $restaurant->social = $this->escapeSocialArray($request->input('social'));
        $restaurant->image = ($request->input('image')) ? escape_non_alphanum($request->input('image')) : '';        
        $restaurant->images = ($request->input('images')) ? $this->escapeAlphaNumArray($request->input('images')) : [];


        $restaurant->staff = ($request->input('staff')) ? $this->escapeStaff($request->input('staff')): [];
        $restaurant->video =( $request->input('video')) ? $this->escapeSingleUrl($request->input('video')) : '';

        if($request->input('fasce')):
            $restaurant->fasce = $request->input('fasce');
        endif;

        $restaurant->save();

        $this->mapRestaurantTag($restaurant);

        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'restaurant'=>$restaurant 
        ];
        return response()->json($response, $statusCode);

    }


    /**
    *
    * Imposta i tag del ristorante
    * 
    * @param Mangiaebevi\Restaurant
    * */
    private function mapRestaurantTag(Restaurant $r)
    {
        
        try{
            $tipi_locali = TipoLocale::all();
            $tipi_cucina = Types::all();
            $regioni_cucina = Regions::all();
            $servizi = Services::all();

            $tags = [];

            $tags[] = $r->name;

            if($r->valid_district && $r->valid_district == 1 && count($r->district) > 0) $tags[] = $r->district[0];

            $len = count($r->location);

            $tags[] = strtolower($r->name);

            if(isset($r->location[1]))
                $tags[] = strtolower($r->location[1]); // indirizzo
            if(isset($r->location[2]))
                $tags[] = strtolower($r->location[2]); // città
            if(isset($r->location[3]))
                $tags[] = strtolower($r->location[3]); // città[2]
            
            if($len > 5):
                
                if(isset($r->location[$len-1]))
                    $tags[] = strtolower($r->location[$len-1]); // cap
                if(isset($r->location[$len-2]))
                    $tags[] = strtolower($r->location[$len-2]); // nazione
                if(isset($r->location[$len-3]))
                    $tags[] = strtolower($r->location[$len-3]); // regione
            
            endif;



            // tipo locale
            $n = ($r->tipoLocale && !empty($r->tipoLocale[0])) ? $this->returnTheName($r->tipoLocale[0],$tipi_locali) : '';
            if($n != '') $tags[] = strtolower($n);

            // servizi
            if(isset($r->services) && is_array($r->services) && !empty($r->services)) :
                foreach ($r->services as $service_id) {
                    $n = $this->returnTheName($service_id,$servizi);
                    if($n != '') $tags[] = $n;
                }
            endif;
            // regione cucina
            if(isset($r->tipoCucina) && is_array($r->tipoCucina) && !empty($r->tipoCucina)) :
                foreach ($r->tipoCucina as $tipo_id) {
                    $n = $this->returnTheName($tipo_id,$tipi_cucina);
                    if($n != '') $tags[] = $n;
                }
            endif;
            // regioni cucina
            if(isset($r->regioneCucina) && is_array($r->regioneCucina) && !empty($r->regioneCucina)) :
                foreach ($r->regioneCucina as $regione_id) { 
                    $n = $this->returnTheName($regione_id,$tipi_cucina);
                    if($n != '') $tags[] = $n;
                }
            endif;
                    

            $r->tags = $tags;
            $r->tags_string = (count($tags) > 0) ? implode(", ", $tags) : '';
            $r->save();

        } 
        catch(ErrorException $e)
        {
            Log::info('Errore mappatura tag: '.$r->name);
        }
    }

    /**
    * Prendi il tipo di locale
    * 
    * **/
    private function returnTheName($id,$array_all){
        $ret = '';
        foreach ($array_all as $obj) 
            if($obj->_id == $id) $ret = $obj->name;  

        return $ret;      
    }

    /**
    *
    * Escape staff array
    * 
    * @param array
    * @return array
    * **/
    private function escapeStaff($arr){
        if(isset($arr['sommelier']) && count($arr['sommelier']) > 0 && is_array($arr['sommelier'])):
            for($n = 0;$n < count($arr['sommelier']); $n++):
                $arr['sommelier'][$n] = strip_tags($arr['sommelier'][$n]);
            endfor;
        endif;
        if(isset($arr['barman']) && count($arr['barman']) > 0 && is_array($arr['barman'])):
            for($n = 0;$n < count($arr['barman']); $n++):
                $arr['barman'][$n] = strip_tags($arr['barman'][$n]);
            endfor;
        endif;
        if(isset($arr['maitre']) && count($arr['maitre']) > 0 && is_array($arr['maitre'])):
            for($n = 0;$n < count($arr['maitre']); $n++):
                $arr['maitre'][$n] = strip_tags($arr['maitre'][$n]);
            endfor;
        endif;
        if(isset($arr['chef']) && count($arr['chef']) > 0 && is_array($arr['chef'])):
            for($n = 0;$n < count($arr['chef']); $n++):
                $arr['chef'][$n] = strip_tags($arr['chef'][$n]);
            endfor;
        endif;
        return $arr;
    }

    /**
    *
    * Escape social array
    * 
    * @param array
    * @return array
    * **/
    private function escapeSocialArray($social){
        $social[0]['facebook'] = (isset($social[0]['facebook']) && filter_var($social[0]['facebook'], FILTER_VALIDATE_URL)) ? $social[0]['facebook'] : '';
        $social[1]['twitter'] = (isset($social[1]['twitter']) && filter_var($social[1]['twitter'], FILTER_VALIDATE_URL)) ? $social[1]['twitter'] : '';
        $social[2]['pinterest'] = (isset($social[2]['pinterest']) && filter_var($social[2]['pinterest'], FILTER_VALIDATE_URL)) ? $social[2]['pinterest'] : '';
        $social[3]['instagram'] = (isset($social[3]['instagram']) && filter_var($social[3]['instagram'], FILTER_VALIDATE_URL)) ? $social[3]['instagram'] : '';
        $social[4]['googlePlus'] = (isset($social[4]['googlePlus']) && filter_var($social[4]['googlePlus'], FILTER_VALIDATE_URL)) ? $social[4]['googlePlus'] : '';
        $social[5]['likedin'] = (isset($social[5]['likedin']) && filter_var($social[5]['likedin'], FILTER_VALIDATE_URL)) ? $social[5]['likedin'] : '';
        $social[6]['youtube'] = (isset($social[6]['youtube']) && filter_var($social[6]['youtube'], FILTER_VALIDATE_URL)) ? $social[6]['youtube'] : '';
        $social[7]['tripadvisor'] = (isset($social[7]['tripadvisor']) && filter_var($social[7]['tripadvisor'], FILTER_VALIDATE_URL)) ? $social[7]['tripadvisor'] : '';

        return $social;
    }

    /**
    *
    * Escape single url
    * 
    * @param array
    * @return array
    * **/
    private function escapeSingleUrl($url){
        //$url = (starts_with('http', $url)) ? $url : 'http://'.$url;
        $url = (filter_var($url, FILTER_VALIDATE_URL)) ? $url : '';
        return $url;
    }

    /**
    *
    * Escape single email
    * 
    * @param array
    * @return array
    * **/
    private function escapeSingleEmail($email){
        $email = (filter_var($email, FILTER_VALIDATE_EMAIL)) ? $email : '';
        return $email;
    }

    /**
    *
    * Escape alpha_num array
    * 
    * @param array
    * @return array
    * **/
    private function escapeAlphaNumArray($arr){
        $new = [];
        foreach ($arr as $val) {
            $new[] = escape_non_alphanum($val);
        }
        return $new;
    }


    /**
     * Update special restaurant values
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function adminUpdate(Request $request)
    {
        //
        if(Auth::user()->role < 3):
            $this->validate($request, [
                'restaurant_id' => 'alpha_num',
                'seodescription' => 'required|string',
                'district' => 'required|array',
                'address' => 'required|string',
                'zip' => 'required|string',
                'city' => 'url'
            ]);
        endif;

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        if(!$restaurant):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        
        $restaurant->seodescription = $request->input('seodescription');
        $restaurant->district = $request->input('district');
        $restaurant->address = $request->input('address');
        $restaurant->city = $request->input('city');
        $restaurant->save();

        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'restaurant'=>$restaurant 
        ];
        return response()->json($response, $statusCode);

    }



    /**
    *
    * Return valid restaurant slug
    * 
    * @param  string  $name
    * @return string
    * */
    private function returnRestaurantSlug($name){
        $slug = str_slug($name);
        if(count(Restaurant::where('slug',$slug)->get()) > 0):
            $n = 2;
            while( count(Restaurant::where('slug',$slug.'-'.$n)->get()) > 0 ):
                $n++;
            endwhile;
            return $slug.'-'.$n;
        else:
            return $slug;
        endif;
    }





    /**
    * Set an array of opening times
    * 
    *   {   
    *       // lunedì
    *       0: [00:00-00:00,00:00-00:00,00:00-00:00],
    *       // martedì
    *       1: [00:00-00:00,00:00-00:00,00:00-00:00],
    *       2,3,4,5,6 // giorni della settimana
    *   } 
    * 
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    * 
    */
    public function manageOrariRestaurant(Request $request)
    {
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'orari' => 'required|array'
        ]);

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        if(!$restaurant):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        $statusCode = 200;
        
        $restaurant->fasce = $request->input('orari'); 
        $restaurant->save();
        $message = 'ok';
        

        return response()->json(['message'=>$message,'orari'=>$request->input('orari')], $statusCode);
    }

    /**
     * Add special closing/opening dates
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response $response
     */
    public function setSpecialDate(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id' => 'required|alpha_num',
            'type' => 'required|integer',
            'day' => 'required|date_format:Y-m-d',
            'from' => 'string',
            'to' => 'string',
            'all_day' => 'required|integer'
        ]);
        $restaurant = Restaurant::find($request->input('restaurant_id'));
        
        if($restaurant->special_dates):
            $special_dates = $restaurant->special_dates;
        else:
            $special_dates = [];
        endif;

        $special_dates[] = [
        'day'=>$request->input('day'),
        'type'=>$request->input('type'), // 0 chiusura, 1 apertura
        'from'=>$request->input('from'),
        'to'=>$request->input('to'),
        'all_day'=>$request->input('all_day') // 0 no, 1 si
        ];

        $restaurant->special_dates = $special_dates;

        $restaurant->save();

        
        return response()->json(['message'=>'ok'], 200);
    }

    /**
     * Remove special closing/opening dates
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response $response
     */
    public function removeSpecialDate(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id' => 'required|alpha_num',
            'type' => 'required|integer',
            'day' => 'required|date_format:Y-m-d',
            'from' => 'string',
            'to' => 'string',
            'all_day' => 'required|integer'
        ]);
        $restaurant = Restaurant::find($request->input('restaurant_id'));
        
        $new_dates = [];
        if($restaurant->special_dates):
            foreach ($restaurant->special_dates as $date) {
                
                if($date['day'] != $request->input('day') ||
                    $date['type'] != $request->input('type') ||
                    $date['from'] != $request->input('from') ||
                    $date['to'] != $request->input('to') ||
                    $date['all_day'] != $request->input('all_day')):
                    $new_dates[] = $date;
                endif;
            }
        endif;
        
        $restaurant->special_dates = $new_dates;

        $restaurant->save();

        
        return response()->json(['message'=>'ok'], 200);
    }

    
    /** 
     *
     * Set Restaurant structure (tables) and the time between reservations
     * 
     * @param type Request $request 
     * @return type
     */
    public function addChangeStrutturaRestaurant(Request $request)
    {
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'time' => 'required|numeric',
            'struttura'=>'required|array'
        ]);
        $statusCode = 200;
        
        $restaurant = Restaurant::find($request->input('restaurant_id'));
        if(!$restaurant):
            return response()->json(['message'=>'error.empty'], $statusCode);
        endif;
        
        
        $restaurant->time_shift = $request->input('time');
        $restaurant->struttura = $request->input('struttura');
        $restaurant->save();

        return response()->json(['message'=>'ok','restaurant'=>$restaurant], $statusCode);
    }

    /** 
     *
     * Add restaurant Menu, it will take all the menu object, the plate id wil be verified
     * 
     * @param type Request $request 
     * @return type
     */
    public function addChangeMenuRestaurant(Request $request)
    {
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'menu' => 'required|array'
        ]);
        
        $statusCode = 200;
        
        $restaurant = Restaurant::find($request->input('restaurant_id'));
        if(!$restaurant):
            return response()->json(['message'=>'error.empty'], $statusCode);
        endif;
        
        $menus = $this->validate_menu($request->input('menu'));

        //Plates::where('restaurant',$request->input('restaurant_id'))->delete(); // che porcata
        $plate_ids = [];
        $m = 0;    
        foreach($menus['menus'] as $menu):
            $c = 0;
            
            foreach($menu['categorie'] as $categoria):
                $p = 0;
                
                foreach ($categoria['piatti'] as $piatto):
                    
                    
                    $add_plate_to_db = false;
                    if(isset($piatto['id'])):
                        $plate = Plates::find($piatto['id']);
                        if(!$plate) : 
                            $add_plate_to_db = true;
                        endif;
                    else:
                        $add_plate_to_db = true;
                    endif;

                    if($add_plate_to_db):
                        $plate = new Plates();                        
                    endif;
                    if(!isset($piatto['allergeni'])) $piatto['allergeni'] = [];
                    if(!isset($piatto['options'])) $piatto['options'] = [];
                    if(!isset($piatto['ingredienti'])) $piatto['ingredienti'] = [];
                    $plate->name = $piatto['name'];
                    $plate->image = (isset($piatto['image'])) ? $piatto['image'] : '';
                    $plate->ingredienti = (isset($piatto['ingredienti'])) ? $piatto['ingredienti'] : [];
                    $plate->price = (isset($piatto['price'])) ? floatval($piatto['price']) : 0.00;
                    
                    $plate->description = (isset($piatto['description'])) ? $piatto['description'] : "";
                    $plate->custom_info = (isset($piatto['custom_info'])) ? $piatto['custom_info'] : "";
                    $plate->options = (isset($piatto['options'])) ? $piatto['options'] : [];
                    $plate->allergeni = (isset($piatto['allergeni'])) ? $piatto['allergeni'] : [];
                    
                    $plate->restaurant = $request->input('restaurant_id');
                    $plate->fascia = (isset($menus['menus'][$m]['fasce'])) ? $menus['menus'][$m]['fasce'] : [];
                    $plate->save();

                    $plate_ids[] = $plate->_id; // soft delete
                    $piatto['id'] = $plate->_id;
                    $piatto['fasce'] = (isset($menus['menus'][$m]['fasce'])) ? $menus['menus'][$m]['fasce'] : [];
                    
                    // metti id nel piatto
                    $menus['menus'][$m]['categorie'][$c]['piatti'][$p] = $piatto;

                    $p++;
                endforeach; // piatto
                $c++;
            endforeach;// categoria
            $m++;
        endforeach;// menu
         
        Plates::whereNotIn('_id',$plate_ids)->where('restaurant',$request->input('restaurant_id'))->update(['deleted'=>1],['upsert' => true]);
        $restaurant->menu = $menus;
        $restaurant->save();

        return response()->json(['message'=>'ok','restaurant'=>$restaurant, 'menu'=>$menus], $statusCode);
    }


    /**
    *
    * Validate menu
    * 
    * @param array menu
    * @return array escaped menu
    * */
    private function validate_menu($menus){
        $m = 0;
        foreach($menus['menus'] as $menu):
            $c = 0;
            $menus['menus'][$m]['name'] = strip_tags($menus['menus'][$m]['name']);
            $menus['menus'][$m]['type'] = (isset($menus['menus'][$m]['type'])) ? intval($menus['menus'][$m]['type']) : 0;
            $menus['menus'][$m]['style'] = (isset($menus['menus'][$m]['style'])) ? intval($menus['menus'][$m]['style']) : 0;
            $menus['menus'][$m]['pre_text'] = (isset($menus['menus'][$m]['pre_text'])) ? strip_tags($menus['menus'][$m]['pre_text']) : '';
            $menus['menus'][$m]['post_text'] = (isset($menus['menus'][$m]['post_text'])) ? strip_tags($menus['menus'][$m]['post_text']) : '';
            foreach($menu['categorie'] as $categoria):
                $p = 0;
                $menus['menus'][$m]['categorie'][$c]['name'] = strip_tags($menus['menus'][$m]['categorie'][$c]['name']);
                $menus['menus'][$m]['categorie'][$c]['line_break'] = isset( $menus['menus'][$m]['categorie'][$c]['line_break']) ? intval( $menus['menus'][$m]['categorie'][$c]['line_break']) : 0;
                foreach ($categoria['piatti'] as $piatto):
                    
                    $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['name'] = strip_tags($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['name']);
                    $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['description'] = strip_tags($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['description']);
                    $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['price'] = (isset($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['price'])) ? number_format($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['price'], 2, '.', '') : 0.00;
                    $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['image'] = (isset($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['image'])) ? escape_non_alphanum($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['image']) : '';

                    if(isset($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['ingredienti'])):
                        for($nn = 0; $nn < count($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['ingredienti']); $nn++):
                            $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['ingredienti'][$nn] = strip_tags($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['ingredienti'][$nn]);
                        endfor;
                    endif;

                    if(isset($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['allergeni'])):
                        for($nn = 0; $nn < count($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['allergeni']); $nn++):
                            $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['allergeni'][$nn] = strip_tags($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['allergeni'][$nn]);
                        endfor;
                    endif;

                    if(isset($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'])):
                        for($nn = 0; $nn < count($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options']); $nn++):
                            $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['name'] = strip_tags($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['name']);
                            $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['type'] = intval($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['type']);
                            if(isset($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['options'])):
                                for($mm = 0; $mm < count($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['options']); $mm++):
                                    $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['options'][$mm]['category'] = $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['name'];
                                    $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['options'][$mm]['no_variation'] = intval($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['options'][$mm]['no_variation']);
                                    $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['options'][$mm]['variation'] = intval($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['options'][$mm]['variation']);
                                    $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['options'][$mm]['price'] = number_format($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['options'][$mm]['price'], 2, '.', '');
                                    $menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['options'][$mm]['name'] = strip_tags($menus['menus'][$m]['categorie'][$c]['piatti'][$p]['options'][$nn]['options'][$mm]['name']);
                                endfor;
                            endif;
                        endfor;
                    endif;

                $p++;
                endforeach; // piatto
                $c++;
            endforeach;// categoria
            $m++;
        endforeach;// menu

        return $menus;
    }


    /**
    *
    * Change restaurant reservation option
    * 
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response
    * */
    public function changeCanReserve(Request $request)
    {
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num'
        ]);

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        $statusCode = 200;
        
        if(!$restaurant) :
            return response()->json(['message'=>'error.empty'], $statusCode);
        endif;        
        
        $restaurant->prenotazione = ($restaurant->prenotazione == 1) ? 0 : 1; 
        $restaurant->save();
        $message = 'ok';
        

        return response()->json(['message'=>$message], $statusCode);
    }

    /**
    *
    * Change restaurant can order
    * 
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response
    * */
    public function changeCanOrders(Request $request)
    {
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num'
        ]);

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        $statusCode = 200;
        
        if(!$restaurant) :
            return response()->json(['message'=>'error.empty'], $statusCode);
        endif;        
                   
        $restaurant->ordina = ($restaurant->ordina == 1) ? 0 : 1; 
        $restaurant->save();
        $message = 'ok';        

        return response()->json(['message'=>$message], $statusCode);
    }

    /**
    *
    * Change restaurant take away option
    * 
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response
    * */
    public function changeTakeAway(Request $request)
    {
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num'
        ]);

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        $statusCode = 200;
        
        if(!$restaurant) :
            return response()->json(['message'=>'error.empty'], $statusCode);
        endif;        
                   
        $restaurant->take_away = ($restaurant->take_away == 1) ? 0 : 1; 
        $restaurant->save();
        $message = 'ok';        

        return response()->json(['message'=>$message], $statusCode);
    }

    /**
    *
    * Change delivery meters range
    * 
    * @param Request $request 
    * @return Illuminate\Http\Response 'message'=>true
    * 
    * */
    public function setRestaurantDeliveryRange(Request $request)
    {
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'range' => 'required|integer' // metri in cui effettua la consegna
        ]);

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        $statusCode = 200;
        
        if(!$restaurant) :
            return response()->json(['message'=>'error.empty'], $statusCode);
        endif;        
                   
        $restaurant->range = intval($request->input('range')); 
        $restaurant->save();
        $message = 'ok';        

        return response()->json(['message'=>$message], $statusCode);
    }

    /**
    *
    * Change delivery zip codes
    * @param Request $request 'restaurant_id','zip'=>array of zip codes
    * @return Illuminate\Http\Response 'message'=>true
    * 
    * */
    public function setRestaurantDeliveryZip(Request $request)
    {
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'delivery_zip' => 'required|array' // metri in cui effettua la consegna
        ]);

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        $statusCode = 200;
        
        if(!$restaurant) :
            return response()->json(['message'=>'error.empty'], $statusCode);
        endif;        
                   
        $restaurant->delivery_zip = $request->input('delivery_zip'); 
        $restaurant->save();
        $message = 'ok';        

        return response()->json(['message'=>$message], $statusCode);
    }

    /**
     * Set slug for all restaurants
     *
     * @return \Illuminate\Http\Response
     */
    public function massiveSetSlug()
    {
        $restaurants = Restaurant::all();
        foreach($restaurants as $restaurant):
            $r = Restaurant::find($restaurant->_id);
            $r->slug = str_slug($r->name,'-');
            $r->save();
        endforeach;
        return response()->json(['message'=>'ok'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  type Request $request->input('id') 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $this->validate($request, [
            'id'  => 'required|alpha_num'
        ]);

        $restaurant = Restaurant::find($request->input('id'));
        if(!$restaurant):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        return response()->json(['message'=>'ok','restaurant'=>$restaurant], 200);
    }
    /**
     * Show restaurant by name. Query LIKE
     *
     * @param  type Request $request->input('id') 
     * @return \Illuminate\Http\Response
     */
    public function showByName(Request $request)
    {
        //
        $this->validate($request, [
            'name'  => 'required|string'
        ]);

        $restaurant = Restaurant::where('name','LIKE', '%'.$request->input('name').'%');
        return response()->json(['message'=>'ok','restaurant'=>$restaurant], $statusCode);
    }

    /**
     * Show restaurant by slug
     *
     * @param  type Request $request->input('id') 
     * @return \Illuminate\Http\Response
     */
    public function showBySlug(Request $request)
    {
        //
        $this->validate($request, [
            'slug'  => 'required|string'
        ]);

        $restaurant = Restaurant::where('slug',$request->input('slug'));
        return response()->json(['message'=>'ok','restaurant'=>$restaurant], $statusCode);
    }
   

    
    /**
     * Remove restaurant
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'restaurant_id' => 'required|alpha_num'
        ]);

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        if(!$restaurant) :
            return response()->json(['message'=>'error.empty'], $statusCode);
        endif;
        $restaurant->delete();
        $message = 'ok';
        
        return response()->json(['message'=>$message], 200);
    }

    

    /**
    *
    * Get restaurant by city name, le prende sono con immagine
    * 
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function getByCity($city, Request $request){
        
        
        //$this->validate( [$city => 'required|string']);

        $restaurants = Restaurant::where('location',$city)
                                ->where('image','!=','')
                                ->whereNotNull('image')
                                ->take(40)
                                ->get();
        
        $response = ['city'=>$city,'restaurants'=>[]];
        foreach ($restaurants as $restaurant) {
            $response['restaurants'][] = $restaurant;
        }
        
        $statusCode = 200;
        return response()->json($response, $statusCode);       
        
    }

    /**
    *
    *  Add image to restaurant
    * 
    * @param Illuminate\Http\Request $request 'restaurant_id', 'image'
    * @return Illuminate\Http\Response $response json 
    * 
    */
    public function addRestaurantImage(Request $request){
        $this->validate($request, [
            'restaurant_id' => 'required|alpha_num',
            'image' => 'mimes:jpeg,gif,png'
        ]);
        
        $statusCode = 200;
        
        $restaurant = Restaurant::find($request->input('restaurant_id'));
        if(!$restaurant):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        
        $image_id = SingleImage::addImages('restaurant',$restaurant->name,$restaurant->name,\Input::file('image'),$restaurant->_id);
        
        $oldimages = $restaurant->images;
        if(count($oldimages) > 0):
            $oldimages[] = $image_id;
            $restaurant->images = $oldimages;
        else:
            $restaurant->images = [$image_id];
        endif;

        if(!$restaurant->image || count($restaurant->image) == 0):
            $restaurant->image = $image_id;
        endif;

        $restaurant->save();

        // return new image name
        $response = [
          'message' => 'ok',
          'image' =>  $image_id
        ];         
        return response()->json($response, $statusCode);
    }

    /**
    *
    *  Set Massive import for images
    * 
    * @param \Illuminate\Http\Request  $request
    * 
    * **/
    public function returnIds(){
        $restaurants = Restaurant::whereNotNull('image')->where('images_imported',null)->get(['name']);
        return response()->json(['message'=>'ok','r'=>$restaurants], 200);
    }
    public function importImages(){

        $restaurants = Restaurant::whereNotNull('image')->where('images_imported',null)->take(1)->get();
        $n = 0;
        $ids = [];
        foreach($restaurants as $restaurant):
            if($this->updateRestaurantImages($restaurant->_id)):
                $n++;
                $ids[] = $restaurant->_id;
            endif;
        endforeach;
        return response()->json(['message'=>'ok','n'=>$n], 200);//,'updated'=>$n,'ristoranti'=>$ids
    }

    /**
    *
    * Massive import update single
    * 
    * @param string $id
    * @return bool
    * */
    private function updateRestaurantImages($id){
        $restaurant = Restaurant::find($id);
        $img = new SingleCityImage();
        if($restaurant):
            $num_images = count($restaurant->images);
            $slug = str_slug($restaurant->name);            
                
            $x = 0;
            $new_images = $restaurant->images;                
            foreach($restaurant->images as $image_url):
                try{
                    if(preg_match("/mangiaebevi/i",$image_url) && $x <= 5):
                        $str = @file_get_contents($image_url);
                        if($str):
                            $return_id = $img->public_add("restaurant",$slug,$slug,$str,$restaurant->_id);
                            if($return_id):
                                $new_images[$x] = $return_id;
                            endif;
                        endif;
                    endif;
                } catch(errorException $e){
                    
                }                
                $x++;
            endforeach;
            $restaurant->image = $new_images[0];
            $restaurant->images = $new_images;
            $restaurant->images_imported = 1;            
            
            $restaurant->save();
        endif;
        return true;
    }


    /**
    *
    * Import remaining restaurants
    * 
    * @return \Illuminate\Http\Response $response
    * */
    public function import_remaining(){
        $added = 0; // max 5
        $update_lo_stesso = 0;
        $restaurant_updated = 0; // max 1
        $img = new SingleCityImage();

        $restaurants = Restaurant::whereNotNull('image')->get();
        foreach($restaurants as $restaurant):
            $num_images = count($restaurant->images);
            $slug = str_slug($restaurant->name);    
            if($num_images == 1):
                // ha 1 immagine sola
                if(preg_match("/mangiaebevi/i",$restaurant->image)):
                    try{                        
                        $str = @file_get_contents($image_url);
                        $update_risto = Restaurant::find($restaurant->_id);
                        if($str):
                            $return_id = $img->public_add("restaurant",$slug,$slug,$str,$restaurant->_id);
                            if($return_id):
                                $image = $return_id;
                                $update_risto->image = $image;
                                $update_risto->images = [$image];
                            endif;
                        else:
                            // impossibile prendere immagine
                            // probabilmente 404
                            // la tolgo completamente
                            $update_risto->image = null;
                            $update_risto->images = [];
                        endif;
                        $update_risto->save();
                        return response()->json(['message'=>'ok','n'=>1], 200);
                    } catch(errorException $e){
                        
                    }  
                endif; // immagine matcha mangiaebevi
            else:
                $update_risto = Restaurant::find($restaurant->_id);
                $new_images = [];
                foreach ($update_risto->images as $single_image) :
                    if(preg_match("/mangiaebevi/i",$single_image) && $added <= 5):
                        try{
                            $str = @file_get_contents($single_image);
                            if($str):
                                $return_id = $img->public_add("restaurant",$slug,$slug,$str,$restaurant->_id);
                                if($return_id):
                                    $new_images[] = $return_id; 
                                    $added++;                                   
                                endif;
                            else:
                                // aggiungi in ogni caso
                                $update_lo_stesso = 1;
                            endif;                            
                        } catch(errorException $e){

                        }
                    else:
                        $new_images[] = $single_image;
                    endif;
                endforeach;
                // non hanno matchato
                if($added > 0 || $update_lo_stesso == 1):
                    // aggiorno il ristorante solo se ci sono stati match
                    $update_risto->image = $new_images[0];
                    $update_risto->images = $new_images;
                    $update_risto->save();
                    return response()->json(['message'=>'ok','n'=>1], 200);
                endif;
                
            endif;

        endforeach;
        return response()->json(['message'=>'ok','n'=>0], 200);
    }



    /**
    *
    * Add Facebook Page id to restaurant
    * 
    * @param Illuminate\Http\Request $request
    * @return Illuminate\Http\Response $response
    * **/
    public function addFacebookPage(Request $request){
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'facebook' => 'required|numeric' // id di facebook sono numerici
        ]);

        // verifica che non sia già inserita da qualcuno
        $just = Restaurant::where('facebook_page_id',$request->input('facebook'))->get(['_id']);
        if(count($just) > 0) return response()->json(['message'=>'just'], 200);

        $r = Restaurant::find($request->input('restaurant_id'));
        $r->facebook_page_id = $request->input('facebook');

        $r->save();
        return response()->json(['message'=>'ok'], 200);
    }

    /**
    *
    * Remove Facebook Page id to restaurant
    * 
    * @param Illuminate\Http\Request $request
    * @return Illuminate\Http\Response $response
    * **/
    public function removeFacebookPage(Request $request){
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num'
        ]);

        // verifica che non sia già inserita da qualcuno        
        $r = Restaurant::find($request->input('restaurant_id'));
        $r->facebook_page_id = "";
        $r->save();
        return response()->json(['message'=>'ok'], 200);
    }

    /**
    *
    * Return by Facebook page id
    * 
    * @param Illuminate\Http\Request $request
    * @return Illuminate\Http\Response $response
    * **/
    public function getByFacebookPage(Request $request){
        $this->validate($request, [
            'page_id'  => 'required|numeric'
        ]);

        // verifica che non sia già inserita da qualcuno        
        $r = Restaurant::where('facebook_page_id',intval( $request->input('page_id') ) )->get();
        if(count($r) <= 0) return response()->json(['message'=>'not found page id: '.$request->input('page_id')], 403);

        return response()->json(['message'=>'ok','restaurant'=>$r[0]], 200);
    }


    /**
    * 
    *
    * mongo db to remember queries:
    *
    * Matches arrays that contain all elements specified in the query.
    *
    *  User::where('roles', 'all', array('moderator', 'author'))->get();
    *
    *
    */
}
