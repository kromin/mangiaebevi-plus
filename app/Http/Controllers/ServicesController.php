<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use Jenssegers\Mongodb\Model as Eloquent;

use MangiaeBevi\Services;
use MangiaeBevi\Restaurant;




class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $services = Services::all();        
        return response()->json(['message'=>'Ok','services'=>$services], 200);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
             
        $this->validate($request, [
            'name' => 'required|string',
            'image' => 'string',
            'icon' => 'string'
        ]);

        
        $service = new Services();
        $service->name = $request->input('name');
        $service->slug = str_slug($request->input('name'));
        $service->image = ($request->input('image')) ? $request->input('image') : '';
        $service->icon = ($request->input('icon')) ? $request->input('icon') : '';
        $service->save();

        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'service' => $service
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);

        $service = Services::find($request->input('id'));
        
        $statusCode = 200;
        $response = [
          'message' => 'Ok',
          'service' => $service 
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request, [
            'id'    => 'required|alpha_num',
            'name' => 'required|string',
            'image' => 'string',
            'icon' => 'string'
        ]);

        $service = Services::findOrFail($request->input('id'));
        $service->name = $request->input('name');
        $service->slug = str_slug($request->input('name'));
        $service->image = ($request->input('image')) ? $request->input('image') : '';
        $service->icon = ($request->input('icon')) ? $request->input('icon') : '';
        $service->save();
        
        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'service'=>$service
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);
        $service = Services::find($request->input('id'));
        if(!$service):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        $restaurants = Restaurant::whereIn('services',[$request->input('id')])->get();
        foreach ($restaurants as $r) {
            $risto = Restaurant::find($r->_id);
            $servizi = [];
            foreach ($risto->services as $servizio) {
                if($servizio != $request->input('id')){
                    $servizi[] = $servizio;
                }
            }
            $risto->services = $servizi;
            $risto->save();
        }

        $service->delete();
        return response()->json(['message'=>'ok'], 200);
    }


    public function massiveFillServices()
    {
        $restaurants = Restaurant::all();
        $services = [];
        foreach($restaurants as $restaurant){ 
            if($restaurant->services) :          
                foreach($restaurant->services as $service){
                    $services[] = $service;
                }
            endif;
        }

        $services = array_unique($services);
        
        foreach($services as $service){
            $addToCollection = new Services();
            $addToCollection->name = $service;
            $addToCollection->save();
        }
        return response()->json(['message'=>'ok'], 200);
    }

    public function massiveCleanRestaurantsServices()
    {

        $services = Services::all();

        $restaurants = Restaurant::all();
        foreach($restaurants as $restaurant){
            // take restaurant services 
            $single_services = [];
            // per ogni servizio del ristorante
            if($restaurant->services){
                foreach($restaurant->services as $service){
                    // vedi i servizi dell'app e per ognuno di loro
                    foreach($services as $single_service_collection){
                        // se il nome è uguale al servizio del ristorante
                        if($single_service_collection->name == $service){
                            // inseriscilo nell'array che aggiorna il ristorante
                            $single_services[] = $single_service_collection->_id;
                        }
                    }
                }
            }
            if(!empty($single_services)) :
                // aggiorniamo il singolo ristorante
                $this->updateRestaurantServices($restaurant->_id,$single_services);
            endif;
        }
        return response()->json(['message'=>'ok'], 200);
    }

    private function updateRestaurantServices($restaurant_id,$services)
    {
        $restaurant = Restaurant::find($restaurant_id);
        $restaurant->services = $services;
        $restaurant->save();
    }

    /**
    *
    * Return servizi in json
    * 
    * @param  \Illuminate\Http\Request
    * @return \Illuminate\Http\Response
    * 
    * */
    public function returnServizi(Request $request){
        $services = Services::where(1)->get(['name','slug']);

        return response()->json(['services'=>$services], 200);
    }

}
