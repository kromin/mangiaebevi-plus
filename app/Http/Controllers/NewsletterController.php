<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use Event;
use MangiaeBevi\Events\NewsletterRegistration;

use MangiaeBevi\Newsletter;
use Log;

use Auth;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $subscribers = Newsletter::all();        
        return response()->json(['message'=>'ok','subscribers'=>$subscribers], 200);
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'email' => 'required|unique:newsletter_collection|email|max:255',
            'name' => 'required|string'
        ]);


        try{
            $statusCode = 200;
            $response = [
              'message' => 'ok' 
            ];

            $newsletter = new Newsletter();
            $newsletter->name = $request->input('name');
            $newsletter->email = $request->input('email');
            $newsletter->delete_string = base64_encode( ( $newsletter->name . $newsletter->email . str_random(30) ) );
            $newsletter->save();

            if(!Auth::check()):
                // Evento iscrizione newsletter non da utente
                // invio email di ringraziamento
                Event::fire(new NewsletterRegistration($newsletter));
            endif;
            
            return response()->json($response, $statusCode);

        } catch (Exception $e){
            $statusCode = 200;
            return response()->json(['message'=>'error'], $statusCode);
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request->input('id')
     * @return \Illuminate\Http\Response
     */
    public function showSingle(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);

        $newsletter = Newsletter::findOrFail($request->input('id'));
        if(!$newsletter) :
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        return response()->json(['message'=>'ok','subscriber'=>$newsletter], 200);

    }

   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num',
            'email' => 'required|email|max:255',
            'name' => 'required|string'
        ]);

        $newsletter = Newsletter::findOrFail($request->input('id'));
        if(!$newsletter) :
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        $newsletter->email = $request->input('email');
        $newsletter->name  = $request->input('name');
        $newsletter->save();
        return response()->json(['message'=>'ok'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request->input('id')
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);

        $newsletter = Newsletter::findOrFail($request->input('id'));
        if(!$newsletter) :
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        $newsletter->delete();
        return response()->json(['message'=>'ok'], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  POST id, str
     * @return \Illuminate\Http\Response
     */
    public function destroyFromEmail(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num',
            'str' => 'required|string'
        ]);

        $newsletter = Newsletter::where('_id', $request->input('id'))->where('delete_string', $request->input('str') )->firstOrFail();
        if(!$newsletter) :
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        $newsletter->delete();
        return response()->json(['message'=>'ok'], 200);

    }
}
