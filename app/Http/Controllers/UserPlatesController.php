<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\UserPlate;
use MangiaeBevi\Plates;
use MangiaeBevi\User;
use Auth;
use Log;
use MangiaeBevi\Http\Controllers\UserActionsController as UserActionStatic;

class UserPlatesController extends Controller
{
    
    /**
     * Display a plate uploaded.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewSingle(Request $request)
    {
        //
        $this->validate($request, [
            'plate'  => 'required|alpha_num'
        ]);

        $p = UserPlate::find($request->input('plate'));
        
        if(!$p) return response()->json(['message'=>'error.empty'],404);
        
        return response()->json(['message'=>'ok','plate'=>$p],200);
    }

    /**
     * Add a community image plate
     *
     * @return \Illuminate\Http\Response
     */
    public function addPlateImage(Request $request)
    {
        //
        $this->validate($request, [
            'plate'  => 'required|alpha_num',
            'image'  => 'required|alpha_num',
            'text'   => 'required|string',
            'rating' => 'required|integer'
        ]);

        $p = Plates::find($request->input('plate'));
        
        if(!$p) return response()->json(['message'=>'error.empty'],404);
        
        $plateimage = new UserPlate;
        $plateimage->image = $request->input('image');
        $plateimage->text = $request->input('text');
        $plateimage->rating = $request->input('rating');
        $plateimage->plate = $request->input('plate');
        $plateimage->user = Auth::user()->_id;
        $plateimage->save();

        UserActionStatic::addUserAction(4,[$plateimage->_id,$plateimage->plate]);
        
        return response()->json(['message'=>'ok','plateimage'=>$plateimage],200);
    }

    /**
     * Add a community image plate
     *
     * @return \Illuminate\Http\Response
     */
    public function viewPlateImageByUser(Request $request)
    {
        //
        $this->validate($request, [
            'user'  => 'required|alpha_num',
            'skip' => 'integer',
            'take' => 'required|integer|max:10'
        ]);

        if(!$request->input('skip') || $request->input('skip') == 0):
            $plateimages = UserPlate::where('user',$request->input('user'))->take($request->input('take'))->orderBy('created_at','desc')->get();
        else:
            $plateimages = UserPlate::where('user',$request->input('user'))->skip($request->input('skip'))->take($request->input('take'))->orderBy('created_at','desc')->get();
        endif;

        $results = [];
        foreach ($plateimages as $i) {
            $i['plate_obj'] = Plates::find($i->plate);
            $results[] = $i;
        }
        
        return response()->json(['message'=>'ok','plateimages'=>$plateimages],200);
    }


    /**
     * Add a community image plate
     *
     * @return \Illuminate\Http\Response
     */
    public function viewPlateImageByPlate(Request $request)
    {
        //
        $this->validate($request, [
            'plate'  => 'required|alpha_num',
            'skip' => 'integer',
            'take' => 'required|integer|max:10'
        ]);

        if(!$request->input('skip') || $request->input('skip') == 0):
            $plateimages = UserPlate::where('plate',$request->input('plate'))->take($request->input('take'))->orderBy('created_at','desc')->get();
        else:
            $plateimages = UserPlate::where('plate',$request->input('plate'))->skip($request->input('skip'))->take($request->input('take'))->orderBy('created_at','desc')->get();
        endif;

        $results = [];
        foreach ($plateimages as $i) {
            $i['user_obj'] = User::where('_id',$i->user)->get(['name','avatar'])[0];
            $results[] = $i;
        }
        
        return response()->json(['message'=>'ok','plateimages'=>$plateimages],200);
    }
}
