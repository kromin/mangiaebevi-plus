<?php
/**
*
* Classe per gestire le iscrizioni dei ristoratori,
* aggiunge questi dati:
* billing_plan: [
* 	'active'=>0,1
* 	'token'=>'string',
* 	'payment_id'=>'string'
* ]
* 
* 
* */
namespace MangiaeBevi\Http\Controllers;


use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use Jenssegers\Mongodb\Model as Eloquent;


use Braintree_ClientToken;
use Braintree_Transaction;
use Braintree_Subscription;
use Braintree_Customer;
use Braintree_Descriptor;

use MangiaeBevi\Pacchetti;
use MangiaeBevi\User;
use MangiaeBevi\Orders;
use MangiaeBevi\Payments;
use MangiaeBevi\Restaurant;

use Log;
use Auth;
use Session;

use MangiaeBevi\Http\Controllers\NotificationsController as Notification;

use Carbon\Carbon;

use Srmklive\PayPal\Services\ExpressCheckout;


class PaymentsController extends Controller
{
    /**
    *
    * Retrieve Braintree user token
    * @deprecated
	* 
    * **/
    public function getToken(){
    	echo Braintree_ClientToken::generate();
    	//Session::put('BTtoken', $token);
    }

    /**
	*
	* Pay a subscription plan
	* 
	* 'nonce'=>'required|string',
	* 'token' => 'required|string',
	* 'plan' => 'required|alpha_num', // 0 = no, 1 = si
	* 'billing' => 'required|integer'
	* @deprecated
	* 
	* @param Illuminate\Http\Request $request
	* @return Response json 'message'=>ok | error.no_valid_plan | error.empty | error.payment_refused
	* 
    **/
    public function postPaymentPlan(Request $request){
    	$this->validate($request, [
            'nonce'=>'required|string',
            'type' => 'required',
            'plan' => 'required|alpha_num', 
            'billing' => 'required|integer'
        ]);

    	//$token = Session::get('BTtoken');

        $pacchetto = Pacchetti::find($request->input('plan'));
        if(!$pacchetto) return response()->json(['message'=>'error.empty'],200);

        $price = ($request->input('billing') == 0) ? $pacchetto->price_month : $pacchetto->price_annual;


        $customer = $this->createCustomer($request->input('nonce'),$request->input('type'));
        if(!$customer) return response()->json(['message'=>'error.no_customer'],200);
        
        $token = $customer['token'];
        

        $plans = explode("|",$pacchetto->braintree_plan);
        // verifica che esista il piano
        if(isset($plans[$request->input('billing')])) $plan_id = $plans[$request->input('billing')];
        else return response()->json(['message'=>'error.no_valid_plan','payment'=>$result],200);


        $result = Braintree_Subscription::create([
		  'paymentMethodToken' => $token,
		  'planId' => $plan_id
		]);
		if($result->success):
			// dovrebbe essere ok
			//Log::info($result);

			$transaction = $result->subscription->transactions[0];		
			//Log::info($transaction->_get('id'));
			Log::info($transaction->id);

			// non so se mi serve ma salvo comunque il customer id, il piano di pagamento
	        $user = User::find(Auth::user()->_id);

	        $transaction_info = [
	        	'customer_id'=>$customer['customer_id'],
	        	'payment_plan'=>$pacchetto->name,
	        	'payment_period'=>intval($request->input('billing')),
	        	'transaction_id'=>$transaction->id,
	        	'type'=>'subscription'
	        ];
	        if(!$user->transactions) :
	        	$user->transaction = [$transaction_info];
	       	else:
	       		$user_transactions = $user->transactions;
	       		$user_transactions[] = $transaction_info;
	       		$user->transactions = $user_transactions;
	       	endif;
	        
	        $user->active = (!$user->active || $user->active == 0) ? 1 : 0;
	        $user->role = ($user->role < 2) ? 2 : $user->role; // cambio ruolo utente e lo faccio diventare cliente
	        $user->save();


			// [id utente, nome utente, nome del piano]
			$parameters = [Auth::user()->_id,Auth::user()->name.' '.Auth::user()->surname,$pacchetto->name];
			Notification::addNotification(3,$parameters,0);

			return response()->json(['message'=>'ok','payment'=>$result],200);
		else:
			return response()->json(['message'=>'error.payment_refused','payment'=>$result],200);
		endif;
    }


    /**
	*
	* Pay an order, save transaction in payments_collection and update order status to 1
	* 
	* 'nonce'=>'required|string'	* 
	* @deprecated
	* 
	* @param Illuminate\Http\Request $request
	* @return Response json 'message'=>ok | error.empty | error.payment_refused
	* 
    **/
    public function saveOrderPayment(Request $request){
    	$this->validate($request, [
            'nonce'=>'required|string'
        ]);

    	$order_id = Session::get('current_order');
    	$order = Orders::find($order_id);

    	if(!$order) return response()->json(['message'=>'error.empty'],200);


    	$result = Braintree_Transaction::sale([
		  'amount' => $order->price,
		  'paymentMethodNonce' => $request->input('nonce'),//,
		  /*'customFields' => [
		        'menoo_order' => $order->_id
		    ]*/
		  'options' => [
		    'submitForSettlement' => True
		  ]
		]);

    	if($result->success):
    		$transaction_id = $result->transaction->id;

    		$payment = new Payments();
    		$payment->user = Auth::user()->_id;
    		$payment->transaction = $transaction_id;
    		$payment->price = $order->price;
    		$payment->save();

    		$order->transaction = $transaction_id;
    		$order->payment = $payment->_id;
    		$order->status = 1;
    		
    		$restaurant = Restaurant::find($order->restaurant);
    		$ristoratore = ($restaurant->user) ? $restaurant->user : '0';

    		$order->restaurant_info = [
    		'image'=>$restaurant->image,
    		'name'=>$restaurant->name,
    		'slug'=>$restaurant->slug
    		];
    		$order->user_info = [
    		'image'=>Auth::user()->avatar,
    		'name'=>Auth::user()->name . ' '.Auth::user()->surname
    		];
    		$order->save();

    		// [id_ordine, id utente, nome utente, prezzo totale, data consegna]
    		$parameters = [$order->_id,Auth::user()->_id,Auth::user()->name.' '.Auth::user()->surname,$order->price,$order->date];
    		Notification::addNotification(1,$parameters,$ristoratore);

    		return response()->json(['message'=>'ok'],200);
    	else:
    		return response()->json(['message'=>'error.payment_refused','payment'=>$result],200);
    	endif;
		
    	
    }



    /**
	*
	* Create a Braintree customer, required for subscription plans
	* 
	* @deprecated
	* 
	* @param nonce client Payment nonce
	* @param account no longer required
	* @return false on error
	* @return array message=>ok, customer_id, token (payment token to add the plan)
	* 
    ***/
    private function createCustomer($nonce,$account){
    	$result = Braintree_Customer::create([
		    'firstName' => Auth::user()->name,
		    'lastName' => Auth::user()->surname,
		    'email' => Auth::user()->email,
		    'paymentMethodNonce' => $nonce
		]);
		
		if ($result->success) {	
			return ['message'=>'ok','customer_id'=>$result->customer->id,'token'=>$result->customer->paymentMethods()[0]->token];		    
		} else {
		    foreach($result->errors->deepAll() AS $error) {
		        Log::info($error->code . ": " . $error->message . "\n");
		    }
		    return false;
		}
    }



    /**
    *
    * Starting using paypal recurring
    * 
    * @param Illuminate\Http\Request $request
	* @return route
    * ***/
    public function addBillingPlanToUser(Request $request){
    	$provider = new ExpressCheckout;

    	$token = ($request->query->has('token')) ? $request->input('token') : null;
    	$PayerID = ($request->query->has('PayerID')) ? $request->input('PayerID') : null;

    	if(!$token || !$PayerID) :
    		echo 'si è verificato un errore'; 
    		return;
    	endif;


    	$u = User::where('billing_plan.token',$token)->firstOrFail();
    	if(!$u) :
    		echo 'si è verificato un errore'; 
    		return;
		endif;

		$plan = Pacchetti::find($u->billing_plan['plan']);
		if(!$plan):
    		echo 'si è verificato un errore'; 
    		return;
		endif;

		$per = ($u->billing_plan['period'] == 0) ? 'mensile' : 'annuale';
		$billing_period = ($u->billing_plan['period'] == 0) ? 'Month' : 'Year';
		$billing_freq = ($u->billing_plan['period'] == 0) ? '1' : '1';
		$price = ($u->billing_plan['period'] == 0) ? $plan->price_month : $plan->price_annual;
		$description = "Iscrizione al piano di abbonamento ".$plan->name. " di MangiaeBevi";


    	// The $token is the value returned from SetExpressCheckout API call
		$startdate = Carbon::now()->toAtomString();
		$profile_desc = $description;
		Log::info('cazzo');
		$data = [
		    'PROFILESTARTDATE' => $startdate,
		    'DESC' => $profile_desc,
		    'BILLINGPERIOD' => $billing_period, // Can be 'Day', 'Week', 'SemiMonth', 'Month', 'Year'
		    'BILLINGFREQUENCY' => $billing_freq, // set 12 for monthly, 52 for yearly
		    'AMT' => $price, // Billing amount for each billing cycle
		    'CURRENCYCODE' => 'EUR', // Currency code
		    'TRIALBILLINGPERIOD'=>'Month',
		    'TRIALBILLINGFREQUENCY'=>'1',
		    'TRIALAMT'=>'0',
		    'TRIALTOTALBILLINGCYCLES'=>'3' 
		    /*'TRIALBILLINGPERIOD' => 'Day',  // (Optional) Can be 'Day', 'Week', 'SemiMonth', 'Month', 'Year'
		    'TRIALBILLINGFREQUENCY' => 10, // (Optional) set 12 for monthly, 52 for yearly 
		    'TRIALTOTALBILLINGCYCLES' => 1, // (Optional) Change it accordingly
		    'TRIALAMT' => 0, // (Optional) Change it accordingly*/
		];
		$response = $provider->createRecurringPaymentsProfile($data, $token);

		if($response['ACK'] != 'Success'):
			//return response()->json(['d'=>$response],403);
			return redirect()->route('/u/',[$u->_id]);
			echo 'si è verificato un errore'; 
    		return;
		endif;

		if($response['ACK'] == 'Success'):
			$billing_infos = $u->billing_plan;
			$billing_infos['PROFILEID'] = $response['PROFILEID'];
			$billing_infos['PROFILESTATUS'] = $response['PROFILESTATUS']; // ActiveProfile
			$billing_infos['CORRELATIONID'] = $response['CORRELATIONID'];
			$billing_infos['active'] = 1;
			
			$u->billing_plan = $billing_infos;
			$u->save();
		endif;

		return redirect()->route('/u/',[$u->_id]);

    }

    /**
    *
    * Sospendi pagamento ricorrente
    * 
    * @param Illuminate\Http\Request $request
    * @return Illuminate\Http\Response $response
    * ***/
    public function suspendBillingPlanToUser(Request $request){
    	$this->validate($request, [
            'profile'=>'required|string'
        ]);

    	$provider = new ExpressCheckout;

    	
    	$u = User::where('billing_plan.PROFILEID',$request->input('profile'))->firstOrFail();
    	if(!$u || (Auth::user()->_id != $u->_id && Auth::user()->role < 3)) return response()->json(['message'=>'error.empty'],404);		
		
    	$response = $provider->suspendRecurringPaymentsProfile($request->input('profile'));
		if($response['ACK'] != 'Success') return response()->json(['message'=>'error.paypal'],403);


		$billing = $u->billing_period;
		$billing['active'] = 0;
		$u->billing_plan = $billing;
		$u->save();
		
		return response()->json(['message'=>'ok'],200);

    }

    /**
    *
    * Reactivate
    * 
    * @param Illuminate\Http\Request $request
    * @return Illuminate\Http\Response $response
    * ***/
    public function reactivateBillingPlanToUser(Request $request){
    	$this->validate($request, [
            'profile'=>'required|string'
        ]);

    	$provider = new ExpressCheckout;

    	
    	$u = User::where('billing_plan.PROFILEID',$request->input('profile'))->firstOrFail();
    	if(!$u || (Auth::user()->_id != $u->_id && Auth::user()->role < 3)) return response()->json(['message'=>'error.empty'],404);		
		
    	$response = $provider->reactivateRecurringPaymentsProfile($request->input('profile'));
		if($response['ACK'] != 'Success') return response()->json(['message'=>'error.paypal'],403);


		$billing = $u->billing_period;
		$billing['active'] = 1;
		$u->billing_plan = $billing;
		$u->save();
		
		return response()->json(['message'=>'ok'],200);

    }

    /**
    *
    * Cancel paypal recurring
    * 
    * @param Illuminate\Http\Request $request
    * @return Illuminate\Http\Response $response
    * ***/
    public function cancelBillingPlanToUser(Request $request){

    	$this->validate($request, [
            'profile'=>'required|string'
        ]);

    	$provider = new ExpressCheckout;

    	
    	$u = User::where('billing_plan.PROFILEID',$request->input('profile'))->firstOrFail();
    	if(!$u || (Auth::user()->_id != $u->_id && Auth::user()->role < 3)) return response()->json(['message'=>'error.empty'],404);		
		

		$response = $provider->cancelRecurringPaymentsProfile($request->input('profile'));
		if($response['ACK'] != 'Success') return response()->json(['message'=>'error.paypal'],403);


		$billing = $u->billing_period;
		$billing['active'] = 0;
		$u->billing_plan = []; // lo cancello completamente
		$u->save();
		
		return response()->json(['message'=>'ok'],200);

    }

    /**
    *
    * Verifica status pagamento e disattivalo dal profilo utente se non attivo
    * 
    * @param string $profileid
    * @return null
    * ***/
    public function verifyBillingPlanStatus($profileid){

    	$provider = new ExpressCheckout;

    	$u = User::where('billing_plan.PROFILEID',$profileid)->firstOrFail();
    	if(!$u) return response()->json(['message'=>'error.empty'],404);		
		
    	$billing = $u->billing_period;
		if($billing['active'] == 1):
			$response = $provider->getRecurringPaymentsProfileDetails($profileid);
			if($response['PROFILESTATUS'] != 'ActiveProfile') :
				$billing['active'] = 0;
				$u->billing_plan = $billing;
				$u->save();
			endif;
		endif;

    }

    /**
    *
    * Starting using paypal recurring
    * 
    * @param string $profileid
    * @return Illuminate\Http\Response $response
    * ***/
    public function setUserBillingPlan(Request $request){
    	
    	$this->validate($request, [
            'plan'=>'required|alpha_num',
            'period'=>'required|integer' // 0 -> mensile, 1 => annuale
        ]);

        $u = User::find(Auth::user()->_id);
        if(!$u) return response()->json(['message'=>'error.empty'],403);

        $plan = Pacchetti::find($request->input('plan'));
		$price = ($request->input('period') == 0) ? $plan->price_month : $plan->price_annual;
		$billing = ($request->input('period') == 0) ? 'Month' : 'Year';

    	$provider = new ExpressCheckout;



    	$data = [];
		$data['items'] = [
		    [
		        'name' => $plan->name,
		        'price' => $price,
		        'qty' => 1
		    ]
		];

		$data['invoice_id'] = "#".$plan->_id."-".$u->_id;
		$data['invoice_description'] = "Iscrizione al piano di abbonamento ".$plan->name. " di MangiaeBevi";
		$data['return_url'] = url('/payment/success');
		$data['cancel_url'] = url('/');

		$total = 0;
		foreach($data['items'] as $item) {
		    $total += $item['price'];
		}

		$data['total'] = $total;

		$response = $provider->setExpressCheckout($data, true);

		if($response['ACK'] != 'Success') return response()->json(['message'=>'error.paypal'],403);


		$token = $response['TOKEN'];
		$paypal_link = $response['paypal_link'];

		$billing_plan = [
			'active'=>0, // ancora non attivo
		 	'token'=>$token,
		 	'payment_id'=>'',
		 	'plan'=>$plan->_id,
		 	'period'=>$request->input('period')// 0->mese,1=>anno
		];
		$u->billing_plan = $billing_plan;
		$u->save();

		
		return response()->json(['message'=>'ok','redirect'=>$paypal_link],200);

			
    }
}
