<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;
use MangiaeBevi\Plates;
use MangiaeBevi\Restaurant;


use Log;

class PlatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $statusCode = 200;
        $response = [
          'plates'  => Plates::all()
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewSingle(Request $request)
    {
        //
        $this->validate($request, [
            'plate'  => 'required|alpha_num'
        ]);

        $p = Plates::find($request->input('plate'));
        
        if(!$p) return response()->json(['message'=>'error.empty'],404);
        
        return response()->json(['message'=>'ok','plate'=>$p],200);
    }

    /**
     * Display a listing of the resource.
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function indexByRestaurant(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num'
        ]);
        $statusCode = 200;
        $response = [
          'plates'  => Plates::where('restaurant',$request->input('restaurant_id'))->get()
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant_id'  => 'required|alpha_num',
            'id'  => 'required|alpha_num'
        ]);
        $statusCode = 200;
        $plates = Plates::find($request->input('id'))->delete();

        $restaurant = Restaurant::find($request->input('restaurant_id'));
        
        // togli il piatto dal menu
        $menus = $restaurant->menu;
        $m = 0;    
        foreach($menus['menus'] as $menu):
            $c = 0;
            foreach($menu['categorie'] as $categoria):
                $p = 0;
                foreach ($categoria['piatti'] as $piatto):
                    
                    if($piatto['id'] == $request->input('id')):
                        unset($menus['menus'][$m]['categorie'][$c]['piatti'][$p]);
                    endif;
                    
                    // metti id nel piatto
                    //$menus['menus'][$m]['categorie'][$c]['piatti'][$p] = $piatto;

                    $p++;
                endforeach; // piatto
                $c++;
            endforeach;// categoria
            $m++;
        endforeach;// menu
        
        $restaurant->menu = $menus;
        $restaurant->save();
        
        return response()->json(['message'=>'ok'], $statusCode);
    }


    

    
}
