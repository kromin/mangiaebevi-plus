<?php
/**
*
* @todo export, quelli con virtual tour non cancellare, tenere quelli inviati da fabio
* 
* 
* 
* ***/
namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use Jenssegers\Mongodb\Model as Eloquent;
use Intervention\Image\ImageManager;
use Image;
use MangiaeBevi\Http\Controllers\GoogleAddressController as Address;
use MangiaeBevi\Restaurant;
use MangiaeBevi\Plates;
use MangiaeBevi\UserPlate;
use MangiaeBevi\HomeCity;
use Log;
use Storage;
use File;
use DB;

use Carbon\Carbon;

use MangiaeBevi\Services;
use MangiaeBevi\Types;
use MangiaeBevi\Regions;
use MangiaeBevi\TipoLocale;
use MangiaeBevi\User;
use MangiaeBevi\Images;

use Srmklive\PayPal\Services\ExpressCheckout;

class AdministrationController extends Controller
{


    public function setSlugsForCustom(){
        $t = Types::all();
        foreach ($t as $type) {
            $new = Types::find($type->_id);
            $slug = str_slug($new->name);
            $new->slug = $slug;
            $new->save();
        }

        $t = Regions::all();
        foreach ($t as $type) {
            $new = Regions::find($type->_id);
            $slug = str_slug($new->name);
            $new->slug = $slug;
            $new->save();
        }

        $t = TipoLocale::all();
        foreach ($t as $type) {
            $new = TipoLocale::find($type->_id);
            $slug = str_slug($new->name);
            $new->slug = $slug;
            $new->save();
        }

        $t = Services::all();
        foreach ($t as $type) {
            $new = Services::find($type->_id);
            $slug = str_slug($new->name);
            $new->slug = $slug;
            $new->save();
        }
        echo 'ok';
    }

    public function getNotImagesFromRistos(){
        $i = Restaurant::where('approved','!=',2)->orderBy("name","asc")->get();
        $ret = '';
        $n = 0;
        $risto_num = 0;
        $image_num = 0;
        $array_ristoranti = [];
        foreach ($i as $risto) {
            if(isset($risto->images) && count($risto->images) > 0){
                $n = 0;
                foreach ($risto->images as $image_id) {
                    if(!file_exists('../../'.env('APP_STATIC_PATH').'/restaurant/small/'.$image_id.'.png')) :
                        $n++; 
                    echo $image_id.'<br />';
                        $image_num++;                          
                    endif;
                } 
                if($n > 0){
                    $risto_num++;
                    $ret .= "<b>".$risto->name."</b> <br /><small>".$risto->address.", ".$n." immagini mancanti</small><br /><br />";
                }
            }
                
        }
        $ret .= "<b>Num ristoranti da aggiornare: </b>".$risto_num."<br /><br />";
        $ret .= "<b>Num immagini totali: </b>".$image_num."<br /><br />";
        echo $ret;
    }

    public function getNotImages(){
        $i = Images::where('type','restaurant')->get();
        $ret = '';
        $n = 0;
        $array_ristoranti = [];
        foreach ($i as $image) {
                
            if(!file_exists('../../'.env('APP_STATIC_PATH').'/restaurant/small/'.$image->_id.'.png')) :
                $r = Restaurant::find($image->ref_id);
                if($r):
                    if(!in_array($r->name, $array_ristoranti)):
                    $ret .= "<b>".$r->name."</b> <br /><small>".$r->address."</small><br /><br />";
                    $array_ristoranti[] = $r->name;
                    $n++;
                    endif;
                endif;    
            endif;

        }
        $ret .= "Numero ristoranti: ".$n;
        echo $ret;
    }

    /**
    *
    * Vedi pagine in cache
    * 
    * */
    public function SetPasswordCiaoAndSocials(Request $request){
        User::whereNotNull('name')->update(
            ['friends'=>[],
            'friend_request'=>[],
            'following'=>[],
            'followedby'=>[],
            'foodcoin'=>env('FOODCOIN_REGISTRATION'),
            'current_address'=>'',
            'address_list'=>[],
            'favourite_list'=>[],
            'wishlist'=>[],
            'recommended_plates'=>[],
            'recommended_restaurants'=>[],
            'billing_plan' => []
            ]);
        Restaurant::whereNotNull('name')->update(
            ['recommendations'=>0]);
        Plates::whereNotNull('name')->update(
            ['recommendations'=>0]);
    }


    public function verifyPayments(){
        $u = User::where('billing_plan.active',1)->get();
        $provider = new ExpressCheckout;
        foreach($u as $user){
            $billing = $user->billing_period;
            if($billing['active'] == 1):
                $response = $provider->getRecurringPaymentsProfileDetails($profileid);
                if($response['PROFILESTATUS'] != 'ActiveProfile') :
                    $billing['active'] = 0;
                    $billing['PROFILESTATUS'] = $response['PROFILESTATUS'];
                    $user->billing_plan = $billing;
                    $user->save();
                endif;
            endif;
        }
    }



    public function replaceRegioneTipologia($nome){
        $n = Regions::where('name',$nome)->get();
       
        $ristoranti_aggiornati = [];

        if(count($n) > 0)  {
            echo 'ID regione '.$n[0]->_id.'<br />';
            echo 'Ce ne sono '.count($n);

            $regione_id = $n[0]->_id;
            } else {
                echo 'Vuota: '.$nome.'<br />';
                echo 'Non inserisco nulla<br />';
                die();
            }

        $t = Types::where('name',$nome)->get();
        $id_nuova_tipologia = '';
        if(count($t) > 0)  {
            echo 'ID tipologia già inserita '.$t[0]->_id.'<br />';
            echo 'Ce ne sono '.count($t);
            $id_nuova_tipologia = $t[0]->_id;
            
            } else {
                echo 'Vuota la tipologia: '.$nome.'<br />';
                echo 'La inserisco<br />';

                $add_t = new Types();
                $add_t->name = $nome;
                $add_t->save();

                $id_nuova_tipologia = $add_t->_id;
            }

        // tolgo la regione e la metto come tipologia
        $restaurants = Restaurant::whereIn('regioneCucina',[$regione_id])->get();
        foreach ($restaurants as $r) {
            $risto = Restaurant::find($r->_id);
            $regioni = [];
            foreach ($risto->regioneCucina as $regione) {
                if($regione != $regione_id) $regioni[] = $regione;
            }
            $risto->regioneCucina = $regioni;

            $ha_già_la_tipologia = false;
            $tipi = [];

            if($risto->tipoCucina):
                foreach ($risto->tipoCucina as $tipo) {
                    // di default li aggiungo
                    $tipi[] = $tipo;
                    // se aveva già la tipologia lo imposto true e evito di aggiornare
                    if($tipo == $id_nuova_tipologia)  $ha_già_la_tipologia = true;
                }
            endif;
            
            if(!$ha_già_la_tipologia) {
                $tipi[] = $id_nuova_tipologia;
                $risto->tipoCucina = $tipi;
            }

            $ristoranti_aggiornati[] = $risto->name;
            $risto->save();
        }

        var_dump($ristoranti_aggiornati);

    }


    /**
    *
    * Vedi pagine in cache
    * 
    * */
    public function getStaticPages(Request $request){
        $files = Storage::disk('local')->files('static');
        var_dump($files);
    }
    /**
    *
    * Cancella pagine in cache
    * 
    * */
    public function deleteStaticPages(Request $request){
        Storage::disk('local')->deleteDirectory('static');
        echo 'ok';
    }

    public function resetApprovedByStageur(Request $request){
        $r = Restaurant::where('approved',1)->where('imported_stageur',1)->skip(0)->limit(264)->orderBy('created_at','desc')->get();
        echo json_encode($r);
        foreach($r as $restaurant){
            $single =  Restaurant::find($restaurant->_id);
            if($single):
                //$single->approved = 0;
                //$single->save();
            endif;
        }
    }

    /**
    *
    * Resetta indirizzo ristorante mettendo città e cap, e se presente il distretto
    * 
    * */
    public function setRestaurantsCities(Request $request){

        $return_array = [];
        $r = Restaurant::all();
        
        foreach ($r as $restaurant) :
            
            $single =  Restaurant::find($restaurant->_id);
            
            if($single->city && $single->zip):
                $city =  $single->city;
                $zip = $single->zip;
                $state = $single->state;
                if(!preg_match('/'.$city.'/i', $single->address) 
                    && !preg_match('/'.$zip.'/i', $single->address)
                    && !preg_match('/'.$state.'/i', $single->address)
                    ) :
                    $single->old_address = $single->address;
                    $single->edited_address_automatically = 1;
                    $single->address = $single->address. ' - '.$zip.' '.$city .' ('.$single->state.')';
                    $return_array[] = $single->address;
                    $single->save();
                endif;
            endif;

        endforeach;
        echo json_encode($return_array);
    }

    /**
    *
    * Crea immagini per mobile app 900x600
    * 
    * */
    public function createAppRestaurantImages(Request $request){
        $files = scandir ('../../'.env('APP_STATIC_PATH').'/restaurant/big');
        $added = scandir ('../../'.env('APP_STATIC_PATH').'/restaurant/mobile');

        $max = count($files) - count($added);
        $n = 0;
        foreach ($files as $file_name) {
            if($file_name != '.' && $file_name != '..' && $n < 10):
                try{
                    if(!file_exists('../../'.env('APP_STATIC_PATH').'/restaurant/mobile/'.$file_name)):
                        $img = Image::make('../../'.env('APP_STATIC_PATH').'/restaurant/big/'.$file_name);
                        $img->fit(900, 600);
                        $img->save('../../'.env('APP_STATIC_PATH').'/restaurant/mobile/'.$file_name, 100);
                        Log::info('salvata: '.$file_name);
                        $n++;
                    endif;
                    
                } catch(Exception $e){
                    Log::info('Errore immagine '.$file_name);
                }
            endif;
        }
        if($n == 10):
            echo $n.' caricate, faccio il redirect';
            return redirect('api/v1/administration/mobile_images');
        else:            
            echo 'fine modifica';
        endif;
    }

    /**
    *
    * Approva i nuovi ristoranti
    * 
    * */
    public function approveNewRestaurants(Request $request){
        $r = Restaurant::where('approved',0)->get();
        echo count($r);
        foreach ($r as $restaurant) :
            
            $single =  Restaurant::find($restaurant->_id);
            //$ret = Address::returnDistrict($single->loc['coordinates'][1],$single->loc['coordinates'][0]);
            //$single->district = ($ret != '') ? [$ret] : [];
            //$single->valid_district = ($ret != '') ? 1 : 0;
            if($single->city && $single->cap):
                $city =  $single->city;
                $cap = $single->cap;
                if(!preg_match('/'.$city.'/i', $single->address) || !preg_match('/'.$cap.'/i', $single->address)) $single->address .= $single->address.', '.$cap.', '.$city;
            endif;
            $single->approved = 1;
            $single->imported_stageur = 1;
            $single->save();

        endforeach;
        echo 'ok';
    }

    /**
    *
    * Imposta i distretti dei ristoranti come valid == 0
    * 
    * */
    public function resetRestaurantDistricts(Request $request){
        Restaurant::where('valid_district',1)->update(['valid_district',0]);
        
        echo 'updated';
    }

    /**
    *
    * Vedi quanti distretti mancano da sistemare
    * 
    * */
    public function howMuchDistricts(Request $request){
        $mancanti = DB::table('restaurant_collection')->where('valid_district',0)->count();
        $fatti =  DB::table('restaurant_collection')->where('valid_district',1)->count();
        $totali = DB::table('restaurant_collection')->count();
        echo $fatti .' su '.$totali . 'e mancanti '.$mancanti;
    }

    /**
    *
    * Imposta i distretti dei ristoranti
    * 
    * */
    public function setRestaurantDistricts(Request $request){

        $r = Restaurant::where('valid_district',0)->take(50)->get(['_id']);
        $n = 0;
        foreach ($r as $restaurant) :
            
            $single =  Restaurant::find($restaurant->_id);
            $ret = Address::returnDistrict($single->loc['coordinates'][1],$single->loc['coordinates'][0]);
            $single->district = ($ret != '') ? [$ret] : [];
            $single->valid_district = 1;
            $single->save();
            $n++;
        endforeach;
        echo $n . 'updated';
    }

    /**
    *
    * Imposta i prezzi dei ristoranti
    * 
    * */
    public function setRestaurantPrices(Request $request){
        $r = Restaurant::all();
        foreach ($r as $restaurant) {

            $single =  Restaurant::find($restaurant->_id);

            $price = 35;
            $price_max = 50;
            $price_min = 20;

            if($single->priceMax && $single->priceMax != '') $price_max = $single->priceMax; 
            if($single->priceMin && $single->priceMin != '') $price_min = $single->priceMin; 
            if($single->price && $single->price != '') $price = $single->price; 

            $single->priceMax = $price_max;
            $single->priceMin = $price_min;

            $single->price = intval( (($price_max + $price_min) / 2 ) );

            $single->save();

        }
    }

    /**
    *
    * Dai un rating ai ristoranti in base alle informazioni che danno
    * 
    * */
    public function setRestaurantRating(Request $request){
        $r = Restaurant::all();
        foreach ($r as $restaurant) {

            $single =  Restaurant::find($restaurant->_id);

            $rating = count($single->location);
            // aggiungi i caratteri della descrizione / 150
            $rating += ($single->description != '') ? (strlen($single->description) / 150) : 0;
            
            // aggiungi 20 per immagine principale
            $rating += ($single->image != '') ? 20 : 0;
            // aggiungi 3 per ogni immagine oltre alla principale
            $rating += (count($single->images) > 1) ? count($single->images)*3 : 0;

            if($single->prenotazione == 1) $rating += 20;
            if($single->ordina == 1) $rating += 20;


            $rating += (count($single->services) > 0) ? count($single->services)*2 : 0;
            $rating += (count($single->tipoCucina) > 0) ? count($single->tipoCucina)*2 : 0;
            $rating += (count($single->regioneCucina) > 0) ? count($single->regioneCucina)*2 : 0;

            if($single->user && $single->user != 0) $rating += 30;

            $single->rating = $rating;
            $single->save();

        }
    }

    /**
     * Mette gli slug a tutti i ristoranti
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string 'finito'
     */
    public function cleanAllDistricts(Request $request)
    {
        //
        $restaurant = Restaurant::all();

        
        foreach ($restaurant as $ristorante) {
            $r = Restaurant::find($ristorante->_id);
            if($r):
                if($r->district && $r->valid_district == 1 && !is_array($r->district) && $r->district != '') :
                    $r->district = [$r->district];
                    $r->save();
                endif;
            endif;                        
            
        }

        echo 'finito';
    }

    /**
     * Mette gli slug a tutti i ristoranti
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string 'finito'
     */
    public function setAllSlug(Request $request)
    {
        //
        $restaurant = Restaurant::where('approved',1)->get();

        $added_slug = []; // metto gli slug in array


        foreach ($restaurant as $ristorante) {
            $r = Restaurant::find($ristorante->_id);
            
            $r->slug = str_slug($r->name);
            $n = 2;
            if(in_array($r->slug, $added_slug)):
                while(in_array($r->slug.'-'.$n, $added_slug)):
                    $n++;
                endwhile;
                $r->slug = $r->slug.'-'.$n;
            endif;
            $added_slug[] = $r->slug;
            
            $r->save();
        }

        echo 'finito';
    }

    

    /**
    *
    * Imposta tag per query di ricerca
    * 
    * */
    public function mapRestaurantsTags(Request $request){

        $tipi_locali = TipoLocale::all();
        $tipi_cucina = Types::all();
        $regioni_cucina = Regions::all();
        $servizi = Services::all();

        $r = Restaurant::all();
        foreach ($r as $restaurant) {
            $single =  Restaurant::find($restaurant->_id);
            // per indirizzo prendo il secondo e gli ultimi 3
            $tags = [];

            $tags[] = $single->name;

            if($single->valid_district && $single->valid_district == 1 && count($single->district) > 0) $tags[] = $single->district[0];

            $len = count($single->location);

            $tags[] = strtolower($single->name);

            if(isset($single->location[1]))
                $tags[] = strtolower($single->location[1]); // indirizzo
            if(isset($single->location[2]))
                $tags[] = strtolower($single->location[2]); // città
            if(isset($single->location[3]))
                $tags[] = strtolower($single->location[3]); // città[2]
            
            if($len > 5):
                
                if(isset($single->location[$len-1]))
                    $tags[] = strtolower($single->location[$len-1]); // cap
                if(isset($single->location[$len-2]))
                    $tags[] = strtolower($single->location[$len-2]); // nazione
                if(isset($single->location[$len-3]))
                    $tags[] = strtolower($single->location[$len-3]); // regione
            
            endif;



            // tipo locale
            $n = ($single->tipoLocale && !empty($single->tipoLocale[0])) ? $this->returnTheName($single->tipoLocale[0],$tipi_locali) : '';
            if($n != '') $tags[] = strtolower($n);

            // servizi
            if(isset($single->services) && is_array($single->services) && !empty($single->services)) :
                foreach ($single->services as $service_id) {
                    $n = $this->returnTheName($service_id,$servizi);
                    if($n != '') $tags[] = $n;
                }
            endif;
            // regione cucina
            if(isset($single->tipoCucina) && is_array($single->tipoCucina) && !empty($single->tipoCucina)) :
                foreach ($single->tipoCucina as $tipo_id) {
                    $n = $this->returnTheName($tipo_id,$tipi_cucina);
                    if($n != '') $tags[] = $n;
                }
            endif;
            // regioni cucina
            if(isset($single->regioneCucina) && is_array($single->regioneCucina) && !empty($single->regioneCucina)) :
                foreach ($single->regioneCucina as $regione_id) { 
                    $n = $this->returnTheName($regione_id,$tipi_cucina);
                    if($n != '') $tags[] = $n;
                }
            endif;
                    

            $single->tags = $tags;
            $single->tags_string = (count($tags) > 0) ? implode(", ", $tags) : '';
            $single->save();
        }
    }

    /**
    * Prendi il tipo di locale
    * 
    * **/
    private function returnTheName($id,$array_all){
        $ret = '';
        foreach ($array_all as $obj) 
            if($obj->_id == $id) $ret = $obj->name;  

        return $ret;      
    }


    public function cancelAllTags(Request $request){
        Restaurant::all()->update("tags",null);
    }

    /**
     * Genera la sitemap
     * 
     * prende le pagine statiche
     * prende i ristoranti con ordinazione o take away, prende le loro città e genera gli url relativi 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generateSitemap()
    {
        //
        $app_url = env('APP_PROTOCOL').'://'.env('APP_DOMAIN');
        $static_pages = [
        'chi-siamo','lavora-con-noi','contattaci','come-funziona','programma-fedelta','termini-e-condizioni','privacy-policy','promuovi-il-tuo-ristorante','aggiungi-un-ristorante'
        ];
        $sitemap_name = 'sitemap.xml';


        // Iniziamo la costruzione

        $sitemap_content = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        // home page
        $sitemap_content .= '
        <url><loc>'.$app_url.'/</loc><changefreq>monthly</changefreq><priority>1.0</priority></url>
        ';
        foreach ($static_pages as $page) {
            $sitemap_content .= '
            <url><loc>'.$app_url.'/'.$page.'</loc><changefreq>monthly</changefreq><priority>0.8</priority></url>
            ';
        }

        /**
        *
        * Loop città in home page
        * 
        * */
        $homecities = HomeCity::where("website",1)->orWhere("website",2)->get();
        foreach ($homecities as $city) {
            $name = $city->name;
            $sitemap_content .= '
            <url><loc>'.$app_url.'/citta/'.urlencode($name).'/</loc><changefreq>monthly</changefreq><priority>0.6</priority></url>
            ';
        }
        $restaurants = Restaurant::where('approved',1)->get();
        /**
        *
        * Fai loop dei ristoranti
        * 
        * */
        $cities = [];
        foreach ($restaurants as $restaurant) :
            if($restaurant->city) :
            $sitemap_content .= '
            <url><loc>'.$app_url.'/'.urlencode($restaurant->city).'/'.$restaurant->slug.'/</loc><changefreq>monthly</changefreq><priority>0.5</priority></url>
            ';
            endif;
        endforeach;

        /**
        *
        * Loop profili utenti
        * 
        * */
        $users = User::all();
        foreach ($users as $user) {
            $sitemap_content .= '
            <url><loc>'.$app_url.'/u/'.$user->_id.'/</loc><changefreq>monthly</changefreq><priority>0.5</priority></url>
            ';
        }

        /**
        *
        * Loop profili utenti
        * 
        * */
        $plates = Plates::all();
        foreach ($plates as $plate) {
            $sitemap_content .= '
            <url><loc>'.$app_url.'/piatto/'.$plate->_id.'/</loc><changefreq>monthly</changefreq><priority>0.5</priority></url>
            ';
        }

        /**
        *
        * Loop piatti community
        * 
        * */
        $plates = UserPlate::all();
        foreach ($plates as $plate) {
            $sitemap_content .= '
            <url><loc>'.$app_url.'/community/piatto/'.$plate->_id.'/</loc><changefreq>monthly</changefreq><priority>0.5</priority></url>
            ';
        }

        $sitemap_content .= '</urlset>';

        File::put(public_path().'/'.$sitemap_name, $sitemap_content);
        Log::info('sitemap generata il '.Carbon::now('Europe/London')->toDateString());

    }


    /**
    *
    * Imposta la descrizione per il SEO dei ristoranti
    * @todo aggiungerlo al controller dei ristoranti
    * */
    public function setRestaurantSeoDescription(Request $request){
        $restaurants = Restaurant::all();
        foreach ($restaurants as $restaurant) :
            $risto = Restaurant::find($restaurant->_id);

            if(strlen($risto->description) > 0):
                $desc = htmlspecialchars ( htmlentities (substr( strip_tags( $risto->description ),0,130), "0", "UTF-8" ).'...', ENT_QUOTES);
            else:
                $desc = '';
            endif;
            
            $seoDesc = $risto->name.' '.$desc;
            $risto->seodescription = $seoDesc;
            $risto->save();
        endforeach;
        echo 'fine';
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setAllNotApproved(Request $request)
    {
        //
        $restaurants = Restaurant::all();
        foreach ($restaurants as $restaurant) {
            $risto = Restaurant::find($restaurant->_id);
            $risto->approved = 0;
            $risto->save();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setAllApproved(Request $request)
    {
        //
        $restaurants = Restaurant::all();
        foreach ($restaurants as $restaurant) {
            $risto = Restaurant::find($restaurant->_id);
            $risto->approved = 1;
            $risto->save();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function vediLocationVuota(Request $request)
    {
        //
        $restaurants = Restaurant::all();
        foreach ($restaurants as $restaurant) {
            if(!$restaurant->valid_address || $restaurant->valid_address != 1){
                echo $restaurant->name.'<br />';
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cleanWeb(Request $request)
    {
        $restaurants = Restaurant::all();
        $errors = [];
        foreach ($restaurants as $restaurant) {
            
            $edit = false;

            $contact = $restaurant->contact;
            $phones = $contact['phone'];
            $new_phones = [];
            foreach ($phones as $phone) {
                if(preg_match("/www/i", $phone)){
                    // è il sito web
                    $contact['web'] = $phone;
                    $edit = true;
                }
                elseif(preg_match("/@/i", $phone)){
                    // è la mail
                    $contact['email'] = $phone;
                    $edit = true;
                } elseif($phone != '') {
                    $new_phones[] = $phone;
                } else {
                    // errore, che minchia è???
                    $edit = true;
                }
            }

            $contact['phone'] = $new_phones;

            if(preg_match("|http://http://|i", $contact['web'])){
                $contact['web'] = str_replace("http://http://", "http://", $contact['web']);
                $edit = true;
            }

            if(!preg_match("|http://|i", $contact['web'])){
                $contact['web'] = "http://".$contact['web'];
                $edit = true;
            }

            if($contact['web'] == 'http://'){
                $contact['web'] = "";
                $edit = true;
            }

            if($edit){
                $errors[] = ['ristorante'=>$restaurant->name,'contact'=>$contact];
                $risto = Restaurant::find($restaurant->_id);
                $risto->contact = $contact;
                $risto->save();
            }

        }

        echo json_encode($errors);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cleanOrari(Request $request)
    {
        //
        $restaurants = Restaurant::all();//::where('name','Bellacarne')->get();//
        $tutte_fasce = []; // temporaneo per debug
        foreach ($restaurants as $restaurant) {
            $arr_orari = $restaurant->orari; // array
            $fasce = []; // array di array

            for($x = 0;$x < 7;$x++){
                // per ogni giorno fai il parsing delle fasce orarie
                // prendi ogni carattere e se numerico vallo a mettere come orario
                // $arr_orari[$x]
                $day = $arr_orari[$x][0];
                //echo json_encode($day) . $restaurant->name; exit();
                $day_arr = str_split($day);
                
                $fasce_del_giorno_corrente = [];
                // ogni fascia ha 2 orari: 00:00 00:00
                // in totale devo arrivare a 8 numeri e 2 fasce da 4

                $fascia_corrente = [];
                //var_dump($day_arr);
                for ($c = 0; $c <= count($day_arr); $c++) {
                    if(isset($day_arr[$c])):    
                        $char = $day_arr[$c];
                        //echo 'fascia: '.json_encode($fascia_corrente).'<br />';
                        
                        if((is_numeric($char) || $char == '0') && $char != '-' && $char != ':' && $char != ' ' && $char != '.'):
                            // il carattere è numerico
                            //echo 'sto inserendo: <b>'.$char.'</b><br />';
                            $fascia_corrente[] = $char;

                            if(count($fascia_corrente) == 8):
                                // abbiamo completato una fascia da - a
                                // costruisco la stringa per la fascia oraria
                                // la chiamo f per comodità
                                $f = $fascia_corrente;
                                $stringa =  $f[0].$f[1].":"
                                            .$f[2].$f[3]."-"
                                            .$f[4].$f[5].":"
                                            .$f[6].$f[7];

                                $fasce_del_giorno_corrente[] = $stringa;

                                //echo $stringa.'<br />';
                                // resetto la fascia corrente
                                $fascia_corrente = [];
                            endif;

                        else:
                            //echo 'non metto '.$char.'<br />';

                            // se : o . il precedente è un orario, di conseguenza devo avere almeno 2 caratteri prima
                            if($char == ':' || $char == '.'):
                                
                                $len_fascia = count($fascia_corrente);
                                if($len_fascia == 1){
                                    // è il primo orario, verifico che ci siano il numero 0 e il numero 1
                                    if(isset($fascia_corrente[0]) && isset($fascia_corrente[1])){
                                        // è tutto ok, abbiamo sia il primo che il secondo orario
                                    } else {
                                        $current_char = $fascia_corrente[0];
                                        $fascia_corrente[0] = '0';
                                        $fascia_corrente[1] = $current_char;
                                    }
                                } elseif($len_fascia == 5) {
                                    if(isset($fascia_corrente[4]) && isset($fascia_corrente[5])){
                                        // è tutto ok, abbiamo sia il primo che il secondo orario
                                    } else {
                                        // manca uno dei due
                                        $current_char = $fascia_corrente[4];
                                        $fascia_corrente[4] = '0';
                                        $fascia_corrente[5] = $current_char;
                                    }
                                }
                            endif;

                        endif;

                    endif;
                }
                // finito il parsing, ora aggiorno l'array di fasce mettendo quelle del giorno corrente
                $fasce[] = $fasce_del_giorno_corrente;

            }

            $risto = Restaurant::find($restaurant->_id);
            $risto->fasce = $fasce;
            $risto->save();
            
            $tutte_fasce[] = ['restaurant'=>$restaurant->name,'fasce'=>$fasce];
        }

        echo json_encode($tutte_fasce);
    }

     /**
     * Imposta località dei ristoranti
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function locationRestaurants(Request $request)
    {
        //
        $restaurant = Restaurant::where('valid_address','!=',1)->get();
        foreach ($restaurant as $ristorante) {
            $r = Restaurant::find($ristorante->_id);
            $location = false;
            
            if(!$r->valid_address || $r->valid_address != 1):
                $location = Address::returnLocation($r->address);
                if($location):
                
                    $r->location = $location[2];
                    $r->loc = ['type'=>"Point","coordinates"=>[doubleval($location[1]),doubleval($location[0])]];            
                    $r->city = $location[3];
                    $r->state = $location[4];
                    $r->zip = $location[5];
                    $r->valid_address = 1;

                else:
                    echo 'no location for '.$r->name.'<br />';
                endif;
            endif;

            if(!$r->valid_district || $r->valid_district != 1 && isset($location) && $location):
                $zona = Address::returnDistrict($location[0],$location[1]);
                if($zona):
                    $r->district = $zona;
                    $r->valid_district = 1;
                else:
                    echo 'no district for '.$r->name.'<br />';
                endif;
            endif;
            
            $r->save();
        }
        echo 'fatto';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggleSlugAddressAndPhoneEmpty(Request $request)
    {
        //
        $restaurant = Restaurant::all();

        $added_slug = []; // metto gli slug in array


        foreach ($restaurant as $ristorante) {
            $r = Restaurant::find($ristorante->_id);
            $location = false;
            
            if(!$r->valid_address || $r->valid_address != 1):
                $location = Address::returnLocation($r->address);
                if($location):
                
                    $r->location = $location[2];
                    $r->loc = ['type'=>"Point","coordinates"=>[doubleval($location[1]),doubleval($location[0])]];            
                    $r->city = $location[3];
                    $r->state = $location[4];
                    $r->zip = $location[5];
                    $r->valid_address = 1;

                endif;
            endif;

            if(!$r->valid_district || $r->valid_district != 1 && isset($location) && $location):
                $zona = Address::returnDistrict($location[0],$location[1]);
                if($zona):
                    $r->district = $zona;
                    $r->valid_district = 1;
                endif;
            endif;

            $n = 2;
            if(in_array($r->slug, $added_slug)):
                while(in_array($r->slug.'-'.$n, $added_slug)):
                    $n++;
                endwhile;
                $r->slug = $r->slug.'-'.$n;
            endif;
            $added_slug[] = $r->slug;

            $contact = $r->contact;
            $new_phones = [];
            foreach ($contact['phone'] as $number) {
                if($number != ''):
                    $new_phones[] = $number;
                endif;
            }
            $contact['phone'] = $new_phones;
            $r->contact = $contact;

            $r->save();
        }

        echo 'finito';
    }

    /**
     * Rimuovi le bozze di mangiaebevi dal db
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteDrafts()
    {
        //
        $content = Storage::get('bozze.txt');
        $rows        = explode("\n", $content);
        array_shift($rows);

        foreach($rows as $string)
        {
            $risto = Restaurant::where('ID_EXPORT',intval($string))->get();
            if(!$risto || count($risto) == 0){
                Log::info($string);
                echo $string.' non trovato <br />';
            } else {
                $risto[0]->delete();
                echo $string.' cancellato <br />';
            }
        }
        echo 'ok';
    }

    public function setImages(){
        $restaurants = Restaurant::all();

        $this->image_manager = new ImageManager();

        $img_path = 'public/img/restaurants/';
        
        foreach($restaurants as $restaurant){
            $n = 1;
            if(isset($restaurant->image) && $restaurant->image != ""){
                
                if(isset($restaurant->images) && !empty($restaurant->images)){

                    $all_restaurant_images = [];
                    foreach($restaurant->images as $single_image){
                        if(preg_match("/http:/i", $single_image)){
                            if($this->save_image($single_image,$restaurant->_id,$n)){
                                $all_restaurant_images[] = $restaurant->_id.'_'.$n.'.png';
                            } else {
                                $all_restaurant_images[] = $single_image;
                            }
                            $n++;
                        }
                    }
                    $restaurant->images = $all_restaurant_images;
                    $restaurant->save();

                }
            }            
            
        }
    }

    /**
    *
    * Export local data in json export.json
    *
    **/
    public function temporary_export(){
        $restaurants = Restaurant::all();
        $export = [];    
        foreach ($restaurants as $risto) {
            $export[] = $risto;
        }
        $export = json_encode($export);
        Storage::disk('local')->put('export.json', $export);
    }

    /**
    *
    * Simply view the structure of the json db
    **/
    public function viewJson(){
        $restaurants = Restaurant::all();
        for($i = 0;$i < 1; $i++){
            if(isset($restaurants[$i])){
                $response[] = $restaurants[$i];
            }            
        }        
        $response = json_encode($response);
        return $response;
    }

    /**
    *
    * Populate db collections with json data
    *
    **/
    public function temporary_seed(){
        $content = Storage::get('export.json');

        $c = json_decode($content,true);    
        foreach ($c as $risto) {
            // longitudine,latitudine

            // risto coordinates
            /*if(is_float($risto['loc']['coordinates'][0]) && is_float($risto['loc']['coordinates'][1])){
               Restaurant::create($risto);
            }*/
            
            $risto['description'] = str_replace("\"", "", $risto['description']);
            $lng = $risto['loc']['coordinates'][0];
            $lat = $risto['loc']['coordinates'][1];

            if(!is_float($lng) || $lng > 180 || $lng < -180){
                $lng = number_format($lng, 4, '.', '');

                $lng = str_replace(".", "", $lng);
                // sta tra 
                $first = substr ( $lng , 0, 1 );
                if($first == 1){
                    // ok 2 decimali
                    $lng = substr_replace($lng, ".", 2, 0);
                } else {
                    $lng = substr_replace($lng, ".", 1, 0);
                }
                $risto['loc']['coordinates'][0] = floatval($lng);
            }

            if(!is_float($lat) || $lat > 90 || $lat < -90){
                $lat = number_format($lat, 4, '.', '');

                $lat = str_replace(".", "", $lat);
                // sta tra 
                $first = substr ( $lat , 0, 1 );
                $lat = substr_replace($lat, ".", 2, 0);                
                $risto['loc']['coordinates'][1] = floatval($lat);
            }
            
            Restaurant::create($risto);            
            
        }
    }

    private function save_image($image_instance,$id,$n){
            $img_path = 'img/restaurants/';
            try{
                $image = $this->image_manager->make($image_instance);//->resize(300, 200);
                // thumb
                $image->save($img_path.'big/'.$id.'_'.$n.'.png');
                $image->resize(346,230);
                $image->save($img_path.'medium/'.$id.'_'.$n.'.png');
                $image->resize(40,40);
                $image->save($img_path.'small/'.$id.'_'.$n.'.png');
            } catch (Exception $e){
                try{
                    $image_content = file_get_contents($image_instance);
                    $image = $this->image_manager->make($image_content);//->resize(300, 200);
                    // thumb
                    $image->save($img_path.'big/'.$id.'_'.$n.'.png');
                    $image->resize(346,230);
                    $image->save($img_path.'medium/'.$id.'_'.$n.'.png');
                    $image->resize(40,40);
                    $image->save($img_path.'small/'.$id.'_'.$n.'.png');
                }catch (Exception $e){
                    return false;
                } finally {
                    return true;
                }
            } finally{
                return true;
            }
            
    }
}
