<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use Google_Client;
use Google_Service_Analytics;

use MangiaeBevi\ReservationRequest;
use MangiaeBevi\Restaurant;

use DB;
use Carbon\Carbon;
use Log;
class StatController extends Controller
{

    private $analytics;
    private $client;
    private $date;
    private $sessions;
    private $segment;
    private $VIEW_ID;
    private $segmentDimensions;
    public $apiResponse;

    public function __construct(){
        $KEY_FILE_LOCATION = storage_path('app/laravel-analytics/meb10284cc6ea3d.json');
        $this->VIEW_ID = env('ANALYTICS_VIEW_ID');//"<REPLACE_WITH_VIEW_ID>";

        $client = new Google_Client();
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);

        $this->client = $client;
        $this->setAnalytics();

    }

    
    private function setAnalytics(){
        $this->analytics = new Google_Service_Analytics($this->client);//Google_Service_AnalyticsReporting
    }
    


    private function returnDayStats(){
        return $this->analytics->data_ga->get(
            'ga:' . $this->VIEW_ID,
            '30daysAgo',
            'today',
            'ga:pageviews,ga:sessionDuration,ga:uniquePageviews,ga:timeOnPage',
            ['filters'=>'ga:pageTitle=='.$this->restaurant,
             'dimensions'=>'ga:date']
            );
    }

    private function returnMonthStats(){
        return $this->analytics->data_ga->get(
            'ga:' . $this->VIEW_ID,
            '365daysAgo',
            'today',
            'ga:pageviews,ga:sessionDuration,ga:uniquePageviews,ga:timeOnPage',
            ['filters'=>'ga:pageTitle=='.$this->restaurant,
             'dimensions'=>'ga:month']
            );
    }

    private function returnPriceByArray($array){
        $total = 0;
        $n = 0;
        foreach ($array as $price) {
            $n++;
            $total += intval($price['price']);
        }
        return ($n > 0) ? number_format($total/$n, 2, '.', '') : '-';
    }
    /**
    *
    * Sample callback from demo
    * 
    * 
    * */
    public function restaurantStats(Request $request){        
                
        $this->validate($request,[
            'restaurant_id'=>'required|alpha_num'
            ]);
        //56c2e130bffebc28088b4678
        $this->restaurant = $request->input('restaurant_id');//56c2e130bffebc28088b4678';        
        $restaurant = Restaurant::find($this->restaurant);

        $queryBuilder = DB::collection('restaurant_collection');
        $total = 0;

        
        $midprice = $queryBuilder->where('loc', 'near', [
            '$geometry' =>  [
                'type' => 'Point',
                'coordinates' => [floatval($restaurant->loc['coordinates'][0]), floatval($restaurant->loc['coordinates'][1])]
            ],
            '$maxDistance' => 2000,
        ])->get(['price']);
        $midprice = $this->returnPriceByArray($midprice);

        
        $reservations = ReservationRequest::where('restaurant',$this->restaurant)->count();

        /**
        *
        * ga:pageviews,ga:sessionDuration,ga:uniquePageviews,ga:timeOnPage
        * */
        $days = $this->returnDayStats();
        $year = $this->returnMonthStats();

        $res = ['midprice'=>$midprice,'reservations'=>$reservations,'year'=>$year->getRows(),'days'=>$days->getRows()];
        //echo(json_encode($res));            
        //exit;

        return response()->json(['message'=>'ok','stats'=>$res],200);
        
        
    }

    
}
