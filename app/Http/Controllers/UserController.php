<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;
use Carbon\Carbon;

use Jenssegers\Mongodb\Model as Eloquent;
use Auth;
use Cookie;
use Curl;
use MangiaeBevi\User;
use MangiaeBevi\Restaurant;
use DB;

use Image; // intervention Image
use File;
use Input;

use Event;
use MangiaeBevi\Events\UserRegistered;
use MangiaeBevi\Events\UserChangedEmail;


use MangiaeBevi\Http\Controllers\GoogleAddressController as Address;
use MangiaeBevi\Http\Controllers\ImagesController as SingleImage;
use MangiaeBevi\Http\Controllers\NotificationsController as Notification;

use Log;
use Hash;
use Mail;
use MangiaeBevi\Http\Controllers\UserActionsController as UserActionStatic;

use MangiaeBevi\Http\Controllers\RoutingController;

/**
 *
 * @package MangiaeBevi\Http\Controllers
 *
 * @method json index()                     show all users
 * @method json store()                     add a new user
 * @method json addPaymentsToClient()       add the day of payment to user account last_payment
 * @method json changeUserRole()            change the user role performed by administrator
 * @method json changeUserEmailByAdmin()    change the user email address performed by administrator
 * @method json changeUserEmail()           change the user email address
 * @method json changeUserPasswordByAdmin() change the user password performed by administrator
 * @method json changeUserPassword()        change the user password
 * @method json show()                      show user info
 * @method json destroy()                   delete user from db collection
 */
class UserController extends Controller
{

    protected $avaible_mime = ['image/png','image/jpg','image/jpeg'];
    /**
     * Display a listing of the users.
     *
     * @return json json.message string, ok
     * @return array json.users array with users info
     */
    public function index()
    {
        //
        $users = User::all();
        return response()->json(['message'=>'ok','users'=>$users], 200);
    }


    /**
     * Store a new user, add foodcoin
     *
     * @param  \Illuminate\Http\Request  $request email|required name|string|required surname|string|required pwd|string|required role|integer
     * @return json json.message string, ok for user added
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'name' => 'required|string',
            'surname' => 'required|string',
            'pwd' => 'required|min:1|max:50',
            'phone' => 'numeric',
            'role' => 'integer|max:2'
        ]);


        try{
            $statusCode = 200;
            $response = [
              'message' => 'ok'
            ];

            $u = User::where('email',$request->input('email'))->get();

            if(count($u) <= 0):
                $user = new User();
            else:
                $user = User::find($u[0]->_id);
                if(!$user->added_by_restaurant || $user->added_by_restaurant != 1) return response()->json(['message'=>'error.just_present'], 200);
            endif;

                $user->name = strip_tags($request->input('name'));
                $user->surname = ($request->input('surname') && $request->input('surname') != '') ? $request->input('surname') : '';
                $user->email = $request->input('email');
                if($request->input('phone')) $user->phone =  $request->input('phone');
                $user->password = bcrypt( $request->input('pwd') );

                $user->verified = 0;
                $user->verification_code = base64_encode( ( $user->name . $request->input('password') . $request->input('surname') . str_random(30) ) );
                $user->role = ($request->input('role')) ? $request->input('role') : 1;

                $user->friends = [];
                $user->friend_request = [];
                $user->following = [];
                $user->followedby = [];
                $user->foodcoin = 0;
                $user->current_address = '';
                $user->address_list = [];
                $user->favourite = [];
                $user->favourite_list = [];
                $user->wishlist = [];
                $user->billing_plan = [];
                $user->recommended_plates = [];
                $user->recommended_restaurants = [];

                if(Cookie::get('cookie')) $user->temp_cookie = Cookie::get('cookie'); // per i favoriti

                if(Cookie::get('ib')) $user->invitedby = Cookie::get('ib'); // per i favoriti

                Event::fire(new UserRegistered($user));


            $user->save();

            $parameters = [$user->_id,$user->name.' '.$user->surname];
            Notification::addNotification(0,$parameters,0);

            return response()->json($response, $statusCode);


        } catch (Exception $e){
            $statusCode = 200;
            return response()->json(['message'=>'Non valido'], $statusCode);
        }
    }

    /**
    * Verify user registration
    *
    * @param $code string verification code
    * @return bool true if found
    *
    */
    public function verify($code)
    {

        $return_ctrl = new RoutingController;
        $alert = false;
        $us = User::where('verification_code',$code)->get();

        if(count($us) > 0) :
            $user = User::find($us[0]->_id);
        else:
            return $return_ctrl->returnHome();
        endif;
        if($user && $user->verified != 1) :
            $user->verified = 1;
            $user->foodcoin = env('FOODCOIN_REGISTRATION');
            //
            if($user->invitedby && $user->invitedby != ''):
                $u = User::find($user->invitedby);
                if($u):
                    $u->foodcoin = $u->foodcoin+intval(env('FOODCOIN_INVITE'));
                    $u->save();
                endif;
            endif;
            $user->save();
            //return response()->json(['message'=>'ok'], 200);
        //else :
        //    return response()->json(['message'=>'error'], 200);
            $alert = true;
        endif;
        return $return_ctrl->returnHome($alert);
    }

    /**
    * Verify user registration
    *
    * @param $code string verification code
    * @return bool true if found
    *
    */
    public function verifyChangedMail($code)
    {

        $return_ctrl = new RoutingController;
        $alert = false;
        $us = User::where('verification_code',$code)->get();

        if(count($us) > 0) :
            $user = User::find($us[0]->_id);
        else:
            return $return_ctrl->returnHome();
        endif;
        if($user && $user->temporary_email) :
            $user->email = $user->temporary_email;
            $user->save();

            $alert = true;
        endif;
        return $return_ctrl->returnHome($alert);
    }

    /**
     * Add Payment added for user, using the carbon date to store the day, this is for clients who have payd account, preotected by middleware admin
     *
     * First get the user and view the last payment, add a month and set the new, if user didn't pay set the first payment to today
     *
     * @param type Request $request id|required day|format Y-m-d
     * @return json json.message ok for payment added
     */
    public function addPaymentsToClient(Request $request)
    {
        Carbon::setToStringFormat('Y-m-d');

        $this->validate($request, [
            'id'=> 'string',
            'day' => 'date_format:Y-m-d'
        ]);


        $user = User::find($request->input('id'));
        if($user->last_payment && $user->last_payment != "") :

            $day = Carbon::parse($user->last_payment('day'))->addMonth(); // the new month day

        else : // user hasn't payd ever

            $day = Carbon::parse($request->input('day'));

        endif; // if user->last_payment

            $user->last_payment = $day;
            $user->save();

            $statusCode = 200;
            $response = [
              'message' => 'ok'
            ];
            return response()->json($response, $statusCode);
    }

    /**
     * Complete User Profile, will call a new event if old state was not setted
     *
     * @param type Request $request user id, address, city, state, zip, born
     * @return json json.message ok for role changed
     */
    public function completeProfile(Request $request){
        $this->validate($request, [
            'user_id'=> 'required|alpha_num',
            'address' => 'required|string',
            'born' => 'required|date_format:Y-m-d'
        ]);

        $user = User::find($request->input('user_id'));
        if(!$user):
            return response()->json(['message'=>'error.empty'], 200);
        endif;


        if(!$user->address || $user->valid_address == 0 || !$user->valid_address || $user->address != $request->input('address')):
            // costruisci indirizzo

            $location = Address::returnLocation($request->input('address'));

            if(!$location) return response()->json(['message'=>'error.no_valid_address'], 403);

            $user->location = $location[2];
            $user->loc = ['type'=>"Point","coordinates"=>[doubleval($location[1]),doubleval($location[0])]];
            $user->address = $request->input('address');
            $user->city = $location[3];
            $user->state = $location[4];
            $user->zip = $location[5];
            $user->valid_address = 1;

        endif;


        $data = Carbon::createFromFormat('Y-m-d', $request->input('born'))->toDateTimeString();
        $user->born = $data;

        $updated = 0;
        if(!$user->profile_completed || $user->profile_completed != 1){
            $user->foodcoin = $user->foodcoin+intval(env('FOODCOIN_COMPLETE'));
            $updated = 1;
        }
        $user->profile_completed = 1;
        $user->save();

        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'user'=>$user,
          'foodcoin'=>9000,//$user->foodcoin,
          'updated'=>$updated // così mostro il warning
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Change user role to the new
     *
     * @param type Request $request 'id' required|alpha_num 'role' required|integer
     * @return json json.message ok for role changed
     */
    public function changeUserRole(Request $request){
        $this->validate($request, [
            'id'=> 'required|alpha_num',
            'role' => 'required|integer'
        ]);

        $user = User::find($request->input('id'));
        if(Auth::user()->role < 5 && $user->role > 4) return response()->json(['message'=>'error.non_puoi'], 200);
        $user->role = $request->input('role');
        $user->save();

        $statusCode = 200;
        $response = [
          'message' => 'ok'
        ];
        return response()->json($response, $statusCode);
    }


    /**
     * Change user email performed by user itself, imposta una mail temporanea "temporary_email"
     *
     * @param type Request $request user_id and email
     * @return json json.message ok for email changed
     */
    public function changeUserEmail(Request $request){
        $this->validate($request, [
            'user_id'=>'required|alpha_num',
            'email' => 'required|email'
        ]);

        $user = User::find($request->input('user_id'));

        $statusCode = 200;
        $response = [
          'message' => 'ok'
        ];

        if(!$user):
            return response()->json(['message' => 'error.empty'], $statusCode);
        endif;

        $user->temporary_email = $request->input('email');
        if(Auth::user()->role > 2) $user->email = $request->input('email');

        if(Auth::user()->_id == $request->input('user_id')) :
            // se sto cambiando la mia email metto verificato no
            //$user->verified = 0;
            $user->verification_code = base64_encode( ( $user->name . $user->surname . str_random(40) ) );
            Event::fire(new UserChangedEmail($user));
        endif;
        $user->save();
        return response()->json($response, $statusCode);
    }


    /**
     * Change user email performed by user itself
     *
     * @param type Request $request pwd|required|string|min:6|max:30 confirmpwd|required|string|min:6|max:30
     * @return json json.message ok for email changed
     */
    public function changeUserPassword(Request $request){
        $this->validate($request, [
            'user_id'=> 'required|alpha_num',
            'oldpwd' => 'string|min:1|max:50',
            'password' => 'required|string|min:1|max:50',
            'confirmpwd' => 'required|string|min:1|max:50'
        ]);

        $statusCode = 200;

        $user = User::find($request->input('user_id'));

        // non sono admin
        if(Auth::user()->role < 4) :
            if (!Auth::attempt(['email' => $user->email, 'password' => $request->input('oldpwd')])) {
                // Authentication passed...
                $response = [
                              'message' => 'error.nooldpwdmatch'
                            ];
                return response()->json($response, $statusCode);
            }
        endif;


        if($request->input( 'password' ) == $request->input( 'confirmpwd' ) ) :
            $user->password = bcrypt( $request->input('password') );

            //$user->friends = [];
            //$user->following = [];
            //$user->friend_request = [];
            //$user->followedby = [];
            //$user->foodcoin = 0;

            $user->save();

            $response = [
                          'message' => 'ok'
                        ];
        else: // password matches
            $response = [
                          'message' => 'error.nodoublepwdmatch'
                        ];
        endif; // no match



        return response()->json($response, $statusCode);
    }


    /**
     * Imposta allergeni
     *
     * @param   $request [array con elenco allergeni]
     * @return $response [json response]
     */
    public function setAllergeni(Request $request)
    {
        $this->validate($request, [
            'allergeni'=>'array'
            ]);

        $u = User::find(Auth::user()->_id);
        $u->allergeni = $request->input('allergeni');
        $u->save();

        return response()->json(['user'=>$u],200);

    }

    /**
     * Recovery user password
     *
     * @param type Request $request email
     * @return json json.message ok for pwd mail sent
     */
    public function recoveryUserPassword(Request $request){
        $this->validate($request, [
            'email'=> 'required|email'
        ]);


        $user = User::where('email',$request->input('email'))->firstOrFail();

        if(!$user) return response()->json(['message'=>'error.empty'],200);
        if(!$user->password || $user->password == '') return response()->json(['message'=>'error.sociallog'],200);

        $new_pwd = str_random(40);
        $crypted_pwd = bcrypt( $new_pwd );

        $user->password = $crypted_pwd;


        $mail_data = [
            'new'=>$new_pwd,
            'email' => $user->email
        ];
        Mail::send('email.recoverypwd', ['mail_data' => $mail_data],
            function ($m) use ($mail_data) {
            $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
            $m->to($mail_data['email'], 'Admin')->subject('Password modificata');
        });

        $user->save();

        return response()->json(['message'=>'ok'], 200);
    }



    /**
     * Recovery user password
     *
     * @param type Request $request email
     * @return json json.message ok for pwd mail sent
     */
    public function resendVerification(Request $request){
        $this->validate($request, [
            'email'=> 'required|email'
        ]);


        $user = User::where('email',$request->input('email'))->firstOrFail();

        if(!$user) return response()->json(['message'=>'error.empty'],403);
        if(!$user->password || $user->password == '') return response()->json(['message'=>'error.sociallog'],403);

        $user->verification_code = base64_encode( ( $user->name . $request->input('password') . $request->input('surname') . str_random(30) ) );


        $mail_data = [
            'verification'=>$user->verification_code,
            'email'=>$user->email
        ];
        Mail::send('email.resendactivation', ['mail_data' => $mail_data],
            function ($m) use ($mail_data) {
            $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
            $m->to($mail_data['email'], 'Admin')->subject('Link di attivazione mangiaebevi');
        });

        $user->save();

        return response()->json(['message'=>'ok'], 200);
    }


    /**
     * Change user role to the new
     *
     * @param type Request $request user_id and name, surname
     * @return json json.message ok for role changed
     */
    public function changeUserInfo(Request $request){
       $this->validate($request, [
            'user_id'=> 'required|alpha_num',
            'name' => 'required|string',
            'surname' => 'string',
            'phone' => 'numeric',
            'description' => 'string'
        ]);


        try{
            $statusCode = 200;
            $response = [
              'message' => 'ok',
              'user'=>null
            ];


            $user = User::find($request->input('user_id'));

            //$name = ($request->input('surname') && $request->input('surname') != '') ? strip_tags($request->input('name')).' '.strip_tags($request->input('surname')) : strip_tags($request->input('name'));
            $user->name = $request->input('name');//$name;
            $user->surname = $request->input('surname');//'';
            if( $request->input('phone')  ) $user->phone = $request->input('phone');
            if( $request->input('description')  ) $user->description = $request->input('description');
            $user->save();

            $response['user']=$user;

        } catch (Exception $e){
            $statusCode = 200;
            return response()->json(['message'=>'error.invalid'], $statusCode);
        } finally{
            return response()->json($response, $statusCode);
        }
    }

    /**
     * Display user info
     *
     * @param  \Illuminate\Http\Request  $request->input('id')
     * @return json json.message string, ok
     * @return array json.user user's info
     */
    public function show(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);

        try{
            $statusCode = 200;
            $response = [
              'message' => 'ok'
            ];

            $user = User::find($request->input('id'));
            if(count($user) > 0){
                $message_displayed = 'ok';
            } else {
                $message_displayed = 'error.empty';
            }

            if(!$user->favourite):
                $user->favourite = [];
            else:
                if(count($user->favourite) > 0):
                    $ristos = Restaurant::whereIn('_id',$user->favourite)->get(['id','name','image','tipoLocale','loc','address','slug','city']);
                    $user->favourite_restaurants = $ristos;
                endif;
            endif;

            if(!$user->wishlist):
                $user->wishlist = [];
            else:
                if(count($user->wishlist) > 0):
                    $ristos = Restaurant::whereIn('_id',$user->wishlist)->get(['id','name','image','tipoLocale','loc','address','slug','city']);
                    $user->wishlist_restaurants = $ristos;
                endif;
            endif;


            if(!$user->friends) $user->friends = [];
            if(!$user->friend_request) $user->friend_request = [];

            if(!$user->foodcoin) $user->foodcoin = 0;
            if(!$user->allergeni) $user->allergeni = [];

            $response = [
                        'user' => $user,
                        'message' => $message_displayed
                        ];
            //return response()->json(['message'=>$message_displayed,'user'=>$user], $statusCode);

        } catch (Exception $e){
            $statusCode = 200;
            return response()->json(['message'=>'error.invalid'], $statusCode);
        } finally{
            return response()->json($response, $statusCode);
        }
    }

    /**
     * Change user avatar
     *
     * @param  \Illuminate\Http\Request  $request->input('user_id') $request->input('image') image data
     * @return json json.message string, ok
     * @return array json.user user's info
     */
    public function changeAvatar(Request $request)
    {

        $this->validate($request, [
            'user_id' => 'required|alpha_num',
            'image' => 'required'//|mimes:jpeg,gif,png,svg
        ]);

        $statusCode = 200;

        $user = User::find($request->input('user_id'));
        if(!$user):
            return response()->json(['message'=>'error.empty'], 200);
        endif;


        $this->deleteAvatar($user);
        $user->avatar = SingleImage::addImages('users',$user->name.' '.$user->surname,$user->name.' '.$user->surname.' profile image',\Input::file('image'),$user->_id);

        $user->save();

        // return new image name
        $response = [
          'message' => 'ok',
          'image' =>  $user->avatar,
          'image_url' => env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/users/medium/'.$user->avatar.'.png'
        ];
        return response()->json($response, $statusCode);

    }

    /**
    *
    * Delete user Avatar
    *
    * @param \User $user object
    * @return true
    *
    * */
    private function deleteAvatar(User $user){
        if($user->avatar && $user->avatar != "") SingleImage::deleteImage($user->avatar,'users');
        return true;
    }



    /**
     * Change user cover image
     *
     * @param  \Illuminate\Http\Request  $request->input('user_id') $request->input('image') image data
     * @return json json.message string, ok
     * @return array json.user user's info
     */
    public function changeCoverImage(Request $request)
    {

        $this->validate($request, [
            'user_id' => 'required|alpha_num',
            'image' => 'required'//|mimes:jpeg,gif,png,svg
        ]);

        $statusCode = 200;

        $user = User::find($request->input('user_id'));
        if(!$user):
            return response()->json(['message'=>'error.empty'], 200);
        endif;


        $this->deleteCoverImage($user);
        $user->cover_image = SingleImage::addImages('users',$user->name.' '.$user->surname,$user->name.' '.$user->surname.' cover image',\Input::file('image'),$user->_id);

        $user->save();

        // return new image name
        $response = [
          'message' => 'ok',
          'image' =>  $user->cover_image,
          'image_url' => env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/users/big/'.$user->cover_image.'.png'
        ];
        return response()->json($response, $statusCode);

    }

    /**
    *
    * Delete user Cover Image
    *
    * @param \User $user object
    * @return true
    *
    * */
    private function deleteCoverImage(User $user){
        if($user->cover_image && $user->cover_image != "") SingleImage::deleteImage($user->cover_image,'users');
        return true;
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request id|string|required
     * @return json response
     */
    public function destroy(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);
        $user = User::find($request->input('id'));
        $user->delete();
        return response()->json(['message'=>'ok'], 200);
    }


    /**
     * Add or remove restaurant from favourite
     *
     * @param  \Illuminate\Http\Request  $request restaurant (required|alpha_num)
     * @return json response
     */
    public function addRemoveFavourite(Request $request)
    {
        //
        $this->validate($request, [
            'restaurant' => 'required|alpha_num'
        ]);
        $user = User::find(Auth::user()->_id);
        if(!$user):
          return response()->json(['message'=>'error.empty'], 200);
        endif;

        if(!$user->favourite):
          $user->favourite = [];
        endif;

        $favourites = $user->favourite;
        if(in_array($request->input('restaurant'), $favourites)):
          // remove
          $index = array_search($request->input('restaurant'), $favourites);
          unset($favourites[$index]);
        else:
          // add
            $favourites[] = $request->input('restaurant');
            UserActionStatic::addUserAction(3,[$request->input('restaurant')]);
        endif;
        if(!isset($favourites)) $favourites = [];
        $user->favourite = $favourites;
        $user->save();
        return response()->json(['message'=>'ok'], 200);
    }


    /**
     * save current lists and favourites
     *
     * @param  \Illuminate\Http\Request  $request restaurant (required|alpha_num)
     * @return json response
     */
    public function setFavourites(Request $request)
    {
        //
        $this->validate($request, [
            'favourites' => 'array',
            'list' => 'array'
        ]);
        $user = User::find(Auth::user()->_id);
        if(!$user):
          return response()->json(['message'=>'error.empty'], 200);
        endif;

        $user->favourite = ($request->input('favourites')) ? $request->input('favourites') : [];
        $user->favourite_list = ($request->input('list')) ? $request->input('list') : [];

        $user->save();
        return response()->json(['message'=>'ok'], 200);
    }


    /**
     * Retrieve restaurant favourite list
     *
     * @param  \Illuminate\Http\Request  $request user_id (required|alpha_num)
     * @return json response
     */
    public function getFavouriteList(Request $request)
    {

        $this->validate($request, [
            'user_id' => 'required|alpha_num'
        ]);
        $user = User::find($request->input('user_id'));
        if(!$user) return response()->json(['message'=>'error.empty'], 200);

        if(!$user->favourite || count($user->favourite) == 0) {
            $restaurants = [];
        } else {
            $favourites = $user->favourite;
            $restaurants = Restaurant::whereIn('_id',$favourites)->get(['_id','slug','name','image','address','loc','city']);
        }

        $lists = ($user->favourite_list) ? $user->favourite_list : [];

        return response()->json(['message'=>'ok','favourites'=>$restaurants,'lists'=>$lists], 200);
    }



    /**
     * Add or remove restaurant from wishlist
     *
     * @param  \Illuminate\Http\Request  $request restaurant (required|alpha_num)
     * @return json response
     */
    public function addRemoveWishlist(Request $request)
    {
        $this->validate($request, [
            'restaurant' => 'required|alpha_num'
        ]);
        $user = User::find(Auth::user()->_id);


        $s = (is_array($user->wishlist)) ? array_search($request->input('restaurant'), $user->wishlist) : false;

        if(is_int($s)):

            $arr = [];
            // remove
            foreach ($user->wishlist as $r) {
                if($r != $request->input('restaurant') ) $arr[] = $r;
            }
            $user->wishlist = $arr;

            //unset($arr[$s]);
            //$user->wishlist = $arr;
            //DB::collection('users_collection')->where('_id', Auth::user()->_id)->pull('wishlist', $request->input('restaurant'));

        else:

            $arr = (is_array($user->wishlist)) ? $user->wishlist : [];
            $arr[] = $request->input('restaurant');
            $user->wishlist = $arr;
            // add
            //DB::collection('users_collection')->where('_id', Auth::user()->_id)->push('wishlist', $request->input('restaurant'), true);

        endif;
        $user->save();

        return response()->json(['message'=>'ok'], 200);
    }

    /**
     * Get wishlist restaurants
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json response
     */
    public function getWishList(Request $request)
    {
        $this->validate($request, [
            'user' => 'required|alpha_num'
        ]);
        $user = User::find($request->input('user'));
        if(!$user) return response()->json(['message'=>'error.empty','wishlist'=>[]], 200);

        if(!$user->wishlist) return response()->json(['message'=>'nowishlist','wishlist'=>[]], 200);

        $wishlist = Restaurant::whereIn('_id',$user->wishlist)->get(['name','slug','address','city','image']);

        return response()->json(['message'=>'ok','wishlist'=>$wishlist], 200);
    }


    /**
     * Set user current address and update location
     *
     * @param type Request $request user id, address
     * @return json json.message ok for role changed
     */
    public function setCurrentLocation(Request $request){
        $this->validate($request, [
            'user_id'=> 'required|alpha_num',
            'address' => 'required|string'
        ]);

        $user = User::find($request->input('user_id'));
        if(!$user):
            return response()->json(['message'=>'error.empty'], 404);
        endif;

        $location = Address::returnLocation($request->input('address'));

        if(!$location) return response()->json(['message'=>'error.no_valid_address'], 403);


        // inserisco address nella lista degli indirizzi dell'utente
        if($user->current_address && $user->current_address != ''):
          $alist = ($user->address_list) ? $user->address_list : [];
          $alist[] = $user->current_address;
          $user->address_list = $alist;
        endif;

        $user->location = $location[2];
        $user->loc = ['type'=>"Point","coordinates"=>[doubleval($location[1]),doubleval($location[0])]];
        $user->city = $location[3];
        $user->state = $location[4];
        $user->zip = $location[5];
        $user->valid_address = 1;

        $user->current_address = $request->input('address');

        $user->save();

        $statusCode = 200;
        $response = [
          'message' => 'ok'
        ];
        return response()->json($response, $statusCode);
    }



    /**
     * Set user current address and update location
     *
     * @param Illuminate\Http\Request $request string
     * @return Response
     */
    public function searchUsers(Request $request){
        $this->validate($request, [
            'string'=> 'required|string'
        ]);

        $stringa = escape_mongodb($request->input('string'));

        $grouped = explode(" ", $stringa);
        $n = count($grouped);

        if($n > 1){

            $lastname = array_pop($grouped);
            $firstname = implode(" ", $grouped);
            $users = User::where('name', 'like', '%'.$stringa.'%')
            ->orWhere("surname", 'like', '%'.$lastname.'%')
            ->get(['name','email','avatar']);

        } else {
            $users = User::where('name','like', '%'.$stringa.'%')->orWhere("email", $stringa)->get(['name','surname','email','avatar']);
        }



        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'users' => $users
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Set user current address and update location
     *
     * @param Illuminate\Http\Request $request string
     * @return Response
     */
    public function inviteFriends(Request $request){
        $this->validate($request, [
            'emails'=> 'required|array'
        ]);

        $addresses = [];
        foreach ($request->input('emails') as $mail) {
            if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                //if (count(User::where('email',$mail)->get(['_id'])) == 0) {
                    $addresses[] = $mail;
                //}
            }
        }

        $l = env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/registrati/'.Auth::user()->_id.'/';
        $n = Auth::user()->name;
        foreach ($addresses as $address) {
            $mail_data = [
                'to' => $address,
                'name'=>$n,
                'link'=>$l
            ];
            Mail::send('email.invitefriends', ['mail_data' => $mail_data],
                function ($m) use ($mail_data) {
                $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
                $m->to($mail_data['to'], 'Menoo')->subject('Invito alla community di Menoo');
            });
        }


        if ( count(Mail::failures()) > 0 ) {
           foreach (Mail::failures as $email_address) {
               Log::info('Impossibile inviare email a '.$email_address);
            }
        }


        $statusCode = 200;
        $response = [
          'message' => 'ok'
        ];
        return response()->json($response, $statusCode);
    }
}
