<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;
use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;
use DB;

use Jenssegers\Mongodb\Model as Eloquent;
use MangiaeBevi\Restaurant;
use MangiaeBevi\Plates;
use MangiaeBevi\TipoLocale;
use MangiaeBevi\Services;
use MangiaeBevi\Types;
use MangiaeBevi\Regions;
use MangiaeBevi\Locations;
use Curl;
use Log;
use Cookie;

use MangiaeBevi\Http\Controllers\GoogleAddressController as Address;
use MangiaeBevi\Http\Controllers\SearchRequestsController as SearchRequests;

/**
 * 
 * @package MangiaeBevi\Http\Controllers  
 * 
 * @method void parseSearchValues()         parse all searched values and call methods
 * @method json searchByQueryString()       perform research by query string
 * @method json searchByValues()            perform research by advanced search form
 */
class SearchController extends Controller
{
    /**    
    * @property string query_string
    */
    public $query_string;
    /**    
    * @property array search_values
    */
    public $search_values;
    /**    
    * @property json response
    */
    public $response;
    /**    
    * @property array restaurants models resulting from search
    */
    public $restaurants;
    /**
     * @property array searced_services 
     */
    private $searched_services = [];
    /**
     * @property array searced_types
     */
    private $searched_types = [];
    /**
     * @property array searced_regions
     */
    private $searched_regions = [];
    /**
     * @property array searced_locations
     */
    private $searched_locations = [];
    /**
     * @property bool has_searched_location 
     */
    private $has_searched_location = false;

    
    public function searchByQueryString(Request $request)
    {

        $this->validate($request, [
            'query_string' => 'string',
            'lat' => 'numeric',
            'lng' => 'numeric'
        ]);

        $this->restaurants = DB::collection('restaurant_collection');
        
        if($request->input( 'query_string' )):
            $this->query_string = strtolower( $request->input( 'query_string' ) );
            $this->parseQueryString( $this->query_string );
        endif;

        if($request->input('lat') && $request->input('lng') && !$this->has_searched_location) :
            $this->getByLatLng($request->input('lat'),$request->input('lng'),200);            
            $k = 0;          
            $this->response = $this->restaurants->get();  
            foreach($this->response as $restaurant){
                if(!in_array($restaurant['_id'], $this->located_ids)){
                    unset($this->response[$k]);
                }
                $k++;
            }
        else:            
            $this->response = $this->restaurants->take(200)->get();
        endif;
        
        return response()->json(['message'=>'ok','restaurants'=>$this->response], 200);

    }

    
    public function searchByCity(Request $request)
    {

        $this->validate($request, [
            'city' => 'string',
            'action'=>'string'
        ]);

        $form_submitted_action = ($request->input('action')) ? $request->input('action') : 'no';
        return response()->json(['message'=>'ok','type'=>'simple','restaurants'=>Restaurant::where('city',$request->input('city'))->where('approved',1)->orderBy('rating','desc')->get(['id','name','image','slug','tipoLocale','loc','address'])], 200);//$restaurants

    }

    /**
    *
    * Cerca per città
    * 
    * @param string $city
    * @param array $located
    * @return array Collection restaurants
    * */
    public static function searchByCityStatic($city,$located)
    {
        if(empty($located)):
            return Restaurant::where('city',$city)->where('approved',1)->orderBy('rating','desc')->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        else:
            $queryBuilder = DB::collection('restaurant_collection');
        //limit=0 skip=0 Tree: $and approved == 1 city == "Roma" GEONEAR field=geo_point maxdist=100000 isNearSphere=0 Sort: {} Proj: { id: true, name: true, image: true, slug: true, tipoLocale: true, loc: true, address: true, city: true
            return $queryBuilder->where('loc', 'near', [
                '$geometry' =>  [
                    'type' => 'Point',
                    'coordinates' => [floatval($located[1]), floatval($located[0])]
                ],
                '$maxDistance' => 1000000000,
            ])->where('city',$city)->where('approved',1)->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        endif;
    }

    /**
    *
    * Cerca per città
    * 
    * @param string $cucina
    * @param string $stringa
    * @return array Collection restaurants
    * */
    public static function searchByFromBlogStatic($cucina,$stringa)
    {
        $stringa = strtolower(urldecode($stringa));
        return Restaurant::whereIn('tipoCucina',[$cucina])
        ->where('tags_string','like', '%'.$stringa.'%')
        ->where('approved',1)
        ->orderBy('rating','desc')
        ->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        
    }

    /**
    *
    * Cerca per tipo di cucina
    * 
    * @param string $cucina
    * @param array $located
    * @return array Collection restaurants
    * */
    public static function searchByCucinaStatic($cucina,$located)
    {
        if(empty($located)):
            return Restaurant::whereIn('tipoCucina',[$cucina])->where('approved',1)->orderBy('rating','desc')->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        else:
            $queryBuilder = DB::collection('restaurant_collection');
            return $queryBuilder->where('loc', 'near', [
                '$geometry' =>  [
                    'type' => 'Point',
                    'coordinates' => [floatval($located[1]), floatval($located[0])]
                ],
                '$maxDistance' => 1000000000,
            ])->whereIn('tipoCucina',[$cucina])->where('approved',1)->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        endif;
    }


    /**
    *
    * Cerca per regione cucina
    * 
    * @param string $cucina
    * @param array $located
    * @return array Collection restaurants
    * */
    public static function searchByRegioneStatic($cucina,$located)
    {
        if(empty($located)):
            return Restaurant::whereIn('regioneCucina',[$cucina])->where('approved',1)->orderBy('rating','desc')->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        else:
            $queryBuilder = DB::collection('restaurant_collection');
            return $queryBuilder->where('loc', 'near', [
                '$geometry' =>  [
                    'type' => 'Point',
                    'coordinates' => [floatval($located[1]), floatval($located[0])]
                ],
                '$maxDistance' => 1000000000,
            ])->whereIn('regioneCucina',[$cucina])->where('approved',1)->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        endif;
    }

    /**
    *
    * Cerca per servizio
    * 
    * @param string $servizio
    * @param array $located
    * @return array Collection restaurants
    * */
    public static function searchByServiceStatic($servizio,$located)
    {
        if(empty($located)):
            return Restaurant::whereIn('services',[$servizio])->where('approved',1)->orderBy('rating','desc')->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        else:
            $queryBuilder = DB::collection('restaurant_collection');
            return $queryBuilder->where('loc', 'near', [
                '$geometry' =>  [
                    'type' => 'Point',
                    'coordinates' => [floatval($located[1]), floatval($located[0])]
                ],
                '$maxDistance' => 1000000000,
            ])->whereIn('services',[$servizio])->where('approved',1)->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        endif;
    }

    /**
    *
    * Cerca per tipo di locale
    * 
    * @param string $tipo
    * @param array $located
    * @return array Collection restaurants
    * */
    public static function searchByLocaleStatic($tipo,$located)
    {
        if(empty($located)):
            return Restaurant::whereIn('tipoLocale',[$tipo])->where('approved',1)->orderBy('rating','desc')->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        else:
            $queryBuilder = DB::collection('restaurant_collection');
            return $queryBuilder->where('loc', 'near', [
                '$geometry' =>  [
                    'type' => 'Point',
                    'coordinates' => [floatval($located[1]), floatval($located[0])]
                ],
                '$maxDistance' => 1000000000,
            ])->whereIn('tipoLocale',[$tipo])->where('approved',1)->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        endif;
    }


    public function searchByValues(Request $request)
    {
        $social_accounts = ['facebook','linkedin','tripadvisor','instagram','pinterest','twitter','googlePlus','youtube'];
        // others: social account, virtual tour, recensioni, order|prenotation
        $this->validate($request, [
            'name'     => 'string', // valid for name and address
            'chef'     => 'string',
            'type'     => 'string',
            'location' => 'string',
            'maxPrice' => 'integer',
            'services' => 'array',
            'types' => 'array',
            'regions' => 'array',
            //'social' => 'array',
            'recensioni' => 'integer',
            'virtualTour' => 'boolean',
            'prenota'=>'integer'/*,
            'ordina'=>'integer'*/
        ]);


        $this->restaurants = DB::collection('restaurant_collection');
        $this->restaurants->where('approved',1);


        if($request->input('name')) :
            // search in name and address
            $name = $request->input('name');
            $this->restaurants->where('name','like','%'.$name."%");
        endif;

        if($request->input('location')) :
            // search in name and address
            $city = $request->input('location');
            $this->restaurants->where('city','like','%'.$city."%");
        endif; 

        if($request->input('type') && $request->input( 'type' ) != '') :
            
            $this->restaurants->whereIn('tipoLocale',[$request->input( 'type' )]);
        endif;    

        if($request->input( 'services' ) && count($request->input( 'services' )) > 0) :
            
            $this->restaurants->where('services','all',$request->input( 'services' ));
        endif;

        if($request->input( 'types' ) && count($request->input( 'types' )) > 0) :
            
            $this->restaurants->where('tipoCucina','all',$request->input( 'types' ));
        endif;

        if($request->input( 'regions' ) && count($request->input( 'regions' )) > 0) :
            
            $this->restaurants->where('regioneCucina','all',$request->input( 'regions' ));
        endif;
        

        if($request->input( 'maxPrice' ) ) :
            //$this->restaurants->where('priceMax','<',$request->input( 'maxPrice' ));
            $this->restaurants->where('priceMax','<',intval($request->input( 'maxPrice' )));
        endif;

        if($request->input( 'chef' ) && $request->input( 'chef' ) != '') :
            $chef_name = $request->input( 'chef' );
            $this->restaurants->where('staff.chef','like','%'.$chef_name."%");
        endif;

        
        if($request->input( 'virtualTour' )) :
            $this->restaurants->where('virtualTour','!=',"");
            $this->restaurants->whereNotNull('virtualTour');
        endif;

        if($request->input( 'recensioni' ) == 1) :
            $this->restaurants->whereNotNull('recensioni');            
        endif;

        if($request->input( 'prenota' )) :
            $this->restaurants->where('prenotazione',1);            
        endif;
        if($request->input( 'ordina' ) == 1) :
            $this->restaurants->where('ordina',1);            
        endif;


        if($request->input('address') == ''):
            /**
            *
            * Is not searching a specific address
            * 
            * **/
        
            $results = $this->restaurants->get(['id','name','image','slug','tipoLocale','loc','address']);
        else:
            /**
            *
            * If is searching in CAP I've to search inside location array
            * 
            * **/
            if($this->isCap($request->input('address'))):
                $this->restaurants->whereIn('location',[$request->input('address')]);
            //Log::info('location');
                $results = $this->restaurants->get(['id','name','image','slug','tipoLocale','loc','address']);
            else:
                
                
                $location = Address::returnLocation($request->input('address'));
                $all = $this->restaurants->get(['id']);

                $query = [];
                if($all):
                    $ristos = [];
                    if(count($all) > 0):
                        $ristos = array_map(function($obj){                 
                            return $obj['_id'];//->{'$id'};
                                    }, $all);        
                    endif;

                    if(count($ristos) > 0):
                        $query = [ '_id'=>['$in'=>$ristos] ];
                        // start mongodb query
                        $mongodb = DB::getMongoDB();
                        $r = $mongodb->command(
                                array('geoNear' => "restaurant_collection",
                                       'near' => array( 
                                            'type' => "Point", 
                                            'coordinates' => array(doubleval($location[1]), doubleval($location[0]) )
                                        ), 
                                       'spherical' => true, 
                                       'num'=>500, 
                                       'maxDistance' => 100000, // 40 km dal punto di origine
                                       'query' => $query
                                    ));
                        $results = $r['results'];

                    else:
                        return response()->json(['message'=>'error.empty','restaurants'=>[]], 200);
                    endif;
                else:
                    return response()->json(['message'=>'error.empty','restaurants'=>[]], 200);
                endif;
                
            endif; // isCap

        endif;

        
        if(!$results || empty($results)) $results = $this->restaurants->get(['id','name','image','slug','tipoLocale','loc','address']);

        //Log::info(json_encode($results));
        return response()->json(['message'=>'ok','restaurants'=>$results], 200);

    }

    public static function searchByValuesStatic($values)
    {
        $social_accounts = ['facebook','linkedin','tripadvisor','instagram','pinterest','twitter','googlePlus','youtube'];
        
        $obj = new SearchController();


        $obj->restaurants = DB::collection('restaurant_collection');
        $obj->restaurants->where('approved',1);


        if(isset($values['name'])) :
            // search in name and address
            $name = $values['name'];
            $obj->restaurants->where('name','like','%'.$name."%");
        endif;

        if(isset($values['location'])) :
            // search in name and address
            $city = $values['location'];
            $obj->restaurants->where('city','like','%'.$city."%");
        endif; 

        if(isset($values['type']) && $values[ 'type' ] != '') :
            
            $obj->restaurants->whereIn('tipoLocale',[$values[ 'type' ]]);
        endif;    

        if(isset($values[ 'services' ]) && count($values[ 'services' ]) > 0) :
            
            $obj->restaurants->where('services','all',$values[ 'services' ]);
        endif;

        if(isset($values[ 'types' ]) && count($values[ 'types' ]) > 0) :
            
            $obj->restaurants->where('tipoCucina','all',$values[ 'types' ]);
        endif;

        if(isset($values[ 'regions' ]) && count($values[ 'regions' ]) > 0) :
            
            $obj->restaurants->where('regioneCucina','all',$values[ 'regions' ]);
        endif;
        

        if(isset($values[ 'maxPrice' ]) ) :
            //$obj->restaurants->where('priceMax','<',$values[ 'maxPrice' ));
            $obj->restaurants->where('price','<',intval($values[ 'maxPrice' ]));
        endif;

        if(isset($values[ 'chef' ]) && $values[ 'chef' ] != '') :
            $chef_name = $values[ 'chef' ];
            $obj->restaurants->where('staff.chef','like','%'.$chef_name."%");
        endif;

        
        if(isset($values[ 'virtualTour' ])) :
            $obj->restaurants->where('virtualTour','!=',"");
            $obj->restaurants->whereNotNull('virtualTour');
        endif;

        
        if(isset($values[ 'prenota' ])) :
            $obj->restaurants->where('prenotazione',1);            
        endif;

        if(isset($values[ 'menu' ])) :
            $obj->restaurants->where('menu.menus','exists', true);  
        endif;
        
        if(isset($values['address'] )):
            if($values['address'] == ''):
                /**
                *
                * Is not searching a specific address
                * 
                * **/
            
                $results = $obj->restaurants->get(['id','name','image','slug','tipoLocale','loc','address','city']);
            else:
                /**
                *
                * If is searching in CAP I've to search inside location array
                * 
                * **/
                if($obj->isCap($values['address'])):
                    $obj->restaurants->whereIn('location',[$values['address']]);
                //Log::info('location');
                    $results = $obj->restaurants->get(['id','name','image','slug','tipoLocale','loc','address','city']);
                else:
                    
                    
                    $location = Address::returnLocation($values['address']);
                    $all = $obj->restaurants->get(['id']);

                    $query = [];
                    if($all):
                        $ristos = [];
                        if(count($all) > 0):
                            $ristos = array_map(function($obj){                 
                                return $obj['_id'];//->{'$id'};
                                        }, $all);        
                        endif;

                        if(count($ristos) > 0):
                            $query = [ '_id'=>['$in'=>$ristos] ];
                            // start mongodb query
                            $mongodb = DB::getMongoDB();
                            $r = $mongodb->command(
                                    array('geoNear' => "restaurant_collection",
                                           'near' => array( 
                                                'type' => "Point", 
                                                'coordinates' => array(doubleval($location[1]), doubleval($location[0]) )
                                            ), 
                                           'spherical' => true, 
                                           'num'=>500, 
                                           'maxDistance' => 100000, // 40 km dal punto di origine
                                           'query' => $query
                                        ));
                            $results = $r['results'];
                        
                        endif;
                   
                    endif;
                    
                endif; // isCap

            endif;
        endif;
        
        if(!isset($results) || empty($results)) $results = $obj->restaurants->get(['id','name','image','slug','tipoLocale','loc','address','city']);



        return $results;

    }

    /**
    *
    * Check if a string could be an italian cap
    * 
    * @param string $string
    * @return bool
    ***/
    public function isCap($string){
        if(strlen($string) == 5):
            for($x = 0;$x < 5;$x++):
                if(!is_numeric( substr($string, $x, 1) ) ) return false;
            endfor;
            return true;
        endif;
        return false;
    }

    /**
    *
    * Cerca un piatto dal nome
    * 
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response
    ***/
    public function searchPlateByName(Request $request){
        $this->validate($request, [
            'name' => 'required|string',
            'restaurant' => 'alpha_num'
        ]);
        $name = $request->input('name');
        // dovrebbero essere pochi risultati, prendo tutto
        if($request->input('restaurant')) : 
            $plates = Plates::where('name','like','%'.$name.'%')->where('restaurant',$request->input('restaurant'))->where('deleted','!=',1)->get();
        else:
            $plates = Plates::where('name','like','%'.$name.'%')->where('deleted','!=',1)->get();
        endif;
        
        return response()->json(['message'=>'ok','plates'=>$plates], 200);
    }

    /**
    *
    * Cerca un ristorante dal nome
    * 
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response 
    ***/
    public function searchByName(Request $request){
        $this->validate($request, [
            'name' => 'required|string'
        ]);
        $name = $request->input('name');
        // dovrebbero essere pochi risultati, prendo tutto
        $restaurants = Restaurant::where('name','like','%'.$name.'%')->get();

        return response()->json(['message'=>'ok','type'=>'name','restaurants'=>$restaurants], 200);
    }

    /**
    *
    * Prendi un ristorante dallo slug
    * 
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response
    ***/
    public function searchBySlug(Request $request){
        $this->validate($request, [
            'slug' => 'required|string'
        ]);
        $restaurants = Restaurant::where('slug',$request->input('slug'))->get();
        if(count($restaurants) == 0) $restaurants = Restaurant::where('_id',$request->input('slug'))->get();

        return response()->json(['message'=>'ok','type'=>'name','restaurants'=>$restaurants], 200);
    }

    /**
    *
    * Cerca un ristorante per latitudine e longitudine
    * 
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response 
    ***/
    public function searchByLatLng(Request $request){
        $this->validate($request, [
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'action'=>'string'
        ]);

        switch($request->input('action')):
            case 'order':
                $query = [ 'ordina'=>1 ];
            break;
            case 'reserve':
                $query = [ 'prenotazione'=>1 ];
            break;
            default:
                $query = [];
            break;
        endswitch;


        $mongodb = DB::getMongoDB();
        $r = $mongodb->command(
            array('geoNear' => "restaurant_collection", // Put here the collection 
                   'near' => array( // 
                        'type' => "Point", 
                        'coordinates' => array(doubleval($request->input('lng')), doubleval($request->input('lat')) )
                    ), 
                   'spherical' => true, 
                   //'num'=>intval( $max_number_results ),
                   'maxDistance' => 40000, // 40 km dal punto di origine
                   'query'=>$query
                   // for more infos https://docs.mongodb.org/manual/reference/operator/query/near/#op._S_near 
                ));
        if(!isset($r['results'])):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        return response()->json(['message'=>'ok','type'=>'geolocated','restaurants'=>$r['results']], 200);
    }

    /**
    *
    * Cerca da una semplice stringa, funzione piuttosto complessa e non più usata
    *  
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response
    ***/
    public function searchByString(Request $request){
        $this->validate($request, [
            'string' => 'required|string',
            'lat'=>'numeric',
            'lng'=>'numeric'/*,
            'action'=>'string'*/
        ]);

        $string = strtolower($request->input('string'));

        //$result = Restaurant::whereIn('tags',[$string])->where('approved',1)->orderBy('rating','desc')->get(['id','name','image','slug','tipoLocale','loc','address']); 
        /*if(!$result || count($result) == 0)*/ 
        $result = Restaurant::where('tags_string','like', '%'.$string.'%')->where('approved',1)->orderBy('rating','desc')->get(['id','name','image','slug','tipoLocale','loc','address']); 
        if(!$result || count($result) == 0) $result = Restaurant::where('slug','like', '%'.str_slug($string).'%')->where('approved',1)->orderBy('rating','desc')->get(['id','name','image','slug','tipoLocale','loc','address']); 

        if($result && count($result) > 0) return response()->json(['message'=>'ok','restaurants'=>$result,'query'=>$request->input('string')], 200);


        /*$this->restaurants = DB::collection('restaurant_collection');
        $search_in_collection = DB::collection('restaurant_collection');

        $form_submitted_action = ($request->input('action')) ? $request->input('action') : 'no';

        switch($request->input('action')):
            case 'order':
                $this->restaurants->where('ordina',1)->where('approved',1);
                $search_in_collection->where('ordina',1)->where('approved',1);                
                $query = [ 'ordina'=>1,'approved'=>1 ];
            break;
            case 'reserve':
                $this->restaurants->where('prenotazione',1)->where('approved',1);
                $search_in_collection->where('prenotazione',1)->where('approved',1);
                $query = [ 'prenotazione'=>1,'approved'=>1 ];
            break;
            default:
                $this->restaurants->where('approved',1);
                $search_in_collection->where('approved',1);
                $query = [ 'approved'=>1  ];
            break;
        endswitch;

        $mongodb = DB::getMongoDB();

        $string = $request->input('string');*/

        /**
        *
        * Verifico che non sia un CAP
        * 
        * **/
        if($this->isCap($string)):
            SearchRequests::saveRequest($request->input('string'),0,0,2,$form_submitted_action);
            $response = $this->restaurants->whereIn('location',[$string])->get(['id','name','image','slug','tipoLocale','loc']); 
            return response()->json(['message'=>'ok','restaurants'=>$response,'query'=>'cap'], 200);
        endif;


        /**
        *
        * Verifico che non sia una città
        * 
        * **/
         // probabilmente cerca un indirizzo, prendiamolo da google
        $google_response = Curl::to('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($string))->get();
        $response = json_decode($google_response,true);
        $found = false;
        if(isset($response['results'][0]['address_components'])):
            if(!preg_match('~\b(via|piazza|strada|street|piazzale|viale)\b~i', $string)):
                // non cerca via/piazza ecc...
                foreach ($response['results'][0]['address_components'] as $address_component) :
                    # code...
                    //Log::info($address_component['types'][0]);
                    if($address_component['types'][0] == 'locality' && $address_component['long_name'] == $string):
                        // la stringa cercata è uguale alla località trovata da google
                        $found = true;
                    endif;
                endforeach;
            else:
                $found = true;
            endif;
            
            if($found):
                $lat = $response['results'][0]['geometry']['location']['lat'];
                $lng = $response['results'][0]['geometry']['location']['lng'];
                //$this->getByLatLng($lat, $lng);
            
                $r = $mongodb->command(
                array(  'geoNear' => "restaurant_collection", // Put here the collection 
                    'near' => array( // 
                        'type' => "Point", 
                        'coordinates' => array(doubleval($lng), doubleval($lat))
                    ), 
                    'spherical' => true, 
                    'num'=>5000,
                    'maxDistance' => 40000,
                    'query' => $query
                    // for more infos https://docs.mongodb.org/manual/reference/operator/query/near/#op._S_near 
                ));
                SearchRequests::saveRequest($request->input('string'),$lat,$lng,0,$form_submitted_action);
                if(isset($r['results']) && count($r['results']) > 0):                
                    return response()->json(['message'=>'ok','type'=>'geolocated','restaurants'=>$r['results'],'query'=>'city'], 200);//$restaurants
                endif; // isset $r['results']
            endif;
        endif; // indirizzo google ok



        // nè CAP nè Città
        // faccio il parsing
        // prendo gli id
        // faccio query geolocalizzata


        $ids = [];
        $all = false;
                        
        // ricerca per servizi ecc...
        // prendi gli id di quelli con il nome uguale
        // poi cerca tra le impostazioni
        // fai il match coi ristoranti
        // ritorna array di id da mettere in $query
        $by_name = $search_in_collection->where('name','like','%'.$string.'%')->get(['name','image','slug','tipoLocale','loc']);//,'name','image','slug','tipoLocale','loc']);
        if(count($by_name) > 0):
            $all = $by_name;

            SearchRequests::saveRequest($request->input('string'),0,0,3,$form_submitted_action);
            return response()->json(['message'=>'ok','type'=>'by_name','restaurants'=>$by_name], 200);
        endif;

        $this->query_string = $string;    
        $parsed = false;
        if($this->parseQueryString( $this->query_string )):
            $parsed = $this->restaurants->get(['id']); 
            $all = (!$all) ? $parsed : $all+$parsed;
        endif;
                
        // sono state trovate impostazioni
        
        if($all):
            $ristos = [];
            if(count($all) > 0):
                $ristos = array_map(function($obj){                 
                    return $obj['_id'];//->{'$id'};
                            }, $all);        
            endif;

            if(count($ristos) > 0):
                $query += [ '_id'=>['$in'=>$ristos] ];
            endif;
        endif;

        $r = $mongodb->command(
            array('geoNear' => "restaurant_collection",
                   'near' => array( 
                        'type' => "Point", 
                        'coordinates' => array(doubleval($request->input('lng')), doubleval($request->input('lat')) )
                    ), 
                   'spherical' => true, 
                   'num'=>500, 
                   'maxDistance' => 100000, // 40 km dal punto di origine
                   'query' => $query
                ));
        if(isset($r['results'])):
            SearchRequests::saveRequest($request->input('string'),doubleval($request->input('lat')),doubleval($request->input('lng')),4,$form_submitted_action);
            return response()->json(['message'=>'ok','restaurants'=>$r['results'],'query'=>'user'], 200);
        endif;

        return response()->json(['message'=>'error.empty'], 200);
    }

    /**
    *
    * Cerca da una semplice stringa, va a cercare nei tag del ristorante se c'è un match
    *  
    * @param Illuminate\Http\Request
    * @return Illuminate\Http\Response
    ***/
    public static function searchByStringStatic($string,$located){        

        if(empty($located)):
            return Restaurant::where('tags_string','like', '%'.$string.'%')->where('approved',1)->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        else:
            $queryBuilder = DB::collection('restaurant_collection');

            return $queryBuilder->where('loc', 'near', [
                '$geometry' =>  [
                    'type' => 'Point',
                    'coordinates' => [floatval($located[1]), floatval($located[0])]
                ],
                '$maxDistance' => 100000000,
            ])->where('tags_string','like', '%'.$string.'%')->where('approved',1)->get(['id','name','image','slug','tipoLocale','loc','address','city']);
        endif;
       
    }


    /**
    *
    * Ristoranti correlati, prende i locali dello stesso tipo se presente, con slug diverso e ordinati in base alla distanza
    *
    * @param Illuminate\Http\Request restaurant  $id  (alpha_num|required)
    * @return  Illuminate\Http\Response 
    * @return message string ok per risultati trovati, error.empty se non ci sono correlati
    * @return results array oggetti 'ristorante', ne ritorna 3
    *
    */
    public function searchRelated(Request $request){
        $this->validate($request, [
            'restaurant' => 'required|alpha_num'
        ]);
        $restaurant = Restaurant::find($request->input('restaurant'));

        if(!$restaurant):
            return response()->json(['message'=>'error.empty'], 200);
        endif;

        $lat = $restaurant->loc['coordinates'][1];
        $lng = $restaurant->loc['coordinates'][0];

        $tipo = $restaurant->tipoLocale[0];
        
        // costruisci la query dei correlati
        $slug = (!$restaurant->slug) ? $restaurant->slug : str_slug($restaurant->name);
        $query = ['approved'=>1, 'image'=>['$ne'=>null],'slug'=>['$ne'=>$restaurant->slug]];
        
        if($tipo) $query['tipoLocale'] = ['$in'=>[$tipo]];

        $mongodb = DB::getMongoDB();
        $r = $mongodb->command(
            array('geoNear' => "restaurant_collection", // Put here the collection 
                   'near' => array( // 
                        'type' => "Point", 
                        'coordinates' => array(doubleval($lng), doubleval($lat) )
                    ), 
                   'spherical' => true, 
                   'num'=>3, // prendine solo 3
                   'maxDistance' => 40000, // 40 km dal punto di origine
                   'query' => $query
                   // for more infos https://docs.mongodb.org/manual/reference/operator/query/near/#op._S_near 
                ));
        if(!isset($r['results'])):
            return response()->json(['message'=>'error.empty','result'=>$r], 200);
        endif;

        return response()->json(['message'=>'ok','restaurants'=>$r['results']], 200);
    }

    /**
    *
    * Ristoranti correlati, prende i locali dello stesso tipo se presente, con slug diverso e ordinati in base alla distanza
    *
    * @param mongoid $restaurant_id
    * @return  array 
    *
    */
    public static function searchRelatedStatic($restaurant_id){
       
        $restaurant = Restaurant::find($restaurant_id);


        $lat = $restaurant->loc['coordinates'][1];
        $lng = $restaurant->loc['coordinates'][0];

        $tipo = (isset($restaurant->tipoLocale[0])) ? $restaurant->tipoLocale[0] : false;
        
        // costruisci la query dei correlati
        $slug = (!$restaurant->slug) ? $restaurant->slug : str_slug($restaurant->name);
        $query = ['approved'=>1, 'image'=>['$ne'=>null],'slug'=>['$ne'=>$restaurant->slug]];
        
        if($tipo) $query['tipoLocale'] = ['$in'=>[$tipo]];

        $mongodb = DB::getMongoDB();
        $r = $mongodb->command(
            array('geoNear' => "restaurant_collection", // Put here the collection 
                   'near' => array( // 
                        'type' => "Point", 
                        'coordinates' => array(doubleval($lng), doubleval($lat) )
                    ), 
                   'spherical' => true, 
                   'num'=>3, // prendine solo 3
                   'maxDistance' => 40000, // 40 km dal punto di origine
                   'query' => $query
                   // for more infos https://docs.mongodb.org/manual/reference/operator/query/near/#op._S_near 
                ));
        if(!isset($r['results'])):
            return [];
        endif;

        $return_arr = [];
        //Log::info($r['results']);
        foreach ($r['results'] as $risto) {
            $locale = TipoLocale::find($risto['obj']['tipoLocale'][0]);
            $return_arr[] = [
            'name'=>$risto['obj']['name'],
            'city'=>(isset($risto['obj']['city'])) ? $risto['obj']['city'] : '',
            'slug'=>$risto['obj']['slug'],
            'image'=>$risto['obj']['image'],
            'tipoLocale'=>$locale->name,
            'address'=>$risto['obj']['address'],
            'loc'=>$risto['obj']['loc']
            ];
        }

        return $return_arr;
    }





    private function parseQueryString($query_string)
    {
        // get all services
        $services  = $this->getServices();
        $types     = $this->getTypes();
        $regions   = $this->getRegions();
        //$locations = $this->getLocations();

        $this->searched_services   = [];
        $this->searched_types      = [];
        $this->searched_regions    = [];
        //$this->searched_locations  = [];


        foreach($services as $service) :
            $service_name = strtolower( str_replace( "/", " ", $service['name'] ) );
            if(preg_match("/$service_name/i", $query_string )) :
                $this->searched_services[] = $service['_id'];
                $query_string = str_replace( $service_name, "", $query_string );
            endif;
        endforeach;

        foreach($types as $type) :
            $type_name = strtolower( str_replace( "/", " ", $type['name'] ) );
            if(preg_match("/$type_name/i", $query_string)) :
                $this->searched_types[] = $type['_id'];
                $query_string = str_replace( $type_name, "", $query_string );
            endif;
        endforeach;

        foreach($regions as $region) :
            $region_name = strtolower( str_replace( "/", " ", $region['name'] ) );
            if(preg_match("/$region_name/i", $query_string)) :
                $this->searched_regions[] = $region['_id'];
                $query_string = str_replace( $region_name, "", $query_string );
            endif;
        endforeach;

        /*foreach($locations as $location) :
            $location_name = strtolower( str_replace( "/", " ", $location['name'] ) );
            if(preg_match("/$location_name/i", $query_string)) :
                $this->searched_locations[] = $location['_id'];
                $query_string = str_replace( $location_name, "", $query_string );
            endif;
        endforeach;*/

        // start building the query
        // foreach array > 0 get restaurants
        // make unique
        // return values

        if(count($this->searched_services) > 0) :
            $this->searchByServices( $this->searched_services );
            return true;
        endif;
        if(count($this->searched_types) > 0) :
            $this->searchByTypes( $this->searched_types );
            return true;
        endif;
        if(count($this->searched_regions) > 0) :
            $this->searchByRegions( $this->searched_regions );
            return true;
        endif;
        
        return false;
        //if(count($this->searched_locations) > 0) :
        //    $this->searchByLocations( $this->searched_locations );
        //endif;

        //$this->searchInName( $query_string );
        //$this->searchByAddress( $query_string );
        //$this->searchByOptionalStaff( $query_string );

        //##########old$this->response = $this->restaurants;

    }

    private function getServices(){
        return Services::all();
    }
    private function getTypes(){
        return Types::all();
    }
    private function getLocations()
    {
        return Locations::all();        
    }
    private function getRegions()
    {
        return Regions::all();
    }


    private function searchByServices( $services)
    {
        // metti id in array e fai where id
        $this->restaurants->whereNotNull('services');
        $this->restaurants->whereIn('services',$services);        
    }
    private function searchByTypes( $types)
    {
        // metti id in array e fai where id
        $this->restaurants->whereNotNull('tipoCucina');
        $this->restaurants->whereIn('tipoCucina',$types);
    }
    private function searchByLocations( $locations)
    {
        // metti id in array e fai where id
        $this->has_searched_location = true;
        $this->restaurants->whereNotNull('location');
        $this->restaurants->whereIn('location',$locations);
    }
    private function searchByRegions( $regions)
    {
        // metti id in array e fai where id
        $this->restaurants->whereNotNull('regioneCucina');
        $this->restaurants->whereIn('regioneCucina',$regions);
    }

    private function searchInName( $query_string)
    {
        if($query_string != "") :
            $this->restaurants->orWhere('name','like',"%$query_string%");
            $this->restaurants->orWhere('tipoLocale','like',"%$query_string%");
        endif;
        /*$rest = Restaurant::orWhere("name","like","%Casa Gusto Forum%")->get();
        */
        
    }
    private function searchByAddress( $query_string)
    {
        if($query_string != "") $this->restaurants->orWhere('address','like',"%$query_string%");
        /*$words = explode(" ",$query_string);
        foreach($words as $word){
            $word = strtolower($word);
            $this->restaurants->orWhere('address','like',"%$word%");
        }*/
    }
    private function searchByOptionalStaff($query_string)
    {
        if($query_string != "") :
            $this->restaurants->orWhere('staff.chef','like',"%query_string%");
            $this->restaurants->orWhere('staff.sommelier','like',"%query_string%");
            $this->restaurants->orWhere('staff.capo_sala','like',"%query_string%");
            $this->restaurants->orWhere(function($query)
                {
                    $query->whereIn('staff.camerieri', [$this->query_string]);
                });
            $this->restaurants->orWhere(function($query)
                {
                    $query->whereIn('staff.cuochi', [$this->query_string]);
                });
        endif;
    }

    //https://docs.mongodb.org/manual/reference/command/geoNear/#dbcmd.geoNear
    // powerful search query, solo lo chef
    private function searchByStaffChef($name)
    {
        $this->restaurants->where('staff.chef', 'like', "%$name%");
    }

    private function getByLatLng($lat, $lng, $max_number_results = 5000)
    {
        $mongodb = DB::getMongoDB();
        $r = $mongodb->command(
            array('geoNear' => "restaurant_collection", // Put here the collection 
                   'near' => array( // 
                        'type' => "Point", 
                        'coordinates' => array(doubleval($lng), doubleval($lat))
                    ), 
                   'spherical' => true, 
                   'num'=>intval( $max_number_results ),
                   'maxDistance' => 100000, // 100 km dal punto di origine
                   // for more infos https://docs.mongodb.org/manual/reference/operator/query/near/#op._S_near 
                ));

        /**
        * Prendo tutti i risultati e metto gli id in un array in modo che poi possano essere presi dalla query
        *
        */
        
        $this->located_ids = [];

        //var_dump($r);
        if(isset($r['results'])):
            foreach($r['results'] as $localized_restaurant){
                $this->located_ids[] = $localized_restaurant['obj']['_id'];
            }
        endif;
    }





    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    public static function returnDistance(
      $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
      // convert from degrees to radians
      $latFrom = deg2rad($latitudeFrom);
      $lonFrom = deg2rad($longitudeFrom);
      $latTo = deg2rad($latitudeTo);
      $lonTo = deg2rad($longitudeTo);

      $lonDelta = $lonTo - $lonFrom;
      $a = pow(cos($latTo) * sin($lonDelta), 2) +
        pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
      $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

      $angle = atan2(sqrt($a), $b);
      return $angle * $earthRadius;
    }
}
