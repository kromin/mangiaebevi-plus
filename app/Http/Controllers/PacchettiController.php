<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\Pacchetti;

class PacchettiController extends Controller
{
    /**
     * Mostra tutti i piani di pagamento presenti
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pacchetti = Pacchetti::all();       
        return response()->json(['message'=>'ok','pacchetti'=>$pacchetti], 200);
    }

    /**
     * Mostra tutti i piani di pagamento presenti ma solo per menoo
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMenoo()
    {
        //
        $pacchetti = Pacchetti::where('website',0);       
        return response()->json(['message'=>'ok','pacchetti'=>$pacchetti], 200);
    }

    
    /**
     * Aggiungi un piano di pagamento, per le API Braintree vedere: https://developers.braintreepayments.com/guides/recurring-billing/manage/php
     * 
     * 'name' => 'required|string',
     * 'featured' => 'integer', // 0 = no, 1 = si
     * 'services' => 'array',
     * 'price_month'=> 'required|numeric',
     * 'price_annual'=> 'required|numeric',
     * 'period' => 'required|integer', //  0 = month, 1 = annual, 2 = entrambi
     * 'website'=>'integer', // 0 = menoo, 1 = mangiaebevi, 2 = entrambi
     * 'braintree_plan'=>'string'
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string',
            'featured' => 'integer', // 0 = no, 1 = si
            'services' => 'array',
            'price_month'=> 'required|numeric',
            'price_annual'=> 'required|numeric',
            'period' => 'required|integer', //  0 = month, 1 = annual, 2 = entrambi
            'website'=>'integer', // 0 = menoo, 1 = mangiaebevi, 2 = entrambi
            'braintree_plan'=>'string'
        ]);

        $pacchetto = new Pacchetti;
        $pacchetto->name = $request->input('name');
        $pacchetto->featured = $request->input('featured');

        $pacchetto->price_month = $request->input('price_month');
        $pacchetto->price_annual = $request->input('price_annual');
        $pacchetto->period = $request->input('period');
        $pacchetto->website = $request->input('website');
        $pacchetto->braintree_plan = $request->input('braintree_plan');

        $services = [];
        foreach ($request->input('services') as $service) {
            $services[] = $service;
        }
        $pacchetto->services = $services;

        $pacchetto->save();
        
        return response()->json(['message'=>'ok','pacchetto'=>$pacchetto], 200);
    }

    /**
     * Aggiungi un piano di pagamento, per le API Braintree vedere: https://developers.braintreepayments.com/guides/recurring-billing/manage/php
     * 
     * 'id' => 'required|alpha_num',
     * 'name' => 'required|string',
     * 'featured' => 'integer', // 0 = no, 1 = si
     * 'services' => 'array',
     * 'price_month'=> 'required|numeric',
     * 'price_annual'=> 'required|numeric',
     * 'period' => 'required|integer', //  0 = month, 1 = annual, 2 = entrambi
     * 'website'=>'integer', // 0 = menoo, 1 = mangiaebevi, 2 = entrambi
     * 'braintree_plan'=>'string' quando selezionate entrambe le fatturazioni {{fatturazione_mensile}}|{{fatturazione_annuale}}
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
        $this->validate($request, [
            'id'=>'required|alpha_num',
            'name' => 'required|string',
            'featured' => 'integer', // 0 = no, 1 = si
            'services' => 'array',
            'price_month'=> 'required|numeric',
            'price_annual'=> 'required|numeric',
            'period' => 'required|integer', //  0 = month, 1 = annual, 2 = entrambi
            'website'=>'integer', // 0 = menoo, 1 = mangiaebevi, 2 = entrambi
            'braintree_plan'=>'string'
        ]);

        $pacchetto = Pacchetti::find($request->input('id'));
        $pacchetto->name = $request->input('name');
        $pacchetto->featured = $request->input('featured');

        $pacchetto->price_month = $request->input('price_month');
        $pacchetto->price_annual = $request->input('price_annual');
        $pacchetto->period = $request->input('period');
        $pacchetto->website = $request->input('website');
        $pacchetto->braintree_plan = $request->input('braintree_plan');

        $services = [];
        foreach ($request->input('services') as $service) {
            $services[] = $service;
        }
        $pacchetto->services = $services;

        $pacchetto->save();
        
        return response()->json(['message'=>'ok','pacchetto'=>$pacchetto], 200);
    }

    
    /**
     * Rimuovi piano di pagamento
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);

        $pacchetto = Pacchetti::find($request->input('id'));
        if(!$pacchetto):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        
        $pacchetto->delete();
        return response()->json(['message'=>'ok'], 200);

    }
}
