<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\Notifications;
use Carbon\Carbon;
use Auth;
use Log;

use MangiaeBevi\User;

use Mail;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response solo admin può vederle tutte
     */
    public function index()
    {
        //
        $notifications = Notifications::all();
        return response()->json(['message'=>'ok','notifications'=>$notifications], 200);
    }

    /**
     * Display a listing of the resource of passed user. Using isMeOrIsAdmin Middleware
     *
     * @param \Illuminate\Http\Request user_id
     * @return \Illuminate\Http\Response solo admin può vederle tutte
     */
    public function get_by_user(Request $request)
    {
        //
        $this->validate($request, [
            'user_id' => 'required|alpha_num',
            'skip'=>'integer',
            'number'=>'integer'
        ]);

        // se admin prendi anche quelle per admin
        if(Auth::user()->role > 3)
            $notifications = Notifications::where('target_usr',$request->input('user_id'))->orWhere('target_usr',0)->orderBy('created_at','desc');
        else
            $notifications = Notifications::where('target_usr',$request->input('user_id'))->orderBy('created_at','desc');

        if($request->input('skip')):
            $notifications->skip($request->input('skip'));
        endif;

        if($request->input('number')):
            $notifications->take($request->input('number'));
        endif;

        $notifications->update(['read'=>1]);
        $el = $notifications->get();
        return response()->json(['message'=>'ok','notifications'=>$el,'skip'=>$request->input('skip'),'num'=>$request->input('number')], 200);
    }

    /**
     * Display a listing of the resource to read by logged user. Using isUser Middleware
     *
     * @param \Illuminate\Http\Request user_id
     * @return \Illuminate\Http\Response
     */
    public function get_to_read(Request $request)
    {
        //

        if(Auth::user()->role > 3) :
            $notifications = Notifications::where('read',0)->where('target_usr',Auth::user()->_id)->orderBy('created_at','desc');
            $admin_notifications = Notifications::where('read',0)->where('target_usr',0)->orderBy('created_at','desc');


            $el = $notifications->get();
            $admin_el = $admin_notifications->get();

            $notifications->update(['read'=>1]);
            $admin_notifications->update(['read'=>1]);

            foreach ($admin_el as $n) {
                $el[] = $n;
            }
        //->where('target_usr',Auth::user()->_id)->orWhere('target_usr',0)->orderBy('created_at','desc');
        else :
            $notifications = Notifications::where('read',0)->where('target_usr',Auth::user()->_id)->orderBy('created_at','desc');
            $notifications->update(['read'=>1]);
            $el = $notifications->get();
        endif;


        return response()->json(['message'=>'ok','notifications'=>$el], 200);
    }

    /**
     * Set Notification as read
     *
     * @param \Illuminate\Http\Request user_id
     * @return \Illuminate\Http\Response
     */
    public function set_read(Request $request)
    {
        //
        $this->validate($request, [
            'user_id' => 'required|alpha_num',
            'id' => 'required|alpha_num'
        ]);
        if(Auth::user()->role > 3)
            $notification = Notifications::where('_id',$request->input('id'))->update(['read' => 1]);
        else
            $notification = Notifications::where('target_usr',$request->input('user_id'))->where('_id',$request->input('id'))->update(['read' => 1]);

        return response()->json(['message'=>'ok'], 200);
    }

    /**
     * Set All User Notification as read
     *
     * @param \Illuminate\Http\Request user_id
     * @return \Illuminate\Http\Response solo admin può vederle tutte
     */
    public function set_all_read_by_user(Request $request)
    {
        //
        $this->validate($request, [
            'user_id' => 'required|alpha_num'
        ]);

        if(Auth::user()->role > 3)
            $notification = Notifications::where('target_usr',$request->input('user_id'))->orWhere('target_usr',$request->input('user_id'))->update(['read' => 1]);
        else
            $notifications = Notifications::where('target_usr',$request->input('user_id'))->update(['read' => 1]);

        return response()->json(['message'=>'ok'], 200);
    }


    /**
     * Delete notifications older than 1 week. Using isMeOrIsAdmin Middleware. Da testare
     *
     * @param \Illuminate\Http\Request user_id
     * @return \Illuminate\Http\Response solo admin può vederle tutte
     */
    public function delete_old()
    {
        //
        $date = Carbon::now();
        $dt = $date->subWeek();// 1 settimana // subYears(1); // togli più vecchie di 1 anno

        $notifications = Notifications::where('created_at','<',$dt)->where('read',0)->delete();
        return response()->json(['message'=>'ok'], 200);
    }


    /**
    *
    * Add a new Notification, it's a static method
    *
    * types:
    *   0 => nuova registrazione                [id_utente, nome utente]
    *   1 => nuovo ordine                       [id_ordine, id utente, nome utente, prezzo totale, data consegna, id ristorante]
    *   2 => ristorante aggiunto                [id utente, nome utente, id ristorante, nome ristorante] *
    *   3 => iscrizione a piano di abbonamento  [id utente, nome utente, nome del piano] *
    *   4 => nuovo claim                        [id utente, nome utente, id ristorante, nome ristorante] *
    *   5 => claim approvato                    [id ristorante, nome ristorante] *
    *   6 => ristorante approvato               [id ristorante, nome ristorante] *
    *   7 => chiesto rimborso                   [id ordine, restaurant id]
    *   8 => ordine rimborsato                  [id ordine, restaurant id]
    *   9 => nuova prenotazione                 [id utente, nome utente, id ristorante, nome ristorante, data, persone]
    *   10 => utente vuole essere driver        [id utente, nome utente]
    *
    *   11 => prenotazione confermata           [id prenotazione, id ristorante, nome ristorante, data]
    *   12 => accettato cambio prenotazione     [id prenotazione, id ristorante, nome ristorante, data, nome utente]
    *   13 => cambio data prenotazione          [id prenotazione, id ristorante, nome ristorante, vecchia data, nuova data]
    *
    *   14 => rifiutato data prenotazione       [id utente, nome utente]    *
    *   15 => utente seguito                    [id utente, nome utente]
    *   16 => amicizia accettata                [id utente, nome utente]
    *   17 => raccomandazione ristorante        [id utente, nome utente, id ristorante]
    *   18 => richiesta di amicizia             [id utente, nome utente]
    *   19 => risposta richiesta raccomandazione [id utente, nome utente, testo_richiesta_raccomandazione]
    *
    *
    * @param type integer
    * @param parameters array array of useful parameters to identify variables
    * @param target_usr alpha_num target user
    * @return array [message=>'ok|error']
    * */
    public static function addNotification($type,$parameters,$target_usr){

        $notification = new Notifications;

        $notification->type = intval($type);
        $notification->parameters = $parameters;
        $notification->target_usr = $target_usr;
        $notification->read = 0;

        self::sendNotificationEmail($type,$parameters,$target_usr);

        $notification->save();
    }

    /**
    *
    * Send email for some notifications
    *
    * @param type integer
    * @param parameters array array of useful parameters to identify variables
    * @param target_usr alpha_num target user
    * @return
    * */
    private static function sendNotificationEmail($type,$parameters,$target_usr){

        if($target_usr != 0){
            $target = User::find($target_usr);
            if(!$target || !$target->email) return;

            $email_usr = $target->email;
        } else {
            $email_usr = env('ADMIN_EMAIL');
        }

        $mail_values = [];
        $mail_template = '';
        switch($type){
            case 0:
            $mail_values = [
            'username'=>$parameters[1],
            'user_id'=>$parameters[0],
            'title'=>'Nuova registrazione'
            ];
            $mail_template = 'email.newuser';
            break;
            case 3:
            $mail_values = [
            'username'=>$parameters[1],
            'user_id'=>$parameters[0],
            'plan'=>$parameters[2],
            'title'=>'Nuovo abbonamento'
            ];
            $mail_template = 'email.newsubscription';
            break;
            case 4:
            $mail_values = [
            'username'=>$parameters[1],
            'user_id'=>$parameters[0],
            'restaurant_name'=>$parameters[3],
            'restaurant_id'=>$parameters[2],
            'title'=>'Nuovo claim'
            ];
            $mail_template = 'email.newclaim';
            break;
            case 5:
            $mail_values = [
            'restaurant_name'=>$parameters[1],
            'restaurant_id'=>$parameters[0],
            'title'=>'Richiesta accettata'
            ];
            $mail_template = 'email.approvedclaim';
            break;
            case 6:
            $mail_values = [
            'restaurant_name'=>$parameters[1],
            'restaurant_id'=>$parameters[0],
            'title'=>'Ristorante approvato'
            ];
            $mail_template = 'email.approvedrestaurant';
            break;
            case 8:
            $mail_values = [
            'order_id'=>$parameters[0],
            'restaurant_id'=>$parameters[1],
            'title'=>'Ordine rimborsato'
            ];
            $mail_template = 'email.orderrefund';
            break;

            case 11:
            $mail_values = [
            'reservation_id'=>$parameters[0],
            'restaurant_id'=>$parameters[1],
            'restaurant_name'=>$parameters[2],
            'reservation_date'=>$parameters[3],
            'title'=>'Prenotazione confermata'
            ];
            $mail_template = 'email.reservationconfirmed';
            break;
            case 13:
            $mail_values = [
            'reservation_id'=>$parameters[0],
            'restaurant_id'=>$parameters[1],
            'restaurant_name'=>$parameters[2],
            'reservation_old_date'=>$parameters[3],
            'reservation_date'=>$parameters[4],
            'title'=>'Prenotazione modificata'
            ];
            $mail_template = 'email.reservationchanged';
            break;
            case 15:
            $mail_values = [
            'user'=>$parameters[1]." ".$parameters[2],
            'title'=>'Nuovo follower'
            ];
            $mail_template = 'email.newfollower';
            break;
            case 16:
            $mail_values = [
            'user'=>$parameters[1],
            'title'=>'Richiesta di amicizia accettata'
            ];
            $mail_template = 'email.friendrequestaccepted';
            break;
            case 18:
            $mail_values = [
            'user'=>$parameters[1],
            'title'=>'Nuova richiesta di amicizia'
            ];
            $mail_template = 'email.newfriendrequest';
            break;
            case 19:
            $mail_values = [
            'user'=>$parameters[1],
            'text'=>$parameters[2],
            'title'=>'Risposta alla richiesta di raccomandazione'
            ];
            $mail_template = 'email.newanswerrequest';
            break;
            default:
            return;
            break;
        }

        if(!empty($mail_values) && $mail_template != ''):
            $mail_data = [
                'array_values'=>$mail_values,
                'email' => $email_usr
            ];

            Mail::send($mail_template, ['mail_data' => $mail_data],
                function ($m) use ($mail_data) {
                $m->from('support@mangiaebevi.it', 'MangiaeBevi+');
                $m->to($mail_data['email'], 'Admin')->subject($mail_data['array_values']['title']);
            });
        endif;

    }



}
