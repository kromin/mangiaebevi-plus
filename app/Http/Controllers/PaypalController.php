<?php

namespace MangiaeBevi\Http\Controllers;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\Capture;
use PayPal\Api\Refund;
use PayPal\Api\Sale;
use PayPal\Api\Authorization;
use MangiaeBevi\Http\Controllers\NotificationsController as Notification;


use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\Pacchetti;
use MangiaeBevi\User;

use Carbon;
use Config;
use URL;
use Session;
use Redirect;
use Input;
use Log;
use MangiaeBevi\Orders;

class PaypalController extends Controller
{
    private $_api_context;

    public function __construct()
    {
        // setup PayPal api context
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }


    /**
    *
    * Processa pagamenti dei pacchetti annuali da parte dei clienti
    *
    * @param Illuminate/Http/Request pack => _id pacchetto sottoscritto
    * @return boh
    * */
    public function postPaymentPack(Request $request)
    {

        $this->validate($request, [
            'pack'  => 'required|alpha_num'
        ]);

        $pacchetto = Pacchetti::find($request->input('pack'));
        if(!$pacchetto) return response()->json(['message'=>'error.empty'], 200);


        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item = new Item();
        $item->setName($pacchetto->name) // item name
            ->setCurrency('EUR')
            ->setQuantity(1)
            ->setPrice($pacchetto->price_annual); // unit price



        // add item to list
        $item_list = new ItemList();
        $item_list->setItems(array($item));

        $amount = new Amount();
        $amount->setCurrency('EUR')
            ->setTotal($pacchetto->price_annual);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Annual MangiaeBevi+ subscription');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('api/v1/payments/resultPayPack'))
            ->setCancelUrl(URL::route('api/v1/payments/resultPayPack'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                echo "Exception: " . $ex->getMessage() . PHP_EOL;
                $err_data = json_decode($ex->getData(), true);
                exit;
            } else {
                die('Some error occur, sorry for inconvenient');
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        // add payment ID to session
        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {
            // redirect to paypal
            return Redirect::away($redirect_url);
        }

        return Redirect::route('')
            ->with('error', 'Unknown error occurred');
    }



    /**
    *
    * Processa pagamenti degli ordini da parte dei clienti
    * @see http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/CreatePaymentUsingPayPal.html
    *
    * @param Illuminate/Http/Request pack => _id pacchetto sottoscritto
    * @return boh
    * */
    public function postPaymentOrder()
    {


        $order_id = Session::get('current_order');
        $order = Orders::find($order_id);

        if(!$order) return response()->json(['message'=>'error.empty'],200);

        /**
        * @var amount
        * */
        $amount = number_format ( floatval( $order->price ) , 2);



        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item = new Item();
        $item->setName('Pagamento ordine n. #'.$order->_id) // item name
            ->setCurrency('EUR')
            ->setQuantity(1)
            ->setPrice($amount); // unit price



        // add item to list
        $item_list = new ItemList();
        $item_list->setItems(array($item));

        $amount_pp = new Amount();
        $amount_pp->setCurrency('EUR')
            ->setTotal($amount);

        $transaction = new Transaction();
        $transaction->setAmount($amount_pp)
            ->setItemList($item_list)
            ->setDescription('Ordine n. #'.$order->_id)
            ->setInvoiceNumber(uniqid());

        $redirect_urls = new RedirectUrls();
        $redirect_urls
            ->setReturnUrl(env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/api/v1/payments/paypalAccept/'.$order->_id.'/')
            ->setCancelUrl(env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/api/v1/payments/paypalCancel/'.$order->_id.'/');

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                Log::info("Exception: " . $ex->getMessage() . PHP_EOL);
                //return response()->json(['message'=>'error.no_paypal','error'=>json_decode($ex->getData(), true)],200);
                exit;
            } else {
                return response()->json(['message'=>'error.no_paypal'],200);
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        // add payment ID to session
        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) :
            // redirect to paypal
            return redirect($redirect_url);
            //return response()->json(['message'=>'ok','redirect'=>$redirect_url],200);
        endif;

        return response()->json(['message'=>'error.no_paypal'],200);
    }


    /**
    *
    * esegui il pagamento paypal
    * @param payer_id
    * @param payment_id
    * @return bool
    *
    * */
    public function executePaypalPayment($payer,$payment_id){

        $payment = Payment::get($payment_id, $this->_api_context);

        $execution = new PaymentExecution();
        $execution->setPayerId($payer);

        $result = $payment->execute($execution, $this->_api_context);
        return true;
    }

    /**
    *@deprecated
    **/
    public function getPaymentStatusOrder()
    {
        // Get the payment ID before session clear
        $payment_id = Session::get('paypal_payment_id');

        // clear the session payment ID
        Session::forget('paypal_payment_id');

        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            return Redirect::route('original.route')
                ->with('error', 'Payment failed');
        }

        $payment = Payment::get($payment_id, $this->_api_context);

        // PaymentExecution object includes information necessary
        // to execute a PayPal account payment.
        // The payer_id is added to the request query parameters
        // when the user is redirected from paypal back to your site
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        //Execute the payment
        $result = $payment->execute($execution, $this->_api_context);

        echo '<pre>';print_r($result);echo '</pre>';exit; // DEBUG RESULT, remove it later

        if ($result->getState() == 'approved') { // payment made
            return Redirect::route('original.route')
                ->with('success', 'Payment success');
        }
        return Redirect::route('original.route')
            ->with('error', 'Payment failed');
    }




    /**
    *
    * Get Payment details
    * @param request
    * @return response
    * **/
    public function getPaymentDetails(Request $request)
    {
        $this->validate($request,[
            'payment'=>'required|string'
        ]);

        $payment = Payment::get($request->input('payment'), $this->_api_context);
        return response()->json(['message'=>'ok','payment'=>$payment],200);
    }


    /**
    *
    * Refund a Paypal Payment
    * @param request
    * @return response
    * **/

    public function paypalRefundPayment(Request $request){

        $this->validate($request,[
            'order'=>'required|alpha_num'
        ]);

        Log::info('pagamento');

        $order = Orders::find($request->input('order'));
        if(!$order) return response()->json(['message'=>'error.empty'],200);

        $payment = Payment::get($order->payment, $this->_api_context);
        //$transactions = $payment->getTransactions();

        $payment->getTransactions();
        $transactions = $payment->getTransactions();
        $relatedResources = $transactions[0]->getRelatedResources();
        $sale = $relatedResources[0]->getSale();
        $saleId = $sale->getId();

        $amt = new Amount();
        $amt->setCurrency('EUR')
        ->setTotal($order->price);

        $refund = new Refund();
        $refund->setAmount($amt);

        $sale = new Sale();
        $sale->setId($saleId);


        $refundPayment = $sale->refund($refund, $this->_api_context);

        $order->status = 7;
        $order->save();

        $parameters = [$order->_id];
        Notification::addNotification(8,$parameters,$order->user);

        return response()->json(['message'=>'ok','refund'=>$refundPayment],200);

    }
}
