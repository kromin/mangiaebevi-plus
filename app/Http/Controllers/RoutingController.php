<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;
use Log;
use Route;
use File;
use Storage;
use Validator;
use Cookie;
use App;

use MangiaeBevi\Iframe;
use MangiaeBevi\HomeCity;
use MangiaeBevi\User;
use MangiaeBevi\Restaurant;
use MangiaeBevi\Plates;
use MangiaeBevi\UserPlate;
use MangiaeBevi\TipoLocale;
use MangiaeBevi\Services;
use MangiaeBevi\Types;
use MangiaeBevi\Regions;
use MangiaeBevi\ReservationRequest;
use MangiaeBevi\RestaurantClient;
use MangiaeBevi\Recommendation;

use MangiaeBevi\Http\Controllers\SearchController as Search;
use MangiaeBevi\Http\Controllers\SeoController as Seo;

use mPDF;
use Curl;
use View;
use Response;
use Auth;
use Excel;

use Carbon\Carbon;

use Facebook\Facebook;
use Facebook\Facebook\Helpers\FacebookPageTabHelper;

class RoutingController extends Controller
{
    public static $num = 0;

    /**
     * Display a listing of the resource.
     *
     * @return view
     */
    public function returnStatus(Request $request){
        return response()->json(['message'=>'ok'],200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return view
     */
    public function returnFrontendView( Request $request)
    {
        //

        $uri = $request->getRequestUri();

        if($request):
            $r = $request->all();
            if ((isset($r['static']) && $r['static'])) :
                //Log::info('sto servendo una pagina statica');
                return SeoController::returnStaticPage($uri);
            endif;
        endif;


        return view('index');
    }

     /**
     * Ritorna un template nel frontend, lo uso per i modali
     *
     * @return view
     */
    public function returnFrontendTemplate($template_name = '')
    {
        //
        if($template_name == ''):
            return view('templates/frontend/index');
        else :
            if (view()->exists('templates/frontend/'.$template_name)) :
                return view('templates/frontend/'.$template_name);
            endif;
        endif;

    }

    /**
     * Return test iframe view
     *
     * @return view
     */
    public function returnIframeTestView()    {
        return view('iframetest');
    }
    /**
     * Return Iframe main view
     *
     * @return view
     */
    public function returnIframeView($restaurant)
    {
        if(!$restaurant) return;

        $restauranto = Iframe::where('restaurant',$restaurant)->firstOrFail();
        if(!$restauranto) return;

        return view('iframe',['url' => $restauranto->url]);
    }

    /**
     * Return Facebook iframe
     *
     * @return view
     */
    public function returnFacebookFrame(Request $request)
    {

        $fb = new Facebook([
          'app_id' => env('FB_ID'),
          'app_secret' => env('FB_SECRET'),
          'default_graph_version' => 'v2.2',
          ]);


        //$fb = App::make('SammyK\LaravelFacebookSdk\LaravelFacebookSdk');
        $helper = $fb->getPageTabHelper();
        $accessToken = $helper->getAccessToken();
        if (! isset($accessToken)) {
            exit;
        }
        $p = $helper->getPageId();
        return view('facebook')->withPage($p);
    }

    /**
     * Return Facebook iframe
     *
     * @return view
     */
    public function returnFakeFacebookFrame(Request $request)
    {

        return view('facebook')->withPage('149796585185243');
    }

    /**
     * Return Facebook template
     *
     * @return view
     */
    public function returnFacebookTemplate($template_name = '')
    {

        if (view()->exists('templates/facebook/'.$template_name)) :
            return view('templates/facebook/'.$template_name);
        endif;
    }



    public function returnIframeTemplate($template_name = '')
    {
        //
        if($template_name == ''):
            return view('templates/iframe/restaurant');
        else :
            if (view()->exists('templates/iframe/'.$template_name)) :
                return view('templates/iframe/'.$template_name);
            endif;
        endif;

    }

    /**
    *
    * Return Homepage template
    *
    * @param bool $with_alert (for alert verification)
    * @return \View application\home
    * ***/
    public function returnHome($with_alert = false){
        $home_city = HomeCity::orderBy('order', 'asc')->get(['name','image','lat','lng']);

        $seo = [
            'MangiaeBevi+ - Prenota e pre-ordina nei migliori ristoranti d’Italia',
            '/',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'food',
            'Con MangiaeBevi+ puoi prenotare il tuo tavolo preferito nei migliori ristoranti e pre-ordinare il tuo menu direttamente online evitando code e attese.'
        ];
        if(!$with_alert)
            return view('/application/home')->withCities($home_city)->withSeo($seo);
        else
            return view('/application/home')->withCities($home_city)->withSeo($seo)->withAlert(1);
    }

    /**
    *
    * Return Login template
    *
    * @param bool $with_alert (for alert verification)
    * @return \View application\home
    * ***/
    public function returnLogin(){
        if(Auth::check()) return $this->returnHome();
        $seo = [
            'MangiaeBevi+ | Accedi',
            '/',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'food',
            'MangiaeBevi+, la guida per il palato, cerca tra centinaia di ristoranti, scopri la cucina, i menu, gli orari e prenota comodamente online'
        ];

        return view('/application/login')->withSeo($seo);

    }

    /**
    *
    * Return Registration template
    *
    * @param string $cookie
    * @return \View application\home
    * ***/
    public function returnRegister($cookie = false){
        if($cookie) Cookie::make('ib',$cookie,60*24*365);

        if(Auth::check()) return $this->returnHome();
        $seo = [
            'MangiaeBevi+ | Registrati',
            '/',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'food',
            'Registrati su MangiaeBevi+, la guida per il palato, unisciti alla community o gestisci il tuo ristorante'
        ];

        return view('/application/register')->withSeo($seo);

    }

    /**
    *
    * Return Recovery password template
    *
    * @return \View application\home
    * ***/
    public function returnRecovery(){
        if(Auth::check()) return $this->returnHome();
        $seo = [
            'MangiaeBevi+ | Recupera password',
            '/',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'food',
            'MangiaeBevi+, la guida per il palato, cerca tra centinaia di ristoranti, scopri la cucina, i menu, gli orari e prenota comodamente online'
        ];

        return view('/application/recovery')->withSeo($seo);

    }

    /**
    *
    * Return Recovery password template
    *
    * @return \View application\home
    * ***/
    public function returnActivate(){
        if(Auth::check()) return $this->returnHome();
        $seo = [
            'MangiaeBevi+ | Attiva account',
            '/',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'food',
            'MangiaeBevi+, la guida per il palato, cerca tra centinaia di ristoranti, scopri la cucina, i menu, gli orari e prenota comodamente online'
        ];

        return view('/application/activate')->withSeo($seo);

    }


    /**
    *
    * Return Static page template
    *
    * @param string $page
    * @return \View application\home
    * ***/
    public function returnStaticPage($page){
        $break = false;
        switch($page){
            case 'chi-siamo':
                $page_name = 'who';
                $blue_header = 0;
                $seo = [
                'Chi siamo | MangiaeBevi+','/chi-siamo','/img/pages/chi_siamo.jpg','/img/pages/chi_siamo.jpg','','Leggi la storia e l\'evoluzione di MangiaeBevi+'
                ];
            break;
            case 'lavora-con-noi':
                $page_name = 'job';
                $blue_header = 0;
                $seo = [
                'Lavora con noi | MangiaeBevi+','/lavora-con-noi','/img/pages/business.jpg','/img/pages/business.jpg','','Lavora con noi, entra nel team di MangiaeBevi+, consulta le posizioni aperte ed inviaci il tuo curriculum'
                ];
            break;
            case 'contattaci':
                $page_name = 'contact';
                $blue_header = 0;
                $seo = [
                'Contattaci | MangiaeBevi+','/contattaci','/img/contact.jpg','/img/contact.jpg','','Contatta il supporto di MangiaeBevi+ per informazioni commerciali e assistenza'
                ];
            break;
            case 'come-funziona':
                $page_name = 'how';
                $blue_header = 0;
                $seo = [
                'Come funziona | MangiaeBevi+','/come-funziona','/img/pages/cibo.jpg','/img/pages/cibo.jpg','','Leggi come funziona MangiaeBevi+, cerca un ristorante o prenota al tuo ristorante preferito'
                ];
            break;
            case 'programma-fedelta':
                $page_name = 'fidelity';
                $blue_header = 0;
                $seo = [
                'Programma fedeltà | MangiaeBevi+','/programma-fedelta','/img/pages/fidelity.jpg','/img/pages/fidelity.jpg','','A breve online il programma fedeltà di MangiaeBevi+, la guida per il palato'
                ];
            break;
            case 'termini-e-condizioni':
                $page_name = 'terms';
                $blue_header = 0;
                $seo = [
                'Termini e condizioni | MangiaeBevi+','/termini-e-condizioni/','','','','MangiaeBevi+, termini e condizioni'
                ];
            break;
            case 'privacy-policy':
                $page_name = 'privacy';
                $blue_header = 0;
                $seo = [
                'Privacy policy | MangiaeBevi+','/privacy-policy','','','','MangiaeBevi+, privacy e cookie policy'
                ];
            break;
            case 'promuovi-il-tuo-ristorante':
                $page_name = 'promote';
                $blue_header = 0;
                $seo = [
                'Promuovi il tuo ristorante | MangiaeBevi+','/promuovi-il-tuo-ristorante','/img/pages/ristoratori.jpg','/img/pages/ristoratori.jpg','','Promuovi il tuo ristorante con MangiaeBevi+, fidelizza i tuoi clienti e gestisci le prenotazioni della tua attività'
                ];
            break;
            case 'aggiungi-un-ristorante':
                $page_name = 'addrestaurant';
                $blue_header = 0;
                $seo = [
                'Aggiungi un ristorante | MangiaeBevi+','/aggiungi-un-ristorante','/img/pages/ristoratore.png','/img/pages/ristoratore.png','food','Aggiungi il tuo ristorante su MangiaeBevi+, la guida per il palato'
                ];
            break;
            default:
                return $this->return404();
                exit;
            break;
        }

        return view('/application/static')->withSeo($seo)->withBlue_header($blue_header)->withPage_name($page_name);

    }

    /**
    *
    * Mostra archivio per le città principali
    *
    * **/
    public function returnArchiveCity($city,$lat = 0, $lng = 0,$page = 0){


        $arr = [
            'city'=>$city,
            'lat'=>$lat,
            'lng'=>$lng,
            'page'=>$page
        ];
        $validator = Validator::make($arr, [
            'city' => 'required|string',
            'lat' => 'numeric',
            'lng' => 'numeric',
            'page' => 'numeric'
        ]);

        if ($validator->fails()) return $this->return404();

        $home_city = HomeCity::where('name', $city)->get();
        if(count($home_city) == 0) return $this->return404();


        $url = '/citta/'.htmlspecialchars($city,ENT_QUOTES).'/';
        if($lat != 0 && $lng != 0) $url .= '/'.$lat.'/'.$lng.'/';
        if($page != 0) $url .= $page.'/';

        $seo = [
            'Ristoranti a ' . remove_quotes_and_tags ( $city ) . ' | MangiaeBevi+',
            $url,
            env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/homecity/big/'.$home_city[0]->image.'.png',
            env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/homecity/square/'.$home_city[0]->image.'.png',
            'food',
            'Elenco dei ristoranti su MangiaeBevi+ nella città '. remove_quotes_and_tags ( $city )
        ];

        $located = ($lat != 0 && $lng != 0) ? [$lat,$lng] : [];

        $restaurants = Search::searchByCityStatic($city,$located);
        $page_num = $this->returnPageNumber(count($restaurants));

        // paginazione
        $page = ($page == 0) ? 1 : (($page > $page_num || $page < 0) ? 1 : $page);

        $skip = (12*$page)-12;

        $return_restaurants = [];
        $skipped = 0;
        $taken = 0;
        $result_number = 0; // per mappa e capire quando inizializzarla
        for($s = $skip; $s < $skip+12; $s++):
            if(isset($restaurants[$s])) {
                $return_restaurants[] = $restaurants[$s];
                $result_number++;
            }
        endfor;

        $page_link = '/citta/'.htmlspecialchars($city,ENT_QUOTES).'/';
        if($lat != 0 && $lng != 0) $page_link .= $lat.'/'.$lng.'/';
        // fine paginazione


        if($lat != 0 && $lng != 0):
            return view('/application/archive')
                ->withType('city')
                ->withSeo( $seo)
                ->withRestaurants( $return_restaurants)
                ->withPage_num( $page_num)
                ->withNo_index(true)
                ->withCurrent_page($page)
                ->withPage_link($page_link)
                ->withResult_number($result_number);
        else:
            return view('/application/archive')
                ->withType('city')
                ->withSeo( $seo)
                ->withRestaurants( $return_restaurants)
                ->withPage_num( $page_num)
                ->withCurrent_page($page)
                ->withPage_link($page_link)
                ->withResult_number($result_number);
        endif;
    }

    public function returnArchiveCityPaged($city,$page){
        return $this->returnArchiveCity($city,0,0,$page);
    }

    /**
    *
    * Mostra archivio per le città principali
    *
    * **/
    public function returnArchiveCustom($slug, $lat = 0, $lng = 0,$page = 0){


        $string_type = Route::getCurrentRoute()->getName();

        $arr = [
            'slug'=>$slug,
            'lat'=>$lat,
            'lng'=>$lng,
            'page'=>$page
        ];
        $validator = Validator::make($arr, [
            'slug' => 'required|string',
            'lat' => 'numeric',
            'lng' => 'numeric',
            'page' => 'numeric'
        ]);

        if ($validator->fails()) return $this->return404();


        switch($string_type):
            case 'tipo-cucina':
            $cuc = Types::where('slug', $slug)->get();
            break;
            case 'regione-cucina':
            $cuc = Regions::where('slug', $slug)->get();
            break;
            case 'servizi':
            $cuc = Services::where('slug', $slug)->get();
            break;
            case 'tipo-locale':
            $cuc = TipoLocale::where('slug', $slug)->get();
            break;
            // redirect 404 if error
            default:
            return $this->return404();
            break;
        endswitch;

        if(count($cuc) == 0) return $this->return404();


        $url = '/'.$string_type.'/'.$slug.'/';
        if($lat != 0 && $lng != 0) $url .= '/'.$lat.'/'.$lng.'/';
        if($page != 0) $url .= $page.'/';

        $seo = [
            'Ristoranti per la ricerca ' . $slug . ' | MangiaeBevi+',
            $url,
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'food',
            'Elenco dei ristoranti su MangiaeBevi+ per la ricerca: '. $slug
        ];

        $located = ($lat != 0 && $lng != 0) ? [$lat,$lng] : [];
        switch($string_type):
            case 'tipo-cucina':
            $restaurants = Search::searchByCucinaStatic($cuc[0]->_id,$located);
            break;
            case 'regione-cucina':
            $restaurants = Search::searchByRegioneStatic($cuc[0]->_id,$located);
            break;
            case 'servizi':
            $restaurants = Search::searchByServiceStatic($cuc[0]->_id,$located);
            break;
            case 'tipo-locale':
            $restaurants = Search::searchByLocaleStatic($cuc[0]->_id,$located);
            break;
        endswitch;


        $page_num = $this->returnPageNumber(count($restaurants));

        // paginazione
        $page = ($page == 0) ? 1 : (($page > $page_num || $page < 0) ? 1 : $page);

        $skip = (12*$page)-12;

        $return_restaurants = [];
        $skipped = 0;
        $taken = 0;
        $result_number = 0; // per mappa e capire quando inizializzarla
        for($s = $skip; $s < $skip+12; $s++):
            if(isset($restaurants[$s])) {
                $return_restaurants[] = $restaurants[$s];
                $result_number++;
            }
        endfor;

        $page_link = '/'.$string_type.'/'.$slug.'/';
        if($lat != 0 && $lng != 0) $page_link .= $lat.'/'.$lng.'/';
        // fine paginazione


        if($lat != 0 && $lng != 0):
            return view('/application/archive')
                ->withType('city')
                ->withSeo( $seo)
                ->withRestaurants( $return_restaurants)
                ->withPage_num( $page_num)
                ->withNo_index(true)
                ->withCurrent_page($page)
                ->withPage_link($page_link)
                ->withResult_number($result_number);
        else:
            return view('/application/archive')
                ->withType('city')
                ->withSeo( $seo)
                ->withRestaurants( $return_restaurants)
                ->withPage_num( $page_num)
                ->withCurrent_page($page)
                ->withPage_link($page_link)
                ->withResult_number($result_number);
        endif;
    }

    public function returnArchiveCustomPaged($slug,$page){
        return $this->returnArchiveCustom($slug,0,0,$page);
    }


    /**
    *
    * Mostra archivio per ricerca dal blog
    *
    * **/
    public function returnArchiveFromBlog($cucina,$stringa,$page = 0){

        $decoded_string = urldecode($stringa);
        $arr = [
            'cucina'=>$cucina,
            'stringa'=>$decoded_string,
            'page'=>$page
        ];
        $validator = Validator::make($arr, [
            'cucina' => 'required|string',
            'stringa' => 'required|string',
            'page' => 'numeric'
        ]);

        if ($validator->fails()) return $this->return404();

        $slug_cucina = Types::where('slug', $cucina)->get();
        if(count($slug_cucina) == 0) return $this->return404();


        $url = '/b/'.$cucina.'/'.urlencode($decoded_string).'/';

        if($page != 0) $url .= $page.'/';

        $seo = [
            'Ristoranti | MangiaeBevi+',
            $url,
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'food',
            'Elenco dei ristoranti su MangiaeBevi+'
        ];

        $restaurants = Search::searchByFromBlogStatic($slug_cucina[0]->_id,$stringa);
        $page_num = $this->returnPageNumber(count($restaurants));

        // paginazione
        $page = ($page == 0) ? 1 : (($page > $page_num || $page < 0) ? 1 : $page);

        $skip = (12*$page)-12;

        $return_restaurants = [];
        $skipped = 0;
        $taken = 0;
        $result_number = 0; // per mappa e capire quando inizializzarla
        for($s = $skip; $s < $skip+12; $s++):
            if(isset($restaurants[$s])) {
                $return_restaurants[] = $restaurants[$s];
                $result_number++;
            }
        endfor;

        $page_link = '/b/'.$cucina.'/'.urlencode($decoded_string).'/';
        // fine paginazione
        return view('/application/archive')
                ->withType('city')
                ->withSeo( $seo)
                ->withRestaurants( $return_restaurants)
                ->withPage_num( $page_num)
                ->withNo_index(true)
                ->withCurrent_page($page)
                ->withPage_link($page_link)
                ->withResult_number($result_number);

    }

    public function returnArchiveFromBlogPaged($cucina,$stringa,$page){
        return $this->returnArchiveFromBlog($cucina,$stringa,$page);
    }



    public function returnRestaurant($city,$restaurant, $cookie = false){

        $no_index = false;

        if($cookie) :
            Cookie::make('ib',$cookie,60*24*365);
        endif;


        $r = Restaurant::where('slug',$restaurant)->get();

        if(count($r) == 0) return $this->return404();

        $risto = $r[0];
        //if(!$risto->city || $risto->city != $city || $risto->city != urldecode($city)) return $this->return404();

        $i = ($risto->image) ? env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/restaurant/big/'.$risto->image.'.png' : env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg';

        $url = '/'.$city.'/'.$restaurant.'/';
        $url .= ($cookie) ? $cookie.'/' : '';

        $seo = [
            '' .htmlspecialchars ( $risto->name, ENT_QUOTES, 'UTF-8'). ' | '.htmlspecialchars ( $risto->city, ENT_QUOTES, 'UTF-8').' | MangiaeBevi+',
            $url,
            $i,
            $i,
            'restaurant',
            ''.htmlspecialchars ( $risto->seodescription, ENT_QUOTES, 'UTF-8')
        ];



        $seocall = Seo::returnRestaurantLDSStatic($risto);


        $share_url = env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/'.urlencode($city).'/'.$risto->slug.'/';


        $related = Search::searchRelatedStatic($risto->_id);



        if($cookie) :
            return view('/application/restaurant')
                ->withType('single_restaurant')
                ->withSeo($seo)
                ->withRestaurant($risto)
                ->withCanonical('/'.$city.'/'.$restaurant.'/')
                ->withLds($seocall[0])
                ->withRestaurant_infos($seocall[1])
                ->withShare_url($share_url )
                ->withRelated($related);
        else :
            return view('/application/restaurant')
                ->withType('single_restaurant')
                ->withSeo($seo)
                ->withRestaurant($risto)
                ->withLds($seocall[0])
                ->withRestaurant_infos($seocall[1])
                ->withShare_url($share_url )
                ->withRelated($related);
        endif;

    }


    public function returnQueryStringResult($main_string,$lat = 0, $lng = 0,$page = 0){

        $string = urldecode($main_string);

        $arr = [
            'string'=>$string,
            'lat'=>$lat,
            'lng'=>$lng,
            'page'=>$page
        ];
        $validator = Validator::make($arr, [
            'string' => 'required|string',
            'lat' => 'numeric',
            'lng' => 'numeric',
            'page' => 'numeric'
        ]);

        if ($validator->fails()) return $this->return404();

        $url = '/s/'.htmlspecialchars($main_string,ENT_QUOTES).'/';
        if($lat != 0 && $lng != 0) $url .= '/'.$lat.'/'.$lng.'/';
        if($page != 0) $url .= $page.'/';

        $seo = [
            'Risultati della ricerca ' .htmlspecialchars ( $string, ENT_QUOTES, 'UTF-8'). ' | MangiaeBevi+',
            $url,
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'food',
            'Elenco dei ristoranti su MangiaeBevi+ per la ricerca '.htmlspecialchars ( $string, ENT_QUOTES, 'UTF-8')
        ];

        $located = ($lat != 0 && $lng != 0) ? [$lat,$lng] : [];
        $restaurants = Search::searchByStringStatic($string,$located);
        $page_num = $this->returnPageNumber(count($restaurants));

        // paginazione
        $page = ($page == 0) ? 1 : (($page > $page_num || $page < 0) ? 1 : $page);

        $skip = (12*$page)-12;

        $return_restaurants = [];
        $skipped = 0;
        $taken = 0;
        $result_number = 0; // per mappa e capire quando inizializzarla
        for($s = $skip; $s < $skip+12; $s++):
            if(isset($restaurants[$s])) {
                $return_restaurants[] = $restaurants[$s];
                $result_number++;
            }
        endfor;

        $page_link = '/s/'.htmlspecialchars($main_string,ENT_QUOTES).'/';
        if($lat != 0 && $lng != 0) $page_link .= $lat.'/'.$lng.'/';
        // fine paginazione

        return view('/application/archive')
                ->withType('query_string')
                ->withSeo($seo)
                ->withRestaurants($return_restaurants)
                ->withPage_num($page_num)
                ->withNo_index(true)
                ->withCurrent_page($page)
                ->withPage_link($page_link)
                ->withResult_number($result_number);
    }

    public function returnQueryStringResultPaged($string,$page){
        return $this->returnQueryStringResult($string,0,0,$page);
    }

    public function returnAdvancedSearch($query_encoded, $page = 0){
        $query = json_decode(urldecode($query_encoded),true);

        if(!is_array($query)) return $this->return404();

        $validator = Validator::make($query, [
            'name'     => 'string', // valid for name and address
            'chef'     => 'string',
            'type'     => 'string',
            'location' => 'string',
            'maxPrice' => 'integer',
            'services' => 'array',
            'types' => 'array',
            'regions' => 'array',
            'recensioni' => 'integer',
            'virtualTour' => 'boolean',
            'prenota'=>'integer',
            'menu'=>'boolean'
        ]);

        if ($validator->fails()) return $this->return404();

        $url = '/ricerca-avanzata/'.htmlspecialchars( $query_encoded, ENT_QUOTES ).'/';
        if($page != 0) $url .= $page.'/';

        $seo = [
            'Risultati della ricerca avanzata | MangiaeBevi+',
            $url,
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'food',
            'Elenco dei ristoranti su MangiaeBevi+ per la ricerca avanzata'
        ];

        $restaurants = (array) Search::searchByValuesStatic($query);

        $page_num = $this->returnPageNumber(count($restaurants));

        // paginazione
        $page = ($page == 0) ? 1 : (($page > $page_num || $page < 0) ? 1 : $page);

        $skip = (12*$page)-12;

        $return_restaurants = [];
        $skipped = 0;
        $taken = 0;
        $result_number = 0; // per mappa e capire quando inizializzarla
        for($s = $skip; $s < $skip+12; $s++):
            if(isset($restaurants[$s])) {
                $return_restaurants[] = $restaurants[$s];
                $result_number++;
            }
        endfor;

        $page_link = '/ricerca-avanzata/'.htmlspecialchars( $query_encoded, ENT_QUOTES ).'/';

        // fine paginazione

        return view('/application/archive')
            ->withType('advanced_search')
            ->withSeo($seo)
            ->withRestaurants($return_restaurants)
            ->withPage_num($page_num)
            ->withNo_index(true)
            ->withCurrent_page($page)
            ->withPage_link($page_link)
            ->withResult_number($result_number);
    }

    /**
    *
    * Modifica ristorante
    *
    * **/
    public function returnEditView($restaurant){
        if(!Auth::check() || !Auth::user()->role) return $this->return404();

        $seo = [
            'Modifica il ristorante | MangiaeBevi+',
            '/modifica-ristorante/'.$restaurant.'/',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            '',
            'Modifica il tuo ristorante'
        ];
        return view('/application/edit')
                ->withType('advanced_search')
                ->withSeo($seo)
                ->withRestaurant($restaurant)
                ->withNo_index(true);
    }

    public function returnPrintMenu($restaurant,$menuindex,$menustyle,$pdf){
        $r = Restaurant::find($restaurant);
        if(!$r
            || !isset($r['menu'])
            || !isset($r['menu']['menus'])
            || !is_array($r['menu']['menus'])
            || count($r['menu']['menus'])-1 < intval($menuindex)) return $this->return404();

        $menu = $r['menu']['menus'][intval($menuindex)];

        $type = ($pdf == 0) ? 'print' : 'pdf';
        return view('/application/printmenu')
                ->withMenu($menu)
                ->withRestaurant($r)
                ->withType($type)
                ->withStyle($menustyle);
    }

    public function returnPrintPDF($restaurant,$menuindex,$menustyle){
        $r = Restaurant::find($restaurant);
        if(!$r
            || !isset($r['menu'])
            || !isset($r['menu']['menus'])
            || !is_array($r['menu']['menus'])
            || count($r['menu']['menus'])-1 < intval($menuindex)) return $this->return404();

        $mpdf = new mPDF("it", "A4", "15");
        $url = env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/printMenuPage/'.$restaurant.'/'.$menuindex.'/'.$menustyle.'/1/';

        $html = Curl::to($url)->get();

        //$mpdf->useSubstitutions = true; // optional - just as an example
        //$mpdf->SetHeader($url . "\n\n" . 'Page {PAGENO}');  // optional - just as an example
        $stylesheet = Curl::to(env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/app.css')->get();
        $mpdf->WriteHTML($stylesheet,1);
        //$mpdf->WriteHTML($html,2);

        $mpdf->WriteHTML($html,2);

        $contents = View::make('application/pdf')->with('cn', $mpdf->Output());
        $response = Response::make($contents, 200);
        $response->header('Content-Type', 'application/pdf');
        return $response;

    }


    public function returnExcelClients($restaurant){
        $r = Restaurant::find($restaurant);

        if(Auth::user()->_id != $r->user && Auth::user()->role > 3) return $this->return404();

        $clients = $r->clients()->with('user')->orderBy('name', 'DESC')->orderBy('surname', 'DESC')->get();

        $clienti = [];

        foreach ($clients as $c) {
            $clienti[] = [
                'nome'=>$c->name,
                'cognome' => $c->surname,
                'email' => (isset($c->email)) ? $c->email : ( (isset($c->user->email)) ? $c->user->email : ''),
                'mobile' => (isset($c->phone)) ? esc_html($c->phone) : ( (isset($c->user->phone)) ? $c->user->phone : ''),
                'telefono' => (isset($c->phone_)) ? esc_html($c->phone_) : '',
                'data di nascita' => (isset($c->dt)) ? esc_html($c->dt) : '',
                'indirizzo' => (isset($c->address)) ? esc_html($c->address) : '',
                'cap' => (isset($c->cap)) ? esc_html($c->cap) : '',
                'città'=>(isset($c->city)) ? esc_html($c->city) : '',
                'provincia'=>(isset($c->pr)) ? esc_html($c->pr) : '',
                'stato'=>(isset($c->state)) ? esc_html($c->state) : '',
                'lingua'=>(isset($c->language)) ? esc_html($c->language) : '',
                'allergie'=>(isset($c->allergie)) ? esc_html(implode(", " , $c->allergie)) : '',
                'preferenze'=>(isset($c->preferenze)) ? esc_html(implode(", " , $c->preferenze)) : '',
                'note'=>(isset($c->note)) ? esc_html($c->note) : ''
            ];
        }
        //var_dump($clienti); die();
        Excel::create('clienti_'.$r->slug, function($excel) use($clienti) {

            $excel->sheet('Lista clienti', function($sheet) use($clienti) {

                $sheet->fromArray($clienti);

            });

        })->export('xls');
    }

    public function getSingleRestaurantClientScheda($restaurant, $client)
    {
        $client = RestaurantClient::find($client);
        if(!$client || $client->restaurant_id != $restaurant) return $this->return404();

        $r = Restaurant::find($restaurant);
        if(Auth::user()->_id != $r->user && Auth::user()->role > 3) return $this->return404();


        $res = ReservationRequest::where('restaurant',$r->_id)
        ->where('user',$client->user_id)
        ->orderBy('date','desc')
        ->orderBy('time','desc')
        ->get();

        $n_p = 0;
        $n_pc = 0;
        $n_pnc = 0;
        $last = '-';
        $current = 0;
        $preorders = 0;
        foreach ($res as $r) :
            $n_p++;
            if(isset($r->preorder)) $preorders++;
            if($r->assigned_foodcoin) :
                if($r->removed_foodcoin):
                    $n_pnc++;
                else:
                    $n_pc++;
                endif;
            endif;


            if($current == 0) $last = $r->date->format('d/m/Y').' '.$r->time;
            $current++;

        endforeach;
        $raccomandato = Recommendation::where('user',$client->_id)->where('ref',$restaurant)->where('type',0)->get();
        $ret_rac = (count($raccomandato) <= 0) ? 0 : 1;

        $infos = [
            'reccomended'=>$ret_rac, // 0 no, 1 si
            'reservations'=>$n_p,
            'preorders'=>$preorders,
            'confirmed_reservations'=>$n_pc,
            'discarded_reservations'=>$n_pnc,
            'last'=>$last
        ];


        return view('/application/printscheda')
                ->withClient($client)->withInfos($infos);


    }



    public function returnViewReservations($restaurant,$type,$from,$to,$token){

        $r = Restaurant::find($restaurant);
        if(!$r) return $this->return404();

        if($r->temporary_token != $token || $r->temporary_token == '') return view('/application/reservationsview')->withReservations([]);

        // togli il token
        $r->temporary_token = '';
        $r->save();

        $struttura = (isset($r->struttura) && isset($r->struttura['sale'])) ? $r->struttura : ['sale'=>[['name'=>'']]];

        switch($type):
            case 0:
            $vars = ['from'=>$from,'to'=>$to];
            $validator = Validator::make($vars, [
                'from'     => 'required|date_format:d-m-Y',
                'to'     => 'required|date_format:d-m-Y'
            ]);

            if ($validator->fails()) return view('/application/reservationsview')->withReservations([]);

            $f = Carbon::createFromFormat('d-m-Y H:i', $from." 00:00");
            $t = Carbon::createFromFormat('d-m-Y H:i', $to." 24:00");

            $reservations = ReservationRequest::where('restaurant',$restaurant)
                                                ->where('date','>=',$f)
                                                ->where('date','<=',$t)
                                                ->orderBy('date','asc')
                                                ->orderBy('fascia_oraria','asc')
                                                ->get();
            $testo_titolo = 'dal '.$from.' al '.$to;
            break;
            case 1:
            $vars = ['date'=>$from];
            $validator = Validator::make($vars, [
                'date'     => 'required|date_format:d-m-Y'
            ]);

            if ($validator->fails()) return view('/application/reservationsview')->withReservations([]);

            $f = Carbon::createFromFormat('d-m-Y H:i', $from." 00:00");
            $t = Carbon::createFromFormat('d-m-Y H:i', $from." 24:00");
            $reservations = ReservationRequest::where('restaurant',$restaurant)
                                                ->where('date','>=',$f)
                                                ->where('date','<=',$t)
                                                ->orderBy('date','asc')
                                                ->orderBy('fascia_oraria','asc')
                                                ->get();
            $testo_titolo = 'del '.$from;
            break;
            default:
                return view('/application/reservationsview')->withReservations([]);
            break;
        endswitch;

        return view('/application/reservationsview')
                ->withReservations($reservations)
                ->withStruttura($struttura)
                ->withTestoTitolo($testo_titolo);

    }

    public function returnExportReservationsPDF($restaurant,$type,$from,$to = false){
        $r = Restaurant::find($restaurant);
        if(!$r) return $this->return404();

        if(!Auth::check()) return $this->return404();
        if(Auth::user()->role < 3 && Auth::user()->_id != $r->user) return $this->return404();


        // salva un token per aprire la get
        $token = str_random(40);
        $r->temporary_token = $token;
        $r->save();

        switch($type):
            case 0:
            $url = env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/mostra/prenotazioni/'.$restaurant.'/0/'.$from.'/'.$to.'/'.$token;
            break;
            case 1:
            $url = env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/mostra/prenotazioni/'.$restaurant.'/1/'.$from.'/-/'.$token;
            break;
            default:
            return $this->return404();
            break;
        endswitch;

        $mpdf = new mPDF("it", "A4", "15");
        $html = Curl::to($url)->get();

        $stylesheet = Curl::to(env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/style.css')->get();
        $mpdf->WriteHTML($stylesheet,1);
        $mpdf->WriteHTML($html,2);

        $contents = View::make('application/reservationsexport')->with('cn', $mpdf->Output());
        $response = Response::make($contents, 200);
        $response->header('Content-Type', 'application/pdf');
        return $response;

    }

    /**
    *
    * Profilo utente
    *
    */
    public function returnUserProfile($user){

        $arr = ['u'=>$user];
        $validator = Validator::make($arr, [
            'u' => 'required|alpha_num',
        ]);

        if ($validator->fails()) return $this->return404();

        $u = User::find($user);
        if(!$u) return $this->return404();

        $is_friend = (Auth::check() && in_array(Auth::user()->_id, $u->friends)) ? true : false;

        $image = (isset($u->cover_image) && $u->cover_image != '')
                ? env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/users/medium/'.$u->cover_image .'.png'
                : ((isset($u->avatar) && $u->avatar != '')
                    ? env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/users/medium/'.$u->avatar.'.png'
                    : env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg');

        $seo = [
            'Profilo di '.htmlspecialchars ( $u->name, ENT_QUOTES, 'UTF-8').' '.htmlspecialchars ( $u->surname, ENT_QUOTES, 'UTF-8').' | MangiaeBevi+',
            '/u/'.$user.'/',
            $image,
            $image,
            '',
            'Profilo di '.htmlspecialchars ( $u->name, ENT_QUOTES, 'UTF-8').' '.htmlspecialchars ( $u->surname, ENT_QUOTES, 'UTF-8').' su MangiaeBevi+'
        ];


        $return_user = [
            '_id'=>$u->_id,
            'name'=>$u->name,
            'surname'=>$u->surname,
            'cover'=>(isset($u->cover_image) && $u->cover_image != '') ? env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/users/big/'.$u->cover_image .'.png' : env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg',
            'avatar'=>(isset($u->avatar) && $u->avatar != '') ? env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/users/medium/'.$u->avatar.'.png' : env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/default-avatar.jpg',
            'description'=>$u->description,
            'followers'=>(isset($u->followers_num)) ? $u->followers_num : 0,
            'following'=>(isset($u->following_num))  ? $u->following_num : 0,
            'friends'=>(isset($u->friends_num))  ? $u->friends_num : 0,
            'address'=>(isset($u->address)) ? $u->address : '',
            'foodcoin'=>(isset($u->foodcoin)) ? $u->foodcoin : 0,
            'profile_completed'=>(isset($u->profile_completed)) ? $u->profile_completed : 0,
            'current_address'=>(isset($u->current_address)) ? $u->current_address : ''
        ];

        if(Auth::check() && Auth::user()->_id == $user)
            return view('/application/user')->withSeo($seo)->withUser($user)->withIsme(true)->withUserArray($return_user)->withIsFriend($is_friend);
        else
            return view('/application/user')->withSeo($seo)->withUser($user)->withIsme(false)->withUserArray($return_user)->withIsFriend($is_friend);
    }


    public function returnRestaurantPlate($plate){
        $pl = Plates::find($plate);
        if(!$pl) return $this->return404();

        $restaurant = Restaurant::find($pl->restaurant);
        if(!$restaurant) return $this->return404();

        $i = ($pl->image) ? env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/restaurant/big/'.$pl->image.'.png' : env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg';

        $url = '/piatto/'.$pl->_id.'/';

        $seo = [
            '' .htmlspecialchars ( $pl->name, ENT_QUOTES, 'UTF-8'). ' | MangiaeBevi+',
            $url,
            $i,
            $i,
            '',
            'Il piatto ' .htmlspecialchars ( $pl->name, ENT_QUOTES, 'UTF-8'). ' su MangiaeBevi+'
        ];

        return view('/application/plate')->withPl($pl)->withRestaurant($restaurant)->withSeo($seo);

    }

    public function returnCommunityPlate($plate){
        $pl = UserPlate::find($plate);
        if(!$pl) return $this->return404();

        $parent = Plates::find($pl->plate);
        if(!$parent) return $this->return404();

        $restaurant = Restaurant::find($parent->restaurant);
        if(!$restaurant) return $this->return404();

        $user = User::find($pl->user);
        if(!$restaurant) return $this->return404();



        $i = env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/community/big/'.$pl->image.'.png' ;

        $url = '/community/piatto/'.$pl->_id.'/';

        $seo = [
            '' .htmlspecialchars ( $parent->name, ENT_QUOTES, 'UTF-8'). ' | MangiaeBevi+',
            $url,
            $i,
            $i,
            '',
            'La foto del piatto ' .htmlspecialchars ( $parent->name, ENT_QUOTES, 'UTF-8'). ' aggiunta da '.htmlspecialchars ( $user->name, ENT_QUOTES, 'UTF-8').' '.htmlspecialchars ( $user->surname, ENT_QUOTES, 'UTF-8').' su MangiaeBevi+'
        ];

        return view('/application/community_plate')->withPl($pl)->withRestaurant($restaurant)->withParent($parent)->withUser($user)->withSeo($seo);
    }

    public function return404(){

        $seo = [
            'MangiaeBevi+','/','/img/homebg.jpg','/img/homebg.jpg','','Pagina non trovata'
        ];

        return view('/application/404')->withNo_index(true)->withSeo($seo);

    }

    public function returnPageNumber($resultsNumber){
        $p_n = intval($resultsNumber / 12);
        if($resultsNumber % 12 != 0) $p_n++;

        return $p_n;
    }


    public function returnPreOrder($restaurant, $reservation){

        $no_index = false;

        $r = Restaurant::where('slug',$restaurant)->get();
        $reservationCollection = ReservationRequest::find($reservation);


        if(count($r) < 1 || !$reservationCollection || $reservationCollection->restaurant != $r[0]->_id || !Auth::check() || Auth::user()->_id != $reservationCollection->user) return $this->return404();


        $risto = $r[0];
        $seocall = Seo::returnRestaurantLDSStatic($risto);

        $i = ($risto->image) ? env('APP_PROTOCOL').'://'.env('APP_STATIC_DOMAIN').'/restaurant/big/'.$risto->image.'.png' : env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/img/homebg.jpg';

        $url = '/preorder/'.$restaurant.'/'.$reservation.'/';

        $seo = [
            '' .htmlspecialchars ( $risto->name, ENT_QUOTES, 'UTF-8'). ' | '.htmlspecialchars ( $risto->city, ENT_QUOTES, 'UTF-8').' | MangiaeBevi+',
            $url,
            $i,
            $i,
            'restaurant',
            ''.htmlspecialchars ( $risto->seodescription, ENT_QUOTES, 'UTF-8')
        ];

        $city = ($risto->city && $risto->city != '') ? urlencode($risto->city) : '-';
        $share_url = env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/'.$city.'/'.$risto->slug.'/';

        return view('/application/preorder')
            ->withType('single_restaurant')
            ->withSeo($seo)
            ->withUser(Auth::user()->_id)
            ->withRestaurant($risto)
            ->withShare_url($share_url )
            ->withReservation($reservationCollection)
            ->withRestaurant_infos($seocall[1]);

    }
}
