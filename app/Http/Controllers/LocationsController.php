<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use Jenssegers\Mongodb\Model as Eloquent;

use MangiaeBevi\Locations;
use MangiaeBevi\Restaurant;
class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $locations = Locations::all();        
        return response()->json(['message'=>'ok','locations'=>$locations], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        $locations = new Locations();
        $locations->name = $request->input('name');
        $locations->save();

        $statusCode = 200;
        $response = [
          'message' => 'ok' 
        ];
        return response()->json($response, $statusCode);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);

        $location = Locations::find($request->input('id'));
        if(!$location):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        
        $statusCode = 200;
        $response = [
          'message' => 'ok',
          'location' => $location 
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request, [
            'id'    => 'required|alpha_num',
            'name'  => 'required|string'
        ]);

        $location = Locations::find($request->input('id'));
        if(!$location):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        $location->name = $request->input('name');
        $location->save();
        
        $statusCode = 200;
        $response = [
          'message' => 'ok'
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|alpha_num'
        ]);
        $location = Locations::find($request->input('id'));
        if(!$location):
            return response()->json(['message'=>'error.empty'], 200);
        endif;
        $location->delete();
        return response()->json(['message'=>'Ok'], 200);
    }


    public function massiveFillLocations()
    {
        $restaurants = Restaurant::all();
        $locations = [];
        foreach($restaurants as $restaurant){ 
            if($restaurant->location) :          
                foreach($restaurant->location as $location){
                    $locations[] = $location;
                }
            endif;
        }

        $locations = array_unique($locations);
        
        foreach($locations as $location){
            $addToCollection = new Locations();
            $addToCollection->name = $location;
            $addToCollection->save();
        }
        return response()->json(['message'=>'ok'], 200);
    }

    public function massiveCleanRestaurantsLocations()
    {

        $locations = Locations::all();

        $restaurants = Restaurant::all();
        foreach($restaurants as $restaurant){
            // take restaurant locations 
            $single_locations = [];
            // per ogni servizio del ristorante
            if($restaurant->location){
                foreach($restaurant->location as $location){
                    // vedi i servizi dell'app e per ognuno di loro
                    foreach($locations as $single_location_collection){
                        // se il nome è uguale al servizio del ristorante
                        if($single_location_collection->name == $location){
                            // inseriscilo nell'array che aggiorna il ristorante
                            $single_locations[] = $single_location_collection->_id;
                        }
                    }
                }
            }
            if(!empty($single_locations)) :
                // aggiorniamo il singolo ristorante
                $this->updateRestaurantLocations($restaurant->_id,$single_locations);
            endif;
        }
        return response()->json(['message'=>'ok'], 200);
    }

    private function updateRestaurantLocations($restaurant_id,$locations)
    {
        $restaurant = Restaurant::find($restaurant_id);
        $restaurant->location = $locations;
        $restaurant->save();
    }


}
