<?php

namespace MangiaeBevi\Http\Controllers;

use Illuminate\Http\Request;

use MangiaeBevi\Http\Requests;
use MangiaeBevi\Http\Controllers\Controller;

use MangiaeBevi\API;

class APIController extends Controller
{
    /**
     * Display all iframes, only for administrator
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['message'=>'ok','apis'=>API::all()],200);
    }

    
    /**
     * Aggiungi un token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name'=>'required|string'
            ]);

        $key = str_random(40); // token da utilizzare per richiamare il frame
        $secret = str_random(40); // token da utilizzare per richiamare il frame
        $api = new API();

        $api->name = $request->input('name');
        $api->key = $key;
        $api->secret = $secret;
        $api->requests = 0;
        $api->save();

        return response()->json(['message'=>'ok','api'=>$api],200);
    }

    
    /**
     * Cancella token
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        //
        $this->validate($request,[
            'id'=>'required|alpha_num'
        ]);

        API::find($request->input('id'))->delete();
        return response()->json(['message'=>'ok'],200);
    }
}
