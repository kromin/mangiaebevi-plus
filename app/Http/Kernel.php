<?php

namespace MangiaeBevi\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \MangiaeBevi\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class/*,
        \MangiaeBevi\Http\Middleware\VerifyCsrfToken::class,
        \MangiaeBevi\Http\Middleware\CrawlerCheck::class,
        \MangiaeBevi\Http\Middleware\SEOMiddleware::class,
        \MangiaeBevi\Http\Middleware\ReturnTranslatedMessageMiddleware::class*/
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \MangiaeBevi\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \MangiaeBevi\Http\Middleware\RedirectIfAuthenticated::class,
        'notLogged' => \MangiaeBevi\Http\Middleware\LoggedMiddleware::class,
        'onlyLogged' => \MangiaeBevi\Http\Middleware\OnlyLoggedMiddleware::class,
        'isGod' => \MangiaeBevi\Http\Middleware\IsGod::class,
        'isAdmin' => \MangiaeBevi\Http\Middleware\IsAdmin::class,
        'isClient' => \MangiaeBevi\Http\Middleware\IsClient::class,
        'isClientRedirect' => \MangiaeBevi\Http\Middleware\IsClientRedirect::class,
        'isClientRedirectTemplate' => \MangiaeBevi\Http\Middleware\IsClientRedirectTemplate::class,
        'isEditor' => \MangiaeBevi\Http\Middleware\IsEditor::class,
        'isUser' => \MangiaeBevi\Http\Middleware\IsUser::class,
        'isMeOrIsAdmin' => \MangiaeBevi\Http\Middleware\isMeOrIsAdmin::class,
        'canManageRestaurant' => \MangiaeBevi\Http\Middleware\canManageRestaurant::class,
        'isMineImage' => \MangiaeBevi\Http\Middleware\isMineImage::class,
        'apiRequest' => \MangiaeBevi\Http\Middleware\APIRequestMiddleware::class
    ];
}
