<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


    Route::get('/getStatus', ['uses'=>'RoutingController@returnStatus']);
    Route::get('/verify/{code}', ['uses'=>'UserController@verify']);
    Route::get('/verify/confirmchangedmail/{code}', ['uses'=>'UserController@verifyChangedMail']);
    
    Route::get('/restaurant_clients/{restaurant}/', 'RoutingController@returnExcelClients');
    Route::get('/scheda_cliente/{restaurant}/{client}/', 'RoutingController@getSingleRestaurantClientScheda');

    Route::get('orders/pay_single/{reservation_id}', 'OrdersController@payFromRedirect');
    Route::get('orders/confirm_preorder_paid', 'OrdersController@doPayment');
    Route::get('orders/cancel', 'OrdersController@cancelPayment');
    

    Route::group(
        [
        'middleware' => ['isAdmin'],
        ], function () {   
        Route::get('/generasitemap','AdministrationController@generateSitemap');
    });

    Route::get('/accedi/', ['uses'=>'RoutingController@returnLogin']);
    Route::get('/registrati/', ['uses'=>'RoutingController@returnRegister']);
    Route::get('/registrati/{cookie}/', ['uses'=>'RoutingController@returnRegister']);
    Route::get('/recupera-password/', ['uses'=>'RoutingController@returnRecovery']);
    Route::get('/attiva-account/', ['uses'=>'RoutingController@returnActivate']);

    Route::get('/preorder/{restaurant}/{reservation}', ['uses'=>'RoutingController@returnPreOrder']);

    Route::get('/printMenuPage/{restaurant}/{menuindex}/{menustyle}/{pdf}/', ['middleware' => 'doNotCacheResponse','uses'=>'RoutingController@returnPrintMenu']);
    Route::get('/printPDF/{restaurant}/{menuindex}/{menustyle}/', ['middleware' => 'doNotCacheResponse','uses'=>'RoutingController@returnPrintPDF']);


    Route::get('/mostra/prenotazioni/{restaurant}/{type}/{from}/{to}/{token}/', ['middleware' => 'doNotCacheResponse','uses'=>'RoutingController@returnViewReservations']);
    Route::get('/mostra/prenotazioni/{restaurant}/{type}/{date}/{to}/{token}/', ['middleware' => 'doNotCacheResponse','uses'=>'RoutingController@returnViewReservations']);
    Route::get('/export/prenotazioni/{restaurant}/{type}/{from}/{to}/', ['middleware' => 'doNotCacheResponse','uses'=>'RoutingController@returnExportReservationsPDF']);
    Route::get('/export/prenotazioni/{restaurant}/{type}/{date}/', ['middleware' => 'doNotCacheResponse','uses'=>'RoutingController@returnExportReservationsPDF']);


    
    Route::post('/importframe/facebook/', ['middleware' => 'doNotCacheResponse','uses'=>'RoutingController@returnFacebookFrame']);
    Route::get('/importframefake/facebook/', ['middleware' => 'doNotCacheResponse','uses'=>'RoutingController@returnFakeFacebookFrame']);


    

    Route::get('/tipicucina', ['uses'=>'TypesController@returnCucine']);
    Route::get('/regionicucina', ['uses'=>'RegionsController@returnRegioniCucine']);
    Route::get('/tipilocale', ['uses'=>'TipoLocaleController@returnLocali']);
    Route::get('/servizilocali', ['uses'=>'ServicesController@returnServizi']);

    
    Route::get('/iframetest/', ['uses' => 'RoutingController@returnIframeTestView']);
    
    Route::get('/iframe/{restaurant}', ['middleware' => 'doNotCacheResponse','uses' => 'RoutingController@returnIframeView']);
    Route::get('/iframe/{restaurant}/t/{type}', ['middleware' => 'doNotCacheResponse','uses' => 'RoutingController@returnIframeView']);
    
    Route::get('/iframe/{restaurant}/{test}', ['uses' => 'RoutingController@returnIframeView']);
    Route::get('/iframe/{restaurant}/{test}/t/{type}/', ['uses' => 'RoutingController@returnIframeView']);
    
    //Route::get('/reservationframe/facebook/{restaurant}', ['middleware' => 'doNotCacheResponse','uses' => 'RoutingController@returnFacebookIframeView']);
    //Route::get('/menuframe/facebook/{type}/{restaurant}/', ['middleware' => 'doNotCacheResponse','uses' => 'RoutingController@returnFacebookIframeView']);

    Route::get('/static/{token}/menu/frameloader', ['middleware' => 'doNotCacheResponse','uses' => 'IframeController@returnLoaderMenuFile']);
    Route::get('/static/{token}/frameloader', ['middleware' => 'doNotCacheResponse','uses' => 'IframeController@returnLoaderFile']);
    
    
    Route::get('templates/iframe/{template}', ['uses' => 'RoutingController@returnIframeTemplate'])->where('template_name', '[A-Za-z0-9\_]+');
    Route::get('templates/facebook/{template}', ['uses' => 'RoutingController@returnFacebookTemplate'])->where('template_name', '[A-Za-z0-9\_]+');


    Route::get('/templates/{template_name}', ['uses' => 'RoutingController@returnFrontendTemplate'])->where('template_name', '[A-Za-z\_]+');


    /**
    *
    * Routes del sito web
    *
    **/
    
    Route::get('/', ['uses' => 'RoutingController@returnHome']);

    Route::get('/image/{id}/{format}/{h}/{w}/{quality}', ['uses' => 'ImagesController@getImageFromUrl']);


    Route::get('/socialog/{provider}', ['middleware' => 'doNotCacheResponse', 'uses' => 'AuthController@redirectToProvider'])->where('provider', '[A-Za-z]+');
    Route::get('/socialogged/{provider}', ['middleware' => 'doNotCacheResponse', 'uses' => 'AuthController@handleProviderCallback'])->where('provider', '[A-Za-z]+');

    // Scloby
    //Route::get('/scloby/', ['middleware' => 'doNotCacheResponse', 'uses' => 'SclobyController@getSclobyCode']);

    
    Route::get('/u/{user_id}', ['as'=>'/u/','uses' => 'RoutingController@returnUserProfile']);
    
    Route::get('/s/{string}/', ['middleware' => 'doNotCacheResponse', 'uses' => 'RoutingController@returnQueryStringResult']);
    Route::get('/s/{string}/{page}/', ['middleware' => 'doNotCacheResponse', 'uses' => 'RoutingController@returnQueryStringResultPaged']);
    Route::get('/s/{string}/{lat}/{lng}/', ['uses' => 'RoutingController@returnQueryStringResult']);
    Route::get('/s/{string}/{lat}/{lng}/{page}', ['uses' => 'RoutingController@returnQueryStringResult']);
    
    Route::get('/ricerca-avanzata/{advanced}/', ['uses' => 'RoutingController@returnAdvancedSearch']);
    Route::get('/ricerca-avanzata/{advanced}/{page}/', ['uses' => 'RoutingController@returnAdvancedSearch']);
    
    Route::get('/modifica-ristorante/{restaurant_slug}/', ['uses' => 'RoutingController@returnEditView']);

    Route::get('/piatto/{piatto}/', ['uses' => 'RoutingController@returnRestaurantPlate']);
    Route::get('/community/piatto/{piatto}/', ['uses' => 'RoutingController@returnCommunityPlate']);
    
    Route::get('/citta/{city}/', ['uses' => 'RoutingController@returnArchiveCity']);
    Route::get('/citta/{city}/{page}/', ['uses' => 'RoutingController@returnArchiveCityPaged']);
    Route::get('/citta/{city}/{lat}/{lng}', ['uses' => 'RoutingController@returnArchiveCity']);
    Route::get('/citta/{city}/{lat}/{lng}/{page}', ['uses' => 'RoutingController@returnArchiveCity']);

    

    Route::get('/b/{cucina}/{stringa}/', ['uses' => 'RoutingController@returnArchiveFromBlog']);
    Route::get('/b/{cucina}/{stringa}/{page}/', ['uses' => 'RoutingController@returnArchiveFromBlogPaged']);
    



    Route::get('/tipo-cucina/{cucina}/', ['as'=>'tipo-cucina', 'uses' => 'RoutingController@returnArchiveCustom']);
    Route::get('/tipo-cucina/{cucina}/{page}/', ['as'=>'tipo-cucina', 'uses' => 'RoutingController@returnArchiveCustomPaged']);
    Route::get('/tipo-cucina/{cucina}/{lat}/{lng}', ['as'=>'tipo-cucina', 'uses' => 'RoutingController@returnArchiveCustom']);
    Route::get('/tipo-cucina/{cucina}/{lat}/{lng}/{page}', ['as'=>'tipo-cucina', 'uses' => 'RoutingController@returnArchiveCustom']);

    Route::get('/regione-cucina/{cucina}/', ['as'=>'regione-cucina', 'uses' => 'RoutingController@returnArchiveCustom']);
    Route::get('/regione-cucina/{cucina}/{page}/', ['as'=>'regione-cucina', 'uses' => 'RoutingController@returnArchiveCustomPaged']);
    Route::get('/regione-cucina/{cucina}/{lat}/{lng}', ['as'=>'regione-cucina', 'uses' => 'RoutingController@returnArchiveCustom']);
    Route::get('/regione-cucina/{cucina}/{lat}/{lng}/{page}', ['as'=>'regione-cucina', 'uses' => 'RoutingController@returnArchiveCustom']);

    Route::get('/servizi/{servizio}/', ['as'=>'servizi', 'uses' => 'RoutingController@returnArchiveCustom']);
    Route::get('/servizi/{servizio}/{page}/', ['as'=>'servizi', 'uses' => 'RoutingController@returnArchiveCustomPaged']);
    Route::get('/servizi/{servizio}/{lat}/{lng}', ['as'=>'servizi', 'uses' => 'RoutingController@returnArchiveCustom']);
    Route::get('/servizi/{servizio}/{lat}/{lng}/{page}', ['as'=>'servizi', 'uses' => 'RoutingController@returnArchiveCustom']);

    Route::get('/tipo-locale/{tipo}/', ['as'=>'tipo-locale', 'uses' => 'RoutingController@returnArchiveCustom']);
    Route::get('/tipo-locale/{tipo}/{page}/', ['as'=>'tipo-locale', 'uses' => 'RoutingController@returnArchiveCustomPaged']);
    Route::get('/tipo-locale/{tipo}/{lat}/{lng}', ['as'=>'tipo-locale', 'uses' => 'RoutingController@returnArchiveCustom']);
    Route::get('/tipo-locale/{tipo}/{lat}/{lng}/{page}', ['as'=>'tipo-locale', 'uses' => 'RoutingController@returnArchiveCustom']);





    Route::get('/{static_page}', ['uses' => 'RoutingController@returnStaticPage']);
    
    Route::get('/{city}/{restaurant_slug}/', ['middleware' => 'doNotCacheResponse','uses' => 'RoutingController@returnRestaurant']);
    Route::get('/{city}/{restaurant_slug}/{cookie}/', ['middleware' => 'doNotCacheResponse', 'uses' => 'RoutingController@returnRestaurant']);






    
        
    
        


    Route::get('/uploads/', ['uses' => 'RoutingController@returnFrontendView']);

    Route::group(
        [
        'prefix' => 'api/v1'
        ], function () {			
            //Route::get('restaurants', 'RestaurantController@index');
            Route::post('search/restaurants', 'SearchController@searchByQueryString');
            Route::get('city/{city}', ['uses'=>'RestaurantController@getByCity']);	
            Route::get('list/servizi', 'ServicesController@index');	
            Route::get('list/tipi', 'TypesController@index');
            Route::get('list/regioni', 'RegionsController@index');
            Route::post('getRelated', 'SearchController@searchRelated');
            Route::post('searchAdvanced', 'SearchController@searchByValues');
        // All my routes that needs a logged in user
    });

    /**
        * Tested Routes
    */

    Route::group(
        [
        'prefix' => 'api/v1'
        ], function () {		
            Route::post('view_single', 'UserController@show');
            Route::post('is_logged', 'AuthController@isLogged');
            Route::post('recovery_pwd', 'UserController@recoveryUserPassword');
            Route::post('resend_verification', 'UserController@resendVerification');
            Route::post('get_favourites', 'UserController@getFavouriteList');
    });



    // Pagamenti piani di abbonamento
    Route::get('/payment/success', ['uses' => 'PaymentsController@addBillingPlanToUser']);

    Route::group(
    [
    'prefix' => 'api/v1/payments',
    ], function () {
        

        Route::post('plan', ['uses' => 'PaymentsController@setUserBillingPlan']);

        Route::group(['middleware'=>'isUser'], function(){
            Route::post('cancelplan', ['uses' => 'PaymentsController@cancelBillingPlanToUser']);
            Route::post('suspendplan', ['uses' => 'PaymentsController@suspendBillingPlanToUser']);
            Route::post('reactivateplan', ['uses' => 'PaymentsController@reactivateBillingPlanToUser']);
        });           
        
    });

    Route::group(
        [
        'prefix' => 'api/v1/tools'
        ], function () {		
            Route::post('get_main_site', 'ToolController@getSiteInfo');
            Route::post('get_directions', 'GoogleAddressController@returnDirections');
            Route::post('validate_address', 'GoogleAddressController@validateAddress');
            Route::post('contact', 'ToolController@contactMail');
            Route::post('reccomendMail', 'ToolController@reccomendMail');

            Route::post('getLDSInformations', 'SeoController@returnRestaurantLDS');

            Route::post('isToRender', 'ToolController@isToRender');
            Route::post('storeStatic', 'ToolController@addStaticPage');

            Route::post('saveSearchRequest', 'SearchRequestsController@saveRequestNewsletter');

            Route::group(
            [
            'middleware' => 'isEditor'
            ], function () {    
                Route::post('getRestaurantEmpty', 'ToolController@getRestaurantWithoutImages');
                Route::post('saveSingleImage', 'ToolController@sobstituteRestaurantImage');
            });
    });

    

    Route::group(
        [
        'prefix' => 'api/v1/iframe'
        ], function () {        
            Route::group(['middleware'=>'isAdmin'], function(){
                Route::post('get_all', 'IframeController@index');
                Route::post('delete', 'IframeController@delete');
            });
            Route::group(['middleware'=>'canManageRestaurant'], function(){
                Route::post('add', 'IframeController@store');
                Route::post('addURL', 'IframeController@storeURL');
                Route::post('mod', 'IframeController@update');
                Route::post('view_by_restaurant', 'IframeController@returnByRestaurant');
            });
            Route::group(['middleware'=>'isUser'], function(){
                Route::post('view_mine', 'IframeController@returnMine');            
            });     
    });

    

    Route::group(
        [
        'prefix' => 'api/v1/newsletter'
        ], function () {        
            Route::post('view_single', 'NewsletterController@showSingle');
            Route::post('register', 'NewsletterController@store');
            Route::post('remove_subscriber_from_email', 'NewsletterController@destroyFromEmail');
    });

    Route::group(
        [
        'prefix' => 'api/v1/services'
        ], function () {        
            Route::post('view_all', 'ServicesController@index');
            Route::post('view_single', 'ServicesController@show');
    });
    Route::group(
        [
        'prefix' => 'api/v1/regions'
        ], function () {        
            Route::post('view_all', 'RegionsController@index');
            Route::post('view_single', 'RegionsController@show');
    });
    Route::group(
        [
        'prefix' => 'api/v1/types'
        ], function () {        
            Route::post('view_all', 'TypesController@index');
            Route::post('view_single', 'TypesController@show');
        
    });

    Route::group(
        [
        'prefix' => 'api/v1/tipolocale'
        ], function () {        
            Route::post('view_all', 'TipoLocaleController@index');
            Route::post('view_single', 'TipoLocaleController@show');
        
    });

    Route::group(
        [
        'prefix' => 'api/v1/locality'
        ], function () {        
            Route::post('view_all', 'LocationsController@index');
            Route::post('view_single', 'LocationsController@show');
            
    });

    Route::group(
        [
        'prefix' => 'api/v1/creditcard'
        ], function () {        
            Route::post('view_all', 'CreditCardController@index');
            Route::post('view_single', 'CreditCardController@show');            
            
    });

    Route::group(
        [
        'prefix' => 'api/v1/notifications'
        ], function () {        
            Route::group(['middleware'=>'isUser'], function(){
                Route::post('view_to_read', 'NotificationsController@get_to_read');         
            });

            Route::group(['middleware'=>'isMeOrIsAdmin'], function(){
                Route::post('view_mine', 'NotificationsController@get_by_user');
                Route::post('set_read', 'NotificationsController@set_read');
                Route::post('set_all_read', 'NotificationsController@set_all_read_by_user');
            });
            
            Route::group(['middleware'=>'isAdmin'], function(){
                Route::post('view_all', 'NotificationsController@index');
                Route::post('delete_old', 'NotificationsController@delete_old');
            });     
    });

    

    Route::group(
        [
        'prefix' => 'api/v1/images'
        ], function () {        
            Route::post('view_all', 'ImagesController@index');
            Route::post('view_single', 'ImagesController@show');
            Route::post('view_by_type', 'ImagesController@get_by_type'); #
            
            Route::group(['middleware'=>'isEditor'], function(){
                Route::post('remove_single', 'ImagesController@destroy');
            });
            Route::group(['middleware'=>'isUser'], function(){
                Route::post('add_single', 'ImagesController@store');
            }); 
            // upload e rimozione immagini da utenti
            Route::group(['prefix'=>'public', 'middleware'=>'isClient'], function(){
                Route::post('add_single', 'ImagesController@store');
                Route::group(['middleware'=>'isMineImage'], function(){
                    Route::post('remove_single', 'ImagesController@destroy');
                });
            });     
    });

    
    
    
    Route::group(
        [
        'prefix' => 'api/v1/restaurants'
        ], function () {        
            
            Route::post('view_all', 'RestaurantController@index');
            Route::post('view_all_filtered', 'RestaurantController@get_for_simple_results');

            Route::post('view_all_clients', 'RestaurantController@get_clients');
            Route::post('view_all_filtered_clients', 'RestaurantController@get_filtered_clients');
            
            Route::post('view_single', 'RestaurantController@show');

            Route::post('get_fasce', 'RestaurantController@getFasce');

            Route::post('view_by_facebook_page', 'RestaurantController@getByFacebookPage');

            Route::group(
                [
                'prefix' => 'search'
                ], function () {        
                    Route::post('search_by_name', 'SearchController@searchByName');
                    Route::post('search_by_slug', 'SearchController@searchBySlug');
                    Route::post('search_by_lat_lng', 'SearchController@searchByLatLng');
                    Route::post('search_by_string', 'SearchController@searchByString');
                    Route::post('search_by_city', 'SearchController@searchByCity');
            });


            Route::group(['middleware'=>'isUser'], function(){
                Route::post('add_single', 'RestaurantController@store');
                Route::post('claim', 'RestaurantController@claim'); 
            });

            Route::group(['middleware'=>'isClient'], function(){
                Route::post('view_all_mine', 'RestaurantController@get_mine');
                Route::post('view_all_mine_claimed', 'RestaurantController@getClaimedByMe');
                Route::post('view_all_mine_filtered', 'RestaurantController@get_mine_filtered');                
            });

            Route::group(['middleware'=>'isEditor'], function(){
                Route::post('edit_single_admin', 'RestaurantController@adminUpdate');
            });

            Route::group(['middleware'=>'canManageRestaurant'], function(){
                Route::post('edit_single', 'RestaurantController@update');
                Route::post('remove_single', 'RestaurantController@destroy');
                Route::post('change_prenotazione', 'RestaurantController@changeCanReserve');
                Route::post('change_ordinazione', 'RestaurantController@changeCanOrders');
                Route::post('change_take_away', 'RestaurantController@changeTakeAway');
                

                Route::post('change_range', 'RestaurantController@setRestaurantDeliveryRange');
                Route::post('set_zip', 'RestaurantController@setRestaurantDeliveryZip');

                Route::post('set_special_date', 'RestaurantController@addRestaurantImage');
                Route::post('remove_special_date', 'RestaurantController@removeSpecialDate');
                
                Route::post('change_orari', 'RestaurantController@manageOrariRestaurant');
                Route::post('add_struttura', 'RestaurantController@addChangeStrutturaRestaurant');
                Route::post('add_menu', 'RestaurantController@addChangeMenuRestaurant');
                Route::post('upload_image', 'RestaurantController@addRestaurantImage');

                Route::post('add_facebook_page', 'RestaurantController@addFacebookPage');
                Route::post('remove_facebook_page', 'RestaurantController@removeFacebookPage');

                Route::post('get_stats', ['uses'=>'StatController@restaurantStats']);

            });
            
            Route::group(['middleware'=>'isAdmin'], function(){
                Route::post('remove_single', 'RestaurantController@destroy');
                Route::post('approve_single', 'RestaurantController@approve');
            });

            Route::group(['middleware'=>'isAdmin'], function(){
                Route::post('approve_claim', 'RestaurantController@approveClaim'); //#
                Route::post('get_claimed', 'RestaurantController@getClaimed'); //#
                Route::post('view_by_user', 'RestaurantController@getByUser');
                Route::post('view_all_claimed', 'RestaurantController@getClaimedBy');
            });

            Route::group(
            [
            'prefix' => 'clients',
            'middleware'=>'canManageRestaurant'
            ], function () {  
                Route::post('add_custom_client', 'ClientController@addCustomClient');
                Route::post('edit_custom_client', 'ClientController@editCustomClient');
                Route::post('delete_custom_client', 'ClientController@deleteCustomClient');
                Route::post('get_restaurant_clients', 'ClientController@getRestaurantClients');
                Route::post('get_client_info', 'ClientController@getClientInfo');
            });
    });

    Route::group(
        [
        'prefix' => 'api/v1/reservations'
        ], function () {		
            
            Route::post('get_restaurant_fasce', 'ReservationRequestcontroller@getRestaurantFasce'); 
            Route::post('get_restaurant_avaible_fasce', 'ReservationRequestcontroller@getRestaurantAvaibleFasce');
            
            Route::group(['middleware'=>'isUser'], function(){
                Route::post('add_reservation', 'ReservationRequestcontroller@reserve'); #
                Route::post('approve_date_change', 'ReservationRequestcontroller@approveChangeDate');
                Route::post('refuse_date_change', 'ReservationRequestcontroller@refuseChangeDate');
                Route::post('view_mine', 'ReservationRequestcontroller@viewMine'); 
                Route::post('view_single', 'ReservationRequestcontroller@viewSingle'); 
                Route::post('delete_reservation', 'ReservationRequestcontroller@setRemoved');
            });

            Route::group(['middleware'=>'canManageRestaurant'], function(){
                Route::post('confirm_reservation', 'ReservationRequestcontroller@confirm'); 
                Route::post('view_by_date', 'ReservationRequestcontroller@viewByDate');#
                Route::post('assign_foodcoin', 'ReservationRequestcontroller@assignFoodcoin');
                Route::post('remove_foodcoin', 'ReservationRequestcontroller@removeFoodcoin');
                Route::post('change_reservation_date', 'ReservationRequestcontroller@changeDate');
                Route::post('view_by_from_to', 'ReservationRequestcontroller@indexByFromToAndRestaurant');
                Route::post('add_manual_reservation', 'ReservationRequestcontroller@manualReservation'); #
            });
            
            Route::group(['middleware'=>'isAdmin'], function(){
                Route::post('view_all', 'ReservationRequestcontroller@index'); #
            });	
    });

    Route::group(
        [
        'prefix' => 'api/v1/plates'
        ], function () {        
            
            Route::post('view_by_restaurant', 'PlatesController@indexByRestaurant'); 

            Route::group(['middleware'=>'canManageRestaurant'], function(){
                Route::post('delete', 'PlatesController@destroy');
            });
                    
            Route::group(['middleware'=>'isAdmin'], function(){
                Route::post('view_all', 'PlatesController@index'); #
            }); 


            Route::post('search/search_by_name', 'SearchController@searchPlateByName');
            Route::post('viewSingle', 'PlatesController@viewSingle');

            Route::group(['prefix' => 'community'], function(){
                Route::group(['middleware'=>'isUser'], function(){
                    Route::post('add', 'UserPlatesController@addPlateImage');
                });
                Route::post('viewByUser', 'UserPlatesController@viewPlateImageByUser');
                Route::post('viewByPlate', 'UserPlatesController@viewPlateImageByPlate');
                Route::post('viewSingle', 'UserPlatesController@viewSingle');
            }); 
    });

    Route::group(
        [
        'prefix' => 'api/v1/orders'
        ], function () {        
            Route::group(
            [
            'middleware' => ['onlyLogged'],
            ], function () {        
                Route::post('add_preorder', 'OrdersController@addPreorder'); 
        });

        Route::post('pay_preorder', 'OrdersController@payPreorder'); 
        
    });
    


    


    Route::group(
        [
        'middleware' => ['notLogged'],
        'prefix' => 'api/v1'
        ], function () {        
            Route::post('register', 'UserController@store');
            Route::post('login', 'AuthController@login');
    });

    Route::group(
        [
        'middleware' => ['onlyLogged'],
        'prefix' => 'api/v1'
        ], function () {        
            Route::post('logout', 'AuthController@logout');
            Route::post('addToFavourite', 'UserController@addRemoveFavourite'); #
            Route::post('setFavourites', 'UserController@setFavourites'); #
            Route::post('addToWishlist', 'UserController@addRemoveWishlist'); #
            Route::post('setAllergeni', 'UserController@setAllergeni');

            
            Route::post('search_user', 'UserController@searchUsers');
            Route::post('invite_friends', 'UserController@inviteFriends');
            /*Route::group(
                [
                'middleware' => ['isMeOrIsAdmin'],
                ],function(){
                    Route::post('get_favourites', 'UserController@getFavouriteList');
            });*/
    });

    Route::group(
        [
        'middleware' => ['isAdmin'],
        'prefix' => 'api/v1'
        ], function () {        
            Route::post('view_users', 'UserController@index');
            Route::post('change_user_role', 'UserController@changeUserRole');
            Route::post('remove_single', 'UserController@destroy');
    });

    Route::group(
        [
        'middleware' => ['isMeOrIsAdmin'],
        'prefix' => 'api/v1'
        ], function () {        
            Route::post('edit_user', 'UserController@changeUserInfo');
            Route::post('mod_avatar', 'UserController@changeAvatar');
            Route::post('mod_cover', 'UserController@changeCoverImage');
            Route::post('mod_email', 'UserController@changeUserEmail');
            Route::post('mod_pwd', 'UserController@changeUserPassword');
            Route::post('complete_profile', 'UserController@completeProfile');
            Route::post('add_current_address', 'UserController@setCurrentLocation');
    });

    Route::group(
        [
        'prefix' => 'api/v1/social'
        ], function () { 

            Route::post('isFollowing', 'SocialController@isFollowing');
            Route::post('canAddToFriends', 'SocialController@canAddToFriends');
            Route::post('getWishlist', 'UserController@getWishList');

            Route::group(
            [
            'middleware' => ['onlyLogged'],
            ], function () { 
                Route::post('addToFollowing', 'SocialController@addRemoveFollowing');                
            
                Route::post('addFriendRequest', 'SocialController@addFriendRequest');
                Route::post('acceptFriendRequest', 'SocialController@acceptFriendRequest');
                Route::post('refuseFriendRequest', 'SocialController@refuseFriendRequest');
                Route::post('removeFriend', 'SocialController@removeFriend');
            });

            
            Route::post('viewFriends', 'SocialController@viewFriends');
            Route::post('viewFollowers', 'SocialController@viewFollowers');
            Route::post('viewFollowing', 'SocialController@viewFollowing');
            Route::post('viewDashboard', 'SocialController@viewDashboard');

            Route::group(
            [
            'middleware' => ['isMeOrIsAdmin']
            ], function () {   
                Route::post('viewFriendRequest', 'SocialController@viewFriendRequest');
            });

    });

    Route::group(
        [
        'prefix' => 'api/v1/recommendation'
        ], function () {        
            Route::group(
            [
            'middleware' => ['onlyLogged']
            ], function () { 
                Route::post('publishRecommendation', 'RecommendationController@publishRecommendation');
            }); 
            
            Route::post('viewByUser', 'RecommendationController@viewByUser');
            Route::post('viewByPlate', 'RecommendationController@viewByPlate');
            Route::post('viewByRestaurant', 'RecommendationController@viewByRestaurant');
            Route::post('viewNear', 'RecommendationController@viewNear');

    });

    Route::group(
        [
        'prefix' => 'api/v1/recommendation-request'
        ], function () {        
            
            Route::group(
            [
            'middleware' => ['onlyLogged']
            ], function () { 
                Route::post('askRecommendationRequest', 'RecommendationRequestController@askRecommendationRequest');
                Route::post('answerRecommendationRequest', 'RecommendationRequestController@answerRecommendationRequest');
            }); 
            Route::post('viewByUserRequests', 'RecommendationRequestController@viewByUserRequests');
            Route::post('viewNearRequests', 'RecommendationRequestController@viewNearRequests');
            Route::post('viewAnswersRequests', 'RecommendationRequestController@viewAnswersRequests');

    });

    Route::group(
    [
    'prefix' => 'v1/api/public',
    'middleware' => 'apiRequest'
    ], function () {

        Route::post('test','APIRequestController@test');
        Route::post('getInfo','APIRequestController@getWebsiteInfo');
        
        Route::post('mailLogin','APIRequestController@accessWithEmail');
        Route::post('socialLogin','APIRequestController@accessWithSocial');
        Route::post('getUser','APIRequestController@getUser');
        Route::post('register','APIRequestController@register');
        Route::post('completeProfile','APIRequestController@completeProfile');
        Route::post('addRemoveFavourite','APIRequestController@addRemoveFavourite');
        Route::post('getRestaurants','APIRequestController@getRestaurantByLatLng');
        Route::post('getRestaurantsByCity','APIRequestController@getRestaurantByCity');
        Route::post('getRestaurantsByAdvancedSearch','APIRequestController@searchByValues');
        Route::post('getSingleRestaurantReservationAvaibleTime','APIRequestController@getRestaurantAvaibleFasce');

        Route::post('reserve','APIRequestController@reserve');
        Route::post('get_user_reservations','APIRequestController@getUserReservation');
        Route::post('getSingleRestaurant','APIRequestController@getSingleRestaurant');
        Route::post('addOrder','APIRequestController@addOrder');
        Route::post('setOrderStatus','APIRequestController@setOrderStatus');
        Route::post('viewUserOrders','APIRequestController@viewUserOrders');
        Route::post('viewSingleOrder','APIRequestController@viewSingleOrder');
        Route::post('wannaBeDriver','APIRequestController@wannaBeDriver');
        Route::post('setDriverPosition','APIRequestController@setDriverPosition');
        Route::post('assignOrderToDriver','APIRequestController@assignOrderToDriver');
        Route::post('assignRatingToDriver','APIRequestController@assignRatingToDriver');
        Route::post('getMineNotified','APIRequestController@getMineNotified');
        Route::post('getDriverOrders','APIRequestController@getDriverOrders');

        Route::post('addPreOrder','APIRequestController@addPreorder');
        Route::post('deleteReservation','APIRequestController@deleteReservation');

}); 


    

    Route::get('error/api/get/immagini_ristoranti', ['uses' => 'AdministrationController@getNotImagesFromRistos']);
    Route::get('{any?}', ['uses' => 'RoutingController@returnFrontendView'])->where('any', '.*');
    
?>