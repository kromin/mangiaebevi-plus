<?php

namespace MangiaeBevi;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

class SearchRequests extends Eloquent
{
    //
    protected $collection = 'search_request_collection';
    protected $fillable = ['query','loc'];
}
