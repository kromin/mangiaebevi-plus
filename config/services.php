<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION'),//'us-east-1',
    ],

    'stripe' => [
        'model'  => MangiaeBevi\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => env('FB_KEY'),
        'client_secret' => env('FB_SECRET'),
        'redirect' => env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/socialogged/facebook'//url('/socialogged/facebook'),
    ],
    'twitter' => [
        'client_id' => env('TW_KEY'),
        'client_secret' => env('TW_SECRET'),
        'redirect' => env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/socialogged/twitter'//url('/socialogged/twitter'),
    ],
    'linkedin' => [
        'client_id' => env('LI_KEY'),
        'client_secret' => env('LI_SECRET'),
        'redirect' => env('APP_PROTOCOL').'://'.env('APP_DOMAIN').'/socialogged/linkedin'//url('/socialogged/linkedin'),
    ],

];
